<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RealNotification extends Model
{
   public $timestamps=TRUE;
   protected $fillable = ['notification_type','notification_id','notification_for','notification_message','seen_at'];
   protected $guarded = []; 
}

