<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ExperienceImage extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'experience_id ', 'created_at', 'updated_at','location_id', 'image_name', 'status'
    ];


    /**
	 * Get the review and ratings of experience for homepage
	 */
	public function experience() {
	    return $this->belongsTo('App\Experience');
	}
}
