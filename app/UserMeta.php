<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserMeta extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'why_join_us', 'unique_experiences', 'user_interest','ability_skills', 'company_name','company_registration_no', 'company_web_link', 'legal_requirements','guiding_text','other_text','describes_you_best','describes_you_best_2','current_status','gender','dob','user_residence','latitude','longitude','social_links','account_type'
    ];
    public function experiences()
    {
    	  return $this->belongsTo('App\Experience','user_id','user_id');
         
    }
}
