<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ExperienceAdditionalPrice extends Model
{
    /**
    * The attributes that are mass assignable.
    *
    * @var array
    */
    protected $fillable = [
        'id', 'created_at', 'updated_at', 'experience_id' , 'additional_person','price_per_person','additional_price'
    ];

    /**
    * Get the experience featur relation of experience for homepage
    */
	public function experience() {
	    return $this->belongsTo('App\Experience');
	}
}
