<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ExperienceLanguageRelation extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id','user_id', 'created_at', 'updated_at', 'experience_id','language_id','type'
    ];

    //
    public function experience() {
	    return $this->belongsTo('App\Experience');
	}
}
