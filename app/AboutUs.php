<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AboutUs extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'page_slug ', 'page_content', 'page_title', 'page_meta', 'page_description', 'page_featured_image', 'created_at', 'updated_at', 'about_first_image', 'about_second_image' 
    ];
}
