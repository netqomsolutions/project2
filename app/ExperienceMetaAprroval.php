<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ExperienceMetaAprroval extends Model
{
     protected $fillable = [
        'id', 'provider_description', 'created_at', 'updated_at', 'experience_id' , 'exp_additional_information','exp_price_included','exp_price_excluded','exp_other_info','is_children_discount_active','children_age_for_free','children_age_for_discount','children_discount_1','meet_location'
    ];
    public function experienceAprroval() {
	    return $this->belongsTo('App\ExperienceAprroval');
	}
}
