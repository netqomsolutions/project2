<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CountryCode extends Model
{
    public $timestamps = true;
   protected $fillable = [
        'id', 'iso', 'name','nicename', 'iso3','numcode','phonecode','created_at', 'updated_at' 
    ];
}
