<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ExperienceFeatureRelation extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'status', 'created_at', 'updated_at', 'experience_id' , 'experience_feature_id'
    ];

    /**
	 * Get the experience featur relation of experience for homepage
	 */
	public function experience() {
	    return $this->belongsTo('App\Experience');
	}
}
