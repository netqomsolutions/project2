<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HowWork extends Model
{
   protected $fillable = [
        'id', 'title', 'description','type','created_by','status', 'created_at', 'updated_at' 
    ];
}
