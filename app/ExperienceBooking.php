<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ExperienceBooking extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'status', 'user_id', 'scout_id', 'experience_id','booking_date','booking_start_time','booking_end_time','amount','total_price','discount','children','adults','infants','charge_id','amount_captured','amount_refunded','last4','network','exp_month','exp_year','currency','order_number','slot_id','ip'
    ];
    protected $guarded = []; 

    public function traveler(){
    	return $this->belongsTo('App\User','user_id');
    }

    public function scout(){
        return $this->belongsTo('App\User','scout_id');
    }

    public function experience(){
    	return $this->belongsTo('App\Experience','experience_id');
    }
    public function experience_meta_detail(){
        return $this->belongsTo('App\ExperienceMeta','experience_id','experience_id');
    }
     public function review_ratings()
    {
        return $this->hasMany('App\ReviewRating','booking_id');
    }
}
