<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Testimonial extends Model
{
     protected $fillable = [
        'id', 'title', 'description','person_name','person_designation','profile_image','created_by','status', 'created_at', 'updated_at' 
    ];
}
