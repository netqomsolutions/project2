<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class SendMail extends Mailable
{
    use Queueable, SerializesModels;

    public $data;
    public $templatename;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data,$template)
    {
        $this->data = $data;
        $this->templatename = $template;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    
    public function build()
    {
         if(isset($data['email'])){
             $email =$data['email'];
             $name =$data['name'];
         }else{
             $email ='info@localsfromzero.org';
             $name ='LocalsFromZero';
         }
          // $fromEmail= env('mail_from_address');
       return $this->from($email,$name)->subject($this->data['subject'])->view('mail.'.$this->templatename)->with('data', $this->data);
    }
}
