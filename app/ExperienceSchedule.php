<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ExperienceSchedule extends Model
{
     protected $fillable = [
        'id', 'experience_id', 'schedule_type','day','start_date', 'end_date','start_time', 'end_time', 'recurrence', 'created_by','status', 'created_at', 'updated_at' 
    ];
    public function experience() {
	    return $this->belongsTo('App\Experience');
	}
}
