<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserLanguage extends Model
{
    protected $fillable = [
    	'id','user_id','language_id','created_at','updated_at'
    ];
}
