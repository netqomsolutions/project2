<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class User extends Authenticatable implements MustVerifyEmail
{
    use Notifiable;
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
     //protected $softCascade = ['experiences','review_ratings'];
    protected $fillable = [
        'user_fname', 'user_lname', 'user_role', 'user_mobile','user_mobile_code', 'user_address','email', 'password', 'status','user_image','user_image_path','user_english_level','user_primary_language','user_other_languages','about_me','created_by','stripe_id','user_stripe_account_id','is_scout','ip','provider','provider_id','access_token','email_verified_at'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
   
    /**
     * Get the review of scout for homepage
     */

    public function review_ratings()
    {
        return $this->hasMany('App\ReviewRating','review_for','id');
    }
     public function scout_review_ratings()
    {
        return $this->hasMany('App\ReviewRating','review_for_scout','id');
    }


    /**
     * Get the experience for homepage
     */
    public function experiences()
    {
        return $this->hasMany('App\Experience','user_id','id');
    }
    public function hostexperiences()
    {
        return $this->hasMany('App\Experience','assigned_host','id');
    }

    public function user_meta(){
        return $this->hasOne('App\UserMeta','user_id');
    }

    public function user_bank_detail(){
        return $this->hasOne('App\UserBankDetail','user_id');
    }

    public function scout() {
    return $this->belongsTo('App\User','created_by');
    }

    protected static function boot()
    {
        parent::boot();

      static::deleting(function($users) {
         foreach ($users->review_ratings()->get() as $review) {
            $review->delete();
         } 
         foreach ($users->experiences()->get() as $exp) {
            $exp->delete();
         }
      });
    }
    public function user_languages()
    {
        return $this->hasOne('App\Language','id','user_primary_language');
    }

    public function messages()
    {
        return $this->hasMany('App\Message','sender_id','id');
    } 
    public function user_experience_languages()
    {
        return $this->hasMany('App\ExperienceLanguageRelation');
    }
    
}
