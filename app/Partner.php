<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Partner extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'link', 'image'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
   
}
