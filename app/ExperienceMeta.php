<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ExperienceMeta extends Model
{
     /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'provider_description', 'created_at', 'updated_at', 'experience_id' , 'exp_additional_information','exp_price_included','exp_price_excluded','exp_other_info','is_children_discount_active','children_age_for_free','children_age_for_discount','children_discount_1','meet_location'
    ];

    /**
	 * Get the experience featur relation of experience for homepage
	 */
	public function experience() {
	    return $this->belongsTo('App\Experience');
	}
}
