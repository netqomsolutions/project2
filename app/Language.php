<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Language extends Model
{
	public $timestamps = true;
   protected $fillable = [
        'id', 'name', 'short','created_at', 'updated_at' 
    ];
}
