<?php

namespace App\Http\Controllers;
use Spaceinvoices\Spaceinvoices;
use Spaceinvoices\Organizations;
use Spaceinvoices\Clients;
use Spaceinvoices\Accounts;
use Spaceinvoices\Documents;
use Spaceinvoices\Payments;
use DateTime;
use DatePeriod;
use DateInterval;
//use App\Http\Traits\SpaceInvoicesOrgTrait; 
//use App\Http\Controllers\OrganizationController;


class SpaceInvoicesApolloController extends Controller
{
    
    //  use SpaceInvoicesOrgTrait;
    private $token;
    private $accountID;
    private $orgId;
    public function __construct()
    {
        $this->token     = config('services.SPACEINVOICES.SPACEINVOICES_ACCESSTOKEN');
        $this->accountID = config("services.SPACEINVOICES.SPACEINVOICES_ACCOUNT_ID");
        $this->orgId     = config("services.SPACEINVOICES.SPACEINVOICES_ORGID");
        Spaceinvoices::setAccessToken($this->token);
    }
    
    public function index(Request $request)
    {
        
        
        $return = Organizations::create($this->accountID, array(
            "name" => "International Space Agency 5",
            "address" => "Rocket Road 1",
            "city" => "Brussels",
            "zip" => "10000",
            "country" => "Belgium",
            "IBAN" => "123454321 123454321",
            "bank" => "Revolut Ltd"
        ));
        echo "<pre>";
        print_r($return);
        
    }
    public function organizationCreate($data)
    {
        if ($data) {
            
            $name = $data['user_fname'] . ' ' . $data['user_lname'];
            return $return = Organizations::create($this->accountID, array(
                "name" => $name,
                "address" => $data['user_address'],
                "email" => $data['email'],
                "country" => 'slovenia'
            ));
            //echo"<pre>";print_r($return);
        }
        
    }
    public function organizationCreateHost($data)
    {
        if ($data) {
          
            $name = $data['company_name'];
            $phone = $data['user_mobile_code'] . '' . $data['user_mobile'];
            return $return = Organizations::create($this->accountID, array(
                "name" => $name,
                "address" => $data['user_address'],
                "email" => $data['email'],
                "country" => 'slovenia',
                "phone" =>  $phone,
                "companyNumber " => $data['user_mobile'],
                "website " =>  $data['company_web_link']
            ));
            //echo"<pre>";print_r($return);
        }
        
    }
    
    public function updateOrganization($orgid=null,$data = array())
    {
       /* 
        $data = array(
            'name' => 'Scout Basic',
            'address' => 'Rocket street 15',
            'address2' => '',
            'city' => 'Brussels',
            'zip' => '10000',
            'state' => '',
            'country' => 'Belgium',
            'countryAlpha2Code' => 'BE',
            'taxNumber' => '10489185',
            'taxSubject' => true,
            'companyNumber' => '0987654321',
            'IBAN' => 'GB49 REVO 0099 6979 5754 56',
            'bank' => 'Revolut ltd',
            'SWIFT' => '',
            'website' => 'http://lfz.myfileshosting.com/',
            'phone' => '+32 466 903 942',
            'locale' => 'en',
            'timezone' => 'CEDT',
            'active' => true,
            'custom' => array()
        );*/
        
        $return = Organizations::updateOrg($orgid, $data);  
         return $this->spaceInvoiceApiReturn($return );
        
        
    }
    public function getOrganization($orgid)
    {
          $return = Organizations::getById($orgid);
         return $this->spaceInvoiceApiReturn( $return );
    }
    
    public function clientCreate(Request $request)
    {
        $token = config("services.SPACEINVOICES.SPACEINVOICES_ACCESSTOKEN");
        
        $return = Clients::create($this->orgId, array(
            "name" => "Space Exploration Technologies corp",
            "address" => "Rocket Road",
            "city" => "Hawthorne",
            "zip" => "CA 90250",
            "country" => "USA",
            "email" => "info@spacex.com",
            "contact" => "Elon M.",
            "phone" => "+1 123 456 78900"
        ));
        echo "<pre>";
        print_r($return);
        
    }
    public function getClient(Request $request, $id)
    {
        $getClientDetails = Clients::getById($id);
        echo "<pre>";
        print_r($getClientDetails);
        
    }
    public function updateClient(Request $request, $id)
    {
        $getClientDetails = Clients::edit($id, array(
            "name" => "Space Exploration Technologies corp",
            "address" => "Rocket Road",
            "city" => "Hawthorne",
            "zip" => "CA 90250",
            "country" => "USA",
            "email" => "info@spacex.com",
            "contact" => "Elon M.",
            "phone" => "+1 123 456 78910"
        ));
        echo "<pre>";
        print_r($getClientDetails);
        
    }
    public function deleteClient(Request $request, $id)
    {
        $getClientDetails = Clients::delete($id);
        echo "<pre>";
        print_r($getClientDetails);
        
    }
    public function clientList(Request $request)
    {
        
        $getClientDetails = Clients::find($this->orgId);
        echo "<pre>";
        print_r($getClientDetails);
        
    }
    public function createAccount($data, $type)
    {
        if ($data) {
            
            return $getClientDetails = Accounts::create(array(
                "email" => $data['email'],
                "password" => '123@abc',
                "firstname" => $data['user_fname'],
                "lastname" => $data['user_lname'],
                // "custom" => $type,
                "emailVerified" => true
            ));
            
        }
        
    }
    public function updateAccount($accountId,$data, $type)
    {
        if (!empty($data) && !empty($accountId)) {
            
            return $getClientDetails = Accounts::Edit($accountId,$data);
            
        }
        
    }

    public function getAccount($id)
    {
        $getAccountDetails = Accounts::getById($id);
        return $this->spaceInvoiceApiReturn($getAccountDetails);
        
    }
    public function inviteOrg2($orgid)
    {
        
        $accid = $this->accountID;
        return $getClientDetails = Organization::invite($orgID, array(
            "accountId" => $accid,
            "role" => "admin",
            "requireConfirmation" => false,
            "sendEmail" => false,
            "rejected" => false,
            "disabled" => false
        ));
        
        
    }
    public function inviteOrg($orgid = null, $accountId = null)
    {
        $accid = $this->accountID;
        return $getClientDetails = Organizations::invite($orgid, array(
            "accountId" => $accountId,
            "role" => "admin",
            "requireConfirmation" => false,
            "sendEmail" => false,
            "rejected" => false,
            "disabled" => false
        ));
        
        
    }
    public function uploadCertificate($orgid = null, $certificate_path = null,$passphrase=null)
    {

     $retrunData =  Organizations::uploadCertificate($orgid, $certificate_path,$passphrase);
       return $this->spaceInvoiceApiReturn($retrunData);
          
    }
    
    public function hasCertificate($orgid = null)
    {

     $retrunData =  Organizations::hasCertificate($orgid,$environment='test');
       return $retrunData;
          
    }
    
    public function addBusinessPremises($orgid = null,$data=null)
    {
     $data['environment']='test';
     $retrunData =  Organizations::addBusinessPremises($orgid,$data);
       return $this->spaceInvoiceApiReturn($retrunData);
          
    }
    public function getBusinessPremises($orgid = null)
    {
     $data['environment']='test';
     $retrunData =  Organizations::getBusinessPremises($orgid);
       return $this->spaceInvoiceApiReturn($retrunData);
          
    }
    public function addElectronicDevices($premissesId = null,$data=null)
    {
     $data['environment']='test';
     $retrunData =  Organizations::addElectronicDevices($premissesId,$data);
       return $this->spaceInvoiceApiReturn($retrunData);
          
    }
     public function invoicePayments($data=[]) {

     if(isset($data['admin_org']) && $data['admin_org']!=null)
     {
        $invoice_array=$this->createDocumentArray($this->orgId,$this->accountID,$data['booking_detail'],"traveler");
        $createDocument = Documents::create($this->orgId, $invoice_array);
     }elseif(isset($data['scout_org']) && $data['scout_org']!=null){

       $invoice_array=$this->createDocumentArray($data['scout_org'],$data['scout_acc'],$data['booking_detail'],"scout");
      
        $createDocument = Documents::create($data['scout_org'], $invoice_array);

     }elseif(isset($data['host_org']) && $data['host_org']!=null){
       $invoice_array=$this->createDocumentArray($data['host_org'],$data['host_acc'],$data['booking_detail'],"host");
        
        $createDocument = Documents::create($data['host_org'], $invoice_array);
     }
       $currentDateTime1= new DateTime();
        $currentDateTime=$currentDateTime1->format('Y-m-d');
       if(isset($createDocument->id)){

         $docId = $createDocument->id;
          $testPaymentData = array(
          "type" => "cash",
          "date" => $currentDateTime,
          "amount" => $data['booking_detail']->amount,
          "description"=>"Booking for ".$data['booking_detail']->experience->experience_name
        );
         $create = Payments::create($docId, $testPaymentData);
       
       }
       
  
    }
    public function createDocumentArray($orgId,$accId,$bookingDetail,$type){

        if($type=='traveler')
        {
            $name=$bookingDetail->traveler->user_fname.' '.$bookingDetail->traveler->user_lname;
            $unit="Person";
            $q=$bookingDetail->adults+$bookingDetail->children;
            $discount=  $bookingDetail->discount;
            $price= $bookingDetail->total_price/$q;
        }else{
          $orgData=$this->getOrganization($this->orgId);
           $unit="Booking";
           $q=1;
           $discount=0;
           $price= $bookingDetail->amount;
          if($orgData['isSucceeded'])
          {
            $orgName= $orgData['data']->name;
            $name=$orgName;
          }
          else{
            $name="LFZ";
          }
        }
         return $testDocumentData = array(
          "type" => "invoice",
          "dateService"=>$bookingDetail->booking_date,
          "dateServiceTo"=>$bookingDetail->booking_date,
          "currencyId"=>  "EUR",
          "accountId"=>  $accId,
          "organizationId"=>$orgId ,
          "_documentClient" => array(
            "name" => $name,
          ),
          "_documentItems" => [
            array(
              "name" => $bookingDetail->experience->experience_name,
              "quantity" => $q,
              "unit" => $unit,
              "price" => $price,
              "discount" => $discount,
              "discountIsAmount"=> true,
            )
          ],
           "totalPaid"=>$bookingDetail->total_price,
          "totalDue"=> 0,
          "paidInFull"=> true,
          "hasUnit"=> false,
        );

    }
     public function invoicePayments87($orgId = null,$data=[]) {
     echo "==================== PAYMENTS ====================<br>";

// we need new document, so we can add payment to it (we will delete it in the end)
$testDocumentData = array(
  "type" => "invoice",
  "currencyId"=>  "EUR",
  "accountId"=>  $this->accountID,
  "organizationId"=> $this->orgId,
  "_documentClient" => array(
    "name" => "Rocket Man",
    "country" => "USA"
  ),
  "_documentItems" => [
    array(
      "name" => "Experience name",
      "quantity" => 2,
      "unit" => "Person",
      "price" => 10000
    )
  ]
);
$testOrganizationId='6048981f393d703d8472696b';
$testOrganizationId='603f44af4bae820dee8bbf3f';
  $createDocument = Documents::create($testOrganizationId, $testDocumentData);
   $docId = $createDocument->id;
 echo "<pre> doc--"; print_r($createDocument);
 $testPaymentData = array(
  "type" => "bank",
  "date" => "2021-03-15",
  "amount" => 10000
);

$testPaymentDataEdit = array(
  "type" => "bank",
  "date" => "2021-03-15",
  "amount" => 2000,
  "documentId" => "604ef085d04503248f4b1846"
);
// $docId='604ef085d04503248f4b1846';
echo "================= Create =================\n";
$create = Payments::create($docId, $testPaymentData);

echo "<pre>"; print_r($create);
 echo $paymentId = $create->id;
var_dump($create);

echo "\n================= Get all =================\n";
$all = Payments::find($testOrganizationId);
var_dump($all);
/*
echo "\n================= Edit =================\n";
$edit = Payments::edit($paymentId, $testPaymentDataEdit);
var_dump($edit);

echo "\n================= Get all with filters =================\n";
$all = Payments::find($testOrganizationId, $testFilter);
var_dump($all);

echo "\n================= Get all payments for document =================\n";
$allDocument = Payments::findDocumentPayments($docId);
var_dump($allDocument);*/

//echo "\n================= Delete =================\n";
//$delete = Payments::delete($paymentId);
//var_dump($delete);

 //Documents::delete($docId); 
          
    }
    
    public function spaceInvoiceApiReturn($data)
    {
    $retrunData=['isSucceeded'=>FALSE,'data'=>'','msg'=>''] ; 
        if (!empty($data)) {
            if (isset($data->error)) {
                $retrunData['isSucceeded'] = FALSE;
                $retrunData['msg']    = $data->error->message;
                $retrunData['data']   = $data;
            } else {
                $retrunData['isSucceeded'] = TRUE;
                $retrunData['data']   = $data;
            }
            
        }
        return $retrunData;
    }
}