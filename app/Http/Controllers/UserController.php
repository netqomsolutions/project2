<?php
namespace App\Http\Controllers;
use DB;
use Illuminate\Http\Request;
use App\User;
use App\CountryCode;
use App\UserMeta;
use Stripe;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Image;
use Intervention\Image\Exception\NotReadableException;
use DateTime;
use App\Http\Controllers\SpaceInvoicesApolloController;
use App\OrganizationController;
use App\Http\Traits\CommonTrait;
class UserController extends Controller
{
   use CommonTrait;

	 /*
    * show Profile Function
    */
     public function myProfile(Request $request){
       $page_title = 'Manage Personal Information';
       $page_description = 'Some description for the page'; 
       $countries =  DB::table('country_codes')->select("iso","phonecode")->get();

       $languages =  DB::table('languages')->get(); 
       if (Auth::check()) {
        $scoutData = User::where(['id' => Auth::user()->id])->first(); 
        $getCountryCode = CountryCode::get();
        if(Auth::user()->user_role==2){
            return view('traveler.profile',compact('scoutData','page_title','page_description','languages','countries','getCountryCode'));
        }
        else if(Auth::user()->user_role==3)
        {
           return view('scout.profile',compact('scoutData','page_title','page_description','languages','countries','getCountryCode'));
        }
        else if(Auth::user()->user_role==4)
        {
           return view('host.profile',compact('scoutData','page_title','page_description','languages','countries','getCountryCode'));
        }
        else if(Auth::user()->user_role==1)
        {
           return view('admin.profile',compact('scoutData','page_title','page_description','languages','countries','getCountryCode'));
        }
       else{
        return view('profile',compact('scoutData','page_title','page_description','languages','countries','getCountryCode'));
    }
}
else{
    return redirect()->route('login');
}
}

public function accountInformation()
{
   $page_title = 'Manage Account Information';
   $page_description = 'Some description for the page';
   if (Auth::check()) {
    $accountDetail=[];
    $scoutData = User::where(['id' => Auth::user()->id])->first(); 
    if(!empty($scoutData->user_stripe_account_id) && $scoutData->user_stripe_account_id!=='NULL')
    {
      try {
        $stripe = new \Stripe\StripeClient(config('services.stripe.secret'));
         $accountDetail= $stripe->accounts->retrieve(
          $scoutData->user_stripe_account_id,
        );
        
      }
      catch (\Exception $e) {
        return redirect()->back()->with('failure',$e->getMessage());
      }
    }
    if(Auth::user()->user_role==2)
    {
        return view('traveler.account-information',compact('scoutData','page_title','page_description','accountDetail'));
    }
    if(Auth::user()->user_role==3 || Auth::user()->user_role==4)
    {
        return view('account-information',compact('scoutData','page_title','page_description','accountDetail'));
    }
}
else{
 return redirect()->route('account-information');
}
}

public function addAccountInformation()
{
   $page_title = 'Add Account Information';
   $page_description = 'Some description for the page';
   if (Auth::check()) {
    if(Auth::user()->user_role==3 || Auth::user()->user_role==4)
    {
        $scoutData = User::with(['user_bank_detail'])->where(['id' => Auth::user()->id])->first(); 
        return view('add-account-information',compact('scoutData','page_title','page_description'));
    }
}
else{
 return redirect()->route('login');
}
}
public function accountCreateUpdate(Request $request)
{
    if (Auth::check()) {
      try {
     $stripe = new \Stripe\StripeClient(config('services.stripe.secret'));
      $folderPath="users/documents";
      $front_file_name =$this->imageUpload($request,$folderPath,'identity_document_front');
      $back_file_name =$this->imageUpload($request,$folderPath,'identity_document_back');
      $front_additional_document =$this->imageUpload($request,$folderPath,'additional_document_front'); 
      $back_additional_document =$this->imageUpload($request,$folderPath,'additional_document_back');
     $front_identity_doc=$stripe->files->create([
      'purpose' => 'identity_document',
      'file' => fopen(public_path($folderPath.'/'.$front_file_name), 'r'),
     ]);
     $back_identity_doc=$stripe->files->create([
      'purpose' => 'identity_document',
      'file' => fopen(public_path($folderPath.'/'.$back_file_name), 'r'),
     ]); 
     $additional_document_front=$stripe->files->create([
      'purpose' => 'additional_verification',
      'file' => fopen(public_path($folderPath.'/'.$front_additional_document), 'r'),
     ]);
     $additional_document_back=$stripe->files->create([
      'purpose' => 'additional_verification',
      'file' => fopen(public_path($folderPath.'/'.$back_additional_document), 'r'),
     ]);
     $bday=DateTime::createFromFormat('Y-m-d', $request->dob)->format('d');
     $bmonth=DateTime::createFromFormat('Y-m-d', $request->dob)->format('m');
     $byear=DateTime::createFromFormat('Y-m-d', $request->dob)->format('Y');
     $accountDetail=$stripe->accounts->create([
        'type' => 'custom',
        'country' => $request->country,
        'email' => $request->email,
        'business_type' => 'individual',
        'business_profile'=>[
          'url'=>url('/'),
          'mcc'=>'4722',
        ],
        'individual'=>[
          'email'=>$request->email,
          'first_name'=>$request->user_fname,
          'last_name'=>$request->user_lname,
          'phone'=>$request->phone,
          'dob'=>[
            'day'=>   $bday,
            'month'=> $bmonth,
            'year'=>  $byear,
          ],
          'address'=>[
            'city'=>$request->city,
            'line1'=>$request->address_line1,
            'line2'=>$request->address_line2,
            'postal_code'=>$request->postal_code
          ],
          'verification'=>[
            'document'=>[
              'back'=>$back_identity_doc->id,
              'front'=>$front_identity_doc->id
            ],
            'additional_document'=>[
              'front'=>$additional_document_front->id,
              'back'=>$additional_document_back->id
            ],
          ],
        ],
        'tos_acceptance'=>[
            'date'=>time(),
            'ip'=>$request->ip(),
          ],
        'capabilities' => [
            'card_payments' => ['requested' => true],
            'transfers' => ['requested' => true],
        ],
        'external_account'=>[
          'object'=>'bank_account',
          'country'=>$request->country,
          'currency'=>'EUR',
          'account_holder_name'=>$request->user_fname.' '.$request->user_lname,
          'account_number'=>$request->account_number,

        ],
    ]);
     
     $user= User::find($request->u_id);
     $user->user_stripe_account_id=$accountDetail->id;
     $user->save();
     if($request->filled('is_host'))
     {
       return redirect()->route('scout-host-account-information',['id'=>base64_encode($request->u_id)])->with('success',"Bank account successfully added");
     }
     else{
      return redirect()->route('account-information')->with('success',"Bank account successfully added");
     }
     return redirect()->route($redirect)->with('success',"Bank account successfully added");
   
   }
   catch (\Exception $e) {
      return redirect()->back()->with('failure',$e->getMessage());
    }
 }else{
    return redirect()->route('login');
}
}

public function accountDelete(Request $request)
{
  try {
       $user= User::find($request->user_id);
        $stripe = new \Stripe\StripeClient(config('services.stripe.secret'));
         $accountDetail= $stripe->accounts->delete(
          $user->user_stripe_account_id,
        );
     
     $user->user_stripe_account_id=NULL;
     $user->save();
      return response()->json([
         "message"       => 'Account successfully deleted',
         "isSucceeded"   => TRUE,
     ]);
      }
      catch (\Exception $e) {
       return response()->json([
                "message"       => $e->getMessage(),
                "isSucceeded"   => FALSE,
            ]);
      }

}

    //Edit profile function for all
public function editProfile(){

  if (Auth::check()) {

   $userData = User::find(Auth::user()->id);

   $countries =  DB::table('country_codes')->select("iso","phonecode")->get();

   $languages =  DB::table('languages')->select("name","short")->get(); 

            //scout edit profile page
   if(Auth::user()->user_role==3){
    return view('scout.edit-profile',compact('userData','countries','languages'));
}

            //host edit profile page
elseif (Auth::user()->user_role==4) {
    return view('scout.add_host',compact('userData','countries','languages'));
}
else{
    return view('edit-profile',compact('userData'));
}
}
else{
  return redirect()->route('login');
}
}

public function updateProfile(Request $request) {
    if(Auth::check()){
        $userId = Auth::user()->id; 
         $userExperienceLanguages= "";
         $getUserExpLanguage=[];
      if(Auth::user()->user_role==3)
      {
        $user = User::with('experiences')->find($userId);
         $userExperienceLanguages = $user->experiences;
         $userExperience_data['type'] =1 ;
      }
      else if(Auth::user()->user_role==4)
      {
        $user = User::with('hostexperiences')->find($userId);
         $userExperienceLanguages = $user->hostexperiences;
         $userExperience_data['type'] =2 ;

      }
      else{
         $user = User::find($userId);
      }
       
       
        $CheckUserLang = explode(",",$user->user_other_languages);
        if(!empty($user->user_primary_language))
          {
                 in_array($user->user_primary_language, $CheckUserLang) ? '':array_push($CheckUserLang, $user->user_primary_language); 
                
          }
        $data = $request->except(['_token','email','admin_comission']);

        if(Auth::user()->user_role==1){
            $folderPath="users/admin";
        }

        elseif (Auth::user()->user_role==2) {
            $folderPath="users/travellers";
        }

        elseif(Auth::user()->user_role==3){
            $folderPath="users/scout";
        }

        if($request->hasFile('user_image')) {
         if ((file_exists(public_path('/users/'. $user->id .'/'. $user->image)) && $user->user_image)) {
    	           // unlink(public_path('/users/'. $user->id .'/'. $user->user_image));
         }
         $data['user_image'] = $this->imageUpload($request,$folderPath,'user_image');
          $thumbnailpic = "/profile-thumb-".$data['user_image'];
            createThumbnail($folderPath,$data['user_image'],$thumbnailpic,$width=160,$height=160); 
         $data['user_image_path'] = $folderPath;
     }
         
     if($request->filled('user_other_languages')){
        $getUserExpLanguage = $data['user_other_languages'];
        $data['user_other_languages'] = implode(',',$request->user_other_languages);
    }
    if($request->filled('admin_comission')){
      $comData= ['admin_comission'=>$request->admin_comission];
      $result = saveMetaData($comData);
      
    }
  
    $user->update($data);
     
    $user_meta = UserMeta::where('user_id',$userId)->first();
         
    if(!empty($user_meta)){
      if($request->filled('legal_requirements')){
        $data['legal_requirements'] = implode(',',$request->legal_requirements);
      }
      else{
        $data['legal_requirements'] =null;
      }
      if($request->filled('account_type')){

        $data['account_type'] = $request->account_type;
        if($request->account_type==0)
        {
           $data2['scout']  = User::where(['id' => $userId])->first();
            $ApolloData = $this->apolloCreate($data2['scout']); // Create the User Apollo Org And Account

            if($ApolloData['status']){
               $data['apollo_org_id']= $ApolloData['apollo_org_id'];
               $data['apollo_acc_id']= $ApolloData['apollo_acc_id'];
              
            }
        }

      }
     
        $user_meta->update($data);
     
    }
   /* date:30-01-2021 @RT  add language in experience When user update it language*/  
    if($request->filled('user_other_languages')   || $request->filled('user_primary_language')  ){
          $userExperience_data['created_at'] = date('Y-m-d H:i:s');
          $userExperience_data['updated_at'] = date('Y-m-d H:i:s');
          if(!empty($data['user_primary_language'])){   //  if the user_primary_language not in other language array then pust to array 
               in_array($data['user_primary_language'], $getUserExpLanguage) ? '' : array_push($getUserExpLanguage, $data['user_primary_language']); 
            }
          if(!empty($userExperienceLanguages)){
                 $newLanguage=  array_merge($CheckUserLang, $getUserExpLanguage);
                 $i=0;
               if(array_unique($newLanguage)){   // Check the Diffrence between new value and old value
                     $UserLangDataArray=array();
                     foreach($userExperienceLanguages as $userExpLang)  {
                           $i++;
                             
                              $userExperience_data['user_id'] = $userId;
                              $userExperience_data['experience_id'] =$userExpLang->id;
                              if(isset($getUserExpLanguage)){
                            
                                  foreach($getUserExpLanguage as $lang)
                                    {
                                      $language_data[$i] =  $userExperience_data; 
                                      $language_data[$i]['language_id'] = $lang; 
                                       $i++;
                                    } 
                               } 
                     }
                     if(!empty($language_data)){
                          DB::table('experience_language_relations')->where('user_id', '=', $userId)->delete();
                          $experienceLanguages = DB::table('experience_language_relations')->insert($language_data);

                   }
              } 
          }
   }

    /*********************END 30-01-2021 @RT **********************/

    if($user){
        return redirect()->back()->with('success','Profile Updated Successfully.');
    }
    else{
     return redirect()->back()->with('failure','Opps,Something went wrong,please try again.');
 }
}
else{
   return redirect()->route('login');
}
}

public function checkMobileExistence(Request $request)
{
   if($request->isMethod('post')){  
      $phone= Auth::user()->user_mobile;
      $phoneCheck = User::where(['user_mobile' => $request->user_mobile])->first();
      if ($phoneCheck && $phone != $request->user_mobile ) { 
       echo 'false';  
   } else { echo 'true'; }
}
}

public function changePasswordForm(Request $request){
   $page_title = 'Change Password';
   $page_description = 'Some description for the page';
   if (Auth::user()) {
      $scoutData = User::where(['id' => Auth::user()->id])->first(); 
      return view('change-password',compact('scoutData','page_title', 'page_description'));

  }
  else{
   return redirect()->route('login');
}
}

public function changePassword(Request $request){
    $user = Auth::user();
    $user->password = bcrypt($request->password);
    $user->save();
    return redirect()->back()->with("success","Password changed successfully !");
}

     /*
    * Check User old Password
    */
     public function checkOldPassword(Request $request){
         if($request->isMethod('post')){  
           if (!(Hash::check($request->current_password, Auth::user()->password))) {
              echo 'false'; 
          }
          else { echo 'true'; }
      }     
  } 
     /*
    *Upload Image function
    */
     public function imageUpload($request,$folderPath,$imageName){
        $fName = '';
        if($request->hasFile($imageName)){

            $originName = $request->file($imageName)->getClientOriginalName();
            $fileName = pathinfo($originName, PATHINFO_FILENAME);
            $extension = $request->file($imageName)->getClientOriginalExtension();
            $fileName = $fileName.'_'.uniqid().'.'.$extension;  
            $thumbnailpic= 'thumb'.'-'.$fileName;      
            if($request->file($imageName)->move(public_path($folderPath), $fileName)){
                
                 $fName = $fileName;
            }
            return $fName;
        }
        return $fName;       
    }


    public function socialActivity(Request $request){
        if(!Auth::check()){
            return redirect()->route('login'); die; 
        }
             $user = Auth::user();
             $scoutData = User::where(['id' => Auth::user()->id])->first(); 
             $user_meta = UserMeta::where('user_id',$user->id)->first();
             $social_link = $user_meta->social_links;  
             if($request->isMethod('post')){  
                $post = $request->except(['_token']); 
                $data['social_links'] = json_encode($post); 
            if(!empty($user_meta) && $user_meta->update($data)){return redirect()->back()->with('success','Social Links Updated Successfully.');}else{ return redirect()->back()->with('failure','Opps,Something went wrong,please try again.');}  
          }else{return view('scout.social_activity',compact('scoutData','social_link','user_meta'));}


}

public function fiscalization(Request $request){
        if(!Auth::check()){
            return redirect()->route('login'); die; 
        }
        if(Auth::user()->user_role==3 || Auth::user()->user_role==4)
        {


         $user = Auth::user();
         $scoutData = User::where(['id' => Auth::user()->id])->first(); 
         $user_meta = UserMeta::where('user_id',$user->id)->first();  

        $response_array = array();
        $response_array['premises'] = array();
        $response_array['devices'] = array();
        $response_array['org'] = array();
        $response_array['hasCertificate'] = '';
        if(isset($user_meta->apollo_org_id)){
         $spaceInvoicesApolloController = new SpaceInvoicesApolloController ; 
         $orgData= $spaceInvoicesApolloController->getOrganization($user_meta->apollo_org_id);  
         //$AccData= $spaceInvoicesApolloController->getAccount($user_meta->apollo_acc_id);  
         $BusinessPremisesData= $spaceInvoicesApolloController->getBusinessPremises($user_meta->apollo_org_id);
         $hasCertificate= $spaceInvoicesApolloController->hasCertificate($user_meta->apollo_org_id);  
        $response_array['org']= ($orgData['isSucceeded'] ? $orgData['data']: null );
      //  $response_array['acc']= ($AccData['isSucceeded'] ? $AccData['data']: null );
        $response_array['hasCertificate']= (isset($hasCertificate->exists) ?$hasCertificate->exists:'' );
        $businessPremises= ($BusinessPremisesData['isSucceeded'] ?$BusinessPremisesData['data']:false);
        if($businessPremises){
         foreach ($businessPremises as $premise) {
         
        $response_array['premises'][$premise->id] = $premise->businessPremiseId;
        $devices = array();

        foreach ($premise->electronicDevices as $device) {
          $devices[$device->id] = $device->electronicDeviceId;
        }
        if(!empty($devices))
        {
        $response_array['devices'][$premise->businessPremiseId] = $devices;
      }
      
     } 
    }
    //echo "<pre>";print_r($response_array);
   // die();
    }
 
        return view('scout.fiscalization',compact('scoutData','user_meta','response_array'));
      }else{
         return redirect()->back()->with("failure","Unable to access.");
      }
    


}

  public function fiscalizationRequest(Request $request){
   
          if(!Auth::check()){
              return redirect()->route('login'); die; 
          } 
           if($request->isMethod('post')){  
              $data = $request->except(['_token','id']);  

              /*print_r($data);
              die;*/
               $Orgdata = array(
              'name' => $data['name'],
              'address' => $data['address'], 
              'city' =>  $data['city'],
              'zip' => $data['zip'],   
              'taxNumber' => $data['taxNumber'],
              'taxSubject' => true,     
              'custom' => array()
          );
          $ApolloController = new SpaceInvoicesApolloController ; 
          $updateOrg= $ApolloController->updateOrganization($request->id,$Orgdata);
          //$updateAcc= $ApolloController->updateAccount($request->id,$Orgdata);
          return json_encode($updateOrg); 
            
         }  
       
  }
  public function uploadFursCertificate(Request $request){
   
          if(!Auth::check()){
              return redirect()->route('login'); die; 
          } 

           $data['certificate']="";
           if($request->isMethod('post')){  
              $data = $request->except(['_token','id']);  
              $folderPath="users/documents";
               if($request->hasFile('certificate')) {
                  $data['certificate'] = $request->file('certificate')  ;  
          }
          $scoutData = User::where(['id' => Auth::user()->id])->first(); 
         $ApolloController = new SpaceInvoicesApolloController ; 
        $orgData= $ApolloController->getOrganization($scoutData->user_meta->apollo_org_id); 
       if(!empty($orgData))
       {
          $updateOrg= $ApolloController->uploadCertificate($request->id, $data['certificate'],$request->passphrase); 
          return json_encode($updateOrg); 
        }
        else{
           return response()->json([
                "msg"       => "Please first fill Organization detail.",
                "isSucceeded"   => FALSE,
            ]);
        }
            
         }  
       
  }
  public function addBusinessPremises(Request $request){
   
          if(!Auth::check()){
              return redirect()->route('login'); die; 
          } 
           if($request->isMethod('post')){  
              $data = $request->except(['_token','id']);  
       $scoutData = User::where(['id' => Auth::user()->id])->first(); 
      $ApolloController = new SpaceInvoicesApolloController ; 
      $hasCertificate= $ApolloController->hasCertificate($scoutData->user_meta->apollo_org_id); 
      if($hasCertificate->exists)
      {
          $updateOrg= $ApolloController->addBusinessPremises($request->id, $data); 
          $premises= $ApolloController->getBusinessPremises($request->id);
          $premisesData= ($premises['isSucceeded'] ? $premises['data']: null ); 
          $premisesOption='';
          //echo"<pre>";
          //print_r($premisesData);

          if($updateOrg['isSucceeded']){
          $premisesOption .= ' <option value="">Choose</option>';
          if($premisesData!=null){
           foreach($premisesData as $premises ){
               $premisesOption .= '<option value="'.$premises->id.'">'.$premises->businessPremiseId.'</option>';
           }
          }
          $updateOrg['premises']=$premisesOption;
        }
          return json_encode($updateOrg);
      }else{
        return response()->json([
                "msg"       => "Please first upload certificate.",
                "isSucceeded"   => FALSE,
            ]);
      }
           
            
         }  
       
  }
  public function addElectronicDevices(Request $request){
   
          if(!Auth::check()){
              return redirect()->route('login'); die; 
          } 
           if($request->isMethod('post')){  
              $data = $request->except(['_token','id','businessPremiseId']); 
                $scoutData = User::where(['id' => Auth::user()->id])->first();  
      $ApolloController = new SpaceInvoicesApolloController ; 
       $BusinessPremisesData= $ApolloController->getBusinessPremises($scoutData->user_meta->apollo_org_id);
       if($BusinessPremisesData['isSucceeded'])
       {
          $updateOrg= $ApolloController->addElectronicDevices($request->businessPremiseId, $data); 
          return json_encode($updateOrg); 
        }else{
          return response()->json([
                "msg"       => "Please first add Business Premises.",
                "isSucceeded"   => FALSE,
            ]);
        }
            
         }  
       
  }

}