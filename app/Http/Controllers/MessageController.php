<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;   
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\URL;
use App\Experience;
use App\Message;
use DB;
use App\User;
use App\Quotation;
use App\Mail\SendMail;
class MessageController extends Controller
{
    
    public function index(Request $request)
    {
        $user = Auth::user(); 
        $login_id = $user->id;  
        $recid  = Message::where(['sender_id' => $login_id])->select('receiver_id as rc_id')->distinct('rc_id')
        ->get()
        ->toArray();
        if(empty($recid)){
            $recid  = Message::where(['receiver_id' => $login_id])->select('sender_id as rc_id')->distinct('rc_id')
            ->get()
            ->toArray(); 
        } 
        $userList = array();
        $countMessages = array();
        foreach ($recid as $key => $id) { 
            $userList[] = DB::table('users')->where(['id' => $id['rc_id']])->get();
            $countMessages[] = DB::table('messages')->where(['sender_id' => $login_id])
            ->where(['receiver_id' => $id['rc_id']])->where(['status' =>'0'])->select('id')->get()->count();
             

        } 

        $allMessageCount = array_sum($countMessages); 

        // echo "<pre>"; print_r($allMessageCount); die;  

        return view('message.index',compact('userList','user','countMessages','allMessageCount'));
    }


    public function save_messages(Request $request){


        if($request->isMethod('post')){
            $data = $request->except(['_token']); 
            $msg =Message::create($data);
             $mail = array(); 
            $mail['scout']=User::where(['id'=>$data['receiver_id']])->first();
            if($mail['scout']['user_role']==3)
            {
             $mail['subject']= "New Message";
             $sendEmail = Mail::to([$data['useremail']])->send( new SendMail($mail,'new_message_notification'));
            }
            if($data['isonline'] == 0){
                if($mail['scout']['user_role']==2)
                {
                 $mail['subject']= "New Message";
                 $sendEmail = Mail::to([$data['useremail']])->send( new SendMail($mail,'new_message_notification_trav'));
                }
                if(Auth::user()->user_role==2)
                {
                    $scoutLast =Message::where(['sender_id'=>$data['receiver_id'],"receiver_id"=>$data['sender_id']])->orderBy('id','DESC')->first();
                    if($scoutLast->message!='Thank you very much for your inquiry. I will answer you in the shortest possible time.')
                    {
                    $newData=[
                        "message"=>"Thank you very much for your inquiry. I will answer you in the shortest possible time.",
                        "sender_id"=>$data['receiver_id'],
                        "receiver_id"=>$data['sender_id'],
                        "status"=>1

                    ];
                     $msg =Message::create($newData);
                 }
                }

            }
            if($mail['scout']['user_role']==3)
                { 
                    saveNotification('traveler_message',array('scout' =>$data['receiver_id']),$msg->id); 
                }
                if($mail['scout']['user_role']==2)
                { 
                    saveNotification('scout_message',array('traveler' =>$data['receiver_id']),$msg->id);
                }
            
            return response()->json([ 
                "isSucceeded"   => 1,
            ]);
        }
    } 
    public function update_message_status(Request $request){
        if($request->isMethod('post')){ 
            $update = Message::where(['receiver_id' => $request->sender_id,'sender_id' => $request->reciver_id])
               ->update(['status' => '1']);
            $return  = 0;
            if($update){
                $return  = 1;
            }  
            return response()->json([ 
                "isSucceeded"   => $return,
            ]);
        }
    }

    public function user_data_msg_old(Request $request)
    {
        $user = Auth::user(); 
        if($request->isMethod('post')){
                // $post = $request->except(['_token']); 
            $reciver_id =$request->reciver_id;


            $incoming_mgs = Message::join('users', 'messages.sender_id', '=', 'users.id')
                    ->where(["messages.sender_id" => $reciver_id])
                    ->select('messages.*','messages.id  as mid','users.*','users.id  as uid')
                    ->get();
                    // echo "<pre>"; print_r($incoming_mgs ); die;
            $outgoing_msg = Message::join('users', 'messages.receiver_id', '=', 'users.id')
                    ->where(["messages.receiver_id" => $reciver_id])
                    ->select('messages.*','messages.id  as mid','users.*','users.id  as uid')
                    ->get(); 
            $sort_data = DB::table('messages')->get();
            $idSortArr = array();
            $i=0;
            foreach ($sort_data as $key => $value) {
                $idSortArr+= array($i => $value->id);
                $i++;
            }
            foreach ($idSortArr as $l => $sort) {
                foreach ($incoming_mgs as $k => $chat_data) {

                    $date_arr = explode(' ', $chat_data->created_at);
                    $cstm_date = date("F-d", strtotime($date_arr[0]));
                    $date_kd =  str_replace('-',' ',$cstm_date);
                    $cstm_time = date("g:i A", strtotime($date_arr[1]));

                    if($idSortArr[$l]==$chat_data->mid) { 

                       $userImgIn = "https://i.stack.imgur.com/l60Hf.png"; 
                    if(!empty($chat_data->user_image_path ) && !empty($chat_data->user_image ) ){
                       $userImgIn = URL::to('/').'/public/'.$chat_data->user_image_path.'/'.$chat_data->user_image; 
                    } 

                        $incoming_mgs_html='<div class="d-flex flex-column mb-5 align-items-start msg-inner incoming">
                        <div class="d-flex align-items-center">
                        <div class="symbol symbol-circle symbol-40 mr-3">
                        <img class="msg-inner-img" alt="Pic" src="'.$userImgIn.'" />
                        </div>
                        <div>
                        <a href="#" class="text-dark-75 text-hover-primary font-weight-bold font-size-h6 msg-text">'.$chat_data->user_fname.' '.$chat_data->user_lname.'</a>
                        <span class="text-muted font-size-sm">'.$cstm_time.'  |  '.$date_kd.'</span>
                        </div>
                        </div>
                        <div class="mt-2 rounded p-5 bg-light-success text-dark-50 font-weight-bold font-size-lg text-left max-w-400px">'.$chat_data->message.'</div>
                        </div>'; 
                        echo $incoming_mgs_html; 
                    }
                }

                foreach ($outgoing_msg as $k => $chat_data) { 
                   
                    $date_arr = explode(' ', $chat_data->created_at);
                    $cstm_date = date("F-d", strtotime($date_arr[0]));
                    $date_kd =  str_replace('-',' ',$cstm_date);
                    $cstm_time = date("g:i A", strtotime($date_arr[1]));
                    if($idSortArr[$l]==$chat_data->mid) {
                         $userImgOut = "https://i.stack.imgur.com/l60Hf.png"; 
                         if(!empty(Auth::user()->user_image_path) && !empty(Auth::user()->user_image)){
                           $userImgOut = URL::to('/').'/public/'.Auth::user()->user_image_path.'/'.Auth::user()->user_image;
                      }

                        $outgoing_msg_html='<div class="d-flex flex-column mb-5 align-items-end msg-inner outgoing">
                        <div class="d-flex align-items-center">
                        <div>
                        <span class="text-muted font-size-sm">'.$cstm_time.'  |  '.$date_kd.'</span>
                        <a href="#" class="text-dark-75 text-hover-primary font-weight-bold font-size-h6">You</a>
                        </div>
                        <div class="symbol symbol-circle symbol-40 ml-3">
                        <img class="msg-inner-img" alt="Pic" src="'.$userImgOut.'"  />
                        </div>
                        </div>
                        <div class="mt-2 rounded p-5 bg-light-primary text-dark-50 font-weight-bold font-size-lg msg-text text-right max-w-400px ">'.$chat_data->message.'</div>
                        </div>'; 
                        echo $outgoing_msg_html;
                    }

                }
            }
        }
    }

    public function user_data_msg(Request $request)
    {
        $user = Auth::user(); 
        if($request->isMethod('post')){
                // $post = $request->except(['_token']); 
            $reciver_id =$request->reciver_id;
            $oldCount =$request->oldCount; 
            $all_msg_html='';
            $incoming_mgs = Message::join('users', 'messages.sender_id', '=', 'users.id')
                    ->where(["messages.sender_id" => $reciver_id])
                    ->select('messages.*','messages.id  as mid','messages.created_at  as createdat','users.*','users.id  as uid')
                    ->get();
                    // echo "<pre>"; print_r($incoming_mgs ); die;
            $outgoing_msg = Message::join('users', 'messages.receiver_id', '=', 'users.id')
                    ->where(["messages.receiver_id" => $reciver_id])
                    ->select('messages.*','messages.id  as mid','messages.created_at  as createdat','users.*','users.id  as uid')
                    ->get(); 
               
            $incoming_mgs_count = count(collect($incoming_mgs));
            $outgoing_msg_count = count(collect($outgoing_msg));
            $newCount= $incoming_mgs_count + $outgoing_msg_count;
            $sort_data = DB::table('messages')->get();
            $idSortArr = array();
            $i=0;
            foreach ($sort_data as $key => $value) {
                $idSortArr+= array($i => $value->id);
                $i++;
            }
            foreach ($idSortArr as $l => $sort) {
                foreach ($incoming_mgs as $k => $chat_data) {
                    $date_arr='';
                    $date_arr = explode(' ', $chat_data->createdat);
                    $cstm_date = date("F-d", strtotime($date_arr[0]));
                    $date_kd =  str_replace('-',' ',$cstm_date);
                    $cstm_time = date("g:i A", strtotime($date_arr[1]));

                    if($idSortArr[$l]==$chat_data->mid) { 

                       $userImgIn = "https://i.stack.imgur.com/l60Hf.png"; 
                    if(!empty($chat_data->user_image_path ) && !empty($chat_data->user_image ) ){
                       $userImgIn = URL::to('/').'/public/'.$chat_data->user_image_path.'/'.$chat_data->user_image; 
                    } 

                        $incoming_mgs_html='<div class="d-flex flex-column mb-5 align-items-start msg-inner incoming">
                        <div class="d-flex align-items-center">
                        <div class="symbol symbol-circle symbol-40 mr-3">
                        <img class="msg-inner-img" alt="Pic" src="'.$userImgIn.'" />
                        </div>
                        <div>
                        <a href="#" class="text-dark-75 text-hover-primary font-weight-bold font-size-h6 msg-text">'.$chat_data->user_fname.' '.$chat_data->user_lname.'</a>
                        <span class="text-muted font-size-sm">'.$cstm_time.'  |  '.$date_kd.'</span>
                        </div>
                        </div>
                        <div class="mt-2 rounded p-5 bg-light-success text-dark-50 font-weight-bold font-size-lg text-left max-w-400px">'.$chat_data->message.'</div>
                        </div>'; 
                       $all_msg_html  .= $incoming_mgs_html; 
                       // echo $incoming_mgs_html; 
                    }
                }

                foreach ($outgoing_msg as $k => $chat_data) { 
                      $date_arr='';
                    $date_arr = explode(' ', $chat_data->createdat);
                    $cstm_date = date("F-d", strtotime($date_arr[0]));
                    $date_kd =  str_replace('-',' ',$cstm_date);
                    $cstm_time = date("g:i A", strtotime($date_arr[1]));
                    if($idSortArr[$l]==$chat_data->mid) {
                         $userImgOut = "https://i.stack.imgur.com/l60Hf.png"; 
                         if(!empty(Auth::user()->user_image_path) && !empty(Auth::user()->user_image)){
                           $userImgOut = URL::to('/').'/public/'.Auth::user()->user_image_path.'/'.Auth::user()->user_image;
                      }

                        $outgoing_msg_html='<div class="d-flex flex-column mb-5 align-items-end msg-inner outgoing">
                        <div class="d-flex align-items-center">
                        <div>
                        <span class="text-muted font-size-sm">'.$cstm_time.'  |  '.$date_kd.'</span>
                        <a href="#" class="text-dark-75 text-hover-primary font-weight-bold font-size-h6">You</a>
                        </div>
                        <div class="symbol symbol-circle symbol-40 ml-3">
                        <img class="msg-inner-img" alt="Pic" src="'.$userImgOut.'"  />
                        </div>
                        </div>
                        <div class="mt-2 rounded p-5 bg-light-primary text-dark-50 font-weight-bold font-size-lg msg-text text-right max-w-400px ">'.$chat_data->message.'</div>
                        </div>'; 
                        $all_msg_html .=   $outgoing_msg_html;
                       // echo $outgoing_msg_html;
                    }

                }
            }
           // echo  $all_msg_html;
              return response()->json([ 
                "isSucceeded"   =>true,
                "html"   => $all_msg_html,
                "newCount"   => $newCount,
                "oldCount"   => $oldCount,
            ]); 
        }
    }

}
?>