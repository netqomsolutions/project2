<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;   
use Illuminate\Support\Facades\Mail;
use App\Experience;
use App\Message;

class AvailabilityController extends Controller
{
 	public function index(Request $request)
    {
    	return view('scout.availability_calender');
    }
}