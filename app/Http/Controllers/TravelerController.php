<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Hash;
use App\ContentManagementSystem;
use App\AboutUs;
use App\HomePage; 
use App\User;
use App\ExperienceCategory;
use App\ExperienceFeature;
use App\Experience;
use App\Country;
use App\State;
use App\City;
use Stripe;
use App\ExperienceBooking;
use App\Wishlist;
use App\ReviewRating;
use DataTables;
use App\Message;
use DB;
use App\Quotation;
use Illuminate\Support\Facades\Auth; 
use Session;
use App\Mail\SendMail;
use PDF;
class TravelerController extends Controller
{
    /**
    * Traveler Dashboard Controll Function
    */
    public function dashboard(Request $request ,$scoutId=null)
    {
        $page_title = 'Traveler Dashboard';
        $page_description = 'Some description for the page';

        $user = Auth::user(); 
        $login_id = $user->id;  
        $recid  = Message::where(['sender_id' => $login_id])->select('receiver_id as rc_id')->distinct('rc_id')
        ->get()
        ->toArray();
        if(empty($recid)){
            $recid  = Message::where(['receiver_id' => $login_id])->select('sender_id as rc_id')->distinct('rc_id')
            ->get()
            ->toArray(); 
        } 
        $dusers=array_column($recid, 'rc_id');
        $userList = array();
        $countMessages = array();
        if(empty($userList) && $scoutId && !in_array($scoutId, $dusers)){
            $tUser =  DB::table('users')->where(['id' => $scoutId])->get();
            $userList[] = $tUser;
        } 
        foreach ($recid as $key => $id) {  
            $userList[] = getCountMessages($id['rc_id'],$login_id);
        } 
        $total_wishlist = Wishlist::where('user_id',$login_id)->count();

        $total_booked_exp = ExperienceBooking::where('user_id',$login_id)->count();

        // 0 is for fetching experience reviews
        $total_reviews = ReviewRating::where('user_id',$login_id)->count();

         // echo "<pre>"; print_r($userList);die;   

        return view('traveler.dashboard', compact('page_title', 'page_description','userList','user','total_wishlist','total_booked_exp','total_reviews','scoutId'));
    }

    /*
    * Profile Function
    */
    public function myProfile(Request $request){
        $scoutData = User::where(['id' => Auth::user()->id ])->first();  
        return view('profile',compact('scoutData'));
    }


    public function ManageWishlist(Request $request)
    {
        $user = Auth::user();
        $Wishlistdata = Wishlist::join('experiences', 'experiences.id', '=', 'wishlists.experience_id')
            ->where(['wishlists.user_id' => $user->id])
            ->select('experiences.*','experiences.id  as expid','experiences.user_id as scout_id','wishlists.*','wishlists.id  as wid')
            ->get();
            // echo "<pre>"; print_r($Wishlistdata->toArray());die;
        $page_title = 'Manage Wishlist';
        $page_description = 'Some description for the page';
        return view('traveler.manage_wishlist', compact('page_title', 'page_description','Wishlistdata','user'));
    }

    public function bookExperience(Request $request,$id){
        $id = base64_decode($id);
        dd($id);
    }

    public function ManageBookedExperiences(Request $request){

        $page_title = 'Manage Booked Experiences';

        $login_id = Auth::id();

        $experiences = ExperienceBooking::with(['traveler','experience'])->where('user_id',$login_id)->get();

        return view('traveler.manage_booked_experiences',compact('experiences','page_title'));
    }
     /*
    * Manage Booked Experiences by scout
    */
    public function bookedExperienceDetail(Request $request,$id){
          $page_title = 'Booking Information';
        $experience = ExperienceBooking::with(['traveler','experience','experience_meta_detail'])->where('id',base64_decode($id))->first();
            return view('traveler.booking_detail',compact('experience','page_title'));
         
    }
    public function experienceFeedback(Request $request,$id)
    {
        $page_title = 'Experience Feedback';
         $user = Auth::user();
         $experience = ExperienceBooking::with(['traveler','experience','experience_meta_detail'])->where('id',base64_decode($id))->first();
          $message = "Please submit your feedback"; 
         $forMCheck = array();
          $forMCheck['experience-form'] ='';
          $forMCheck['scout-form'] =''; 
          $forMCheck['host-form']=''; 
          $forMCheck['is-completed-exp']='0'; 
          $forMCheck['is-completed-host']='0'; 
          $forMCheck['is-completed-scout']='0';
        return view('traveler.add_review',compact('page_title','experience','forMCheck','message','user'));
    }
    public function AddExperienceReview(Request $request)
    {
        if($request->isMethod('post')){
             $experience= Experience::with(['review_ratings','user'])->where(['id'=>$request->experience_id])->first();
            $user = Auth::user();
            $userArr  = array(
                'status' =>'1', 
                'rating' =>$request->star_value, 
                'review' =>$request->review_text, 
                'user_id' =>$user->id,  
                'review_type' =>$request->user_type,  
                'review_for' =>$request->review_for,  
                'experience_id' =>$request->experience_id,
                'review_for_scout' =>$experience->user_id,
                'review_for_host' =>$experience->assigned_host,
                'booking_id' =>$request->booking_id,
            ); 
            if(ReviewRating::create($userArr)){
                    return response()->json([
                        "message"       => 'Review submit successfully',
                        "isSucceeded"   => 1, 
                        "experience"=>$experience,
                    ]);
               }else{
                    return response()->json([
                        "message"       => 'oops there may be some error',
                        "isSucceeded"   => 0, 
                    ]);
               }

        }
    }

     public function invoice(Request $request,$id)
    {
         $page_title = 'Invoice';
           $user = Auth::user();
         $bookingDetail= ExperienceBooking::with(['experience','traveler','experience_meta_detail'])->where(['id'=>base64_decode($id)])->first();
        return view('traveler.invoice',compact('page_title','bookingDetail'));
    }

    public function bookedExperienceNow(Request $request){
        if($request->isMethod('post')){
            $data = $request->except(['freshExpId']);
            $expDetail = Experience::with(['experience_meta_detail','experience_additional_prices' => function($q) use($request){
                $q->where(['additional_person' => $request->adults , 'experience_id' => $request->freshExpId]);
            }])->where(['id' => $request->freshExpId])->first();
            $data['experience_id'] = $expDetail->id;
            $data['user_id'] = Auth::user()->id;
            $children_age_for_discount = $expDetail->experience_meta_detail->children_age_for_discount;
            $experience_price_vailid_for = $expDetail->experience_price_vailid_for;
            $children_discount_1 = $expDetail->experience_meta_detail->children_discount_1;
            $totalAmt = 0;
            $totalChildAmt = 0;
            if($expDetail->experience_type == 1){

                for ($i=0; $i < $request->adults ; $i++) { 
                    if($i<$expDetail->experience_price_vailid_for){
                        $totalAmt = $expDetail->experience_low_price;
                    }else{
                        $totalAmt = $totalAmt + $expDetail->experience_additional_person;
                    }
                }
            }else if($expDetail->experience_type == 2){
                for ($i=0; $i < $request->adults ; $i++) { 
                    if($i < $expDetail->minimum_participant){
                        $totalAmt = $expDetail->experience_low_price;
                    }else{
                        $totalAmt = $totalAmt + $expDetail->experience_additional_person;
                    }
                }
            }else if($expDetail->experience_type == 3){
                if(!empty($expDetail->experience_additional_prices)){
                    if($request->adults>=$expDetail->experience_price_vailid_for)
                    {
                    $totalAmt = $expDetail->experience_additional_prices[0]->additional_price;
                    }
                    else{
                        $totalAmt = $expDetail->experience_low_price;
                    }
                }
            }
            $totalChildAmt = $request->children * $expDetail->experience_low_price;
            $disPrice = ($children_discount_1 / 100 ) * $totalChildAmt; 
            $childNetPrice = $totalChildAmt - $disPrice;
            $totalAmt =  $childNetPrice + $totalAmt;
            $data['discount'] = $disPrice;
            $data['price'] = $totalAmt;
            $data['status'] = 0;
            $data['adults'] = $request->adults;
            $data['children'] = $request->children;
            $data['infants'] = $request->infants;
            $data['booking_start_time'] = $request->booking_start_time;
            $data['booking_end_time'] = $request->booking_end_time;
            $data['freshExpId'] = $request->freshExpId;
            if(Session::has('cart')){
                Session::forget('cart');
            }
            Session::push('cart', $data);
            if(Session::has('cart')){
                return response()->json([
                    "message"       => 'Your records save successfully',
                    "isSucceeded"   => TRUE,
                    "data"          => $data
                ]);
            }else{
                return response()->json([
                    "message"       => '',
                    "isSucceeded"   => FALSE,
                    "data"          => []
                ]);
            }
        }
    }

   

    /**
    * Cancel Experience By Traveller
    */
    public function cancelExp(Request $request){
        if($request->isMethod('post')){
            if($request->charge_id){
                try{
                    $stripe = new \Stripe\StripeClient(config('services.stripe.secret'));
                    $refundCheck = $stripe->refunds->create(['charge' => $request->charge_id , 'amount' => floor($request->amount * 100)]);
                    if($refundCheck->id){
                        $st = '';
                        if($refundCheck->status == 'succeeded'){
                            $st = '6';
                        }elseif($refundCheck->status == 'pending'){
                            $st = '7';
                        }elseif($refundCheck->status == 'failed'){
                            $st = '8';
                        }elseif($refundCheck->status == 'canceled'){
                            $st = '9';
                        }  
                        $checkOrderUp = Order::where(['id' => $request->order_id])->update(['refund_id' => $refundCheck->id , 'status' => $st]);
                        if($checkOrderUp){
                            return response()->json([
                                "message"       => 'Experience canceled successfully.',
                                "isSucceeded"   => TRUE
                            ]);     
                        }
                        
                    }
                }catch(\Exception $e){
                    return response()->json([
                        "message"       => $e->getMessage(),
                        "isSucceeded"   => FALSE
                    ]); 
                }
            }
        }
    }
    /**
    * Generate the PDF  By Traveller
    */
     public function pdfcreate(Request $request,$id)
    {
        $bookingDetail = ExperienceBooking::with(['traveler','experience','experience_meta_detail'])->where('id',base64_decode($id))->first();
        $data = ['bookingDetail' => $bookingDetail];
     // echo"<pre>";  print_r($bookingDetail);echo"<pre>";
        
        $pdf = PDF::loadView('traveler.invoice_pdf', $data);
         return $pdf->download('Invoice.pdf');
    }
}
