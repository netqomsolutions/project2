<?php

namespace App\Http\Controllers;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\File; 
use App\ContentManagementSystem;
use App\HowWork;
use App\AboutUs;
use App\HomePage;
use App\User;
use App\UserMeta;
use App\Blog;
use App\Experience;
use App\ExperienceBooking;
use App\ExperienceCategory;
use App\ExperienceFeature;
use App\Country;
use App\State;
use App\City;
use App\Partner;
use App\BlogCategory;
use App\Testimonial;
use App\ExperienceAprroval;
use App\ExperienceMetaAprroval;
use App\ExpAdditionalPriceApp;
use App\ExpCategoryRelationApp;
use App\ExpFeatureRelationApp;
use DataTables;
use Illuminate\Support\Facades\Auth;   
use App\Mail\SendMail; 
use PDF;
use App\Http\Controllers\SpaceInvoicesApolloController;
use App\OrganizationController;
use App\Http\Traits\CommonTrait;
use DateTime;
use DatePeriod;
use DateInterval;

class AdminController extends Controller
{
       use CommonTrait;

    /**
    * Dshboard Function
    */
    public function dashboard()
    {
        $page_title = 'Dashboard';
        $page_description = 'Some description for the page';

        $data['total_scouts'] = User::where(['user_role' => 3])->whereIn('status', ['1', '2'])->latest()->get()->count();
        $data['total_travelers'] = User::where(['user_role' => 2])->latest()->get()->count();
        $data['total_experiences'] = Experience::get()->count();

        return view('admin.dashboard', compact('page_title', 'page_description','data'));
    }

    /**
    * View Experience Function
    */
    public function experience(){
        $page_title = 'Experience';
        $page_description = 'Some description for the page';

        return view('admin.experience', compact('page_title', 'page_description'));
    }

    /*
    * Edit About Us Page Controls
    */
    public function editAboutUs(Request $request){
        $page_title = 'About Us';
        $page_description = 'Some description for the page';
        $aboutPageData = AboutUs::find(1);
        return view('admin.about_us', compact('page_title', 'page_description', 'aboutPageData'));
    }

    /**
     * Common Ckeditor upload image
    */
    public function uploadCkeditorImage(Request $request)
    {
        if($request->hasFile('upload')) {
            $originName = $request->file('upload')->getClientOriginalName();
            $fileName = pathinfo($originName, PATHINFO_FILENAME);
            $extension = $request->file('upload')->getClientOriginalExtension();
            $fileName = $fileName.'_'.time().'.'.$extension;
            $request->file('upload')->move(public_path('images'), $fileName);
            $CKEditorFuncNum = $request->input('CKEditorFuncNum');
            $url = asset('images/'.$fileName); 
            $msg = 'Image uploaded successfully'; 
            $response = "<script>window.parent.CKEDITOR.tools.callFunction($CKEditorFuncNum, '$url', '$msg')</script>";
               
            @header('Content-type: text/html; charset=utf-8'); 
            echo $response;
        }
    }


    /*
    * Update About Us Page Controls
    */
    public function updateAboutUs(Request $request){
        if($request->isMethod('post')){
            $data = $request->except(['_token']);  
            $oldAboutData = AboutUs::find(1);
            $data['page_featured_image'] = $oldAboutData->page_featured_image;
            $data['about_first_image'] = $oldAboutData->about_first_image;
            $data['about_second_image'] = $oldAboutData->about_second_image;
            $folderPath = 'pages/about_us';            
            if($request->hasFile('page_featured_image')) {
                $data['page_featured_image'] = $this->imageUpload($request,$folderPath,'page_featured_image');
            }
            if($request->hasFile('about_first_image')) {
                $data['about_first_image'] = $this->imageUpload($request,$folderPath,'about_first_image');
            }
            if($request->hasFile('about_second_image')) {
                $data['about_second_image'] = $this->imageUpload($request,$folderPath,'about_second_image');
            }
            $checkDataSave = AboutUs::where('id', '=', 1)->update($data);
            if($checkDataSave){
                return redirect()->route('admin-edit-about-us')->with('success','Your about us page updated successfully.');
            }else{
                return redirect()->route('admin-edit-about-us')->with('failure','Opps,something wrong,please try after some time.');
            }
        }
    }

    /*
    *Upload Image function
    */
    public function imageUpload($request,$folderPath,$imageName){
        $fName = '';
        if($request->hasFile($imageName)){
        $originName = $request->file($imageName)->getClientOriginalName();
        $fileName = pathinfo($originName, PATHINFO_FILENAME);
        $extension = $request->file($imageName)->getClientOriginalExtension();
        $fileName = $fileName.'_'.time().'.'.$extension;            
        if($request->file($imageName)->move(public_path($folderPath), $fileName)){
            $fName = $fileName;
        }
        return $fName;
        }
        return $fName;       
    }

    /*
    * Edit Home Page
    */
    public function editHomePage(Request $request){
        $page_title = 'Home page';
        $page_description = 'Some description for the page';
        $oldHomeData = HomePage::find(1);
        $PageData = ContentManagementSystem::where(['id' => 6])->first();
        $conData = json_decode($PageData->page_content,true);
        $PageData->section_one_title = $conData['section_one_title'];
        $PageData->section_two_title = $conData['section_two_title'];
        $PageData->section_three_title = $conData['section_three_title'];
        $PageData->section_one_description = $conData['section_one_description'];
        $PageData->section_two_description = $conData['section_two_description'];
        $PageData->section_three_description = $conData['section_three_description'];
        return view('admin.edit_home_page',compact('page_title', 'page_description','oldHomeData','PageData'));
    }


    /*
    * Banner Multiple Upload Image function
    */
    public function bannerMultiImageUpload($request,$folderPath,$imageName){
        $realImageName = '';
        if($request) {
        $originName = $request->getClientOriginalName();
        $fileName = pathinfo($originName, PATHINFO_FILENAME);
        $extension = $request->getClientOriginalExtension();
        $fileName = $fileName.'_'.time().'.'.$extension;            
        if($request->move(public_path($folderPath), $fileName)){
            $realImageName = $fileName;
        }
        return  $fileName;       
        }
    }


    /*
    * Update HomePage Controls
    */
    public function updateHomePage(Request $request){
        if($request->isMethod('post')){
            $folderPath = 'pages/homepage';
            foreach($request['banner_title'] as $key=>$res)
            { 
            if(!empty($request->file('banner_image')[$key])) {                     
                $banner_image = $this->bannerMultiImageUpload($request->file('banner_image')[$key],$folderPath,'banner_image');
            }else{
                $banner_image= $request['old_banner_image'][$key];
            }
            $bannerContent[]=[
                "banner_small_heading"  =>$request['banner_small_heading'][$key],
                "banner_title"          =>$request['banner_title'][$key],
                "banner_description"    =>$request['banner_description'][$key],
                "banner_button_name"    =>$request['banner_button_name'][$key],
                "banner_button_link"    =>$request['banner_button_link'][$key],
                "banner_image"          =>$banner_image,
            ];
            }
            /*** explore Content ***/
            if($request->hasFile('explore_video')) {
                $explore_video = $this->imageUpload($request,$folderPath,'explore_video');
            }else{
                $explore_video= $request['old_explore_video'];
            }
            if($request->hasFile('explore_background_image')) {                 
                $explore_background_image = $this->imageUpload($request,$folderPath,'explore_background_image');
            }else{
                $explore_background_image= $request['old_explore_background_image'];
            }
            if($request->hasFile('explore_video_image')){
                $explore_video_image = $this->imageUpload($request,$folderPath,'explore_video_image');
            }else{
                $explore_video_image= $request['old_explore_video_image_old'];
            }
            $exploreContent=[
                "explore_small_heading"     =>$request['explore_small_heading'],
                "explore_title"             =>$request['explore_title'],
                "explore_video_image"       =>$explore_video_image,
                "explore_video"             =>$explore_video,
                "explore_background_image"  =>$explore_background_image,
                "explore_description"       =>$request['explore_description'],
            ];
            $data['slug']=$request['slug'];    
            $data['meta']=$request['meta'];
            $data['banner_content'] = json_encode($bannerContent);
            $data['explore_content'] = json_encode($exploreContent); 
            $checkDataSave = HomePage::where('id', '=', 1)->update($data);
            if($checkDataSave){
                return redirect()->route('admin-edit-home-page')->with('success','Home page updated successfully.');
            }else{
                return redirect()->route('admin-edit-home-page')->with('failure','Opps,something wrong,please try after some time.');
            }
        }
    }

    /*
    *  Partners list view page
    */

    public function partnersList(Request $request)
    {
        if ($request->ajax()) {
            $data = Partner::all();
            return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('action', function($row){
                    $btn = '<a href="javascript:void(0)" data-toggle="modal" data-target="#add-partners-modal" partner-id="'.base64_encode($row->id).'" act-name="edit-partners" class="edit btn btn-success btn-sm action-partner-add-edit-btn mr-1">Edit</a><a href="javascript:void(0)" partner_id="'.base64_encode($row->id).'" class="delete btn btn-danger btn-sm action-partner-btn" data-toggle="confirmation">Delete</a>';
                    return $btn;
                })
                ->rawColumns(['action'])
                ->make(true);
        }
    } 
   /*
   * Get Partner by partner id
   */
    public function getPartnerById(Request $request)
    {
        if ($request->ajax()) {
            $data = Partner::where(['id' => base64_decode($request->id)])->first();
           if($data){
                return response()->json([
                    "message"       => 'Partner get successfully.',
                    "isSucceeded"   => TRUE,
                    "data"          => $data
                ]);
            }else{
                return response()->json([
                    "message"       => 'There is no Partner.',
                    "isSucceeded"   => FALSE,
                    "data"          => $data
                ]);

            }
        }
    }

    /*
    *  Add partner control function
    */

    public function addPartner(Request $request)
    {
         if($request->isMethod('post')){
             $partner_id='';
            if(isset($request->partner_id)){
             $partner_id  = $request->partner_id;
            }
            $data = $request->except(['_token','partner_id']);
            $folderPath = 'pages/partners';
            if($request->hasFile('image')) {                 
                $data['image'] = $this->imageUpload($request,$folderPath,'image');
            } 
           if($partner_id!=='') {
               $partner=  Partner::where('id', '=', base64_decode($partner_id))->update($data);
               $msg="Partner updated successfully";
            }else{ 
                 $partner = Partner::create($data);
                 $msg="Partner created successfully";
            }
      if($partner)
                 {
                   return redirect()->route('admin-edit-home-page')->with('success',$msg);
                 }else{
                  return redirect()->route('admin-edit-home-page')->with('failure','Opps,something wrong,please try after some time.');
            }

        }
    }
     /*
    *  delete partner control function
    */

    public function deletePartner(Request $request)
    {
         if($request->isMethod('post')){
            if(!empty($request->partner_id)){
                $del = Partner::where(['id' => base64_decode($request->partner_id)])->delete();
               if($del){
                    return response()->json([
                        'isSucceeded' => TRUE,
                        'message'=>"Partner successfully Deleted."
                    ]);
                }else{
                    return response()->json([
                        'isSucceeded' => FALSE,
                        'message'=>"Something went wrong."
                    ]);
                }     
            }
        }
    }


    /*
    * Multiple Upload Image function
    */
    public function multiImageUpload($request,$folderPath,$imageName){
        $data = [];
        if($request->hasFile($imageName)) {
            $files = $request->file($imageName);
            foreach($files as $file){
                $originName = $file->getClientOriginalName();
                $fileName = pathinfo($originName, PATHINFO_FILENAME);
                $extension = $file->getClientOriginalExtension();
                $fileName = $fileName.'_'.time().'.'.$extension;            
                if($file->move(public_path($folderPath), $fileName)){
                    $data[] = $fileName;    
                }                
            }
            return $data;           
        }
        return $data;
    }

    /*
    * Scout list function
    */
    public function scoutsList(Request $request)
    {
        $page_title = 'List Of Scouts';
        $page_description = 'Some description for the page';
        if ($request->ajax()) {
             $data = User::with('user_meta')->whereHas('user_meta',function($q){
                $q->where(['account_type'=>0]);
            })->where(['user_role' => 3])->whereIn('status', ['1', '2'])->latest()->get();
            return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('action', function($row){
                    $btn = '<a href="'. route('admin-scout-detail',['id' => base64_encode($row->id)]) .'" class="edit btn btn-success btn-sm">View</a> <a href="javascript:void(0)" data-toggle="modal" scout_id="'.base64_encode($row->id).'" class="delete btn btn-danger btn-sm action-scout-btn">Delete</a>';
                    if($row->user_meta->student_balance>0)
                    {
                        $btn .='<a href="javascript:void(0)" data-toggle="modal" data-target="#studentPaymentModal" scout_id="'.base64_encode($row->id).'"  class="delete btn btn-success btn-sm scout-pay-button ml-1">Pay</a>';
                    }
                    return $btn;
                })
                ->rawColumns(['action'])
                ->make(true);
        }
      
        return view('admin.scouts_list',compact('page_title','page_description'));
    }
    /*
    * Student Scout list function
    */
    public function studentScoutsList(Request $request)
    {
        $page_title = 'List Of Student Scouts';
        $page_description = 'Some description for the page';
        if ($request->ajax()) {
            $data = User::with('user_meta')->whereHas('user_meta',function($q){
                $q->where(['account_type'=>1]);
            })->where(['user_role' => 3])->whereIn('status', ['1', '2'])->latest()->get();
            return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('action', function($row){
                    $btn = '<a href="'. route('admin-scout-detail',['id' => base64_encode($row->id)]) .'" class="edit btn btn-success btn-sm">View</a> <a href="javascript:void(0)" data-toggle="modal" scout_id="'.base64_encode($row->id).'" class="delete btn btn-danger btn-sm action-scout-btn">Delete</a> 
                    <a href="javascript:void(0)" data-toggle="modal" data-target="#studentPaymentModal" scout_id="'.base64_encode($row->id).'"  class="delete btn btn-success btn-sm scout-pay-button">Pay</a>';
                    return $btn;
                })
                ->rawColumns(['action'])
                ->make(true);
        }
      
        return view('admin.student_scout_list',compact('page_title','page_description'));
    }
     /*
    * Traveler list function
    */
    public function travelerList(Request $request)
    {
        $page_title = 'List Of Travelers';
        $page_description = 'Some description for the page';
        if ($request->ajax()) {
            $data = User::where(['user_role' => 2])->latest()->get();
            return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('action', function($row){
                    $btn = '<a href="'. route('admin-scout-detail',['id' => base64_encode($row->id)]) .'" class="edit btn btn-success btn-sm">View</a>';
                    return $btn;
                })
                ->rawColumns(['action'])
                ->make(true);
        }
      
        return view('admin.travelers_list',compact('page_title','page_description'));
    }



    /*
    * New Scout Request list function
    */
    public function scoutsRequestList(Request $request)
    {
        $page_title = 'List Of Scouts Request';
        if ($request->ajax()) {
            $data = User::where(['user_role' => 3 , 'status' => 0])->get();
            return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('action', function($row){
                    $btn = '<a href="'. route('admin-scout-approval',['id' => base64_encode($row->id)]) .'" class="edit btn btn-success btn-sm">View</a>';
                    return $btn;
                })
                ->rawColumns(['action'])
                ->make(true);
        }
      
        return view('admin.scout_request_list',compact('page_title'));
    }


    /*
    * Scout Deatil function
    */
    public function scoutDetail(Request $request,$id){ 
        if($request->isMethod('get') &&  $id){
            $page_title = 'Scout Detail';
             $languages =  DB::table('languages')->get(); 
            $scoutData = User::with(['scout','user_meta'])->where(['id' => base64_decode($id)])->first();   
           return view('admin.scout_detail',compact('page_title','scoutData','languages')); 
        }
    }


    /*
    * Update Scout Commission function
    */
        public function updateScoutComission(Request $request){ 
        if($request->isMethod('post')){
            $reqdata = $request->except(['_token']);
             $reqdata['scout_commission']=$request->scout_commission; 
            $scoutData = UserMeta::where(['user_id' => base64_decode($request->id)])->update($reqdata);        
            if($scoutData){
                 return response()->json([
                        'isSucceeded' => TRUE,
                        'message'=>"Comission successfully updated."
                    ]);
            }else{
                 return response()->json([
                        'isSucceeded' => FALSE,
                        'message'=>"Somthing went wrong!."
                    ]);
            }
        }
    }
    /*
    * Scout Approval function
    */
    public function scoutApproval(Request $request,$id){ 
        if($request->isMethod('get') &&  $id){
            $page_title = 'Scout Deatil';
            $languages =  DB::table('languages')->get(); 
            $scoutData = User::with(['user_meta'])->where(['id' => base64_decode($id)])->first();          
            return view('admin.scout_approval',compact('page_title','scoutData','languages')); 
        }
    }

    /*
    * Update Scout Status function
    */
    public function updateScoutRequestStatus(Request $request){ 
        if($request->isMethod('post')){
            $reqdata = $request->except(['_token','scout_id']);
            $reqdata['email_verified_at']=date('Y-m-d H:i:s');
            $msg='';
            $scoutData = User::where(['id' => base64_decode($request->scout_id)])->update($reqdata);
            $data['scout']  = User::where(['id' => base64_decode($request->scout_id)])->first(); 
            
             if( $data['scout']->user_meta->account_type==1){
                 $route="admin-student-scouts";
              }else{
                 
                 $route="admin-scouts"; 
              }
            if($request->status == 1){
                $msg = 'Scout approve successfully.';
              if( $data['scout']->user_meta->account_type==1){ 
              }else{
                 $ApolloData = $this->apolloCreate($data['scout']); // Create the User Apollo Org And Account
                  if($ApolloData['status']){
                     $returndata['apollo_org_id']= $ApolloData['apollo_org_id'];
                     $returndata['apollo_acc_id']= $ApolloData['apollo_acc_id'];
                     $scoutMetaData = UserMeta::where(['user_id' => base64_decode($request->scout_id)])->update($returndata);
                       } 
              }
            }
            elseif($request->status == 3){
               $msg = 'Scout rejected successfully.';
               user::where(['id' => base64_decode($request->scout_id)])->forceDelete();
            } 
          
            $data['status'] =($data['scout']['status']==1)? "Approved" : "Disapproved";
            $data['subject']= "Scout " .$data['status'];
           // $sendEmail = Mail::to([$data['scout']['email']])->send( new SendMail($data,'new_scout_approval'));
                      
            if($scoutData){

                return redirect()->route($route)->with('success',$msg);
            }else{
                return redirect()->route($route)->with('failure','Opps,something went wrong,please try again.');
            }
        }
    }


    /*
    * Update Scout Status function
    */
    public function updateScoutStatus(Request $request){ 
        if($request->isMethod('post')){
            $data = $request->except(['_token','scout_id']);
            $msg='';
            if($request->status == 1){
                $msg = 'Scout active successfully.';
            }elseif($request->status == 2){
                $msg = 'Scout inactive successfully.';
            }
            $scoutData = User::where(['id' => base64_decode($request->scout_id)])->update($data);            
            if($scoutData){
                return redirect()->route('admin-scouts')->with('success',$msg);
            }else{
                return redirect()->route('admin-scouts')->with('failure','Opps,something went wrong,please try again.');
            }
        }
    }

    /*
    * Edit Scout list function
    */
    public function editScout(Request $request,$id){ 
        if($request->isMethod('get') &&  $id){
            $page_title = 'Edit Scout';
            $page_description = 'Some description for the page';
            $scoutData = User::where(['id' => base64_decode($id)])->first();             
            return view('admin.edit_scout',compact('page_title','page_description', 'scoutData')); 
        }
    }

    /*
    * Update Scout list function
    */
    public function updateScout(Request $request){ 
        if($request->isMethod('post')){
            $data = $request->except(['_token','scout_id']);
            $scoutData = User::where(['id' => base64_decode($request->scout_id)])->update($data);            
            if($scoutData){
                return redirect()->route('admin-scouts')->with('success','Scout updated successfully.');
            }else{
                return redirect()->route('admin-scouts')->with('failure','Opps,something went wrong,please try again.');
            }
        }
    }

    /*
    *Delete Scout By Admin
    */
    public function deleteScout(Request $request){
        if($request->isMethod('post')){
            if(!empty($request->scout_id)){
                $del = User::where(['id' => base64_decode($request->scout_id)])->delete();
                if($del){
                    return response()->json([
                        'isSucceeded' => TRUE,
                        'message'=>"successfully Deleted."
                    ]);
                }else{
                    return response()->json([
                        'isSucceeded' => FALSE,
                        'message'=>"Something went wrong."
                    ]);
                }     
            }
        }
    }

    /*
    * view Privacy Page Controls
    */
    public function editPrivacyPolicy(Request $request){
        $page_title = 'Privacy Policy';
        $page_description = 'Some description for the page';
        $privacyPageData = ContentManagementSystem::find(1);
        return view('admin.edit_privacy_policy', compact('page_title', 'page_description', 'privacyPageData'));
    }
     /*
    * view Cancellation Page Controls
    */
    public function editCanellationPolicy(Request $request){
        $page_title = 'Cancellation Policy';
        $page_description = 'Some description for the page';
        $privacyPageData = ContentManagementSystem::find(8);
        return view('admin.edit_cancellation_policy', compact('page_title', 'page_description', 'privacyPageData'));
    }

    /*
    * view Privacy Page Controls
    */
    public function editTermsOfUse(Request $request){
        $page_title = 'Terms of Use';
        $page_description = 'Some description for the page';
        $PageData = ContentManagementSystem::find(2);
        return view('admin.edit_terms_use', compact('page_title', 'page_description', 'PageData'));
    }
     /*
    * view How its work Page Controls
    */
    public function editHowItWorks(Request $request){
        $page_title = 'How It Works';
        $page_description = 'Some description for the page';
        $PageData = ContentManagementSystem::where(['id' => 5])->first();
        $conData = json_decode($PageData->page_content,true);
        $PageData->tab_one_image = $conData['tab_one_image'];
        $PageData->tab_one_content = $conData['tab_one_content'];
        $PageData->tab_two_content = $conData['tab_two_content'];
        $PageData->tab_three_content = $conData['tab_three_content'];
        return view('admin.edit_how_it_works', compact('page_title', 'page_description', 'PageData'));
    }
     /*
    * add update how scout tab listing control
    */
    public function addUpdateScoutTabList(Request $request){
       if($request->isMethod('post')){
            $data = $request->except(['_token','list_id']);
            $data['created_by']= Auth::user()->id;
           if($request['list_id']!='') {
               $howWork=  HowWork::where('id', '=', base64_decode($request['list_id']))->update($data);
               $msg="List updated successfully";
            }else{
                 $howWork = HowWork::create($data);
                $msg="List created successfully";
            }
             if($howWork)
                 {
                     return redirect()->route('admin-edit-how-it-works')->with('success',$msg);
                 }else{
                return redirect()->route('admin-edit-how-it-works')->with('failure','Opps,something wrong,please try after some time.');
            }

        }
    }
    /*
    * add update how scout tab listing control
    */
    public function deleteScoutTabListById(Request $request){
       if($request->isMethod('post')){
            if(!empty($request->list_id)){
                $del = HowWork::where(['id' => base64_decode($request->list_id)])->delete();
                if($del){
                    return response()->json([
                        'isSucceeded' => TRUE,
                        'message'=>"List successfully Deleted."
                    ]);
                }else{
                    return response()->json([
                        'isSucceeded' => FALSE,
                        'message'=>"Something went wrong."
                    ]);
                }    
            }
        }
    }
     /*
    *  Scout tab list Controls
    */

    public function scoutTabList(Request $request)
    {
        if ($request->ajax()) {
            $data = HowWork::where(['type' => 'scout_tab'])->get();
            return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('action', function($row){
                    $btn = '<a href="javascript:void(0)" data-toggle="modal" data-target="#add-scout-list" list-id="'.base64_encode($row->id).'" act-name="edit-scout-tab-list" page-name="scout_tab" class="edit btn btn-success btn-sm action-add-edit-btn">Edit</a> <a href="javascript:void(0)" act-name="del-scout-tab-list" list-id="'.base64_encode($row->id).'" class="delete btn btn-danger btn-sm action-scout-list-delete-btn">Delete</a>';
                    return $btn;
                })
                ->rawColumns(['action'])
                ->make(true);
        }
    }  
     public function getExperienceTabList(Request $request)
    {
        if ($request->ajax()) {
            $data = HowWork::where(['type' => 'experience_tab'])->get();
            return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('action', function($row){
                    $btn = '<a href="javascript:void(0)" data-toggle="modal" data-target="#add-scout-list" list-id="'.base64_encode($row->id).'" act-name="edit-scout-tab-list" page-name="experience_tab" class="edit btn btn-success btn-sm action-add-edit-btn">Edit</a> <a href="javascript:void(0)" data-toggle="modal" data-target="#delete-scout-tab-list" act-name="del-scout-tab-list" list-id="'.base64_encode($row->id).'" class="delete btn btn-danger btn-sm action-scout-list-delete-btn">Delete</a>';
                    return $btn;
                })
                ->rawColumns(['action'])
                ->make(true);
        }
    }  
     /*
    *  Scout tab list Controls
    */

    public function getScoutTabListById(Request $request)
    {
        if ($request->isMethod('post')) {
            $data = HowWork::where(['id' => base64_decode($request->id)])->first();
           if($data){
                return response()->json([
                    "message"       => 'Experiences get successfully.',
                    "isSucceeded"   => TRUE,
                    "data"          => $data
                ]);
            }else{
                return response()->json([
                    "message"       => 'There is no experience.',
                    "isSucceeded"   => FALSE,
                    "data"          => $data
                ]);

            }
        }
    }  

     /*
    * view Blog Page Controls
    */
    public function editBlogPage(Request $request){
        $page_title = 'Blog';
        $page_description = 'Some description for the page';
        $PageData = ContentManagementSystem::find(3);
        return view('admin.edit_blog_page', compact('page_title', 'page_description', 'PageData'));
    } 

     /*
    *  Blog Category list Controls
    */

    public function blogCategoryList(Request $request)
    {
        if ($request->ajax()) {
            $data = BlogCategory::all();
            return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('action', function($row){
                    $btn = '<a href="javascript:void(0)" data-toggle="modal" data-target="#add-bog-cat-modal" blog-cat-id="'.base64_encode($row->id).'" page-name="'.$row->name.'" act-name="edit-blog-cat" class="edit btn btn-success btn-sm action-blog-edit-btn">Edit</a> <a href="javascript:void(0)" blog-cat-id="'.base64_encode($row->id).'" class="delete btn btn-danger btn-sm action-blog-cat-delete-btn">Delete</a>';
                    return $btn;
                })
                ->rawColumns(['action'])
                ->make(true);
        }
    } 
    /*
    * add blog category Controls
    */
    public function addBlogCategory(Request $request){
       if($request->isMethod('post')){
            $data = $request->except(['_token','blog_cat_id']);
           $data['slug']= Str::slug($request['name'], '-');
           if($request['blog_cat_id']!='') {
               $blogCat=  BlogCategory::where('id', '=', base64_decode($request['blog_cat_id']))->update($data);
               $msg="Blog Category updated successfully";
            }else{
                 $blogCat = BlogCategory::create($data);
                $msg="Blog Category created successfully";
            }
             if($blogCat)
                 {
                     return redirect()->route('admin-blog-categories')->with('success',$msg);
                 }else{
                return redirect()->route('admin-blog-categories')->with('failure','Opps,something wrong,please try after some time.');
            }

        }
    }
     /*
    * Delete blog category Controls
    */
    public function deleteBlogCategory(Request $request){
       if($request->isMethod('post')){
            if(!empty($request->blog_cat_id)){
            $del = BlogCategory::where(['id' => base64_decode($request->blog_cat_id)])->delete();
                if($del){
                    return response()->json([
                        'isSucceeded' => TRUE,
                        'message'=>"Category successfully Deleted."
                    ]);
                }else{
                    return response()->json([
                        'isSucceeded' => FALSE,
                        'message'=>"Something went wrong."
                    ]);
                }      
            }
        }
    }  
    /*
    *  blog List  Controls
    */
    public function blogList(Request $request){
       $page_title = 'List of Blog';
        $page_description = 'Some description for the page';
        if ($request->ajax()) {
            $data = Blog::with('blogCat')->get();
            return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('action', function($row){
                    $btn = '<a href="'. route('admin-edit-blog',['id' => base64_encode($row->id)]) .'" class="edit btn btn-success btn-sm">Edit</a> <a href="javascript:void(0)"  blog-id="'.base64_encode($row->id).'" class="delete btn btn-danger btn-sm action-blog-delete-btn">Delete</a>';
                    return $btn;
                })
                ->rawColumns(['action'])
                ->make(true);
        }
      
        return view('admin.blogList',compact('page_title','page_description'));
    }

   /*
    * addb update blog Controls
    */
    public function addUpdateBlog(Request $request)
    {
        if($request->isMethod('post')){
            $data = $request->except(['_token','blog_id','newFile']);
           $data['blog_slug']= Str::slug($request['blog_title'], '-');
           $data['created_by']= Auth::user()->id;
           $folderPath = 'pages/blogs'; 
           if($request->hasFile('feature_image')) { 
           if(isset($request->newFile))
           {            
                $data['feature_image'] = $this->blogImageUpload($request,$folderPath,'newFile');
            }else{
                 $data['feature_image'] = $this->blogImageUpload($request,$folderPath,'feature_image');
                
            }
            }
           if($request['blog_id']!=0) {
               $blogCat=  Blog::where('id', '=', $request['blog_id'])->update($data);
               $msg="Blog updated successfully";
            }else{
                 $blogCat = Blog::create($data);
                $msg="Blog created successfully";
            }
             if($blogCat)
                 {
                     return response()->json([
                        "message"       => $msg,
                        "isSucceeded"   => TRUE,
                    ]);
                 }else{
                 return response()->json([
                        "message"       => "Opps,something wrong,please try after some time.",
                        "isSucceeded"   => TRUE,
                    ]);
            }

        }

    }
    public function blogImageUpload($request,$folderPath,$imageName)
    {
        $fName = '';
        $savefileName = '';
        if($request->hasFile($imageName)){
            $originName = $request->file($imageName)->getClientOriginalName();
            $fileName = pathinfo($originName, PATHINFO_FILENAME);
            $extension = $request->file($imageName)->getClientOriginalExtension();
            $fileName = $fileName.'_'.time().'.'.$extension; 
            $fileName = str_replace(' ', '_', $fileName);         
           $fileName = str_replace('list-thumb-', '', $fileName);      
           $fileName = str_replace('detail-thumb-', '', $fileName);      
            if($request->file($imageName)->move(public_path($folderPath), $fileName)){
                //$fName = "/list-thumb-".$fileName;
                if($imageName=='feature_image')
                {
                    $thumbnailpic1='/detail-thumb-'.$fileName;
                    $expdatabaseFile= createThumbnail($folderPath,$fileName,$thumbnailpic1,$width=1070,$height=600);
                    $savefileName="detail-thumb-".$fileName;
                    $image_path= public_path('pages/blogs/'.$fileName);
                    if(File::exists($image_path)) {
                        File::delete($image_path);
                    } 
                }else{
                $savefileName=$fileName;
            }
               $thumbnailpic="/list-thumb-".$savefileName;
            $explistFile= createThumbnail($folderPath,$fileName,$thumbnailpic,$width=700,$height=500); 
            $image_path = $folderPath.$fileName;  // Value is not URL but directory file path  
             
            }
            return $savefileName;
        }
        return $savefileName;    
    }

     /*
    * add blog  Controls
    */
    public function addBlogForm(Request $request,$id=null){
         $page_title = 'Add Blog';
        $page_description = 'Some description for the page';
         $blogCategories = BlogCategory::all();
         if($request->isMethod('get') && $id==null){
           return view('admin.addBlogForm',compact('page_title','page_description','blogCategories'));
        }else{
             $resData = Blog::where(['id' => base64_decode($id)])->first(); 
            return view('admin.addBlogForm',compact('page_title','page_description','blogCategories','resData'));
        }
     
    }
     /*
    * Delete blog  Controls
    */
    public function deleteBlog(Request $request){
        if($request->isMethod('post')){
            if(!empty($request->blog_id)){
                $del = Blog::where(['id' => base64_decode($request->blog_id)])->delete();
                if($del){
                    return response()->json([
                        'isSucceeded' => TRUE,
                        'message'=>"Blog successfully Deleted."
                    ]);
                }else{
                    return response()->json([
                        'isSucceeded' => FALSE,
                        'message'=>"Something went wrong."
                    ]);
                }    
            }
        }
     
    }

      /*
    * view Blog Page Controls
    */
    public function editContactUs(Request $request){
        $page_title = 'Contact Us';
        $page_description = 'Some description for the page';
        $contactUs =  ContentManagementSystem::where(['id' => 4])->first();  
        $conData = json_decode($contactUs->page_content,true);
        $contactUs->email = $conData['email'];
        $contactUs->address = $conData['address'];
        $contactUs->phone = $conData['phone'];
        $contactUs->address_line_2 = $conData['address_line_2'];
        $contactUs->address_link = $conData['address_link'];
        $LinkData =  ContentManagementSystem::where(['id' => 7])->first();
        $liData = json_decode($LinkData->page_content,true);
        $LinkData->facebook = $liData['facebook'];
        $LinkData->twitter = $liData['twitter'];
        $LinkData->instagram = $liData['instagram'];
        $LinkData->linkedin = $liData['linkedin'];
        return view('admin.edit_contact_detail', compact('contactUs','page_title', 'page_description','LinkData'));
    }

    /*
    * Update CMS Page Controls
    */
    public function updateCmsPages(Request $request){
        if($request->isMethod('post')){
            $data = $request->except(['_token','page_id','email','address','phone','tab_one_image','tab_one_content','tab_two_content','tab_three_content','section_one_title','section_two_title','section_three_title','section_one_description','section_two_description','section_three_description','old_tab_one_img','facebook','instagram','twitter','linkedin','address_line_2','address_link']);  
            $data['user_id']=Auth::user()->id;
            $folderPath = 'pages/privacy'; 
            if( $request['page_id']==4)
            {
                $data['page_content']= json_encode([
                    "email"     =>$request->email,
                    "address"   =>$request->address,
                    "phone"     =>$request->phone,
                    "address_line_2"     =>$request->address_line_2,
                    "address_link"     =>$request->address_link,
                ]);
                $message="Contact us successfully updated";
                $route="admin-edit-contact-us";
            }
            if( $request['page_id']==5)
            {
                 if($request->hasFile('tab_one_image')) {
                    $tab_one_image = $this->imageUpload($request,$folderPath,'tab_one_image');
                 }else{
                    $tab_one_image=$request->old_tab_one_img;
                 }
                $data['page_content']= json_encode([
                    "tab_one_image"     =>$tab_one_image,
                    "tab_one_content"   =>$request->tab_one_content,
                    "tab_two_content"   =>$request->tab_two_content,
                    "tab_three_content"   =>$request->tab_three_content,
                ]);
                $message="How it work page successfully updated";
                $route="admin-edit-how-it-works";
            } 
            if( $request['page_id']==6)
            {
                $data['page_content']= json_encode([
                    "section_one_title"         =>$request->section_one_title,
                    "section_two_title"         =>$request->section_two_title,
                    "section_three_title"       =>$request->section_three_title,
                    "section_one_description"   =>$request->section_one_description,
                    "section_two_description"   =>$request->section_two_description,
                    "section_three_description" =>$request->section_three_description,
                ]);
                $message="Connect & Discover successfully updated";
                $route="admin-edit-home-page";
            }
            if( $request['page_id']==7)
            {
                $data['page_title']=$request->page_title;
                $data['page_content']= json_encode([
                    "facebook"         =>$request->facebook,
                    "twitter"          =>$request->twitter,
                    "instagram"        =>$request->instagram,
                    "linkedin"         =>$request->linkedin,
                ]);
                $message="Social Links successfully updated";
                $route="admin-edit-contact-us";
            }
            if($request->hasFile('page_featured_image')) {
                $data['page_featured_image'] = $this->imageUpload($request,$folderPath,'page_featured_image');
            }
            $checkDataSave = ContentManagementSystem::where('id', '=', $request['page_id'])->update($data);
            if($request['page_id']==1)
            {
                $message="Privacy Policy successfully updated";
                $route="admin-edit-privacy-policy";
            }
            elseif($request['page_id']==2)
            {
                $message="Terms of use successfully updated";
                $route="admin-edit-term-of-use";
            }
            elseif($request['page_id']==3)
            {
                $message="Blog page successfully updated";
                $route="admin-blog-categories";
            }
            elseif($request['page_id']==8)
            {
                $message="Cancellation Policy successfully updated";
                $route="admin-edit-cancellation-policy";
            }
            if($checkDataSave){
                return redirect()->route($route)->with('success',$message);
            }else{
                return redirect()->route($route)->with('failure','Opps,something wrong,please try after some time.');
            }
        }
    }
     /*
    * Add Update Testimonial Controls
    */
    public function addUpdateTestimonials(Request $request)
    {
        if($request->isMethod('post')){
            $data = $request->except(['_token','testimonial_id']);
            $data['created_by']= Auth::user()->id;
            $folderPath='pages/testimonialUser';
            if($request->hasFile('profile_image')) {
                $data['profile_image'] = $this->imageUpload($request,$folderPath,'profile_image');
            }
           if($request['testimonial_id']!='') {
               $testimonial=  Testimonial::where('id', '=', base64_decode($request['testimonial_id']))->update($data);
               $msg="Testimonial updated successfully";
            }else{
                 $testimonial = Testimonial::create($data);
                $msg="Testimonial created successfully";
            }
            if($testimonial){
                return redirect()->route('admin-edit-about-us')->with('success',$msg);
                }else{
                return redirect()->route('admin-edit-about-us')->with('failure','Opps,something wrong,please try after some time.');
            }

        }
    }
     /*
    * Add Update Testimonial Controls
    */
    public function getTestimonialList(Request $request)
    {
        if($request->ajax()){
         $data = Testimonial::all();
            return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('action', function($row){
                    $btn = '<a href="javascript:void();" data-toggle="modal" data-target="#add-testimonial-modal" testimonial-id="'.base64_encode($row->id).'" act-name="edit-testimonial" class="edit btn btn-success btn-sm action-add-edit-btn">Edit</a> <a href="javascript:void(0)" testimonial-id="'.base64_encode($row->id).'" class="delete btn btn-danger btn-sm action-testimonial-delete-btn">Delete</a>';
                    return $btn;
                })
                ->rawColumns(['action'])
                ->make(true);        
    
        }
    }
     /*
    * Add Update Testimonial Controls
    */
    public function getTestimonialById(Request $request)
    {
       if ($request->isMethod('post')) {
            $data = Testimonial::where(['id' => base64_decode($request->id)])->first();
           if($data){
                return response()->json([
                    "message"       => 'Testimonial get successfully.',
                    "isSucceeded"   => TRUE,
                    "data"          => $data
                ]);
            }else{
                return response()->json([
                    "message"       => 'There is no Testimonial.',
                    "isSucceeded"   => FALSE,
                    "data"          => $data
                ]);

            }
        }
    } 

    public function deleteTestimonialById(Request $request)
    {
       if($request->isMethod('post')){
            if(!empty($request->testimonial_id)){
                $del = Testimonial::where(['id' => base64_decode($request->testimonial_id)])->delete();
                if($del){
                return response()->json([
                    "message"       => 'Testimonial Deleted successfully.',
                    "isSucceeded"   => TRUE,
                ]);
            }else{
                return response()->json([
                    "message"       => 'Something went wrong.',
                    "isSucceeded"   => FALSE,
                ]);

            }  
            }
        }
    }



    public function experienceCategories(Request $request){ 
         $page_title="Manage Experience Categories";
        if($request->ajax()){ 
              $data = ExperienceCategory::all();
            return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('action', function($row){
                    $btn = '<a href="javascript:void();" data-toggle="modal" data-target="#add-exp-cat-modal-edit" data-id="'.base64_encode($row->id).'" act-name="edit-testimonial" class="edit btn add-exp-cat-modal-edit btn-success btn-sm action-add-edit-btn">Edit</a> <a href="javascript:void(0)" data-id="'.base64_encode($row->id).'" class="delete btn btn-danger btn-sm action-row-delete-btn">Delete</a>';
                    return $btn;
                })
                ->rawColumns(['action'])
                ->make(true);        
    
        }
                $checkDataupdatess ='';
                $checkDataSave ='';
                if($request->isMethod('post')){
                    $data = $request->except(['_token']);

                 $msg='Opps,something wrong,please try after some time.';
                 $msgType='failure';
            if(!empty($data['id'])){
               $checkDataupdate = ExperienceCategory::where('id', '=', $data['id'])->update($data);
               if($checkDataupdate){
                 $msgType='success';
                 $msg='Experince page updated successfully.';
               }

            }else{
                $insert = ExperienceCategory::create($data);
               if($insert){
                 $msgType='success';
                 $msg='Experince inserted successfully.';
               }
            }
         return redirect()->route('admin-experience-categories')->with($msgType,$msg);
        }
        return view('admin.experienceCategories',compact('page_title'));

    }

    public function getExperienceCategories(Request $request){
         if($request->is_ajax == 1){ 
               $data = ExperienceCategory::where(['id' => base64_decode($request->id)])->first();

            if($data){
                return response()->json([
                    "message"       => 'Testimonial get successfully.',
                    "isSucceeded"   => TRUE,
                    "data"          => $data
                ]);
            }else{
                return response()->json([
                    "message"       => 'There is no Testimonial.',
                    "isSucceeded"   => FALSE,
                    "data"          => $data
                ]);

            }
               
                  
    
        } 
    }

    public function deleteData(Request $request){
        if(Auth::user())
        {
          if($request->is_ajax == 1){ 
            $table = $request->table;
            $id = base64_decode($request->id);
             $del= DB::table($table)->where('id', '=', $id)->delete();
            if($del){
                return response()->json([
                    "message"       => 'Deleted successfully.',
                    "isSucceeded"   => TRUE,
                ]);
            }else{
                return response()->json([
                    "message"       => 'Something went wrong.',
                    "isSucceeded"   => FALSE,
                ]);

            }  
            }
        }
    }
    
    /*
    * View Experience List
    */
     public function getExperienceList(Request $request){
        $page_title = 'List of Experiences';
        if ($request->ajax()) {
            $data = Experience::with('host','user')->get();
            
            return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('action', function($row){
                    $btn = '<a target="_blank" href="'. route('experience-detail',['id' => base64_encode($row->id)]) .'" class="btn btn-success btn-sm">View</a>';
                    return $btn;
                })
                ->rawColumns(['action'])
                ->make(true);
        }

       return view('admin.experience_list',compact('page_title'));
    }
    
    /*
    * View Experience Detail
    */
     public function viewExperienceDetail(Request $request){
        if($request->isMethod('get')){
             $experience = Experience::with('host')->where(['id' => base64_decode($id)])->first();
            return view('admin.experience_detail',compact('experience'));
        }
    } 
    /*
    * View Experience Approval List @RT
    */
     public function getExperienceApprovalList(Request $request){
        $page_title = 'List of Experiences';
        if ($request->ajax()) {
           
            
         $data = ExperienceAprroval::with('host','user')->where(['status' => 3])->get(); 
            return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('action', function($row){
                    $btn = '<a   href="'. route('admin-view-experience-approval-detail',['id' => base64_encode($row->id)]) .'" class="btn btn-success btn-sm">View</a>';
                    return $btn;
                })
                ->rawColumns(['action'])
                ->make(true);
        }

        return view('admin.experience_request_list',compact('page_title'));
    } 
  
    /*
    * View Aprroval Experience Detail
    */
      public function viewExperienceApprovalDetail(Request $request,$id){
        if($request->isMethod('get')){
             $experienceAprroval = ExperienceAprroval::with('user','experience_meta_detail')->where(['id' => base64_decode($id)])->first();
             $experience = Experience::with('user','experience_meta_detail')->where(['id' => base64_decode($id)])->first();  
            return view('admin.experience_approval_detail',compact('experience','experienceAprroval'));
        } 
    } 
    /*
    * View Host List
    */
     public function getHostList(Request $request){
        $page_title = 'List of Host';
        if ($request->ajax()) {
            $data = User::with('scout')->where(['user_role' => 4])->get();
            return Datatables::of($data)
                ->make(true);
        }

       return view('admin.host_list',compact('page_title'));
    } 
    /*
    * View Host @RT
    */
     public function getHost(Request $request){
        $page_title = 'Get Of Host';
        if ($request->ajax()) {
            if($request->id){
            $data = User::with(['scout','user_meta'])->where(['user_role' => 4,'id'=>base64_decode($request->id)])->first(); 
            $languages =  DB::table('languages')->get();  

           $html=  view('admin.view_host_model', compact('data','languages'))->render(); 
            return response()->json([
                "message"       => 'Monthly option getting successfully.',
                "isSucceeded"   => TRUE,
                "html"          => $html
            ]);
        }
        }
      
    }  
     /*
    * Update experience status
    */
     public function updateExperienceStatus(Request $request){
        $page_title = 'List Of Host';
        if($request->isMethod('post')){
            $msg = '';
             $data['experience'] = Experience::with('user')->where(['id' => base64_decode($request->id)])->first();
            if($request->status == 1){
                $msg = 'Experience Approved successfully';
                $data['subject'] = "EXPERIENCE APPROVED";
                $data['status'] = "Approved";
                $sendEmail = Mail::to([$data['experience']['user']['email']])->send( new SendMail($data,'experience_approved'));
                saveNotification('experince_approve',array('scout' =>$data['experience']['user']['id']),base64_decode($request->id)); 
            }else{
                $msg = 'Experience Rejected successfully';
                $data['subject'] = "EXPERIENCE REJECTED";
                $data['status'] = "Rejected";
                $data['reason'] = $request->reason;
                $sendEmail = Mail::to([$data['experience']['user']['email']])->send( new SendMail($data,'experience_rejected'));
                saveNotification('experince_reject',array('scout' =>$data['experience']['user']['id']),base64_decode($request->id));  
            }
            $data = Experience::where(['id' => base64_decode($request->id)])->update(['status' => $request->status]);
            if($data){

                return response()->json([
                    'isSucceeded' => TRUE,
                    'message' => $msg
                ]);
            }else{
                return response()->json([
                    'isSucceeded' => FALSE,
                    'message' => 'Opps there is a problem related to this experience'
                ]);
            }
        }
    }
     
     /*
    * Approve experience status
    */
     public function updateExperienceAppStatus(Request $request){
        $page_title = 'List Of Host';
        if($request->isMethod('post')){
            $msg = '';
             $data['experience'] = Experience::with('user')->where(['id' => base64_decode($request->id)])->first();
            if($request->status == 1){
                $msg = 'Experience Approved successfully';
                $data['subject'] = "EXPERIENCE RE APPROVED";
                $data['status'] = "Approved";
                // $sendEmail = Mail::to([$data['experience']['user']['email']])->send( new SendMail($data,'experience_re_approved'));
                //saveNotification('experince_approve',array('scout' =>$data['experience']['user']['id']),base64_decode($request->id));  

          try {
                 DB::transaction(function () use($request) {
                  $approvalData = ExperienceAprroval::where(['id' => base64_decode($request->id)])->first();
                  $appMetaData = ExperienceMetaAprroval::where(['experience_id' => base64_decode($request->id)])->first();
                  $appAdditionalPriceData = ExpAdditionalPriceApp::where(['experience_id' => base64_decode($request->id)])->get();
                  $appExpCategoryRelationData = ExpCategoryRelationApp::where(['experience_id' => base64_decode($request->id)])->get();
                  $appFeatureRelationData = ExpFeatureRelationApp::where(['experience_id' => base64_decode($request->id)])->get();
                 if(!empty($approvalData) ){

                      $approvalDataArray=$approvalData->toArray();
                 if(count($approvalDataArray) > 0 ){
                      $approvalDataArray['status']=1;
                      unset($approvalDataArray['experience_feature_image'])  ;
                      if($approvalDataArray['experience_latitude'] == '0.00000000' && $approvalDataArray['experience_longitude'] == '0.00000000'){
                         unset($approvalDataArray['experience_latitude'])  ;
                         unset($approvalDataArray['experience_longitude'])  ;
                      }
                     DB::table('experiences')->updateOrInsert(['id'=>base64_decode($request->id)],array_filter($approvalDataArray)); 
                  } 
                  } 
                  if(!empty($appMetaData) ){

                     $appMetaArray=$appMetaData->toArray();
                  if(count($appMetaArray)> 0 ){
                      unset($appMetaArray['id'])  ;
                      if($appMetaArray['meet_latitude'] == '0.00000000' && $appMetaArray['meet_longitude'] == '0.00000000'){
                         unset($appMetaArray['meet_latitude'])  ;
                         unset($appMetaArray['meet_longitude'])  ;
                      }
                     DB::table('experience_metas')->updateOrInsert(['experience_id'=>base64_decode($request->id)],array_filter($appMetaArray)); 
                  } 
                  } 
                  if(!empty($appAdditionalPriceData)){

                     $appAdditionalPriceArray=$appAdditionalPriceData->toArray();
                  if(count($appAdditionalPriceArray)> 0 ){
                      DB::table('experience_additional_prices')->where('experience_id', '=', base64_decode($request->id))->delete();
                      foreach($appAdditionalPriceArray as $AdditionalPrice)
                        {
                            unset($AdditionalPrice['id'])  ; 
                             $experienceImages = DB::table('experience_additional_prices')->insert($AdditionalPrice);

                        }
                  }
                  }
               
                  if(!empty($appExpCategoryRelationData)){
                    
                    $appExpCategoryRelationArray=$appExpCategoryRelationData->toArray();
                  if(count($appExpCategoryRelationArray)> 0 ){
                     DB::table('experience_category_relations')->where('experience_id', '=', base64_decode($request->id))->delete();
                      foreach($appExpCategoryRelationArray as $cat)
                        {   unset($cat['id'])  ;
                            $experienceImages = DB::table('experience_category_relations')->insert($cat);
                        } 
                  } 
                  } 
                  if(!empty($appFeatureRelationData)){
                      $appFeatureRelationArray=$appFeatureRelationData->toArray();
                  if(count($appFeatureRelationArray)> 0 ){
                       DB::table('experience_feature_relations')->where('experience_id', '=', base64_decode($request->id))->delete();
                         foreach($appFeatureRelationArray as $feat)
                        {    unset($feat['id'])  ;
                            $experienceFeatureRelations = DB::table('experience_feature_relations')->insert($feat);
                        }
                  }  
                  }  
                 });
              $data2 = ExperienceAprroval::where(['id' => base64_decode($request->id)])->delete();
               //$sendEmail = Mail::to([$data['experience']['user']['email']])->send( new SendMail($data,'experience_rejected'));
                saveNotification('experince_reapproval_success',array('scout' =>$data['experience']['user']['id']),base64_decode($request->id));
              return response()->json([
                    'isSucceeded' => TRUE,
                    'message'=>"Experience Approved successfully.",
                    "experience_id"=>$request->exp_id,
                ]);

            } catch (\Exception $e) {
                echo $e->getMessage();
                die;
                return response()->json([
                        'isSucceeded' => FALSE,
                        'message'=>$e->getMessage()
                    ]);
            }
            }else{
                $msg = 'Experience Rejected successfully';
                $data['subject'] = "EXPERIENCE REJECTED";
                $data['status'] = "Rejected";
                $data['reason'] = $request->reason;
                $sendEmail = Mail::to([$data['experience']['user']['email']])->send( new SendMail($data,'experience_re_rejected'));
                 saveNotification('experince_reapproval_failed',array('scout' =>$data['experience']['user']['id']),base64_decode($request->id));
               $data = ExperienceAprroval::where(['id' => base64_decode($request->id)])->delete();
            }
             if($data){

                return response()->json([
                    'isSucceeded' => TRUE,
                    'message' => $msg
                ]);
            }else{
                return response()->json([
                    'isSucceeded' => FALSE,
                    'message' => 'Opps there is a problem related to this experience'
                ]);
            }
        }
    }  
    /*
    * View Experience Detail
    */
    public function ManageGoogleTags(Request $request){
        if($request->isMethod('get')){
            $page_title="Manage Google Analytics";
            return view('admin.google_tags',compact('page_title'));
        }
    }  
    /*
    * View Setting Page
    */ 
   public function ManageSettings(Request $request){
        if($request->isMethod('get')){
         //  $googleData = Setting::where(['id' =>1])->first();
          $googleData =[];
            return view('admin.settings',compact('googleData'));
        }
    } 
    /*
    * View Experience Detail
    */
    public function updateManageSettings(Request $request){
        if($request->isMethod('post')){

            $data = $request->except(['_token']);
            $result = saveMetaData($data);
            if($result)
            {
            return redirect()->back()->with("success","Google Tags successfully updated.");
            }
      
      }
  }

    /*
    *  Booking list view page
    */

    public function bookings(Request $request)
    {
          $page_title="Booking List";
          return view('admin.booking_list',compact('page_title'));
    } 
    /*
    *  Booking list view page
    */

    public function bookingList(Request $request)
    {

        if ($request->ajax()) { 
            $data = ExperienceBooking::with(['scout','traveler','experience'])->get();
             return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('action', function($row){
                    $btn = '<a href="'. route('admin-booked-experience-detail',['id' => base64_encode($row->id)]) .'" class="btn btn-success btn-sm">View</a>';
                    return $btn;
                })
                ->rawColumns(['action'])
                ->make(true);
        }  
    } 
   
     /*
    * Manage Booked Experiences by Admin
    */
    public function bookedExperienceDetail(Request $request,$id){
          $page_title = 'Booking Information';
        $experience = ExperienceBooking::with(['scout','traveler','experience'])->where('id',base64_decode($id))->first();

      // /  echo "<pre>"; print_r($experience->toArray());
            return view('admin.booking_detail',compact('experience','page_title'));
        
    } 

     /**
    * Generate the PDF  By Admin   @RT
    */
     public function pdfcreate(Request $request,$id)
    {
        $bookingDetail = ExperienceBooking::with(['traveler','experience','experience_meta_detail'])->where('id',base64_decode($id))->first();
        $data = ['bookingDetail' => $bookingDetail]; 
        $pdf = PDF::loadView('scout.invoice_pdf', $data);
        return $pdf->download('localsfromzero.pdf');
    }
public function exportCsv(Request $request,$id)
    {
         $fileName = 'Scout.csv';
         $languages =  DB::table('languages')->get(); 
         $scoutDatas = User::with(['user_meta'])->where(['id' => base64_decode($id)])->get();  

         $languagearrya=[];
 
          foreach($languages as $language)  {
             $languagearrya[$language->id]=$language->name; 
          } 
         
          $ability_skills_lable= [
                                 'personal_travel ' => 'Using online resources for personal travel needs',
                                 'social_media ' => 'Managing social media accounts',
                                 'reservation_systems ' => 'Using reservation systems' ,
                                 'managing_reservation ' => 'Managing reservation systems' ,
                                 'mobile_travel_app ' => 'Using mobile travel applications' ,
                                 'developing_mobile_travel_app ' => 'Developing mobile travel applications' ,
                                 'photography ' => 'Photography' ,
                                 'video_production ' => 'Video production',
                                 '360_pictures_video_production ' => '360 pictures and video production' ,
                                 'personal_deadline_management ' => 'Personal time/deadliness management' ,
                                 'team_management ' =>'Team management'];
          $ability_skills_val= [
                                 '1' => 'No skills',
                                 '2' => 'Some skills',
                                 '3' => 'Average skills' ,
                                 '4' => 'Above average skills' ,
                                 '5' => ' Professional level skills' ];
            $headers = array(
                "Content-type"        => "text/csv",
                "Content-Disposition" => "attachment; filename=$fileName",
                "Pragma"              => "no-cache",
                "Cache-Control"       => "must-revalidate, post-check=0, pre-check=0",
                "Expires"             => "0"
            );

            $columns = array('Full Name', 'Email', 'Phone', 'Date of birth', 'Status', 'Gender', 'Address', 'Place of residence','English level','Primary language', 'Other languages','Why would you join us?','Briefly describe two unique experiences','What are your personal and/or professional areas of interest?','Using online resources for personal travel needs','Managing social media accounts','Using reservation systems','Managing reservation systems','Using mobile travel applications','Developing mobile travel applications','Photography','Video production','360 pictures and video production','Personal time/deadliness management','Team management','What describes Scout best (Group 1)','What describes Scout best (Group 2)');
            
            $callback = function() use($scoutDatas, $columns, $languagearrya,$ability_skills_val,$ability_skills_lable) {
            
                $file = fopen('php://output', 'w'); 
                fputcsv($file, $columns);
                 $user_primary_language='';
                 $user_other_languages='';
  
                foreach ($scoutDatas as $scoutData) {
                    $ability_skills = unserialize($scoutData->user_meta->ability_skills);
                    if($scoutData->status == 0 || $scoutData->status ==2 ){
                        $status= "Inactive";
                    }else if($scoutData->status == 1){
                      $status ="Active";
                     } 
                     if(isset($scoutData) && !empty($scoutData->user_primary_language)){
                     $user_primary_language = $languagearrya[$scoutData->user_primary_language];  
                     }
                     $get_other_language = [];
                      if(isset($scoutData) && !empty($scoutData->user_other_languages)){
                        $get_other_language = explode(",",$scoutData->user_other_languages);
                      }                        
                    foreach($get_other_language as $language){
                       $user_other_languages .= $languagearrya[$language] .' ,';
                      }  
                     
                    $row['Full Name']  =  $scoutData->user_fname .' '. $scoutData->user_lname;
                    $row['Email']    = $scoutData->email;
                    $row['Phone']    = formatPhoneNo($scoutData->user_mobile,$scoutData->user_mobile_code);
                    $row['Date of birth']    =  $scoutData->user_meta->dob;
                    $row['Status']  = $status;
                    $row['Gender']  = ($scoutData->user_meta->gender == 1?'Male':'Female');
                    $row['Address']  = ($scoutData->user_address ? $scoutData->user_address : 'Not available');
                    $row['Place of residence']  = ($scoutData->user_meta->user_residence ? $scoutData->user_meta->user_residence : 'Not available');
                    $row['English level']  = $scoutData->user_english_level;
                    $row['Primary language']  = $user_primary_language;
                    $row['Other languages']  = $user_other_languages;
                    $row['Why would you join us?']  = $scoutData->user_meta->why_join_us;
                    $row['Briefly describe two unique experiences']  = $scoutData->user_meta->unique_experiences;
                    $row['What are your personal and/or professional areas of interest?']  = $scoutData->user_meta->user_interest;
                    foreach($ability_skills as $key=>$val){
                      $row[$ability_skills_lable[$key]]=  $ability_skills_val[$val];
                     }
                     
                    $row['What describes Scout best (Group 1)']  = $scoutData->user_meta->describes_you_best;
                    $row['What describes Scout best (Group 2)']  = $scoutData->user_meta->describes_you_best_2;
                     fputcsv($file,  $row );
                }
                fclose($file);
            }; 

           return response()->stream($callback, 200, $headers);
        }
    public function addCoordinatorsForm(Request $request,$id=null){
            $page_title = 'Manage Coordinators';
        if($request->isMethod('get') && $id==null){
            return view('admin.add_coordinator',compact('page_title'));
        }else{
             $userData = User::where(['id' => base64_decode($id)])->first(); 
            $user_meta = UserMeta::where('user_id',$userData->id)->first();
             $userData->social_link = $user_meta->social_links; 
             $userData->current_status = $user_meta->current_status; 
            return view('admin.add_coordinator',compact('userData','page_title'));
        }
    }
    public function getCoordinatorList(Request $request){
       
        if($request->ajax()){
         $data = User::with('user_meta')->where(['user_role' => 5])->get();
            return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('action', function($row){
                    $btn = '<a href="'.route('admin-update-coordinators',['id'=>base64_encode($row->id)]).'" class="edit btn btn-success btn-sm action-add-edit-btn">Edit</a> <a href="javascript:void(0)" coordinator-id="'.base64_encode($row->id).'" class="delete btn btn-danger btn-sm action-coordinator-delete-btn">Delete</a>';
                    return $btn;
                })
                ->rawColumns(['action'])
                ->make(true);        
    
        }
    } 
    public function createUpdateCoordinators(Request $request)
    {
         $data = $request->except(['_token','user_id','current_status','social']);
         if($request->filled('user_id'))
         {
           $user = User::find($request['user_id']);
            $user_image=$user->user_image;
       }
      
        $file_path="users/coordinator";
        $data = array(
                'user_role' => 5,
                'status' => 1
            ); 
         if($request->hasFile('user_image')) {   
                $file = $request->user_image;              
                $user_image = $this->profileImageUpload($file,$file_path);
                $thumbnailpic = "/profile-thumb-".$user_image;
                createThumbnail($file_path,$user_image,$thumbnailpic,$width=160,$height=160);

        }
        $userdata=[
            "user_role"=>5,
            "status"=>1,
            "user_fname"=>$request->user_fname,
            "email"=>"coordinator-".time()."gmail.com",
            "user_lname"=>$request->user_lname,
            "user_mobile"=>rand(000,999).time(),
            "user_image"=>$user_image,
            "is_scout"=>$request->is_scout,
            "about_me"=>$request->about_me,
            "user_image_path"=>$file_path
        ];
        $userMeta=[
            "current_status"=>$request->current_status,
            "social_links"=>json_encode($request->social),
        ];
        $user = User::updateOrCreate(['id'=>$request->user_id],$userdata);
        $userMeta= UserMeta::updateOrCreate(['user_id'=>$user->id],$userMeta);
        if($user)
        {
             return redirect()->route('admin-edit-about-us')->with("success","Coordinator Saved");
        }
    }

    public function deleteCoordinatorById(Request $request)
    {
       if($request->isMethod('post')){
            if(!empty($request->coordinator_id)){
                $del = user::where(['id' => base64_decode($request->coordinator_id)])->forceDelete();
                if($del){
                return response()->json([
                    "message"       => 'Coordinator Deleted successfully.',
                    "isSucceeded"   => TRUE,
                ]);
            }else{
                return response()->json([
                    "message"       => 'Something went wrong.',
                    "isSucceeded"   => FALSE,
                ]);

            }  
            }
        }
    }

    public function profileImageUpload($file,$path){

        $org_name = $file->getClientOriginalName();
        
        $org_name = pathinfo($org_name, PATHINFO_FILENAME);
        
        $extension = $file->getClientOriginalExtension();

        $file_name = $org_name.'_'.uniqid().'.'.$extension;

        $file->move( public_path().'/'.$path, $file_name);

        return $file_name;      
    }
       /** Ckeditor Image Upload Function */
    public function upload(Request $request)
    {
        if($request->hasFile('upload')) {
            $originName = $request->file('upload')->getClientOriginalName();
            $fileName = pathinfo($originName, PATHINFO_FILENAME);
            $extension = $request->file('upload')->getClientOriginalExtension();
            $fileName = $fileName.'_'.time().'.'.$extension;
        
            $request->file('upload')->move(public_path().'/pages/blogs', $fileName);
   
            $CKEditorFuncNum = $request->input('CKEditorFuncNum');
            $url = asset('pages/blogs/'.$fileName); 
            $msg = 'Image uploaded successfully'; 
            $response = "<script>window.parent.CKEDITOR.tools.callFunction($CKEditorFuncNum, '$url', '$msg')</script>";
               
            @header('Content-type: text/html; charset=utf-8'); 
            echo $response;
        }
    }
    /**
    * Split Booking Payment after 48 hours
    */
    public function sendPaymentStudentScout(Request $request)
    {
        $user=User::with(['user_meta'])->where(['id'=>base64_decode($request->scout_id)])->first();
        $user_meta = UserMeta::where('user_id',base64_decode($request->scout_id))->first();
        if($request->payment_type==2)
        {
            $paidAmount= $request->partial_amount;
        }else{
             $paidAmount= $user_balance;
        }

        $user_balance=$user->user_meta->student_balance;
        $remainBalance= $user_balance-$paidAmount;
        if($user_balance!==null && (float)$paidAmount<=(float)$user_balance)
        {
            
           
            $stripeArray=[
              'amount' => (float)$paidAmount*100,
              'currency' => 'eur',
              'destination' => $user->user_stripe_account_id,
              'transfer_group' =>'Order-'.$user->id,
            ];
            $currentDateTime1= new DateTime();
            $currentDateTime=$currentDateTime1->format('Y-m-d H:i:s');
            $stripe = new \Stripe\StripeClient(config('services.stripe.secret'));
            $scoutresult =$stripe->transfers->create($stripeArray);
                    $scoutTransactionId=$scoutresult->id;
          
                   
                $insertPayment=[
                    [
                        "scout_id"=>$user->id,
                        "payment_type"=>$request->payment_type,
                        "amount"=> (float)$paidAmount,
                        "status"=>1,
                        "transaction_id"=>$scoutTransactionId,
                        "created_at"=>$currentDateTime,
                        "updated_at"=>$currentDateTime,
                    ],
                    
                ];
                StudentScoutPayment::insert($insertPayment);
                $user_meta->update(['student_balance'=>$remainBalance]);
                  return redirect()->route('admin-student-scouts')->with("success","Payment send");
            }
            else{
                 return redirect()->route('admin-student-scouts')->with("failure","Partially amount must be less than or equal to student balance.");
            }
        }
  
}
