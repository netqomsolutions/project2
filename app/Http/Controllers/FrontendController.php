<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use App\Mail\ContactUs;
use App\Mail\SendMail;
use Illuminate\Http\Request;
use App\ExperienceCategory;
use App\Experience;
use App\ExperienceAdditionalPrice;
use App\ExperienceBooking;
use App\ExperienceMeta;
use App\ExperienceSchedule;
use App\ReviewRating;
use App\UserMeta;
use App\BlogCategory;
use App\Blog;
use App\User;
use App\AboutUs;
use App\Language;
use App\HowWork;
use App\HomePage;
use App\ExperienceFeature;
use App\ContentManagementSystem;
use App\Country;
use App\State;
use App\City;
use App\Partner;
use App\Testimonial;
use App\Wishlist;
use App\Payment;
use DataTables;
use DB;
use View;
use Carbon\Carbon;
use Carbon\CarbonInterval;
use Carbon\CarbonPeriod;
use DateTime;
use DatePeriod;
use DateInterval;
use Session;
use App\RealNotification;
class FrontendController extends Controller
{
    /**
     * Homepage methods below
    */    
    public function index()
    { 
        $user = Auth::user();
        $UserwishList = array(); 
        if(Auth::user()){
          $UserwishList  = Wishlist::where(['user_id' => $user->id])->get(); 
        } 
        $page_title = 'Homepage';
        $page_description = 'Some description for the page';        
        $experienceCategories = ExperienceCategory::all();   // getting exp_categories
        $reviewRatingsForExperience = ReviewRating::where(['review_type' => 0])->get();   //getting experience that are rated by traveler
        $listOfScouts = User::withCount('review_ratings')->where(['user_role' => 3, 'status' => 1 ])->paginate(3);   //getting scouts   
          
        $experiences = Experience::withCount('review_ratings')->where(['status' => 1])->limit(6)->get();   //getting experiences 
        $listOfCoordinators = User::withCount('review_ratings')->with('user_meta')->where(['user_role' => 5])->get();   //getting coordinators
        $homePageData = HomePage::find(1);
        $connect = ContentManagementSystem::find(6);
        $testimonials = Testimonial::all();
        if(!empty($connect))
        {
        $data = json_decode($connect->page_content,true);
        $connect->section_one_title = $data['section_one_title'];
        $connect->section_two_title = $data['section_two_title'];
        $connect->section_three_title = $data['section_three_title'];
        $connect->section_one_description = $data['section_one_description'];
        $connect->section_two_description = $data['section_two_description'];
        $connect->section_three_description = $data['section_three_description'];
        }
        
        return view('front_end.home_page', compact('page_title', 'page_description' , 'experienceCategories','reviewRatingsForExperience', 'listOfScouts' , 'experiences', 'listOfCoordinators','homePageData','connect','testimonials','user','UserwishList'));
    }

    /*
    * Check user mobile exist or not
    */
    public function getAllScouts(Request $request){
        if($request->ajax()){
             $listOfScouts = User::withCount('review_ratings')->where(['user_role' => 3, 'status' => 1 ])->paginate(3); 
            $html = View::make('front_end.scout-list',compact('listOfScouts'))->render();
            if(count($listOfScouts)){
                 return response()->json([
                    "message"       => 'Scouts get successfully.',
                    "isSucceeded"   => TRUE,
                    "data"          => $html,
                ]);
            }else{
                return response()->json([
                    "message"       => 'There is no Scouts.',
                    "isSucceeded"   => FALSE,
                    "data"          => $html,
                    "locations"     => ""
                ]);

            }
        }
    } 
    /*
    * Check user mobile exist or not
    */
    public function checkMobileExistence(Request $request){
        if($request->isMethod('post')){  
            $phoneCheck = User::where(['user_mobile' => $request->user_mobile])->first();
            if ( $phoneCheck && $phoneCheck->user_mobile == $request->user_mobile ) { echo 'false';  } else { echo 'true'; }
        }
    }

    /*
    * About us Page Display Function
    */
     public function aboutUs(){
            $page_title = 'About Us Page';
            $page_description = 'Some description for the page';
            $aboutUs = AboutUs::find(1);
            $testimonials = Testimonial::all();
             $listOfCoordinators = User::withCount('review_ratings')->with('user_meta')->where(['user_role' => 5])->get();   //getting coordinators
            return view('front_end.about_us',compact('page_title', 'page_description','testimonials','aboutUs','listOfCoordinators'));
     }

    /*
    * privacy policy Page Display Function
    */    
    public function privacyPolicy(){
        $page_title = 'Privacy Policy';
        $page_description = 'Some description for the page';
        $privacyPolicy = ContentManagementSystem::find(1);
        return view('front_end.privacy_policy',compact('page_title', 'page_description','privacyPolicy'));
    } 
    /*
    * Cancellation policy Page Display Function
    */    
    public function cancellationPolicy(){
        $page_title = 'Cancellation Policy';
        $page_description = 'Some description for the page';
        $privacyPolicy = ContentManagementSystem::find(8);
        return view('front_end.cancellation_policy',compact('page_title', 'page_description','privacyPolicy'));
    }

     /*
    * Terms & Condition Page Display Function
    */    
    public function termsConditions(){
        $page_title = 'Terms & Conditions';
        $page_description = 'Some description for the page';
        $pageData = ContentManagementSystem::find(2);
        return view('front_end.terms_condition',compact('page_title', 'page_description','pageData'));
    }


    /*
    * How it works Page Display Function
    */    
    public function howitWorks(){
        $page_title = 'How it Works';
        $page_description = 'Some description for the page';
        $pageData = ContentManagementSystem::find(5);
        $data = json_decode($pageData->page_content,true);
        $pageData['tab_one_image'] = $data['tab_one_image'];
        $pageData['tab_one_content'] = $data['tab_one_content'];
        $pageData['tab_two_content'] = $data['tab_two_content'];
        $pageData['tab_three_content'] = $data['tab_three_content'];
        $howScout = HowWork::where(['type' => 'scout_tab'])->get();      
        $howExperience = HowWork::where(['type' => 'experience_tab'])->get();      
        return view('front_end.how_it_works',compact('pageData','page_title', 'page_description','howScout','howExperience'));
    }

    /*
    * Contact us Page Display Function
    */    
    public function contactUs(){
        $page_title = 'Contact Us';
        $page_description = 'Some description for the page';
        $socialLinks=ContentManagementSystem::find(7);
        $data = json_decode($socialLinks->page_content,true);
        $socialLinks->facebook=$data['facebook'];
        $socialLinks->instagram=$data['instagram'];
        $socialLinks->twitter=$data['twitter'];
        $socialLinks->linkedin=$data['linkedin'];
        $contactUs = ContentManagementSystem::where(['id' => 4])->first();
        $conData = json_decode($contactUs->page_content,true);
        $contactUs->email = $conData['email'];
        $contactUs->address = $conData['address'];
        $contactUs->phone = $conData['phone'];
        $contactUs->address_line_2 = $conData['address_line_2'];
        $contactUs->address_link = $conData['address_link'];
         $getCountryCode =  DB::table('country_codes')->select("iso","phonecode")->get(); 
        return view('front_end.contact_us',compact('contactUs','page_title', 'page_description','socialLinks','getCountryCode'));
    }

    /*
    * Contact us Page Display Function
    */
    public function contactUsPost(Request $request){
        if($request->isMethod('post')){

            $data = $request->except(['_token']);
            $sendEmail = Mail::to('info@localsfromzero.org')->send( new ContactUs($data));
            $sendEmail = Mail::to($data['email'])->send( new SendMail($data,'contact_us_to_user'));
            if(count(Mail::failures()) > 0){
                return response()->json([
                    "message"       => 'There is problem to contact our team now,please try after some time',
                    "isSucceeded"   => FALSE,
                ]);
            }else{
                return response()->json([
                    "message"       => 'Thanks for contacting us!.',
                    "isSucceeded"   => TRUE,
                ]);
            }
        }
    }

     /*
    * Blog List Page Display Function
    */    
    public function blogList(Request $request){
        $conditions=[];
    if(isset($request->cat) && $request->cat!='')
    {
        $id= $this->getBlogCategoryId($request->cat);
          $conditions[]=['blog_category_id','=', $id];
    }
    if(isset($request->search) && $request->search!=''){
        $conditions[]=['blog_title','like', '%'.$request->search.'%'];
    } 
     $blogsData = Blog::with('blogCat')->where($conditions)->get();
    

        $page_title = 'Blogs';
        $page_description = 'Some description for the page';
        $blogCategory = BlogCategory::all();
        
        $pageData = ContentManagementSystem::find(3);
        return view('front_end.blog_list',compact('blogsData','page_title', 'page_description','pageData','blogCategory'));
    }
    public function getBlogCategoryId($slug)
    {
         $blogCategory = BlogCategory::where('slug','=',$slug)->first();
         return $blogCategory->id;
    }

     /*
    * Blog Detail Page Display Function
    */    
    public function blogDetail(Request $request,$id=null){
        $page_title = 'Blogs';
        $page_description = 'Some description for the page';   
        $blog = Blog::where(['id'=>$id])->first();
        $pageData = ContentManagementSystem::find(3);
        return view('front_end.blog_detail',compact('pageData','page_title', 'page_description', 'blog'));
    }

    /*
    * Experience List For Frontend User
    */
    public function experienceList(Request $request)
    {
        if($request->isMethod('get')){
            $experienceCategories = ExperienceCategory::all();
            $languages = Language::all();
            $experienceFeatures = ExperienceFeature::all();
            $experiences = Experience::withCount('review_ratings')->where(['status' => 1])->orderBy('experience_overall_rating', 'DESC')->paginate(8);   //getting experiences
            $reviewRatingsForExperience = ReviewRating::where(['review_type'=>0])->orderBy('id', 'DESC')->limit(2)->get(); //experience that are rated by traveler
            return view('front_end.experience_list',compact('experienceCategories','experienceFeatures','reviewRatingsForExperience','languages'));
        }
    }

    /*
    * Experience filter method
    */
    public function experienceMat(Request $request){
        $user = Auth::user();
        $UserwishList = array(); 
        if(Auth::user()){
          $UserwishList  = Wishlist::where(['user_id' => $user->id])->get(); 
        } 
        if($request->ajax()){
            $conditions[]=['status','=', 1];
            if (isset($request->search_name) && !empty($request->search_name)) {
                $conditions[]=['experience_name','like', '%'.$request->search_name.'%'];
               
            } 
            if (isset($request->search_cat) && !empty($request->search_cat)) {
                $conditions[]=['experience_category_id','=', $request->search_cat];
            }
            if (isset($request->minimumPrice)) {
                $request->minimumPrice =  str_replace( ',', '', $request->minimumPrice );
                $conditions[]=['experience_low_price','>=', $request->minimumPrice];
            }
            if (isset($request->maximumPrice)) {
                $request->maximumPrice =  str_replace( ',', '', $request->maximumPrice );
                $conditions[]=['experience_high_price','<=', $request->maximumPrice];
            }
            if (isset($request->minimumDuration)) {
                $request->minimumDuration =  str_replace( ',', '', $request->minimumDuration );
                $conditions[]=['experience_duration','>=', $request->minimumDuration];
            }
            if (isset($request->maximumDuration)) {
                $request->maximumDuration =  str_replace( ',', '', $request->maximumDuration );
                $conditions[]=['experience_duration','<=', $request->maximumDuration];
            } 
            if ($request->filled('search_address') && $request->filled('search_lat') && $request->filled('search_lng')) {   
               // $conditions[]=['experience_lname','like', '%'.$request->search_address.'%'];
                $expLat=$request->search_lat;
                $expLng=$request->search_lng;
                 $experiences = Experience::selectRaw("id,experience_name,extra_person,experience_low_price,experience_high_price,experience_lname,experience_duration,experience_start_time,user_id,assigned_host,experience_type,experience_additional_person,experience_group_size,experience_price_vailid_for,minimum_participant,maximum_participant,experience_feature_image,experience_overall_rating,experience_booking_in_advance_time,experience_latitude,experience_longitude,6371000 * acos(cos(radians(" . $expLat . ")) * cos(radians(experience_latitude)) * cos(radians(experience_longitude) - radians(" . $expLng . ")) + sin(radians(" .$expLat. ")) * sin(radians(experience_latitude))) AS distance")->orderBy("distance",'asc')->having("distance", "<", "10000")->with(['user','review_ratings'])->where($conditions);

            }else{
          //      DB::enableQueryLog();
            $experiences = Experience::with(['user','review_ratings'])->where($conditions); 
          //  dd(DB::getQueryLog());
           // die;
        }
            
            if(isset($request->other_lang_check)) {
                $experiences = $experiences->whereHas('experience_language_relations',function ($query) use($request) {
                    $query->whereIn('language_id', $request->other_lang_check)->where('type','1');
                });
            }

            if(isset($request->other_lang_check_host)) {
                $experiences = $experiences->orWhereHas('experience_language_relations',function ($query) use($request) {
                    $query->whereIn('language_id', $request->other_lang_check_host)->where('type','2');
                });
            }  

            if(isset($request->exp_feat)){
                $experiences = $experiences->orWhereHas('experience_feature_relations',function ($query) use($request) {
                    $query->whereIn('experience_feature_id', $request->exp_feat);
                });
            }
            if(isset($request->start_time)) {
                $experiences = $experiences->whereIn('experience_start_time',$request->start_time);
            }
            if(isset($request->exp_cat)) {
                $experiences = $experiences->whereHas('experience_category_relation',function ($query) use($request) {
                    $query->whereIn('experience_category_id', $request->exp_cat);
                });
            }
            if($request->sort_by == 1){
                $experiences = $experiences->orderBy('experience_overall_rating', 'DESC');   
            }
            $experiences = $experiences->paginate(8);
           

            $html = View::make('front_end.experience_mat',compact('experiences','UserwishList','user'))->render();
            if(count($experiences)){
                $locations=[];
                foreach ($experiences as $key => $experience)
                {
                  $locations[]=[
                "lat"=> (float)$experience->experience_latitude,
                "lng"=> (float)$experience->experience_longitude,
                "exp_id"=>$experience->id,
                "exp_name"=>$experience->experience_name,
                  ];
                }
                 $locations=json_encode($locations);
                return response()->json([
                    "message"       => 'Experiences get successfully.',
                    "isSucceeded"   => TRUE,
                    "data"          => $html,
                    "locations"     => $locations
                ]);
            }else{
                return response()->json([
                    "message"       => 'There is no experience.',
                    "isSucceeded"   => FALSE,
                    "data"          => $html,
                    "locations"     => ""
                ]);
            }
        }
    }

   
     /*
    * Experience Detail Page Display Function
    */    
    public function experienceDetail(Request $request,$id=null){
        $user = Auth::user();
        $UserwishList = array(); 
        // echo "<pre>"; print_r($user_meta);die;

        if(Auth::user()){
          $UserwishList  = Wishlist::where(['user_id' => $user->id])->get(); 
        } 
        $page_title = 'Experience Detail';
        $page_description = 'Some description for the page';
        $experience = Experience::with('user','review_ratings','experience_images','experience_meta_detail','experience_schedule','experience_additional_price')->where(['id'=>base64_decode($id)])->first(); 
        $user_meta = UserMeta::where('user_id',$experience->user_id)->first();
        $social_link = $user_meta->social_links; 
         
        return view('front_end.experience_detail',compact('page_title', 'page_description','experience','user','UserwishList','id','social_link'));
    }


    /*
    * Scout detail Function
    */
    public function userDetail(Request $request, $id){
         $user = Auth::user();
         $UserwishList = array(); 
        if(Auth::user()){
          $UserwishList  = Wishlist::where(['user_id' => $user->id])->get(); 
        } 
        $userData = User::withCount('review_ratings')->with(['user_languages','scout_review_ratings'])->where(['id' => base64_decode($id)])->first();
        $user_meta = UserMeta::where('user_id',base64_decode($id))->first();
        $social_link = $user_meta->social_links; 

        if($userData->user_role==3 || $userData->is_scout==1){
         $languages =  DB::table('languages')->get();
          $experiences = Experience::with(['user','review_ratings'])->where(['status' => 1, 'user_id' => base64_decode($id)])->orderBy('experience_overall_rating', 'DESC')->paginate(6);   //getting experiences
        return view('front_end.scout_detail',compact('userData','languages','experiences','id','social_link','UserwishList','user'));
        }else{
            return view('front_end.coordinator_detail',compact('userData','social_link'));
        }
    }
    /*
    * Scout Popup detail Function
    */
    public function scoutPopupDetail(Request $request){
        if($request->ajax()){
             $html="";
            $languages =  DB::table('languages')->get();
            $userData = User::withCount('review_ratings')->with(['user_languages'])->where(['id' => base64_decode($request->scout_id)])->first(); 
              
             $user_meta = UserMeta::where('user_id',base64_decode($request->scout_id))->first();
             $social_link = $user_meta->social_links;
            
        if($userData){
             $html = View::make('front_end.scout-popup',compact('userData','languages','user_meta','social_link'))->render();
                 return response()->json([
                    "message"       => 'Popup Detail get successfully.',
                    "isSucceeded"   => TRUE,
                    "data"          => $html,
                ]);
            }else{
                return response()->json([
                    "message"       => 'There is detail.',
                    "isSucceeded"   => FALSE,
                    "data"          => $html,
                    "locations"     => ""
                ]);

            }
        }
        
    }
    /* 
    *show experiences on Scout detail page with pagination 
    */
     public function scoutExperienceMat(Request $request)
    {
         $user = Auth::user();
        $UserwishList = array(); 
        if(Auth::user()){
          $UserwishList  = Wishlist::where(['user_id' => $user->id])->get(); 
        } 
       
        if($request->ajax()){
            $conditions[]=['status','=', 1];
            $conditions[]=['user_id','=', base64_decode($request->user_id)];
            $experiences = Experience::with(['user','review_ratings'])->where($conditions)->paginate(6);
            $html = View::make('front_end.scout-experience-list',compact('experiences','UserwishList','user'))->render();
            if(count($experiences)){
                 return response()->json([
                    "message"       => 'Experiences get successfully.',
                    "isSucceeded"   => TRUE,
                    "data"          => $html,
                ]);
            }else{
                return response()->json([
                    "message"       => 'There is no experience.',
                    "isSucceeded"   => FALSE,
                    "data"          => $html,
                    "locations"     => ""
                ]);

            }
        }
        
    }
    public function AddToWishList(Request $request){
               
            if($request->is_ajax == 1){
                $data = $request->except(['_token','is_ajax']);
                $return  = 0;
                 if(Wishlist::create($data)){
                  $return  = 1;
                 }  
                 return response()->json([ 
                    "isSucceeded"   => $return,
                ]);
            }else{
                $whereArray = array('user_id' => $request->user_id,'experience_id' => $request->experience_id);
                $query = DB::table('wishlists');
                foreach($whereArray as $field => $value) {
                    $query->where($field, $value);
                }   
                  $return  = 0;
                 if($query->delete()){
                  $return  = 1;
                 }  
                 return response()->json([ 
                    "isSucceeded"   => $return,
                ]); 
             } 
    }

    public function experienceDetailByid(Request $request)
    {
        if($request->ajax()){
         $experience = Experience::with('user','review_ratings','experience_images')->where(['id'=>$request->id])->first(); 
         if($experience){
             return response()->json([
                    "message"       => 'Experiences get successfully.',
                    "isSucceeded"   => TRUE,
                    "experience"     => $experience
                ]);
            }else{
                return response()->json([
                    "message"       => 'There is no experience.',
                    "isSucceeded"   => FALSE,
                    "experience"     => ""
                ]);
            }

        }
    }

       public function reviewSubmission(Request $request)
        {
              $user = Auth::user();
              $message = "Please submit your feedback"; 
              $forMCheck = array();
              $forMCheck['experience-form'] ='';
              $forMCheck['scout-form'] =''; 
              $forMCheck['host-form']=''; 
              $forMCheck['is-completed-exp']='0'; 
              $forMCheck['is-completed-host']='0'; 
              $forMCheck['is-completed-scout']='0'; 
              if($request->isMethod('post')){
               $userArr  = array(
                    'status' =>'1', 
                    'rating' =>$request->star_value, 
                    'review' =>$request->review_text, 
                    'user_id' =>$user->id,  
                    'review_type' =>$request->review_for,  
                    'experience_id' =>$request->experience_id,  
                    'user_type' =>$request->user_type,  
                ); 
               $id = DB::table('review_ratings')->insertGetId($userArr);
               if($id){
                    return response()->json([
                        "message"       => 'Review submit successfully',
                        "isSucceeded"   => 1, 
                    ]);
               }else{
                    return response()->json([
                        "message"       => 'oops there may be some error',
                        "isSucceeded"   => 0, 
                    ]);
               }
              }else{
                $expid=''; 

                if($request->isMethod('get')){$expid =  base64_decode($request->get('Eid'));}
                   if(empty($expid)){ return redirect('/'); }
                
                   
                   $isCheck = DB::table('review_ratings')->select('user_type','id')->where(['experience_id'=>$expid,'user_id'=>$user->id])->get()->toArray();
                   $experience =  Experience::select("user_id","assigned_host",'experience_name')->where(['id'=>$expid])->first(); 
                   $expName = $experience['experience_name']; 
                    foreach ($isCheck as $key => $ischeck) {
                        
                        if($ischeck->user_type == 0){ // experience
                             $forMCheck['experience-form'] = "experience-form-isfalse";
                             $forMCheck['is-completed-exp'] = "1";
                        }
                       
                        if($ischeck->user_type == 3){ 
                         // scout
                             $forMCheck['is-completed-scout'] = "1";
                             $forMCheck['scout-form'] = "scout-form-isfalse";
                        }
                         
                        if($ischeck->user_type == 4){ // host
                              
                             $forMCheck['host-form'] = "host-form-isfalse";
                             $forMCheck['is-completed-host'] = "1";
                        }
                        
                        if(!empty($forMCheck['experience-form']) &&  !empty($forMCheck['scout-form'])  && !empty($forMCheck['host-form'])){
                            $message = "Thanks you already submitted your feedback with us"; 
                            $expName = ''; 
                        } 
                       
                    } 
               
                return view('front_end.review_submission',compact("user",'expid','experience','forMCheck','message','expName'));
              }
             
        }
    public function getAllSlotsByExpId(Request $request,$id=null)
    {
        $experience = Experience::with(['user','experience_schedule'=>function($q){
        return $q->whereDate('start_date','>=',date('Y-m-d'))->orderBy('start_date', 'ASC')->orderBy('start_time', 'ASC');
        }])->where(['id'=>base64_decode($id)])->first(); 
        $experince_new =array(); 
         foreach ($experience->experience_schedule->toArray() as $key => $value) {
            if(in_array($value['start_date'], $value)){
              $experince_new[] =$value['start_date'] ;   
            }
         }
        $experince_new = array_unique($experince_new); 
        $alreayBookedSlots=$this->getAlreadyBookedSlots(base64_decode($id));
        return view('front_end.all_slots',compact('experience','alreayBookedSlots','experince_new'));
    }
    public function getBookingSlots(Request $request)
    {
        if($request->ajax())
        { 
            $totalPerson= $request->adultCount+$request->childCount;
            $page=$request->page;
             $expDetail = Experience::where('id',$request->freshExpId)->first();
            $expAdvanceBookingTime =$expDetail->experience_booking_in_advance_time; 
            $expSchedule = ExperienceSchedule::with(['experience'])->where(['experience_id' => $request->freshExpId,'start_date'=>$request->bookingDate])->orderBy('start_time', 'ASC')->get();
            $reqBookingDate= $request->bookingDate;
            $reqdata=$request->all();
            $slotsData= $this->getSlotsPrice($reqdata);
            $alreayBookedSlots= $this->getAlreadyBookedSlots($request->freshExpId);
            if($page=="detail-page")
            {
                $html = View::make('front_end.detail-page-slot-html',compact('expSchedule','alreayBookedSlots','slotsData','totalPerson','reqBookingDate'))->render();
            }
            else{
                $html = View::make('front_end.all-slot-html',compact('expSchedule','alreayBookedSlots','slotsData','totalPerson','reqBookingDate'))->render();
            }
             if(count($expSchedule)){
                $upon=FALSE;
                if($expSchedule[0]->schedule_type==1)
                {
                    $upon=TRUE;
                }
                return response()->json([
                    "message"       => $html,
                    "isSucceeded"   => TRUE, 
                    "count"=>  count($expSchedule),
                    "upon"=>  $upon,
                ]); 
                 
            }
            else{
                
                return response()->json([
                    "message"       => $html,
                    "isSucceeded"   => FALSE, 
                ]);
            }
        }
    }

    public function getAlreadyBookedSlots($exp_id)
    {
        $bookingData= ExperienceBooking::where(['experience_id'=>$exp_id])->orderBy('booking_date')->get();
        return $bookingData;
    }

    public function getSlotsPrice($reqdata)
    {
        $reqdata['totalPerson'] =$reqdata['adultCount']+$reqdata['childCount'];
        $expDetail = Experience::with(['experience_meta_detail'])->where(['id' => $reqdata['freshExpId']])->first();
       
         $totalPerson =$reqdata['adultCount']+$reqdata['childCount'];
         $minimumPerson=$expDetail->minimum_participant; 
         $childDiscount=0;
         $additionalPerson=0;
         $atleastM=$expDetail->experience_additional_person * $minimumPerson;
         if($totalPerson>$minimumPerson)
          {
             $additionalPerson= $totalPerson - $minimumPerson;
           }
         if($expDetail->experience_type == 1){ 
               
              $minimumPrice=$totalPerson*$expDetail->experience_additional_person;
                
         }else{
            if($totalPerson>$minimumPerson)
                    {
                    $addDetail=  ExperienceAdditionalPrice::where(['experience_id'=>$reqdata['freshExpId'], 'additional_person'=>$totalPerson])->first();

                       $minimumPrice= $addDetail->additional_price;
                  }else{
                     $addDetail=  ExperienceAdditionalPrice::where(['experience_id'=>$reqdata['freshExpId']])->first();
                     $minimumPrice= $addDetail->additional_price;
                  }
         }

        if($expDetail->experience_meta_detail->is_children_discount_active==1 && $reqdata['childCount']!=0)
            {
                $children_discount_1=$expDetail->experience_meta_detail->children_discount_1;
                if($expDetail->experience_type == 2)
                {
                    if($totalPerson>$minimumPerson)
                    {
                        $childAmount=$reqdata['childCount']*($minimumPrice/$totalPerson);
                    }else{
                        $childAmount=$reqdata['childCount']*($minimumPrice/$minimumPerson);
                    }
                    
                      $childDiscount = ($children_discount_1 / 100 ) * $childAmount; 

                }else{
                 $childAmount=$reqdata['childCount']*$expDetail->experience_additional_person;
                $childDiscount = ($children_discount_1 / 100 ) * $childAmount;
            }
        }
      $totalAmt=$minimumPrice-$childDiscount; 
 
        $totalwithoutDiscount=$minimumPrice;  
       if($totalAmt<=$atleastM)
        {
          $totalAmount=  $atleastM;
          $totalwithoutDiscount=$atleastM; 
          $minimum_pay=TRUE;
        }else{
             $totalAmount=  $totalAmt;
              $minimum_pay=FALSE;
            
        }
        
        return [
            'totalAmount'=>$totalAmount,
            'totalwithoutDiscount'=>$totalwithoutDiscount,
            'childDiscount'=>$childDiscount,
            'totalWithDiscount'=>$totalAmt,
            'minimum_pay'=>$minimum_pay
        ];      
    }
    /**
    * Payment page foor booked experience
    */
    public function paymentPage(Request $request){
        if($request->isMethod('get')){
             if(Session::has('cart')){
                $session = $request->session()->get('cart')[0];
                $experience= Experience::with(['experience_meta_detail'])->where(['id'=>$session['experience_id']])->first();
                 return view('front_end.paymentPage',compact('experience','session'));
             }else{
                return redirect('experience-list');
             }
           
        }
    }
    public function getExperiencePayment(Request $request){
        if(Session::has('cart')){
             $session = $request->session()->get('cart')[0];
             $data=$request->all();
        $data['freshExpId']=$session['experience_id'];
        $experience= Experience::with(['experience_meta_detail'])->where(['id'=>$session['experience_id']])->first();
         $slotsData= $this->getSlotsPrice($data);
         $session['discount'] = $slotsData['childDiscount'];
        $session['total_price'] =  $slotsData['totalwithoutDiscount'];
        $session['amount'] =  $slotsData['totalAmount'];
        $session['minimum_pay'] =  $slotsData['minimum_pay'];
        $session['totalWithDiscount'] =  $slotsData['totalWithDiscount'];
        $session['adults'] = $request->adultCount;
        $session['children'] = $request->childCount;
        $session['infants'] = $request->infantCount;
        if(Session::has('cart')){
                Session::forget('cart');
            }
          Session::push('cart', $session);
          $totalPerson=$session['adults']+$session['children'];
          $htmlCre="";
           if($experience->experience_type==1){
            $experience_low_price=   number_format($experience->experience_low_price)  ;
           }
          else{
             $experience_low_price= $experience->minimum_participant * $experience->experience_additional_person;
          } 
          $htmlCre .='<h4>Price details</h4>
                    <p>Number of guests <span>'.$totalPerson.'</span></p>
                    <p>Discount <span>€ '.number_format($session['discount']).'</span></p>
                    <p><b>Total</b><span>'. (($session['amount']<$session['total_price'])? '<b class="total-price-overline">€'.number_format($session['total_price']).'</b>' : '') . ' <b>€ '.number_format($session['amount']).'</b></span></p>'.(($experience->experience_meta_detail->is_children_discount_active==1)? '<p class="align-items-start"><b style="color:#ff4486;">Note:</b>Discount for children only applies above the minimum price (€'.number_format($experience_low_price).') per experience.</p>' : '');

         if(Session::has('cart')){
                return response()->json([
                    "message"       => $htmlCre,
                    "isSucceeded"   => TRUE,
                    "data"          => [],
                ]);
            }else{
                return response()->json([
                    "message"       => '',
                    "isSucceeded"   => FALSE,
                    "data"          => []
                ]);
            }
         
      
    }else{
        return redirect('experience-list');
    }
    }

    public function createExperienceBooking(Request $request)
    {
        if(Auth::check())
        {
             $expSchedule = ExperienceSchedule::with(['experience'])->where(['id' => base64_decode( $request->slot_id)])->first();
             $alreayBookedSlots= $this->getAlreadyBookedSlots($expSchedule->experience_id)->toArray();
             
             $alreadyBookedSlot=[];
             if(!empty($alreayBookedSlots))
            {
            $alreadyBookedSlot=array_column($alreayBookedSlots,'slot_id');
            }
            
            $advanceTime=$expSchedule->experience->experience_booking_in_advance_time;
            $bookingDatetime=$expSchedule->start_date.' '.$expSchedule->start_time;
            $advanceBookingTime = date('Y-m-d H:i:s', strtotime($bookingDatetime) - 60 * 60 * $advanceTime);
            $userCurrentDateTime= date('Y-m-d H:i:s');
            $data['discount'] = $request->booking_discount;
            $data['slot_id'] = $expSchedule->id;
            $data['total_price'] = $request->booking_total_amount;
            $data['amount'] = $request->booking_amount;
            $data['booking_date'] = $expSchedule->start_date;
            $data['status'] = 0;
            $data['adults'] = $request->adults;
            $data['children'] = $request->children;
            $data['infants'] = $request->infants;
            $data['booking_start_time'] = $expSchedule->start_time;
            $data['booking_end_time'] = $expSchedule->end_time;
            $data['experience_id'] = $expSchedule->experience_id;
            $data['ip'] = $request->ip();
            if(Session::has('cart')){
                Session::forget('cart');
            }
            if(strtotime($userCurrentDateTime)< strtotime($advanceBookingTime) && !in_array($expSchedule->id,$alreadyBookedSlot))
            {
            Session::push('cart', $data);
            }
            if(Session::has('cart')){
                return response()->json([
                    "message"       => 'Your records save successfully',
                    "isSucceeded"   => TRUE,
                    "data"          => $data
                ]);
            }else{
                return response()->json([
                    "message"       => '',
                    "isSucceeded"   => FALSE,
                    "data"          => []
                ]);
            }
        }
        else{
             return redirect('/');
        }
    }

     /**
    * Confirm and pay
    */
    public function bookingConfirm(Request $request){
        if($request->isMethod('post')){
            if(Session::has('cart')){
                $session = $request->session()->get('cart')[0];
                $stripe = new \Stripe\StripeClient(config('services.stripe.secret'));
                $userDetail = Auth::user();
                $experience=Experience::where('id',$session['experience_id'])->first();
                $stripeCustomerId = $userDetail->stripe_id;
                if ( empty($stripeCustomerId) ) { 
                    //Create customer if not before created               
                    $stripeCustomer = $stripe->customers->create([ "email"  => $userDetail->email ]); 
                    $stripeCustomerId = $stripeCustomer->id;
                    $updateCus = User::where(['id' => Auth::user()->id])->update(['stripe_id' => $stripeCustomerId]);
                }
                $pay = $stripe->charges->create([
                    'amount' => $session['amount']*100,
                    'currency' => 'eur',
                    'source' => $request['token']['id'],
                    'capture' => false,
                    'description' => 'Booking for '.$experience->experience_name,
                ]);

            $latestOrder = ExperienceBooking::orderBy('created_at','DESC')->first();

            if(empty($latestOrder))
            {
                $latestOrderId=0;
            }else{
                $latestOrderId=$latestOrder->id;
            }
            $today = strtotime(date("H:i:s"));
            $rand = strtoupper(substr(uniqid(sha1(time())),0,4));
            $orderId = $today . $rand;
            $session2=[
                "user_id"=>Auth::user()->id,
                // "order_number" => str_pad($latestOrderId + 1, 6, "0", STR_PAD_LEFT),
                "order_number" => $orderId,
                "scout_id"=>$experience->user_id,
                "charge_id"=>$pay->id,
                "amount"=>$pay->amount/100,
                "last4"=>$pay->source->last4,
                "network"=>$pay->source->brand,
                "exp_month"=>$pay->source->exp_month,
                "exp_year"=>$pay->source->exp_year,
                "currency"=>$pay->currency

            ];
            $insertData=array_merge($session2,$session);
            $booking=ExperienceBooking::create($insertData);
            
            if($booking){
                $insertedId = $booking->id;
                 if(Session::has('cart')){
                Session::forget('cart');
                }
                if(Session::has('booking')){
                Session::forget('booking');
                }
                Session::push('booking', $insertedId);
                return response()->json([
                    "message"       => 'Your booking confirm,thank you for the payment',
                    "isSucceeded"   => TRUE,
                ]);
            }else{
                return response()->json([
                    "message"       => 'Opps something went wrong.',
                    "isSucceeded"   => FALSE,
                ]);
            }
             saveNotification('booking_created',array('admin' =>1,'scout' =>$experience->user_id),$booking->id);
        }
        else{
              return response()->json([
                    "message"       => 'Opps something went wrong.',
                    "isSucceeded"   => FALSE,
                    "session"   => "FALSE",
                ]);
        }
        }
    }
     /**
    * Genreate Order Number
    */
    public function generateRandomString($length = 10) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }
     /**
    * Thankyou for booking
    */
     public function thankYouForBooking(Request $request)
     {
        if(Session::has('booking')){
            $session = $request->session()->get('booking')[0];
            $booking=ExperienceBooking::with('experience')->where(['id'=>$session])->first();
            $page_title= "Thank you for booking";
            return view('front_end.thank-you', compact('page_title','booking'));
        }else{
             //return redirect('/');
             $booking=ExperienceBooking::with('experience')->where(['id'=>1])->first();
            $page_title= "Thank you for booking";
            return view('front_end.thank-you', compact('page_title','booking'));
        }
     }

     /**
    * Cancel Authorization after 24 hours
    */
    public function cancelBookingAuthorization(Request $request)
    {
        $currentDateTime1= new DateTime('NOW');
        $currentDateTime=$currentDateTime1->format('Y-m-d H:i:s');
        $booking=ExperienceBooking::where(['status'=>0])->whereDate('created_at','<',$currentDateTime)->get();
        if(!$booking->isEmpty())
        {
            $stripe = new \Stripe\StripeClient(config('services.stripe.secret'));
            foreach($booking as $book)
            {
                $result= $stripe->refunds->create([
                  'charge' => $book->charge_id,
                ]);
                if($result->status == 'succeeded')
                {
                    $data['status']=6;
                }elseif($result->status == 'pending'){
                    $data['status']=7;
                }elseif($result->status == 'failed'){
                    $data['status']=8;
                }elseif($result->status == 'canceled'){
                    $data['status']=9;
                }
                $data['amount_refunded']=$result->amount/100;
                $data['refund_id']=$result->id;
                $booking = ExperienceBooking::where(['id' => $book->id])->update($data); 
            }
        }

    }
     /**
    * Split Booking Payment after 48 hours
    */
    public function splitBookingPayment(Request $request)
    {
        $admin_comisssion= getSettingMeta('admin_comission');
        $booking=ExperienceBooking::with(['experience','scout'])->where(['status'=>1])->get();
        
        if(!$booking->isEmpty())
        {
             $currentDateTime1= new DateTime();
             $currentDateTime=$currentDateTime1->format('Y-m-d H:i:s');
            $stripe = new \Stripe\StripeClient(config('services.stripe.secret'));
            foreach($booking as $book)
            {
                $bookDateTime1=new DateTime($book->booking_date);
                $hoursToAdd = 48;
                $bookDateTime1->add(new DateInterval("PT{$hoursToAdd}H"));
                $PaymentDateTime=$bookDateTime1->format('Y-m-d H:i:s');
                if(strtotime($currentDateTime)>=strtotime($PaymentDateTime))
                {
                $scoutDetail=User::with('user_meta')->where('id',$book->scout_id)->first();
                $host_id= $book->experience->assigned_host;
                $hostDetail=User::with('user_meta')->where('id',$host_id)->first();
                $scout_comission= $scoutDetail->user_meta->scout_comission;
                $booking_amount=$book->amount_captured;
                $admin_amount= ($admin_comisssion / 100 ) * $booking_amount;
                if(!empty($scout_comission) && $scout_comission!==null)
                {
                    $scout_amount= ($scout_comission / 100 ) * $booking_amount;
                }
                else{
                    $scout_amount= (15 / 100 ) * $booking_amount;
                }
                 $host_amount= $booking_amount-($admin_amount+$scout_amount);
                    $scoutresult =$stripe->transfers->create([
                      'amount' => (float)$scout_amount*100,
                      'currency' => 'eur',
                      'source_transaction'=>$book->charge_id,
                      'destination' => $scoutDetail->user_stripe_account_id,
                      'transfer_group' => $book->order_number,
                    ]);
                   $hostresult = $stripe->transfers->create([
                      'amount' => (float)$host_amount*100,
                      'currency' => 'eur',
                      'source_transaction'=>$book->charge_id,
                      'destination' => $hostDetail->user_stripe_account_id,
                      'transfer_group' => $book->order_number,
                    ]);
                $insertPayment=[
                    [
                        "user_id"=>1,
                        "booking_id"=>$book->id,
                        "experience_id"=>$book->experience_id,
                        "amount"=>$admin_amount,
                        "status"=>1,
                        "transaction_id"=>null,
                        "created_at"=>$currentDateTime,
                        "updated_at"=>$currentDateTime,
                    ],
                    [
                        "user_id"=>$book->scout_id,
                        "booking_id"=>$book->id,
                        "experience_id"=>$book->experience_id,
                        "amount"=>$scout_amount,
                        "status"=>1,
                        "transaction_id"=>$scoutresult->id,
                        "created_at"=>$currentDateTime,
                        "updated_at"=>$currentDateTime,
                    ],
                    [
                        "user_id"=>$book->experience->assigned_host,
                        "booking_id"=>$book->id,
                        "experience_id"=>$book->experience_id,
                        "amount"=>$host_amount,
                        "status"=>1,
                        "transaction_id"=>$hostresult->id,
                        "created_at"=>$currentDateTime,
                        "updated_at"=>$currentDateTime,
                    ]
                ];
                $data['status']=3;
                $booking = ExperienceBooking::where(['id' => $book->id])->update($data); 
                Payment::insert($insertPayment);
            }
            else{
                // echo "ghfjh";
                // die;
            }
        }
        }

    }
     /**
    * Thank you page for Scout
    */
    public function thankYouForRegistration(Request $request)
    {  
         
      return view('front_end.thank_you_for_registration');

    }

    public function realTimeNotificationAjax(){

         $NotificaionData = realTimeNotifcationFetch();   
          echo json_encode($NotificaionData); die;  
    } 
    public function notification_seen_update(Request $request){
        $updateCus = RealNotification::where(['id' => $request->id])->delete();
        echo $updateCus; 
            
    }

    public function clear_up_notification(Request $request){
           $user = Auth::user();
           if(Auth::user()->user_role==1)
           {
            $updateCus = RealNotification::where(['notification_for' =>1])->delete();
           }
           else{
           $updateCus = RealNotification::where(['notification_for' =>$user->id])->delete();
           }
        echo $updateCus; 
            
    }
    
}
