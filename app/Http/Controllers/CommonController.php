<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

class CommonController extends Controller
{
	//check unique mobile no in case of add but not in update case
    public function checkUniqueMobile(Request $request)
    {
    	if($request->isMethod('post')){  
			
        	$exist_mobile = User::where('user_mobile',$request->user_mobile)->first();

        	if ($exist_mobile && $exist_mobile->user_mobile==$request->user_mobile && $exist_mobile->id==$request->id) {
        	 	echo 'true';
    	 	} 
	        elseif ($exist_mobile && $exist_mobile->user_mobile == $request->user_mobile) { 
	        	echo 'false';  
	        }
	        else {
	         echo 'true'; 
	     	}
        }
    }

    //check unique email in case of add but not in update case
    public function checkUniqueEmail(Request $request){

        if($request->isMethod('post')){  

            $user = User::where('email',$request->email)->first();

            if($user && $user->email==$request->email && $user->id==$request->id){
                 echo 'true';
         	}	
         	elseif($user && $user->email==$request->email){
         		echo 'false';
         	}
         	else{
             	echo 'true';
         	} 
        } 
    }
}
