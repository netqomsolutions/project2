<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use App\User;
use \Illuminate\Http\Request;
use Laravel\Socialite\Facades\Socialite;
use Carbon\Carbon;
use DB;
use DateTime;
class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;
    protected $providers = [
        'facebook','google','twitter'
    ];

    /**
* Where to redirect users after login.
*
* @var string
*/
#protected $redirectTo = RouteServiceProvider::HOME;
    public function redirectTo() {
        $role = Auth::user()->user_role;
        switch ($role) {
        case 1:
        return '/admin/dashboard';
        break;
        case 2:
        return '/traveler/dashboard';
        break;
        case 3:
        return '/scout/dashboard';
        break;
        case 4:
        return '/my-profile';
        break;
        default:
        return '/login';
        break;
        }
    }


    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    /**
    *overwrite credentials method to add more than email pass check (override method)
    */ 
    public function credentials(Request $request)
    {
        $credentials = $request->only($this->username(), 'password');
        $credentials = \Arr::add($credentials, 'status', 1);
        return $credentials;
    }


    /**
    * If user not active then send message back to login view (override method)
    */
    protected function sendFailedLoginResponse(Request $request)
    {
        $errors = [$this->username() => trans('auth.failed')];
        // Load user from database
        $user = User::where($this->username(), $request->{$this->username()})->first();

        // Check if user was successfully loaded, that the password matches
        // and active is not 1. If so, override the default error message.
        if ($user && \Hash::check($request->password, $user->password) && $user->status != 1) {
            $errors = [$this->username() => trans('auth.notactivated')];
        }

        if ($request->expectsJson()) {
            return response()->json($errors, 422);
        }
        return redirect()->back()
            ->withInput($request->only($this->username(), 'remember'))
            ->withErrors($errors);
    }
    public function sendLoginResponse(Request $request)
    {
        $request->session()->regenerate();
       $user = User::where('email',$request->email)->first();
       $user->is_online=1;
       $user->update();
        $this->clearLoginAttempts($request);

        if ($response = $this->authenticated($request, $this->guard()->user())) {
            return $response;
        }

        return $request->wantsJson()
                    ? new JsonResponse([], 204)
                    : redirect()->intended($this->redirectPath());
    }

    //Logout Functionality
    public function logout(Request $request) {
       if (Auth::check()) {
         $auth = Auth::user();
        $user = User::where('email',$auth->email)->first();
       $user->is_online=0;
       $user->update();
   }
 
        Auth::logout();
        return redirect('/login');
    }

     public function redirectToProvider($driver)
    {
        if( ! $this->isProviderAllowed($driver) ) {
            return $this->sendFailedResponse("{$driver} is not currently supported");
        }

        try {
            return Socialite::driver($driver)->redirect();
        } catch (Exception $e) {
            // You should show something simple fail message
            return $this->sendFailedResponse($e->getMessage());
        }
    }
     public function handleProviderCallback(Request $request, $driver )
    {
       if (!$request->has('code') || $request->has('denied')) {
            return redirect('login');
        }
        try {
            if($driver=='facebook')
       {
        $fields=['name', 'first_name', 'last_name', 'email', 'gender', 'verified'];
         $user = Socialite::driver($driver)->fields($fields)->user();
       }else{
         $user = Socialite::driver($driver)->user();
       }    
        } catch (Exception $e) {
         
            return $this->sendFailedResponse($e->getMessage());
        }

        // check for email in returned user
        // return empty( $user->email )
        //     ? $this->sendFailedResponse("No email id returned from {$driver} provider.")
        //     : $this->loginOrCreateAccount($user, $driver);
        return empty( $user->email )
            ? $this->sendFailedResponseDueToEmailNull("No email address returned from {$driver} provider.")
            : $this->loginOrCreateAccount($user, $driver);
    }

     protected function sendFailedResponse($msg = null)
    {
        return redirect()->route('login')
            ->withErrors(['msg' => $msg ? : 'Unable to login, try with another provider to login.']);
    }

    protected function loginOrCreateAccount($providerUser, $driver) {
          if($driver=='facebook')
           {
            $firstName=$providerUser->user['first_name'];
            $lastName=$providerUser->user['last_name'];
           }
           else{
             $firstName=$providerUser->user['given_name'];
            $lastName=$providerUser->user['family_name'];
           }
          $currentDateTime1= new DateTime();
          $currentDateTime=$currentDateTime1->format('Y-m-d H:i:s');
        // check for already has account
        $user = User::where('email', $providerUser->getEmail())->first();
       

        // if user already found
        if( $user ) {
            // update the avatar and provider that might have changed
            $user->update([
                'avatar' => $providerUser->avatar,
                'provider' => $driver,
                'provider_id' => $providerUser->id,
                'access_token' => $providerUser->token,
                             ]);
        } else {
              if($providerUser->getEmail()){ //Check email exists or not. If exists create a new user
                $userData=[
                  'user_fname' => $firstName,
                  'user_lname' => $lastName,
                  'email' => $providerUser->getEmail(),
                  'user_image' => $providerUser->getAvatar(),
                  'provider' => $driver,
                  'provider_id' => $providerUser->getId(),
                  'access_token' => $providerUser->token,
                  'password' => '',
                  'user_role' => 2,
                  'status' => 1,
                  'ip'=>$_SERVER['REMOTE_ADDR'],
                  'email_verified_at' =>$currentDateTime,
                  'created_at' =>$currentDateTime,
                  "updated_at"=>$currentDateTime,
            ];
               $user = User::create($userData);
                saveNotification('new_traveler',array('admin' =>1),$user->id); 

             }else{
            
            //Show message here what you want to show
            
           }
        }

        // login the user
        Auth::login($user,true);
        return $this->sendSuccessResponse();
    }
     protected function sendSuccessResponse()
    {
        return redirect()->route('traveler-dashboard');
    }
     private function isProviderAllowed($driver)
    {
        return in_array($driver, $this->providers) && config()->has("services.{$driver}");
    }
    protected function sendFailedResponseDueToEmailNull($msg = null){
        return redirect()->route('login')->with(['failure' => $msg.' Unable to login, try with another provider to login.']);
    }



}
