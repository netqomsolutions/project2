<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;
use App\Providers\RouteServiceProvider;
use App\User;
use App\UserLanguage;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Auth\Events\Registered;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use DB;
use App\Mail\SendMail; 
class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    //protected $redirectTo = RouteServiceProvider::HOME;
    protected $redirectTo = '/login';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'user_fname'             => ['required', 'string', 'max:255'],
            'user_lname'             => ['required', 'string', 'max:255'],
            'user_role'              => ['required'],
            'email'                  => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password'               => ['required', 'string', 'min:8', 'confirmed'],
            'user_mobile'            => ['required' , 'unique:users'],
        ],[
            'user_lname.required'    => 'The last name field is required.',
            'user_fname.required'    => 'The first name field is required.',
            'user_role.required'     => 'This select type field is required.'
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    { 
        $status = 0;
        if($data['user_role'] == 2){
            $status = 1;
        }
        return User::create([
            'user_fname'    => $data['user_fname'],
            'user_lname'    => $data['user_lname'],
            'user_role'     => $data['user_role'],
            'user_mobile'   => $data['user_mobile'],
            'user_mobile_code'   => $data['user_mobile_code'],
            'email'         => $data['email'],
            'password'      => Hash::make($data['password']),
            'status'        => $status,
            'ip'            => $_SERVER['REMOTE_ADDR']
        ]);
    }


    /**
    *Disable auto login after registration using override this method
    */
    public function register(Request $request)
    {


        $this->validator($request->all())->validate();
        $user = $this->create($request->all());  
          // $Arr['treveler'] = $user->id; 
        saveNotification('new_traveler',array('admin' =>1),$user->id); 
        event(new Registered($user));
        //$this->guard()->login($user);

        return $this->registered($request, $user)
                        ?: redirect($this->redirectPath())->with('success','Thank you for registration. Please verify Your email.');
    }
    public function showRegistrationForm(Request $request)
    {
        $getCountryCode =  DB::table('country_codes')->select("iso","phonecode")->get(); 
         return view('auth.register', compact('getCountryCode'));
    }

    public function scoutRegisterForm(Request $request)
    {
        if($request->isMethod('post')){ 
            $postData = $request->except(['_token','user_fname','user_lname','user_email','user_mobile','password','password_confirm','fileToUpload','agree','user_primary_language','user_other_languages','user_english_level','user_mobile_code','about_user']);
            $user_fname  = $request->user_fname;
            $user_lname  = $request->user_lname;
            $user_email  = $request->user_email;
            $user_mobile = $request->user_mobile;
            $password    = $request->password;
            if(!empty($request->user_other_languages))
           {
              $userOtherLang= implode(',',$request->user_other_languages);
           }else{
              $userOtherLang='';
           }
            $userArr  = array(
                'user_fname' =>$user_fname, 
                'user_lname' =>$user_lname, 
                'email' =>$user_email, 
                'user_mobile_code' =>$request->user_mobile_code, 
                'user_mobile' =>$user_mobile, 
                'user_english_level' =>$request->user_english_level, 
                'about_me' =>$request->about_user, 
                'user_primary_language' =>$request->user_primary_language ,
                'user_other_languages' =>$userOtherLang ,
                'user_address' =>$request->user_residence ,
                'ip'            => $request->ip(),
                'password' =>Hash::make($password), 
                'created_at' =>date('Y-m-d H:i:s'), 
                'updated_at' =>date('Y-m-d H:i:s'),
            );
            $folderPath = 'users/scout';  
            $userArr['user_image_path']=$folderPath;
            $userArr['user_role']=3;
            $userArr['user_image'] = $this->imageUpload($request,$folderPath,"fileToUpload");
            $thumbnailpic = "/profile-thumb-".$userArr['user_image'];
            createThumbnail($folderPath,$userArr['user_image'],$thumbnailpic,$width=160,$height=160); 

            $id = DB::table('users')->insertGetId($userArr);
            if($request->user_other_languages){
                foreach ($request->user_other_languages as $key => $value) {
                    UserLanguage::create(['user_id'=> $id,'language_id'=>$value]);
                }
            }

            $dataArr = array();  
            foreach ($postData as $k => $data) {
                if(is_array($data)){
                 $dataArr[$k] = serialize($data);
                }else{
                 $dataArr[$k] = $data; 
                }
            }
             $dataArr['user_id'] = $id;  
             $dataArr['created_at'] = date('Y-m-d H:i:s');  
             $dataArr['updated_at'] = date('Y-m-d H:i:s');  
            $insertId =  DB::table('user_metas')->insertGetId($dataArr);
            $route ="thank-you-for-registration"; 
            if($insertId){
                $userArr['password'] =$password; 
                $userArr['subject'] = "BECOME A SCOUT"; 
                $name= $user_fname .' '.$user_lname;
                saveNotification('new_scout',array('admin' =>1),$id);
                //$sendEmail = Mail::to([$user_email])->send( new SendMail($userArr,'new_scout_registration'));
                return redirect()->route($route)->with('name', $name);
            }else{
                return redirect()->back()->with('failure','Opps,something wrong,please try after some time.');
            }
                
        }else{
            $getCountryCode =  DB::table('country_codes')->select("iso","phonecode")->get(); 
            $languages =  DB::table('languages')->get(); 
            $page_title = 'Scout Registration Form';
            $page_description = 'Some description for the page';
            return view('auth.scout_register', compact('page_title', 'page_description','getCountryCode','languages'));
        }
    }



     public function imageUpload($request,$folderPath,$imageName){
        $fName = '';
        if($request->hasFile($imageName)){
        $originName = $request->file($imageName)->getClientOriginalName();
        $fileName = pathinfo($originName, PATHINFO_FILENAME);
        $extension = $request->file($imageName)->getClientOriginalExtension();
        $fileName = $fileName.'_'.time().'.'.$extension;            
        if($request->file($imageName)->move(public_path($folderPath), $fileName)){
            $fName = $fileName;
        }
        return $fName;
        }
        return $fName;       
    }


    public function check_forEmail(Request $request){
        if($request->isMethod('post')){   
            $user = User::where('email',$request->user_email)->first();
            if($user){
                 echo 'false';
             }else{
                 echo 'true';
             } 
        } 
    }
    public function check_forMobile(Request $request){
        if($request->isMethod('post')){   
            $user = User::where('user_mobile',$request->user_mobile)->first();
            if($user){
                 echo 'false';
             }else{
                 echo 'true';
             } 
        } 
    }

    







}
