<?php

namespace App\Http\Controllers;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Hash;
use App\ContentManagementSystem;
use App\AboutUs;
use App\HomePage;
use App\User;
use App\UserMeta;
use App\Language;
use App\UserBankDetail;
use App\ExperienceCategory;
use App\Experience;
use App\ExperienceMeta;
use App\ExperienceFeature;
use App\ExperienceImage;
use App\ExperienceBooking;
use App\ExperienceSchedule;
use App\ExperienceAprroval;
use App\Country;
use App\State;
use App\City;
use App\Location;
use Carbon\Carbon;
use Carbon\CarbonInterval;
use Carbon\CarbonPeriod;
use App\Mail\SendMail;
use App\Message;
use Illuminate\Support\Facades\Validator;
use DataTables;
use DateTime;
use DatePeriod;
use DateInterval;
use Intervention\Image\Exception\NotReadableException;
use App\Http\Traits\CommonTrait;
use Illuminate\Support\Facades\Auth; 
use PDF;
use Image;
use Illuminate\Support\Facades\File; 
use App\CountryCode;
use Session;
use App\Http\Controllers\SpaceInvoicesApolloController;

class ScoutController extends Controller
{
    use CommonTrait;
    /**
    * Scout Dashboard Controll Function
    */
     public function __construct()
    {
       if(Session::has('experience_gallery')){
            Session::forget('experience_gallery');
        }
    }
    public function dashboard(Request $request,$scoutId=null)
    {
        $page_title = 'Scout Dashboard';
        $page_description = 'Some description for the page';
        $user = Auth::user(); 
        $login_id = $user->id;  
        $recid  = Message::where(['sender_id' => $login_id])->select('receiver_id as rc_id')->distinct('rc_id')
        ->get()
        ->toArray();
        if(empty($recid)){
            $recid  = Message::where(['receiver_id' => $login_id])->select('sender_id as rc_id')->distinct('rc_id')
            ->get()
            ->toArray(); 
        } 
        $dusers=array_column($recid, 'rc_id');
        $userList = array();
        if(empty($userList) && $scoutId && !in_array($scoutId, $dusers)){
            $tUser =  DB::table('users')->where(['id' => $scoutId])->get();
            $userList[] = $tUser;
        } 
        foreach ($recid as $key => $id) {  
            $userList[] = getCountMessages($id['rc_id'],$login_id); 
        }
        $totalExperience = Experience::where('user_id',$login_id)->count();
        $totalBooking = ExperienceBooking::where('scout_id',$login_id)->count();
        $totalEarning = ExperienceBooking::where('scout_id',$login_id)->pluck('amount')->sum();
       
      
        return view('scout.dashboard', compact('page_title', 'page_description','userList','user','totalExperience','scoutId','totalBooking','totalEarning'));
    }

    /*
    * Profile Function
    */
    public function myProfile(Request $request){
        $scoutData = User::where(['id' => Auth::user()->id])->first();  
        return view('profile',compact('scoutData'));
    }

    /**
    * Scout Dashboard Controll Function
    */
    public function scoutBookings()
    {
        $page_title = 'Bookings';
        $page_description = 'Some description for the page';

        return view('scout.dashboard', compact('page_title', 'page_description'));
    }
     /*
    * Add experience form by scout
    */
    public function addHostForm(Request $request,$id=null){

     $check=  checkFiscalization(); // check the User complate the fisclization process
     if(!empty($check) && $check!==null && $check==1)
        { 
            $page_title = 'Manage Host';
            $languages =  Language::get(); 
            $getCountryCode = CountryCode::get();
            if($request->isMethod('get') && $id==null){
                return view('scout.add_host',compact('languages','page_title','getCountryCode'));
            }else{
                 $userData = User::where(['id' => base64_decode($id)])->first(); 
                return view('scout.add_host',compact('userData','languages','page_title','getCountryCode'));
            }
      }else{

          return redirect()->route('fiscalization')->with('failure',"You have to complete the fiscalization process");
         }
    }
    /*
    * Host Creat Function
    */
    public function createHost(Request $request){
        if($request->isMethod('post')){ 

            $user_data = $this->createOrUpdateHost($request); 
            if($user_data['status']){
                return redirect()->route('scout-host-list')->with('success',$user_data['message']);
            }else{

                return redirect()->route('scout-host-list')->with('failure',$user_data['message']);
            }
        }
    }

    /*
    * Host List Show Function 
    */
    public function hostList(Request $request){
        $page_title = 'Manage Host';
        if ($request->ajax()) {
            $data = User::with('user_meta')->where(['user_role' => 4, 'created_by' => Auth::user()->id])->where('status', 1)->latest()->get();
            return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('action', function($row){
                    $btn = '<a href="'. route('scout-edit-host-detail',['id' => base64_encode($row->id)]) .'" class="edit btn btn-success btn-sm">Edit</a> <a href="javascript:void(0);" class="delete btn btn-danger btn-sm action-scout-btn delete-host-confirmation" data-original-title title data-toggle="confirmation" host_id='.base64_encode($row->id).'">Delete</a>';
                    return $btn;
                })
                ->rawColumns(['action'])
                ->make(true);
        }
      
       return view('scout.host_list',compact('page_title'));     
    }
     /*
    * Host Deatil function
    */
    public function hostDetail(Request $request,$id){ 
        if($request->isMethod('get') &&  $id){
            $page_title = 'Scout Deatil';
            $scoutData = User::where(['id' => base64_decode($id)])->first();          
            return view('scout.host_detail',compact('page_title','scoutData')); 
        }
    }


    /*
    * Delete Host By Scout
    */
    public function deleteHost(Request $request){
        if($request->isMethod('post')){
            if(!empty($request->host_id)){
                $del = User::where(['id' => base64_decode($request->host_id)])->delete();
                if($del){
                    return response()->json([
                        'isSucceeded' => TRUE,
                        'message'=>"successfully Deleted."
                    ]);
                }else{
                    return response()->json([
                        'isSucceeded' => FALSE,
                        'message'=>"Something went wrong."
                    ]);
                }    
            }
        }
    }
    /*
    * Experience List 
    */
    public function experienceList(Request $request){
         $page_title = 'Manage Experiences';
          $experiences = Experience::with('host')->where(['user_id' => Auth::user()->id])->get();
        return view('scout.experience_list',compact('experiences','page_title'));
    }
     /*
    * Manage Booked Experiences by scout
    */
    public function bookedExperienceList(Request $request){
          $page_title = 'Manage Experiences';
        $experiences = ExperienceBooking::with(['scout','experience'])->where([['scout_id','=',Auth::user()->id],['status', '<>', '0']])->get();
            return view('scout.manage_booked_experiences',compact('experiences','page_title'));
        
    } 
    /*
    * Manage New Booked Experiences by scout
    */
    public function newBookingExperienceList(Request $request){
          $page_title = 'Manage New Experiences';
        $experiences = ExperienceBooking::with(['scout','experience'])->where(['scout_id'=>Auth::user()->id,'status'=>'0'])->get();
            return view('scout.new-booking',compact('experiences','page_title'));
        
    } 

     /*
    * Manage Booked Experiences by scout
    */
    public function bookedExperienceDetail(Request $request,$id){
          $page_title = 'Booking Information';
        $experience = ExperienceBooking::with(['scout','experience'])->where('id',base64_decode($id))->first();
/*echo "<pre>";print_r($experience->toArray());
        die;*/
            return view('scout.booking_detail',compact('experience','page_title'));
        
    } 
    /*
    * Add experience form by scout
    */
    public function addExperienceForm(Request $request){
     
      $check=  checkFiscalization(); // check the User complate the fisclization process
     if(!empty($check) && $check!==null && $check==1)
            { 
              $page_title = 'Manage Experiences';
            if(Session::has('experience_gallery')){
           Session::forget('experience_gallery');
        }
        if($request->isMethod('get')){
            $stripeAccountId= Auth::user()->user_stripe_account_id;
            /*if(!empty($stripeAccountId) && $stripeAccountId!==null)
            {*/
            $experienceCategories = ExperienceCategory::all();
            $experienceFeatures = ExperienceFeature::all();
            $hosts = User::whereHas('user_meta', function($query) {
                  return $query->select(['user_id','company_name','id','company_registration_no']);
             })->select('id','user_fname','user_lname')->where(['created_by'=>Auth::user()->id,'user_role'=>4])->get();
            $countries = Country::all();
            return view('scout.add_experience',compact('experienceCategories','experienceFeatures','countries','hosts','page_title'));
            /*}else{

                return redirect()->route('account-information')->with('failure',"You have to add Bank detail for add experience to get paid easily.");
            }*/
        }
        }else{

          return redirect()->route('fiscalization')->with('failure',"You have to complete the fiscalization process");
         } 
    }
    
       /*
    * New Experience Create By Admin
    */
    public function createExperience(Request $request){
        if($request->ajax()) {
            if($request->exp_id!=0)
            {
                 return response()->json([
                        'isSucceeded' => FALSE,
                        'message'=>"This Experience Already Saved."
                    ]);
            }
            $data = $request->except(['_token','exp_id',"newfile"]);  
           $scoutDetail= User::where(['id'=>Auth::user()->id])->first();
           $hostDetail= User::where(['id'=>$data['data']['experiences']['assigned_host']])->first();
           $getScoutlanguage=[];
           $getHostlanguage=[];
           if($scoutDetail->user_other_languages!='')
           {
           $getScoutlanguage = explode(",",$scoutDetail->user_other_languages);
            }
           if(!empty($hostDetail->user_primary_language))
           {
             
             in_array($scoutDetail->user_primary_language, $getScoutlanguage) ? '' : array_push($getScoutlanguage, $scoutDetail->user_primary_language); 
           }
            if($hostDetail->user_other_languages!='')
           {
           $getHostlanguage = explode(",",$hostDetail->user_other_languages);
            }
           if(!empty($hostDetail->user_primary_language))
           {
                 in_array($hostDetail->user_primary_language, $getHostlanguage) ? '':array_push($getHostlanguage, $hostDetail->user_primary_language); 
                
           }
           
            $folderPath = 'pages/experiences';     
            $data['data']['experiences']['user_id'] = Auth::user()->id;
            $data['data']['experiences']['ip'] = $request->ip();
            $data['data']['experiences']['created_at'] = date('Y-m-d H:i:s');
            $data['data']['experiences']['updated_at'] = date('Y-m-d H:i:s');
            $data['data']['experience_feature_relations']['created_at'] = date('Y-m-d H:i:s');
            $data['data']['experience_feature_relations']['updated_at'] = date('Y-m-d H:i:s');
            $data['data']['experience_category_relation']['updated_at'] = date('Y-m-d H:i:s');
            $data['data']['experience_category_relation']['created_at'] = date('Y-m-d H:i:s');
            $data['data']['experience_images']['ip'] = $request->ip();
            $data['data']['experience_images']['created_at'] = date('Y-m-d H:i:s');
            $data['data']['experience_images']['updated_at'] = date('Y-m-d H:i:s');
            $data['data']['additional_detail']['ip'] = $request->ip();
            $data['data']['additional_detail']['created_at'] = date('Y-m-d H:i:s');
            $data['data']['additional_detail']['updated_at'] = date('Y-m-d H:i:s'); 
            $data['data']['experience_additional_price']['created_at'] = date('Y-m-d H:i:s');
            $data['data']['experience_additional_price']['updated_at'] = date('Y-m-d H:i:s'); 
            $data['data']['experience_additional_price']['ip'] = $request->ip();;
            $data['data']['language_data']['created_at'] = date('Y-m-d H:i:s');
            $data['data']['language_data']['updated_at'] = date('Y-m-d H:i:s');
            if(empty($data['data']['experiences']['experience_high_price'])){
                $data['data']['experiences']['experience_high_price'] = 0;
            }
            if(empty($data['data']['experiences']['experience_low_price'])){
                $data['data']['experiences']['experience_low_price'] = 0;
            }
            if(isset($data['data']['experiences']['experience_feature_image'])){
                   if(isset($request->newFile))
                      {
                          $cropImage= $request->newFile;
                           
                          $data['data']['experiences']['experience_feature_image'] = $this->imageUpload($request,$folderPath,'newFile'); 
                      }else{
                           $data['data']['experiences']['experience_feature_image'] = $this->imageUpload($request,$folderPath,'data.experiences.experience_feature_image'); 
                      }
                           
                            DB::table('experiences')->where(['id' => $request->exp_id])->update($data['data']['experiences']);  
                        }

            // Saving data into multiple tables
            $resExp=0;
            DB::beginTransaction();
            try {

                    $experienceId = DB::table('experiences')->insertGetId($data['data']['experiences']);
                    $data['data']['additional_detail']['experience_id'] = $experienceId;
                    $resExp=$experienceId;
                     $experienceImages = DB::table('experience_metas')->insert($data['data']['additional_detail']);
                     /* date:30-01-2021 @RT  add language in experience*/
                     // add host and Scout Language into experience_language_relations  tabel @RT 30-01-2021
                     if(!empty($getHostlanguage)){
                          foreach($getHostlanguage as $lang)
                            {
                                $data['data']['language_data']['user_id'] =$data['data']['experiences']['assigned_host'];
                                $data['data']['language_data']['language_id'] = $lang;
                                $data['data']['language_data']['experience_id'] = $experienceId;
                                $data['data']['language_data']['type'] =2 ;
                                $experienceLanguages = DB::table('experience_language_relations')->insert( $data['data']['language_data']);
                            } 
                    }
                    if(!empty($getScoutlanguage)){
                          foreach($getScoutlanguage as $lang)
                            {
                                $data['data']['language_data']['user_id'] = Auth::user()->id;
                                $data['data']['language_data']['language_id'] = $lang;
                                $data['data']['language_data']['experience_id'] = $experienceId;
                                $data['data']['language_data']['type'] =1 ;
                                $experienceLanguages = DB::table('experience_language_relations')->insert( $data['data']['language_data']);
                            } 
                    }
                    
                    if(!empty($data['data']['experience_feature_relations']['experience_feature_id'])){
                        $features=$data['data']['experience_feature_relations']['experience_feature_id'];
                        foreach($features as $feat)
                        {
                    $data['data']['experience_feature_relations']['experience_id'] = $experienceId;
                    $data['data']['experience_feature_relations']['experience_feature_id'] = $feat;
                    $experienceFeatureRelations = DB::table('experience_feature_relations')->insert($data['data']['experience_feature_relations']);
                        }
                    }
                    if(!empty($data['data']['experience_category_relation']['experience_category_id'])){
                        $categories=$data['data']['experience_category_relation']['experience_category_id'];
                        foreach($categories as $cat)
                        {
                            $data['data']['experience_category_relation']['experience_category_id'] = $cat;
                            $data['data']['experience_category_relation']['experience_id'] = $experienceId;
                            $experienceImages = DB::table('experience_category_relations')->insert($data['data']['experience_category_relation']);
                        }
                    }
                    if($data['data']['experiences']['experience_type']==2)
                    {
                    if(!empty($data['data']['experience_additional_price']['additional_person'])){
                        $additionalPerson=$data['data']['experience_additional_price'];
                        foreach($data['data']['experience_additional_price']['additional_person'] as $key=>$person)
                        {
                            $data['data']['experience_additional_price']['additional_person'] =$additionalPerson['additional_person'][$key];
                            $data['data']['experience_additional_price']['price_per_person'] =$additionalPerson['price_per_person'][$key];
                            $data['data']['experience_additional_price']['additional_price'] =$additionalPerson['additional_price'][$key];
                            $data['data']['experience_additional_price']['experience_id'] = $experienceId;
                             $experienceImages = DB::table('experience_additional_prices')->insert($data['data']['experience_additional_price']);
                        }
                    }
                    }
                        if(Session::has('experience_gallery')){
                            $experience_gallery = $request->session()->get('experience_gallery');
                            foreach($experience_gallery as $gallery)
                            {
                                $gallery['experience_id']= $experienceId;
                                 $experienceImages = DB::table('experience_images')->insert($gallery);
                            }
                            Session::forget('experience_gallery');
                        }
                        
                    DB::commit();
                    $data['experience']= Experience::where('id',$experienceId)->first();
                    $data['scout']  = User::where(['id' => Auth::user()->id])->first();
                    $data['subject']="New Experience Created";
                    $sendEmail = Mail::to(Auth::user()->email)->send( new SendMail($data,'new-experience-created'));
                    saveNotification('experince_created',array('admin' =>1),$experienceId);
                    return response()->json([
                        'isSucceeded' => TRUE,
                        'message'=>"Experience created successfully",
                        "experience_id"=>$experienceId,
                    ]);   
                
            } catch (\Exception $e) {
                return response()->json([
                        'isSucceeded' => FALSE,
                        'message'=>$e->getMessage()
                    ]);
            }
                         
        }
    }

     /*
    * Edit Experience control
    */
    public function editExperience(Request $request,$id=null){
        $page_title = 'Edit Experience';
        $page_description = 'Some description for the page';
        $experienceCategories = ExperienceCategory::all();
        $experienceFeatures = ExperienceFeature::all();
        $countries = Country::all(); 
        $expData= Experience::with(['experience_category_relation','experience_feature_relations','experience_images','experience_meta_detail','experience_schedule','experience_additional_price'])->where('id',base64_decode($id))->first(); 
        $appvrovalExp= ExperienceAprroval::where(['id'=>base64_decode($id)])->first(); 
         $hosts = User::whereHas('user_meta', function($query) {
                  return $query->select(['user_id','company_name','id','company_registration_no']);
             })->select('id','user_fname','user_lname')->where(['created_by'=>$expData['user_id'],'user_role'=>4])->get();
        return view('scout.update_experience', compact('page_title', 'page_description', 'experienceCategories','experienceFeatures','countries','expData','hosts','appvrovalExp'));

    }

    /*
    * Update Experience function
    */
    public function updateExperience(Request $request){ 
        if($request->ajax()) {        
            $data = $request->except(['_token','exp_id','preloaded']);  
            $folderPath = 'pages/experiences';
            $data['data']['experiences']['user_id'] = Auth::user()->id;
            $data['data']['experiences']['updated_at'] = date('Y-m-d H:i:s');
            $data['data']['experience_feature_relations']['updated_at'] = date('Y-m-d H:i:s');
            $data['data']['experience_category_relation']['updated_at'] = date('Y-m-d H:i:s');
            $data['data']['experience_category_relation']['created_at'] = date('Y-m-d H:i:s');
            $data['data']['experience_feature_relations']['created_at'] = date('Y-m-d H:i:s');
            $data['data']['additional_detail']['updated_at'] = date('Y-m-d H:i:s');    
            $data['data']['experience_additional_price']['created_at'] = date('Y-m-d H:i:s');
            $data['data']['experience_additional_price']['updated_at'] = date('Y-m-d H:i:s');
            if(empty($data['data']['experiences']['experience_high_price'])){
                $data['data']['experiences']['experience_high_price'] = 0;
            }
            if(isset($data['data']['experiences']['experience_feature_image'])){
                $data['data']['experiences']['experience_feature_image'] = $this->imageUpload($request,$folderPath,'data.experiences.experience_feature_image');   
            }
            // Saving data into multiple tables
            try {
                DB::transaction(function () use($data,$request,$folderPath) {
                    DB::table('experiences')->where(['id' => $request->exp_id])->update($data['data']['experiences']);
                    DB::table('experience_metas')->where(['experience_id' => $request->exp_id])->update($data['data']['additional_detail']);
                    
                    if(!empty($data['data']['experience_feature_relations']['experience_feature_id'])){
                        DB::table('experience_feature_relations')->where('experience_id', '=', $request->exp_id)->delete();
                        $features=$data['data']['experience_feature_relations']['experience_feature_id'];
                        foreach($features as $feat)
                        {
                    $data['data']['experience_feature_relations']['experience_id'] = $request->exp_id;
                    $data['data']['experience_feature_relations']['experience_feature_id'] = $feat;
                    $experienceFeatureRelations = DB::table('experience_feature_relations')->insert($data['data']['experience_feature_relations']);
                        }
                    }
                    if(!empty($data['data']['experience_category_relation']['experience_category_id'])){
                        DB::table('experience_category_relations')->where('experience_id', '=', $request->exp_id)->delete();
                        $categories=$data['data']['experience_category_relation']['experience_category_id'];
                        foreach($categories as $cat)
                        {
                            $data['data']['experience_category_relation']['experience_category_id'] = $cat;
                            $data['data']['experience_category_relation']['experience_id'] = $request->exp_id;
                            $experienceImages = DB::table('experience_category_relations')->insert($data['data']['experience_category_relation']);
                        }
                    }
                    if($data['data']['experiences']['experience_type']==3)
                    {
                    if(!empty($data['data']['experience_additional_price']['additional_person'])){
                         DB::table('experience_additional_prices')->where('experience_id', '=', $request->exp_id)->delete();
                        $additionalPerson=$data['data']['experience_additional_price'];
                        foreach($data['data']['experience_additional_price']['additional_person'] as $key=>$person)
                        {
                            $data['data']['experience_additional_price']['additional_person'] =$additionalPerson['additional_person'][$key];
                            $data['data']['experience_additional_price']['additional_price'] =$additionalPerson['additional_price'][$key];
                            $data['data']['experience_additional_price']['experience_id'] = $request->exp_id;
                             $experienceImages = DB::table('experience_additional_prices')->insert($data['data']['experience_additional_price']);
                        }
                    }
                    }
                    if(isset($data['data']['experience_images']['image_name'])){
                         $data['data']['experience_images']['created_at'] = date('Y-m-d H:i:s');
                        $data['data']['experience_images']['updated_at'] = date('Y-m-d H:i:s');
                        $files = $request->file('data.experience_images.image_name');
                        foreach($files as $file){
                            $originName = $file->getClientOriginalName();
                            $fileName = pathinfo($originName, PATHINFO_FILENAME);
                            $extension = $file->getClientOriginalExtension();
                            $fileName = $fileName.'_'.time().'.'.$extension;            
                            if($file->move(public_path($folderPath), $fileName)){
                                $data['data']['experience_images']['image_name'] = $fileName;
                                $data['data']['experience_images']['experience_id'] = $request->exp_id;
                                $experienceImages = DB::table('experience_images')->insert($data['data']['experience_images']);
                            }             
                        }
                    }
                });
            } catch (\Exception $e) {
                return response()->json([
                        'isSucceeded' => FALSE,
                        'message'=>$e->getMessage()
                    ]);
            }
           return response()->json([
                        'isSucceeded' => TRUE,
                        'message'=>"Experience Details successfully saved.",
                        "experience_id"=>$request->exp_id,
                    ]);
        }
    }  

    /*
    * New Update Experience function
    */
    public function updateExperienceNewOld(Request $request){ 
        if($request->ajax()) {        
            $data = $request->except(['_token','exp_id','preloaded']);  
            $folderPath = 'pages/experiences';
            // Saving data into multiple tables
            try {
                DB::transaction(function () use($data,$request,$folderPath) {
                    if(isset($data['data']['experiences'])){
                        $data['data']['experiences']['user_id'] = Auth::user()->id;
                        $data['data']['experiences']['updated_at'] = date('Y-m-d H:i:s');
                        $data['data']['experiences']['ip'] = $request->ip();
                         if(empty($data['data']['experiences']['experience_high_price'])){
                                $data['data']['experiences']['experience_high_price'] = 0;
                          }
                        DB::table('experiences')->where(['id' => $request->exp_id])->update($data['data']['experiences']);
                        if(isset($data['data']['experiences']['experience_type']))
                            { 
                                 if($data['data']['experiences']['experience_type']==3)
                                {    
                                    if(!empty($data['data']['experience_additional_price']['additional_person'])){
                                         $data['data']['experience_additional_price']['ip'] = $request->ip();
                                         $data['data']['experience_additional_price']['created_at'] = date('Y-m-d H:i:s');
                                         $data['data']['experience_additional_price']['updated_at'] = date('Y-m-d H:i:s');

                                         DB::table('experience_additional_prices')->where('experience_id', '=', $request->exp_id)->delete();
                                        $additionalPerson=$data['data']['experience_additional_price'];
                                        foreach($data['data']['experience_additional_price']['additional_person'] as $key=>$person)
                                        {
                                            $data['data']['experience_additional_price']['additional_person'] =$additionalPerson['additional_person'][$key];
                                            $data['data']['experience_additional_price']['additional_price'] =$additionalPerson['additional_price'][$key];
                                            $data['data']['experience_additional_price']['experience_id'] = $request->exp_id;
                                             $experienceImages = DB::table('experience_additional_prices')->insert($data['data']['experience_additional_price']);
                                        }
                                    }
                                }
                             }
                        }
                    if(isset($data['data']['additional_detail'])){
                         $data['data']['additional_detail']['ip'] = $request->ip(); 
                         $data['data']['additional_detail']['updated_at'] = date('Y-m-d H:i:s'); 
                         DB::table('experience_metas')->where(['experience_id' => $request->exp_id])->update($data['data']['additional_detail']);
                    }
                    if(isset($data['data']['experience_feature_relations'])){
                        $data['data']['experience_feature_relations']['updated_at'] = date('Y-m-d H:i:s');
                        $data['data']['experience_feature_relations']['created_at'] = date('Y-m-d H:i:s');   
                        if(!empty($data['data']['experience_feature_relations']['experience_feature_id'])){
                            DB::table('experience_feature_relations')->where('experience_id', '=', $request->exp_id)->delete();
                            $features=$data['data']['experience_feature_relations']['experience_feature_id'];
                            foreach($features as $feat)
                            {
                        $data['data']['experience_feature_relations']['experience_id'] = $request->exp_id;
                        $data['data']['experience_feature_relations']['experience_feature_id'] = $feat;
                        $experienceFeatureRelations = DB::table('experience_feature_relations')->insert($data['data']['experience_feature_relations']);
                            }
                        }
                     }
                    if(isset($data['data']['experience_category_relation'])){
                         $data['data']['experience_category_relation']['updated_at'] = date('Y-m-d H:i:s');
                         $data['data']['experience_category_relation']['created_at'] = date('Y-m-d H:i:s');
                        if(!empty($data['data']['experience_category_relation']['experience_category_id'])){
                            DB::table('experience_category_relations')->where('experience_id', '=', $request->exp_id)->delete();
                            $categories=$data['data']['experience_category_relation']['experience_category_id'];
                            foreach($categories as $cat)
                            {
                                $data['data']['experience_category_relation']['experience_category_id'] = $cat;
                                $data['data']['experience_category_relation']['experience_id'] = $request->exp_id;
                                $experienceImages = DB::table('experience_category_relations')->insert($data['data']['experience_category_relation']);
                            }
                        }
                    }
                    if(isset($data['data']['experiences']['experience_feature_image'])){
                            $data['data']['experiences']['experience_feature_image'] = $this->imageUpload($request,$folderPath,'data.experiences.experience_feature_image'); 
                            DB::table('experiences')->where(['id' => $request->exp_id])->update($data['data']['experiences']);  
                        }
                    if(isset($data['data']['experience_images']['image_name'])){
                         
                         $data['data']['experience_images']['created_at'] = date('Y-m-d H:i:s');
                         $data['data']['experience_images']['updated_at'] = date('Y-m-d H:i:s');
                         $files = $request->file('data.experience_images.image_name');
                        foreach($files as $file){
                            $originName = $file->getClientOriginalName();
                            $fileName = pathinfo($originName, PATHINFO_FILENAME);
                            $extension = $file->getClientOriginalExtension();
                            $fileName = $fileName.'_'.time().'.'.$extension; 
                            $fileName = str_replace(' ', '_', $fileName);        
                            $fileName = str_replace('list-thumb-', '', $fileName);        
                            $fileName = str_replace('detail-thumb-', '', $fileName);  
                            $fileName=preg_replace('/[^A-Za-z0-9\-]/', '', $fileName);            
                            if($file->move(public_path($folderPath), $fileName)){
                                $fName = "/list-thumb-".$fileName;
                                $fName2 = "list-thumb-".$fileName;
                                $thumbnailpic="/thumbs/detail-thumb-".$fileName;
                                $explistFile= createThumbnail($folderPath,$fileName,$fName,$width=365,$height=140);    
                                $expdetailFile= createThumbnail($folderPath,$fileName,$thumbnailpic,$width=985,$height=530); 
                                $image_path = $folderPath.$fileName;  // Value is not URL but directory file path
                                if(File::exists($image_path)) {
                                    File::delete($image_path);
                                }   
                                $data['data']['experience_images']['image_name'] = $fName2;
                                $data['data']['experience_images']['experience_id'] = $request->exp_id;
                                $experienceImages = DB::table('experience_images')->insert($data['data']['experience_images']);
                            }             
                        }
                    }
                });
            } catch (\Exception $e) {
                echo $e->getMessage();
                die;
                return response()->json([
                        'isSucceeded' => FALSE,
                        'message'=>$e->getMessage()
                    ]);
            }
           return response()->json([
                        'isSucceeded' => TRUE,
                        'message'=>"Experience Details successfully saved.",
                        "experience_id"=>$request->exp_id,
                    ]);
        }
    } 
      /*
    * New Update Experience function
    */
    public function updateExperienceNew(Request $request){ 
        if($request->ajax()) { 
            $data = $request->except(['_token','exp_id','preloaded','newfile']); 
             
            $folderPath = 'pages/experiences';
            // Saving data into multiple tables
            try {
                DB::transaction(function () use($data,$request,$folderPath) {
                    if(isset($data['data']['experiences']) && !isset($data['data']['experiences']['experience_feature_image'])){
                        $data['data']['experiences']['user_id'] = Auth::user()->id;
                        $data['data']['experiences']['updated_at'] = date('Y-m-d H:i:s');
                         if(empty($data['data']['experiences']['experience_high_price'])){
                                $data['data']['experiences']['experience_high_price'] = 0;
                          }
                         if($data['data']['experiences']['experience_type']==2){
                                $data['data']['experiences']['experience_low_price'] = 0;
                          }
                        $oldData= DB::table('experiences')->pluck('deleted_at','ip')->where(['id' => $request->exp_id])->first();
                    
                        $data['data']['experiences']['id'] = $request->exp_id; 
                        $data['data']['experiences']['status'] = 0; 
                        // Merge the array old and new data to save in temp table
                        $collection = collect($oldData);
                        $merged = $collection->merge($data['data']['experiences']); 

                         DB::table('experience_aprrovals')->updateOrInsert(['id'=>$request->exp_id],$merged->all()); 
                        if(isset($data['data']['experiences']['experience_type']))
                            { 
                                 if($data['data']['experiences']['experience_type']==2)
                                {    
                                    if(!empty($data['data']['experience_additional_price']['additional_person'])){
                                         $data['data']['experience_additional_price']['created_at'] = date('Y-m-d H:i:s');
                                         $data['data']['experience_additional_price']['updated_at'] = date('Y-m-d H:i:s');
                                          DB::table('exp_additional_price_apps')->where('experience_id', '=', $request->exp_id)->delete();
                                         $additionalPerson=$data['data']['experience_additional_price'];
                                        foreach($data['data']['experience_additional_price']['additional_person'] as $key=>$person)
                                        {
                                            $data['data']['experience_additional_price']['additional_person'] =$additionalPerson['additional_person'][$key];

                                            $data['data']['experience_additional_price']['price_per_person'] =$additionalPerson['price_per_person'][$key];
                                            $data['data']['experience_additional_price']['additional_price'] =$additionalPerson['additional_price'][$key];
                                            $data['data']['experience_additional_price']['experience_id'] = $request->exp_id;
                                             $experienceImages = DB::table('exp_additional_price_apps')->insert($data['data']['experience_additional_price']);
                                        }
                                    }
                                }
                             }
                        }
                    if(isset($data['data']['additional_detail'])){
                       $oldDataMeta= DB::table('experience_metas')->pluck('deleted_at','ip')->where(['experience_id' => $request->exp_id])->first(); 

                        $collectionMeta = collect($oldDataMeta); 
                         $data['data']['additional_detail']['experience_id'] =$request->exp_id;  
                          $mergedMeta = $collectionMeta->merge($data['data']['additional_detail']);
                         DB::table('experience_meta_aprrovals')->updateOrInsert(['experience_id'=>$request->exp_id],$mergedMeta->all());
                    }
                    if(isset($data['data']['experience_feature_relations'])){
                        $data['data']['experience_feature_relations']['updated_at'] = date('Y-m-d H:i:s');
                        $data['data']['experience_feature_relations']['created_at'] = date('Y-m-d H:i:s');   
                        if(!empty($data['data']['experience_feature_relations']['experience_feature_id'])){ 
                            DB::table('exp_feature_relation_apps')->where('experience_id', '=', $request->exp_id)->delete();
                            $features=$data['data']['experience_feature_relations']['experience_feature_id'];
                            foreach($features as $feat)
                            {
                        $data['data']['experience_feature_relations']['experience_id'] = $request->exp_id;
                        $data['data']['experience_feature_relations']['experience_feature_id'] = $feat;
                        $experienceFeatureRelations = DB::table('exp_feature_relation_apps')->insert($data['data']['experience_feature_relations']);
                            }
                        }
                     }
                    if(isset($data['data']['experience_category_relation'])){
                         $data['data']['experience_category_relation']['updated_at'] = date('Y-m-d H:i:s');
                         $data['data']['experience_category_relation']['created_at'] = date('Y-m-d H:i:s');
                        if(!empty($data['data']['experience_category_relation']['experience_category_id'])){ 
                            DB::table('exp_category_relation_apps')->where('experience_id', '=', $request->exp_id)->delete();
                            $categories=$data['data']['experience_category_relation']['experience_category_id'];
                            foreach($categories as $cat)
                            {
                                $data['data']['experience_category_relation']['experience_category_id'] = $cat;
                                $data['data']['experience_category_relation']['experience_id'] = $request->exp_id;
                                $experienceImages = DB::table('exp_category_relation_apps')->insert($data['data']['experience_category_relation']);
                            }
                        }
                    }
                    if(isset($data['data']['experiences']['experience_feature_image'])){
                        if(isset($request->newFile))
                        {
                            $cropImage= $request->newFile;
                             
                            $data['data']['experiences']['experience_feature_image'] = $this->imageUpload($request,$folderPath,'newFile'); 
                        }else{
                             $data['data']['experiences']['experience_feature_image'] = $this->imageUpload($request,$folderPath,'data.experiences.experience_feature_image'); 
                        }
                           
                            DB::table('experiences')->where(['id' => $request->exp_id])->update($data['data']['experiences']);  
                        }
                    
                });
            } catch (\Exception $e) {
                echo $e->getMessage();
                die;
                return response()->json([
                        'isSucceeded' => FALSE,
                        'message'=>$e->getMessage()
                    ]);
            }
           return response()->json([
                        'isSucceeded' => TRUE,
                        'message'=>"Experience Details successfully saved.",
                        "experience_id"=>$request->exp_id,
                    ]);
        }
    } 
    /*
    * View experience Detail form by scout
    */
    public function viewExperienceDetail(Request $request,$id){
         if($request->isMethod('get')){
             $experience = Experience::with('host')->where(['id' => base64_decode($id)])->first();
            return view('scout.experience_detail',compact('experience'));
        }
        
    } 
    /*
    * Delete experience Detail form by scout
    */
    public function deleteExperience(Request $request){
        if($request->isMethod('post')){
            if(!empty($request->exp_id)){
                $del = Experience::where(['id' => base64_decode($request->exp_id)])->delete();
                if($del){
                    return response()->json([
                        'isSucceeded' => TRUE,
                        'message'=>"successfully Deleted."
                    ]);
                }else{
                    return response()->json([
                        'isSucceeded' => FALSE,
                        'message'=>"Something went wrong."
                    ]);
                }    
            }
        }
        
    } 
    /*
    * Delete experience Detail form by scout
    */
    public function CheckScheduleExistence(Request $request){
        if($request->ajax()){
            $start_date = Carbon::createFromFormat('d-m-Y', $request->start_date)->format('Y-m-d');
            $dateCheck = ExperienceSchedule::where(['start_date' => $start_date,'experience_id'=>$request->exp_id])->first();
            if ($dateCheck) { 
                echo 'false';  
            } 
            else { 
                echo 'true'; 
            }
        }
        
    }
     /*
    * Delete experience Detail form by scout
    */
    /*
    * Add Update Experience Schedule
    */
    public function CreateUpdateSchedule(Request $request){
        if($request->ajax()){
          
            $recurrences=[];
            $data = $request->except(['_token','select']); 
            $data['recurrence']=($request->schedule_type==1 || $request->schedule_type==3)? 0 : $request->recurrence; 
            $data['created_by']=Auth::user()->id;
            $data['end_date'] = (!empty($request->end_date))? $request->end_date : null ;
            $data['start_date'] = $request->start_date;
            $data['start_time'] = (!empty($request->start_time))? $request->start_time : null;
            $data['end_time'] = (!empty($request->end_time))? $request->end_time : null;
          if($request->schedule_type==2 || $request->schedule_type==3)
            {
                    $existSlots= $this->getAlreadyExistSlots($data['experience_id'],$data['start_date'],$data['end_date'],$data['start_time'],$data['end_time']); 
         
                    if($existSlots)
                    {
                        return response()->json([
                                    'isSucceeded' => FALSE,
                                    'message'=>"Time slot already exist",
                                    'alreadyExists'=>TRUE,
                                ]);
                    } 
           } 
            if($request->schedule_type==2 || $request->schedule_type==1)
            {
                $schedule=$this->getRecurrence($data);
            }
            else if($request->schedule_type==3)
            {
                $data['end_date']=$data['start_date'];
                $data['day']=Carbon::createFromFormat('Y-m-d', $data['start_date'])->format('l');
                //  if($this->scheduleExitence($data['start_date'],$data['experience_id'])==FALSE) {
                    $expDetail=Experience::where(['id'=>$data['experience_id']])->first();
                $expDuration = $expDetail->experience_duration;
                $interval = new DateInterval('PT'.$expDuration.'H');
                $freshExperienceDuration = $expDuration * 60;
                 $startDateTime = new DateTime($data['start_date'].' '.$data['start_time']);
                $endDateTime   = new DateTime($data['start_date'].' '.$data['end_time']);
                $to_time = strtotime($data['start_date'].' '.$data['end_time']);
                $from_time = strtotime($data['start_date'].' '.$data['start_time']);
                $totalDuration = round(abs($to_time - $from_time) / 60,2);
                $diffr = floor($totalDuration / $freshExperienceDuration);
                $diffr = abs($diffr);
                $times    = new DatePeriod($startDateTime, $interval, $endDateTime);
                   if($diffr>0){  
                    $j=0; 
                   foreach($times as $key=>$time) {
                       if($j<$diffr){
                       $data['start_time']=$time->format('H:i:s');
                       $data['end_time']=$time->add($interval)->format('H:i:s');
                   $schedule= ExperienceSchedule::create($data);
                    $j++;
                       }
                   }
                   }
                // }
            }
           if(isset($schedule))
           {
                return response()->json([
                    'isSucceeded' => TRUE,
                    'message'=>"Schedule successfully created",
                    'experience_id'=>$request->experience_id,

                    ]);
           }else{
            return response()->json([
                        'isSucceeded' => FALSE,
                        'message'=>"Something went wrong."
                    ]);
           }
           
        }
        
    }
    /* Update Schedule */
    public function updateSchedule(Request $request)
    {
        if($request->ajax()){
            $data = $request->except(['_token','select','schedule_id','schedule_exp_id']);
            $data['start_time'] = (!empty($request->start_time))? $request->start_time : null;
            $data['end_time'] = (!empty($request->end_time))? $request->end_time : null;
             $schedule=  ExperienceSchedule::where('id', '=', $request['schedule_id'])->update($data);
            if($schedule)
           {
                return response()->json([
                    'isSucceeded' => TRUE,
                    'message'=>"Schedule successfully Updated",
                    'experience_id'=>$request->schedule_exp_id,

                    ]);
           }else{
            return response()->json([
                        'isSucceeded' => FALSE,
                        'message'=>"Something went wrong."
                    ]);
           }
        }

    }

    public function getRecurrence($data)
    {
        $expDetail=Experience::where(['id'=>$data['experience_id']])->first();
        $expDuration = $expDetail->experience_duration;
        $interval = new DateInterval('PT'.$expDuration.'H');
        $freshExperienceDuration = $expDuration * 60;
        $startDate = Carbon::parse($data['start_date']);
        $endDate = Carbon::parse($data['end_date']);
        if($data['recurrence']==2){
            $recurrences['times'] = $endDate->diffInWeeks($startDate);
            $recurrences['function']='addWeek';
            $startDateTime = new DateTime($data['start_date'].' '.$data['start_time']);
            $endDateTime   = new DateTime($data['start_date'].' '.$data['end_time']);
            $to_time = strtotime($data['start_date'].' '.$data['end_time']);
            $from_time = strtotime($data['start_date'].' '.$data['start_time']);
        }
        else if($data['recurrence']==3){
            if($data['monthly_type']==1)
            {
                $recurrences['times'] = $endDate->diffInMonths($startDate);
                $recurrences['function']='addMonth';
                $startDateTime = new DateTime($data['start_date'].' '.$data['start_time']);
                $endDateTime   = new DateTime($data['start_date'].' '.$data['end_time']);
                $to_time = strtotime($data['start_date'].' '.$data['end_time']);
                $from_time = strtotime($data['start_date'].' '.$data['start_time']);
            }
            else{
                $recurrences['times'] = $endDate->diffInMonths($startDate);
                $recurrences['function']='addMonth';
                $monthDays=$this->insertSpecificMonth($data,$recurrences);
                if($monthDays) 
                {
                    return TRUE;
                }
                
            }
        }
        else{
            $recurrences['times'] = $endDate->diffInDays($startDate);
            $recurrences['function']='addDay';
            if($data['schedule_type']==1)
            {
                $startDateTime = new DateTime($data['start_date']." 07:00:00");
                $endDateTime   = new DateTime($data['start_date']." 20:00:00"); 
                $to_time = strtotime($data['start_date'].' 20:00:00');
                $from_time = strtotime($data['start_date'].' 07:00:00');
            }
            else{
                $startDateTime = new DateTime($data['start_date'].' '.$data['start_time']);
                $endDateTime   = new DateTime($data['start_date'].' '.$data['end_time']);
                $to_time = strtotime($data['start_date'].' '.$data['end_time']);
                $from_time = strtotime($data['start_date'].' '.$data['start_time']);
            }
           
        }
         $totalDuration = round(abs($to_time - $from_time) / 60,2);
            $diffr = floor($totalDuration / $freshExperienceDuration);
            $diffr = abs($diffr);
        if($recurrences['function']=='addWeek' && !empty($data['week_repeat_days']))
        {
            $weekDays=$this->insertWeekDays($data);
            if($weekDays) 
            {
                 return TRUE;
            }
        }
        else{ 
            $times    = new DatePeriod($startDateTime, $interval, $endDateTime);
            $data['day']=Carbon::createFromFormat('Y-m-d', $data['start_date'])->format('l'); 
            $data['end_date']=$data['start_date'];
           if($recurrences['function']=='addDay' && $data['schedule_type']==1)
            {
            if($this->scheduleExitence($data['start_date'],$data['experience_id'])==FALSE) { 
            if($diffr>0){  
             $j=0; 
            foreach($times as $key=>$time) {
                if($j<$diffr){
                $data['start_time']=$time->format('H:i:s');
                $data['end_time']=$time->add($interval)->format('H:i:s');
            $schedule= ExperienceSchedule::create($data);
             $j++;
                }
            }
            }
            }
            }
            else{

            if($diffr>0){  
             $j=0; 
            foreach($times as $key=>$time) {
                if($j<$diffr){
                $data['start_time']=$time->format('H:i:s');
                $data['end_time']=$time->add($interval)->format('H:i:s');
            $schedule= ExperienceSchedule::create($data);
             $j++;
                }
            }
            }
           }
            for($i = 0; $i <$recurrences['times']; $i++)
            {   
                
                 $data['start_date']=$startDate->{$recurrences['function']}()->format('Y-m-d');
                 $data['day']=Carbon::createFromFormat('Y-m-d', $data['start_date'])->format('l');
                 $data['end_date']= $data['start_date'];
               if($recurrences['function']=='addDay' && $data['schedule_type']==1)
                           {
                               if($this->scheduleExitence($data['start_date'],$data['experience_id'])==FALSE) { 
                                    if($diffr>0){  
                                       $j=0; 
                                      foreach($times as $key=>$time) {
                                          if($j<$diffr){
                                          $data['start_time']=$time->format('H:i:s');
                                          $data['end_time']=$time->add($interval)->format('H:i:s');
                                      $schedule= ExperienceSchedule::create($data);
                                      $j++;
                                          }
                                      }
                                      }
                           }
                           }
                           else{
                        if($diffr>0){  
                                    $j=0;  
                                    foreach($times as $key=>$time) {
                                        if($j<$diffr){
                                        $data['start_time']=$time->format('H:i:s');
                                        $data['end_time']=$time->add($interval)->format('H:i:s');
                                    $schedule= ExperienceSchedule::create($data);
                                     $j++;
                                        }
                                    }
                                    }
                          }
                 
            }  
            return TRUE;
        }
    }
    public function insertWeekDays($data)
    {
        $period = CarbonPeriod::create($data['start_date'], $data['end_date']);
         $expDetail=Experience::where(['id'=>$data['experience_id']])->first();
        $expDuration = $expDetail->experience_duration;
        $interval = new DateInterval('PT'.$expDuration.'H');
        $freshExperienceDuration = $expDuration * 60;
         $startDateTime = new DateTime($data['start_date'].' '.$data['start_time']);
        $endDateTime   = new DateTime($data['start_date'].' '.$data['end_time']);
        $to_time = strtotime($data['start_date'].' '.$data['end_time']);
        $from_time = strtotime($data['start_date'].' '.$data['start_time']);
        $totalDuration = round(abs($to_time - $from_time) / 60,2);
        $diffr = floor($totalDuration / $freshExperienceDuration);
        $diffr = abs($diffr);
        $times    = new DatePeriod($startDateTime, $interval, $endDateTime);
        foreach ($period as $date) {
            if(in_array($date->format('l'), $data['week_repeat_days'])){
                $data['start_date'] = $date->format('Y-m-d');
                $data['end_date']   = $data['start_date'];
                $data['day']        = $date->format('l');
                if($this->scheduleExitence($data['start_date'],$data['experience_id'])==FALSE) {
                 if($diffr>0){  
                   $j=0; 
                  foreach($times as $key=>$time) {
                      if($j<$diffr){
                      $data['start_time']=$time->format('H:i:s');
                      $data['end_time']=$time->add($interval)->format('H:i:s');
                  $schedule= ExperienceSchedule::create($data);
                  $j++;
                      }
                  }
                  }
                }
                
            } 
        }
        return TRUE;
    }

    public function insertSpecificMonth($data,$recurrences)
    {
        $weekdayMap = [
                0 => 'Sunday',
                1 => 'MONDAY',
                2 => 'TUESDAY',
                3 => 'WEDNESDAY',
                4 => 'THURSDAY',
                5 => 'FRIDAY',
                6 => 'SATURDAY',
            ];
        $weekNoMap = [
                1 => 'first',
                2 => 'second',
                3 => 'third',
                4 => 'fourth',
                5 => 'fifth',
            ];
            $expDetail=Experience::where(['id'=>$data['experience_id']])->first();
            $expDuration = $expDetail->experience_duration;
            $interval = new DateInterval('PT'.$expDuration.'H');
            $freshExperienceDuration = $expDuration * 60;
            $dayOfTheWeek = Carbon::parse($data['start_date'])->dayOfWeek;
            $WeekNo = Carbon::parse($data['start_date'])->weekOfMonth;
            $weekday = $weekdayMap[$dayOfTheWeek];
            $weekName = $weekNoMap[$WeekNo];
            $startDateTime = new DateTime($data['start_date'].' '.$data['start_time']);
            $endDateTime   = new DateTime($data['start_date'].' '.$data['end_time']);
            $to_time = strtotime($data['start_date'].' '.$data['end_time']);
            $from_time = strtotime($data['start_date'].' '.$data['start_time']);
            $totalDuration = round(abs($to_time - $from_time) / 60,2);
            $diffr = floor($totalDuration / $freshExperienceDuration);
            $diffr = abs($diffr);
            $times    = new DatePeriod($startDateTime, $interval, $endDateTime); 
           $endDate = Carbon::parse($data['end_date']);
            for($i = 0; $i< $recurrences['times']+1; $i++)
            {
                $startDate = Carbon::parse($data['start_date']);
                $month=$startDate->format('F');
                $year=$startDate->format('Y');
                if($i > 0)
                {
                   $date =$startDate->{$recurrences['function']}();
                   $month=$date->format('F');
                   $year=$date->format('Y');
                }
                if($startDate->lt($endDate))
                {
                $data['start_date']=  Carbon::parse($weekName.' '.$weekday.' of '.$month.' '. $year)->format('Y-m-d');
                $data['day']=Carbon::createFromFormat('Y-m-d', $data['start_date'])->format('l');
                $data['end_date']= $data['start_date'];
                 if($this->scheduleExitence($data['start_date'],$data['experience_id'])==FALSE) {
                if($diffr>0){  
                   $j=0; 
                  foreach($times as $key=>$time) {
                      if($j<$diffr){
                      $data['start_time']=$time->format('H:i:s');
                      $data['end_time']=$time->add($interval)->format('H:i:s');
                  $schedule= ExperienceSchedule::create($data);
                  $j++;
                      }
                  }
                  }
                }
                }
            }
        return TRUE;
    }

    public function scheduleExitence($date,$exp_id)
    {
       $dateCheck = ExperienceSchedule::where(['start_date' => $date,'experience_id'=>$exp_id])->first();
            if ($dateCheck) { 
               return TRUE;  
            } 
            else { 
                return FALSE; 
            }
    }

   
    

    /**
    * Get Schedules by experience id
    */
    public function schedulesByExperienceID(Request $request){
        if($request->ajax()){
            if($request->id){
                $selectedSchedule=[];
                $expData=Experience::with(['experience_schedule'])->where('id',$request->id)->first();
                foreach($expData['experience_schedule'] as $schedule)
                {
                    /*if($schedule->schedule_type==1)
                    {
                        $endtime= $schedule['end_date'];
                        $starttime=$schedule['start_date'];
                    }
                    else{*/
                        $endtime   =$schedule['end_date'].'T'.$schedule['end_time'];
                        $starttime =$schedule['start_date'].'T'.$schedule['start_time'];
                    //}
                    $selectedSchedule[]=array(
                        "id"           =>$schedule['id'],
                        "exp_id"       =>$expData['id'],
                        "sche_type"    =>$schedule['schedule_type'],
                        "title"        => $expData->experience_name,
                        "start"        =>$starttime,
                        "end"          =>$endtime,
                        "className"    =>['selected-eventCls selected-event-'.$schedule['id']]
                    );
                }
                if(count($expData['experience_schedule'])){
                    return response()->json([
                        "message"       => 'Schedule getting successfully.',
                        "isSucceeded"   => TRUE,
                        "schedules"     => $selectedSchedule
                    ]);
                }else{
                    return response()->json([
                        "message"       => 'There is no state related to this country.',
                        "isSucceeded"   => FALSE,
                        "data"          => $html
                    ]);
                }
            }
        }
    }
    /**
    * Get Schedules by Schedule id
    */
    public function scheduleById(Request $request){
        if($request->ajax()){
            if($request->id){
                $selectedSchedule=[];
                $expData=ExperienceSchedule::with(['experience'])->where('id',$request->id)->first();
                if($expData){
                    return response()->json([
                        "message"       => 'Schedule getting successfully.',
                        "isSucceeded"   => TRUE,
                        "schedule"     => $expData
                    ]);
                }else{
                    return response()->json([
                        "message"       => 'There is no state related to this country.',
                        "isSucceeded"   => FALSE,
                        "schedule"      =>""
                    ]);
                }
            }
        }
    }

     /*
    * Delete Schedule By id
    */
    public function deleteSchedule(Request $request){
        if($request->isMethod('post')){
            if(!empty($request->id)){
                $del = ExperienceSchedule::where(['id' => base64_decode($request->id)])->delete();
                if($del){
                    return response()->json([
                        'isSucceeded' => TRUE,
                        'message'=>"successfully Deleted."
                    ]);
                }else{
                    return response()->json([
                        'isSucceeded' => FALSE,
                        'message'=>"Something went wrong."
                    ]);
                }    
            }
        }
    }
     /*
    * Delete Schedule By Experience id
    */
    public function resetAllSchedule(Request $request){
        if($request->isMethod('post')){
            if(!empty($request->exp_id)){
                $data= ExperienceSchedule::where(['experience_id' => base64_decode($request->exp_id)])->get();
                if(!$data->isEmpty())
                {
                $del = ExperienceSchedule::where(['experience_id' => base64_decode($request->exp_id)])->delete();
                if($del){
                    return response()->json([
                        'isSucceeded' => TRUE,
                        'message'=>"Schedule successfully Deleted."
                    ]);
                }else{
                    return response()->json([
                        'isSucceeded' => FALSE,
                        'message'=>"Something went wrong."
                    ]);
                } 
                }else{
                     return response()->json([
                        'isSucceeded' => FALSE,
                        'message'=>"There is no schedule for delete"
                    ]);
                }   
            }
        }
    }
    /**
    * Get Month Options by Start date
    */
    public function getMonthOptions(Request $request){
        if($request->ajax()){
            $weekdayMap = [
                0 => 'Sunday',
                1 => 'Monday',
                2 => 'Tuesday',
                3 => 'Wednesday',
                4 => 'Thursday',
                5 => 'Friday',
                6 => 'Saturday',
            ];
            $weekNoMap = [
                1 => 'First',
                2 => 'Second',
                3 => 'Third',
                4 => 'Fourth',
                5 => 'Fifth',
            ];
             $date=Carbon::createFromFormat('Y-m-d', $request->startDate)->format('j');
             $dayOfTheWeek = Carbon::parse($request->startDate)->dayOfWeek;
            $WeekNo = Carbon::parse($request->startDate)->weekOfMonth;
            $weekday = $weekdayMap[$dayOfTheWeek];
            $weekName= $weekNoMap[$WeekNo];
             
            $html= '<option value="1">Monthly on day '.$date.'</option>';
             $html .= '<option value="2">Monthly on '. $weekName.' '.$weekday.'</option>';
            return response()->json([
                "message"       => 'Monthly option getting successfully.',
                "isSucceeded"   => TRUE,
                "html"          => $html
            ]);
            
        }
    }
    /**
    * Get states accoding to the country id
    */
    public function getState(Request $request){
        if($request->isMethod('post')){
            if($request->countryId){
                $states = State::where(['country_id' => $request->countryId])->get();
                $html = '<option value="">Select State</option>';
                foreach ($states as $key => $state) {
                    $html .= '<option value="'.$state['id'].'">'.$state['name'].'</option>';
                }
                if(count($states)){
                    return response()->json([
                        "message"       => 'States getting successfully.',
                        "isSucceeded"   => TRUE,
                        "data"          => $html
                    ]);
                }else{
                    return response()->json([
                        "message"       => 'There is no state related to this country.',
                        "isSucceeded"   => FALSE,
                        "data"          => $html
                    ]);
                }
            }
        }
    }

    /**
    * Get city accoding to the state id
    */
    public function getCity(Request $request){
        if($request->isMethod('post')){
            if($request->stateId){
                $cities = City::where(['state_id' => $request->stateId])->get();
                $html = '<option value="">Select City</option>';
                foreach ($cities as $key => $city) {
                    $html .= '<option value="'.$city['id'].'">'.$city['name'].'</option>';
                }
                if(count($cities)){
                    return response()->json([
                        "message"       => 'Cities getting successfully.',
                        "isSucceeded"   => TRUE,
                        "data"          => $html
                    ]);
                }else{
                    return response()->json([
                        "message"       => 'There is no city related to this state.',
                        "isSucceeded"   => FALSE,
                        "data"          => $html
                    ]);
                }
            }
        }
    }

    /*
    *Upload Image function
    */
    public function imageUpload($request,$folderPath,$imageName){
            $fName = '';
            $savefileName = '';
            if($request->hasFile($imageName)){
                $originName = $request->file($imageName)->getClientOriginalName();
                $fileName = pathinfo($originName, PATHINFO_FILENAME);
                $extension = $request->file($imageName)->getClientOriginalExtension();
                $fileName = $fileName.'_'.time().'.'.$extension; 
                $fileName = str_replace(' ', '_', $fileName);         
               $fileName = str_replace('list-thumb-', '', $fileName);      
               $fileName = str_replace('detail-thumb-', '', $fileName);      
                if($request->file($imageName)->move(public_path($folderPath), $fileName)){
                    //$fName = "/list-thumb-".$fileName;
                    if($imageName=='data.experiences.experience_feature_image')
                    {
                        $thumbnailpic1='/detail-thumb-'.$fileName;
                         $expdatabaseFile= createThumbnail($folderPath,$fileName,$thumbnailpic1,$width=977,$height=550);
                          $savefileName="detail-thumb-".$fileName;
                    }else{
                    $savefileName=$fileName;
                }
                   $thumbnailpic="/thumbs/list-thumb-".$savefileName;
                $explistFile= createThumbnail($folderPath,$fileName,$thumbnailpic,$width=365,$height=140); 
                $image_path = $folderPath.$fileName;  // Value is not URL but directory file path  
                 
                }
                return $savefileName;
            }
            return $savefileName;       
        }
        public function uploadDropzoneImage(Request $request)
        {
              $folderPath = 'pages/experiences';
               $data = $request->except(['_token','exp_id','dropzone_image']);  
           if($request->hasFile('dropzone_image')){
                $originName = $request->file('dropzone_image')->getClientOriginalName();
                $fileName = pathinfo($originName, PATHINFO_FILENAME);
                $extension = $request->file('dropzone_image')->getClientOriginalExtension();
                $fileName = $fileName.'_'.time().'.'.$extension; 
                $fileName = str_replace(' ', '_', $fileName);         
                $fileName = str_replace('list-thumb-', '', $fileName);        
                if($request->file('dropzone_image')->move(public_path($folderPath), $fileName)){
                    $savefileName=$fileName;
                   $thumbnailpic="/thumbs/list-thumb-".$fileName;
                $explistFile= createThumbnail($folderPath,$fileName,$thumbnailpic,$width=365,$height=140); 
                $image_path = $folderPath.$fileName;  // Value is not URL but directory file path 
                $file_path1 = public_path($folderPath).'/'.$fileName;
                     $size = filesize($file_path1);
                      $file_path = asset($folderPath.'/thumbs/list-thumb-'.$fileName);
                if($request->experience_id!=0)
                {
                    
                $data['created_at'] = date('Y-m-d H:i:s');
                $data['updated_at'] = date('Y-m-d H:i:s');
                $data['ip'] = $request->ip();
                $data['image_name'] = $savefileName;
                $data['experience_id'] = $request->experience_id;


                $id = DB::table('experience_images')->insertGetId($data);
                
                if(Session::has('experience_gallery')){
                Session::forget('experience_gallery');
            }
             return response()->json([
                "message"       => 'Image successfully Cropped.',
                "isSucceeded"   => TRUE,
                "file_name"=>$id,
                "file_size"=>$size,
                "file_path"=>$file_path,
            ]);
               }else{
                $session['created_at'] = date('Y-m-d H:i:s');
                $session['updated_at'] = date('Y-m-d H:i:s');
                $session['ip'] = $request->ip();
                $session['image_name'] = $savefileName;
               Session::push('experience_gallery', $session);

                 return response()->json([
                "message"       => 'Image successfully Cropped.',
                "isSucceeded"   => TRUE,
                "file_name"=>$savefileName,
                "file_size"=>$size,
                "file_path"=>$file_path,
            ]);
                }
               }
               
            }
             
        }
        public function getDropzoneImage(Request $request)
        {
            $target_dir = "pages/experiences";
            $file_list=[];
            $images=ExperienceImage::where(['experience_id'=>$request->experience_id])->get();
           if(count($images) > 0) 
           {
            foreach($images as $img)
            {
                $file_path1 = public_path($target_dir).'/'.$img->image_name;
                 $file_path = asset($target_dir.'/thumbs/list-thumb-'.$img->image_name);
                 $size = filesize($file_path1);
                   $file_list[] = array('name'=>$img->id,'size'=>$size,'path'=>$file_path,'file_id'=>$img->id);
            }
             return response()->json([
                "message"       => 'Image successfully get.',
                "isSucceeded"   => TRUE,
                "images"   => $file_list,
            ]);
           }else{
            return response()->json([
                "message"       => 'No Image Found',
                "isSucceeded"   => FALSE,
            ]);
           }
        } 
        public function deleteDropzoneImage(Request $request)
        {
            $target_dir = "pages/experiences";
            if(is_numeric($request->id))
            {
             $detail = ExperienceImage::where(['id' => $request->id])->first();
              $del = ExperienceImage::where(['id' => $request->id])->delete();
             $image_path = public_path($target_dir).'/'.$detail->image_name; 
             $listimage_path = public_path($target_dir).'/thumbs/list-thumb-'.$detail->image_name; 
               if(File::exists($image_path)) {
                   File::delete($image_path);
               } 
            }else{ 
               if(Session::has('experience_gallery')){
               $experience_gallery = $request->session()->get('experience_gallery');
                foreach($experience_gallery as $key=>$gallery)
                {
                    if($gallery['image_name']==$request->id)
                    {
                         unset($experience_gallery[$key]);
                    }
                }
                }
                $del=TRUE; 
                 $image_path = public_path($target_dir).'/'.$request->id; 
               $listimage_path = public_path($target_dir).'/thumbs/list-thumb-'.$request->id; 
               if(File::exists($image_path)) {
                   File::delete($image_path);
               } 
            }
            
             
           if($del) 
           {
             return response()->json([
                "message"       => 'Image successfully deleted.',
                "isSucceeded"   => TRUE,
            ]);
           }else{
            return response()->json([
                "message"       => 'No Image Found',
                "isSucceeded"   => FALSE,
            ]);
           }
        }


    /*
    * function to create thumbnail
    */
    public function create_thumb($target,$ext,$thumb_path,$w,$h){
            list($w_orig,$h_orig)=getimagesize($target);
            $scale_ratio=$w_orig/$h_orig;
            if(($w/$h)>$scale_ratio)
                $w=$h*$scale_ratio;
            else
                $h=$w/$scale_ratio;
     
        if($w_orig<=$w){
            $w=$w_orig;
            $h=$h_orig;
        }
        $img="";
        if($ext=="gif")
            $img=imagecreatefromgif($target);
        else if($ext=="png")
            $img=imagecreatefrompng($target);
        else if($ext=="jpg")
            $img=imagecreatefromjpeg($target);
     
        $tci=imagecreatetruecolor($w,$h);
        imagecopyresampled($tci,$img,0,0,0,0,$w,$h,$w_orig,$h_orig);
        imagejpeg($tci,$thumb_path,80);
        imagedestroy($tci);
    }

    public function invoice(Request $request,$id)
    {
         $page_title = 'Invoice';
           $user = Auth::user();
         $bookingDetail= ExperienceBooking::with(['experience','traveler','experience_meta_detail'])->where(['id'=>base64_decode($id)])->first();
        return view('scout.invoice',compact('page_title','bookingDetail'));
    }
     /**
    * Generate the PDF  By Scout  @RT
    */
     public function pdfcreate(Request $request,$id)
    {
        $bookingDetail = ExperienceBooking::with(['traveler','experience','experience_meta_detail'])->where('id',base64_decode($id))->first();
        $data = ['bookingDetail' => $bookingDetail]; 
        $pdf = PDF::loadView('scout.invoice_pdf', $data);
        return $pdf->download('localsfromzero_invoce.pdf');
    }
     public function testView(Request $request,$id)
    {
         $bookingDetail= ExperienceBooking::with(['experience','traveler','experience_meta_detail'])->where(['id'=>base64_decode($id)])->first();
        $pdf = PDF::loadView('scout.invoice_pdf',compact('bookingDetail')); 
         // return  $pdf->stream();; 
          return view('scout.invoice_pdf',compact('bookingDetail'));
    }

     public function testView3(Request $request,$id) 
    {
         $bookingDetail= ExperienceBooking::with(['experience','traveler','experience_meta_detail'])->where(['id'=>base64_decode($id)])->first();
        $pdf = PDF::loadView('scout.invoice_pdf',compact('bookingDetail')); 
           return  $pdf->stream();; 
        
    }
     public function testView2(Request $request,$id)
    {   
         $user = Auth::user();
         $bookingDetail= ExperienceBooking::with(['experience','traveler','experience_meta_detail'])->where(['id'=>base64_decode($id)])->first();
        $pdf = PDF::loadView('scout.invoice_pdf',compact('bookingDetail')); 
       return  $pdf->stream();; 
        //  return view('scout.invoice_pdf2',compact('bookingDetail'));
    }

    /**
    * change booking status by scout
    */
    public function changeBookingStatus(Request $request)
    {
        if($request->ajax()){
            $data = $request->except(['_token','booking_id']);
            $msg='';
            $bookingDetail= ExperienceBooking::with(['experience'])->where(['id'=>base64_decode($request->booking_id)])->first();
             $stripe = new \Stripe\StripeClient(config('services.stripe.secret'));
            if($request->status == 1){
                $result=$stripe->charges->capture(
                  $bookingDetail->charge_id,
                  []
                );
              $data['amount_captured']=$result->amount_captured/100;
                $msg = 'Experience approved successfully.';
                $spaceInvoicesApolloController = new SpaceInvoicesApolloController ; 
               $orgData= $spaceInvoicesApolloController->invoicePayments(["admin_org"=>config("services.SPACEINVOICES.SPACEINVOICES_ORGID"),"booking_detail"=>$bookingDetail]); 
                $button="Approved";
                saveNotification('booking_approved_by_scout',array('admin' =>1,'traveler' =>$bookingDetail->user_id , 'host' => $bookingDetail->experience->assigned_host),$bookingDetail->id); 
            }
            elseif($request->status == 2){
                $result= $stripe->refunds->create([
                  'charge' => $bookingDetail->charge_id,
                ]);
                if($result->status == 'succeeded')
                {
                    $data['status']=6;
                }elseif($result->status == 'pending'){
                    $data['status']=7;
                }elseif($result->status == 'failed'){
                    $data['status']=8;
                }elseif($result->status == 'canceled'){
                    $data['status']=9;
                }
                $data['amount_refunded']=$result->amount/100;
                $data['refund_id']=$result->id;
                $msg = 'Experience rejected successfully.';
                $button="Rejected";
                saveNotification('booking_rejected_by_scout',array('admin' =>1,'traveler' =>$bookingDetail->user_id ),$bookingDetail->id); 
            }
           $booking = ExperienceBooking::where(['id' => base64_decode($request->booking_id)])->update($data); 
            if($booking)
            {
                return response()->json([
                    "message"       => $msg,
                    "status"        =>$button,
                    "isSucceeded"   => TRUE,
                ]);
            }
            else{
                return response()->json([
                    "message"       => "Something went wrong please try again.",
                    "isSucceeded"   => FALSE,
                ]);
            }
        }
    }
    public function hostAccountDetail(Request $request,$id)
    {
         $page_title= "Add Host Account Information";
         $userData = User::where(['id' => base64_decode($id)])->first();
         $accountDetail=[];
         if(!empty($userData->user_stripe_account_id) && $userData->user_stripe_account_id!=='NULL')
        {
          try {
            $stripe = new \Stripe\StripeClient(config('services.stripe.secret'));
             $accountDetail= $stripe->accounts->retrieve(
              $userData->user_stripe_account_id,
            );
            
          }
          catch (\Exception $e) {
            return redirect()->back()->with('failure',$e->getMessage());
          }
        }
         return view('scout.host_account_detail',compact('page_title','userData','accountDetail'));
    }
    public function addHostAccountForm(Request $request,$id)
    {
         $page_title= "Add Host Account Information";
          $userData = User::where(['id' => base64_decode($id)])->first();
         return view('scout.host_add_account',compact('page_title','userData'));
               
    }
    public function getAlreadyExistSlots($exp_id,$start_date,$end_date,$start_time,$end_time)
    {    
         $end_date= ($end_date ==null? $start_date:$end_date);
         $ExistData = ExperienceSchedule::whereBetween('start_date',[$start_date,$end_date])->where(['experience_id'=>$exp_id,'start_date'=>$start_date])->whereTime('start_time', '<=', $start_time )->whereTime('end_time', '>=', $end_time)->get(); 
          
        if(count($ExistData) > 0){
            return TRUE;
        }else{
             return FALSE;
        }
    }

    public function getUponExistSlots($exp_id,$start_date,$end_date)
    {    
         $end_date= ($end_date ==null? $start_date:$end_date);
         $ExistData = ExperienceSchedule::whereBetween('start_date',[$start_date,$end_date])->where(['experience_id'=>$exp_id,'start_date'=>$start_date])->get(); 
          
        if(count($ExistData) > 0){
            return TRUE;
        }else{
             return FALSE;
        }
    }

    public function checkHostBank(Request $request){
        if($request->ajax())
        {
            $user= User::where(['id'=>$request->assigned_host])->first(); 
            $check=  checkFiscalization($request->assigned_host); // check the User complate the fisclization process

            if(empty($user->user_stripe_account_id) && $user->user_stripe_account_id==null)
            { 

                $data["message"]=  'No bank detail found';
                $data["isSucceeded"]  = TRUE; 
                $data["fiscalization"]  =  $check; 
                return response()->json($data);
            }
            else{
                $data["message"]=  'Bank detail found';
                $data["isSucceeded"]  = TRUE; 
                $data["fiscalization"]  =  $check; 
                return response()->json($data);
            }
         
        }
    }
    public function checkHostBankRemote(Request $request)
    {
      if($request->ajax())
        {
            $user= User::where(['id'=>$request['data']['experiences']['assigned_host']])->first();
            if(empty($user->user_stripe_account_id) && $user->user_stripe_account_id==null)
            {
                echo 'true';  
            }
            else{
                echo 'true';
            }
        }
    }

   

     /*
    * Update experience Approval status
    */
     public function updateExperienceApprovalStatus(Request $request){
        $page_title = 'List Of Exprience';
        if($request->isMethod('post')){
            $msg = '';
             $data['experience'] = ExperienceAprroval::with('user')->where(['id' => base64_decode($request->id)])->first();
            if($request->status == 1){
                $msg = 'Your experience send for  Approval successfully';
                $data['subject'] = "EXPERIENCE APPROVED";
                $data['status'] = "Approved";
              //  $sendEmail = Mail::to([$data['experience']['user']['email']])->send( new SendMail($data,'experience_approved'));
               saveNotification('experince_reapproval_by_scout',array('admin' =>1),base64_decode($request->id)); 
            }else{
                $msg = 'Experience Not send for Approval successfully';
                $data['subject'] = "EXPERIENCE REJECTED";
                $data['status'] = "Rejected";
               // $sendEmail = Mail::to([$data['experience']['user']['email']])->send( new SendMail($data,'experience_rejected'));
                //saveNotification('experince_reject',array('scout' =>$data['experience']['user']['id']),base64_decode($request->id));  
            }
            $data = ExperienceAprroval::where(['id' => base64_decode($request->id)])->update(['status' =>3]);
            if($data){

                return response()->json([
                    'isSucceeded' => TRUE,
                    'message' => $msg
                ]);
            }else{
                return response()->json([
                    'isSucceeded' => FALSE,
                    'message' => 'Opps there is a problem related to this experience'
                ]);
            }
        }
    }  

}

    
