<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ContentManagementSystem;
use App\AboutUs;
use App\HomePage;
use App\User;
use App\Experience;
use App\ExperienceBooking;
use App\ExperienceCategory;
use App\ExperienceFeature;
use App\Country;
use App\State;
use App\City;
use DataTables;
use App\Http\Traits\CommonTrait;
use Illuminate\Support\Facades\Auth;
use PDF; 
use App\Payment;   
class HostController extends Controller
{

    use CommonTrait;
    /**
    * Host Dashboard Controll Function
    */
    public function dashboard()
    {
        $page_title = 'Dashboard';
        $page_description = 'Some description for the page';

        return view('host.dashboard', compact('page_title', 'page_description'));
    }

    /*
    * Profile Function
    */
    public function myProfile(Request $request){
        $scoutData = User::where(['id' => Auth::user()->id])->first();  
        return view('profile',compact('scoutData'));
    }

    /*
    * Host profile update Function
    */
    public function updateHost(Request $request){
        if($request->isMethod('post')){
             
            $user_data = $this->createOrUpdateHost($request);

            if($user_data['status']){
                return redirect()->route('host-dashboard')->with('success',$user_data['message']);
            }else{
                return redirect()->route('host-dashboard')->with('failure',$user_data['message']);
            }
        }
    }

    /*
    * Host Experience list
    */
    public function experienceList(Request $request){

        if ($request->ajax()) {
            $data = Experience::with('user')->where(['assigned_host' => Auth::user()->id])->get();
            
            return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('action', function($row){
                    $btn = '<a href="'. route('host-view-experience',['id' => base64_encode($row->id)]) .'" class="btn btn-success btn-sm">View</a>';
                    return $btn;
                })
                ->rawColumns(['action'])
                ->make(true);
        }

        return view('host.experience-list');
    }

    /*
    * View experience Detail for host
    */
    public function viewExperienceDetail(Request $request,$id){
         if($request->isMethod('get')){
             $experience = Experience::with('user')->where(['id' => base64_decode($id)])->first();
            return view('host.view-experience',compact('experience'));
        }
        
    } 
     /*
    * Manage Booked Experiences
    */
    public function bookedExperienceList(Request $request){

     $check=  checkFiscalization(); // check the User complate the fisclization process
     if(!empty($check) && $check!==null && $check==1)
        { 
             if($request->isMethod('get')){
                $page_title = 'Manage Bookings';
                $experiences = ExperienceBooking::with(['scout','experience'])->whereHas('experience',function($q){
                    $q->where(['assigned_host'=>Auth::user()->id]);
                })->where('status',1)->get();
                 return view('host.manage_booked_experiences',compact('experiences','page_title'));
               
            }
      }else{

          return redirect()->route('fiscalization')->with('failure',"You have to complete the fiscalization process");
         } 
    }


     /*
    * show booking Details
    */
    public function bookedExperienceDetail(Request $request,$id){
          $page_title = 'Booking Information';
        $experience = ExperienceBooking::with(['scout','experience'])->where('id',base64_decode($id))->first();
            return view('host.booking_detail',compact('experience','page_title'));
        
    }  
     /**
    * Generate the PDF  By Host
    */
     public function pdfcreate(Request $request,$id)
    {
         $user = Auth::user();  
        if(Auth::user()){
            $bookingDetail = ExperienceBooking::with(['traveler','experience','experience_meta_detail'])->where('id',base64_decode($id))->first();
            $PaymentDetail = Payment::where(['booking_id'=>base64_decode($id), 'user_id'=>$user->id])->first(); 
            $data = ['bookingDetail' => $bookingDetail,'PaymentDetail'=>$PaymentDetail]; 
            $pdf = PDF::loadView('host.invoice_pdf', $data);
            return $pdf->download('Invoice.pdf'); 
            // return $pdf->stream ('Invoice.pdf');  
        }  
      
    }
}
