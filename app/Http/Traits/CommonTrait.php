<?php

namespace App\Http\Traits;
use App\User;
use App\UserMeta;
use App\UserBankDetail;
use App\Mail\SendMail;
use Illuminate\Support\Facades\Auth; 
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Hash;
use DB;
use App\Http\Controllers\SpaceInvoicesApolloController;

trait CommonTrait {
	
	//Create or Update Host
	public function createOrUpdateHost($request){

		try{

         
         $user=[];
			//$user = User::where('id', $request['user_id'])->first();
         if($request->filled('user_id'))
         {
           $user = User::with('hostexperiences')->find($request['user_id']);

           $userExperienceLanguages = $user->hostexperiences;
       }
          
			$file_path="users/host";

			$data = array(
			    'user_role' => 4,
			    'status' => 1
			); 

			if($request->filled('user_fname')){
				$data['user_fname'] = $request->user_fname;
			}

			if($request->filled('user_lname')){
				$data['user_lname'] = $request->user_lname;
			}

			if($request->filled('user_address')){
				$data['user_address'] = $request->user_address;
			}

			if($request->filled('user_mobile_code')){
				$data['user_mobile_code'] = $request->user_mobile_code;
			}
			if($request->filled('user_mobile')){
				$data['user_mobile'] = $request->user_mobile;
			}
			if($request->filled('user_primary_language')){
				$data['user_primary_language'] = $request->user_primary_language;
			}

			if($request->filled('user_other_languages')){
				$getUserExpLanguage = $request->user_other_languages;
				$data['user_other_languages'] = implode(',',$request->user_other_languages);
			}
			
			if($request->hasFile('user_image')) {   
				$file = $request->user_image;              
			    $data['user_image'] = $this->profileImageUpload($file,$file_path);
			    $thumbnailpic = "/profile-thumb-".$data['user_image'];
			    createThumbnail($file_path,$data['user_image'],$thumbnailpic,$width=160,$height=160);
			    $data['user_image_path'] = $file_path;
			}

			if (!empty($user)) {

			    $CheckUserLang = explode(",",$user->user_other_languages);
		        if(!empty($user->user_primary_language)) {
		                 in_array($user->user_primary_language, $CheckUserLang) ? '':array_push($CheckUserLang, $user->user_primary_language); 
	               }
			   $user->update($data);
			   $msg="Host updated successfully";
	            if($request->filled('user_other_languages')   || $request->filled('user_primary_language')  ){

			           $userExperience_data['type'] =2;
			           $userExperience_data['created_at'] = date('Y-m-d H:i:s');
			           $userExperience_data['updated_at'] = date('Y-m-d H:i:s');
			          if(!empty($data['user_primary_language'])){   //  if the user_primary_language not in other language array then pust to array 
			               in_array($data['user_primary_language'], $getUserExpLanguage) ? '' : array_push($getUserExpLanguage, $data['user_primary_language']); 
			            }   
			           if(!empty($userExperienceLanguages)){
				               $newLanguage=  array_merge($CheckUserLang, $getUserExpLanguage);
				                 $i=0;
				               if(array_unique($newLanguage)){   // Check the Diffrence between new value and old value
				                     $UserLangDataArray=array();
				                     foreach($userExperienceLanguages as $userExpLang)  {
				                           $i++;
				                             
				                              $userExperience_data['user_id'] = $user->id;
				                              $userExperience_data['experience_id'] =$userExpLang->id;
				                              if(isset($getUserExpLanguage)){
				                            
				                                  foreach($getUserExpLanguage as $lang)
				                                    {
				                                      $language_data[$i] =  $userExperience_data; 
				                                      $language_data[$i]['language_id'] = $lang; 
				                                       $i++;
				                                    } 
				                               } 
				                     }
				                     if(!empty($language_data)){
				                     	
				                          DB::table('experience_language_relations')->where('user_id', '=', $user->id)->delete();
				                          $experienceLanguages = DB::table('experience_language_relations')->insert($language_data);

				                   }
				              } 
				          }

	            }
			   
			} 
			else {

			    $password = substr($request->user_fname,0,3);
			    $password .= substr($request->user_lname,0,3);
			    $password .= "@1234";
			    $hashed_password= Hash::make($password);
			    $data['password'] = $hashed_password;
			    $data['created_by'] = Auth::id();
			    $data['email'] = $request->email;
			    $user = User::create($data);
  
                $ApolloData = $this->apolloCreate($request->all(),'2'); // Create the User Apollo Org And Account 
                $user_meta_data = array();
               if($ApolloData['status']){
	               $user_meta_data['apollo_org_id']= $ApolloData['apollo_org_id'];
	               $user_meta_data['apollo_acc_id']= $ApolloData['apollo_acc_id'];
	               $user_meta_data['user_id']= $user->id;
	              $user_meta1 = UserMeta::insert($user_meta_data); 
                } 
			    $data['password'] = $password;
			    $data['subject'] = "Welcome to the Localsfromzero";
			   // $data = $request->except(['_token']);
			    saveNotification('new_host',array('admin' =>1),$user->id); 
				//$sendEmail = Mail::to([$request->email])->send( new SendMail($data,'new_host_details'));
			    $msg="Host created successfully";
			}
			
              $user_meta_data = array();
			if($request->filled('company_name')){
				$user_meta_data['company_name'] = $request->company_name;
			}

			if($request->filled('company_registration_no')){
				$user_meta_data['company_registration_no'] = $request->company_registration_no;
			}

			if($request->filled('legal_requirements')){
				$user_meta_data['legal_requirements'] = implode(',',$request->legal_requirements);
			}else{
				$user_meta_data['legal_requirements'] =null;
			}

			if($request->filled('company_web_link')){
			    $user_meta_data['company_web_link'] = $request->company_web_link;
			}

			if($request->filled('other_text')){
			    $user_meta_data['other_text'] = $request->other_text;
			}else{
				$user_meta_data['other_text'] =null;
			}

			if($request->filled('guiding_text')){
			    $user_meta_data['guiding_text'] = $request->guiding_text;
			} else{
				$user_meta_data['guiding_text'] =null;
			}
             

			if(count($user_meta_data)>0){
				//User meta info update or create
				$user_meta = UserMeta::updateOrCreate(
				 ['user_id' => $user->id],
				 $user_meta_data
				);
			}
           
			$user_bank_data = array();

			if($request->filled('account_holder')){
				$user_bank_data['account_holder_name'] = $request->account_holder;
			}

			if($request->filled('bank_name')){
				$user_bank_data['bank_name'] = $request->bank_name;
			}

			if($request->filled('swift_code')){
				$user_bank_data['swift_code'] = $request->swift_code;
			}

			if($request->filled('iban')){
				$user_bank_data['iban_number'] = $request->iban;
			}

			if(count($user_bank_data)>0){
				$user_bank_detail = UserBankDetail::updateOrCreate(
					['user_id' => $user->id],
					$user_bank_data
				);
			}

			$response = array();

			$response['status'] = true;

			$response['message'] = $msg;

			return $response;
		}
		catch (\Exception $e) {

			$response['status'] = false;
            print_r($e);
            die;
			$response['message'] = "Opps,Something went wrong,please try again."; 
            return $response;
        }
	}

	 /*
	*Upload Image function
	*/
 	public function profileImageUpload($file,$path){

 		$org_name = $file->getClientOriginalName();
	    
		$org_name = pathinfo($org_name, PATHINFO_FILENAME);
	    
	    $extension = $file->getClientOriginalExtension();

	    $file_name = $org_name.'_'.uniqid().'.'.$extension;

		$file->move( public_path().'/'.$path, $file_name);

		return $file_name;      
	} 
	 /*
	*Create the ORG and account for user
	*/
 	public function apolloCreate($data,$type=1){ 
 		$returnData=['status'=>false,'msg'=>'','data'=>''];
       if($data){
      
 		 $SpaceInvoices_controller = new SpaceInvoicesApolloController; 
 		   if($type==2){
 		 	 $OrgDetails= $SpaceInvoices_controller->organizationCreateHost($data); 
 		 }else{  
            $OrgDetails= $SpaceInvoices_controller->organizationCreate($data); 
         }
        
            if(isset($OrgDetails->id)) {
            	$returnData['status']=TRUE;
            	$returnData['apollo_org_id']=$OrgDetails->id;
             $accountDetails= $SpaceInvoices_controller->createAccount($data,1); 
              if(isset($accountDetails->id)) {
              	$returnData['apollo_acc_id']=$accountDetails->id;
                $inviteOrg= $SpaceInvoices_controller->inviteOrg($OrgDetails->id,$accountDetails->id); 
             } else{
              $returnData['msg']='Account Not Create';
              $returnData['data']= $accountDetails;
             }
            }else{
            $returnData['msg']='Org Not Create';
            $returnData['data']=$OrgDetails;
            }  
	   }
	   return $returnData;
   }
}

?>