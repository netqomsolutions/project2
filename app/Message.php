<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
     /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'status','sender_id','receiver_id','message'
    ];
    public function sender() {
        return $this->belongsTo('App\User','sender_id','id');
    }
    public function receiver() {
        return $this->belongsTo('App\User','receiver_id','id');
    }

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
}
