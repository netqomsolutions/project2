<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ExpCategoryRelationApp extends Model
{
     /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'status', 'created_at', 'updated_at', 'experience_id' , 'experience_category_id'
    ];

}
