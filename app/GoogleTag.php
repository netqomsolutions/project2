<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GoogleTag extends Model
{
     protected $fillable = ['header_tags','footer_tags','created_at','updated_at'];
}
