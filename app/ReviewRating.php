<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ReviewRating extends Model
{
	use SoftDeletes;
	protected $dates = ['deleted_at'];
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillalbe = ['experience_id','user_id','review','rating','created_at','updated_at','review_type','booking_id','review_for_scout','review_for_host'];

    protected $guarded = []; 

    /**
	 * Get the user that owns the review and ratings.
	 */
	public function user() {
	    return $this->belongsTo('App\User');
	}

	/**
	 * Get the review and ratings of experience for homepage
	 */
	public function experience() {
	    return $this->belongsTo('App\Experience');
	}
}
