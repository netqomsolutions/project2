<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ExperienceAprroval extends Model
{
       protected $fillable = [
        'id', 'experience_hint ', 'experience_high_price' ,'experience_name', 'experience_price', 'experience_duration', 'experience_group_size', 'user_id', 'status', 'created_at', 'updated_at', 'experience_description' ,'assigned_host', 'experience_category_id', 'experience_price_vailid_for', 'experience_additional_person', 'experience_booking_in_advance_time','experience_feature_image','experience_overall_rating','experience_start_time','minimum_participant','maximum_participant'
    ];
        /**
     * Get the review of experience for homepage
     */
    public function review_ratings()
    {
        return $this->hasMany('App\ReviewRating');
    }


    /**
     * Get the image of experience for homepage
     */
    public function experience_images()
    {
        return $this->hasMany('App\ExperienceImage');
    } 
    /**
     * Get the category of experience for homepage
     */
    public function experience_category_relation()
    {
        return $this->hasMany('App\ExpCategoryRelationApp');
    }

    /**
	 * Get the user that owns the experiences.
	 */
	public function user() {
	    return $this->belongsTo('App\User','user_id','id');
	}

     /**
     * Get the user that owns the experiences.
     */
    public function host() {
        return $this->belongsTo('App\User','assigned_host','id');
    }


   /**
     * Get the experience_feature_relations of experience 
     */
    public function experience_feature_relations()
    {
        return $this->hasMany('App\ExpFeatureRelationApp','experience_id');
    }

    /**
     * Get the experience_location of experience 
     */
    public function experience_locations(){
        return $this->hasOne('App\ExperienceLocation');
    } 
    /**
     * Get the experience meta detail of experience 
     */
    public function experience_meta_detail(){
        return $this->hasOne('App\ExperienceMetaAprroval','experience_id');
    }

    /**
     * Get the experience_schedule of experience 
     */
    public function experience_schedule()
    {
        return $this->hasMany('App\ExperienceSchedule');
    }
     /**
     * Get the experience_additional_price of experience 
     */
    public function experience_additional_price()
    {
        return $this->hasMany('App\ExpAdditionalPriceApp','experience_id');
    }
    public function experience_language_relations()
    {
        return $this->hasMany('App\ExperienceLanguageRelation','experience_id');
    }

    public function experience_additional_prices(){
        return $this->hasMany('App\ExpAdditionalPriceApp');
    }
    public function experience_scout_booking(){
        return $this->hasMany('App\ExperienceBooking','experience_id','id');
    }
}
