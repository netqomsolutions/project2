<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HomePage extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
           'status','user_id','slug','banner_small_heading','banner_explore_button','banner_title','banner_description','banner_image','meta','explore_title','explore_small_heading','explore_description','explore_video','explore_video_image','explore_background_image','created_at','updated_at' 
    ];
}
