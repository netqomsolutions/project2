<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ExperienceLocation extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id','status', 'created_at', 'updated_at', 'experience_id','location_id'
    ];


    /**
     * Get the image of experience for homepage
     */
    public function location()
    {
        return $this->belong('App\Location');
    }
}
