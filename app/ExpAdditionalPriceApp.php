<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ExpAdditionalPriceApp extends Model
{
       /**
    * The attributes that are mass assignable.
    *
    * @var array
    */
    protected $fillable = [
        'id', 'created_at', 'updated_at', 'experience_id' , 'additional_person','price_per_person','additional_price'
    ];

}
