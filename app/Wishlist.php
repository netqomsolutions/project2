<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Wishlist extends Model
{
   /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'experience_id', 'is_favourite'
    ];
    public function experience()
    {
         return $this->belongsTo('App\Experience');
    }
    public function user() {
        return $this->belongsTo('App\User');
    }


    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
}
