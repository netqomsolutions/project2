<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Auth;
use App\Services\DigitalOceanServerProvider;
use App\Partner;
use App\ContentManagementSystem;
use App\Message;
use DB;

class AppServiceProvider extends ServiceProvider
{
   
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
       
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer(
            '*',  
            function ($view) {
                $socialLinks=ContentManagementSystem::find(7);
                $contact=ContentManagementSystem::find(4);
                if(!empty($contact))
                {
                $condata = json_decode($contact->page_content,true);
                $contact->email=$condata['email'];
                $contact->address=$condata['address'];
                $contact->phone=$condata['phone'];
                $contact->address_line_2 = $condata['address_line_2'];
                $contact->address_link = $condata['address_link'];
                }
                 if(!empty($socialLinks))
                {
                $data = json_decode($socialLinks->page_content,true);
                $socialLinks->facebook=$data['facebook'];
                $socialLinks->instagram=$data['instagram'];
                $socialLinks->twitter=$data['twitter'];
                $socialLinks->linkedin=$data['linkedin'];
                }
                $countMessages = $this->countMessages();
                $header_tag = getSettingMeta('header_tags');
                $footer_tag = getSettingMeta('footer_tags');
                $view->with(['partners'=> Partner::all(),'socialLinks'=> $socialLinks, 'contact'=>$contact,'countMessages' =>$countMessages,'header_tag'=>$header_tag,'footer_tag'=>$footer_tag]);
            }
        );
         

    }

    public function countMessages()
    {
        $user = Auth::user(); 
        if($user){
           $login_id = $user->id;  
        $recid  = Message::where(['sender_id' => $login_id])->select('receiver_id as rc_id')->distinct('rc_id')
        ->get()
        ->toArray();

        if(empty($recid)){
            $recid  = Message::where(['receiver_id' => $login_id])->select('sender_id as rc_id')->distinct('rc_id')
            ->get()
            ->toArray(); 

        } 
        $userList = array();
        $countMessages = array();
        foreach ($recid as $key => $id) {  
            // DB::enableQueryLog();
            $userList[] = DB::table('users')->where(['id' => $id['rc_id']])->get();
            $countMessages[] = DB::table('messages')->where(['sender_id' => $login_id])
            ->where(['receiver_id' => $id['rc_id']])->where(['status' =>'0'])->select('id')->get()->count();
             

        } 
 

        return  $allMessageCount = array_sum($countMessages); 
 
    }else{
      return 0;  
    }
        
        // echo "<pre>"; print_r($countMessages); die;  

        
    }
}
