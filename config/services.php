<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Mailgun, Postmark, AWS and more. This file provides the de facto
    | location for this type of information, allowing packages to have
    | a conventional file to locate the various service credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
        'endpoint' => env('MAILGUN_ENDPOINT', 'api.mailgun.net'),
    ],

    'postmark' => [
        'token' => env('POSTMARK_TOKEN'),
    ],

    'ses' => [
        'key' => env('AWS_ACCESS_KEY_ID'),
        'secret' => env('AWS_SECRET_ACCESS_KEY'),
        'region' => env('AWS_DEFAULT_REGION', 'us-east-1'),
    ],

    'stripe' => [
        'model'  => App\User::class,
        'key' => env('STRIPE_KEY'),
        'secret' => env('STRIPE_SECRET'),
    ],
     'facebook' => [
        'client_id'     => env('FB_ID'),
        'client_secret' => env('FB_SECRET'),
        'redirect'      => env('APP_URL') . 'oauth/facebook/callback',
    ],

    'google' => [
        'client_id'     => env('GL_ID'),
        'client_secret' => env('GL_SECRET'),
        'redirect'      => env('APP_URL') . 'oauth/google/callback',
    ],
    'google_map_key' => [
        'key'     => env('GOOGLE_MAP_KEY'),
    ],
    'SPACEINVOICES' => [
        'SPACEINVOICES_ACCESSTOKEN'     => env('SPACEINVOICES_ACCESSTOKEN'),
        'SPACEINVOICES_ACCOUNT_ID'     => env('SPACEINVOICES_ACCOUNT_ID'),
        'SPACEINVOICES_ORGID'     => env('SPACEINVOICES_ORGID'),
    ],
    'mail_footer_detail' => [
        'email'     => env('MAIL_FOOTER_EMAIL'),
        'phone'     => env('MAIL_FOOTER_PHONE'),
    ],
    'notifications' => [
        'notification_for'=>[
            'new_traveler' => 'admin',  //  signup new treveler
            'new_scout' => 'admin',  //  add new scout or register
            'new_host' => 'admin',  // add new host
            'scout_message' => 'traveler',  // got new messages 
            'traveler_message' => 'scout',  // got new messages 
            'booking_created'=>'admin,scout',  // on experince booking 
            'booking_approved_by_scout'=>'admin,traveler,host',  // on booking approved 
            'booking_rejected_by_scout'=>'admin,traveler',  // on booking rejected  
            'experince_created' => 'admin',  // experince created by scout 
            'experince_approve' => 'scout',  // experince approved by admin  
            'experince_reject' => 'scout',  // experince rejected pproved by admin 
            'experince_reapproval_by_scout' => 'admin',  // experince rejected pproved by admin 
            'experince_reapproval_success' => 'scout',  // experince rejected pproved by admin 
            'experince_reapproval_failed' => 'scout',  // experince rejected pproved by admin 
        ],
        'admin' => [
            'new_traveler' => 'New Traveler Registered {name}',
            'new_traveler_url' => '/admin/traveler-list',
            'new_scout' => 'New Scout "{name}" Registered ',
            'new_scout_url' => '/admin/scouts-request-list',
            'new_host' => 'New Host Registered {name}',
            'new_host_url' => '/admin/host-list',
            'booking_created' => 'New booking received',
            'booking_created_url' => '/admin/bookings',
            'booking_approved_by_scout' => '{name} Approved New Booking.',
            'booking_approved_by_scout_url' => '/admin/bookings',
            'booking_rejected_by_scout' => ' Booking Rejected by {name}',
            'booking_rejected_by_scout_url' => '/admin/bookings',
            'experince_created' => 'New Experience created by {name}', 
            'experince_created_url' => '/admin/experience-list',
            'experince_reapproval_by_scout'=>'{name} send reapproval experience request.',
            'experince_reapproval_by_scout_url'=>'/admin/experience-approval-list' 

        ],
       'traveler' => [
            'booking_approved_by_scout' => 'Your booking is Approved By {name}',
            'booking_approved_by_scout_url' => '/traveler/booked-experiences-list',
            'booking_rejected_by_scout' => 'Your booking is Rejected By {name}',
            'booking_rejected_by_scout_url' => '/traveler/booked-experiences-list',
            'scout_message' => 'You have received message from {name}',
            'scout_message_url' => '/traveler/dashboard',
        ],
        'host' => [
            'booking_approved_by_scout' => '{name} Assigned new booking',
            'booking_approved_by_scout_url' => '/host/bookings', 
        ],
        'scout' =>[
            'experince_approve' => 'Your Experience is Approved By Admin',
            'experince_approve_url' => '/scout/view-experience',
            'experince_reapproval_success'=>'Your Experience Succesfully Reapproved By Admin.',
            'experince_reapproval_success_url'=>'/scout/view-experience',
            'experince_reapproval_failed'=>'Your Experience Reapproved Rejected By Admin.',
            'experince_reapproval_failed_url'=>'/scout/view-experience',
            'experince_reject' => 'Your Experience is Rejected By Admin',
            'experince_reject_url' => '/scout/view-experience',
            'booking_created' => 'New booking received ',
            'booking_created_url' => '/scout/new-bookings',
            'traveler_message' => 'You have received message from {name}',
            'traveler_message_url' => '/scout/dashboard',
        ],

    ],

];
