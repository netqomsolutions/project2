<?php
// Aside menuer()->id);
$url = 'traveler';

return [

    'items' => [

        // Dashboard
        [
            'title' => 'Dashboard',
            'root' => true,
            'icon' => 'public/media/svg/icons/Design/Dashboard.svg', 
            'page' => $url.'/dashboard',
            'new-tab' => false,
        ],
        [
            'title' => 'Wishlist',
            'root' => true,
            'icon' => 'public/media/svg/icons/Design/wishlist.svg', 
            'page' => $url.'/wishlist',
            'new-tab' => false,
        ],
        [
            'title' => 'Booked Experiences',
            'root' => true,
            'icon' => 'public/media/svg/icons/Design/booking.svg', 
            'page' => $url.'/booked-experiences-list',
            'new-tab' => false,
        ],
     ]

];
