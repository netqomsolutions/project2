<?php
$url = 'admin';

return [

    'items' => [
        // Dashboard
        [
            'title' => 'Dashboard',
            'root' => true,
            'icon' => 'public/media/svg/icons/Design/Dashboard.svg', 
            'page' => $url.'/dashboard',
            'new-tab' => false,
        ],

        // Custom
        [
            'title' => 'CMS',
            'user_role'=>'1',
            'desc' => '',
            'icon' => 'public/media/svg/icons/Design/Cms.svg',
            'bullet' => 'dot',
            'root' => true,
            'submenu' => [
                [
                    'title' => 'Home Page',
                    'page' => $url.'/edit-homepage'
                ],
                [
                    'title' => 'About Us',
                    'page' => $url.'/edit-about-us'
                ],
                [
                    'title' => 'How It Works',
                    'page' => $url.'/edit-how-it-works'
                ], 
                [
                    'title' => 'Contact Us',
                    'page' => $url.'/edit-contact-us'
                ],  
                [
                    'title' => 'Privacy Policy',
                    'page' => $url.'/edit-privacy-policy'
                ],
                [
                    'title' => 'Cancellation Policy',
                    'page' => $url.'/edit-cancellation-policy'
                ],
                [
                    'title' => 'Term Of Use',
                    'page' => $url.'/edit-term-of-use'
                ],
                [
                    'title' => 'Blog',
                      'desc' => '',
                     'icon' => 'public/media/svg/icons/Design/Cms.svg',
                      'bullet' => 'dot',
                      'root' => true,
                    'submenu'=>[
                        [
                    'title' => 'Manage Categories',
                    'page' => $url.'/edit-blog-page'
                ],  
                [
                    'title' => 'Manage Blogs',
                    'page' => $url.'/blog-list'
                ],
                    ],
                ],
                [
                    'title' => 'Manage Google Analytics',
                    'page' => $url.'/manage-google-tags'
                ],  
            ]

        ],

        [
            'title' => 'Experience',
            'user_role'=>'1',
            'desc' => '',
            'icon' => 'public/media/svg/icons/Design/Experience.svg',
            'bullet' => 'dot',
            'root' => true,
            'submenu' => [
                [
                    'title' => 'Manage Categories',
                    'page' => $url.'/experience-categories'
                ],
                [
                    'title' => 'View Experiences',
                    'page' => $url.'/experience-list'
                ], 
                [
                    'title' => 'View Experiences Re-approval',
                    'page' => $url.'/experience-approval-list'
                ], 
               
            ]
        ],
         [
            'title' => 'Scouts',
            'user_role'=>'1',
            'desc' => '',
            'icon' => 'public/media/svg/icons/Design/Scout.svg',
            'bullet' => 'dot',
            'root' => true,
            'submenu' => [
                [
                    'title' => 'View Scouts',
                    'page' => $url.'/scouts-list'
                ],
                [
                    'title' => 'View Student Scouts',
                    'page' => $url.'/student-scouts-list'
                ],
                [
                    'title' => 'View Scouts Request',
                    'page' => $url.'/scouts-request-list'
                ]
            ]
        ],
        [
            'title' => 'Hosts',
            'root' => true,
            'icon' => 'public/media/svg/icons/Design/Scout.svg', 
            'page' => $url.'/host-list',
            'new-tab' => false,
        ],
        [
            'title' => 'Travelers',
            'root' => true,
            'icon' => 'public/media/svg/icons/Design/Scout.svg', 
            'page' => $url.'/traveler-list',
            'new-tab' => false, 
            'submenu' => [
                [
                    'title' => 'Travelers',
                    'page' => $url.'/traveler-list'
                ], 
                [
                    'title' => 'View Booking',
                    'page' => $url.'/bookings'
                ],
               
            ]
        ],
         
      
    ]

];
