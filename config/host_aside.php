<?php
// Aside menuer()->id);
$url = 'host';

return [

    'items' => [

     [
            'title' => 'Manage Profile',
            'user_role'=>'4',
            'desc' => '',
            'icon' => 'public/media/svg/icons/Design/Experience.svg',
            'bullet' => 'dot',
            'root' => true,
            'page' => 'my-profile',
            'new-tab' => false,
        ],
        [
            'title' => 'Manage Bookings',
            'user_role'=>'4',
            'desc' => '',
            'icon' => 'public/media/svg/icons/Design/Experience.svg',
            'bullet' => 'dot',
            'root' => true,
            'page' => $url.'/bookings',
            'new-tab' => false,
        ],
    ]

];
