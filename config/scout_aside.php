<?php
// Aside menuer()->id);
$url = 'scout';

return [

    'items' => [

        // Custom
        [
            'title' => 'Dashboard',
            'root' => true,
            'icon' => 'public/media/svg/icons/Design/Dashboard.svg', 
            'page' => $url.'/dashboard',
            'new-tab' => false,
        ],
        [
            'title' => 'Experiences',
            'user_role'=>'3',
            'desc' => '',
            'icon' => 'public/media/svg/icons/Design/Experience.svg',
            'bullet' => 'dot',
            'root' => true,
            'submenu' => [
                [
                    'title' => 'Add Experience',
                    'page' => $url.'/add-experience'
                ],
                [
                    'title' => 'List of Experiences',
                    'page' => $url.'/view-experience'
                ] ,
                [
                    'title' => 'Booked Experiences',
                    'page' => $url.'/booked-experience'
                ],
                [
                    'title' => 'New Bookings',
                    'page' => $url.'/new-bookings'
                ]
            ]
        ],

        [
            'title' => 'Manage Host',
            'user_role'=>'3',
            'desc' => '',
            'icon' => 'public/media/svg/icons/Design/host.svg',
            'bullet' => 'dot',
            'root' => true,
            'submenu' => [
                [
                    'title' => 'Add Host',
                    'page' => $url.'/add-host'
                ],
                [
                    'title' => 'List of Host',
                    'page' => $url.'/host-list'
                ]
            ]
        ],
        // [
        //     'title' => 'Inbox',
        //     'class'=>'inbox-notification',
        //     'root' => true,
        //     'icon' => 'public/media/svg/icons/Design/inbox.svg', 
        //     'page' => $url.'/inbox',
        //     'new-tab' => false,
        // ],
        
     ]

];
