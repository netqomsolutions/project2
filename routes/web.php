<?php

use Illuminate\Support\Facades\Route;

//use App\Http\Controllers\Auth\RegisterController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::any('/register/scout', 'Auth\RegisterController@scoutRegisterForm')->name('scout-register');

Route::any('/check_forEmail', 'Auth\RegisterController@check_forEmail')->name('check-for-Email');

Route::any('/check_forMobile', 'Auth\RegisterController@check_forMobile')->name('check-for-Mobile');

Route::post('/check-unique-Email', 'CommonController@checkUniqueEmail')->name('check-unique-email');

Route::post('/check-unique-Mobile', 'CommonController@checkUniqueMobile')->name('check-unique-mobile');

Route::get('/payment-page','FrontendController@paymentPage')->name('payment-page');
Route::get('/auth/social', 'auth\logincontroller@show')->name('social.login');
Route::get('/oauth/{driver}', 'auth\logincontroller@redirecttoprovider')->name('social.oauth');
Route::get('/oauth/{driver}/callback', 'auth\logincontroller@handleprovidercallback')->name('social.callback');


Route::get('/', 'FrontendController@index')->name('/');
Route::get('/about-us', 'FrontendController@aboutUs')->name('about-us');
Route::any('/get-booking-slots', 'FrontendController@getBookingSlots')->name('get-booking-slots');
Route::get('/experience-list','FrontendController@experienceList')->name('experiences-list');
Route::get('/experience-detail/{id}','FrontendController@experienceDetail')->name('experience-detail');
Route::get('/get-experience-slots/{id}','FrontendController@getAllSlotsByExpId')->name('get-experience-slots');
Route::post('/experience-detail-by-id','FrontendController@experienceDetailByid')->name('get-experience-detail');
Route::get('/mat','FrontendController@experienceMat')->name('experiences-mat');
Route::get('/scout-mat','FrontendController@scoutExperienceMat')->name('scout-experiences-mat');
Route::get('/get-scout','FrontendController@getAllScouts')->name('get-all-scout');
Route::post('/popup-scout-detail','FrontendController@scoutPopupDetail')->name('popup-scout-detail');
Route::get('/privacy-policy', 'FrontendController@privacyPolicy')->name('privacy-policy'); 
Route::get('/cancellation-policy', 'FrontendController@cancellationPolicy')->name('cancellation-policy'); 
Route::get('/terms-of-use', 'FrontendController@termsConditions')->name('terms-of-use');
Route::get('/how-it-works', 'FrontendController@howitWorks')->name('how-it-works');
Route::get('/contact-us', 'FrontendController@contactUs')->name('contact-us');
Route::get('/blogs', 'FrontendController@blogList')->name('blogs');
Route::get('/blog-detail/{id?}', 'FrontendController@blogDetail')->name('blog-detail');
Route::get('/scout-detail/{id}','FrontendController@userDetail')->name('scout-detail');
Route::get('/coordinator-detail/{id}','FrontendController@userDetail')->name('coordinator-detail');
Route::post('/check-mobile-existence', 'FrontendController@checkMobileExistence')->name('mobile-existence');
Route::post('/contact-us-post', 'FrontendController@contactUsPost')->name('contact-us-post');

Route::get('/my-profile', 'UserController@myProfile')->name('user-profile');
Route::any('/social-activity', 'UserController@socialActivity')->name('social-activity');
Route::any('/fiscalization', 'UserController@fiscalization')->name('fiscalization');
Route::any('/fiscalization-request', 'UserController@fiscalizationRequest')->name('fiscalization-request');
Route::any('/upload-certificate', 'UserController@uploadFursCertificate')->name('upload-certificate');
Route::any('/add-business-premises', 'UserController@addBusinessPremises')->name('add-business-premises');
Route::any('/add-devices', 'UserController@addElectronicDevices')->name('add-devices');
Route::post('/check-user-mobile-existence', 'UserController@checkMobileExistence')->name('user-mobile-existence');
Route::post('/check-old-password', 'UserController@checkOldPassword')->name('user-check-old-password');
Route::post('/user-change-password', 'UserController@changePassword')->name('user-change-password');
Route::post('/profile-update','UserController@updateProfile')->name('profile-update');
Route::get('/profile-edit', 'UserController@editProfile')->name('user-profile-edit');
Route::get('/change-password-form','UserController@changePasswordForm')->name('user-change-password-form');
Route::get('/account-information','UserController@accountInformation')->name('account-information');
Route::get('/add-account-information','UserController@addAccountInformation')->name('add-account-information');
Route::post('/create-update-account','UserController@accountCreateUpdate')->name('create-update-account');
Route::post('/delete-account','UserController@accountDelete')->name('delete-user-bank-account');
Route::post('/check-host-bank','ScoutController@checkHostBank')->name('check-host-bank');
Route::post('/check-host-bank-remote','ScoutController@checkHostBankRemote')->name('check-host-bank-remote');
Route::post('/create-update-schedule','ScoutController@CreateUpdateSchedule')->name('create-update-schedule');
Route::post('/check-same-date-schedule','ScoutController@CheckScheduleExistence')->name('check-same-date-schedule');
Route::post('/validate-schedule-time','ScoutController@ValidateScheduleTime')->name('validate-schedule-time');
Route::post('/schedule-by-experience-id','ScoutController@schedulesByExperienceID')->name('schedule-by-experience-id');
Route::post('/get-month-options','ScoutController@getMonthOptions')->name('get-month-options');
Route::post('/schedule-by-id','ScoutController@scheduleById')->name('schedule-by-id');
Route::post('/delete-schedule','ScoutController@deleteSchedule')->name('delete-schedule');
Route::post('/delete-exp-schedule','ScoutController@resetAllSchedule')->name('delete-exp-schedule');
Route::post('/update-schedule','ScoutController@updateSchedule')->name('update-schedule');
Route::post('/experience-booking','FrontendController@createExperienceBooking')->name('create-experience-booking');
Route::post('/get-payment-total','FrontendController@getExperiencePayment')->name('get-payment-total');
Route::post('/confirm-and-pay','FrontendController@bookingConfirm')->name('booking-confirm-and-pay');
Route::get('/thank-you','FrontendController@thankYouForBooking')->name('thank-you-for-booking');
Route::get('/cancel-booking-authorization','FrontendController@cancelBookingAuthorization')->name('cancel-booking-authorization');
Route::get('/split-booking-payments','FrontendController@splitBookingPayment')->name('split-booking-payments');
Route::get('/thank-you-for-registration','FrontendController@thankYouForRegistration')->name('thank-you-for-registration');

Route::get('/text','PagesController@test');
//Demo routes
Route::get('/datatables', 'PagesController@datatables');
Route::get('/ktdatatables', 'PagesController@ktDatatables');
Route::get('/select2', 'PagesController@select2');
Route::get('/icons/custom-icons', 'PagesController@customIcons');
Route::get('/icons/flaticon', 'PagesController@flaticon');
Route::get('/icons/fontawesome', 'PagesController@fontawesome');
Route::get('/icons/lineawesome', 'PagesController@lineawesome');
Route::get('/icons/socicons', 'PagesController@socicons');
Route::get('/icons/svg', 'PagesController@svg');

Route::post('/add-to-wishlist','FrontendController@AddToWishList')->name('add-to-wishlist');
Route::any('/realTimeNotificationAjax','FrontendController@realTimeNotificationAjax')->name('realTimeNotificationAjax');
Route::any('/notification_seen_update','FrontendController@notification_seen_update')->name('notification_seen_update');
Route::any('/clear_up_notifications','FrontendController@clear_up_notification')->name('clear_up_notifications');

//Quick search dummy route to display html elements in search dropdown (header search)
Route::get('/quick-search', 'PagesController@quickSearch')->name('quick-search');

Auth::routes();

//Common routes for all roles
Route::get('logout', '\App\Http\Controllers\Auth\LoginController@logout')->name('logout');

Route::post('ckeditor/upload', 'CkeditorController@upload')->name('ckeditor.upload');

//Set the configuratio. Only verified email person access url
Auth::routes(['verify' => true]);
 Route::any('/delete-row', 'AdminController@deleteData')->name('delete-row');

// Admin Routes Only
Route::group(['prefix' => 'admin','middleware' => ['auth','App\Http\Middleware\AdminMiddleware']], function() {

    Route::get('/dashboard', 'AdminController@dashboard')->name('admin-dashboard');    
    
    Route::get('/scouts-list', 'AdminController@scoutsList')->name('admin-scouts'); 
    Route::get('/student-scouts-list', 'AdminController@studentScoutsList')->name('admin-student-scouts'); 
    Route::get('/scouts-request-list', 'AdminController@scoutsRequestList')->name('admin-scouts-request'); 
    Route::get('/traveler-list', 'AdminController@travelerList')->name('admin-travelers'); 

    Route::get('/user-detail/{id}', 'AdminController@scoutDetail')->name('admin-scout-detail');
    Route::get('/scout-approval/{id}', 'AdminController@scoutApproval')->name('admin-scout-approval');
    Route::get('/edit-scout/{id}', 'AdminController@editScout')->name('admin-edit-scout'); 
    Route::post('/delete-scout','AdminController@deleteScout')->name('admin-delete-scout'); 
    Route::post('/update-scout', 'AdminController@updateScout')->name('admin-update-scout');
    Route::post('/update-scout-request-status', 'AdminController@updateScoutRequestStatus')->name('admin-update-scout-request-status');
    Route::post('/update-scout-request-commission', 'AdminController@updateScoutComission')->name('admin-update-scout-request-commission');
    Route::post('/update-scout-status', 'AdminController@updateScoutStatus')->name('admin-update-scout-status');


    Route::get('/edit-about-us', 'AdminController@editAboutUs')->name('admin-edit-about-us');
    Route::post('/update-about-us', 'AdminController@updateAboutUs')->name('admin-update-about-us');

    Route::post('/add-update-testimonial', 'AdminController@addUpdateTestimonials')->name('admin-add-update-testimonial');
    Route::get('/get-testimonial', 'AdminController@getTestimonialList')->name('admin-testimonials'); 


    Route::post('/get-testimonial-by-id', 'AdminController@getTestimonialById')->name('admin-get-testimonial');
    Route::post('/delete-testimonial-by-id', 'AdminController@deleteTestimonialById')->name('admin-delete-testimonial');

    Route::get('/edit-homepage', 'AdminController@editHomePage')->name('admin-edit-home-page');
    Route::post('/update-home-page', 'AdminController@updateHomePage')->name('admin-update-home-page');

    Route::get('/partners-list', 'AdminController@partnersList')->name('admin-partners');
    Route::post('/add-partner', 'AdminController@addPartner')->name('admin-add-partner');
    Route::post('/get-partner-by-id', 'AdminController@getPartnerById')->name('admin-get-partner-by-id');
    Route::post('/delete-partner','AdminController@deletePartner')->name('admin-delete-partner');


    Route::post('/send-payment-scout','AdminController@sendPaymentStudentScout')->name('admin-send-payment-scout');
    Route::post('/scout-by-id','AdminController@getUserById')->name('admin-scout-by-id');



    Route::get('/edit-privacy-policy', 'AdminController@editPrivacyPolicy')->name('admin-edit-privacy-policy');
    Route::get('/edit-cancellation-policy', 'AdminController@editCanellationPolicy')->name('admin-edit-cancellation-policy');
    Route::get('/edit-term-of-use', 'AdminController@editTermsOfUse')->name('admin-edit-term-of-use');
    Route::get('/edit-contact-us', 'AdminController@editContactUs')->name('admin-edit-contact-us');
    Route::get('/edit-how-it-works', 'AdminController@editHowItWorks')->name('admin-edit-how-it-works');

    Route::get('/get-scout-tab-list', 'AdminController@scoutTabList')->name('admin-scout-tab-list');
    Route::get('/experience-tab-list', 'AdminController@getExperienceTabList')->name('admin-experience-tab-list');
    Route::post('/add-update-scout-tab-list', 'AdminController@addUpdateScoutTabList')->name('admin-add-update-scout-tab-content');
    Route::post('/get-scout-tab-list', 'AdminController@getScoutTabListById')->name('admin-get-scout-tab-list');
    Route::post('/delete-how-work-list', 'AdminController@deleteScoutTabListById')->name('admin-delete-how-work-list');

    Route::get('/edit-blog-page', 'AdminController@editBlogPage')->name('admin-blog-categories');
    Route::get('/get-blog-category-list', 'AdminController@blogCategoryList')->name('admin-blog-categories-list');
    Route::post('/add-blog-category', 'AdminController@addBlogCategory')->name('admin-add-blog-category');
    Route::post('/delete-blog-category', 'AdminController@deleteBlogCategory')->name('admin-delete-blog-category');
    Route::get('/blog-list', 'AdminController@blogList')->name('admin-blog-list');
    Route::get('/add-blog', 'AdminController@addBlogForm')->name('admin-add-blog');
    Route::get('/edit-blog/{id}', 'AdminController@addBlogForm')->name('admin-edit-blog');
    Route::post('/admin-add-blog', 'AdminController@addUpdateBlog')->name('admin-add-update-blog');
    Route::post('/delete-blog', 'AdminController@deleteBlog')->name('admin-delete-blog');
    
    Route::post('/update-cms-pages', 'AdminController@updateCmsPages')->name('admin-update-cms-pages');

    Route::any('/experience-categories', 'AdminController@experienceCategories')->name('admin-experience-categories');
    Route::post('/get-experience-categories', 'AdminController@getExperienceCategories')->name('get-experience-categories');


    Route::get('/experience-list', 'AdminController@getExperienceList')->name('admin-experience-list');
    Route::get('/view-experience-detail/{id}', 'ScoutController@viewExperienceDetail')->name('admin-view-experience-detail');
    Route::get('/experience-approval-list', 'AdminController@getExperienceApprovalList')->name('admin-experience-approval-list');
    Route::get('/view-experience-approval-detail/{id}', 'AdminController@viewExperienceApprovalDetail')->name('admin-view-experience-approval-detail');
    Route::get('/host-list', 'AdminController@getHostList')->name('admin-host-list');
    Route::post('/host-details', 'AdminController@getHost')->name('admin-host-details');
    Route::get('/manage-google-tags', 'AdminController@ManageGoogleTags')->name('admin-manage-google-tags');

    Route::get('/gift-card-list', 'AdminController@getGiftCardList')->name('admin-gift-card-list');

    Route::post('/change-experience-status', 'AdminController@updateExperienceStatus')->name('admin-change-experience-status'); 
    Route::post('/change-experience-approval-status', 'AdminController@updateExperienceAppStatus')->name('admin-change-experience-app-status'); 
    Route::post('/update-google-tags', 'AdminController@updateGoogleTags')->name('admin-update-google-tags'); 


    Route::get('/manage-setting', 'AdminController@ManageSettings')->name('admin-manage-setting');
    Route::post('/update-setting', 'AdminController@updateManageSettings')->name('admin-update-setting'); 
    Route::get('/bookings', 'AdminController@bookings')->name('admin-bookings');
    Route::get('/booking-list', 'AdminController@bookingList')->name('admin-booking-list'); 
    Route::get('/booked-experience-detail/{id}', 'AdminController@bookedExperienceDetail')->name('admin-booked-experience-detail');

    Route::get('/pdf-create/{id}','AdminController@pdfcreate')->name('admin-pdf-create');
    Route::get('/export-csv/{id}','AdminController@exportCsv')->name('admin-export-csv');
    Route::get('/coordinator-list','AdminController@getCoordinatorList')->name('admin-coordinator-list');
    Route::get('/add-coordinator','AdminController@addCoordinatorsForm')->name('admin-add-coordinators');
    Route::get('/update-coordinator/{id}', 'AdminController@addCoordinatorsForm')->name('admin-update-coordinators');
    Route::post('/add-coordinators','AdminController@createUpdateCoordinators')->name('create-update-coordinator');
    Route::post('/delete-coordinator-by-id', 'AdminController@deleteCoordinatorById')->name('admin-delete-coordinator');
     Route::post('/ckeditor-upload', 'AdminController@upload')->name('ckeditor-upload');

});

// Scout Routes Only
Route::group(['prefix' => 'scout','middleware' => ['auth','App\Http\Middleware\ScoutMiddleware']], function() {

    Route::get('/dashboard/{scout_id?}', 'ScoutController@dashboard')->name('scout-dashboard');
    Route::get('/add-experience', 'ScoutController@addExperienceForm')->name('scout-add-experience');
    Route::get('/view-experience', 'ScoutController@experienceList')->name('scout-experience-list');
    Route::get('/booked-experience', 'ScoutController@bookedExperienceList')->name('scout-booked-experience-list');
    Route::get('/booked-experience-detail/{id}', 'ScoutController@bookedExperienceDetail')->name('scout-booked-experience-detail');
    Route::get('/new-bookings', 'ScoutController@newBookingExperienceList')->name('scout-new-bookings-list');
    Route::post('/create-experience', 'ScoutController@createExperience')->name('scout-create-experience');
    Route::post('/delete-experience', 'ScoutController@deleteExperience')->name('scout-delete-experience');
    Route::get('/edit-experience/{id}', 'ScoutController@editExperience')->name('scout-edit-experience');
    Route::get('/view-experience-detail/{id}', 'ScoutController@viewExperienceDetail')->name('scout-view-experience-detail');
    Route::post('/get-state','ScoutController@getState')->name('experience-get-state');
    Route::post('/get-city','ScoutController@getCity')->name('experience-get-city');
    Route::post('/update-experience', 'ScoutController@updateExperience')->name('scout-update-experience');
    Route::post('/update-experience-new', 'ScoutController@updateExperienceNew')->name('scout-update-experience-new');
    Route::get('/add-host', 'ScoutController@addHostForm')->name('scout-add-host');
    Route::get('/add-host-account-information/{id}', 'ScoutController@addHostAccountForm')->name('scout-add-host-account-information');
    Route::get('/host-account-information/{id}', 'ScoutController@hostAccountDetail')->name('scout-host-account-information');
    Route::post('/create-host', 'ScoutController@createHost')->name('scout-create-host');
    Route::get('/host-list', 'ScoutController@hostList')->name('scout-host-list');
    Route::get('/host-detail/{id}', 'ScoutController@hostDetail')->name('scout-host-detail');
    Route::get('/edit-host/{id}', 'ScoutController@addHostForm')->name('scout-edit-host-detail');
    Route::post('/delete-host','ScoutController@deleteHost')->name('scout-delete-host'); 
    Route::get('/inbox/{scout_id?}/{exp_id?}', 'MessageController@index')->name('scout-inbox'); 
    Route::get('/set-experience-availability', 'AvailabilityController@index')->name('scout-set-experience-availability');
    Route::any('/invoice/{id}', 'ScoutController@invoice')->name('scout-invoice');
    Route::get('/pdf-create/{id}','ScoutController@pdfcreate')->name('scout-pdf-create');
    Route::get('/test-view/{id}','ScoutController@testView')->name('scout-test-view');
    Route::get('/test-view3/{id}','ScoutController@testView3')->name('scout-test-view3');
    Route::get('/test-view2/{id}','ScoutController@testView2')->name('scout-test-view2');
    Route::post('/confirm-booking','ScoutController@changeBookingStatus')->name('scout-confirm-booking'); 
    Route::post('/change-experience-status', 'ScoutController@updateExperienceApprovalStatus')->name('scout-change-experience-status');
     Route::post('/upload-dropzone-image', 'ScoutController@uploadDropzoneImage')->name('upload-dropzone-image'); 
     Route::post('/get-dropzone-images', 'ScoutController@getDropzoneImage')->name('get-dropzone-images'); 
     Route::post('/delete-dropzone-image', 'ScoutController@deleteDropzoneImage')->name('delete-dropzone-image'); 

});

// Traveler Routes Only
Route::group(['prefix' => 'traveler','middleware' => ['verified','auth','App\Http\Middleware\TravelerMiddleware']], function() {
    Route::get('/dashboard/{scout_id?}', 'TravelerController@dashboard')->name('traveler-dashboard');
    Route::get('/wishlist', 'TravelerController@ManageWishlist')->name('traveler-wishlist');
    Route::get('/inbox', 'MessageController@index')->name('traveler-inbox');
    Route::any('/review-submission', 'FrontendController@reviewSubmission')->name('review-submission');
     Route::get('/experience-feedback/{id}', 'TravelerController@experienceFeedback')->name('experience-feedback');  
     Route::post('/add-experience-feedback', 'TravelerController@AddExperienceReview')->name('add-experience-feedback');  
     Route::any('/invoice/{id}', 'TravelerController@invoice')->name('invoice');  

    Route::get('book-experience/{id}','TravelerController@bookExperience')->name('traveler-book-experience');  

    Route::get('booked-experiences-list','TravelerController@ManageBookedExperiences')->name('traveler-booked-experiences');
    Route::post('/book-experience-now','TravelerController@bookedExperienceNow')->name('traveler-book-experience-now');
    Route::get('/booked-experience-detail/{id}', 'TravelerController@bookedExperienceDetail')->name('traveler-booked-experience-detail');
    Route::get('/','TravelerController@pdfcreate')->name('traveler-confirm-and-pay');
    Route::get('/pdf-create/{id}','TravelerController@pdfcreate')->name('traveler-pdf-create');

});
Route::post('/save-message', 'MessageController@save_messages')->name('save-message'); 
Route::post('/user-data-msg', 'MessageController@user_data_msg')->name('user-data-msg'); 
Route::post('/update-message-status', 'MessageController@update_message_status')->name('update-message-status'); 

// Host Routes Only
Route::group(['prefix' => 'host','middleware' => ['auth','App\Http\Middleware\HostMiddleware']], function() {

    Route::get('/dashboard', 'HostController@dashboard')->name('host-dashboard');

    Route::post('/profile-update','HostController@updateHost')->name('host-profile-update');

    Route::get('/experience-list','HostController@experienceList')->name('host-experience-list');
    Route::get('/bookings','HostController@bookedExperienceList')->name('host-booking-list');
    Route::get('/booked-experience-detail/{id}', 'HostController@bookedExperienceDetail')->name('host-booked-experience-detail');

    Route::get('/view-experience/{id}','HostController@viewExperienceDetail')->name('host-view-experience');

    Route::get('/pdf-create/{id}','HostController@pdfcreate')->name('host-pdf-create');
});

Route::get('/create-client', 'SpaceInvoicesApolloController@clientCreate')->name('create-client'); 
Route::get('/create-client', 'SpaceInvoicesApolloController@clientCreate')->name('create-client'); 
Route::get('/get-client/{id}', 'SpaceInvoicesApolloController@getClient')->name('get-client'); 
Route::get('/update-client/{id}', 'SpaceInvoicesApolloController@updateClient')->name('update-client'); 
Route::get('/delete-client/{id}', 'SpaceInvoicesApolloController@deleteClient')->name('delete-client'); 
Route::get('/clients-list', 'SpaceInvoicesApolloController@clientList')->name('delete-client'); 
Route::post('/create-account', 'SpaceInvoicesApolloController@createAccount')->name('create-account');
Route::get('/get-account/{id}', 'SpaceInvoicesApolloController@getAccount')->name('get-account'); 
 
Route::get('/invite-org', 'SpaceInvoicesApolloController@inviteOrg2')->name('invite-org'); 
Route::get('/update-org', 'SpaceInvoicesApolloController@updateOrganization')->name('update-org'); 
Route::get('/create-invoice', 'SpaceInvoicesApolloController@invoicePayments')->name('create-invoice'); 


