<?php

use Illuminate\Database\Seeder;

class HomePageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         DB::table('home_pages')->insert([
         	'id' => '1','status' => '1','user_id' => '1','slug' => 'test','meta' => 'test','created_at' => '2020-11-06 10:16:08','updated_at' => '2021-01-17 19:26:14','banner_content' => '[{"banner_small_heading":"Create","banner_title":"Locals From Zero","banner_description":"pleasant memories AND meaningful connections","banner_button_name":"Start Exploring","banner_button_link":"https:\\/\\/www.24ur.com\\/","banner_image":"banner_1605683687.jpg"},{"banner_small_heading":"Create","banner_title":"Locals From Zero","banner_description":"pleasant memories AND meaningful connections","banner_button_name":"Start Exploring","banner_button_link":"https:\\/\\/www.google.com\\/","banner_image":"banner2_1605683687.jpg"},{"banner_small_heading":"Create","banner_title":"Locals From Zero","banner_description":"pleasant memories AND meaningful connections","banner_button_name":"Start Exploring","banner_button_link":"https:\\/\\/www.google.com\\/","banner_image":"banner3_1605683687.jpg"}]','explore_content' => '{"explore_small_heading":"Come & Explore","explore_title":"Locals From Zero Initiative","explore_video_image":"video-thumb_1605683567.jpg","explore_video":"localsVideo_1604661324.mp4","explore_background_image":"video-bg_1605683567.jpg","explore_description":"<p>Steve Jobs once said: &#39;&#39;The people who are crazy enough to think that they can change the world are the ones who do it&#39;&#39;. -&nbsp;<strong>And we believe we can do it!<\\/strong><\\/p>\\r\\n\\r\\n<p>We are a group of young, visionary people who run an online marketplace for local experiences. We try to highlight our local heroes. We believe that these are the hidden secrets of an authentic and unforgettable experience. And we have one goal -&nbsp;<strong>to bring tourism back to its roots.<\\/strong>&nbsp;Join our marketplace and become part of the change towards a new, more sensitive tourism!<\\/p>"}'
         ]);
    }
}
