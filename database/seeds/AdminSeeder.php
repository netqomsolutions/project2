<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'user_fname'    => 'Master',
            'user_lname'    => 'Admin',
            'email'         => 'lfzadmin@gmail.com',
            'user_role'     => 1,
            'status'        => 1,
            'user_mobile'   => '0000000001',
            'user_address'  => 'Slovenia',
            'password'      => Hash::make('adminLfz@317'),
        ]);
    }
}
