<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;
use App\Language;

class LanguageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $languages=[ ['id' => '1','name' => 'English','short' => 'en','created_at' => Carbon::now()->format('Y-m-d H:i:s'),'updated_at' => Carbon::now()->format('Y-m-d H:i:s')],
  ['id' => '2','name' => 'German','short' => 'de','created_at' => Carbon::now()->format('Y-m-d H:i:s'),'updated_at' => Carbon::now()->format('Y-m-d H:i:s')],
  ['id' => '3','name' => 'French','short' => 'fr','created_at' => Carbon::now()->format('Y-m-d H:i:s'),'updated_at' => Carbon::now()->format('Y-m-d H:i:s')],
  ['id' => '4','name' => 'Dutch','short' => 'nl','created_at' => Carbon::now()->format('Y-m-d H:i:s'),'updated_at' => Carbon::now()->format('Y-m-d H:i:s')],
  ['id' => '5','name' => 'Italian','short' => 'it','created_at' => Carbon::now()->format('Y-m-d H:i:s'),'updated_at' => Carbon::now()->format('Y-m-d H:i:s')],
  ['id' => '6','name' => 'Spanish','short' => 'es','created_at' => Carbon::now()->format('Y-m-d H:i:s'),'updated_at' => Carbon::now()->format('Y-m-d H:i:s')],
  ['id' => '7','name' => 'Polish','short' => 'pl','created_at' => Carbon::now()->format('Y-m-d H:i:s'),'updated_at' => Carbon::now()->format('Y-m-d H:i:s')],
  ['id' => '8','name' => 'Russian','short' => 'ru','created_at' => Carbon::now()->format('Y-m-d H:i:s'),'updated_at' => Carbon::now()->format('Y-m-d H:i:s')],
  ['id' => '9','name' => 'Japanese','short' => 'ja','created_at' => Carbon::now()->format('Y-m-d H:i:s'),'updated_at' => Carbon::now()->format('Y-m-d H:i:s')],
  ['id' => '10','name' => 'Portuguese','short' => 'pt','created_at' => Carbon::now()->format('Y-m-d H:i:s'),'updated_at' => Carbon::now()->format('Y-m-d H:i:s')],
  ['id' => '11','name' => 'Swedish','short' => 'sv','created_at' => Carbon::now()->format('Y-m-d H:i:s'),'updated_at' => Carbon::now()->format('Y-m-d H:i:s')],
  ['id' => '12','name' => 'Chinese','short' => 'zh','created_at' => Carbon::now()->format('Y-m-d H:i:s'),'updated_at' => Carbon::now()->format('Y-m-d H:i:s')],
  ['id' => '13','name' => 'Catalan','short' => 'ca','created_at' => Carbon::now()->format('Y-m-d H:i:s'),'updated_at' => Carbon::now()->format('Y-m-d H:i:s')],
  ['id' => '14','name' => 'Ukrainian','short' => 'uk','created_at' => Carbon::now()->format('Y-m-d H:i:s'),'updated_at' => Carbon::now()->format('Y-m-d H:i:s')],
  ['id' => '15','name' => 'Norwegian (BokmÃ¥l)','short' => 'no','created_at' => Carbon::now()->format('Y-m-d H:i:s'),'updated_at' => Carbon::now()->format('Y-m-d H:i:s')],
  ['id' => '16','name' => 'Finnish','short' => 'fi','created_at' => Carbon::now()->format('Y-m-d H:i:s'),'updated_at' => Carbon::now()->format('Y-m-d H:i:s')],
  ['id' => '17','name' => 'Vietnamese','short' => 'vi','created_at' => Carbon::now()->format('Y-m-d H:i:s'),'updated_at' => Carbon::now()->format('Y-m-d H:i:s')],
  ['id' => '18','name' => 'Czech','short' => 'cs','created_at' => Carbon::now()->format('Y-m-d H:i:s'),'updated_at' => Carbon::now()->format('Y-m-d H:i:s')],
  ['id' => '19','name' => 'Hungarian','short' => 'hu','created_at' => Carbon::now()->format('Y-m-d H:i:s'),'updated_at' => Carbon::now()->format('Y-m-d H:i:s')],
  ['id' => '20','name' => 'Korean','short' => 'ko','created_at' => Carbon::now()->format('Y-m-d H:i:s'),'updated_at' => Carbon::now()->format('Y-m-d H:i:s')],
  ['id' => '21','name' => 'Indonesian','short' => 'id','created_at' => Carbon::now()->format('Y-m-d H:i:s'),'updated_at' => Carbon::now()->format('Y-m-d H:i:s')],
  ['id' => '22','name' => 'Turkish','short' => 'tr','created_at' => Carbon::now()->format('Y-m-d H:i:s'),'updated_at' => Carbon::now()->format('Y-m-d H:i:s')],
  ['id' => '23','name' => 'Romanian','short' => 'ro','created_at' => Carbon::now()->format('Y-m-d H:i:s'),'updated_at' => Carbon::now()->format('Y-m-d H:i:s')],
  ['id' => '24','name' => 'Persian','short' => 'fa','created_at' => Carbon::now()->format('Y-m-d H:i:s'),'updated_at' => Carbon::now()->format('Y-m-d H:i:s')],
  ['id' => '25','name' => 'Arabic','short' => 'ar','created_at' => Carbon::now()->format('Y-m-d H:i:s'),'updated_at' => Carbon::now()->format('Y-m-d H:i:s')],
  ['id' => '26','name' => 'Danish','short' => 'da','created_at' => Carbon::now()->format('Y-m-d H:i:s'),'updated_at' => Carbon::now()->format('Y-m-d H:i:s')],
  ['id' => '27','name' => 'Esperanto','short' => 'eo','created_at' => Carbon::now()->format('Y-m-d H:i:s'),'updated_at' => Carbon::now()->format('Y-m-d H:i:s')],
  ['id' => '28','name' => 'Serbian','short' => 'sr','created_at' => Carbon::now()->format('Y-m-d H:i:s'),'updated_at' => Carbon::now()->format('Y-m-d H:i:s')],
  ['id' => '29','name' => 'Lithuanian','short' => 'lt','created_at' => Carbon::now()->format('Y-m-d H:i:s'),'updated_at' => Carbon::now()->format('Y-m-d H:i:s')],
  ['id' => '30','name' => 'Slovak','short' => 'sk','created_at' => Carbon::now()->format('Y-m-d H:i:s'),'updated_at' => Carbon::now()->format('Y-m-d H:i:s')],
  ['id' => '31','name' => 'Malay','short' => 'ms','created_at' => Carbon::now()->format('Y-m-d H:i:s'),'updated_at' => Carbon::now()->format('Y-m-d H:i:s')],
  ['id' => '32','name' => 'Hebrew','short' => 'he','created_at' => Carbon::now()->format('Y-m-d H:i:s'),'updated_at' => Carbon::now()->format('Y-m-d H:i:s')],
  ['id' => '33','name' => 'Bulgarian','short' => 'bg','created_at' => Carbon::now()->format('Y-m-d H:i:s'),'updated_at' => Carbon::now()->format('Y-m-d H:i:s')],
  ['id' => '34','name' => 'Slovenian','short' => 'sl','created_at' => Carbon::now()->format('Y-m-d H:i:s'),'updated_at' => Carbon::now()->format('Y-m-d H:i:s')],
  ['id' => '35','name' => 'VolapÃ¼k','short' => 'vo','created_at' => Carbon::now()->format('Y-m-d H:i:s'),'updated_at' => Carbon::now()->format('Y-m-d H:i:s')],
  ['id' => '36','name' => 'Kazakh','short' => 'kk','created_at' => Carbon::now()->format('Y-m-d H:i:s'),'updated_at' => Carbon::now()->format('Y-m-d H:i:s')],
  ['id' => '37','name' => 'Waray-Waray','short' => 'war','created_at' => Carbon::now()->format('Y-m-d H:i:s'),'updated_at' => Carbon::now()->format('Y-m-d H:i:s')],
  ['id' => '38','name' => 'Basque','short' => 'eu','created_at' => Carbon::now()->format('Y-m-d H:i:s'),'updated_at' => Carbon::now()->format('Y-m-d H:i:s')],
  ['id' => '39','name' => 'Croatian','short' => 'hr','created_at' => Carbon::now()->format('Y-m-d H:i:s'),'updated_at' => Carbon::now()->format('Y-m-d H:i:s')],
  ['id' => '40','name' => 'Hindi','short' => 'hi','created_at' => Carbon::now()->format('Y-m-d H:i:s'),'updated_at' => Carbon::now()->format('Y-m-d H:i:s')],
  ['id' => '41','name' => 'Estonian','short' => 'et','created_at' => Carbon::now()->format('Y-m-d H:i:s'),'updated_at' => Carbon::now()->format('Y-m-d H:i:s')],
  ['id' => '42','name' => 'Azerbaijani','short' => 'az','created_at' => Carbon::now()->format('Y-m-d H:i:s'),'updated_at' => Carbon::now()->format('Y-m-d H:i:s')],
  ['id' => '43','name' => 'Galician','short' => 'gl','created_at' => Carbon::now()->format('Y-m-d H:i:s'),'updated_at' => Carbon::now()->format('Y-m-d H:i:s')],
  ['id' => '44','name' => 'Simple English','short' => 'simple','created_at' => Carbon::now()->format('Y-m-d H:i:s'),'updated_at' => Carbon::now()->format('Y-m-d H:i:s')],
  ['id' => '45','name' => 'Norwegian (Nynorsk)','short' => 'nn','created_at' => Carbon::now()->format('Y-m-d H:i:s'),'updated_at' => Carbon::now()->format('Y-m-d H:i:s')],
  ['id' => '46','name' => 'Thai','short' => 'th','created_at' => Carbon::now()->format('Y-m-d H:i:s'),'updated_at' => Carbon::now()->format('Y-m-d H:i:s')],
  ['id' => '47','name' => 'Newar / Nepal Bhasa','short' => 'new','created_at' => Carbon::now()->format('Y-m-d H:i:s'),'updated_at' => Carbon::now()->format('Y-m-d H:i:s')],
  ['id' => '48','name' => 'Greek','short' => 'el','created_at' => Carbon::now()->format('Y-m-d H:i:s'),'updated_at' => Carbon::now()->format('Y-m-d H:i:s')],
  ['id' => '49','name' => 'Aromanian','short' => 'roa-rup','created_at' => Carbon::now()->format('Y-m-d H:i:s'),'updated_at' => Carbon::now()->format('Y-m-d H:i:s')],
  ['id' => '50','name' => 'Latin','short' => 'la','created_at' => Carbon::now()->format('Y-m-d H:i:s'),'updated_at' => Carbon::now()->format('Y-m-d H:i:s')],
  ['id' => '51','name' => 'Occitan','short' => 'oc','created_at' => Carbon::now()->format('Y-m-d H:i:s'),'updated_at' => Carbon::now()->format('Y-m-d H:i:s')],
  ['id' => '52','name' => 'Tagalog','short' => 'tl','created_at' => Carbon::now()->format('Y-m-d H:i:s'),'updated_at' => Carbon::now()->format('Y-m-d H:i:s')],
  ['id' => '53','name' => 'Haitian','short' => 'ht','created_at' => Carbon::now()->format('Y-m-d H:i:s'),'updated_at' => Carbon::now()->format('Y-m-d H:i:s')],
  ['id' => '54','name' => 'Macedonian','short' => 'mk','created_at' => Carbon::now()->format('Y-m-d H:i:s'),'updated_at' => Carbon::now()->format('Y-m-d H:i:s')],
  ['id' => '55','name' => 'Georgian','short' => 'ka','created_at' => Carbon::now()->format('Y-m-d H:i:s'),'updated_at' => Carbon::now()->format('Y-m-d H:i:s')],
  ['id' => '56','name' => 'Serbo-Croatian','short' => 'sh','created_at' => Carbon::now()->format('Y-m-d H:i:s'),'updated_at' => Carbon::now()->format('Y-m-d H:i:s')],
  ['id' => '57','name' => 'Telugu','short' => 'te','created_at' => Carbon::now()->format('Y-m-d H:i:s'),'updated_at' => Carbon::now()->format('Y-m-d H:i:s')],
  ['id' => '58','name' => 'Piedmontese','short' => 'pms','created_at' => Carbon::now()->format('Y-m-d H:i:s'),'updated_at' => Carbon::now()->format('Y-m-d H:i:s')],
  ['id' => '59','name' => 'Cebuano','short' => 'ceb','created_at' => Carbon::now()->format('Y-m-d H:i:s'),'updated_at' => Carbon::now()->format('Y-m-d H:i:s')],
  ['id' => '60','name' => 'Tamil','short' => 'ta','created_at' => Carbon::now()->format('Y-m-d H:i:s'),'updated_at' => Carbon::now()->format('Y-m-d H:i:s')],
  ['id' => '61','name' => 'Belarusian (TaraÅ¡kievica)','short' => 'be-x-old','created_at' => Carbon::now()->format('Y-m-d H:i:s'),'updated_at' => Carbon::now()->format('Y-m-d H:i:s')],
  ['id' => '62','name' => 'Breton','short' => 'br','created_at' => Carbon::now()->format('Y-m-d H:i:s'),'updated_at' => Carbon::now()->format('Y-m-d H:i:s')],
  ['id' => '63','name' => 'Latvian','short' => 'lv','created_at' => Carbon::now()->format('Y-m-d H:i:s'),'updated_at' => Carbon::now()->format('Y-m-d H:i:s')],
  ['id' => '64','name' => 'Javanese','short' => 'jv','created_at' => Carbon::now()->format('Y-m-d H:i:s'),'updated_at' => Carbon::now()->format('Y-m-d H:i:s')],
  ['id' => '65','name' => 'Albanian','short' => 'sq','created_at' => Carbon::now()->format('Y-m-d H:i:s'),'updated_at' => Carbon::now()->format('Y-m-d H:i:s')],
  ['id' => '66','name' => 'Belarusian','short' => 'be','created_at' => Carbon::now()->format('Y-m-d H:i:s'),'updated_at' => Carbon::now()->format('Y-m-d H:i:s')],
  ['id' => '67','name' => 'Marathi','short' => 'mr','created_at' => Carbon::now()->format('Y-m-d H:i:s'),'updated_at' => Carbon::now()->format('Y-m-d H:i:s')],
  ['id' => '68','name' => 'Welsh','short' => 'cy','created_at' => Carbon::now()->format('Y-m-d H:i:s'),'updated_at' => Carbon::now()->format('Y-m-d H:i:s')],
  ['id' => '69','name' => 'Luxembourgish','short' => 'lb','created_at' => Carbon::now()->format('Y-m-d H:i:s'),'updated_at' => Carbon::now()->format('Y-m-d H:i:s')],
  ['id' => '70','name' => 'Icelandic','short' => 'is','created_at' => Carbon::now()->format('Y-m-d H:i:s'),'updated_at' => Carbon::now()->format('Y-m-d H:i:s')],
  ['id' => '71','name' => 'Bosnian','short' => 'bs','created_at' => Carbon::now()->format('Y-m-d H:i:s'),'updated_at' => Carbon::now()->format('Y-m-d H:i:s')],
  ['id' => '72','name' => 'Yoruba','short' => 'yo','created_at' => Carbon::now()->format('Y-m-d H:i:s'),'updated_at' => Carbon::now()->format('Y-m-d H:i:s')],
  ['id' => '73','name' => 'Malagasy','short' => 'mg','created_at' => Carbon::now()->format('Y-m-d H:i:s'),'updated_at' => Carbon::now()->format('Y-m-d H:i:s')],
  ['id' => '74','name' => 'Aragonese','short' => 'an','created_at' => Carbon::now()->format('Y-m-d H:i:s'),'updated_at' => Carbon::now()->format('Y-m-d H:i:s')],
  ['id' => '75','name' => 'Bishnupriya Manipuri','short' => 'bpy','created_at' => Carbon::now()->format('Y-m-d H:i:s'),'updated_at' => Carbon::now()->format('Y-m-d H:i:s')],
  ['id' => '76','name' => 'Lombard','short' => 'lmo','created_at' => Carbon::now()->format('Y-m-d H:i:s'),'updated_at' => Carbon::now()->format('Y-m-d H:i:s')],
  ['id' => '77','name' => 'West Frisian','short' => 'fy','created_at' => Carbon::now()->format('Y-m-d H:i:s'),'updated_at' => Carbon::now()->format('Y-m-d H:i:s')],
  ['id' => '78','name' => 'Bengali','short' => 'bn','created_at' => Carbon::now()->format('Y-m-d H:i:s'),'updated_at' => Carbon::now()->format('Y-m-d H:i:s')],
  ['id' => '79','name' => 'Ido','short' => 'io','created_at' => Carbon::now()->format('Y-m-d H:i:s'),'updated_at' => Carbon::now()->format('Y-m-d H:i:s')],
  ['id' => '80','name' => 'Swahili','short' => 'sw','created_at' => Carbon::now()->format('Y-m-d H:i:s'),'updated_at' => Carbon::now()->format('Y-m-d H:i:s')],
  ['id' => '81','name' => 'Gujarati','short' => 'gu','created_at' => Carbon::now()->format('Y-m-d H:i:s'),'updated_at' => Carbon::now()->format('Y-m-d H:i:s')],
  ['id' => '82','name' => 'Malayalam','short' => 'ml','created_at' => Carbon::now()->format('Y-m-d H:i:s'),'updated_at' => Carbon::now()->format('Y-m-d H:i:s')],
  ['id' => '83','name' => 'Western Panjabi','short' => 'pnb','created_at' => Carbon::now()->format('Y-m-d H:i:s'),'updated_at' => Carbon::now()->format('Y-m-d H:i:s')],
  ['id' => '84','name' => 'Afrikaans','short' => 'af','created_at' => Carbon::now()->format('Y-m-d H:i:s'),'updated_at' => Carbon::now()->format('Y-m-d H:i:s')],
  ['id' => '85','name' => 'Low Saxon','short' => 'nds','created_at' => Carbon::now()->format('Y-m-d H:i:s'),'updated_at' => Carbon::now()->format('Y-m-d H:i:s')],
  ['id' => '86','name' => 'Sicilian','short' => 'scn','created_at' => Carbon::now()->format('Y-m-d H:i:s'),'updated_at' => Carbon::now()->format('Y-m-d H:i:s')],
  ['id' => '87','name' => 'Urdu','short' => 'ur','created_at' => Carbon::now()->format('Y-m-d H:i:s'),'updated_at' => Carbon::now()->format('Y-m-d H:i:s')],
  ['id' => '88','name' => 'Kurdish','short' => 'ku','created_at' => Carbon::now()->format('Y-m-d H:i:s'),'updated_at' => Carbon::now()->format('Y-m-d H:i:s')],
  ['id' => '89','name' => 'Cantonese','short' => 'zh-yue','created_at' => Carbon::now()->format('Y-m-d H:i:s'),'updated_at' => Carbon::now()->format('Y-m-d H:i:s')],
  ['id' => '90','name' => 'Armenian','short' => 'hy','created_at' => Carbon::now()->format('Y-m-d H:i:s'),'updated_at' => Carbon::now()->format('Y-m-d H:i:s')],
  ['id' => '91','name' => 'Quechua','short' => 'qu','created_at' => Carbon::now()->format('Y-m-d H:i:s'),'updated_at' => Carbon::now()->format('Y-m-d H:i:s')],
  ['id' => '92','name' => 'Sundanese','short' => 'su','created_at' => Carbon::now()->format('Y-m-d H:i:s'),'updated_at' => Carbon::now()->format('Y-m-d H:i:s')],
  ['id' => '93','name' => 'Nepali','short' => 'ne','created_at' => Carbon::now()->format('Y-m-d H:i:s'),'updated_at' => Carbon::now()->format('Y-m-d H:i:s')],
  ['id' => '94','name' => 'Zazaki','short' => 'diq','created_at' => Carbon::now()->format('Y-m-d H:i:s'),'updated_at' => Carbon::now()->format('Y-m-d H:i:s')],
  ['id' => '95','name' => 'Asturian','short' => 'ast','created_at' => Carbon::now()->format('Y-m-d H:i:s'),'updated_at' => Carbon::now()->format('Y-m-d H:i:s')],
  ['id' => '96','name' => 'Tatar','short' => 'tt','created_at' => Carbon::now()->format('Y-m-d H:i:s'),'updated_at' => Carbon::now()->format('Y-m-d H:i:s')],
  ['id' => '97','name' => 'Neapolitan','short' => 'nap','created_at' => Carbon::now()->format('Y-m-d H:i:s'),'updated_at' => Carbon::now()->format('Y-m-d H:i:s')],
  ['id' => '98','name' => 'Irish','short' => 'ga','created_at' => Carbon::now()->format('Y-m-d H:i:s'),'updated_at' => Carbon::now()->format('Y-m-d H:i:s')],
  ['id' => '99','name' => 'Chuvash','short' => 'cv','created_at' => Carbon::now()->format('Y-m-d H:i:s'),'updated_at' => Carbon::now()->format('Y-m-d H:i:s')],
  ['id' => '100','name' => 'Samogitian','short' => 'bat-smg','created_at' => Carbon::now()->format('Y-m-d H:i:s'),'updated_at' => Carbon::now()->format('Y-m-d H:i:s')],
  ['id' => '101','name' => 'Walloon','short' => 'wa','created_at' => Carbon::now()->format('Y-m-d H:i:s'),'updated_at' => Carbon::now()->format('Y-m-d H:i:s')],
  ['id' => '102','name' => 'Amharic','short' => 'am','created_at' => Carbon::now()->format('Y-m-d H:i:s'),'updated_at' => Carbon::now()->format('Y-m-d H:i:s')],
  ['id' => '103','name' => 'Kannada','short' => 'kn','created_at' => Carbon::now()->format('Y-m-d H:i:s'),'updated_at' => Carbon::now()->format('Y-m-d H:i:s')],
  ['id' => '104','name' => 'Alemannic','short' => 'als','created_at' => Carbon::now()->format('Y-m-d H:i:s'),'updated_at' => Carbon::now()->format('Y-m-d H:i:s')],
  ['id' => '105','name' => 'Buginese','short' => 'bug','created_at' => Carbon::now()->format('Y-m-d H:i:s'),'updated_at' => Carbon::now()->format('Y-m-d H:i:s')],
  ['id' => '106','name' => 'Burmese','short' => 'my','created_at' => Carbon::now()->format('Y-m-d H:i:s'),'updated_at' => Carbon::now()->format('Y-m-d H:i:s')],
  ['id' => '107','name' => 'Interlingua','short' => 'ia','created_at' => Carbon::now()->format('Y-m-d H:i:s'),'updated_at' => Carbon::now()->format('Y-m-d H:i:s')]];
    foreach ($languages as $lang) {
      $language = new language;
      $language->id = $lang['id'];
      $language->name = $lang['name'];
      $language->short = $lang['short'];
      $language->created_at = $lang['created_at'];
      $language->updated_at = $lang['updated_at'];
      $language->save();
      }
    }
}
