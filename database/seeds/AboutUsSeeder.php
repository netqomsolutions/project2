<?php

use Illuminate\Database\Seeder;

class AboutUsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       DB::table('about_us')->insert([
           'id' => '1','status' => '1','user_id' => '1','page_slug' => 'page slug here','page_title' => 'About Us','page_meta' => 'page meta here','created_at' => '2020-10-26 10:43:36','updated_at' => '2020-12-01 15:52:55','page_content' => '<p>#LocalsFromZero mission is to connect you with unique local artisans, experts, associations, clubs, charities and more (*).&nbsp;<br />
You can already book your #LocalsFromZero experiences in Slovenia. And stay tuned for other countries to follow..</p>

<p>All these (*) often invisible or somehow overlooked local actors contribute greatly to the preservation of their traditions, culture and environment. #LocalsFromZero takes care of their prudent involvement in the global tourism market.</p>

<p>The frequent reasons for the invisibility of such stakeholders are lack of resources in advertising, digitization, bureaucracy, etc. That is why our #LFZ Scouts are here to find them and help them show their inspiring skills and experiences to the world.</p>

<p>Be curious and prudent yourself - and visit them for additional authentic and memorable experiences of the regions you travel through!</p>','how_it_started' => '<p>At #TourismFromZero, we have already collected more than 200 ideas on how to move tourism from zero to hero. In April 2020, several students submitted ideas with a common focus on local &quot;from zero&quot; experiences.</p>

<p>We invited these students to join the TFZ team. Together, we have started to develop the #LocalsFromZero concept and implemented the first minimum viable prototype for Slovenia through which you can already book your local experiences.</p>','our_story' => '<p>We are a group of young, committed people with one goal: to bring tourism back to its roots. Encouraged by #TourismFromZero global initiative, we strive to highlight our local heroes. Now is the time for you to discover hidden experiences that were not part of the global tourism market. With your booking of #LocalsFromZero experiences you are leading the change towards a more sensitive tourism.</p>','page_description' => 'Marketplace For Local Experiences','page_featured_image' => 'about-banner.jpg','about_first_image' => 'how-it-started-img.jpg','about_second_image' => 'our-story-img.jpg'
        ]);
    }
}
