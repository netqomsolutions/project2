<?php

use Illuminate\Database\Seeder;
use App\ContentManagementSystem;
class ContentManagementSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */

   
    public function run()
    {
    	 $lfz_content_management_systems = array(
  array('id' => '1','status' => '1','user_id' => '1','page_slug' => 'page slug here','page_title' => 'Privacy Policy','page_meta' => 'page meta is here','page_content' => '<h3>Registrar</h3>

<p>#LocalsFromZero service: Sharetribe Ltd</p>

<p><a href="http://112.196.87.242/locals-from-zero/www.sharetribe.com">www.sharetribe.com</a></p>

<p>info (at) sharetribe.com</p>

<h3>Name of the register</h3>

<p>User register of the #LocalsmFromZero service</p>

<h3>Use of personal details (purpose of register)</h3>

<p>Personal details are collected to make communication and use of service possible. Details can be used for communication between service providers and users and also for direct communication between users. Some personal details are visible on the profile page of the user, but those details are voluntary (except name).</p>

<p>The handling of personal details is not outsourced, but the register data is stored on a server that is rented from a third party company.</p>

<h3>Information content of the register</h3>

<p>The following information may be stored in the register:</p>

<ul>
	<li>Personal details: Name, email address, phone number, street address</li>
	<li>Account details: username, password (stored in encrypted format)</li>
	<li>The description text that the user may write about him/herself</li>
	<li>The offers and requests the user has posted to the service</li>
	<li>The given and received feedback and badges</li>
	<li>Statistical data about service usage, e.g. number times the user has logged in</li>
</ul>

<h3>Regular sources of information</h3>

<p>Personal details are given by the user on registration to the service or when using it later.</p>

<h3>Regular handovers of the information</h3>

<p>The information may be handed over for research purposes as described in the&nbsp;<a href="http://112.196.87.242/locals-from-zero/privacy-policy.html#">Terms of use</a>&nbsp;that the user accepts before starting to use the service. The researchers may not publish any research results so that identifying information would be revealed or that any specific user could be detected.</p>

<p>Information considering users of a single #TourismFromZero with locals community may be handed over to the client who has founded that community or to the community administrators appointed by that client.</p>

<h3>Transfers of the information outside the EU and the European Economic Area</h3>

<p>Information may be stored to a server that may be located inside or outside of the EU and the European Economic Area</p>

<h3>Register protection principles</h3>

<p>The information is stored on computers. The access to the information is restricted with passwords and physical access to the computers is restricted by the server hosting company.</p>','page_description' => 'Marketplace For Local Experiences','page_featured_image' => 'about-banner-1_1606284991.jpg','ip' => NULL,'created_at' => '2020-11-25 11:52:00','updated_at' => '2020-11-27 05:49:55'),
  array('id' => '2','status' => '1','user_id' => '1','page_slug' => 'page slug here','page_title' => 'Terms of use','page_meta' => 'page meta here','page_content' => '<h3>TourismFromZero with locals terms of use</h3>

<p>Last update: June 6, 2020.</p>

<p><strong>#LocalsFromZero</strong>&nbsp;Organizers&#39; and #LFZ Scouts&#39; role in the current development phase of the project is to provide and exchange free online information about the types of local experience offers available in different destinations - for all these offers we do not charge any fees.</p>

<p>Experience Providers (= local artisans, experts, associations, clubs, charities and more) set prices and accept all payments. All payments are handled by the Experience Providers.</p>

<p>Despite careful communication, all information and prices listed on the #LocalsFromZero website are informal. For the final confirmation of the content, prices and the available time slots, the tourist must communicate with our #LFZ Scouts.</p>

<p><strong>#LFZs</strong>&nbsp;Scouts are responsible for the greatest possible care and promptness of the information provided. However, they cannot be held liable in case of inaccurate or subsequently changed data provided by Experience Providers.</p>

<p><strong>Experience Providers</strong>&nbsp;are responsible for the lawful execution and accounting of their activities. The ultimate responsibility for the content, price and timing of the experience rests with the Experience Provider, who also assumes full responsibility for any disputes, accidents and unfortunate occurrences at the Experience Provider&#39;s location.</p>

<p>Experience Providers are responsible for complying with all necessary health and safety regulations while accepting visitors to and hosting them on their premises. Experience Providers are deemed liable for all costs incurred in the event of cancellation caused on their behalf.</p>

<p><strong>The Organizers</strong>&nbsp;of the project &quot;#LocalsFromZero&quot; have the right to change the functionality of the website and the website user experience, which may lead to a shift in content.</p>

<p>The Organizers of the project &quot;#LocalsFromZero&quot; have the right to change these rules without prior notice. Any use of this website after the modification of these rules automatically implies the acceptance of the modifications made.</p>

<p>More legal information about #LocalsFromZero and #TourismFromZero can be found under the link Disclaimer at&nbsp;<a href="http://tourismfromzero.org/">http://tourismfromzero.org/</a>.</p>

<h3>More legal information about Shartribe platform:</h3>

<p>#LocalsFromZero is a web service built on the Sharetribe platform. The general terms of Sharetribe below apply also to the use of #LocalsFromZero.</p>

<p>Sharetribe is a social media service that allows its users to change favors and items and communicate with each other. The Sharetribe-service may only be used in accordance with these terms of use. The service provider reserves the right to change these terms of use if required. Valid terms of use can be found from Sharetribe&rsquo;s website.</p>

<h3>Rights of Content</h3>

<p>The users themselves retain the right to all text, pictures and other content that they create in the service. The users allow others to utilize the content in accordance with the nature of the service and furthermore allow the service provider to file information and data and make changes that are necessary for the service or the study, however other rights are not transferred from the users, unless specifically otherwise agreed. The responsibility of the content lies with the user, who has produced it to the service. The service provider has the right to remove any material when it deems it necessary.</p>

<h3>Disclaimer</h3>

<p>No guarantees of the functioning of the Sharetribe service are given. The users are themselves responsible for their actions in the service and they should estimate the reliability of other users before dealing with them. The service provider can under no circumstances be liable for damage that is caused to the user. The user may not store any information or data in the service, and expect it to remain there.</p>

<h3>The Removal of a User</h3>

<p>The service provider has the right to remove any users from Sharetribe and terminate their right of use of the service without any specific reason and without being liable for compensation.</p>

<h3>Applicable Jurisdiction</h3>

<p>The jurisdiction that is applicable in this service and these terms of use is that of Finland, unless something else is required by binding law.</p>','page_description' => 'Marketplace For Local Experiences','page_featured_image' => 'about-banner (1)_1605699702.jpg','ip' => NULL,'created_at' => '2020-11-25 11:52:03','updated_at' => '2020-11-27 05:50:04'),
  array('id' => '3','status' => '3','user_id' => '1','page_slug' => 'page slug here','page_title' => 'Our Blog','page_meta' => 'page meta here','page_content' => '','page_description' => 'Marketplace For Local Experiences','page_featured_image' => 'blog-banner_1605759734.jpg','ip' => NULL,'created_at' => '2020-11-25 11:52:09','updated_at' => '2020-11-19 04:22:44'),
  array('id' => '4','status' => '1','user_id' => '1','page_slug' => 'Page slug is he','page_title' => 'Contact Us','page_meta' => 'page meta is here','page_content' => '{"email":"locals@zero.com","address":"Zavod Ideas From Zero Cankarjeva cesta 1 1270 Litija","phone":"18001234561"}','page_description' => 'Page description is here','page_featured_image' => 'about-banner-1_1606285293.jpg','ip' => NULL,'created_at' => '2020-11-25 10:18:28','updated_at' => '2020-12-11 05:13:28'),
  array('id' => '5','status' => '1','user_id' => '1','page_slug' => 'page slug here','page_title' => 'How it works','page_meta' => 'page meta here','page_content' => '{"tab_one_image":"how-it-work-main-img_1606456168.jpg","tab_one_content":"<p>Tourism has numerous negative impacts on the natural and social environment. Many of the negative impacts arise because tourism flows are unevenly distributed among only a few hotspots.<\\/p>\\r\\n\\r\\n<p>We are here to influence how the world travels.<\\/p>\\r\\n\\r\\n<p>Experience the hospitality of real locals. Set foot in places you have never heard of before. Try new things and get involved in local communities.<\\/p>","tab_two_content":"<p>LocalsFromZero is a marketplace for local experiences. But it works a bit differently than all the other platforms out there.<\\/p>\\r\\n\\r\\n<p>We are introducing a new layer of so-called LFZ Scouts. Scouts are our field agents who know their regions inside and out. Their task is to find enthusiastic and hidden local experts and hosts - and help them to become part of the tourism market. These small but important players are often overlooked, although they make a great contribution to the preservation of local (past and present) traditions, culture and environment. Our Scouts make sure that their stories are told and seen. So what are you waiting for?<\\/p>\\r\\n\\r\\n<ul>\\r\\n\\t<li>Find an experience that suits you best<\\/li>\\r\\n\\t<li>Connect with a Scout and book the experience<\\/li>\\r\\n\\t<li>Enjoy the experience and join our community<\\/li>\\r\\n<\\/ul>","tab_three_content":"<p>The LocalsFromZero is so much more than just a marketplace. We are building a community. A community that wants better, more sensitive and sustainable tourism.<\\/p>\\r\\n\\r\\n<p>We visit our experience providers with our scouts and test the experiences. We exchange ideas. We listen to their concerns, train and take action!<\\/p>\\r\\n\\r\\n<p>We organize workshops and online courses tailored to their needs. We send them a newsletter with new insights into tourism, hospitality and the service industry.<\\/p>\\r\\n\\r\\n<p>We make a podcast in which we talk to successful tourism personalities who share their knowledge.<\\/p>\\r\\n\\r\\n<p>And we educate travelers through our social media.<br \\/>\\r\\nMake sure you follow us and help us to bring tourism back to a better reality. From zero to hero!<\\/p>"}','page_description' => 'Marketplace For Local Experiences','page_featured_image' => 'about-banner (1)_1606292889.jpg','ip' => NULL,'created_at' => '2020-11-25 13:03:49','updated_at' => '2020-11-27 05:49:34'),
  array('id' => '6','status' => '1','user_id' => '1','page_slug' => '','page_title' => 'connect & discover','page_meta' => '','page_content' => '{"section_one_title":"Find Interesting Experience","section_two_title":"Connect to Scouts","section_three_title":"Enjoy Your Experience","section_one_description":"Discover less known places and be one step closer to the local people. Book an experience and contribute to more sensitive, inclusive, and impactful tourism.","section_two_description":"Scouts are local agents who know their region inside and out. They take care of your booking and answer all your questions.","section_three_description":"Finally, you can indulge in your unique experience and build a deeper connection with the place and the local people."}','page_description' => '','page_featured_image' => '','ip' => NULL,'created_at' => '2020-11-27 10:47:31','updated_at' => '2021-01-17 13:55:43'),
  array('id' => '7','status' => '1','user_id' => '1','page_slug' => '','page_title' => 'Site Social Links','page_meta' => '','page_content' => {"facebook":"https:\\/\\/www.facebook.com\\/localsfromzero","twitter":"https:\\/\\/twitter.com\\/TourismFromZero","instagram":"https:\\/\\/www.instagram.com\\/localsfromzero","linkedin":"https:\\/\\/www.linkedin.com\\/company\\/localsfromzero"}','page_description' => '','page_featured_image' => '','ip' => NULL,'created_at' => '2020-12-01 13:41:55','updated_at' => '2020-12-01 09:30:36')
);
        foreach ($lfz_content_management_systems as $content) {
      $language = new ContentManagementSystem;
      $language->id = $content['id'];
      $language->status = $content['status'];
      $language->user_id = $content['user_id'];
      $language->page_slug = $content['page_slug'];
      $language->page_title = $content['page_title'];
      $language->page_meta = $content['page_meta'];
      $language->page_description = $content['page_description'];
      $language->page_featured_image = $content['page_featured_image'];
      $language->created_at = $content['created_at'];
      $language->updated_at = $content['updated_at'];
      $language->save();
      }
    }
}
