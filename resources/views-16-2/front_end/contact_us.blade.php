@extends('layouts.app')
@section('content')
<style type="text/css">
	.contact_submit{
	    margin-top: 8px;
	    background-color: #afc578;
	    color: #fff;
	    text-transform: uppercase;
	    font-size: 19px;
	    max-width: 30%;
	}
	.suc{
		color: green;
	}
	.fail{
		color: red;
	}    
</style>
<?php $url = asset('pages/privacy/'.$contactUs->page_featured_image); ?>
<section class="inner-banner about-banner contact-banner" style="background-image: url('<?php echo $url; ?>')">
	<div class="container">
	<div class="row">
		<div class="col-lg-12 col-md-12 col-12">
		<h3>Contact <span class="green-color">Us</span></h3>
		<p>{{ $contactUs->page_description }}</p>
		</div>
	</div>
	</div>
	</section>
	
	<section class="contact-sec-info">
		<div class="container">
			<div class="row">
				<div class="company-info d-flex justify-content-between">
					<div class="common-info-box company-address">
						<span><i class="fas fa-map-marker-alt" aria-hidden="true"></i></span>
						<p>{{ $contactUs->address }}</p>
					</div>
					<div class="common-info-box phone-no">
						<span><i class="fas fa-phone-alt" aria-hidden="true"></i></span>
						<p><b>Phone:</b> <a href="tel:18001234567">{{ $contactUs->phone }}</a></p>
					</div>
					<div class="common-info-box email-address">
						<span><i class="fas fa-envelope" aria-hidden="true"></i></span>
						<p><b>Email:</b> <a href="mailto:locals@zero.com">{{ $contactUs->email }}</a></p>
					</div>
				</div>
				<div class="contact-form d-flex justify-content-between">
					<div class="main-heading">
						<h5 class="curly-font green-color">Contact  Us</h5>
						<h3>Have Any Question? <br>Feel Free To Contact <br>With Us.</h3>
						<ul class="contact-social">
						<li><a href="{{(isset($socialLinks) && $socialLinks->facebook!='')? $socialLinks->facebook : '#'}}"><i class="fab fa-facebook-f"></i></a></li>
		                <li><a href="{{(isset($socialLinks) && $socialLinks->instagram!='')? $socialLinks->instagram : '#'}}"><i class="fab fa-instagram"></i></a></li>
		                <li><a href="{{(isset($socialLinks) && $socialLinks->twitter!='')? $socialLinks->twitter : '#'}}"><i class="fab fa-twitter"></i></a></li>
		                <li><a href="{{(isset($socialLinks) && $socialLinks->linkedin!='')? $socialLinks->linkedin : '#'}}"><i class="fab fa-linkedin-in"></i></a></li>
						</ul>
					</div>
					<form method="post" id="contact-us-form" action="">
						<div class="my-error">
							<input type="text" placeholder="Your Name" name="name" id="name" />
						</div>
						<div class="my-error">
							<input type="email" placeholder="Your Email" name="email" id="email" />
						</div>
						<div class="my-error">
							<input type="text" placeholder="Phone Number" name="phone" id="phone" />
						</div>
						<div class="my-error">
							<input type="text" placeholder="Subject" name="subject" id="subject" />
						</div>
						<div class="my-error textarea-form">
							<textarea placeholder="Write Message" name="message" id="message"></textarea>
						</div>
						<input type="submit" class="submit-btn" value="Send Message" />
						<span id="msg" style="display: block;"></span>
					</form>
				</div>
			</div>
		</div>
	</section>
	@endsection
@section('javascript')
<script type="text/javascript">

	$(document).ready(function () {
		$('#contact-us-form').validate({
			submitHandler: function(form) {
			  ajaxSubmit();
			},
		    rules: {
			    name: {
			    	required: true,
			    },
			    email: {
			    	required: true,
			    },
			    subject:{
		          	required: true,
		        },
		        message:{
	                required: true,
	            },
	            phone:{
                    required: true,
                    digits: true,
                    maxlength: 10,
                    minlength: 10,
                },
		    },
		    messages: {
		    	name: {
		    		required: "Please enter your name",
		      	},	
	        	email: {	        		
	          		required: "Please enter your email",           
	        	},
	        	subject: {
	          		required : "Please enter your subject",
	       		},
		        message:{
	                required:"Please enter message here",
	            },
	            phone:{
	            	required: "Please enter your phone"
	            }
		    },
		    errorElement: 'span',		    
		    errorPlacement: function (error, element) {
		      	error.addClass('invalid-feedback');
		      	element.closest('.my-error').append(error);
		    },
		    highlight: function (element, errorClass, validClass) {
		      	$(element).addClass('is-invalid');
		    },
		    unhighlight: function (element, errorClass, validClass) {
		      	$(element).removeClass('is-invalid');
		    }
		});
	});


	function ajaxSubmit(){
		$('.loader').css('display','flex');
		var name = $('#name').val();
		var email = $('#email').val();
		var message = $('#message').val();
		var subject = $('#subject').val();
		var phone = $('#phone').val();
		$.ajax({
	        url: '{{ route("contact-us-post") }}',
	        type: 'POST',
	        data: { name: name ,email: email ,message: message ,phone: phone , subject: subject , "_token": "{{ csrf_token() }}", },
	        success: function(response)
	        {
	        	$('.loader').css('display','none');
	        	$('#contact-us-form')[0].reset();
	        	$('#msg').html(response.message);
	        	if(response.isSucceeded){
	        		$('#msg').removeClass('suc');
	        		$('#msg').removeClass('fail');
	        		$('#msg').addClass('suc');
	        	}else{
	        		$('#msg').removeClass('suc');
	        		$('#msg').removeClass('fail');
	        		$('#msg').addClass('fail');
	        	}	    
	        	$('#msg').fadeIn();
	        	setTimeout(function(){
	        		$('#msg').fadeOut();
	        	},5000)
	        }
	    });
	}
</script>

@endsection