@extends('layouts.app')
@section('content')
<section class="thankyou-main">
	<div class="container">
		<div class="row">
			<div class="col-md-12 mb-5">
				<h2 class="text-center text-uppercase">Thanks for your interest! <span>:)</span></h2>
				<p class="text-center">Your booking request has been transferred to scout, please wait for 24 hours to get the confirmation from our side. We wish you all the fun for your experience.</p>
			</div>
			<div class="d-flex flex-wrap w-100">
				<div class="col-md-12">
					<h3 class="mb-3">Order Summary</h3>
					<div class="gray-bg">
						<ul class="m-0 p-0 list-unstyled d-flex flex-wrap">
							<li><span><i class="fas fa-box-open"></i> Order number</span><b>{{$booking->order_number}}</b></li>
							
							<li><span><i class="fas fa-male"></i> Adult Count</span><b>{{$booking->adults}}</b></li>
							<li><span><i class="fas fa-clock"></i> Date</span><b>{{date('M d, Y', strtotime($booking->created_at))}}</b></li>
							<li><span><i class="fas fa-percentage"></i> Discount Price</span><b>€{{number_format($booking->discount)}}</b></li>
							<li><span><i class="fas fa-child"></i> Children Count</span><b>{{$booking->children}}</b></li>
							
							<li><span><i class="fas fa-euro-sign"></i> Total Price</span><b>€{{number_format($booking->amount)}}</b></li>
							<li><span><i class="fas fa-baby"></i> Infants Count</span><b>{{$booking->infants}}</b></li>
							
							<li><span><i class="fab fa-cc-visa"></i> Payment method</span><b>{{$booking->network}}</b></li>
						</ul>
					</div>
				</div>
				<div class="col-md-12 mt-5">
					<h3 class="mb-3">Voucher</h3>
					<div class="d-flex flex-wrap justify-content-between">
						<div class="sec-with-shadow">
							<h4>{{$booking->experience->experience_name}}</h4>
							<p><i class="fas fa-map-marker-alt"></i> {{$booking->experience->experience_lname}}</p>
						</div>
						<div class="sec-with-shadow">
							<h4>Scout Name</h4>
							<p><i class="fas fa-envelope"></i> {{$booking->experience->user->email}}</p>
							<p><i class="fas fa-phone-alt"></i> {{$booking->experience->user->user_mobile}}</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

@endsection