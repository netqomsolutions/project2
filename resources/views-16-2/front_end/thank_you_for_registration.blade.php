@extends('layouts.app')
@section('content')
<section class="thankyou-main">
	<div class="container">
		<div class="row">
			<div class="col-md-12 mb-5">
				<h1 class="text-center text-uppercase" style="color:#ff4883;font-weight: bold;font-size: 45px;letter-spacing: 1px;">Lia Grazia</h1>
				<h2 class="text-center text-uppercase">Thanks for your registration! <span>:)</span></h2>
				<p class="text-center">Thank you for completing the <span style="color:#91ba2b;">"Become a Scout"</span> form. We look forward to having you on board. It will take approximately 1-3 days for our team to review your request. You will then receive a follow-up message in your email inbox with further instructions.</p>
			</div>
		 
		</div>
	</div>
</section>

@endsection