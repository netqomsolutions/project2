@extends('layouts.app')
@section('content')
<?php $url = asset('pages/about_us/'.$aboutUs->page_featured_image); ?>
<section class="inner-banner about-banner" style="background-image: url('<?php echo $url; ?>');">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-12">
                <h3>{{ $aboutUs->page_title }}</span></h3>
                <p>{{ $aboutUs->page_description }}</p>
            </div>
        </div>
    </div>
</section>
<!-- featured exp start here -->
<section class="about-sec aos-item" data-aos="fade-down">
    <div class="container">
        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-12">
                <div class="about-img">
                    <img src="{{ asset('images/splash-overlay.png') }}" alt="splash-img" class="absolute-img">
                    <img src="{{ asset('images/about-img.jpg') }}" alt="about-img">
                    <h3 class="curly-font">Slovenia</h3>
                </div>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-12">
                <div class="about-text">
                    <div class="main-heading">
                        <h5 class="curly-font green-color">Discover & Connect</h5>
                        <h3>How it works</h3>
                    </div>
                    {!! $aboutUs->page_content !!}
                </div>
            </div>
        </div>
    </div>
</section>
<!-- featured exp ends here -->
<section class="how-it-started pink-bg">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-6 col-md-6 col-12 no-pad d-flex align-items-center">
                <div class="how-it-work-text">
                    <h4>HOW IT ALL STARTED</h4>
                   {!! $aboutUs->how_it_started !!}
                </div>
            </div>
            <div class="col-lg-6 col-md-6 col-12 no-pad">
                <img src="{{ asset('pages/about_us/'.$aboutUs->about_first_image) }}" alt="img"/>
            </div>
        </div>
    </div>
</section>
<section class="how-it-started green-bg">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-6 col-md-6 col-12 no-pad">
                <img src="{{ asset('pages/about_us/'.$aboutUs->about_second_image)  }}" alt="img"/>
            </div>
            <div class="col-lg-6 col-md-6 col-12 no-pad d-flex align-items-center">
                <div class="how-it-work-text">
                    <h4>OUR STORY</h4>
                    {!! $aboutUs->our_story !!}
                </div>
            </div>
        </div>
    </div>
</section>
<!-- TESTIMONIALS -->

<section class="testimonials aos-item" data-aos="fade-in">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="main-heading">
                    <h5 class="curly-font green-color">What Travellers Say</h5>
                    <h3>Testimonials</h3>
                </div>
            </div>
            <div class="col-sm-12">
                <div id="customers-testimonials" class="owl-carousel">
                    <!--TESTIMONIAL 1 -->
                    @forelse($testimonials as $test)
                   
                        <div class="item">
                            <div class="shadow-effect">
                                <div class="item-details">
                                    <h5>{{ $test->title }}</h5>
                                    <p>{{ $test->description }}</p>
                                </div>
                                @if($test->profile_image!=null)
                                    <img class="img-responsive" src="{{ asset('pages/testimonialUser/'.$test->profile_image) }}" alt="">  
                                @else
                                <img class="img-responsive" src="{{ asset('users/user.png') }}" alt="team" />
                                @endif
                                <h4 class="testimonial-name">{{ $test->person_name}}<!--span class="designation">Traveler</span--></h4>
                            </div>
                        </div>
                    @empty

                    @endforelse
                </div>
            </div>
        </div>
    </div>
</section>
<!-- END OF TESTIMONIALS -->
@endsection
@section('javascript')
<script src="{{ asset('front_end/js/owl.carousel.min.js') }}" type="text/javascript" charset="utf-8"></script>
    <script>
        $(document).ready(function($) {
        "use strict";
        $('#customers-testimonials').owlCarousel( {
                loop: true,
                center: true,
                items: 3,
                margin: 30,
                autoplay: true,
                dots:true,
            nav:true,
                autoplayTimeout: 8500,
                smartSpeed: 450,
            navText: ['<i class="fa fa-angle-left"></i>','<i class="fa fa-angle-right"></i>'],
                responsive: {
                    0: {
                        items: 1
                    },
                    768: {
                        items: 2
                    },
                    1170: {
                        items: 3
                    }
                }
            });
        });
    </script>
</script>
@endsection