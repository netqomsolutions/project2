@extends('layouts.app')
@section('content')
@php 
	$schedule = '';
	$scheduleArray = [];
	if(!empty($experience->experience_schedule->toArray())){
		$schedule = array_column($experience->experience_schedule->toArray(),'start_date');
	}
	if(!empty($schedule))
	{
     foreach($schedule as $sch)
     {
     	$scheduleArray[]=date('j-n-Y',strtotime($sch));
     }
	}
	$schedule=json_encode($scheduleArray,JSON_UNESCAPED_SLASHES);
	$freshExp = json_encode($experience,JSON_UNESCAPED_SLASHES);
	$social_links = json_decode($social_link);
	$sliderthumCount=0; 
	$sliderCount=0;
	$sliderbulletCount=0;
	$exp_path = '';
        $exp_image = '';
        $exp_title=\Illuminate\Support\Str::limit($experience->experience_name,60);
        
        if(isset($experience) && optional($experience->user)){
            $exp_image = optional($experience->user)->user_image;
            $exp_path = optional($experience->user)->user_image_path;
        }
@endphp
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<link rel="stylesheet" href="{{ asset('css/horizontalvertical.css')}}">
<style type="text/css">
	#horizon-slider ul li:last-child ,.dotwrap div:last-child {
    display: none;
}
</style>
<!-- featured exp start here -->
	<section class="featured-exp featured-exp-listing-grid product-detail-section featured-exp-details">
	<div class="container">
	<div class="row">
	<!-- exp-box start -->

		<div class="col-lg-8 col-md-8 col-12">
		
		
<div class="container p-sm-0">
    <div   id="galleryCarousel" class="carousel slide row">
      <div class="thumbli p-0">
            <div id="carousel-pager" class="carousel slide " data-ride="carousel" data-interval="1000">
                <!-- Carousel items -->
                 <ul class="carousel-indicators m-0">
				  <li class="list-inline-item active">
                        <a id="carousel-selector-0" class="selected" data-slide-to="0" data-target="#galleryCarousel">
                            <img src="{{ asset('pages/experiences/'.$experience->experience_feature_image)}}" class="img-fluid">
                        </a>
                    </li>
					
					  @forelse ($experience->experience_images as $img) 
					  <?php $sliderthumCount++ ;?>

                      <li class="list-inline-item ">
                        <a id="carousel-selector-{{$sliderthumCount}}" class="selected" data-slide-to="{{$sliderthumCount}}" data-target="#galleryCarousel">
                            <img src="{{ asset('pages/experiences/'.$img->image_name) }}" class="img-fluid">
                        </a>
                     </li> 
					  @empty
				      @endforelse
                </ul>
                <!-- Controls -->
                <a class="left carousel-control" href="#carousel-pager" role="button" data-slide="prev">
                    <span class="glyphicon glyphicon-chevron-up" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="right carousel-control" href="#carousel-pager" role="button" data-slide="next">
                    <span class="glyphicon glyphicon-chevron-down" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div>
        </div>
        <div class="mainslideriv">
        <div class="mainslider">
             <div class="carousel-inner">
                    <div class="active carousel-item" data-slide-number="0">
                        <img src="{{ asset('pages/experiences/thumbs/'.str_replace('list-thumb','detail-thumb',$experience->experience_feature_image))}}" class="img-fluid">
                    </div>
                  
					@forelse ($experience->experience_images as $img) 
					<?php $sliderCount++ ; ?>
                    <div class="carousel-item" data-slide-number="{{$sliderCount}}">
                        <img src="{{ asset('pages/experiences/thumbs/'.str_replace('list-thumb','detail-thumb',$img->image_name)) }}" class="img-fluid">
                    </div> 
                    @empty
				    @endforelse
                    
                    <a class="carousel-control-prev" href="#galleryCarousel" role="button" data-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="carousel-control-next" href="#galleryCarousel" role="button" data-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>

                </div>
                <!-- main slider carousel nav controls -->


                <ul class="carousel-indicators list-inline mx-auto mb-0">
                    <li class="list-inline-item active">
                        <a id="carousel-selector-0" class="selected" data-slide-to="0" data-target="#galleryCarousel"> 
                        </a>
                    </li>
					@forelse ($experience->experience_images as $img)
						<?php $sliderbulletCount++ ;?>  
					<li class="list-inline-item ">
                        <a id="carousel-selector-{{$sliderbulletCount}}" class="selected" data-slide-to="{{$sliderbulletCount}}" data-target="#galleryCarousel"> 
                        </a>
                    </li> 
					@empty
				    @endforelse
                    
                </ul>
        </div>

  
    </div>
</div>
</div>
		<div class="detail-shadow-box" id="detail-shadow-box">
		<div class="main-heading text-left with-wishlist d-flex align-items-center">
			<h3>{{ $experience->experience_name }}</h3>
			@if(!empty($user) && $user->user_role == 2)
                          @php
                            $userId = $user->id; 
                            $is_traveler = "addToWishList";
                            @endphp
                          @else
                           @php
                            $is_traveler = "notToaddInWishList";
                            $userId = 0; 
                            @endphp
                          @endif 
                            @php
                              $addToFav = "wishlist-span";
                              $addToFavText = "Add to wishlist";
                            @endphp
                            
                       @if(!empty($UserwishList))
                             @foreach ($UserwishList as $key => $val) 
                                @if($experience->id == $val->experience_id && $val->is_favourite == 1)
                                 @php
                                        $addToFav = "add-to-fav";
                                        $addToFavText = "In WishList";
                                        @endphp
                                 @endif 
                            @endforeach
                        @endif
			<div class="price position-relative experience-detail-list">
			  <a id="wishlist-show" href="javascript:void(0)" data-default="wishlist-span" data-expid="{{ $experience->id }}" data-id="{{ $userId }}" class="{{ $addToFav }} {{ $is_traveler }}"><i class="far fa-heart"></i></a>
							<span id="wishlist-tooltip"> {{ $addToFavText }} </span>
			 </div>
		</div>
		<ul class="after-head-ul d-flex flex-wrap">
			<li><i class="text-euro">€</i> <span><span class="price-inner">{{ number_format($experience->experience_low_price) }} {{ $experience->experience_high_price != 0.00 ? "- ".number_format($experience->experience_high_price) : ''  }}</span> valid for: <b>1 - {{$experience->experience_price_vailid_for}} guests</b></span></li>
			<li><i class="fas fa-user-plus"></i> <span>Extra guest: <b>{{$experience->experience_additional_person}} EUR</b></span></li>
			<li><i class="far fa-user"></i> <i class="far fa-user"></i> <span>Max guests: <b>{{$experience->maximum_participant}}</b></span></li>
			<li><i class="far fa-clock"></i> <span>Duration: <b>{{$experience->experience_duration}} h</b></span></li>
			<li><i class="far fa-clock"></i> <span>Book in advance: <b>{{$experience->experience_booking_in_advance_time}} h</b></span></li>
    </ul>
	<p>{{$experience->experience_description}}</p>
	</div>
    
   <div class="mobile_view_sec">
     <div class="filter-heading mt-2"><h4>Pricing &amp; Ratings</h4></div> 
  
      <div class="filters-left pricing-rating-box">
        
        <!-- <p class="price-p">Price: <span class="price-span">€30</span></p> -->
        <p class="price-inner">€{{number_format($experience->experience_low_price)}}/<span>1-{{$experience->experience_price_vailid_for}} guests</span></p>
        <div class="dates-guests d-flex align-items-center">
        <p class="flex-column align-items-start">DATES<span><input id="datepickermobile" class="exp_detail_datepicker" value="{{date('M d')}}" readonly ></span></p>
        <p id="guest-count" class="flex-column align-items-start guest-field guest-count-class">GUESTS<span class="d-flex align-items-center"><b class="font-weight-normal mr-1" id="dynamic_guest_count">1</b> guest</span></p>
        </div>
        <div class="guest_inner_sec">
       <ul>
        
		<li><span class="left_bar">Adults </span><span class="right_bar"><button class="minus-adult-button"><i class="fa fa-minus"></i></button> <span><input type="number"  min="1" max="{{ $experience->maximum_participant }}" value="1" id="quantity" name="adults" class="adult-cls"><button class="plus-adult-button"><i class="fa fa-plus"></i></button></span></span></li>
         <li><span class="left_bar">Children <br><span class="text_sec_inner">{{$experience->experience_meta_detail->children_age_for_free.'-'.$experience->experience_meta_detail->children_age_for_discount}}</span></span><span class="right_bar"><button class="minus-children-button"><i class="fa fa-minus"></i></button><span><input type="number" min="0" name="children" class="child-cls" max="{{ $experience->maximum_participant }}" value="0" id="quantity"><button class="plus-children-button"><i class="fa fa-plus plus-button"></i></button></span></span></li>
         <li><span class="left_bar">Infants <br><span class="text_sec_inner">Under 2</span></span><span class="right_bar"><button class="minus-infant-button"><i class="fa fa-minus minus-button"></i> </button><span><input type="number" name="infant" min="0" class="infant-cls" value="0" id="quantity"><button class="plus-infant-button"><i class="fa fa-plus plus-button"></i></button></span></span></li>
        </ul><a href="javascript:void(0)" class="save-guest-data">Save</a>
     </div>

		<div class="price-choose-sec-main">
			<div class="putNewHtml">
			
		</div>
		<p class="upon_para" style="display:none;">This is an upon agreement experience. Please contact the scout before booking, if any of the slots suggested above are available on a specific day. </p>
		<a href="{{route('get-experience-slots',['id'=>base64_encode($experience->id)])}}" class="show-more-btn">Show More Dates <i class="fas fa-long-arrow-alt-right" aria-hidden="true"></i></a>
		</div>
  
      <div class="scout-box green-bg">
		<div class="scout-img-name">
            @if (file_exists(public_path($exp_path)) && $exp_image)
			<img src="{{ asset($exp_path.'/'.$exp_image) }}" alt="scout-img"> 
			@else
			<img src="{{ asset('users/user.png') }}" alt="scout-img"> 
			@endif
			<a href="#" class="scout-name">{{$experience->user->user_fname.' '.$experience->user->user_lname}}</a></div>
		
		<div class="scout-rating">
		@if($mainArrRa = array_column($experience->user->review_ratings->toArray(), 'rating'))
		    @php
		        $mainArrRa = array_count_values($mainArrRa);                                    
		        $eachUpeer = array();  $eachLower = array(); $loop = 0;
		        foreach ($mainArrRa as $key => $val) {
		            $eachUpeer[] = $key * $val;
		            $eachLower[] = $val;
		        }
		        $ratingValSum = array_sum($eachUpeer);
		        $ratingNumSum = array_sum($eachLower);
		        $rating = $ratingValSum / $ratingNumSum;    
		    @endphp
		<p>{{ round($rating, 2) }} <i class="fas fa-star" aria-hidden="true"></i> ({{ $ratingNumSum }})</p>
		@else
		<p>0<i class="fas fa-star"></i> (0)</p>
		@endif
		</div>
		
		</div>
      	 
      <div class="filter-cancel-policy">
        <h3>Cancellation policy</h3>
        <p>Full refund available for cancellation made up to  48 h prior to booking start time</p>
      </div>
  
      </div>
   </div> 
	<div class="filter-heading-border"><h4>Price includes:</h4></div>
	<p class="description-p">{{$experience->experience_meta_detail->exp_price_included}}</p>
	<div class="filter-heading-border"><h4>Price excludes:</h4></div>
	<p class="description-p">{{$experience->experience_meta_detail->exp_price_excluded}}</p>	
	
	<div class="filter-heading-border"><h4>Phone:</h4></div>
	<p class="description-p"><a href="tel:+386 (0) 41-862-201">+386 (0) 41-862-201</a></p>
	
	<div class="filter-heading-border"><h4>Hint:</h4></div>
	<p class="description-p">Our #LFZ Scouts and their local experience providers may not be professionally trained as 5* hotel staff. If you can get through to them, though, you can be sure that you will get the best possible authentic insight there is to encounter. See the About link at the top of the page for more info.</p>
	<div class="additional-info-main">
	<div class="additional-info">
		<h5>More information about the experience:</h5>
		<p>{{$experience->experience_meta_detail->exp_additional_information}}</p>
		<h5>More information about the provider:</h5>
		<p>{{$experience->experience_meta_detail->provider_description}}</p>
		<h5>Other info:</h5>
		<p>{{$experience->experience_meta_detail->exp_other_info}}</p>
	</div>
</div>
	@php
		$reviews = $experience->review_ratings;
		$experience_reviews = $reviews->where('review_type',0);
		$scout_reviews = $reviews->where('review_type',3);
		$host_reviews = $reviews->where('review_type',4);
	@endphp
	<div class="tab-reviews exp-detail-new experience_review_tab">
		
		<ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
  <li class="nav-item" role="presentation">
    <a class="nav-link active" id="pills-home-tab" data-toggle="pill" href="#exp_reviews" role="tab" aria-controls="pills-home" aria-selected="true">Experience Reviews<div class="rating">
			
			<div class="d-flex align-items-center tab-ratings">{{getExperienceReviewbyType($experience->id,0)}}
			<ul>
			<li class="filled-star"><i class="fas fa-star" aria-hidden="true"></i></li>
			</ul>({{$reviews->where('review_type',0)->count()}})</div>
			
			</div></a>
  </li>
  <li class="nav-item" role="presentation">
    <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#scout_reviews" role="tab" aria-controls="pills-profile" aria-selected="false">Scout Reviews<div class="rating">
			
			<div class="d-flex align-items-center tab-ratings">{{getExperienceReviewbyType($experience->id,3)}}
			<ul>
			<li class="filled-star"><i class="fas fa-star" aria-hidden="true"></i></li>
			</ul>({{($reviews->where('review_type',3)->count())}})</div>
			
			</div></a>
  </li>
  <li class="nav-item" role="presentation">
    <a class="nav-link" id="pills-contact-tab" data-toggle="pill" href="#host_reviews" role="tab" aria-controls="pills-contact" aria-selected="false">Host Reviews<div class="rating">
			
			<div class="d-flex align-items-center tab-ratings">{{getExperienceReviewbyType($experience->id,4)}}
			<ul>
			<li class="filled-star"><i class="fas fa-star" aria-hidden="true"></i></li>
			</ul>({{$reviews->where('review_type',4)->count()}})</div>
			
			</div></a>
  </li>
</ul>

<div class="tab-content" id="pills-tabContent">
  
  <div class="tab-pane fade show active" id="exp_reviews" role="tabpanel" aria-labelledby="pills-home-tab">
  <!-- testimonial left start here -->

	<div class="left-testimonial-box">
	
<!-- 	<div class="single-testimonial">
	
		<div class="customer-testimonial-details">
			<img src="{{ asset('front_end/images/testimonial-left-img2.png') }}" />
			<div class="name-country">
			
				<div class="rate-name"><a href="#">Karen gard Experience Review</a>
					<div class="rating">
			
			<div class="d-flex align-items-center tab-ratings">4.36
			<ul>
			<li class="filled-star"><i class="fas fa-star" aria-hidden="true"></i></li>
			</ul></div>
			
			</div>
				</div>
				<p><span class="coutry-flag"><img src="images/uk-flag.jpg" /> United kingdom</span> <span class="date green-color">20, Mar, 2020</span></p>
					
			</div>
		</div>
	
	<p>The actual tour was very good. The guides were very friendly and informative. However, the only disappointing part was t...</p>
	</div>
	
	
	<hr> -->
	@if(!empty($experience_reviews))
	@if($host_reviews->count() > 0)
	@foreach($experience_reviews as $exp_review)
	@php
	    $str_length=\Illuminate\Support\Str::length($exp_review->review);
        $exp_image = optional($exp_review->user)->user_image;
        $exp_path = optional($exp_review->user)->user_image_path;

        if(empty($exp_image)){
	    	$exp_image = 'testimonial-left-img.png';
    	}

    	if(empty($exp_path)){
		    $exp_path = 'front_end/images';
    	}
	    
	@endphp
	<div class="single-testimonial">
		<div class="customer-testimonial-details">
			<img src="{{ asset($exp_path.'/'.$exp_image) }}" />
			<div class="name-country">
			<div class="rate-name">
				<a href="javascript;">{{ $exp_review->user->user_fname.' '.$exp_review->user->user_lname }}</a><div class="rating">
			<div class="d-flex align-items-center tab-ratings">{{ $exp_review->rating }}
			<ul>
			<li class="filled-star"><i class="fas fa-star" aria-hidden="true"></i></li>
			</ul></div>
			
			</div></div>
				<p><span class="coutry-flag"><img src="images/newzeland-flag.jpg" /> {{ $exp_review->user->user_address}}</span> <span class="date green-color">{{ date('d M, Y',strtotime($exp_review->created_at))}}</span></p>
					
			</div>
		</div>
	
	<p class="minimize" data-lenght="{{$str_length}}">{{ $exp_review->review }}</p>
	</div>

	<hr>
	@endforeach
	 @else
       <p class="no-reviews d-flex align-items-center m-0 p-4">Be the first reviewer!! <a href="{{route('login') }}" class="btn btn-danger pink-bg ml-4">Click here</a></p>
     @endif
	@endif
	</div>
	
	<!-- testimonial left ends here -->	

  </div>
  
  <div class="tab-pane fade" id="scout_reviews" role="tabpanel" aria-labelledby="pills-profile-tab">
  	
 <!-- testimonial left start here -->

	<div class="left-testimonial-box">
	
<!-- 	<div class="single-testimonial">
	
		<div class="customer-testimonial-details">
			<img src="{{ asset('front_end/images/testimonial-left-img.png') }}" />
			<div class="name-country">
				<a href="#">Karen gard Scout Review</a>
				<p><span class="coutry-flag"><img src="images/uk-flag.jpg" /> United kingdom</span> <span class="date green-color">20, Mar, 2020</span></p>
					<div class="rating">
			
			<ul>
			<li class="filled-star"><i class="fas fa-star" aria-hidden="true"></i></li>
			<li class="filled-star"><i class="fas fa-star" aria-hidden="true"></i></li>
			<li class="filled-star"><i class="fas fa-star" aria-hidden="true"></i></li>			
			<li class="filled-star"><i class="fas fa-star-half-alt" aria-hidden="true"></i></li>
			<li class="empty-star"><i class="fas fa-star" aria-hidden="true"></i></li>
			</ul>
			
			</div>
			</div>
		</div>
	
	<p>The actual tour was very good. The guides were very friendly and informative. However, the only disappointing part was t...</p>
	</div>
	
	<hr> -->
	
	@if(!empty($scout_reviews))
    @if($scout_reviews->count() > 0)
	@foreach($scout_reviews as $exp_review)
		@php
		    $str_length=\Illuminate\Support\Str::length($exp_review->review);
	        $exp_image = optional($exp_review->user)->user_image;
	        $exp_path = optional($exp_review->user)->user_image_path;

	        if(empty($exp_image)){
		    	$exp_image = 'testimonial-left-img.png';
	    	}

	    	if(empty($exp_path)){
			    $exp_path = 'front_end/images';
	    	}
		    
		@endphp
		<div class="single-testimonial">
			<div class="customer-testimonial-details">
				<img src="{{ asset($exp_path.'/'.$exp_image) }}" />
				<div class="name-country">
				<div class="rate-name">
					<a href="#">{{ $exp_review->user->user_fname.' '.$exp_review->user->user_lname }}</a><div class="rating">
				<div class="d-flex align-items-center tab-ratings">{{ $exp_review->rating }}
				<ul>
				<li class="filled-star"><i class="fas fa-star" aria-hidden="true"></i></li>
				</ul></div>
				
				</div></div>
					<p><span class="coutry-flag"><img src="images/newzeland-flag.jpg" /> {{ $exp_review->user->user_address}}</span> <span class="date green-color">{{ date('d M, Y',strtotime($exp_review->created_at))}}</span></p>
						
				</div>
			</div>
		
		<p class="minimize" data-lenght="{{$str_length}}">{{ $exp_review->review }}</p>
		</div>

		<hr>
	@endforeach

	 @else
       <p class="no-reviews d-flex align-items-center m-0 p-4">Be the first reviewer!! <a href="{{route('login') }}" class="btn btn-danger pink-bg ml-4">Click here</a></p>
     @endif
      @endif
	</div>
	
	<!-- testimonial left ends here -->

  </div>
  <div class="tab-pane fade" id="host_reviews" role="tabpanel" aria-labelledby="pills-contact-tab">
  	
  	 <!-- testimonial left start here -->

	<div class="left-testimonial-box">

		@if(!empty($host_reviews))
	    @if($host_reviews->count() > 0)
		@foreach($host_reviews as $exp_review)
			@php
			    $str_length=\Illuminate\Support\Str::length($exp_review->review);
		        $exp_image = optional($exp_review->user)->user_image;
		        $exp_path = optional($exp_review->user)->user_image_path;

		        if(empty($exp_image)){
			    	$exp_image = 'testimonial-left-img.png';
		    	}

		    	if(empty($exp_path)){
				    $exp_path = 'front_end/images';
		    	}
			    
			@endphp
			<div class="single-testimonial">
				<div class="customer-testimonial-details">
					<img src="{{ asset($exp_path.'/'.$exp_image) }}" />
					<div class="name-country">
					<div class="rate-name">
						<a href="#">{{ $exp_review->user->user_fname.' '.$exp_review->user->user_lname }}</a><div class="rating">
					<div class="d-flex align-items-center tab-ratings">{{ $exp_review->rating }}
					<ul>
					<li class="filled-star"><i class="fas fa-star" aria-hidden="true"></i></li>
					</ul></div>
					
					</div></div>
						<p><span class="coutry-flag"><img src="images/newzeland-flag.jpg" />{{ $exp_review->user->user_address}}</span> <span class="date green-color">{{ date('d M, Y',strtotime($exp_review->created_at))}}</span></p>
							
					</div>
				</div>
			
			<p class="minimize" data-lenght="{{$str_length}}">{{ $exp_review->review }}</p>
			</div>

			<hr>
		@endforeach	
           @else
	       <p class="no-reviews d-flex align-items-center m-0 p-4">Be the first reviewer!!  <a href="{{route('login') }}" class="btn btn-danger pink-bg ml-4">Click here</a></p>
	     @endif
	  @endif
	</div>
	
	<!-- testimonial left ends here -->
  </div>
</div>

	</div>
	

	
	</div>
	

	
		<div class="col-lg-4 col-md-4 col-12">
		<div class="chat-box">
	<div class="chat-stick d-flex align-items-center flex-column">
		@if ($experience->user->user_image_path && file_exists(public_path($experience->user->user_image_path)) && $experience->user->user_image)
		<img src="{{ asset($experience->user->user_image_path.'/'.$experience->user->user_image) }}" alt="Admin" class="" />
		@else
		<img src="{{ asset('users/user.png') }}" alt="Admin" class=""  />
		@endif 
		<div class="chat-stick-inner pl-0 w-100 mb-2">
			<div class="scout-rating w-100 text-center">
				<h3><span>Scout:</span> <i class="far fa-comment-dots"></i> {{$experience->user->user_fname.' '.$experience->user->user_lname}}</h3>
				 @if($mainArrRa = array_column($experience->user->review_ratings->toArray(), 'rating'))
            @php
                $mainArrRa = array_count_values($mainArrRa);                                    
                $eachUpeer = array();  $eachLower = array(); $loop = 0;
                foreach ($mainArrRa as $key => $val) {
                    $eachUpeer[] = $key * $val;
                    $eachLower[] = $val;
                }
                $ratingValSum = array_sum($eachUpeer);
                $ratingNumSum = array_sum($eachLower);
                $rating = $ratingValSum / $ratingNumSum;    
            @endphp
            <p>{{ round($rating, 2) }} <i class="fas fa-star"></i> ({{ $ratingNumSum }})</p>
            @else
            <p>0<i class="fas fa-star"></i> (0)</p>
            @endif
			</div>
		<div class="d-flex align-items-center justify-content-center">
			<ul class="contact-social pl-0 mt-0">
				@if(!empty($social_links->facebook_link))  
				<li><a target="_blank" href="{{ !empty($social_links->facebook_link) ? $social_links->facebook_link : 'javascript:;' }}"><i class="fab fa-facebook-f" aria-hidden="true"></i></a></li>
				@endif
				@if(!empty($social_links->instagram_link)) 
				<li><a target="_blank" href="{{ !empty($social_links->instagram_link) ? $social_links->instagram_link : 'javascript:;' }}"><i class="fab fa-instagram" aria-hidden="true"></i></a></li>
				@endif
				@if(!empty($social_links->pinterest_link)) 
				<li><a target="_blank" href="{{ !empty($social_links->pinterest_link) ? $social_links->pinterest_link : 'javascript:;' }}"><i class="fab fa-pinterest-p" aria-hidden="true"></i></a></li>
				@endif
				@if(!empty($social_links->linkedin_link)) 
				<li><a target="_blank" href="{{ !empty($social_links->linkedin_link) ? $social_links->linkedin_link : 'javascript:;' }}"><i class="fab fa-linkedin-in" aria-hidden="true"></i></a></li>
				@endif
				@if(!empty($social_links->twitter_link)) 
				<li><a target="_blank" href="{{ !empty($social_links->twitter_link) ? $social_links->twitter_link : 'javascript:;' }}"><i class="fab fa-twitter" aria-hidden="true"></i></a></li>
				@endif

			</ul>
		</div> 
		@if(!Auth::user())
		<a class="btn btn-success green-bg rounded-btn mt-3" href="{{ route('login') }}">Contact</a>
		@else
		  @if(Auth::user()->user_role==2)
		  <a class="btn btn-success green-bg rounded-btn mt-3" href="{{route('traveler-dashboard',$experience->user->id) }}">Contact </a>
		  @endif
		 @endif
		</div>
	</div>
</div>
	
	<!-- <div class="share-feedback-btn col-md-12 p-0" style="float: right;">
		<a class="share-btn btn btn-success green-bg" target="_blank" href="{{ url('/traveler/review-submission?Eid='.$id) }}">
			<i class="fas fa-share-alt"></i> Share Your Feedback
		</a>
		
	</div> -->
		<div class="right-map">
		<div style="height:100%;" id="map" class="MapId"></div>
		</div>
	<div class="scroll-stick-sec" id="scroll-div">
  <div class="desktop_view_sec">
  	<input type="hidden" value="{{$experience->id}}" id="freshExpId">
		<div class="filter-heading"><h4><i class="fas fa-calculator"></i> Price Calculation</h4></div>

		<div class="filters-left pricing-rating-box">
			
			<!-- <p class="price-p">Price: <span class="price-span">€30</span></p> -->
			<p class="price-inner">€{{number_format($experience->experience_low_price)}}/<span>1-{{$experience->experience_price_vailid_for}} guests</span></p>
			<div class="dates-guests d-flex align-items-center">
			<p class="flex-column align-items-start showDatepicker">DATES<span><input id="datepickerdesktop" class="exp_detail_datepicker" value="{{date('M d')}}" autocomplete="off" /><input type="hidden" name="booking_date" id="booking_date" value="{{date('Y-m-d')}}" autocomplete="off"></span></p>
      <p id="guest-count" class="flex-column align-items-start guest-field guest-count-class">GUESTS<span class="d-flex align-items-center"><b class="font-weight-normal mr-1" id="dynamic_guest_count">1</b> guest</span></p>
      </div>
      <div class="guest_inner_sec">
       <ul>
        
		<li><span class="left_bar">Adults </span><span class="right_bar"><button class="minus-adult-button"><i class="fa fa-minus"></i></button> <span><input type="number"  min="1" max="{{ $experience->maximum_participant }}" value="1" id="quantity" name="adults" class="adult-cls"><button class="plus-adult-button"><i class="fa fa-plus"></i></button></span></span></li>
         <li><span class="left_bar">Children <br><span class="text_sec_inner">{{$experience->experience_meta_detail->children_age_for_free.'-'.$experience->experience_meta_detail->children_age_for_discount}}</span></span><span class="right_bar"><button class="minus-children-button"><i class="fa fa-minus"></i></button><span><input type="number" min="0" name="children" class="child-cls" max="{{ $experience->maximum_participant }}" value="0" id="quantity"><button class="plus-children-button"><i class="fa fa-plus plus-button"></i></button></span></span></li>
         <li><span class="left_bar">Infants <br><span class="text_sec_inner">Under 2</span></span><span class="right_bar"><button class="minus-infant-button"><i class="fa fa-minus minus-button"></i> </button><span><input type="number" name="infant" min="0" class="infant-cls" value="0" id="quantity"><button class="plus-infant-button"><i class="fa fa-plus plus-button"></i></button></span></span></li>
        </ul><a href="javascript:void(0)" class="save-guest-data">Save</a>
     </div>

		<div class="price-choose-sec-main">
			<div class="putNewHtml">
			
		</div>
		<p class="upon_para" style="display:none;">This is an upon agreement experience. Please contact the scout before booking, if any of the slots suggested above are available on a specific day. </p>
		<a href="{{route('get-experience-slots',['id'=>base64_encode($experience->id)])}}" class="show-more-btn">Show More Dates <i class="fas fa-long-arrow-alt-right" aria-hidden="true"></i></a>
		</div>


		<div class="filter-cancel-policy">
			<h3>Cancellation policy</h3>
			<p>Full refund available for cancellation made up to  48 h prior to booking start time</p>
		</div>
		<!--a style="max-width:150px;" href="javascript:void(0)" class="btn btn-success pink-bg rounded-btn m-auto d-block book-me-now">Book Now</a-->

		</div>
  </div>
		
		
		</div>
		

    <!--<button type="button" class="btn btn-danger btn-block pink-bg rounded-btn">Check Availability</button>--->
			

		</div>
	
	
	</div>
	</div>
	</section>
	
	@endsection
@section('javascript')
 <script src="https://polyfill.io/v3/polyfill.min.js?features=default"></script>
    <script src="https://unpkg.com/@google/markerclustererplus@4.0.1/dist/markerclustererplus.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js" ></script>
  <script src="{{ asset('js/horizontalvertical.js')}}" ></script>

<script >
var lat="{{($experience->experience_latitude!='')? $experience->experience_latitude : '46.06627'}}",
	lng="{{($experience->experience_longitude!='')? $experience->experience_longitude : '14.3920139'}}",
	exp_id="{{$experience->id}}",
    iconurl ="{{asset('front_end/images/marker.png')}}",
    exp_name="{{ $experience->experience_name }}",
    maxParticipants="{{ $experience->maximum_participant }}";
    var schedule=<?= $schedule;?>;
    var freshExp = <?= $freshExp;?>;
</script>
<script type="text/javascript">
	 $(".list-inline-item a").click(function() {
	    var currentimg = $(this).data("slide-to") + parseInt(1); 

		$('.thumbli').animate({
		     scrollTop: $('.thumbli li:nth-child('+currentimg+')').position().top
		}, 'slow');

		 $(".mainslider .list-inline-item").removeClass("active"); 
		$('.mainslider .carousel-indicators li:nth-child('+currentimg+')').addClass("active");
	    
	});
	 $(".carousel-control-prev").click(function() { 
		var currentimg =  $(".thumbli .list-inline-item.active a").data("slide-to") - parseInt(1);
		$('.thumbli').animate({
		     scrollTop: $('.thumbli li:nth-child('+currentimg+')').position().top
		}, 'slow');
		 $(".mainslider .list-inline-item").removeClass("active"); 
		$('.mainslider .carousel-indicators li:nth-child('+currentimg+')').addClass("active");
	    
	});
	 $(".carousel-control-next").click(function() { 
		var currentimg =  $(".thumbli .list-inline-item.active a").data("slide-to") + parseInt(1);
		$('.thumbli').animate({
		     scrollTop: $('.thumbli li:nth-child('+currentimg+')').position().top
		}, 'slow');
		 $(".mainslider .list-inline-item").removeClass("active"); 
		$('.mainslider .carousel-indicators li:nth-child('+currentimg+')').addClass("active");
	    
	});
 
	$(window).load(function(){
		  var path = HOST_URL+'/';
		  $.ajaxSetup({
	        headers: {
	            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	        }
		  });
	      var bookingDate = $('#booking_date').val();
	      var adultCount = $('.adult-cls').val();
	      var childCount = $('.child-cls').val();
	      var infantCount = $('.infant-cls').val();
	      var freshExpId = $('#freshExpId').val();
	      $.ajax({
	        url: path+"get-booking-slots", 
	        type: "POST",             
	        data: {freshExpId:freshExpId,bookingDate:bookingDate , adultCount:adultCount,childCount:childCount,infantCount:infantCount,page:"detail-page"},
	        dataType:'json', 
	        success: function(data) {
	          if(data.isSucceeded)
	          {
	          	if(data.upon)
	          	{
	          		$('.upon_para').show();
	          	}
	            $('.putNewHtml').html(data.message);
	            if(data.count>1)
            	{
	            $('.putNewHtml').addClass('slots-scroll');
	        	}
	          }
	          else{
	            $('.putNewHtml').html(data.message);
	             $('.putNewHtml').removeClass('slots-scroll');
	          }
	        }
	      });
	})
</script>
<script type="text/javascript">
       $(document).click(function(e){
         var container = $(".pricing-rating-box"); 
        // If the target of the click isn't the container
        if(!container.is(e.target) && container.has(e.target).length === 0 ){ 
                 $(".guest_inner_sec").slideUp();
        } 
    });
</script>
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
 <script src="{{ asset('js/experience-booking.js') }}"></script>
 
@endsection
	<!-- featured exp ends here -->