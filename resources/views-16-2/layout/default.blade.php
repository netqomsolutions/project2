{{--
Template Name: Metronic - Responsive Admin Dashboard Template build with Twitter Bootstrap 4 & Angular 8
Author: KeenThemes
Website: http://www.keenthemes.com/
Contact: support@keenthemes.com
Follow: www.twitter.com/keenthemes
Dribbble: www.dribbble.com/keenthemes
Like: www.facebook.com/keenthemes
Purchase: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
Renew Support: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
License: You must have a valid license purchased only from themeforest(the above link) in order to legally use the theme for your project.
 --}}
<!DOCTYPE html>
<html   lang="{{ str_replace('_', '-', app()->getLocale()) }}" {{ Metronic::printAttrs('html') }} {{ Metronic::printClasses('html') }} id="{{request()->route()->getActionMethod().'-'.request()->route()-> getActionName()}}">
    <head>
        <meta charset="utf-8"/>

        <meta name="csrf-token" content="{{ csrf_token() }}">

        {{-- Title Section --}}
        <title>{{ config('app.name') }} | @yield('title', $page_title ?? '')</title>

        {{-- Meta Data --}}
        <meta name="description" content="@yield('page_description', $page_description ?? '')"/>
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"/>

        {{-- Favicon --}}
        <link rel="icon" href="{{ asset('front_end/images/favicon.png') }}" type="image/png" sizes="16x16">
        <link href="https://fonts.googleapis.com/css2?family=Barlow:wght@400;500;600;700&family=Pacifico&display=swap" rel="stylesheet">
        
        {{-- Fonts --}}
        {{ Metronic::getGoogleFontsInclude() }}

        {{-- Global Theme Styles (used by all pages) --}}
        @foreach(config('layout.resources.css') as $style)
            <link href="{{ config('layout.self.rtl') ? asset(Metronic::rtlCssPath($style)) : asset($style) }}" rel="stylesheet" type="text/css"/>
        @endforeach

        {{-- Layout Themes (used by all pages) --}}
        @foreach (Metronic::initThemes() as $theme)
            <link href="{{ config('layout.self.rtl') ? asset(Metronic::rtlCssPath($theme)) : asset($theme) }}" rel="stylesheet" type="text/css"/>
        @endforeach

        <link href="{{ asset('front_end/css/common.css') }}" rel="stylesheet" type="text/css" />
       

        <link href="https://cdn.datatables.net/1.10.21/css/jquery.dataTables.min.css" rel="stylesheet">
        <link href="https://cdn.datatables.net/1.10.21/css/dataTables.bootstrap4.min.css" rel="stylesheet">
        <link href="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.6.1/css/bootstrap4-toggle.min.css" rel="stylesheet">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css">
        <link rel="stylesheet" href="{{ asset('css/common_all.css') }}">
        <link href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/2.0.1/css/toastr.css" rel="stylesheet"/>
        <link href="{{ asset('css/image-uploader.min.css') }}" rel="stylesheet"/>
        <link href="{{ asset('front_end/css/bootstrap-select.min.css') }}" rel="stylesheet" type="text/css" />


        

        
        
        {{-- Includable CSS --}}
        @yield('styles')
    </head>

    <body {{ Metronic::printAttrs('body') }} {{ Metronic::printClasses('body') }} >

        @if (config('layout.page-loader.type') != '')
            @include('layout.partials._page-loader')
        @endif

        @include('layout.base._layout')

        <script>var HOST_URL = "{{ route('/') }}";</script>
        <script>var CSRF_TOKEN = "{{ csrf_token() }}";</script>

        {{-- Global Config (global config for global JS scripts) --}}
        <script>
            var KTAppSettings = {!! json_encode(config('layout.js'), JSON_PRETTY_PRINT|JSON_UNESCAPED_SLASHES) !!};
        </script>


        {{-- Global Theme JS Bundle (used by all pages)  --}}
        @foreach(config('layout.resources.js') as $script)
            <script src="{{ asset($script) }}" type="text/javascript"></script>
        @endforeach
        <script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
        <script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script>
        <script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
        <script src="https://cdn.datatables.net/1.10.21/js/dataTables.bootstrap4.min.js"></script>
        <script src="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.6.1/js/bootstrap4-toggle.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/2.0.1/js/toastr.js"></script>
        <script src="{{ asset('js/bootstrap-confirmation.min.js') }}"></script>
        <script src="{{ url('node_modules/@dashboardcode/bsmultiselect/dist/js/BsMultiSelect.min.js') }}"></script>  
        <script src="{{asset('js/pages/custom/profile/profile.js')}}"></script>
        <script src="{{ asset('js/image-uploader.min.js') }}"></script>
        <!-- <script src="{{ asset('js/pages/custom/chat/chat.js') }}"></script> -->
        <script src="{{ asset('js/common_all.js') }}"></script>
        <script src="{{ asset('js/chat_dash.js') }}"></script>
        <script src="{{ asset('js/notifications.js') }}"></script>
        <script src="{{ asset('front_end/js/bootstrap-select.min.js') }}"></script>
        {{-- Includable JS --}}
        @yield('scripts')
        <script type="text/javascript">
            $(function () {
        var confirmationArray={
            placement       : 'top',
            title           : 'Are you sure?',
            btnOkClass      : 'btn btn-sm btn-danger',
            btnOkLabel      : 'Delete',
            btnOkIcon       : 'glyphicon glyphicon-ok',
            btnCancelClass  : 'btn btn-sm btn-default',
            btnCancelLabel  : 'Cancel',
            btnCancelIcon   : 'glyphicon glyphicon-remove',
        };
        $(".user_mobileMasking").inputmask("mask", {
         "mask": "(0) 99 999 999"
  });

  $('[data-toggle="tooltip"]').tooltip()
});
        </script>

    </body>
    
</html>

