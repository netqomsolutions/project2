{{-- Header --}}
 @php 
 $notifdata =  realTimeNotifcationFetch();
 $count =0;  
 @endphp
        @if(!empty($notifdata))
       @php 
        
        $count = $notifdata['main']['total_message']; 
        @endphp
        @endif
@if (config('layout.extras.notifications.dropdown.style') == 'light')
    <div class="d-flex flex-column pt-12 bg-dark-o-5 rounded-top">
        {{-- Title --}}
        <h4 class="d-flex flex-center">
            <span class="text-dark">User Notifications</span>
            <span class="btn btn-text btn-success btn-sm font-weight-bold btn-font-md ml-2">23 new</span>
        </h4>

        {{-- Tabs --}}
        <ul class="nav nav-bold nav-tabs nav-tabs-line nav-tabs-line-3x nav-tabs-primary mt-3 px-8" role="tablist">
            <li class="nav-item">
                <a class="nav-link active show" data-toggle="tab" href="#topbar_notifications_notifications"  >Alerts</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" data-toggle="tab" href="#topbar_notifications_events"  >Events</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" data-toggle="tab" href="#topbar_notifications_logs"  >Logs</a>
            </li>
        </ul>
    </div>
@else
    <div class="d-flex flex-column pt-2 bgi-size-cover bgi-no-repeat rounded-top" style="background-color: #ff4883;">
        {{-- Title --}}
        <h4 class="d-flex flex-center rounded-top">
            <span class="text-white">Notifications</span>
            <span id="notificationCount" class="btn btn-text btn-sm font-weight-bold btn-font-md ml-2 green-bg-btn">{{ $count }} new</span>
        </h4>

        {{-- Tabs --}}
        <!-- <ul class="nav nav-bold nav-tabs nav-tabs-line nav-tabs-line-3x nav-tabs-line-transparent-white nav-tabs-line-active-border-success mt-2 px-8" role="tablist">
            <li class="nav-item">
                <a class="nav-link active show" data-toggle="tab" href="#topbar_notifications_notifications"  >Alerts</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" data-toggle="tab" href="#topbar_notifications_events"  >Events</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" data-toggle="tab" href="#topbar_notifications_logs"  >Logs</a>
            </li>
        </ul> -->
    </div>
@endif
      

{{-- Content --}}
<div class="tab-content">
    {{-- Tabpane --}}
    <div class="tab-pane active show p-8" id="topbar_notifications_notifications" role="tabpanel">
        {{-- Scroll --}}
        <div class="scroll pr-7 mr-n7" data-scroll="true" data-height="300" data-mobile-height="200" id="notification_inner">
            {{-- Item --}} 
@if(!empty($notifdata))
            @foreach($notifdata['main']['msg'] as $k => $data)
              @php 
                $seenit ="notseenActive";
                @endphp
                @if(!empty($notifdata['main']['seen_at'][$k]))
                @php 
                $seenit ="alreadySeenInactive";
                @endphp
                @endif
 
            <div class="d-flex align-items-center mb-6 {{$seenit}}-main">
                {{-- Symbol --}}
                <!-- <div class="symbol symbol-40 symbol-light-primary mr-5">
                    <span class="symbol-label">
                        {{ Metronic::getSVG("media/svg/icons/Home/Library.svg", "svg-icon-lg svg-icon-primary") }}
                    </span>
                </div> -->

                {{-- Text --}}
               
                <div class="d-flex flex-column font-weight-bold">
                    <a href="javascript:void(0)" class="notification_an_lfz {{ $seenit }}" data-id="{{ $notifdata['main']['id'][$k] }}" data-src="{{ $notifdata['main']['url'][$k] }}" class="text-dark text-hover-primary mb-1 font-size-lg">
                        <span href="#" class="btn btn-icon btn-light  btn-sm">
                            <span class="svg-icon svg-icon-2x"><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                    <rect x="0" y="0" width="24" height="24"></rect>
                                    <path d="M6.82866499,18.2771971 L13.5693679,12.3976203 C13.7774696,12.2161036 13.7990211,11.9002555 13.6175044,11.6921539 C13.6029128,11.6754252 13.5872233,11.6596867 13.5705402,11.6450431 L6.82983723,5.72838979 C6.62230202,5.54622572 6.30638833,5.56679309 6.12422426,5.7743283 C6.04415337,5.86555116 6,5.98278612 6,6.10416552 L6,17.9003957 C6,18.1765381 6.22385763,18.4003957 6.5,18.4003957 C6.62084305,18.4003957 6.73759731,18.3566309 6.82866499,18.2771971 Z" fill="#000000" opacity="0.3"></path>
                                    <path d="M12.828665,18.2771971 L19.5693679,12.3976203 C19.7774696,12.2161036 19.7990211,11.9002555 19.6175044,11.6921539 C19.6029128,11.6754252 19.5872233,11.6596867 19.5705402,11.6450431 L12.8298372,5.72838979 C12.622302,5.54622572 12.3063883,5.56679309 12.1242243,5.7743283 C12.0441534,5.86555116 12,5.98278612 12,6.10416552 L12,17.9003957 C12,18.1765381 12.2238576,18.4003957 12.5,18.4003957 C12.6208431,18.4003957 12.7375973,18.3566309 12.828665,18.2771971 Z" fill="#000000"></path>
                                </g>
                            </svg>
                        </span>
                    </span>
 
                        <span class="text-muted">{{ $data }} </span></a>
                    
                </div>
            </div>
 @endforeach
 @else
 <span class="text-muted"> No New Notification </span>
  @endif
             
        </div>
    </div>

    {{-- Tabpane --}}
    <div class="tab-pane" id="topbar_notifications_events" role="tabpanel">
        {{-- Nav --}}
        <div class="navi navi-hover scroll my-4" data-scroll="true" data-height="300" data-mobile-height="200">
            {{-- Item --}}
            <a href="#" class="navi-item">
                <div class="navi-link">
                    <div class="navi-icon mr-2">
                        <i class="flaticon2-line-chart text-success"></i>
                    </div>
                    <div class="navi-text">
                        <div class="font-weight-bold">
                            New report has been received
                        </div>
                        <div class="text-muted">
                            23 hrs ago
                        </div>
                    </div>
                </div>
            </a>

            {{-- Item --}}
            <a href="#" class="navi-item">
                <div class="navi-link">
                    <div class="navi-icon mr-2">
                        <i class="flaticon2-paper-plane text-danger"></i>
                    </div>
                    <div class="navi-text">
                        <div class="font-weight-bold">
                            Finance report has been generated
                        </div>
                        <div class="text-muted">
                            25 hrs ago
                        </div>
                    </div>
                </div>
            </a>

            {{-- Item --}}
            <a href="#" class="navi-item">
                <div class="navi-link">
                    <div class="navi-icon mr-2">
                        <i class="flaticon2-user flaticon2-line- text-success"></i>
                    </div>
                    <div class="navi-text">
                        <div class="font-weight-bold">
                            New order has been received
                        </div>
                        <div class="text-muted">
                            2 hrs ago
                        </div>
                    </div>
                </div>
            </a>

            {{-- Item --}}
            <a href="#" class="navi-item">
                <div class="navi-link">
                    <div class="navi-icon mr-2">
                        <i class="flaticon2-pin text-primary"></i>
                    </div>
                    <div class="navi-text">
                        <div class="font-weight-bold">
                            New customer is registered
                        </div>
                        <div class="text-muted">
                            3 hrs ago
                        </div>
                    </div>
                </div>
            </a>

            {{-- Item --}}
            <a href="#" class="navi-item">
                <div class="navi-link">
                    <div class="navi-icon mr-2">
                        <i class="flaticon2-sms text-danger"></i>
                    </div>
                    <div class="navi-text">
                        <div class="font-weight-bold">
                            Application has been approved
                        </div>
                        <div class="text-muted">
                            3 hrs ago
                        </div>
                    </div>
                </div>
            </a>

            {{-- Item --}}
            <a href="#" class="navi-item">
                <div class="navi-link">
                    <div class="navi-icon mr-2">
                        <i class="flaticon2-pie-chart-3 text-warning"></i>
                    </div>
                    <div class="navinavinavi-text">
                        <div class="font-weight-bold">
                            New file has been uploaded
                        </div>
                        <div class="text-muted">
                            5 hrs ago
                        </div>
                    </div>
                </div>
            </a>

            {{-- Item --}}
            <a href="#" class="navi-item">
                <div class="navi-link">
                    <div class="navi-icon mr-2">
                        <i class="flaticon-pie-chart-1 text-info"></i>
                    </div>
                    <div class="navi-text">
                        <div class="font-weight-bold">
                            New user feedback received
                        </div>
                        <div class="text-muted">
                            8 hrs ago
                        </div>
                    </div>
                </div>
            </a>

            {{-- Item --}}
            <a href="#" class="navi-item">
                <div class="navi-link">
                    <div class="navi-icon mr-2">
                        <i class="flaticon2-settings text-success"></i>
                    </div>
                    <div class="navi-text">
                        <div class="font-weight-bold">
                            System reboot has been successfully completed
                        </div>
                        <div class="text-muted">
                            12 hrs ago
                        </div>
                    </div>
                </div>
            </a>

            {{-- Item --}}
            <a href="#" class="navi-item">
                <div class="navi-link">
                    <div class="navi-icon mr-2">
                        <i class="flaticon-safe-shield-protection text-primary"></i>
                    </div>
                    <div class="navi-text">
                        <div class="font-weight-bold">
                            New order has been placed
                        </div>
                        <div class="text-muted">
                            15 hrs ago
                        </div>
                    </div>
                </div>
            </a>

            {{-- Item --}}
            <a href="#" class="navi-item">
                <div class="navi-link">
                    <div class="navi-icon mr-2">
                        <i class="flaticon2-notification text-primary"></i>
                    </div>
                    <div class="navi-text">
                        <div class="font-weight-bold">
                            Company meeting canceled
                        </div>
                        <div class="text-muted">
                            19 hrs ago
                        </div>
                    </div>
                </div>
            </a>

            {{-- Item --}}
            <a href="#" class="navi-item">
                <div class="navi-link">
                    <div class="navi-icon mr-2">
                        <i class="flaticon2-fax text-success"></i>
                    </div>
                    <div class="navi-text">
                        <div class="font-weight-bold">
                            New report has been received
                        </div>
                        <div class="text-muted">
                            23 hrs ago
                        </div>
                    </div>
                </div>
            </a>

            {{-- Item --}}
            <a href="#" class="navi-item">
                <div class="navi-link">
                    <div class="navi-icon mr-2">
                        <i class="flaticon-download-1 text-danger"></i>
                    </div>
                    <div class="navi-text">
                        <div class="font-weight-bold">
                            Finance report has been generated
                        </div>
                        <div class="text-muted">
                            25 hrs ago
                        </div>
                    </div>
                </div>
            </a>

            {{-- Item --}}
            <a href="#" class="navi-item">
                <div class="navi-link">
                    <div class="navi-icon mr-2">
                        <i class="flaticon-security text-warning"></i>
                    </div>
                    <div class="navi-text">
                        <div class="font-weight-bold">
                            New customer comment recieved
                        </div>
                        <div class="text-muted">
                            2 days ago
                        </div>
                    </div>
                </div>
            </a>

            {{-- Item --}}
            <a href="#" class="navi-item">
                <div class="navi-link">
                    <div class="navi-icon mr-2">
                        <i class="flaticon2-analytics-1 text-success"></i>
                    </div>
                    <div class="navi-text">
                        <div class="font-weight-bold">
                            New customer is registered
                        </div>
                        <div class="text-muted">
                            3 days ago
                        </div>
                    </div>
                </div>
            </a>
        </div>
    </div>

    {{-- Tabpane --}}
    <div class="tab-pane" id="topbar_notifications_logs" role="tabpanel">
        {{-- Nav --}}
        <div class="d-flex flex-center text-center text-muted min-h-200px">
            All caught up!
            <br/>
            No new notifications.
        </div>
    </div>
</div>
