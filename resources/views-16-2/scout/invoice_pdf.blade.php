<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Invoice - LFZ</title>

    <style type="text/css">
        @page {
            margin: 0px;
        }
        body {
            margin: 0px;
            font-size: 20px;
        }
        * {
            font-family: Verdana, Arial, sans-serif;
        }
        a {
            color: #fff;
            text-decoration: none;
        }
        table {
            font-size: x-small;
        }
        tfoot tr td {
            font-weight: bold;
            font-size: x-small; 
        }
        td { 
            font-size: 16px;
             color: #2c2e35;
        }
        .invoice table {
            margin: 15px;
        }
        .invoice h3 {
            margin-left: 15px;
        }
        .information {
            background-color: #fff;
            color: #2c2e35;
        }
        .information .logo {
            margin: 5px;
        }
        .information table {
            padding: 10px;
        }
        .bgGray {
               background-color: #f4f4f4;
			    font-size: 16px;
			    padding: 26px 20px; 
			    border-radius: 8px;
			    margin: 40px 0 0 0;
        }

       .invoice td ,   .invoice th { 
			    font-size: 16px;
			    padding: 22px 16px; 
			    border-radius: 8px;
			    margin: 40px 0 0 0;
        }
        .invoice-foot {
        	    font-size: 26.67px;
                font-weight: 500;
                display: block;
            }
        .logo{
            width:100%;
        }
    </style>

</head>
<body>
@php 

$condata = json_decode($contact->page_content,true);
$contact->email=$condata['email'];
$contact->address=$condata['address'];
$contact->phone=$condata['phone'];

             $experience_price_vailid_for = $bookingDetail->experience->experience_price_vailid_for;
             $admin_comisssion= getSettingMeta('admin_comission');
             $booking_amount=$bookingDetail->amount;
             $admin_amount= ($admin_comisssion / 100 ) * $booking_amount;

             if(!empty($scout_comission) && $scout_comission!==null){
                 $scout_amount= ($scout_comission / 100 ) * $booking_amount;
             }else{
                $scout_amount= (15 / 100 ) * $booking_amount;
             }
            $totalaAmt=  $scout_amount;  
            $totalChildAmt = $bookingDetail->children * $bookingDetail->experience->experience_low_price;   
            $totalguest = $bookingDetail->adults + $bookingDetail->children  + $bookingDetail->infants ;
@endphp
<div class="information">
    <table width="100%">
        <tr>
            <td align="left" style="width: 40%;">
             <img style="width:150px;" src="{{asset('images/logo.jpg')}}" alt="Logo"   class="logo"/>

            </td>
            
            <td align="right" style="width: 40%;">
  
            <span style="font-size: 20px;font-weight: 500;text-align: right;margin:0;display:block;width:100%;float:left;"><span style="width:300px;display:inline-block;float:right;">{{$contact->address}}</span></span><br/><br/>
            <span style="display:block;float:left;width:100%;margin:20px 0 0;">
                Tax ID: 14056887<br/>
                IBAN: SI56 6100 0002 4450 361<br/>
                BIC code: HDELSI22<br/>
                Registration No.: 8714207000<br/>
            </span>
                 
            </td>
        </tr>

    </table>
</div>
<div class="information" style="margin:20px 0 0;">
    <table width="100%">
        <tr>
            <td align="left" style="width: 40%;">
                 <b>Customer invoice No.</b>{{$bookingDetail->order_number}}<br/><b>Reservation code:</b> H205XML<br/>
    				Litija,{{date('M d, Y ', strtotime($bookingDetail->booking_date))}}<br/>
    				<p style="font-size: 20px;font-weight: 400;margin: 30px 0 0 0;">{{$bookingDetail->traveler->user_address}}</p>
           </td>
            
            <td align="right" style="width: 40%;">
             
                <table width="100%" style="padding:0;">
                <tr>
                    <td style="padding:0;" align="right">
                        <img src="{{asset('images/qr-code-img.png')}}">
                        <p style="font-size: 16.67px;font-weight: 400;margin:0;line-height:0;">Experience info.</p>
                    </td>
                </tr>
                <tr>
                    <td align="right" style="padding:20px 0 0;">
                        <b>Scout phone:</b> {{ formatPhoneNo($bookingDetail->scout->user_mobile,$bookingDetail->scout->user_mobile_code) }} <br/>
                        <b>Local provider phone:</b>{{ formatPhoneNo($bookingDetail->experience->host->user_mobile,$bookingDetail->experience->host->user_mobile_code) }}<br/>
                   </td>
                </tr>
                </table>
                 
            </td>
        </tr>

    </table>
</div>


<br/>

<div class="invoice"> 
    <table width="100%">
      
        <tbody>
        <tr class='bgGray'>
            <th align="left" >Experience title:</th>
            <td align="right" >{{$bookingDetail->experience->experience_name}}</td> 
        </tr>
        <tr>
            <th align="left" >No. of guests:</th>
            <td align="right" >{{$totalguest}}</td> 
        </tr>
        <tr class='bgGray'>
            <th align="left" >Travel to destination:</th>
            <td align="right">{{$bookingDetail->experience->experience_lname}}</td> 
        </tr>
        <tr>
            <th align="left" >Exact location to meet:</th>
            <td align="right">{{$bookingDetail->experience_meta_detail->meet_location}}</td> 
        </tr>
        <tr class='bgGray'>
            <th align="left" >Experience date and time:</th>
            <td align="right">{{date('M d, Y', strtotime($bookingDetail->booking_date))}} at {{date('h:m', strtotime($bookingDetail->booking_date))}}</td> 
        </tr>
        </tbody>

        <tfoot>
        <tr> 
            <td align="center" colspan="2">RESERVATION CHARGES</td> 
        </tr> 
          <tr  class='bgGray'> 
            <td align="left" style="margin: 0;font-weight: 400;font-size: 16.67px;" ><b style="font-size: 26.67px;font-weight: 500;display: block;" >Total amount:</b> <br/> Tax:</td>
            <td align="right" class="gray" style="margin: 0;font-weight: 400;font-size: 16.67px;"><b style="font-size: 26.67px;font-weight: 500;display: block;">EUR {{number_format($totalaAmt)}}</b></br>EUR 0,00</td>
        </tr>
        <tr>
                    <td colspan="2" align="center" style="font-size: 16px;font-weight: 400;">VAT is not calculated on the basis of paragraph 1 of Article 94 of the Value Added Tax Act.</td>
                </tr>
                <tr>
                    <td colspan="2" align="left" style="font-size: 16px;font-weight: 400;">EOR: a4ceaa98-910d-42fb-b419-a684af0db78b<br>ZOI: 6fd9618990e808ee655a93542e5b7b1d</td>
                </tr>
        </tfoot>
    </table>
</div>
 
</body>
</html>