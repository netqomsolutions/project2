{{-- Extends layout --}}
@extends('layout.default')
{{-- Content --}}
@section('content')
<style>
	.toggle.ios, .toggle-on.ios, .toggle-off.ios { border-radius: 20rem; }
	.toggle.ios .toggle-handle { border-radius: 20rem; }
	div#action-scout {
	    align-items: center;
	    justify-content: center;
	}
</style>
@php
$condata = json_decode($contact->page_content,true);
$contact->email=$condata['email'];
$contact->address=$condata['address'];
$contact->phone=$condata['phone'];

 $experience_price_vailid_for = $bookingDetail->experience->experience_price_vailid_for;
            $children_discount_1 = $bookingDetail->experience_meta_detail->children_discount_1;
            $totalAmt = 0;
            $totalChildAmt = 0;
            if($bookingDetail->experience->experience_type == 1){
                for ($i=0; $i < $bookingDetail->adults ; $i++) { 
                    if($i<$bookingDetail->experience->experience_price_vailid_for){
                        $totalAmt = $bookingDetail->experience->experience_low_price;
                    }else{
                        $totalAmt = $totalAmt + $bookingDetail->experience->experience_additional_person;
                    }
                }
            }else if($bookingDetail->experience->experience_type == 2){
                for ($i=0; $i < $bookingDetail->adults ; $i++) { 
                    if($i < $bookingDetail->experience->minimum_participant){
                        $totalAmt = $bookingDetail->experience->experience_low_price;
                    }else{
                        $totalAmt = $totalAmt + $bookingDetail->experience->experience_additional_person;
                    }
                }
            }else if($bookingDetail->experience->experience_type == 3){
                if(!empty($bookingDetail->experience->experience_additional_prices)){
                    $totalAmt = $bookingDetail->experience->experience_additional_prices[0]->additional_price;
                }
            }
            $totalChildAmt = $bookingDetail->children * $bookingDetail->experience->experience_low_price;
            $disPrice = ($children_discount_1 / 100 ) * $totalChildAmt; 
            $childNetPrice = $totalChildAmt - $disPrice;
            $totalaAmt =  $childNetPrice + $totalAmt;
@endphp
@if(session()->has('success'))
            <div class="alert alert-success alluser-page-suc" style="text-align: left;float: left;width: 100%;">
                {{ session()->get('success') }}
            </div>
            <script type="text/javascript">setTimeout(function(){ $('.alluser-page-suc').fadeOut(); }, 3000);</script>
        @elseif(session()->has('failure'))
            <div class="alert alert-danger alluser-page-suc" style="text-align: left; left;float: left;width: 100%;">
                {{ session()->get('failure') }}
            </div>
            <script type="text/javascript">setTimeout(function(){ $('.alluser-page-suc').fadeOut(); }, 3000);</script>
        @endif
        <div class="d-flex flex-row w-100">
<div class="card card-custom overflow-hidden mb-8 w-100">
									<div class="card-body p-0">
										<!-- begin: Invoice-->
										<!-- begin: Invoice header-->
										<div class="row justify-content-center py-8 px-8 py-md-27 px-md-0">
											<div class="col-md-9">
												<div class="d-flex justify-content-between pb-10 pb-md-20 flex-column flex-md-row">
													<h1 class="display-4 font-weight-boldest mb-10">INVOICE</h1>
													<div class="d-flex flex-column align-items-md-end px-0">
														<!--begin::Logo-->
														<!--end::Logo-->
														<span class="d-flex flex-column align-items-md-end opacity-70">
															<span>{{$contact->address}}</span>
															<!--span>Mississippi 96522</span-->
														</span>
													</div>
												</div>
												<div class="border-bottom w-100"></div>
												<div class="d-flex justify-content-between pt-6">
													<div class="d-flex flex-column flex-root">
														<span class="font-weight-bolder mb-2">Date</span>
														<span class="opacity-70">{{date('M d, Y ', strtotime($bookingDetail->booking_date))}}</span>
													</div>
													<div class="d-flex flex-column flex-root">
														<span class="font-weight-bolder mb-2">INVOICE NO.</span>
														<span class="opacity-70">{{$bookingDetail->charge_id}}</span>
													</div>
													<div class="d-flex flex-column flex-root">
														<span class="font-weight-bolder mb-2">INVOICE TO.</span>
														<span class="opacity-70">{{$bookingDetail->traveler->user_address}}</span>
													</div>
												</div>
											</div>
										</div>
										<!-- end: Invoice header-->
										<!-- begin: Invoice body-->
										<div class="row justify-content-center py-8 px-8 py-md-10 px-md-0">
											<div class="col-md-9">
												<div class="table-responsive">
													<table class="table">
														<thead>
															<tr>
																<th class="pl-0 font-weight-bold text-muted text-uppercase">Members</th>
																<th class="text-right font-weight-bold text-muted text-uppercase">Count</th>
																<th class="text-right font-weight-bold text-muted text-uppercase">Discount</th>
																<th class="text-right pr-0 font-weight-bold text-muted text-uppercase">Amount</th>
															</tr>
														</thead>
														<tbody>
															<tr class="font-weight-boldest">
																<td class="pl-0 pt-7">Adults </td>
																<td class="text-right pt-7">{{$bookingDetail->adults}}</td>
																<td class="text-right pt-7">0</td>
																<td class="text-pink pr-0 pt-7 text-right">{{number_format($totalAmt)}}</td>
															</tr>
															<tr class="font-weight-boldest border-bottom-0">
																<td class="border-top-0 pl-0 py-4">Children </td>
																<td class="border-top-0 text-right py-4">{{$bookingDetail->children}}</td>
																<td class="border-top-0 text-right py-4">{{number_format($disPrice)}}</td>
																<td class="text-pink border-top-0 pr-0 py-4 text-right">{{number_format($childNetPrice)}}</td>
															</tr>
															<tr class="font-weight-boldest border-bottom-0">
																<td class="border-top-0 pl-0 py-4">Infants</td>
																<td class="border-top-0 text-right py-4">{{$bookingDetail->infants}}</td>
																<td class="border-top-0 text-right py-4">0</td>
																<td class="text-pink border-top-0 pr-0 py-4 text-right">Free</td>
															</tr>
														</tbody>
													</table>
												</div>
											</div>
										</div>
										<!-- end: Invoice body-->
										<!-- begin: Invoice footer-->
										<!--div class="row justify-content-center bg-gray-100 py-8 px-8 py-md-10 px-md-0">
											<div class="col-md-9">
												<div class="table-responsive">
													<table class="table">
														<thead>
															<tr>
																<th class="font-weight-bold text-muted text-uppercase">BANK</th>
																<th class="font-weight-bold text-muted text-uppercase">ACC.NO.</th>
																<th class="font-weight-bold text-muted text-uppercase">DUE DATE</th>
																<th class="font-weight-bold text-muted text-uppercase">TOTAL AMOUNT</th>
															</tr>
														</thead>
														<tbody>
															<tr class="font-weight-bolder">
																<td>BARCLAYS UK</td>
																<td>12345678909</td>
																<td>Jan 07, 2018</td>
																<td class="text-pink font-size-h3 font-weight-boldest">20,600.00</td>
															</tr>
														</tbody>
													</table>
												</div>
											</div>
										</div-->
										<div class="row justify-content-center bg-gray-100 py-8 px-8 py-md-10 px-md-0">
											<div class="col-md-9">
												<div class="table-responsive">
													<table class="table">
														<thead>
															<tr>
																<th class="font-weight-bold text-muted text-uppercase">Card</th>
																<th class="font-weight-bold text-muted text-uppercase">Number</th>
																<th class="font-weight-bold text-muted text-uppercase">Expiry Date</th>
																<th class="font-weight-bold text-muted text-uppercase">TOTAL AMOUNT</th>
															</tr>
														</thead>
														<tbody>
															<tr class="font-weight-bolder">
																<td>{{$bookingDetail->network}}</td>
																<td>**** **** **** {{$bookingDetail->last4}}</td>
																<td>{{$bookingDetail->exp_month.'/'.$bookingDetail->exp_year}}</td>
																<td class="text-pink font-size-h3 font-weight-boldest">{{number_format($totalaAmt)}}</td>
															</tr>
														</tbody>
													</table>
												</div>
											</div>
										</div>
										<!-- end: Invoice footer-->
										<!-- begin: Invoice action-->
										<div class="row justify-content-center py-8 px-8 py-md-10 px-md-0">
											<div class="col-md-9">
												<div class="d-flex justify-content-between">
													<a href="javascript;" class="btn btn-light-primary font-weight-bold pink-bg-btn" download >Download Invoice</a>
													<button type="button" class="btn btn-primary font-weight-bold green-bg-btn	" onclick="window.print();">Print Invoice</button>
												</div>
											</div>
										</div>
										<!-- end: Invoice action-->
										<!-- end: Invoice-->
									</div>
								</div>
     </div>
     @endsection
{{-- Scripts Section --}}
@section('scripts')

@endsection