<!doctype html>
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
  <head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <meta name="format-detection" content="telephone=no">
    <meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0; user-scalable=no;">
    <meta http-equiv="x-ua-compatible" content="IE=9; IE=8; IE=7; IE=EDGE">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="x-ua-compatible" content="ie-edge">
    <meta name="x-apple-disable-message-reformatting">
    <title>BECOME A SCOUT<</title> 
  </head>
 <body style="margin:0;padding:0;">
    <table cellspacing="0" cellpadding="0" width="600" align="center" style="font-family: Arial, Helvetica, sans-serif;background: url({{ asset('images/email-template-bg.jpg') }});">
        <tr><td style="padding:50px 0 0;" align="center"><img src="{{ asset('front_end/images/logo.png') }}"/></td></tr>
        <tr><td align="center" style="font-size:25px;color:#0d0d0d;letter-spacing: 1px;padding:40px 0 0;">WELCOME SCOUT</td></tr>
        <tr><td align="center" style="color:#ff4883;font-weight: bold;font-size: 45px;letter-spacing: 1px;">{{ isset($data) ? ucfirst($data['user_fname']).' '.ucfirst($data['user_lname']): '' }}</td></tr>
        <tr><td align="center" style="font-size:16px;line-height: 27px;padding: 35px 30px 38px;color:#0d0d0d;">Thank you for completing the <span style="color:#91ba2b;">"Become a Scout"</span> form. We look forward to having you on board. It will take approximately 1-3 days for our team to review your request. You will then receive a follow-up message in your email inbox with further instructions.</td></tr>
        <tr><td align="center" style="font-weight: bold;font-size:17px;color:#0d0d0d;padding:0 0 20px;">Any additional question?</td></tr>
        <tr>
            <td>
                <table width="100%" style="background: #dfdfdf;padding: 20px 25px 16px;">
                    <tr>
                        <td colspan="2" style="font-weight: bold;font-size:15px;padding:0;"><a style="color:#0d0d0d;text-decoration: none;" href="mailto:locals@tourismfromzero.org"><img src="{{ asset('images/mail-icon-email.png') }}" style="margin: 0 4px 0 0px;position: relative;top: 1px;"> locals@tourismfromzero.org</a></td>
                        <td align="right" colspan="2" style="font-weight: bold;font-size:15px;padding:0;"><a style="color:#0d0d0d;text-decoration: none;" href="tel:+386041825569"><img src="{{ asset('images/phone-icon-email.png') }}" style="margin: 0 4px 0 0px;position: relative;top: 3px;"> +386 (0) 41 825 569</a></td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</body>
</html>