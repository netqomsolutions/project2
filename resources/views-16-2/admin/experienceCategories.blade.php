{{-- Extends layout --}}
@extends('layout.default')

{{-- Content --}}
@section('content')
<div class="row">
    <div class="col-lg-12">
    	<meta name="csrf-token" content="{{ csrf_token() }}" />
      
		 <div class="section-heading-home text-center">
                    <h3> Experince Category Management</h3>
                   </div>
		<div class="card card-custom">
			<div class="card-header">
				<div class="card-title justify-content-between w-100 flex-column-sm">
					<div class="left-cart-label d-flex align-items-center">
					<span class="card-icon">
						<i class="flaticon2-supermarket text-primary"></i>
					</span>
					<h3 class="card-label">List of Experince Categories </h3>
				</div>
					<button type="button" class="btn btn-sm float-right add-exp-cat-modal green-bg action-blog-edit-btn"  blog-cat-id="" page-name="" act-name="edit-blog-cat" data-toggle="modal" data-target="#add-exp-cat-modal" data-id="0">Add New Category</button>
				</div>
			</div>
			<div class="card-body">
				
				<!--begin: Datatable-->
				<table class="table table-bordered categoris-datatable" style="margin-top: 13px !important">
					<thead>
						<tr>
							<th>Name</th>
							<th>Actions</th>
						</tr>
					</thead>
				</table>
				<!--end: Datatable-->
				<!-- /.modal -->
			    <div class="modal fade" id="add-exp-cat-modal">
			        <div class="modal-dialog">
			            <form id="add-blog-category-form" action="{{route('admin-experience-categories')}}" method="post" enctype="multipart/form-data">
			                <div class="modal-content">
			                    <div class="modal-header">
			                        <h4 class="modal-title scout-title">Add New Experince Category</h4>
			                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
			                        <span aria-hidden="true">&times;</span></button>
			                    </div>
			                    <div class="modal-body">
			                       
			                        @csrf
			                        <div class="form-group">
			                        <label>Name</label>
			                        <input type="text" name="name"  class="form-control"  value="" id="name">
			                        <input type="hidden" name="id" id="exp_cat_id" value="">
			                    	</div>
			                    	
			                    </div>
			                    <div class="modal-footer justify-content-between">
			                        <button type="button" class="btn btn-outline-light" data-dismiss="modal">Cancel</button>
			                        <button type="submit" class="btn btn-outline-light" id="add-blog-cat-button">Submit</button>
			                    </div>
			                </div>
			            </form>
			        </div>
			    </div>
			    <!-- /.modal -->
			</div>
		</div>
		<!--end::Card-->
    </div>
</div>

@endsection

{{-- Scripts Section --}}
@section('scripts')

<script type="text/javascript">
$.ajaxSetup({ headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') } });
 var table = $('.categoris-datatable').DataTable({
            processing: true,
            serverSide: true,
            ajax: "{{ route('admin-experience-categories') }}",
            columns: [
                
                {data: 'name', name: 'name'},
                {
                    data: 'action', 
                    name: 'action', 
                    orderable: false, 
                    searchable: true
                },
            ],
            order: [[1, 'asc']],
            fnDrawCallback: function( oSettings ) {
            /* 01-02-2021 @RT */
                  $('.action-row-delete-btn').confirmation({
                  	    content: "You want to delete this.",
			            onConfirm: function() {
			                $this = $(this); 
				 			var id = $(this).attr("data-id");  
				            $.ajax({
					            url: `{{ route("delete-row") }}`,
					            type: "POST",
					            data: {'id':id,'is_ajax':1,'table':'experience_categories'},
					            dataType: "Json",
					            success: function (data) {
					            toastr.options.timeOut = 1500;
					            if(data.isSucceeded){
					            toastr.success(data.message);
					            $('.categoris-datatable').DataTable().ajax.reload();
					            }else{
					            toastr.error("Please try again");
					            }
					            },
					            error: function (xhr, ajaxOptions, thrownError) {
					            toastr.error("Please try again");
					            }
			               });
			                }

			     });
        }
    });


  
 $(document).on("click",'.add-exp-cat-modal-edit,.add-exp-cat-modal',function(){
    $this = $(this);
 	var cat_id = $this.attr("data-id");
	var title;
	if(cat_id == 0){
		 title = "Add New Experince Category";
	}else{
		 title = "Edit New Experince Category";
	}
 	$.ajax({
          type: 'POST',
          url: "{{ route('get-experience-categories') }}",
          data: {_token: "{{ csrf_token() }}", 'id':cat_id,'is_ajax':1} ,
          dataType: "Json",
            success: function(res) {  
            		if(cat_id == 0){
            			 $(".scout-title").text(title);
		            	  $("#name").val('');
		            	  $("#exp_cat_id").val('');
            		}else{
            			 $(".scout-title").text(title);
		            	  $("#name").val(res.data.name);
		            	  $("#exp_cat_id").val(res.data.id);
            		}
            	  
            	  $("#add-exp-cat-modal").modal('show');
            }
        });
 });
 
 
</script>
@endsection