{{-- Extends layout --}}
@extends('layout.default')
{{-- Content --}}
@section('content')
<div class="content d-flex flex-column flex-column-fluid pt-0 pb-7" id="kt_content">
 
		<!--begin::Entry-->
	<div class="d-flex flex-column-fluid">
		<!--begin::Container-->
			<!--begin::Card-->
			<div class="card card-custom w-100">
				<div class="card-header">
					<div class="card-title">
						<h3 class="card-label">Booking List </h3>
					</div>
					<div class="card-toolbar">
						 
					</div>
				</div>
				<div class="card-body">
					<!--begin: Search Form-->
					<!--begin::Search Form-->
					<div class="mb-7">
						<!--div class="row align-items-center">
							<div class="col-lg-9 col-xl-8">
								<div class="row align-items-center">
									<div class="col-md-4 my-2 my-md-0">
										<div class="input-icon">
											<input type="text" class="form-control" placeholder="Search..." id="kt_datatable_search_query" />
											<span>
												<i class="flaticon2-search-1 text-muted"></i>
											</span>
										</div>
									</div>
									<div class="col-md-4 my-2 my-md-0">
										<div class="d-flex align-items-center">
											<label class="mr-3 mb-0 d-none d-md-block">Start Date:</label>
												<input type="date" class="form-control" placeholder="Search..." id="start_date" />
										</div>
									</div>
									<div class="col-md-4 my-2 my-md-0">
										<div class="d-flex align-items-center">
											<label class="mr-3 mb-0 d-none d-md-block">End Date:</label>
											<input type="date" class="form-control" placeholder="Search..." id="end_date" />
										</div>
									</div>
									<div class="col-md-4 my-2 my-md-0">
										<div class="d-flex align-items-center">
											<label class="mr-3 mb-0 d-none d-md-block">Status:</label>
											<select class="form-control" id="kt_datatable_search_status">
												<option value="">All</option>
												<option value="1">Processing</option>
												<option value="2">Approved</option>
												<option value="3">Rejected</option>
												<option value="4">Complete</option>
												<option value="5">Canceled</option>
												<option value="6">Failed</option>
												<option value="7">Warning</option>
												<option value="8">Refund Succeeded</option>
												<option value="9">Refund Pending</option>
												<option value="10">Refund Failed</option>
												<option value="11">Refund canceled</option> 
											</select>
										</div>
									</div>
									<div class="col-md-4 my-2 my-md-0">
										<div class="d-flex align-items-center">
											<label class="mr-3 mb-0 d-none d-md-block">Type:</label>
											<select class="form-control" id="kt_datatable_search_type">
												<option value="">All</option>
												<option value="1">Online</option>
												<option value="2">Retail</option>
												<option value="3">Direct</option>
											</select>
										</div>
									</div>
								</div>
							</div>
							<div class="col-lg-3 col-xl-4 mt-5 mt-lg-0">
								<a href="#" class="btn btn-light-primary px-6 font-weight-bold">Search</a>
							</div>
						</div-->
					</div>
					<!--end::Search Form-->
					<!--end: Search Form-->
					<!--begin: Datatable-->
					<div class="datatable datatable-bordered datatable-head-custom" >
				

					</div>
					<table class="table table-bordered   yajra-datatable table-responsive-sm"   style="margin-top: 13px !important">
					<thead>
						<tr>
							 <th>Order Number</th>
							<th>Experience Name</th>
							<th>Members</th>
							<th>Paid/Discount</th>  
							<th>Booking Date</th>  
							<th>Actions</th>  
						</tr>
					</thead>
				</table>
					<!--end: Datatable-->
				</div>
			</div>
			<!--end::Card-->
		<!--end::Container-->
	</div>
	<!--end::Entry-->
</div>
@endsection
{{-- Scripts Section --}}
@section('scripts')
<script type="text/javascript">
     $(document).ready(function(){ 
        $.ajaxSetup({ headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') } });
             var table = $('.yajra-datatable').DataTable({
	        processing: true,
	        serverSide: true,
	        search: {
				input: $('#kt_datatable_search_query'),
				delay: 400,
				key: 'generalSearch'
			},
	        ajax: "{{ route('admin-booking-list') }}",
	         columns: [
	            {data: 'DT_RowIndex', id: 'DT_RowIndex',orderable: false,render:function(data,type,row){	       
	            	var status = {
							0: {'title': 'Processing ', 'class': ' label-light-primary'},
							1: {'title': 'Approved', 'class': ' label-light-danger'},
							2: {'title': 'Rejected', 'class': ' label-light-primary'},
							3: {'title': 'Complete ', 'class': ' label-light-success'},
							4: {'title': 'Canceled', 'class': ' label-light-info'},
							5: {'title': 'Failed', 'class': ' label-light-danger'},
							6: {'title': 'Refund succeeded ', 'class': ' label-light-warning'},
							7: {'title': 'Refund pending', 'class': ' label-light-warning'},
							8: {'title': 'Refund failed', 'class': ' label-light-warning'},
							9: {'title': 'Refund canceled', 'class': ' label-light-warning'},
						};
						return '<span class="text-dark-75 font-weight-bolder d-block font-size-lg">'+row.order_number+'</span><span class="label font-weight-bold label-lg  font-weight-bold ' + status[row.status].class + ' label-inline">' + status[row.status].title + '</span>' ;
	            	            
	            }},
	            {data: 'experience_name', name: 'experience_name',render:function(data,type,row){	 
	            		return  '<span class="text-muted font-weight-bold d-block" style=" white-space:normal; height:50px">'+row.experience.experience_name +'</span>';
	            	            
	            }},
	            {data: 'adults', name: 'adults',render:function(data,type,row){	 
	            		return ' \<div class="d-flex align-items-center flex-wrap text-font-change">\
											<div class="d-flex align-items-center flex-lg-fill mr-7 my-1">\
												<span class="mr-2">\
													<i class="text-muted fas fa-male"></i>\
												</span>\
												<div class="text-dark-75">\
													<span class="text-dark-50 font-weight-bold"></span>'+row.adults+'</span>\
												</div>\
											</div>\
											<!--end: Item-->\
											<!--begin: Item-->\
											<div class="d-flex align-items-center flex-lg-fill mr-7 my-1">\
												<span class="mr-2">\
													<i class="text-muted fas fa-child"></i>\
												</span>\
												<div class="d-flex flex-column text-dark-75">\
													<span class="font-weight-bold">\
													<span class="text-dark-50 font-weight-bold"></span>'+row.children+'</span>\
												</div>\
											</div>\
											<!--end: Item-->\
											<!--begin: Item-->\
											<div class="d-flex align-items-center flex-lg-fill mr-0 my-1">\
												<span class="mr-2">\
													<i class="text-muted fas fa-baby"></i>\
												</span>\
												<div class="d-flex flex-column text-dark-75">\
													<span class="font-weight-bold ">\
													<span class="text-dark-50 font-weight-bold"></span>'+row.infants+'</span>\
												</div>\
											</div>\
											<!--end: Item-->\
										</div>';
	            	            
	            }}, 

	            {data: 'total_price', name: 'total_price',render:function(data,type,row){	 
	            		return '<span class="text-dark-75 font-weight-bolder d-block font-size-lg">€'+row.total_price+'</span><span class="text-muted font-weight-bold">€'+row.discount+'</span> '; 
	            	            
	            }}, 
	            {data: 'booking_date', name: 'booking_date',render:function(data,type,row){	 
	            		  var fromDate = new Date(row.booking_date );
                         var date = new Date(fromDate).toDateString("yyyy-M-d");
                        
						return "<span class='text-dark-75 font-weight-bolder d-block font-size-lg'>"+date +'</span> <span class="text-muted font-weight-bold d-block font-size-sm">From : '+timeconvert(row.booking_start_time)+'</span><span class="text-muted font-weight-bold d-block font-size-sm">To : '+timeconvert(row.booking_end_time)+'</span>'  ;
	            	            
	            }}, 
	            {
	                data: 'action', 
	                name: 'action', 
	                orderable: false, 
	                searchable: true
	            },
	        ],
	        order: [[1, 'asc']], 
	    });
	 
	  $('#kt_datatable_search_status').on('change', function() {
			table.search($(this).val().toLowerCase(), 'status');
		});
/*
		$('#kt_datatable_search_type').on('change', function() {
			datatable.search($(this).val().toLowerCase(), 'Type');
		});*/

		$('#kt_datatable_search_status, #kt_datatable_search_type').selectpicker();
	    });

function timeconvert(time){

	 var hours = Number(time.match(/^(\d+)/)[1]);
	 var minutes = Number(time.match(/:(\d+)/)[1]);
	 //var AMPM = time.match(/\s(.*)$/)[1];
	if( hours<12) hours = hours+12;
	if( hours==12) hours = hours-12;
	var suffix = hours >= 12 ? "PM":"AM";
	var sHours = hours.toString();
	var sMinutes = minutes.toString();
	if(hours<10) sHours = "0" + sHours;
	if(minutes<10) sMinutes = "0" + sMinutes; 
	//console.log(sHours + ":" + sMinutes +' '+suffix);
    return sHours + ":" + sMinutes +' '+suffix;
}
</script>
@endsection
