{{-- Extends layout --}}
@extends('layout.default')
{{-- Content --}}
@section('content')
<div class="row">
    <div class="col-lg-12">
        <div class="card card-custom gutter-b example example-compact">
            <div class="card-header">
                <h3 class="card-title">Edit Scout</h3>
                <div class="card-toolbar" style="display: none;">
                    <div class="example-tools justify-content-center">
                        <span class="example-toggle" data-toggle="tooltip" title="View code"></span>
                        <span class="example-copy" data-toggle="tooltip" title="Copy code"></span>
                    </div>
                </div>
            </div>
            <!--begin::Form-->
            <form id="edit-scout-form" class="form" method="post" action="{{ route('admin-update-scout') }}">
                @csrf
                <div class="card-body">
                    <div class="form-group row">
                        <div class="col-lg-6 my-error">
                            <label>Full Name:</label>
                            <input type="text" name="user_fname" value="{{ !empty($scoutData) ? $scoutData->user_fname : '' }}" class="form-control" placeholder="Enter first name" />
                        </div>
                        <div class="col-lg-6 my-error">
                            <label>Last Name:</label>
                            <input type="text" name="user_lname" value="{{ !empty($scoutData) ? $scoutData->user_lname : '' }}" class="form-control" placeholder="Enter last name" />
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-lg-6 my-error">
                            <label>Email:</label>
                            <input type="email" name="email" readonly value="{{ !empty($scoutData) ? $scoutData->email : '' }}" style="background-color: grey" class="form-control" placeholder="Enter your email address" />
                        </div>
                        <div class="col-lg-6 my-error">
                            <label>Address:</label>
                            <input type="text" name="user_address" value="{{  !empty($scoutData) ? $scoutData->user_address : ''  }}" class="form-control" placeholder="Enter your address" />
                        </div>
                    </div>
                </div>
                <div class="card-footer">
                    <input type="hidden" name="scout_id" value="{{ !empty($scoutData) ? base64_encode( $scoutData->id) : 0 }}">
                    <div class="row">
                        <div class="col-lg-6">
                            <button type="submit" class="btn btn-primary mr-2">Save</button>
                            <a href="{{ route('admin-scouts') }}" class="btn btn-secondary">Back</a>
                        </div>
                    </div>
                </div>
            </form>
            <!--end::Form-->
        </div>
    </div>
</div>
@endsection
{{-- Scripts Section --}}
@section('scripts')
<script type="text/javascript">
    $(document).ready(function () {
    $('#edit-scout-form').validate({
        submitHandler: function(form) {
          // do other things for a valid form
          form.submit();
        },
        rules: {
            user_fname: {
                required: true
            },
            user_lname: {
                required: true,
            },
            email:{
                required: true,
            },
            user_address:{
                required: true
            },
        },
        messages: {
            user_fname: {
                required: "Please enter your first name",
            },  
            user_lname: {                 
                required : "Please enter your last name",
            },
            user_address: {
                required : "Please enter your address",
            },
            email: {
                required : "Please enter your email address",
            },
        },
        errorElement: 'span',           
        errorPlacement: function (error, element) {
          error.addClass('invalid-feedback');
          element.closest('.my-error').append(error);
        },
        highlight: function (element, errorClass, validClass) {
          $(element).addClass('is-invalid');
        },
        unhighlight: function (element, errorClass, validClass) {
          $(element).removeClass('is-invalid');
        }
    });
    });
</script>
@endsection