	<div class="row">
    <div class="col-lg-12">
    	<meta name="csrf-token" content="{{ csrf_token() }}" />
      
		 <div class="section-heading-home text-center">
                    <h3> Testimonials Management</h3>
                   </div>
		<div class="card card-custom">
			<div class="card-header">
				<div class="card-title justify-content-between w-100 flex-column-sm">
					<div class="left-cart-label d-flex align-items-center">
					<span class="card-icon">
						<i class="flaticon2-supermarket text-primary"></i>
					</span>
					<h3 class="card-label">Testimonials Listing </h3>
				</div>
					<button type="button" class="btn btn-sm float-right green-bg action-add-edit-btn"  list-id=" " act-name="add-testimonials" data-toggle="modal" data-target="#add-testimonial-modal" data-id="0">Add New</button>
				</div>
			</div>
			<div class="card-body">
				
				<!--begin: Datatable-->
				<table class="table table-bordered yajra-datatable table-responsive-sm" style="margin-top: 13px !important">
					<thead>
						<tr>
							<th>Person Image</th>
							<th>Person Name</th>
							<th>Title</th>
							<th>Actions</th>
						</tr>
					</thead>
				</table>
				<!--end: Datatable-->
				<!-- /.modal -->
			    <div class="modal fade add-scout-cls" id="add-testimonial-modal">
			        <div class="modal-dialog">
			            <form id="add-update-testimonial" action="{{route('admin-add-update-testimonial')}}" method="post" enctype="multipart/form-data">
			                <div class="modal-content">
			                    <div class="modal-header">
			                        <h4 class="modal-title scout-title">Add New Testimonial</h4>
			                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
			                        <span aria-hidden="true">&times;</span></button>
			                    </div>
			                    <div class="modal-body">
			                       
			                        @csrf
			                        
			                        <div class="form-group">
			                        <label>Title</label>
			                        <input type="text" name="title" value="" id="title">
			                        <input type="hidden" name="testimonial_id" id="testimonial_id" value="">
			                    	</div>
			                    	<div class="form-group">
			                        <label>Description</label>
			                        <textarea class="form-control" name="description"  id="description"></textarea>             
			                    	</div>
			                    	<div class="form-group">
			                    	 <label>Person Name</label>
			                    	<input type="text" name="person_name" id="person_name" value="">
			                    	</div>
			                    	<div class="form-group">
			                    	<label>Person Profile Image</label>
			                    	<input type="file" name="profile_image" id="profile_image" value="">
			                    	<input type="hidden" id="old_img" value="">
			                    	</div>
			                    </div>
			                    <div class="modal-footer justify-content-between">
			                        <button type="button" class="btn btn-outline-light" data-dismiss="modal">Cancel</button>
			                        <button type="submit" class="btn btn-outline-light" id="add-blog-cat-button">Submit</button>
			                    </div>
			                </div>
			            </form>
			        </div>
			    </div>
			    <!-- /.modal -->
			</div>
		</div>
		<!--end::Card-->
    </div>
</div>