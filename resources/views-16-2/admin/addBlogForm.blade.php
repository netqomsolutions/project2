{{-- Extends layout --}}
@extends('layout.default')
<script src="https://cdn.ckeditor.com/4.12.1/standard/ckeditor.js"></script>
{{-- Content --}}
@section('content')
<div class="row">
    <div class="col-lg-12">
        <div class="card card-custom">
			<!--begin::Form-->
			<form class="form" id="page-add-blog-form" method="post" enctype="multipart/form-data" action="{{ route('admin-add-update-blog') }}">
				<div class="card-body">@csrf
					<input type="hidden" name="blog_id" value="{{ isset($resData) ? $resData->id : 0 }}">
					<div class="form-group row">
						<div class="col-lg-6 my-error">
							<label>Blog Title:</label>
							<input type="text" class="form-control" value="{{ isset($resData) ? $resData->blog_title : '' }}" name="blog_title" id="blog_title" placeholder="Enter Blog title">
						</div>
						<div class="col-lg-6 my-error">
						<label>Blog Feature Image:</label>
						<input type="file" name="feature_image"  value="" id="feature_image" class="form-control" placeholder="Browse Image">
						<input type="hidden" id="old_img" value="{{ (isset($resData) && $resData->feature_image!=null) ? $resData->feature_image : '' }}">
						</div>			
					</div>
					<div class="form-group row">
						<div class="col-lg-6 my-error">
							<label>Blog Category:</label>
						 <select class="form-control" name="blog_category_id" id="blog_category_id">
	                        <option value="">Select category</option>
	                        @forelse ($blogCategories as $cat)
	                            <option value="{{ $cat->id }}" {{ (isset($resData) && $resData->blog_category_id==$cat->id) ? 'selected' : '' }}>{{ $cat->name }}</option>
	                        @empty

	                        @endforelse
	                    </select>
						</div>
					</div>
					<div class="form-group row">
						<div class="col-lg-12 my-error">
							<label>Blog Description :</label>
							
								<textarea class="form-control" name="blog_description"  id="blog_description">{{ isset($resData) ? $resData->blog_description : '' }}</textarea>
							
						</div>
					</div>
                <div class="form-group row">
                	<div class="col-lg-6">
							<button type="submit" class="btn green-bg mr-2">Save</button>
							
						</div>
                </div>
					
				</div>
			</form>
			<!--end::Form-->
		</div>
	</div>
</div>

@endsection
{{-- Scripts Section --}}
@section('scripts')
<script type="text/javascript">

$(document).ready(function () {
	CKEDITOR.replace("blog_description");
	$.validator.addMethod('filesize', function (value, element, param) {
		return this.optional(element) || (element.files[0].size <= param)
	}, 'File size must be less than {0}');
	$('#page-add-blog-form').validate({
		ignore: [],
		submitHandler: function(form) {
		  // do other things for a valid form
		  form.submit();
		},
	    rules: {
		    blog_title: {
		    	required: true
		    },
		    feature_image: {
		    	required: function(){
                    if($("#old_img").val()!=''){
                        return false;
                    }else{
                        return true;
                    }
                },
                extension: "jpg,jpeg,png",
                filesize: 2*1024*1024,
		    },
		    blog_category_id:{
	          	required: true,
	        },
	        blog_description:{
                required: function() {
                 	CKEDITOR.instances.blog_description.updateElement();
                },
                minlength:10
            }
	    },
	    messages: {
	    	blog_title: {
	    		required: "Please enter a blog slug",
	      	},	
        	feature_image: {	        		
          		required: "Please select the image",
                extension: "Please select jpg,jpeg and png image",
                filesize: "File size must be less than 2MB",
        	},
        	blog_category_id: {
          		required : "Please Select Blog Category",
       		},
	        blog_description:{
                required:"Please enter content here",
                minlength:"Please enter 10 characters"
            }
	    },
	    errorElement: 'span',		    
	    errorPlacement: function (error, element) {
	      error.addClass('invalid-feedback');
	      element.closest('.my-error').append(error);
	    },
	    highlight: function (element, errorClass, validClass) {
	      $(element).addClass('is-invalid');
	    },
	    unhighlight: function (element, errorClass, validClass) {
	      $(element).removeClass('is-invalid');
	    }
	});
});
</script>
@endsection