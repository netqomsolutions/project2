<!DOCTYPE html>
<!--
Template Name: Metronic - Bootstrap 4 HTML, React, Angular 10 & VueJS Admin Dashboard Theme
Author: KeenThemes
Website: http://www.keenthemes.com/
Contact: support@keenthemes.com
Follow: www.twitter.com/keenthemes
Dribbble: www.dribbble.com/keenthemes
Like: www.facebook.com/keenthemes
Purchase: https://1.envato.market/EA4JP
Renew Support: https://1.envato.market/EA4JP
License: You must have a valid license purchased only from themeforest(the above link) in order to legally use the theme for your project.
-->
<html lang="en">
    <!--begin::Head-->
    <head><base href="../../../">
        <meta charset="utf-8" />
        <title>Register Page</title>
        <meta name="description" content="Login page example" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <link rel="canonical" href="https://keenthemes.com/metronic" />
        <!--begin::Fonts-->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" />
        <!--end::Fonts-->
        <!--begin::Page Custom Styles(used by this page)-->
        <link rel="stylesheet" href="{{ asset('front_end/css/bootstrap.min.css') }}" type="text/css">
        <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
        <link href="{{ asset('css/pages/login/login-1.css') }}" rel="stylesheet" type="text/css" />
        <!--end::Page Custom Styles-->
        <!--begin::Global Theme Styles(used by all pages)-->
        <link href="{{ asset('plugins/global/plugins.bundle.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('plugins/custom/prismjs/prismjs.bundle.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('css/style.bundle.css') }}" rel="stylesheet" type="text/css" />
         <link href="{{ asset('front_end/css/bootstrap-select.min.css') }}" rel="stylesheet" type="text/css" />
        <!--end::Global Theme Styles-->
        <!--begin::Layout Themes(used by all pages)-->
        <link href="{{ asset('css/themes/layout/header/base/light.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('css/themes/layout/header/menu/light.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('css/themes/layout/brand/dark.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('css/themes/layout/aside/dark.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('front_end/css/common.css') }}" rel="stylesheet" type="text/css" />
        <!--end::Layout Themes-->
        <link rel="icon" href="{{ asset('front_end/images/favicon.png') }}" type="image/png" sizes="16x16">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <link href="https://fonts.googleapis.com/css2?family=Barlow:wght@400;500;600;700&family=Pacifico&display=swap" rel="stylesheet">


    </head>

    <style type="text/css">
        #submit-button {
            background: -moz-linear-gradient(center top , #FFFFFF, #2a8db4) repeat scroll 0 0 transparent;
            float: right;
            display: none;
        }
        #previous-fieldset {
            display: none;
            float: left;
        }
        #next-fieldset {
            float: right;
        }
        label.error,
        .required {
            font-size: 12px;
            color: #ff4486;
        }
        .word-limit-text.counter-ok b,.word-limit-text.counter-ok .counter{
            color:#afc578;
        }
       /* .greenactive:after {
            content: "\f058";
            font-family: "FontAwesome";
            float: right;
            font-size: 28px;
            position: absolute;
            right: 8px;
            top: 3px;
            opacity: .9;
        }
        .nav-active:after {
            content:  "\f138";
            font-family: "FontAwesome";
            float: right;
            font-size: 28px;
            position: absolute;
            right: 8px;
            top: 3px;
            opacity: .9;
        }
        .active:after {
            content:  "\f138";
            font-family: "FontAwesome";
            float: right;
            font-size: 28px;
            position: absolute;
            right: 8px;
            top: 3px;
            opacity: .9;
        }
        .tablinks { position: relative; }*/

       
</style>
    <!--end::Head-->
    <!--begin::Body-->
    <body id="kt_body" class="header-fixed header-mobile-fixed subheader-enabled subheader-fixed aside-enabled aside-fixed aside-minimize-hoverable page-loading">
         <div id="topofthePage"></div>
        <!--begin::Main-->
        <div class="login_sec_tab">
            <div id="kt_header_mobile" class="header-mobile  header-mobile-fixed ">
    <div class="mobile-logo scout_register_logo_white">
        <a href="{{route('/')}}">

            
            
                                                    
            
            <img alt="Laravel" src="{{asset('images/white-logo.png')}}">
        </a>
    </div>
    <div class="d-flex align-items-center scout_register_burger-icon">

                    <button class="btn p-0 burger-icon burger-icon-left" id="kt_aside_mobile_toggle"><span></span></button>
        
       <!--              <button class="btn p-0 burger-icon ml-4" id="kt_header_mobile_toggle"><span></span></button>
         -->

        <button class="btn btn-hover-text-primary p-0 ml-2" id="kt_header_mobile_topbar_toggle">
            
        </button>

    </div>
</div>
            <div class="row">
 
                    <div class="dashboard_sec">
                            <div class="brand-logo">
                                <a href="{{route('/')}}">
                                    <img alt="" src="{{asset('front_end/images/logo.png')}}">
                                </a>
                            </div>
                        <div class="tab"> 
						    <button class="tablinks active" data-tabid="#lfg_scout" data-count="1" id="defaultOpen">Who is LFZ scout?</button>
						    <button class="tablinks"  data-tabid="#know_better" data-count="2">We would like to know you better</button>
						    <button class="tablinks"  data-tabid="#personal_details" data-count="3">Your personal details</button>
						    <button class="tablinks"  data-tabid="#registration_details" data-count="4">Registration details</button>
						    <button class="tablinks"  data-tabid="#submission" data-count="5">Your submission</button>
                        </div>
                    </div>	
                    <div class="right_form_scout">
                        <input type="text" id="autofocused_input" autofocus="" style="width: 0;height: 0;opacity: 0;padding: 0;border: none;display: block;">
                        <div id="kt_header" class="header" kt-hidden-height="65" style="">
                            <div class="container-fluid d-flex align-items-center justify-content-between">
                                    <h1>Register Scout</h1>
                                    
                            </div>
                        </div>
                        <div class="text_inner">
                            <!-- <div class="alert alert-success alert-dismissible">
                              <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                              <strong>Success!</strong>  
                            </div> -->
                             @if (session('success'))
                            <div class="alert alert-success alert-dismissible alluser-page-suc">
                              <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                              <strong>Success!</strong>  {{ session('success') }}
                            </div>
                             @elseif(session('failure'))

                              <div class="alert alert-danger alert-dismissible alluser-page-suc">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            <strong>Error!</strong> {{ session('failure') }}
                          </div>
                             @endif
                                          
                            

                        <form id="scout_info_form" enctype="multipart/form-data" method="post" action="{{ url('register/scout')  }}"> 
                             @csrf
                            <fieldset> 
                        <div id="lfg_scout" class="tabcontent">
							<div class="center-container">
                            <h2>Who is LFZ Scout?</h2>
                            <p>A #LFZ Scout is a person who helps local experience providers to become more visible on the regional tourism map. They take care of reservations, administration and everything else that the local provider is missing.</p>
                            <img class="who-scout-img" src="{{asset('front_end/images/scout-img-1.png')}}" />
                            <h2>Your MISSION as a LFZ scout is:</h2>
							</div>
                            <p><strong>FIND</strong> small local providers who need help to be part of the broad tourism system<br>
                            <strong>HELP</strong> them develop their experience to become BOOKABLE on the LocalsFromZero platform<br>
                            <strong>TAKE CARE</strong> of their experience, meaning taking care of reservations, administration, digital presence, etc.<br>
                            Let’s go over them together in more detail, but first:</p>
                            <div class="form-group word_sec mt-5">
                                <label>Why would you join us? *</label>
                                <textarea onkeyup="wordCount(this)" placeholder="Write Message" id="why_join_us" name="why_join_us" class="inner_textarea"></textarea>
                                <span class="word-limit-text ">
                                    <span data-need="100" class="counter">0</span>/1400 <b>(100 needed)</b>
                                </span>
                            </div>
                            <h2>FIND small local providers</h2>
                            <p>In your first steps you have to get in contact with local tourism providers and introduce them to the concept of the LocalsFromZero marketplace.<br>
                            But before you go out there, we have to make sure we are on the same page.</p>
                            <h2 class="mt-5">What kind of providers are we looking for?</h2>
                            <ul>
                                <li>Non standardized and packaged tourism offer</li>
                                <li>Available only to a small number of people (boutique)</li>
                                <li>Needs help in creating new tourism experiences</li>
                                <li>The provider is passionate about their work and does so not only for the economic aspect</li>
                                <li>The experience is not staged, but shows their lifestyle</li>
                                <li>It is not widely advertised on larger platforms</li>
                                <li>They show values of hospitality and sincerity</li>
                                <li>They are still in the development phase</li>
                                <li>They need help with digital skills</li>
                            </ul>
                            <div class="form-group word_sec">
                                <label class="mt-5">Briefly describe two unique experiences in your region that first came to your mind as the right "#LFZ scouting material". *</label>
                                <textarea onkeyup="wordCount(this)" placeholder="Write Message" id="unique_experiences" name="unique_experiences" class="inner_textarea"></textarea>
                                <span class="word-limit-text"><span data-need="200" class="counter">0</span>/1400 <b>(200 needed)</b></span>
                            </div>
                            <!-- <div class="prev-next-step d-flex align-items-center justify-content-end">
                            <a class="next-step" href="#">Next</a>
                        </div> -->
                        </div>
                    </fieldset>
                    <fieldset>
                        <div id="know_better" class="tabcontent">
						    <div class="form-group word_sec">
                                <label>What are your personal and/or professional areas of interest? *<span>(in general and in relation to #LFZ scouting)</span></label>
                                <textarea onkeyup="wordCount(this)" placeholder="Write Message" id="user_interest" name="user_interest" class="inner_textarea"></textarea>
                                <span class="word-limit-text"><span data-need="100" class="counter">0</span>/1400 <b>(100 needed)</b></span>
                            </div>
                            <div class="form-group new_status value_inner english_level">
	                            <p class="form_group_text">What is your English level? *</p>
                                <div class="d-flex flex-wrap">
	                            <div class="inner_form col-md-6 col-sm-12 p-0">
                                    <input type="radio" id="radio56" value="A1 Beginner" name="user_english_level" />
                                    <span></span>
                                    <label for="radio01-01">A1 Beginner</label>
                                </div>
	                            <div class="inner_form col-md-6 col-sm-12 p-0">
                                    <input type="radio" id="radio57" value="A2 Elementary English" name="user_english_level" />
                                    <span></span>
                                    <label for="radio01-02">A2 Elementary English</label>
                                </div>
	                            <div class="inner_form col-md-6 col-sm-12 p-0">
                                    <input type="radio" id="radio58" value="B1 Intermediate English" name="user_english_level" />
                                    <span></span>
                                    <label for="radio01-03">B1 Intermediate English</label>
                                </div>
	                            <div class="inner_form col-md-6 col-sm-12 p-0">
                                    <input type="radio" id="radio59" value="B2 Upper-Intermediate English" name="user_english_level" />
                                    <span></span>
                                    <label for="radio01-03">B2 Upper-Intermediate English</label>
                                </div>
	                            <div class="inner_form col-md-6 col-sm-12 p-0">
                                    <input type="radio" id="radio60" value="C1 Advanced English" name="user_english_level" />
                                    <span></span>
                                    <label for="radio01-03">C1 Advanced English</label>
                                </div>
	                            <div class="inner_form col-md-6 col-sm-12 p-0">
                                    <input type="radio" id="radio61" value="C2 Proficiency English" name="user_english_level" />
                                    <span></span>
                                    <label for="radio01-03">C2 Proficiency English</label>
                                </div>
	                            <div class="inner_form col-md-6 col-sm-12 p-0">
                                    <input type="radio" id="radio62" value="Other" name="user_english_level" />
                                    <span></span>
                                    <label for="radio01-03">Other:</label>
                                </div>
                                <label for="user_english_level" class="error" style="display:none"></label>
                            </div>
                            </div>
                            <div class="form-group word_sec">
                                <label>What is your primary language? <span>You should be able to read, write, and speak in this language.</span></label>
                                <select name="user_primary_language" id="primary_language" class="form-control form-control-solid h-auto py-6 px-6 rounded-sm font-size-h6 " placeholder="Select Type" >
                                    <option value=" " selected>--Select--</option>

                                    @foreach($languages as $language)
                                    <option value="{{ $language->id }}">{{ $language->name }}</option> 
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group word_sec scout_other_lang">
                                <label>What other languages do you speak fluently? <span>Only add languages you˙d be comfortable scouting in.</span></label>
                                <select  name="user_other_languages[]" id="edit-states1-id" class="form-control form-control-solid h-auto py-6 px-6 rounded-sm font-size-h6"  multiple="multiple" style="display: none;" >
                                    @foreach($languages as $language)
                                    <option value="{{ $language->id }}">{{ $language->name }}</option> 
                                    @endforeach
                                    </select>
                            </div>
                            <p>We are looking for tourism enthusiasts who want to make an impact in their local community.<br>
                            But for this model to be effective you should have certain skills, like:</p>
                            <ol class="custom-counter">
                                <li>English proficiency</li>
                                <li>Knowledge of managing bookings and booking systems</li>
                                <li>Basic photography skills</li>
                                <li>Copywriting</li>
                                <li>Knowledge of important concepts from tourism (sustainability, impactfulness, social inclusion, …)</li>
                            </ol>
                            <p>We do not expect you to be an expert in these fields. The LocalsFromZero team will help you gain a better understanding of the above skills through our LFZ Academy.</p>
                            <div class="form-group new_status ability_sec">
	                                <p class="form_group_text">How would you rate your ability in the fields of *</p>
	                                <table class="table table-responsive">
                                        <thead> 
                                           <tr>
                                           	<th>
                                             	
                                             </th>
                                             <th>
                                             	1 - No skills
                                             </th>  
                                             <th>
                                             	2 - Some skills
                                             </th>
                                             <th>
                                             	3 - Average skills
                                             </th>
                                             <th>
                                             	4 - Above average skills
                                             </th>
                                             <th>
                                             	5 - Professional level skills
                                             </th>                                             
                                           </tr>
                                        </thead>
                                        <tbody>

                                            @php
                                            $AbilityskillsArr =  array(
                                                "Using online resources for personal travel needs",
                                                "Managing social media accounts",
                                                "Using reservation systems",
                                                "Managing reservation systems",
                                                "Using mobile travel applications",
                                                "Developing mobile travel applications",
                                                "Photography",
                                                "Video production",
                                                "360 pictures and video production",
                                                "Personal time/deadliness management",
                                                "Team management",  
                                            );

                                            $radioSkillArr = array(
                                                "personal_travel",
                                                "social_media",
                                                "reservation_systems",
                                                "managing_reservation",
                                                "mobile_travel_app",
                                                "developing_mobile_travel_app",
                                                "photography",
                                                "video_production",
                                                "360_pictures_video_production",
                                                "personal_deadline_management",
                                                "team_management",

                                            );
                                            @endphp

                                            @foreach($AbilityskillsArr as $k => $Abilityskills )

                                           <tr class="field_sec ability_skills">
                                             <td>
                                                 {{ $Abilityskills }}

                                             </td>
                                             @for($i=1; $i<=5; $i++)
                                             <td>
                                                <div class="inner_form">
                                                    <input class="ability_skills_input" data-targetclass=".{{$radioSkillArr[$k] }}" data-target="#{{$radioSkillArr[$k].'_'.$i }}" required="required" value="{{$i}}" type="radio" id="radio01" name="ability_skills[{{$radioSkillArr[$k] }} ]] " />
                                                    <span></span>
                                                </div>
                                             </td>
                                             @endfor 
                                              <label for="ability_skills[{{$radioSkillArr[$k] }} ]]" class="error">This field is required.</label>
                                           </tr> 
                                           @endforeach 
                                        </tbody>	
	                                </table> 
                                </div>
                                <h2>Take Care of the experience</h2>
                                <p>You are responsible for the experience.<br>
                                That means you take care of the bookings.<br>
                                The expected response time for the booking is 2 hours.</p>
                                <div class="form-group new_status value_inner describing">
	                                <p class="form_group_text">What describes you best: *</p>
                                    <span>Only one circle can be checked in each of the groups below.</span>
                                    <div class="d-flex flex-wrap justify-content-between">
                                    <div class="group-one">
                                    <h3>Group 1</h3>
	                                 <div class="inner_form col-md-12 col-sm-12 p-0">
                                        <input type="radio" id="radio63"  value="I am addicted to my email" name="describes_you_best" />
                                        <span></span>
                                        <label for="radio01-01">I am addicted to my email</label>
                                    </div>
	                                 <div class="inner_form col-md-12 col-sm-12 p-0">
                                        <input type="radio" id="radio64" value="I check it every couple of hours" name="describes_you_best" /><span></span>
                                        <label for="radio01-02">I check it every couple of hours</label>
                                    </div>
	                                 <div class="inner_form col-md-12 col-sm-12 p-0">
                                        <input type="radio" id="radio65" value="I go for long periods without checking my email" name="describes_you_best" /><span></span>
                                        <label for="radio01-03">I go for long periods without checking my email</label>
                                    </div>
	                                 <div class="inner_form col-md-12 col-sm-12 p-0">
                                        <input type="radio" id="radio66" value="I am not a fan of e-mail communication" name="describes_you_best" />
                                        <span></span>
                                        <label for="radio01-03">I am not a fan of e-mail communication</label>
                                    </div>
                                     <label for="describes_you_best" class="error" style="display:none;">This field is required.</label>
                                </div>
                                <div class="group-one">
                                    <h3>Group 2</h3>
	                                 <div class="inner_form col-md-12 col-sm-12 p-0">
                                        <input type="radio" id="radio67" value="I am very organized" name="describes_you_best_2" /><span></span>
                                        <label for="radio01-03">I am very organized</label>
                                    </div>
	                                 <div class="inner_form col-md-12 col-sm-12 p-0">
                                        <input type="radio" id="radio68" value="The organization of my work is not an issue for me" name="describes_you_best_2" /><span></span>
                                        <label for="radio01-03">The organization of my work is not an issue for me</label>
                                    </div>
	                                 <div class="inner_form col-md-12 col-sm-12 p-0">
                                        <input type="radio" id="radio69" value="I have no idea what I do most of the time" name="describes_you_best_2" /><span></span>
                                        <label for="radio01-03">I have no idea what I do most of the time</label>
                                    </div>
                                     <label for="describes_you_best_2" class="error" style="display:none;">This field is required.</label>
                                </div>
                                 </div>

                                
                                </div>
                                <!-- <div class="prev-next-step d-flex align-items-center justify-content-between">
                                    <a class="next-step previous" href="#">Previous</a>
                                    <a class="next-step" href="#">Next</a>
                                </div> -->
						</div>
                    </fieldset>
                    <fieldset>
                        <div id="personal_details" class="tabcontent">
						    <div class="form-detail">
                                <div class="form-group fv-plugins-icon-container word_sec col-md-6 col-sm-12 pl-0">
                                	<label>First Name</label>
                                    <input class="form-control form-control-solid px-6 font-size-h6 " value="" type="text" placeholder="" name="user_fname" autocomplete="off">
                                </div>
                                <div class="form-group fv-plugins-icon-container word_sec col-md-6 col-sm-12 pr-0">
                                	<label>Last Name</label>
                                    <input class="form-control form-control-solid px-6 font-size-h6 " value="" type="text" placeholder="" name="user_lname" autocomplete="off">
                                </div>
                                <div class="form-group fv-plugins-icon-container word_sec col-md-6 col-sm-12 pl-0">
                                	<label>Date of birth:</label>
                                    <input class="form-control form-control-solid datepicker px-6 font-size-h6 " value="" type="text" placeholder="" name="dob" autocomplete="off">
                                </div>
                                <div class="form-group word_sec col-md-6 col-sm-12 pr-0">
                                <label>Gender</label>
                                <select name="gender" id="gender" class="form-control form-control-solid px-6 font-size-h6 " placeholder="Select Type">
                                    <option value=" " selected="">Select your gender</option>
                                    <option value="1">Male</option>
                                    <option value="2">Female</option>
                                </select>
                            </div>
                                <div class="form-group fv-plugins-icon-container word_sec col-md-6 col-sm-12 pl-0">
                                	<label>Email</label>
                                    <input class="form-control form-control-solid px-6 font-size-h6 " value="" type="text" placeholder="" name="user_email" autocomplete="off">
                                </div>
                                <div class="form-group fv-plugins-icon-container word_sec col-md-6 col-sm-12 pr-0">
                                    <label>Mobile Number</label>
                                     <div class="inner_form row">
                                    <div class="inner_form country-code-div col-md-2 col-sm-4 p-0">
                                        <select id="CointryCode" name="user_mobile_code" class="selectpicker form-control form-control-solid px-6 font-size-h6 "data-size="5" data-live-search="true" placeholder="Select Type" >
                                        @foreach($getCountryCode as $CountryCode)
                                        <option value="{{ $CountryCode->phonecode }}">+{{ $CountryCode->phonecode.' '.$CountryCode->iso }}</option>
                                        @endforeach
                                    </select>
                                    </div>
                                     <div class="inner_form col-md-10 col-sm-8">
                                    <input class="form-control form-control-solid px-6 font-size-h6 user_mobileMasking" value="" type="text" placeholder="" name="user_mobile" autocomplete="off">
                                </div>
                            </div>
 
                                    
                                     
                                </div>
                                <div class="form-group fv-plugins-icon-container word_sec col-md-12 col-sm-12 p-0">
                                	<label>Place of residence:</label>
                                    <input class="form-control form-control-solid px-6 font-size-h6 " value="" type="text" id="autocomplete" placeholder="Enter the first few letters" name="user_residence">
                                    <input type="hidden" name="latitude"id="latitude" value="">
                                    <input type="hidden" name="longitude" id="longitude" value="">
                                </div>
						    </div>	
						<div class="form-group new_status value_inner current_status">
	                        <p class="form_group_text">Your current status: *</p>
                            <label for="current_status" class="error" style="display: none;">This field is required.</label>
                            <div class="d-flex flex-wrap">
	                        <div class="inner_form col-md-4 col-sm-12 p-0">
                                <input type="radio" id="radio70" value="Student"  name="current_status" /><span></span><label for="radio01-01">Student</label>
                            </div>
	                        <div class="inner_form col-md-4 col-sm-12 p-0">
                                <input type="radio" id="radio71" value="Self employed" name="current_status" /><span></span><label for="radio01-02">Self employed</label>
                            </div>
	                        <div class="inner_form col-md-4 col-sm-12 p-0">
                                <input type="radio"   id="radio72" value="Regularly employed" name="current_status" /><span></span><label for="radio01-03">Regularly employed</label>
                            </div>
	                        <div class="inner_form col-md-4 col-sm-12 p-0">
                                <input  type="radio" id="radio73" value="Unemployed" name="current_status" /><span></span><label for="radio01-03">Unemployed</label>
                            </div>
	                        <div class="inner_form col-md-4 col-sm-12 p-0">
                                <input   type="radio" id="radio74" value="Retired" name="current_status" /><span></span><label for="radio01-03">Retired</label>
                            </div>
	                        <div class="inner_form col-md-4 col-sm-12 p-0">
                                <input type="radio" id="radio75" value="Other" name="current_status" /><span></span><label for="radio01-03">Other:</label>
                            </div>
                        </div>
                        </div>
                        <p><strong>In order to be paid a commission earned per transaction, you must be either an active student (paid via student referrals) or having an appropriate official entity open in your country (full-time or part-time sole proprietor, Ltd, complementary activity, private/public institution, society, ect.). You can edit these details later in your profile.</strong></p>
                        <div class="profile_id">
                            <div class="row">
                            <div class="col-md-4 col-sm-12">
                            <h3>Your profile photo:</h3>
                            <p>It’s important that guests can see your face. No company logos, favorite pets, blank images, etc. We can’t accept photos that don’t show the real you.</p>
                        </div>
                            <div class="form-group new_status profile_update col-md-6 col-sm-12">
                                 
                                    <label for="fileToUpload">
                                        <div class="profile-pic">
                                            <img src="http://conferenceoeh.com/wp-content/uploads/profile-pic-dummy.png" id="profile-pic-inner"> 
                                            <span class="glyphicon glyphicon-camera"></span>
                                            <span><i class="fa fa-file-image-o" aria-hidden="true"></i></span>
                                        </div>
                                    </label>
									<div class="upload-new-one">
									<button class="upload_profile_btn">Browse Image</button>
                                    <input type="File" name="fileToUpload" id="fileToUpload" accept="image/x-png,image/gif,image/jpeg" >
									</div>
                                
                            </div>	
                        </div>
                            <div class="form-group word_sec">
                                <label>Describe yourself: *<span>What makes you uniquely qualified to become a scout? Tell guests why you’re passionate and knowledgeable about. Note: This description goes public.</span></label>
                                <textarea placeholder="Write Message" id="describe_youself" name="about_user" class="inner_textarea"></textarea>
                            </div>
                        </div>	
                       <!--  <div class="prev-next-step d-flex align-items-center justify-content-between">
                                    <a class="next-step previous" href="#">Previous</a>
                                    <a class="next-step" href="#">Next</a>
                                </div> -->
                    </div>
                </fieldset>
                <fieldset>
                    <div id="registration_details" class="tabcontent">
                        <div class="password_sec">
                        	<div class="form-group fv-plugins-icon-container word_sec">
                        		<label>Set your password:</label>
                                <input class="form-control form-control-solid h-auto py-6 px-6 rounded-sm font-size-h6" type="password" placeholder="" id="password" name="password" autocomplete="off">
                            </div>
                            <div class="form-group fv-plugins-icon-container word_sec">
                        		<label>Confirm your password:</label>
                                <input class="form-control form-control-solid h-auto py-6 px-6 rounded-sm font-size-h6" type="password" placeholder="" id="confirmpassword" name="password_confirm" autocomplete="off">
                            </div>
                            <div class="form-group fv-plugins-icon-container word_sec">
                                <div class="checkbox-flex">
                                    <label class="checkbox mb-0">
                                        <input type="checkbox" name="agree">
                                        <span></span>
                                    </label>
                                    <div class="ml-2">I Agree the
                                        <a href="{{route('terms-of-use')}}" target="_blank">terms and conditions</a>.
                                    </div>
                                </div>
                                <div class="fv-plugins-message-container"></div>
                            </div>
                        </div>	
                        <!-- <div class="prev-next-step d-flex align-items-center justify-content-between">
                                    <a class="next-step previous" href="#">Previous</a>
                                    <a class="next-step" href="#">Next</a>
                                </div> -->
                    </div>
                    </fieldset>	
                    <fieldset>
                    <div id="submission" class="tabcontent">
                        <div class="submit_details">
                            <div class="submission_inner">
                                <h2>Why would you join us?</h2>
                                <p id="submission-whyjoinus"></p>
                                <h2>Briefly describe two unique experiences in your region that first came to your mind as the right "#LFZ scouting material". </h2>
                                <p id="submission-scouting-material"></p>
                                <h2>What are your personal and/or professional areas of interest?</h2>
                                <p id="submission-personal-insterst"></p>
                                <h2>What is your English level?</h2>
                                <p id="submission-english-level" >A2 Elementary English</p>
                                <h2>What is your primary language?</h2>
                                <p id="submission-primary-lang"></p>
                                <h2>How would you rate your ability in the fields of</h2>
                                <div class="form-group new_status ability_sec">
                                <table class="table table-responsive">
                                    <thead> 
                                    <tr>
                                    <th></th>
                                    <th>1 - No skills</th>  
                                    <th>2 - Some skills</th>
                                    <th>3 - Average skills</th>
                                    <th>4 - Above average skills</th>
                                    <th>5 - Professional level skills</th>                                             
                                    </tr>
                                    </thead>
                                    <tbody>
                                     
                                     @foreach($AbilityskillsArr as $k => $Abilityskills )

                                           <tr class="field_sec">
                                             <td>
                                                 {{ $Abilityskills }}
                                             </td>
                                             @for($i=1; $i<=5; $i++)
                                             <td>
                                                <div class="inner_form">
                                                    <!-- span-selected -->
                                                    <span id="{{$radioSkillArr[$k].'_'.$i }}" class="{{ $radioSkillArr[$k] }}"></span> 

                                                    
                                             </td>
                                             @endfor 
                                           </tr> 
                                           @endforeach 
                                     
                                    </tbody>    
                                    </table>
                                </div>
                                <h2>What describes you best:</h2>
                                Group 1<p id="submission-best-describe" ></p>
                                Group 2<p id="submission-best-describe-2" ></p>
                                <h2>Personal Details</h2>
                                <div class="personal_details d-flex align-items-start">
                                <div id="submission-profile-img" class="profile-img">
                                    <img  src="http://conferenceoeh.com/wp-content/uploads/profile-pic-dummy.png"></div>
                                <div class="profile-info">
                                <ul>
                                    <li id="submission-profilename" class="profile-name">Jennifer Lopez</li>
                                    <li><span>Date of birth</span> <span id="submission-dob"></span></li>
                                    <li><span>Gender</span><span id="submission-gender"></span></li>
                                    <li><span>Email</span> <span id="submission-email"></span></li>
                                    <li><span>Place of residence</span> <span id="submission-place-residence"></span> </li>
                                    <li><span>Current status</span> <span id="submission-current-status"></span></li>
                                </ul>
                            </div>
                            </div>
                            <p class="description" id="submission-personal-detail">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
                            </div> 
                        </div>	
                    </div>
                    </fieldset>
                      </form>
                </div>
                </div>	
        </div>
        <!--end::Main-->
        <script>
        var HOST_URL = "https://preview.keenthemes.com/metronic/theme/html/tools/preview";
        var SITE_URL = "{{route('/')}}";

    </script>
        <!--begin::Global Config(global config for global JS scripts)-->
        <script>var KTAppSettings = { "breakpoints": { "sm": 576, "md": 768, "lg": 992, "xl": 1200, "xxl": 1400 }, "colors": { "theme": { "base": { "white": "#ffffff", "primary": "#3699FF", "secondary": "#E5EAEE", "success": "#1BC5BD", "info": "#8950FC", "warning": "#FFA800", "danger": "#F64E60", "light": "#E4E6EF", "dark": "#181C32" }, "light": { "white": "#ffffff", "primary": "#E1F0FF", "secondary": "#EBEDF3", "success": "#C9F7F5", "info": "#EEE5FF", "warning": "#FFF4DE", "danger": "#FFE2E5", "light": "#F3F6F9", "dark": "#D6D6E0" }, "inverse": { "white": "#ffffff", "primary": "#ffffff", "secondary": "#3F4254", "success": "#ffffff", "info": "#ffffff", "warning": "#ffffff", "danger": "#ffffff", "light": "#464E5F", "dark": "#ffffff" } }, "gray": { "gray-100": "#F3F6F9", "gray-200": "#EBEDF3", "gray-300": "#E4E6EF", "gray-400": "#D1D3E0", "gray-500": "#B5B5C3", "gray-600": "#7E8299", "gray-700": "#5E6278", "gray-800": "#3F4254", "gray-900": "#181C32" } }, "font-family": "Poppins" };</script>
        <!--end::Global Config-->
        <!--begin::Global Theme Bundle(used by all pages)-->
        <!-- <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script> -->
        <script src="{{ asset('plugins/global/plugins.bundle.js') }}"></script>
        <script src="{{ asset('plugins/custom/prismjs/prismjs.bundle.js') }}"></script>
        <script src="{{ asset('js/scripts.bundle.js') }}"></script>
          <script src="{{ asset('front_end/js/bootstrap-select.min.js') }}"></script>
       <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
        <script src="{{ url('node_modules/@dashboardcode/bsmultiselect/dist/js/BsMultiSelect.min.js') }}"></script>
        <!--end::Global Theme Bundle-->
        <!--begin::Page Scripts(used by this page)-->
        <script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.12.0/jquery.validate.min.js"></script>
        <script src="{{ asset('js/pages/custom/login/login-general.js') }}"></script>
        <script src="{{ asset('js/pages/crud/forms/widgets/input-mask.js') }}"></script> 
        <script src="{{ asset('js/wizardform.js') }}"></script>
        <script type="text/javascript">
        function onlyNumbers(num){
            if ( /[^0-9]+/.test(num.value) ){
                num.value = num.value.replace(/[^0-9]*/g,"")
            }
        }
        </script>
        <script>
function openCity(evt, cityName) {
    return false;  
}
$(function () {
  var date = new Date();
date.setFullYear( date.getFullYear() - 10);
var year= date.getFullYear();
        $(".datepicker").datepicker({
            defaultDate: '01-01-1950',
            changeMonth: true,
            changeYear: true,
            dateFormat: 'dd-mm-yy',
            yearRange: '1950:'+year,
        });
    });
document.getElementById("defaultOpen").click();
</script>

<script>
    $("select[multiple='multiple']").bsMultiSelect({
                  placeholder    : 'Add a language',
             });
      $("#kt_aside_mobile_toggle").click(function(){
    $(".dashboard_sec").toggleClass("left-slide-menu");
  });

   

        $("#scout_info_form").simpleform({
            speed : 500,
            transition : 'slide',
            progressBar : false,
            showProgressText : false,
            validate: true
        });

         

        function validateForm(formID, Obj){

            switch(formID){
                case 'scout_info_form' :
                    Obj.validate({
                        rules: {
                            why_join_us: {
                                required: true,
                                minlength: 100,
                                maxlength: 1400 
                            },
                            unique_experiences: {
                                required: true,
                                 minlength: 200,
                                 maxlength: 1400 
                            },
                            user_interest: {
                                required: true,
                                minlength: 100,
                                maxlength: 1400 
                            },
                            user_english_level: {
                                required: true
                            },
                            user_primary_language: {
                                required: true
                            },
                            "ability_skills[]": {
                                required: true
                            },
                            describes_you_best: {
                                required: true
                            },
                            describes_you_best_2: {
                                required: true
                            },
                            user_fname: {
                                required: true
                            },
                            user_lname: {
                                required: true
                            },
                            dob: {
                                required: true
                            },
                            gender: {
                                required: true
                            },
                            user_email: {
                                required: true,
                                email:true,
                                remote: {
                                    url: SITE_URL+"/check_forEmail",
                                    type: "post",
                                    data: {
                                    "_token": "{{ csrf_token() }}" 
                                    }
                                 }
                            },
                            current_status:{
                                required: true
                            }, 
                            user_mobile: {
                                 required: true,
                                // number: true,
                                 remote: {
                                    url: SITE_URL+"/check_forMobile",
                                    type: "post",
                                    data: {
                                    "_token": "{{ csrf_token() }}" 
                                    }
                                 }
                            },
                            user_residence: {
                                required: true
                            },
                            fileToUpload: {
                                required: true
                            },
                            about_user: {
                                required: true
                            },
                            password : {
                                required: true,
                                minlength : 5
                            },
                            password_confirm : {
                                minlength : 5,
                                equalTo : "#password"
                            }
                        },
                        messages: {
                            why_join_us: {
                                required: "Please enter Why would you join us?"
                            },
                            unique_experiences: {
                                required: "Please enter LFZ scouting material"
                            },
                            user_email: {
                                required: "Please enter your email address.",
                                email: "Please enter a valid email address.",
                                remote: "Email already in use!"
                            },
                            user_mobile: { 
                                required: "Please enter your mobile number.",
                                email: "Please enter a valid mobile number.",
                                remote: "Mobile already in use!"

                            } 
                        }
                    });
                return Obj.valid();
                break;
 
            }
        }
function wordCount(evt){ 
   var words = $.trim($(evt).val());
   var need =  $(evt).parent(".word_sec").find(".counter").attr("data-need");
    $(evt).parent(".word_sec").find(".counter").text(words.length);
    if(words.length >= need){ 
       $(evt).parent(".word_sec").find(".word-limit-text").addClass("counter-ok") 
    }else{
       $(evt).parent(".word_sec").find(".word-limit-text").removeClass("counter-ok") 
    } 
}



function initMap() {
        var options = {
          types: ['geocode'],
        };
        autocomplete = new google.maps.places.Autocomplete(
            document.getElementById('autocomplete'), options);
        //     autocomplete.setComponentRestrictions({
        //     country: ['AL','AT','AD','AM','BY','BA','BE','BG','CH','CY','CZ','DE','DK','EE','ES','FO','FI','FR','GB','GE','GI','GR','HU', 'HR','IS','IE','IT','LI','LT','LU','LV','MC','MK','MT','NO','NL','PL','PT','RO','RU','SE','SI','SK','SM','TR','UA','VA'],
        // });
        autocomplete.setFields(['address_component', 'geometry', 'icon', 'name']);
        autocomplete.addListener("place_changed", fillInAddress);
          
    }
     function fillInAddress() {
        const place = autocomplete.getPlace();
        if (!place.geometry) {
            window.alert("No details available for input: '" + place.name + "'");
            return;
        }
        $("#longitude").val(place.geometry.location.lng());
        $("#latitude").val(place.geometry.location.lat());
    }
   
        $(".user_mobileMasking").inputmask("mask", {
         "mask": "(0) 99 999 999"
         });
    </script>
       <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDny-gFxyDw-Xur7z92RCuhfZuO3wHzAKw&libraries=places&callback=initMap" type="text/javascript"></script>
    </body>
    
    <!--end::Body-->
</html>