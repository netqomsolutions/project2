<html>
    <head>
        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="{{ asset('front_end/css/bootstrap.min.css') }}" type="text/css">
        <link rel="stylesheet" href="{{ asset('front_end/css/custom.css') }}" type="text/css">
        <link rel="stylesheet" href="{{ asset('front_end/css/responsive.css') }}" type="text/css">
        <link rel="stylesheet" type="text/css" href="{{ asset('front_end/css/slick.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ asset('front_end/css/owl.carousel.min.css') }}">
        <link rel="stylesheet" href="{{ asset('front_end/css/aos.css') }}" />
        <link href="https://fonts.googleapis.com/css2?family=Barlow:wght@400;500;600;700&family=Pacifico&display=swap" rel="stylesheet">
        <link rel="icon" href="{{ asset('front_end/images/favicon.png') }}" type="image/png" sizes="16x16">
       <link href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.css">
       <link rel="stylesheet" href="{{ asset('css/common_all.css') }}">
       <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.7.5/css/bootstrap-select.min.css">
       
       
 
        <title>Locals From Zero</title>
        <?php
        if(!empty($header_tag))
        {
            echo $header_tag;
        }
       ?>

        @yield('head')
        
    </head>
    <body>

         <?php
         if(!empty($footer_tag))
        {
      echo $footer_tag;
        }
       ?>
        <div class="loader"><div class="inner_loader"></div></div>
        <script>var HOST_URL = "{{ route('/') }}";</script>
        <!-- navbar start here -->
        @include('front_end.header')
        <!-- navbar start here -->

        <!-- Content start here -->
        @yield('content')
        <!-- Content ends here -->
        <!-- partner start here -->
        <section class="partner aos-item" data-aos="fade-up">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12">
                        <div class="d-flex">
                            <h3>Partners:</h3>
                            <marquee behavior="scroll" direction="left" onmouseover="this.stop();" onmouseout="this.start();">
                                <ul>
                                    @forelse($partners as $part)
                                     <li><img src="{{ asset('pages/partners/'.$part->image) }}"  alt="{{ $part->name}}"/></li>
                                    @empty
                                   
                                    @endforelse
                                </ul>
                            </marquee>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- partner ends here -->
        <!-- footer start here -->  
        <footer>
            @include('front_end.footer')
        </footer>
        <section class="bottom-bar">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <p>© 2020 Locals From Zero</p>
                    </div>
                </div>
            </div>
        </section>
        <!-- footer ends here -->
        <!-- Optional JavaScript; choose one of the two! -->
        <!-- Option 1: jQuery and Bootstrap Bundle (includes Popper) -->
        <script src="https://code.jquery.com/jquery-2.2.0.min.js" type="text/javascript"></script>
        <script src="{{ asset('front_end/js/popper.min.js') }}" type="text/javascript"></script>
         <script src="https://unpkg.com/sweetalert2@7.18.0/dist/sweetalert2.all.js"></script>
        <script src="{{ asset('front_end/js/bootstrap.bundle.min.js') }}"></script>
        <script src="{{ asset('front_end/js/font-awesome-5.0.js') }}" crossorigin="anonymous"></script>
        <script src="{{ asset('front_end/js/slick.js') }}" type="text/javascript" charset="utf-8"></script>
        <!--script src="{{asset('js/pages/crud/file-upload/image-input.js')}}"></script-->
        
        <script src="{{ asset('front_end/js/aos.js') }}"></script>
        <script src="{{ url('node_modules/@dashboardcode/bsmultiselect/dist/js/BsMultiSelect.min.js') }}"></script>
         <script src="{{ asset('js/bootstrap-confirmation.min.js') }}"></script>
        <script src="{{ asset('js/common_all.js') }}"></script>
        <script src="{{asset('js/autocomplete.js')}}" ></script>
        <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDny-gFxyDw-Xur7z92RCuhfZuO3wHzAKw&libraries=places&callback=initMap" type="text/javascript"></script>
        

        <script>
            AOS.init({
              easing: 'ease-in-out-sine'
            });
        </script>
        <script type="text/javascript">
		   $(function () {
              $('[data-toggle="tooltip"]').tooltip()
            });
            $('.slider').slick({
                infinite: true,
                slidesToShow: 3,
                slidesToScroll: 1,
                arrows: true,
            
                autoplaySpeed: 2000,
                responsive: [
                    {
                        breakpoint: 1200,
                        settings: {
                            slidesToShow: 2,
                            slidesToScroll: 1
                        }
                    },
                    {
                        breakpoint: 1008,
                        settings: {
                            slidesToShow: 1,
                            slidesToScroll: 1
                        }
                    },
                    {
                        breakpoint: 800,
                        settings: {
                            slidesToShow: 1,
                            slidesToScroll: 1
                        }
                        // settings: "unslick"
                    }
            
                ]
            });
        </script>
        
        <script>
            $(document).ready(function($) {
                $('marquee').mouseover(function() {
                    $(this).attr('scrollamount',0);
                }).mouseout(function() {
                     $(this).attr('scrollamount',5);
                });
            });
        </script>
        <script>
            $('.video').parents('.video-wrapper').click(function () {
             if($(this).find(".video").get(0).paused){       
              $(this).find(".video").get(0).play();
            $(this).find(".video").addClass('playvid');   
              $(this).find(".playpause").fadeOut();
               }else{      
               $(this).find(".video").get(0).pause();
            $(this).find(".video").removeClass('playvid'); 
             $(this).find(".playpause").fadeIn();
               }
            });
			$("#scout-after-login").click(function(){
			  $(".dropdown-scout").slideToggle();
			});
        </script>
        <script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
        <script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script>
        <script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
        <script src="https://cdn.datatables.net/1.10.21/js/dataTables.bootstrap4.min.js"></script>
        <script src="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.6.1/js/bootstrap4-toggle.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.7.5/js/bootstrap-select.min.js"></script>
        <!-- <script src="{{ asset('js/pages/crud/forms/widgets/select2.js') }}"></script> -->
        @yield('javascript')
    </body>
</html>