<!doctype html>
<html lang="en">
    <head>
        
    </head>
    <body>
        <!-- navbar start here -->
        <nav class="navbar navbar-expand-sm navbar-dark bg-dark custom-nav aos-item"  data-aos="fade-down">
            <a class="navbar-brand" href="#"><img src="images/logo.png" alt="logo" /></a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#main-menu" aria-controls="main-menu" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="main-menu">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item active">
                        <a class="nav-link" href="#">About Us</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">How It Works</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">Tourism From Zero</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">Podcast</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">Blogs </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">Contact Us</a>
                    </li>
                    <li class="nav-item login-btn">
                        <a class="nav-link btn btn-danger pink-bg" href="#"><i class="fas fa-unlock-alt"></i> Login</a>
                    </li>
                    <li class="nav-item register-btn">
                        <a class="nav-link btn btn-success green-bg" href="#"><i class="fas fa-user-plus"></i> Register</a>
                    </li>
                </ul>
            </div>
            <form class="form-inline my-2 my-md-0">
                <button type="button" class="btn btn-success green-bg become-scout"><i class="fas fa-suitcase"></i> Become a Scout</button>
            </form>
        </nav>
        <!-- navbar start here -->
        <!-- home-slider start here -->
        <div class="carousel slide aos-item home-banner" data-aos="fade-up" id="main-carousel" data-ride="carousel">
            <ol class="carousel-indicators">
                <li data-target="#main-carousel" data-slide-to="0" class="active"></li>
                <li data-target="#main-carousel" data-slide-to="1"></li>
                <li data-target="#main-carousel" data-slide-to="2"></li>
            </ol>
            <!-- /.carousel-indicators -->
            <div class="carousel-inner">
                <div class="carousel-item active">
                    <div class="slide-content">
                        <img class="d-block img-fluid" src="images/banner.jpg" alt="">
                        <div class="carousel-caption d-none d-md-block">
                            <h5 class="curly-font">
                            lets go Now</h3>
                            <h3>Locals <span class="green-color">From</span> Zero</h3>
                            <p>Marketplace For Local Experiences</p>
                            <button type="button" class="btn btn-danger pink-bg banner-btn">Start Exploring</button>
                        </div>
                    </div>
                </div>
                <div class="carousel-item">
                    <div class="slide-content">
                        <img class="d-block img-fluid" src="images/banner2.jpg" alt="">
                        <div class="carousel-caption d-none d-md-block">
                            <h5 class="curly-font">
                            lets go Now</h3>
                            <h3>Locals <span class="green-color">From</span> Zero</h3>
                            <p>Marketplace For Local Experiences</p>
                            <button type="button" class="btn btn-danger pink-bg banner-btn">Start Exploring</button>
                        </div>
                    </div>
                </div>
                <div class="carousel-item">
                    <div class="slide-content">
                        <img class="d-block img-fluid" src="images/banner.jpg" alt="">
                        <div class="carousel-caption d-none d-md-block">
                            <h5 class="curly-font">
                            lets go Now</h3>
                            <h3>Locals <span class="green-color">From</span> Zero</h3>
                            <p>Marketplace For Local Experiences</p>
                            <button type="button" class="btn btn-danger pink-bg banner-btn">Start Exploring</button>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.carousel-inner -->
            <!--a href="#main-carousel" class="carousel-control-prev" data-slide="prev">
                <span class="carousel-control-prev-icon"></span>
                <span class="sr-only" aria-hidden="true">Prev</span>
                </a>
                <a href="#main-carousel" class="carousel-control-next" data-slide="next">
                <span class="carousel-control-next-icon"></span>
                <span class="sr-only" aria-hidden="true">Next</span>
                </a-->
        </div>
        <!-- /.carousel -->
        <!-- home-slider ends here -->
        <!-- search form start here -->
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <form class="search-form">
                        <div class="form-group">
                            <input type="text" class="form-control" id="lets-find" placeholder="Let’s Find the Experience">
                        </div>
                        <div class="form-group mx-sm-3">
                            <input type="text" class="form-control" id="location" placeholder="Location">
                        </div>
                        <div class="form-group mx-sm-3">
                            <select class="form-control">
                                <option>All Categories</option>
                                <option> NATURE</option>
                                <option>FOOD & DRINK</option>
                                <option>HISTORY & CULTURE</option>
                                <option>ART & CRAFTS</option>
                                <option>SPORTS</option>
                                <option>FAMILY</option>
                                <option>TOURS</option>
                                <option>FARM</option>
                                <option>OTHER</option>
                                <option>GIFTS</option>
                            </select>
                        </div>
                        <button type="submit" class="btn btn-danger pink-bg">Search</button>
                    </form>
                </div>
            </div>
        </div>
        <!-- search form ends here -->
        <!-- how it work start here -->
        <section class="how-it-work aos-item" data-aos="fade-down">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12">
                        <div class="main-heading">
                            <h5 class="curly-font green-color">Discover & Connect</h5>
                            <h3>How it works</h3>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-12">
                        <div class="step aos-item" data-aos="fade-in">
                            <span class="cloud-span"><img src="images/how-work-icon1.png" /></span>
                            <h4>Find Interesting Experience</h4>
                            <p>LocalsFromZero experiences you are leading the change towards a more sensitive tourism.</p>
                            <span class="count-num">01</span>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-12">
                        <div class="step aos-item" data-aos="fade-in">
                            <span class="cloud-span"><img src="images/how-work-icon2.png" /></span>
                            <h4>Connect to Scouts</h4>
                            <p>We connect you with locals, with their stories, passions, expertise, their clubs, charities, associations and much more.</p>
                            <span class="count-num">02</span>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-12">
                        <div class="step aos-item" data-aos="fade-in">
                            <span class="cloud-span"><img src="images/how-work-icon3.png" /></span>
                            <h4>Enjoy Your Experience</h4>
                            <p>We want to intensify the value of holidays - and help you build a deeper bond with a place and the people who live there.</p>
                            <span class="count-num">03</span>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- how it work start here -->
        <!-- featured exp start here -->
        <section class="featured-exp aos-item" data-aos="fade-down">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12">
                        <div class="main-heading">
                            <h5 class="curly-font green-color">Choose your perfect Holiday</h5>
                            <h3>Featured Experiences</h3>
                        </div>
                    </div>
                    <!-- exp-box start -->
                    <div class="col-lg-4 col-md-4 col-sm-12">
                        <div class="exp-box">
                            <div class="exp-img"><img src="images/exp1.jpg"/></div>
                            <div class="exp-content">
                                <div class="rating-price">
                                    <div class="rating">
                                        <ul>
                                            <li class="filled-star"><i class="fas fa-star"></i></li>
                                            <li class="filled-star"><i class="fas fa-star"></i></li>
                                            <li class="filled-star"><i class="fas fa-star"></i></li>
                                            <li class="filled-star"><i class="fas fa-star-half-alt"></i></li>
                                            <li class="empty-star"><i class="fas fa-star"></i></li>
                                            <li>10 Reviews</li>
                                        </ul>
                                        <a href="#">EXPERIENCE THE KARST UNDERGROUND</a>
                                    </div>
                                    <div class="price">
                                        <span class="price-span">€27</span>
                                        <a href="#" class="wishlist-span"><i class="far fa-heart"></i></a>
                                    </div>
                                </div>
                                <div class="booking-conditions">
                                    <ul>
                                        <li><i class="far fa-user"></i> Max: 8</li>
                                        <li><i class="far fa-clock"></i> 3 Hours</li>
                                        <li><i class="fas fa-map-marker-alt"></i> Koper</li>
                                    </ul>
                                </div>
                                <div class="scout-box green-bg">
                                    <div class="scout-img-name"><img src="images/exp-scout1.jpg" alt="scout-img"/> <a href="#" class="scout-name">Scout Tadej R</a></div>
                                    <div class="scout-rating">
                                        <p>4.36 <i class="fas fa-star"></i> (12)</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- exp-box ends -->
                    <!-- exp-box start -->
                    <div class="col-lg-4 col-md-4 col-sm-12">
                        <div class="exp-box">
                            <div class="exp-img"><img src="images/exp2.jpg"/></div>
                            <div class="exp-content">
                                <div class="rating-price">
                                    <div class="rating">
                                        <ul>
                                            <li class="filled-star"><i class="fas fa-star"></i></li>
                                            <li class="filled-star"><i class="fas fa-star"></i></li>
                                            <li class="filled-star"><i class="fas fa-star"></i></li>
                                            <li class="filled-star"><i class="fas fa-star-half-alt"></i></li>
                                            <li class="empty-star"><i class="fas fa-star"></i></li>
                                            <li>11 Reviews</li>
                                        </ul>
                                        <a href="#">BIKE TOUR BORSEKA</a>
                                    </div>
                                    <div class="price">
                                        <span class="price-span">€225</span>
                                        <a href="#" class="wishlist-span"><i class="far fa-heart"></i></a>
                                    </div>
                                </div>
                                <div class="booking-conditions">
                                    <ul>
                                        <li><i class="far fa-user"></i> Max: 8</li>
                                        <li><i class="far fa-clock"></i> 3 Hours</li>
                                        <li><i class="fas fa-map-marker-alt"></i> Koper</li>
                                    </ul>
                                </div>
                                <div class="scout-box green-bg">
                                    <div class="scout-img-name"><img src="images/exp-scout2.jpg" alt="scout-img"/> <a href="#" class="scout-name">Scout Luka J</a></div>
                                    <div class="scout-rating">
                                        <p>4.36 <i class="fas fa-star"></i> (12)</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- exp-box ends -->
                    <!-- exp-box start -->
                    <div class="col-lg-4 col-md-4 col-sm-12">
                        <div class="exp-box">
                            <div class="exp-img"><img src="images/exp3.jpg"/></div>
                            <div class="exp-content">
                                <div class="rating-price">
                                    <div class="rating">
                                        <ul>
                                            <li class="filled-star"><i class="fas fa-star"></i></li>
                                            <li class="filled-star"><i class="fas fa-star"></i></li>
                                            <li class="filled-star"><i class="fas fa-star"></i></li>
                                            <li class="filled-star"><i class="fas fa-star-half-alt"></i></li>
                                            <li class="empty-star"><i class="fas fa-star"></i></li>
                                            <li>18 Reviews</li>
                                        </ul>
                                        <a href="#">KARST LIQUEURS AND LOCAL SPECIALTIES TASTING</a>
                                    </div>
                                    <div class="price">
                                        <span class="price-span">€36</span>
                                        <a href="#" class="wishlist-span"><i class="far fa-heart"></i></a>
                                    </div>
                                </div>
                                <div class="booking-conditions">
                                    <ul>
                                        <li><i class="far fa-user"></i> Max: 8</li>
                                        <li><i class="far fa-clock"></i> 3 Hours</li>
                                        <li><i class="fas fa-map-marker-alt"></i> Koper</li>
                                    </ul>
                                </div>
                                <div class="scout-box green-bg">
                                    <div class="scout-img-name"><img src="images/exp-scout1.jpg" alt="scout-img"/> <a href="#" class="scout-name">Scout Tadej R</a></div>
                                    <div class="scout-rating">
                                        <p>4.36 <i class="fas fa-star"></i> (12)</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- exp-box ends -->
                    <!-- exp-box start -->
                    <div class="col-lg-4 col-md-4 col-sm-12">
                        <div class="exp-box">
                            <div class="exp-img"><img src="images/exp4.jpg"/></div>
                            <div class="exp-content">
                                <div class="rating-price">
                                    <div class="rating">
                                        <ul>
                                            <li class="filled-star"><i class="fas fa-star"></i></li>
                                            <li class="filled-star"><i class="fas fa-star"></i></li>
                                            <li class="filled-star"><i class="fas fa-star"></i></li>
                                            <li class="filled-star"><i class="fas fa-star-half-alt"></i></li>
                                            <li class="empty-star"><i class="fas fa-star"></i></li>
                                            <li>20 Reviews</li>
                                        </ul>
                                        <a href="#">OLDTIMER TOUR | Citröen Dyane Tour</a>
                                    </div>
                                    <div class="price">
                                        <span class="price-span">€80</span>
                                        <a href="#" class="wishlist-span"><i class="far fa-heart"></i></a>
                                    </div>
                                </div>
                                <div class="booking-conditions">
                                    <ul>
                                        <li><i class="far fa-user"></i> Max: 8</li>
                                        <li><i class="far fa-clock"></i> 3 Hours</li>
                                        <li><i class="fas fa-map-marker-alt"></i> Koper</li>
                                    </ul>
                                </div>
                                <div class="scout-box green-bg">
                                    <div class="scout-img-name"><img src="images/exp-scout3.jpg" alt="scout-img"/> <a href="#" class="scout-name">Scout Jaka G</a></div>
                                    <div class="scout-rating">
                                        <p>4.65 <i class="fas fa-star"></i> (29)</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- exp-box ends -->
                    <!-- exp-box start -->
                    <div class="col-lg-4 col-md-4 col-sm-12">
                        <div class="exp-box">
                            <div class="exp-img"><img src="images/exp5.jpg"/></div>
                            <div class="exp-content">
                                <div class="rating-price">
                                    <div class="rating">
                                        <ul>
                                            <li class="filled-star"><i class="fas fa-star"></i></li>
                                            <li class="filled-star"><i class="fas fa-star"></i></li>
                                            <li class="filled-star"><i class="fas fa-star"></i></li>
                                            <li class="filled-star"><i class="fas fa-star-half-alt"></i></li>
                                            <li class="empty-star"><i class="fas fa-star"></i></li>
                                            <li>03 Reviews</li>
                                        </ul>
                                        <a href="#">BOUTIQUE WORKSHOP - 
                                        TRADITIONAL SHOEMAKING</a>
                                    </div>
                                    <div class="price">
                                        <span class="price-span">€125</span>
                                        <a href="#" class="wishlist-span"><i class="far fa-heart"></i></a>
                                    </div>
                                </div>
                                <div class="booking-conditions">
                                    <ul>
                                        <li><i class="far fa-user"></i> Max: 8</li>
                                        <li><i class="far fa-clock"></i> 3 Hours</li>
                                        <li><i class="fas fa-map-marker-alt"></i> Koper</li>
                                    </ul>
                                </div>
                                <div class="scout-box green-bg">
                                    <div class="scout-img-name"><img src="images/exp-scout4.jpg" alt="scout-img"/> <a href="#" class="scout-name">Scout Petra T</a></div>
                                    <div class="scout-rating">
                                        <p>3.0 <i class="fas fa-star"></i> (05)</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- exp-box ends -->
                    <!-- exp-box start -->
                    <div class="col-lg-4 col-md-4 col-sm-12">
                        <div class="exp-box">
                            <div class="exp-img"><img src="images/exp5.jpg"/></div>
                            <div class="exp-content">
                                <div class="rating-price">
                                    <div class="rating">
                                        <ul>
                                            <li class="filled-star"><i class="fas fa-star"></i></li>
                                            <li class="filled-star"><i class="fas fa-star"></i></li>
                                            <li class="filled-star"><i class="fas fa-star"></i></li>
                                            <li class="filled-star"><i class="fas fa-star-half-alt"></i></li>
                                            <li class="empty-star"><i class="fas fa-star"></i></li>
                                            <li>24 Reviews</li>
                                        </ul>
                                        <a href="#">DISCOVER COSMETICS FROM SNAIL SALIVA</a>
                                    </div>
                                    <div class="price">
                                        <span class="price-span">€4</span>
                                        <a href="#" class="wishlist-span"><i class="far fa-heart"></i></a>
                                    </div>
                                </div>
                                <div class="booking-conditions">
                                    <ul>
                                        <li><i class="far fa-user"></i> Max: 8</li>
                                        <li><i class="far fa-clock"></i> 3 Hours</li>
                                        <li><i class="fas fa-map-marker-alt"></i> Koper</li>
                                    </ul>
                                </div>
                                <div class="scout-box green-bg">
                                    <div class="scout-img-name"><img src="images/exp-scout1.jpg" alt="scout-img"/> <a href="#" class="scout-name">Scout Uroš J</a></div>
                                    <div class="scout-rating">
                                        <p>5 <i class="fas fa-star"></i> (39)</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- exp-box ends -->
                </div>
            </div>
        </section>
        <!-- featured exp ends here -->
        <!-- video start here -->   
        <section class="video-sec aos-item" data-aos="fade-in">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12">
                        <div class="main-heading">
                            <h5 class="curly-font green-color">Come & Explore</h5>
                            <h3>Locals From Zero Initiative</h3>
                        </div>
                    </div>
                    <div class="col-lg-10 col-md-10 col-sm-12 offset-lg-1 offset-md-1">
                        <p>Inspired by the challenges of the pandemic of 2020 and the problems of overtourism in recent years, the aim of the #TourismFromZero initiative is to provide a forum for the exchange of problems and solutions related to travel, tourism, hospitality, and leisure.</p>
                        <div class="video-wrapper video-thumb">
                            <p>
                                <video class="video" poster="images/video-thumb.jpg" width="100%" height="">
                                    <source src="https://buildingimagination.org/wp-content/uploads/2020/09/Seal-Made-with-Tinkercad-1.mp4" type="video/mp4" />
                                </video>
                            </p>
                            <div class="playpause"></div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- video ends here -->
        <!-- our tesm start here -->
        <section class="team-section aos-item" data-aos="fade-in">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12">
                        <div class="main-heading">
                            <h5 class="curly-font green-color">Our Team</h5>
                            <h3>Coordinators</h3>
                        </div>
                    </div>
                    <div class="col-lg-12 col-md-12 col-12">
                        <div class="slider">
                            <div class="slide team-box">
                                <img src="images/team1.jpg" alt="team">
                                <h4>Dejan Križaj</h4>
                                <p>Teaching, researching, and consuming tourism innovation at the University of Primorska.</p>
                                <button type="button" class="btn btn-success green-bg">Read More</button>
                            </div>
                            <div class="slide team-box">
                                <img src="images/team2.jpg" alt="team">
                                <h4>Scout Darja P <span class="team-rating"><i class="fas fa-star" aria-hidden="true"></i> ( 3.8 )</span></h4>
                                <p>Travelling is my passion, marketing and tourism is my profession.</p>
                                <button type="button" class="btn btn-success green-bg">Read More</button>
                            </div>
                            <div class="slide team-box">
                                <img src="images/team3.jpg" alt="team">
                                <h4>Scout Rudi M <span class="team-rating"><i class="fas fa-star" aria-hidden="true"></i> ( 5.0 )</span></h4>
                                <p>An active, positive and studious person, interested in making travel better, sustainable and local!</p>
                                <button type="button" class="btn btn-success green-bg">Read More</button>
                            </div>
                            <div class="slide team-box">
                                <img src="images/team4.jpg" alt="team">
                                <h4>Scout Tadej R <span class="team-rating"><i class="fas fa-star" aria-hidden="true"></i> ( 4.0 )</span></h4>
                                <p>Teaching, researching, and consuming tourism innovation at the University of Primorska.</p>
                                <button type="button" class="btn btn-success green-bg">Read More</button>
                            </div>
                            <div class="slide team-box">
                                <img src="images/team1.jpg" alt="team">
                                <h4>Dejan Križaj</h4>
                                <p>I am an assistant and Ph.D. student at the University of Primorska, Faculty of tourism studies Turistica.</p>
                                <button type="button" class="btn btn-success green-bg">Read More</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- our tesm ends here -->
        <!-- scouts start here -->
        <section class="scouts aos-item" data-aos="fade-up">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12">
                        <div class="main-heading">
                            <h5 class="curly-font green-color">Locals From Zero</h5>
                            <h3>Meet Our Scouts</h3>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-12">
                        <div class="scouts-box">
                            <img src="images/scout-img1.jpg" />
                            <div class="scout-text">
                                <a href="#">Dominika Koritnik Trepel</a>
                                <div class="rating">
                                    <ul>
                                        <li class="filled-star"><i class="fas fa-star" aria-hidden="true"></i></li>
                                        <li class="filled-star"><i class="fas fa-star" aria-hidden="true"></i></li>
                                        <li class="filled-star"><i class="fas fa-star" aria-hidden="true"></i></li>
                                        <li class="filled-star"><i class="fas fa-star-half-alt" aria-hidden="true"></i></li>
                                        <li class="empty-star"><i class="fas fa-star" aria-hidden="true"></i></li>
                                        <li>20 Reviews</li>
                                    </ul>
                                    <p>I am a long-time tourist guide, a university-educated theologian (SMC in Theology).</p>
                                    <a href="#" class="pink-color">View Now</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-12">
                        <div class="scouts-box">
                            <img src="images/scout-img2.jpg" />
                            <div class="scout-text">
                                <a href="#">Scout Katja K</a>
                                <div class="rating">
                                    <ul>
                                        <li class="filled-star"><i class="fas fa-star" aria-hidden="true"></i></li>
                                        <li class="filled-star"><i class="fas fa-star" aria-hidden="true"></i></li>
                                        <li class="filled-star"><i class="fas fa-star" aria-hidden="true"></i></li>
                                        <li class="filled-star"><i class="fas fa-star-half-alt" aria-hidden="true"></i></li>
                                        <li class="empty-star"><i class="fas fa-star" aria-hidden="true"></i></li>
                                        <li>10 Reviews</li>
                                    </ul>
                                    <p>You will learn how to make traditional Slovenian bread or pastry called ''potica'' with the help.</p>
                                    <a href="#" class="pink-color">View Now</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-12">
                        <div class="scouts-box">
                            <img src="images/scout-img3.jpg" />
                            <div class="scout-text">
                                <a href="#">Scout Luka J</a>
                                <div class="rating">
                                    <ul>
                                        <li class="filled-star"><i class="fas fa-star" aria-hidden="true"></i></li>
                                        <li class="filled-star"><i class="fas fa-star" aria-hidden="true"></i></li>
                                        <li class="filled-star"><i class="fas fa-star" aria-hidden="true"></i></li>
                                        <li class="filled-star"><i class="fas fa-star-half-alt" aria-hidden="true"></i></li>
                                        <li class="empty-star"><i class="fas fa-star" aria-hidden="true"></i></li>
                                        <li>10 Reviews</li>
                                    </ul>
                                    <p>Hey! My name is Luka and I will be your connection to the best experiences in Savinja valley.</p>
                                    <a href="#" class="pink-color">View Now</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-12">
                        <div class="scouts-box">
                            <img src="images/scout-img4.jpg" />
                            <div class="scout-text">
                                <a href="#">Scout Karin M</a>
                                <div class="rating">
                                    <ul>
                                        <li class="filled-star"><i class="fas fa-star" aria-hidden="true"></i></li>
                                        <li class="filled-star"><i class="fas fa-star" aria-hidden="true"></i></li>
                                        <li class="filled-star"><i class="fas fa-star" aria-hidden="true"></i></li>
                                        <li class="filled-star"><i class="fas fa-star-half-alt" aria-hidden="true"></i></li>
                                        <li class="empty-star"><i class="fas fa-star" aria-hidden="true"></i></li>
                                        <li>10 Reviews</li>
                                    </ul>
                                    <p>I would describe myself as a dynamic gallivanter who is always in the drive for new challenges,</p>
                                    <a href="#" class="pink-color">View Now</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-12">
                        <div class="scouts-box">
                            <img src="images/scout-img5.jpg" />
                            <div class="scout-text">
                                <a href="#">Scout Zala R</a>
                                <div class="rating">
                                    <ul>
                                        <li class="filled-star"><i class="fas fa-star" aria-hidden="true"></i></li>
                                        <li class="filled-star"><i class="fas fa-star" aria-hidden="true"></i></li>
                                        <li class="filled-star"><i class="fas fa-star" aria-hidden="true"></i></li>
                                        <li class="filled-star"><i class="fas fa-star-half-alt" aria-hidden="true"></i></li>
                                        <li class="empty-star"><i class="fas fa-star" aria-hidden="true"></i></li>
                                        <li>20 Reviews</li>
                                    </ul>
                                    <p>Hi everyone, my name is Zala and I am a first year student at Turistica.</p>
                                    <a href="#" class="pink-color">View Now</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-12">
                        <div class="scouts-box">
                            <img src="images/scout-img6.jpg" />
                            <div class="scout-text">
                                <a href="#">Scout Urban Š</a>
                                <div class="rating">
                                    <ul>
                                        <li class="filled-star"><i class="fas fa-star" aria-hidden="true"></i></li>
                                        <li class="filled-star"><i class="fas fa-star" aria-hidden="true"></i></li>
                                        <li class="filled-star"><i class="fas fa-star" aria-hidden="true"></i></li>
                                        <li class="filled-star"><i class="fas fa-star-half-alt" aria-hidden="true"></i></li>
                                        <li class="empty-star"><i class="fas fa-star" aria-hidden="true"></i></li>
                                        <li>10 Reviews</li>
                                    </ul>
                                    <p>I have been working in tourism and hospitality for last 7 years. I am a certified local tour guide.</p>
                                    <a href="#" class="pink-color">View Now</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- scouts ends here -->
        <!-- TESTIMONIALS -->
        <section class="testimonials aos-item" data-aos="fade-in">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12">
                        <div class="main-heading">
                            <h5 class="curly-font green-color">What Travellers Say</h5>
                            <h3>Recent Reviews</h3>
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <div id="customers-testimonials" class="owl-carousel">
                            <!--TESTIMONIAL 1 -->
                            <div class="item">
                                <div class="shadow-effect">
                                    <div class="item-details">
                                        <h5>BIKE TOUR BORSEKA</h5>
                                        <p>Integer ut ligula quis lectus fringilla elementum porttitor sed est. Duis fringilla efficitur ligula sed lobortis. Sed tempus faucibus mi,Integer ut ligula quis lectus fringilla elementum porttitor sed est. Duis fringilla efficitur ligula sed lobortis. Sed tempus faucibus mi</p>
                                    </div>
                                    <img class="img-responsive" src="images/testimonial-thumb.jpg" alt="">
                                    <h4 class="testimonial-name">John Doe <span class="designation">College Professor</span></h4>
                                </div>
                            </div>
                            <!--END OF TESTIMONIAL 1 -->
                            <!--TESTIMONIAL 1 -->
                            <div class="item">
                                <div class="shadow-effect">
                                    <div class="item-details">
                                        <h5>OLDTIMER TOUR | Citröen Dyane Tour</h5>
                                        <p>Integer ut ligula quis lectus fringilla elementum porttitor sed est. Duis fringilla efficitur ligula sed lobortis. Sed tempus faucibus mi,Integer ut ligula quis lectus fringilla elementum porttitor sed est. Duis fringilla efficitur ligula sed lobortis. Sed tempus faucibus mi</p>
                                    </div>
                                    <img class="img-responsive" src="images/testimonial-thumb.jpg" alt="">
                                    <h4 class="testimonial-name">John Doe <span class="designation">College Professor</span></h4>
                                </div>
                            </div>
                            <!--END OF TESTIMONIAL 1 -->
                            <!--TESTIMONIAL 1 -->
                            <div class="item">
                                <div class="shadow-effect">
                                    <div class="item-details">
                                        <h5>OLDTIMER TOUR | Citröen Dyane Tour</h5>
                                        <p>Integer ut ligula quis lectus fringilla elementum porttitor sed est. Duis fringilla efficitur ligula sed lobortis. Sed tempus faucibus mi,Integer ut ligula quis lectus fringilla elementum porttitor sed est. Duis fringilla efficitur ligula sed lobortis. Sed tempus faucibus mi</p>
                                    </div>
                                    <img class="img-responsive" src="images/testimonial-thumb.jpg" alt="">
                                    <h4 class="testimonial-name">John Doe <span class="designation">College Professor</span></h4>
                                </div>
                            </div>
                            <!--END OF TESTIMONIAL 1 -->
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- END OF TESTIMONIALS -->
        <!-- partner start here -->
        <section class="partner aos-item" data-aos="fade-up">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12">
                        <div class="d-flex">
                            <h3>Partners:</h3>
                            <marquee behavior="scroll" direction="left" onmouseover="this.stop();" onmouseout="this.start();">
                                <ul>
                                    <li><img src="images/partner-logo1.png"  alt="partner-name"/></li>
                                    <li><img src="images/partner-logo2.png"  alt="partner-name"/></li>
                                    <li><img src="images/partner-logo3.png"  alt="partner-name"/></li>
                                    <li><img src="images/partner-logo2.png"  alt="partner-name"/></li>
                                    <li><img src="images/partner-logo3.png"  alt="partner-name"/></li>
                                </ul>
                            </marquee>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- partner ends here -->
        <!-- footer start here -->  
        <footer class="aos-item" data-aos="fade-down">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-12 col-12">
                        <a href="#" class="footer logo"><img src="images/logo.png" alt="locals from zero" /></a>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-12 col-12">
                        <ul class="footer-social">
                            <li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
                            <li><a href="#"><i class="fab fa-instagram"></i></a></li>
                            <li><a href="#"><i class="fab fa-pinterest-p"></i></a></li>
                            <li><a href="#"><i class="fab fa-linkedin-in"></i></a></li>
                        </ul>
                    </div>
                    <hr>
                    <div class="col-lg-4 col-md-4 col-sm-12 col-12">
                        <h3>About Us</h3>
                        <p>We are a group of young, committed people with one goal: to bring tourism back to its roots. Encouraged by #TourismFromZero global initiative, we strive to highlight our local heroes. Now is the time for you to discover hidden experiences that were not part of the global tourism market.</p>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-12 col-12">
                        <h3>Helpful Links</h3>
                        <ul class="quick-links">
                            <li><a href="#">About</a></li>
                            <li><a href="#">Privacy policy</a></li>
                            <li><a href="#">Tourism From Zero</a></li>
                            <li><a href="#">Gift Card</a></li>
                            <li><a href="#">Podcast</a></li>
                            <li><a href="#">Our Partners</a></li>
                            <li><a href="#">Contact Us</a></li>
                            <li><a href="#">Terms of Use</a></li>
                        </ul>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-12 col-12">
                        <h3>Contact Us</h3>
                        <p><span class="contact-span"><i class="fas fa-map-marker-alt"></i></span> Locals From Zero Turjaška 57, Ročinj, 5215</p>
                        <p><span class="contact-span"><i class="fas fa-phone-alt"></i></span> Phone: <a href="tel:1 800 123 4567">1 800 123 4567</a></p>
                        <p><span class="contact-span"><i class="fas fa-envelope"></i></span> Email: <a href="mailto:locals@zero.com">locals@zero.com</a></p>
                    </div>
                </div>
            </div>
        </footer>
        <section class="bottom-bar">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <p>© 2020 by Tourism From Zero</p>
                    </div>
                </div>
            </div>
        </section>
        <!-- footer ends here -->
        <!-- Optional JavaScript; choose one of the two! -->
        <!-- video modal start here  -->    
        <!-- Modal -->
        <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-body">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                        <iframe width="100%" height="500" src="https://www.youtube.com/embed/1bwovDOke_4" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                    </div>
                </div>
            </div>
        </div>
        <!-- video modal start here  -->
        <!-- Option 1: jQuery and Bootstrap Bundle (includes Popper) -->
        <script src="https://code.jquery.com/jquery-2.2.0.min.js" type="text/javascript"></script>
        <script src="js/bootstrap.bundle.min.js"></script>
        <script src="https://kit.fontawesome.com/16499c553d.js" crossorigin="anonymous"></script>
        <script src="js/slick.js" type="text/javascript" charset="utf-8"></script>
        <script src="js/owl.carousel.min.js" type="text/javascript" charset="utf-8"></script>
        <script src="js/aos.js"></script>
        <script>
            AOS.init({
              easing: 'ease-in-out-sine'
            });
        </script>
        <script type="text/javascript">
            $('.slider').slick({
                infinite: true,
                slidesToShow: 3,
                slidesToScroll: 1,
                arrows: true,
            
                autoplaySpeed: 2000,
                responsive: [
                    {
                        breakpoint: 1200,
                        settings: {
                            slidesToShow: 2,
                            slidesToScroll: 1
                        }
                    },
                    {
                        breakpoint: 1008,
                        settings: {
                            slidesToShow: 1,
                            slidesToScroll: 1
                        }
                    },
                    {
                        breakpoint: 800,
                        settings: {
                            slidesToShow: 1,
                            slidesToScroll: 1
                        }
                        // settings: "unslick"
                    }
            
                ]
            });
        </script>
        <script>
            jQuery(document).ready(function($) {
            "use strict";
            $('#customers-testimonials').owlCarousel( {
                    loop: true,
                    center: true,
                    items: 3,
                    margin: 30,
                    autoplay: true,
                    dots:true,
                nav:true,
                    autoplayTimeout: 8500,
                    smartSpeed: 450,
                navText: ['<i class="fa fa-angle-left"></i>','<i class="fa fa-angle-right"></i>'],
                    responsive: {
                        0: {
                            items: 1
                        },
                        768: {
                            items: 2
                        },
                        1170: {
                            items: 3
                        }
                    }
                });
            });
        </script>
        <script>
            JQuery(function() {
                JQuery('marquee').mouseover(function() {
                    JQuery(this).attr('scrollamount',0);
                }).mouseout(function() {
                     JQuery(this).attr('scrollamount',5);
                });
            });
        </script>
        <script>
            jQuery('.video').parents('.video-wrapper').click(function () {
             if(jQuery(this).find(".video").get(0).paused){       
              jQuery(this).find(".video").get(0).play();
            jQuery(this).find(".video").addClass('playvid');   
              jQuery(this).find(".playpause").fadeOut();
               }else{      
               jQuery(this).find(".video").get(0).pause();
            jQuery(this).find(".video").removeClass('playvid'); 
             jQuery(this).find(".playpause").fadeIn();
               }
            });
        </script>
        <!-- Option 2: jQuery, Popper.js, and Bootstrap JS
            <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
            <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
            <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.min.js" integrity="sha384-w1Q4orYjBQndcko6MimVbzY0tgp4pWB4lZ7lr30WKz0vr/aWKhXdBNmNb5D92v7s" crossorigin="anonymous"></script>
            -->
    </body>
</html>