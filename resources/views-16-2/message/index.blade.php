{{-- Extends layout --}}
@extends('layout.default')
{{-- Content --}}
@section('content') 
<!--begin::Content-->
					 	<div class="content d-flex flex-column flex-column-fluid main-area-message" id="kt_content" data-total-messages="{{ $allMessageCount }} ">
						<!--begin::Entry-->
						<div class="d-flex flex-column-fluid">
							<!--begin::Container-->
								<!--begin::Chat-->
								<div class="d-flex flex-row w-100 mb-10">
									<!--begin::Aside-->
									<div class="flex-row-auto offcanvas-mobile w-350px w-xl-400px" id="kt_chat_aside">
										<!--begin::Card-->
										<div class="card card-custom">
											<!--begin::Body-->
											<div class="card-body">
												<!--begin:Search-->
												<div class="input-group input-group-solid">
													<div class="input-group-prepend">
														<span class="input-group-text">
															<span class="svg-icon svg-icon-lg">
																<!--begin::Svg Icon | path:assets/media/svg/icons/General/Search.svg-->
																<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
																	<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
																		<rect x="0" y="0" width="24" height="24" />
																		<path d="M14.2928932,16.7071068 C13.9023689,16.3165825 13.9023689,15.6834175 14.2928932,15.2928932 C14.6834175,14.9023689 15.3165825,14.9023689 15.7071068,15.2928932 L19.7071068,19.2928932 C20.0976311,19.6834175 20.0976311,20.3165825 19.7071068,20.7071068 C19.3165825,21.0976311 18.6834175,21.0976311 18.2928932,20.7071068 L14.2928932,16.7071068 Z" fill="#000000" fill-rule="nonzero" opacity="0.3" />
																		<path d="M11,16 C13.7614237,16 16,13.7614237 16,11 C16,8.23857625 13.7614237,6 11,6 C8.23857625,6 6,8.23857625 6,11 C6,13.7614237 8.23857625,16 11,16 Z M11,18 C7.13400675,18 4,14.8659932 4,11 C4,7.13400675 7.13400675,4 11,4 C14.8659932,4 18,7.13400675 18,11 C18,14.8659932 14.8659932,18 11,18 Z" fill="#000000" fill-rule="nonzero" />
																	</g>
																</svg>
																<!--end::Svg Icon-->
															</span>
														</span>
													</div>
													<input id="usersearch" type="text" class="form-control py-4 h-auto" placeholder="Search user" />
												</div>
												<!--end:Search-->
												<!--begin:Users-->
												<div class="mt-7 scroll scroll-pull userListing">


													<!--begin:User-->
													@forelse($userList as $k => $userLists)
													 
													@foreach($userLists as $userData)
													<div  class="d-flex align-items-center justify-content-between mb-5 user-listing-main">
														<div class="d-flex align-items-center userListing-inner">
															<div class="symbol symbol-circle symbol-50 mr-3">
																@if(!empty($userData->user_image))
																<img alt="Pic" src="{{ asset($userData->user_image_path.'/'.$userData->user_image) }}" />
																@else
																<img alt="Pic" src="https://i.stack.imgur.com/l60Hf.png" />
																@endif 
															</div>
															<div class="d-flex flex-column userListing-inn">
																<a href="javascript:void(0);" data-scoutid="{{ $userData->id }}" data-expid= "1" data-trevid="{{ $user->id }}" data-io="{{ $k }}"  data-scoutname="{{ $userData->user_fname }} {{ $userData->user_lname }}"   class="text-hover-primary selectToChat font-weight-bold font-size-lg">{{ $userData->user_fname .' ' .$userData->user_lname }} </a>
																
																<span class=" font-weight-bold font-size-sm "> </span>
															</div>
														</div>

														@if(isset($countMessages) && $countMessages > 0)
														<span class="message-count mes-count-{{ $k }}">{{ $countMessages }}
														</span>
														@endif
													</div>
													 
													@endforeach
													@empty
													@endforelse


													  
												</div>
												<!--end:Users-->
											</div>
											<!--end::Body-->
										</div>
										<!--end::Card-->
									</div>
									<!--end::Aside-->
									<!--begin::Content-->
									<div class="flex-row-fluid ml-lg-8" id="kt_chat_content">
										<!--begin::Card-->
										<div class="card card-custom">
											<!--begin::Header-->
											<div class="card-header align-items-center px-4 py-3">
													<div class="text-left flex-grow-1">
													<!--begin::Aside Mobile Toggle-->
													<button type="button" class="btn btn-clean btn-sm btn-icon btn-icon-md d-lg-none" id="kt_app_chat_toggle">
														<span class="svg-icon svg-icon-lg">
															<!--begin::Svg Icon | path:assets/media/svg/icons/Communication/Adress-book2.svg-->
															<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
																<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
																	<rect x="0" y="0" width="24" height="24" />
																	<path d="M18,2 L20,2 C21.6568542,2 23,3.34314575 23,5 L23,19 C23,20.6568542 21.6568542,22 20,22 L18,22 L18,2 Z" fill="#000000" opacity="0.3" />
																	<path d="M5,2 L17,2 C18.6568542,2 20,3.34314575 20,5 L20,19 C20,20.6568542 18.6568542,22 17,22 L5,22 C4.44771525,22 4,21.5522847 4,21 L4,3 C4,2.44771525 4.44771525,2 5,2 Z M12,11 C13.1045695,11 14,10.1045695 14,9 C14,7.8954305 13.1045695,7 12,7 C10.8954305,7 10,7.8954305 10,9 C10,10.1045695 10.8954305,11 12,11 Z M7.00036205,16.4995035 C6.98863236,16.6619875 7.26484009,17 7.4041679,17 C11.463736,17 14.5228466,17 16.5815,17 C16.9988413,17 17.0053266,16.6221713 16.9988413,16.5 C16.8360465,13.4332455 14.6506758,12 11.9907452,12 C9.36772908,12 7.21569918,13.5165724 7.00036205,16.4995035 Z" fill="#000000" />
																</g>
															</svg>
															<!--end::Svg Icon-->
														</span>
													</button>
													<!--end::Aside Mobile Toggle-->
												</div> 
												<div class="text-center flex-grow-1">
													<div class="font-weight-bold font-size-h5" id="scoutName"></div>
												</div>
												 
											</div>
											<!--end::Header-->
											<!--begin::Body-->
											<div class="card-body">
												<!--begin::Scroll-->
												<div class="scroll scroll-pull main-message-area" data-mobile-height="350">
												<div class="loader-lfz" id="loader-lfz" style="display: none;"><img src="<?php echo URL::to('/');?>/public/images/loader.gif"></div>
													<!--begin::Messages-->
													<div class="messages-custom"> 
															<div class="start-chat-wlcmes">
															  <h4> Start your chat</h4>
															  <p> Click on left side list to get started </p>
															  <div class="image-logo">
															  	<img src="https://image.freepik.com/free-vector/users-with-speech-bubbles-vector_53876-82250.jpg">
															  </div>	
															</div>
													 </div>
												<!--end::Scroll-->
											</div>
											<!--end::Body-->
											<!--begin::Footer-->
											<div class="card-footer align-items-center send-textarea force-hide">
												<!--begin::Compose-->
												<textarea class="form-control border-0 p-0 write_msg" rows="2" placeholder="Type a message" data-date_time = "{{ date('Y-m-d h:i:s A') }}" data-sender_id = "{{ $user->id }}"></textarea>
												<div class="d-flex align-items-center justify-content-between mt-5 main-sendBtn">
													 
													<div class="SendBtn">
														<input type="hidden" id="reciver_id" value="">
														<!-- <input type="hidden" id="experience_id" value=""> -->
														<button id="msg_send_btn" type="button" class="btn btn-primary btn-md text-uppercase font-weight-bold py-2 px-6">Send</button>
													</div>
												</div>
												<!--begin::Compose-->
											</div>
											<!--end::Footer-->
										</div>
										<!--end::Card-->
									</div>
									<!--end::Content-->
								</div>
								<!--end::Chat-->
							</div>
							<!--end::Container-->
						<!--end::Entry-->
					</div>
									<!--  Chat Inbox html End here  -->



								</div>
					<!--end::Content-->
@endsection
{{-- Scripts Section --}}
@section('scripts')
<script src="{{asset('js/pages/custom/chat/chat.js')}}"></script>

@endsection