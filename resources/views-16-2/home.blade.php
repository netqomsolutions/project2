@extends('layouts.app')

@section('content')
<!-- home-slider start here -->
<div class="carousel slide aos-item home-banner" data-aos="fade-up"  id="main-carousel" data-ride="carousel">
    <ol class="carousel-indicators">
        <li data-target="#main-carousel" data-slide-to="0" class="active"></li>
        <li data-target="#main-carousel" data-slide-to="1"></li>
        <li data-target="#main-carousel" data-slide-to="2"></li>
    </ol>
    <!-- /.carousel-indicators -->
    <div class="carousel-inner">
        <div class="carousel-item active">
            <div class="slide-content">
                <img class="d-block img-fluid" src="images/banner.jpg" alt="">
                <div class="carousel-caption d-none d-md-block">
                    <h5 class="curly-font">
                    lets go Now</h3>
                    <h3>Locals <span class="green-color">From</span> Zero</h3>
                    <p>Marketplace For Local Experiences</p>
                    <button type="button" class="btn btn-danger pink-bg banner-btn">Start Exploring</button>
                </div>
            </div>
        </div>
        <div class="carousel-item">
            <div class="slide-content">
                <img class="d-block img-fluid" src="images/banner2.jpg" alt="">
                <div class="carousel-caption d-none d-md-block">
                    <h5 class="curly-font">
                    lets go Now</h3>
                    <h3>Locals <span class="green-color">From</span> Zero</h3>
                    <p>Marketplace For Local Experiences</p>
                    <button type="button" class="btn btn-danger pink-bg banner-btn">Start Exploring</button>
                </div>
            </div>
        </div>
        <div class="carousel-item">
            <div class="slide-content">
                <img class="d-block img-fluid" src="images/banner.jpg" alt="">
                <div class="carousel-caption d-none d-md-block">
                    <h5 class="curly-font">
                    lets go Now</h3>
                    <h3>Locals <span class="green-color">From</span> Zero</h3>
                    <p>Marketplace For Local Experiences</p>
                    <button type="button" class="btn btn-danger pink-bg banner-btn">Start Exploring</button>
                </div>
            </div>
        </div>
    </div>
    <!-- /.carousel-inner -->
    <!--a href="#main-carousel" class="carousel-control-prev" data-slide="prev">
        <span class="carousel-control-prev-icon"></span>
        <span class="sr-only" aria-hidden="true">Prev</span>
        </a>
        <a href="#main-carousel" class="carousel-control-next" data-slide="next">
        <span class="carousel-control-next-icon"></span>
        <span class="sr-only" aria-hidden="true">Next</span>
        </a-->
</div>
<!-- /.carousel -->
<!-- home-slider ends here -->
<!-- search form start here -->
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <form class="search-form">
                <div class="form-group">
                    <input type="text" class="form-control" id="lets-find" placeholder="Let’s Find the Experience">
                </div>
                <div class="form-group mx-sm-3">
                    <input type="text" class="form-control" id="location" placeholder="Location">
                </div>
                <div class="form-group mx-sm-3">
                    <select class="form-control">
                        <option>All Categories</option>
                        <option> NATURE</option>
                        <option>FOOD & DRINK</option>
                        <option>HISTORY & CULTURE</option>
                        <option>ART & CRAFTS</option>
                        <option>SPORTS</option>
                        <option>FAMILY</option>
                        <option>TOURS</option>
                        <option>FARM</option>
                        <option>OTHER</option>
                        <option>GIFTS</option>
                    </select>
                </div>
                <button type="submit" class="btn btn-danger pink-bg">Search</button>
            </form>
        </div>
    </div>
</div>
<!-- search form ends here -->
<!-- how it work start here -->
<section class="how-it-work aos-item" data-aos="fade-down">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="main-heading">
                    <h5 class="curly-font green-color">Discover & Connect</h5>
                    <h3>How it works</h3>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-12">
                <div class="step aos-item" data-aos="fade-in">
                    <span class="cloud-span"><img src="images/how-work-icon1.png" /></span>
                    <h4>Find Interesting Experience</h4>
                    <p>LocalsFromZero experiences you are leading the change towards a more sensitive tourism.</p>
                    <span class="count-num">01</span>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-12">
                <div class="step aos-item" data-aos="fade-in">
                    <span class="cloud-span"><img src="images/how-work-icon2.png" /></span>
                    <h4>Connect to Scouts</h4>
                    <p>We connect you with locals, with their stories, passions, expertise, their clubs, charities, associations and much more.</p>
                    <span class="count-num">02</span>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-12">
                <div class="step aos-item" data-aos="fade-in">
                    <span class="cloud-span"><img src="images/how-work-icon3.png" /></span>
                    <h4>Enjoy Your Experience</h4>
                    <p>We want to intensify the value of holidays - and help you build a deeper bond with a place and the people who live there.</p>
                    <span class="count-num">03</span>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- how it work start here -->
<!-- featured exp start here -->
<section class="featured-exp aos-item" data-aos="fade-down">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="main-heading">
                    <h5 class="curly-font green-color">Choose your perfect Holiday</h5>
                    <h3>Featured Experiences</h3>
                </div>
            </div>
            <!-- exp-box start -->
            <div class="col-lg-4 col-md-4 col-sm-12">
                <div class="exp-box">
                    <div class="exp-img"><img src="images/exp1.jpg"/></div>
                    <div class="exp-content">
                        <div class="rating-price">
                            <div class="rating">
                                <ul>
                                    <li class="filled-star"><i class="fas fa-star"></i></li>
                                    <li class="filled-star"><i class="fas fa-star"></i></li>
                                    <li class="filled-star"><i class="fas fa-star"></i></li>
                                    <li class="filled-star"><i class="fas fa-star-half-alt"></i></li>
                                    <li class="empty-star"><i class="fas fa-star"></i></li>
                                    <li>10 Reviews</li>
                                </ul>
                                <a href="#">EXPERIENCE THE KARST UNDERGROUND</a>
                            </div>
                            <div class="price">
                                <span class="price-span">€27</span>
                                <a href="#" class="wishlist-span"><i class="far fa-heart"></i></a>
                            </div>
                        </div>
                        <div class="booking-conditions">
                            <ul>
                                <li><i class="far fa-user"></i> Max: 8</li>
                                <li><i class="far fa-clock"></i> 3 Hours</li>
                                <li><i class="fas fa-map-marker-alt"></i> Koper</li>
                            </ul>
                        </div>
                        <div class="scout-box green-bg">
                            <div class="scout-img-name"><img src="images/exp-scout1.jpg" alt="scout-img"/> <a href="#" class="scout-name">Scout Tadej R</a></div>
                            <div class="scout-rating">
                                <p>4.36 <i class="fas fa-star"></i> (12)</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- exp-box ends -->
            <!-- exp-box start -->
            <div class="col-lg-4 col-md-4 col-sm-12">
                <div class="exp-box">
                    <div class="exp-img"><img src="images/exp2.jpg"/></div>
                    <div class="exp-content">
                        <div class="rating-price">
                            <div class="rating">
                                <ul>
                                    <li class="filled-star"><i class="fas fa-star"></i></li>
                                    <li class="filled-star"><i class="fas fa-star"></i></li>
                                    <li class="filled-star"><i class="fas fa-star"></i></li>
                                    <li class="filled-star"><i class="fas fa-star-half-alt"></i></li>
                                    <li class="empty-star"><i class="fas fa-star"></i></li>
                                    <li>11 Reviews</li>
                                </ul>
                                <a href="#">BIKE TOUR BORSEKA</a>
                            </div>
                            <div class="price">
                                <span class="price-span">€225</span>
                                <a href="#" class="wishlist-span"><i class="far fa-heart"></i></a>
                            </div>
                        </div>
                        <div class="booking-conditions">
                            <ul>
                                <li><i class="far fa-user"></i> Max: 8</li>
                                <li><i class="far fa-clock"></i> 3 Hours</li>
                                <li><i class="fas fa-map-marker-alt"></i> Koper</li>
                            </ul>
                        </div>
                        <div class="scout-box green-bg">
                            <div class="scout-img-name"><img src="images/exp-scout2.jpg" alt="scout-img"/> <a href="#" class="scout-name">Scout Luka J</a></div>
                            <div class="scout-rating">
                                <p>4.36 <i class="fas fa-star"></i> (12)</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- exp-box ends -->
            <!-- exp-box start -->
            <div class="col-lg-4 col-md-4 col-sm-12">
                <div class="exp-box">
                    <div class="exp-img"><img src="images/exp3.jpg"/></div>
                    <div class="exp-content">
                        <div class="rating-price">
                            <div class="rating">
                                <ul>
                                    <li class="filled-star"><i class="fas fa-star"></i></li>
                                    <li class="filled-star"><i class="fas fa-star"></i></li>
                                    <li class="filled-star"><i class="fas fa-star"></i></li>
                                    <li class="filled-star"><i class="fas fa-star-half-alt"></i></li>
                                    <li class="empty-star"><i class="fas fa-star"></i></li>
                                    <li>18 Reviews</li>
                                </ul>
                                <a href="#">KARST LIQUEURS AND LOCAL SPECIALTIES TASTING</a>
                            </div>
                            <div class="price">
                                <span class="price-span">€36</span>
                                <a href="#" class="wishlist-span"><i class="far fa-heart"></i></a>
                            </div>
                        </div>
                        <div class="booking-conditions">
                            <ul>
                                <li><i class="far fa-user"></i> Max: 8</li>
                                <li><i class="far fa-clock"></i> 3 Hours</li>
                                <li><i class="fas fa-map-marker-alt"></i> Koper</li>
                            </ul>
                        </div>
                        <div class="scout-box green-bg">
                            <div class="scout-img-name"><img src="images/exp-scout1.jpg" alt="scout-img"/> <a href="#" class="scout-name">Scout Tadej R</a></div>
                            <div class="scout-rating">
                                <p>4.36 <i class="fas fa-star"></i> (12)</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- exp-box ends -->
            <!-- exp-box start -->
            <div class="col-lg-4 col-md-4 col-sm-12">
                <div class="exp-box">
                    <div class="exp-img"><img src="images/exp4.jpg"/></div>
                    <div class="exp-content">
                        <div class="rating-price">
                            <div class="rating">
                                <ul>
                                    <li class="filled-star"><i class="fas fa-star"></i></li>
                                    <li class="filled-star"><i class="fas fa-star"></i></li>
                                    <li class="filled-star"><i class="fas fa-star"></i></li>
                                    <li class="filled-star"><i class="fas fa-star-half-alt"></i></li>
                                    <li class="empty-star"><i class="fas fa-star"></i></li>
                                    <li>20 Reviews</li>
                                </ul>
                                <a href="#">OLDTIMER TOUR | Citröen Dyane Tour</a>
                            </div>
                            <div class="price">
                                <span class="price-span">€80</span>
                                <a href="#" class="wishlist-span"><i class="far fa-heart"></i></a>
                            </div>
                        </div>
                        <div class="booking-conditions">
                            <ul>
                                <li><i class="far fa-user"></i> Max: 8</li>
                                <li><i class="far fa-clock"></i> 3 Hours</li>
                                <li><i class="fas fa-map-marker-alt"></i> Koper</li>
                            </ul>
                        </div>
                        <div class="scout-box green-bg">
                            <div class="scout-img-name"><img src="images/exp-scout3.jpg" alt="scout-img"/> <a href="#" class="scout-name">Scout Jaka G</a></div>
                            <div class="scout-rating">
                                <p>4.65 <i class="fas fa-star"></i> (29)</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- exp-box ends -->
            <!-- exp-box start -->
            <div class="col-lg-4 col-md-4 col-sm-12">
                <div class="exp-box">
                    <div class="exp-img"><img src="images/exp5.jpg"/></div>
                    <div class="exp-content">
                        <div class="rating-price">
                            <div class="rating">
                                <ul>
                                    <li class="filled-star"><i class="fas fa-star"></i></li>
                                    <li class="filled-star"><i class="fas fa-star"></i></li>
                                    <li class="filled-star"><i class="fas fa-star"></i></li>
                                    <li class="filled-star"><i class="fas fa-star-half-alt"></i></li>
                                    <li class="empty-star"><i class="fas fa-star"></i></li>
                                    <li>03 Reviews</li>
                                </ul>
                                <a href="#">BOUTIQUE WORKSHOP - 
                                TRADITIONAL SHOEMAKING</a>
                            </div>
                            <div class="price">
                                <span class="price-span">€125</span>
                                <a href="#" class="wishlist-span"><i class="far fa-heart"></i></a>
                            </div>
                        </div>
                        <div class="booking-conditions">
                            <ul>
                                <li><i class="far fa-user"></i> Max: 8</li>
                                <li><i class="far fa-clock"></i> 3 Hours</li>
                                <li><i class="fas fa-map-marker-alt"></i> Koper</li>
                            </ul>
                        </div>
                        <div class="scout-box green-bg">
                            <div class="scout-img-name"><img src="images/exp-scout4.jpg" alt="scout-img"/> <a href="#" class="scout-name">Scout Petra T</a></div>
                            <div class="scout-rating">
                                <p>3.0 <i class="fas fa-star"></i> (05)</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- exp-box ends -->
            <!-- exp-box start -->
            <div class="col-lg-4 col-md-4 col-sm-12">
                <div class="exp-box">
                    <div class="exp-img"><img src="images/exp5.jpg"/></div>
                    <div class="exp-content">
                        <div class="rating-price">
                            <div class="rating">
                                <ul>
                                    <li class="filled-star"><i class="fas fa-star"></i></li>
                                    <li class="filled-star"><i class="fas fa-star"></i></li>
                                    <li class="filled-star"><i class="fas fa-star"></i></li>
                                    <li class="filled-star"><i class="fas fa-star-half-alt"></i></li>
                                    <li class="empty-star"><i class="fas fa-star"></i></li>
                                    <li>24 Reviews</li>
                                </ul>
                                <a href="#">DISCOVER COSMETICS FROM SNAIL SALIVA</a>
                            </div>
                            <div class="price">
                                <span class="price-span">€4</span>
                                <a href="#" class="wishlist-span"><i class="far fa-heart"></i></a>
                            </div>
                        </div>
                        <div class="booking-conditions">
                            <ul>
                                <li><i class="far fa-user"></i> Max: 8</li>
                                <li><i class="far fa-clock"></i> 3 Hours</li>
                                <li><i class="fas fa-map-marker-alt"></i> Koper</li>
                            </ul>
                        </div>
                        <div class="scout-box green-bg">
                            <div class="scout-img-name"><img src="images/exp-scout1.jpg" alt="scout-img"/> <a href="#" class="scout-name">Scout Uroš J</a></div>
                            <div class="scout-rating">
                                <p>5 <i class="fas fa-star"></i> (39)</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- exp-box ends -->
        </div>
    </div>
</section>
<!-- featured exp ends here -->
<!-- video start here -->   
<section class="video-sec aos-item" data-aos="fade-in">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="main-heading">
                    <h5 class="curly-font green-color">Come & Explore</h5>
                    <h3>Locals From Zero Initiative</h3>
                </div>
            </div>
            <div class="col-lg-10 col-md-10 col-sm-12 offset-lg-1 offset-md-1">
                <p>Inspired by the challenges of the pandemic of 2020 and the problems of overtourism in recent years, the aim of the #TourismFromZero initiative is to provide a forum for the exchange of problems and solutions related to travel, tourism, hospitality, and leisure.</p>
                <div class="video-wrapper video-thumb">
                    <p>
                        <video class="video" poster="images/video-thumb.jpg" width="100%" height="">
                            <source src="https://buildingimagination.org/wp-content/uploads/2020/09/Seal-Made-with-Tinkercad-1.mp4" type="video/mp4" />
                        </video>
                    </p>
                    <div class="playpause"></div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- video ends here -->
<!-- our tesm start here -->
<section class="team-section aos-item" data-aos="fade-in">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="main-heading">
                    <h5 class="curly-font green-color">Our Team</h5>
                    <h3>Coordinators</h3>
                </div>
            </div>
            <div class="col-lg-12 col-md-12 col-12">
                <div class="slider">
                    <div class="slide team-box">
                        <img src="images/team1.jpg" alt="team">
                        <h4>Dejan Križaj</h4>
                        <p>Teaching, researching, and consuming tourism innovation at the University of Primorska.</p>
                        <button type="button" class="btn btn-success green-bg">Read More</button>
                    </div>
                    <div class="slide team-box">
                        <img src="images/team2.jpg" alt="team">
                        <h4>Scout Darja P <span class="team-rating"><i class="fas fa-star" aria-hidden="true"></i> ( 3.8 )</span></h4>
                        <p>Travelling is my passion, marketing and tourism is my profession.</p>
                        <button type="button" class="btn btn-success green-bg">Read More</button>
                    </div>
                    <div class="slide team-box">
                        <img src="images/team3.jpg" alt="team">
                        <h4>Scout Rudi M <span class="team-rating"><i class="fas fa-star" aria-hidden="true"></i> ( 5.0 )</span></h4>
                        <p>An active, positive and studious person, interested in making travel better, sustainable and local!</p>
                        <button type="button" class="btn btn-success green-bg">Read More</button>
                    </div>
                    <div class="slide team-box">
                        <img src="images/team4.jpg" alt="team">
                        <h4>Scout Tadej R <span class="team-rating"><i class="fas fa-star" aria-hidden="true"></i> ( 4.0 )</span></h4>
                        <p>Teaching, researching, and consuming tourism innovation at the University of Primorska.</p>
                        <button type="button" class="btn btn-success green-bg">Read More</button>
                    </div>
                    <div class="slide team-box">
                        <img src="images/team1.jpg" alt="team">
                        <h4>Dejan Križaj</h4>
                        <p>I am an assistant and Ph.D. student at the University of Primorska, Faculty of tourism studies Turistica.</p>
                        <button type="button" class="btn btn-success green-bg">Read More</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- our tesm ends here -->
<!-- scouts start here -->
<section class="scouts aos-item" data-aos="fade-up">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="main-heading">
                    <h5 class="curly-font green-color">Locals From Zero</h5>
                    <h3>Meet Our Scouts</h3>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-12">
                <div class="scouts-box">
                    <img src="images/scout-img1.jpg" />
                    <div class="scout-text">
                        <a href="#">Dominika Koritnik Trepel</a>
                        <div class="rating">
                            <ul>
                                <li class="filled-star"><i class="fas fa-star" aria-hidden="true"></i></li>
                                <li class="filled-star"><i class="fas fa-star" aria-hidden="true"></i></li>
                                <li class="filled-star"><i class="fas fa-star" aria-hidden="true"></i></li>
                                <li class="filled-star"><i class="fas fa-star-half-alt" aria-hidden="true"></i></li>
                                <li class="empty-star"><i class="fas fa-star" aria-hidden="true"></i></li>
                                <li>20 Reviews</li>
                            </ul>
                            <p>I am a long-time tourist guide, a university-educated theologian (SMC in Theology).</p>
                            <a href="#" class="pink-color">View Now</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-12">
                <div class="scouts-box">
                    <img src="images/scout-img2.jpg" />
                    <div class="scout-text">
                        <a href="#">Scout Katja K</a>
                        <div class="rating">
                            <ul>
                                <li class="filled-star"><i class="fas fa-star" aria-hidden="true"></i></li>
                                <li class="filled-star"><i class="fas fa-star" aria-hidden="true"></i></li>
                                <li class="filled-star"><i class="fas fa-star" aria-hidden="true"></i></li>
                                <li class="filled-star"><i class="fas fa-star-half-alt" aria-hidden="true"></i></li>
                                <li class="empty-star"><i class="fas fa-star" aria-hidden="true"></i></li>
                                <li>10 Reviews</li>
                            </ul>
                            <p>You will learn how to make traditional Slovenian bread or pastry called ''potica'' with the help.</p>
                            <a href="#" class="pink-color">View Now</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-12">
                <div class="scouts-box">
                    <img src="images/scout-img3.jpg" />
                    <div class="scout-text">
                        <a href="#">Scout Luka J</a>
                        <div class="rating">
                            <ul>
                                <li class="filled-star"><i class="fas fa-star" aria-hidden="true"></i></li>
                                <li class="filled-star"><i class="fas fa-star" aria-hidden="true"></i></li>
                                <li class="filled-star"><i class="fas fa-star" aria-hidden="true"></i></li>
                                <li class="filled-star"><i class="fas fa-star-half-alt" aria-hidden="true"></i></li>
                                <li class="empty-star"><i class="fas fa-star" aria-hidden="true"></i></li>
                                <li>10 Reviews</li>
                            </ul>
                            <p>Hey! My name is Luka and I will be your connection to the best experiences in Savinja valley.</p>
                            <a href="#" class="pink-color">View Now</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-12">
                <div class="scouts-box">
                    <img src="images/scout-img4.jpg" />
                    <div class="scout-text">
                        <a href="#">Scout Karin M</a>
                        <div class="rating">
                            <ul>
                                <li class="filled-star"><i class="fas fa-star" aria-hidden="true"></i></li>
                                <li class="filled-star"><i class="fas fa-star" aria-hidden="true"></i></li>
                                <li class="filled-star"><i class="fas fa-star" aria-hidden="true"></i></li>
                                <li class="filled-star"><i class="fas fa-star-half-alt" aria-hidden="true"></i></li>
                                <li class="empty-star"><i class="fas fa-star" aria-hidden="true"></i></li>
                                <li>10 Reviews</li>
                            </ul>
                            <p>I would describe myself as a dynamic gallivanter who is always in the drive for new challenges,</p>
                            <a href="#" class="pink-color">View Now</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-12">
                <div class="scouts-box">
                    <img src="images/scout-img5.jpg" />
                    <div class="scout-text">
                        <a href="#">Scout Zala R</a>
                        <div class="rating">
                            <ul>
                                <li class="filled-star"><i class="fas fa-star" aria-hidden="true"></i></li>
                                <li class="filled-star"><i class="fas fa-star" aria-hidden="true"></i></li>
                                <li class="filled-star"><i class="fas fa-star" aria-hidden="true"></i></li>
                                <li class="filled-star"><i class="fas fa-star-half-alt" aria-hidden="true"></i></li>
                                <li class="empty-star"><i class="fas fa-star" aria-hidden="true"></i></li>
                                <li>20 Reviews</li>
                            </ul>
                            <p>Hi everyone, my name is Zala and I am a first year student at Turistica.</p>
                            <a href="#" class="pink-color">View Now</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-12">
                <div class="scouts-box">
                    <img src="images/scout-img6.jpg" />
                    <div class="scout-text">
                        <a href="#">Scout Urban Š</a>
                        <div class="rating">
                            <ul>
                                <li class="filled-star"><i class="fas fa-star" aria-hidden="true"></i></li>
                                <li class="filled-star"><i class="fas fa-star" aria-hidden="true"></i></li>
                                <li class="filled-star"><i class="fas fa-star" aria-hidden="true"></i></li>
                                <li class="filled-star"><i class="fas fa-star-half-alt" aria-hidden="true"></i></li>
                                <li class="empty-star"><i class="fas fa-star" aria-hidden="true"></i></li>
                                <li>10 Reviews</li>
                            </ul>
                            <p>I have been working in tourism and hospitality for last 7 years. I am a certified local tour guide.</p>
                            <a href="#" class="pink-color">View Now</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- scouts ends here -->
<!-- TESTIMONIALS -->
<section class="testimonials aos-item" data-aos="fade-in">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="main-heading">
                    <h5 class="curly-font green-color">What Travellers Say</h5>
                    <h3>Recent Reviews</h3>
                </div>
            </div>
            <div class="col-sm-12">
                <div id="customers-testimonials" class="owl-carousel">
                    <!--TESTIMONIAL 1 -->
                    <div class="item">
                        <div class="shadow-effect">
                            <div class="item-details">
                                <h5>BIKE TOUR BORSEKA</h5>
                                <p>Integer ut ligula quis lectus fringilla elementum porttitor sed est. Duis fringilla efficitur ligula sed lobortis. Sed tempus faucibus mi,Integer ut ligula quis lectus fringilla elementum porttitor sed est. Duis fringilla efficitur ligula sed lobortis. Sed tempus faucibus mi</p>
                            </div>
                            <img class="img-responsive" src="images/testimonial-thumb.jpg" alt="">
                            <h4 class="testimonial-name">John Doe <span class="designation">College Professor</span></h4>
                        </div>
                    </div>
                    <!--END OF TESTIMONIAL 1 -->
                    <!--TESTIMONIAL 1 -->
                    <div class="item">
                        <div class="shadow-effect">
                            <div class="item-details">
                                <h5>OLDTIMER TOUR | Citröen Dyane Tour</h5>
                                <p>Integer ut ligula quis lectus fringilla elementum porttitor sed est. Duis fringilla efficitur ligula sed lobortis. Sed tempus faucibus mi,Integer ut ligula quis lectus fringilla elementum porttitor sed est. Duis fringilla efficitur ligula sed lobortis. Sed tempus faucibus mi</p>
                            </div>
                            <img class="img-responsive" src="images/testimonial-thumb.jpg" alt="">
                            <h4 class="testimonial-name">John Doe <span class="designation">College Professor</span></h4>
                        </div>
                    </div>
                    <!--END OF TESTIMONIAL 1 -->
                    <!--TESTIMONIAL 1 -->
                    <div class="item">
                        <div class="shadow-effect">
                            <div class="item-details">
                                <h5>OLDTIMER TOUR | Citröen Dyane Tour</h5>
                                <p>Integer ut ligula quis lectus fringilla elementum porttitor sed est. Duis fringilla efficitur ligula sed lobortis. Sed tempus faucibus mi,Integer ut ligula quis lectus fringilla elementum porttitor sed est. Duis fringilla efficitur ligula sed lobortis. Sed tempus faucibus mi</p>
                            </div>
                            <img class="img-responsive" src="images/testimonial-thumb.jpg" alt="">
                            <h4 class="testimonial-name">John Doe <span class="designation">College Professor</span></h4>
                        </div>
                    </div>
                    <!--END OF TESTIMONIAL 1 -->
                </div>
            </div>
        </div>
    </div>
</section>
<!-- END OF TESTIMONIALS -->
<!-- partner start here -->
<section class="partner aos-item" data-aos="fade-up">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="d-flex">
                    <h3>Partners:</h3>
                    <marquee behavior="scroll" direction="left" onmouseover="this.stop();" onmouseout="this.start();">
                        <ul>
                            <li><img src="images/partner-logo1.png"  alt="partner-name"/></li>
                            <li><img src="images/partner-logo2.png"  alt="partner-name"/></li>
                            <li><img src="images/partner-logo3.png"  alt="partner-name"/></li>
                            <li><img src="images/partner-logo2.png"  alt="partner-name"/></li>
                            <li><img src="images/partner-logo3.png"  alt="partner-name"/></li>
                        </ul>
                    </marquee>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- partner ends here -->
@endsection
