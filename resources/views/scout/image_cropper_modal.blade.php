  <div class="alert" role="alert"></div>
  <div class="modal fade imageCropperModal blogImageCropper" id="imageCropperModal" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="modalLabel">Crop the image</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <div class="img-container">
              <img id="modelImage" class="modelImage" src="https://avatars0.githubusercontent.com/u/3456749">
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" id="cancel-crop" data-dismiss="modal">Cancel</button>
            <button type="button" class="btn btn-primary pink-bg-btn" id="crop">Crop</button>
          </div>
        </div>
      </div>
    </div>