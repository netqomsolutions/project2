{{-- Extends layout --}}
@extends('layout.default')
{{-- Content --}}
@section('content')
<style type="text/css">
	.dataTables_wrapper .dataTable th:before {content:none !important;}
	.dataTables_wrapper .dataTable th:after {content:none !important;}
</style>
<div class="row">
    <div class="col-lg-12">
    	<meta name="csrf-token" content="{{ csrf_token() }}" />
       	<!--begin::Card-->
       	@if(session()->has('success'))
            <div class="alert alert-success alluser-page-suc" style="text-align: left;width: 100%;">
                {{ session()->get('success') }}
            </div>
            <script type="text/javascript">setTimeout(function(){ $('.alluser-page-suc').fadeOut(); }, 3000);</script>
        @elseif(session()->has('failure'))
            <div class="alert alert-danger alluser-page-suc" style="text-align: left; left;width: 100%;">
                {{ session()->get('failure') }}
            </div>
            <script type="text/javascript">setTimeout(function(){ $('.alluser-page-suc').fadeOut(); }, 3000);</script>
        @endif
		<script type="text/javascript">setTimeout(function(){ $('.scout-list-success').fadeOut(); }, 3000);</script>
		<div class="card card-custom">
			<div class="card-header">
				<div class="card-title w-100 justify-content-between">
					<div class="list_sec d-flex align-items-center">
					<span class="card-icon">
						<i class="flaticon2-supermarket text-primary"></i>
					</span>
				   <h3 class="card-label">List of Host</h3>
				</div>
				<div class="add-btn">
					<a id="" href="{{route('scout-add-host')}}" class="btn btn-sm float-right green-bg">Add New <i class="fa fa-plus" aria-hidden="true"></i></a>
				</div>
				</div>
			</div>
			<div class="card-body">
				<!--begin: Datatable-->
				<table class="table table-bordered yajra-datatable table-responsive-md list-exp-tble" style="margin-top: 13px !important">
					<thead>
						<tr>
							<th>AVATAR</th>
							<th>NAME</th>
							<th>EMAIL</th>
							<th>REGISTRATION NO.</th>
							<th>STATUS</th>
							<th>ACTIONS</th>
						</tr>
					</thead>
				</table>
				<!--end: Datatable-->
		
			</div>
		</div>
		<!--end::Card-->
    </div>
</div>
@endsection
{{-- Scripts Section --}}
@section('scripts')
<script type="text/javascript">
  	
  	$(document).ready(function(){
  		$.ajaxSetup({ headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') } });
  		var table = $('.yajra-datatable').DataTable({
	        processing: true,
	        serverSide: true,
	        ajax: "{{ route('scout-host-list') }}",
	        columns: [
	            {data: 'DT_RowIndex', id: 'DT_RowIndex',orderable: false,render:function(data,type,row){	       
	            	var imagUrl = '';
	            	if(row.user_image == null){	       
	            		imagUrl = HOST_URL + '/public/users/user.png';
	            		return "<img src=\"" + imagUrl + "\" height=\"50\"/ width=\"50\"/>";
	            	}else{
	            		imagUrl = HOST_URL + '/public/users/host/'+row.user_image;
	            		return "<img data-toggle='confirmation' src=\"" + imagUrl + "\" height=\"50\"/ width=\"50\"/>";
	            	}	            
	            }},
				{data: 'name', name: 'user_fname',render:function(data, type, row){ 
					return row.user_fname +' '+ row.user_lname;

				}},
	            {data: 'email', name: 'email'},
	            {data: 'company_registration_no', name: 'company_registration_no',render:function(data, type, row){ 
					return row.user_meta.company_registration_no;

				}},
	            {data: 'status', name: 'status',render:function(data, type, row){ 
	                var statusLeadColor = '';
	                var leadStatusName = '';
	                if(row.status == '1'){
	                    statusLeadColor = 'badge badge-success';
	                    leadStatusName = 'Active';
	                }else if (row.status == '2') {
	                    statusLeadColor = 'badge badge-danger';
	                    leadStatusName = 'Inactive';
	                }
	                return leadStatusName 
	            }},
	            {
	                data: 'action', 
	                name: 'action', 
	                orderable: false, 
	                searchable: true
	            },
	        ],
	        order: [[1, 'asc']]
	    });
	    $(document).on("click",".action-scout-btn1",function(){
		      var host_id=$(this).attr('host_id');
        swal({
            title: "Are you sure?",
            text: "You want to delete this.",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes!",
            closeOnConfirm: false
            },
            function (isConfirm) {
            if (!isConfirm) return;
            $.ajax({
            url: `{{ route("scout-delete-host") }}`,
            type: "POST",
            data: { host_id : host_id},
            dataType: "Json",
            success: function (data) {
            toastr.options.timeOut = 1500;
            if(data.isSucceeded){
            swal.close();
            toastr.success(data.message);
            $('.yajra-datatable').DataTable().ajax.reload();
            }else{
            swal("Error updating!", "Please try again", "error");
            }
            },
            error: function (xhr, ajaxOptions, thrownError) {
            swal("Error updating!", "Please try again", "error");
            }
            });
        });
		});

		$('.no-action').click(function(){
			$('#action-scout').css('opacity',0);
		  $('#action-scout').css('display','none');
		});
		 
		 
  	});
 
 

$(document).ready(function(){
setTimeout(function(){  
	$(".action-area").removeClass('force-hide');
 $('.delete-host-confirmation').confirmation({
				 onConfirm: function() {
		                   var host_id=$(this).attr('host_id');
				           $.ajax({
					            url: `{{ route("scout-delete-host") }}`,
					            type: "POST",
					            data: { host_id : host_id},
					            dataType: "Json",
					            success: function (data) {
					            toastr.options.timeOut = 1500;
					            if(data.isSucceeded){
					            swal.close();
					            toastr.success(data.message);
					            $('.yajra-datatable').DataTable().ajax.reload();
					            }else{
					             toastr.error("Please try again");
					            }
					            },
					            error: function (xhr, ajaxOptions, thrownError) {
					            	 toastr.error("Please try again");
					            }
					          });
							}

 });
 },1000);
});

</script>
@endsection