{{-- Extends layout --}}
@extends('layout.default')
{{-- Content --}}
@section('content')
<style type="text/css">
    .read-only{
        background-color: '#dedada' !important;
    }
</style>
@php 
    $current_sta = '';

    if(isset($userData) && optional($userData->user_meta)){
        $current_sta = optional($userData->user_meta)->current_status;
    }
@endphp
<div class="row">
    @if(session()->has('success'))
        <div class="alert alert-success alluser-page-suc" style="text-align: left;float: left;width: 100%;">
            {{ session()->get('success') }}
        </div>
        <script type="text/javascript">setTimeout(function(){ $('.alluser-page-suc').fadeOut(); }, 3000);</script>
    @elseif(session()->has('failure'))
        <div class="alert alert-danger alluser-page-suc" style="text-align: left; left;float: left;width: 100%;">
            {{ session()->get('failure') }}
        </div>
        <script type="text/javascript">setTimeout(function(){ $('.alluser-page-suc').fadeOut(); }, 3000);</script>
    @endif
    <div class="col-lg-12">
        <div class="card card-custom gutter-b example example-compact">
            <div class="card-header">
                <h3 class="card-title">Update Profile</h3>
                <div class="card-toolbar" style="display: none;">
                    <div class="example-tools justify-content-center">
                        <span class="example-toggle" data-toggle="tooltip" title="View code"></span>
                        <span class="example-copy" data-toggle="tooltip" title="Copy code"></span>
                    </div>
                </div>
            </div>
            <!--begin::Form-->
            <form id="edit-scout-form" class="form" method="post" action="{{ route('profile-update') }}" enctype="multipart/form-data">
                @csrf
                <div class="card-body">
                    <div class="form-group row">
                        <div class="col-lg-4 my-error">
                            <label>First Name:</label>
                            <input type="text" name="user_fname" value="{{ isset($userData) ? $userData->user_fname : '' }}" class="form-control" placeholder="Enter first name" />
                        </div>
                        <div class="col-lg-4 my-error">
                            <label>Last Name:</label>
                            <input type="text" name="user_lname" value="{{ isset($userData) ? $userData->user_lname : '' }}" class="form-control" placeholder="Enter last name" />
                        </div>
                        <div class="col-lg-4 my-error">
                            <label>Email:</label>
                            <input type="email" name="email" {{ isset($userData) ? "disabled" : "" }} value="{{ isset($userData) ? $userData->email : '' }}" class="form-control {{ isset($userData) ? 'read-only' : '' }}" placeholder="Enter your email address" />
                        </div>
                    </div>
                    <div class="form-group row">                        
                        <div class="col-lg-4 my-error">
                            <label>Address:</label>
                            <input type="text" name="user_address" value="{{ isset($userData) ? $userData->user_address : '' }}" class="form-control" placeholder="Enter your address" />
                        </div>
                        <div class="col-lg-4 my-error">
                            <label>Mobile:</label>
                            <input type="tel" id="user_mobile" name="user_mobile" value="{{ isset($userData) ? $userData->user_mobile : '' }}" class="form-control" placeholder="Enter your mobile number" />
                        </div>
                        <div class="col-lg-4 my-error">
                            <label>Scout Image:</label>
                            <input type="file" name="user_image" value="" class="form-control" placeholder="Browse Image" />
                            <input type="hidden" name="old_user_img" id="old_user_img" value="{{ isset($userData) ? $userData->user_image : '' }}">
                            
                        </div>
                    </div>
                    <div class="form-group my-error new_status value_inner english_level">
                        <label>What is your English level? *</label>
                        <div class="d-flex flex-wrap">
                            @php 
                                $english_levels = [
                                  'A1 Beginner','A2 Elementary English','B1 Intermediate English','B2 Upper-Intermediate English','C1 Advanced English','C2 Proficiency English','Other'
                                ];

                                $get_english_level = '';

                                if(isset($userData) && !empty($userData->user_english_level)){
                                    $get_english_level = $userData->user_english_level;
                                }
                            @endphp
                            @foreach($english_levels as $english_level)
                            @php 
                                $selected_english_level = '';
                                if(!empty($get_english_level) && $english_level==$get_english_level){
                                    $selected_english_level = 'checked';
                                }
                            @endphp
                            <div class="inner_form col-md-6 col-sm-12 p-0">
                                <input type="radio" value="{{ $english_level }} " name="user_english_level" {{ $selected_english_level }}/>
                                <span></span>
                                <label for="radio01-01">{{ $english_level }}</label>
                            </div>
                            @endforeach
                            <label for="user_english_level" class="error" style="display:none"></label>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-lg-6">
                            <label>What is your primary language? <span>You should be able to read, write, and speak in this language.</span></label>
                            <select name="user_primary_language" id="primary_language" class="form-control form-control-solid h-auto py-6 px-6 rounded-sm font-size-h6 " placeholder="Select Type" >
                                @php 
                                    $get_primary_language = '';
                                    if(isset($userData) && !empty($userData->user_primary_language)){
                                        $get_primary_language = $userData->user_primary_language;
                                    }
                                @endphp
                                @if(isset($languages) && !empty($languages))
                                @foreach($languages as $language)
                                    @php
                                        $selected_language = '';
                                        if(!empty($get_primary_language) && $language->short==$get_primary_language){
                                            $selected_language = 'selected';
                                        }
                                    @endphp
                                    <option value="{{ $language->short }}" {{ $selected_language }}>{{ $language->name }}</option> 
                                @endforeach
                                @endif
                            </select>
                        </div>
                        <div class="col-lg-6 my-error">
                            <label>What other languages do you speak fluently? <span>Only add languages you˙d be comfortable scouting in.</span></label>
                            <select  name="user_other_languages[]" class="form-control form-control-solid h-auto py-6 px-6 rounded-sm font-size-h6"  multiple="multiple">
                                @php 
                                    $get_other_language = '';
                                    if(isset($userData) && !empty($userData->user_primary_language)){
                                        $get_other_language = explode(",",$userData->user_other_languages);
                                    }
                                @endphp
                                @if(isset($languages) && !empty($languages))                                @foreach($languages as $language)
                                @php
                                    $select_language = '';
                                    if(in_array($language->short,$get_other_language)){
                                        $select_language = 'selected';
                                    }
                                @endphp
                                <option value="{{ $language->short }}" {{ $select_language }}>{{ $language->name }}</option> 
                                @endforeach
                                @endif
                                </select>
                        </div>
                    </div>
                    <div class="form-group new_status value_inner current_status my-error">
                        <label>Your current status: *</label>
                        <label for="current_status" class="error" style="display: none;">This field is required.</label>
                        <div class="d-flex flex-wrap">
                            <div class="inner_form col-md-4 col-sm-12 p-0">
                                <input type="radio" id="radio70" value="Student"  name="current_status" {{ $current_sta=='Student' ? 'checked':''}}/><span></span><label for="radio01-01">Student</label>
                            </div>
                            <div class="inner_form col-md-4 col-sm-12 p-0">
                                <input type="radio" id="radio71" value="Self employed" name="current_status" {{ $current_sta=='Self employed' ? 'checked':''}}/><span></span><label for="radio01-02">Self employed</label>
                            </div>
                            <div class="inner_form col-md-4 col-sm-12 p-0">
                                <input type="radio"   id="radio72" value="Regularly employed" name="current_status" {{ $current_sta=='Regularly employed' ? 'checked':''}}/><span></span><label for="radio01-03">Regularly employed</label>
                            </div>
                            <div class="inner_form col-md-4 col-sm-12 p-0">
                                <input  type="radio" id="radio73" value="Unemployed" name="current_status" {{ $current_sta=='Unemployed' ? 'checked':''}}/><span></span><label for="radio01-03">Unemployed</label>
                            </div>
                            <div class="inner_form col-md-4 col-sm-12 p-0">
                                <input   type="radio" id="radio74" value="Retired" name="current_status" {{ $current_sta=='Retired' ? 'checked':''}}/><span></span><label for="radio01-03">Retired</label>
                            </div>
                            <div class="inner_form col-md-4 col-sm-12 p-0">
                                <input type="radio" id="radio75" value="Other" name="current_status" {{ $current_sta=='other' ? 'checked':''}}/><span></span><label for="radio01-03">Other:</label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group my-error">
                        <label>Describe yourself: *<span>What makes you uniquely qualified to become a scout? Tell guests why you’re passionate and knowledgeable about. Note: This description goes public.</span></label>
                        @php 
                            $about_self = '';
                            if(isset($userData) && !empty($userData->about_me)){
                                $about_self = $userData->about_me;
                            }
                        @endphp
                        <textarea placeholder="Write Message" id="describe_youself" name="about_me" class=" form-control" rows="3">{{ $about_self ? $about_self : ''}}</textarea>
                    </div>
                    <div class="card-footer">
                        <div class="row">
                            <div class="col-lg-6">
                                <button type="submit" class="btn btn-primary mr-2">Save</button>
                                <a href="{{ route('scout-host-list') }}" class="btn btn-secondary">Back</a>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
            <!--end::Form-->
        </div>
    </div>
</div>
@endsection
{{-- Scripts Section --}}
@section('scripts')
<script type="text/javascript">
    $("select[multiple='multiple']").bsMultiSelect({
      placeholder    : 'Add other languages',
    });

    $.ajaxSetup({headers: {'X-XSRF-TOKEN': decodeURIComponent(/XSRF-Token=([^;]*)/ig.exec(document.cookie)[1])}});
    $(document).ready(function () {

        $.validator.addMethod('filesize', function (value, element, param) {
            return this.optional(element) || (element.files[0].size <= param)
        }, 'File size must be less than {0}');
        $('#edit-scout-form').validate({
            submitHandler: function(form) {
              // do other things for a valid form
              form.submit();
            },
            rules: {
                user_fname: {
                    required: true
                },
                user_lname: {
                    required: true,
                },
                email:{
                    required: true,
                },
                user_address:{
                    required: true
                },
                user_mobile:{
                    required: true,
                    digits: true,
                    maxlength: 10,
                    minlength: 10,
                    remote: {
                        url: HOST_URL+"/check-user-mobile-existence",
                        type: "post"
                    }
                },
                user_image:{
                    required: function(){
                        if($("#old_user_img").val()){
                            return false;
                        }else{
                            return true;
                        }
                    },
                    extension: "jpg,jpeg,png",
                    filesize: 2*1024*1024,
                },
                user_english_level:{
                    required:true
                },
                about_me:{
                    required:true
                },
                user_primary_language:{
                    required:true
                },
                current_status:{
                    required:true
                },
            },
            messages: {
                user_fname: {
                    required: "Please enter your first name",
                },  
                user_lname: {                 
                    required : "Please enter your last name",
                },
                user_address: {
                    required : "Please enter your address",
                },
                email: {
                    required : "Please enter your email address",
                },
                user_mobile: {
                    required : "Please enter your mobile number",
                    remote: "This mobile number already exist"
                },
                user_image:{
                    required: "Please select the image",
                    extension: "Please select jpg,jpeg and png image",
                    filesize: "File size must be less than 2MB",
                },
            },
            errorElement: 'span',           
            errorPlacement: function (error, element) {
              error.addClass('invalid-feedback');
              element.closest('.my-error').append(error);
            },
            highlight: function (element, errorClass, validClass) {
              $(element).addClass('is-invalid');
            },
            unhighlight: function (element, errorClass, validClass) {
              $(element).removeClass('is-invalid');
            }
        });
    });
</script>
@endsection