{{-- Extends layout --}}
@extends('layout.default')

{{-- Content --}}
@section('content')

@php
	$profile_pic = 'users/user.png';

	$profile_path = Auth::user()->user_image_path;

	$profile = Auth::user()->user_image;

	if($profile && $profile_path){
		$profile_pic = $profile_path.'/'.$profile;
	}
@endphp
<!--begin::Container-->
<div class="card card-custom gutter-b ">
	<div class="card-body">
		<!--begin::Details-->
		<div class="d-flex">
			<!--begin: Pic-->
			<div class="flex-shrink-0 mr-7 mt-lg-0 mt-3">
				<div class="symbol symbol-50 symbol-lg-120">
					<img src="{{ asset($profile_pic) }}" alt="image">
				</div>
				<div class="symbol symbol-50 symbol-lg-120 symbol-primary d-none">
					<span class="font-size-h3 symbol-label font-weight-boldest">JM</span>
				</div>
			</div>
			<!--end::Pic-->

			<!--begin::Info-->
			<div class="flex-grow-1 color-change-traveller">
				<!--begin::Title-->
				<div class="d-flex justify-content-between flex-wrap mt-1">
					<div class="d-flex mr-3">
						@if(Auth::user()->user_meta->account_type==1 || Auth::user()->user_meta->student_balance > 0)
						<a class="text-hover-primary font-size-h5 font-weight-bold mr-3">Earned as a student <span class="text-pink bal-text"><i class="fas fa-euro-sign text-pink" aria-hidden="true"></i>{{(Auth::user()->user_meta->student_balance!==null)? number_format(Auth::user()->user_meta->student_balance) : '0'}}</span></a>
						<!--a href="#">
							<i class="flaticon2-correct text-success font-size-h5"></i>
						</a-->
						@endif
					</div>
					<div class="my-lg-0 my-3">
						<a href="{{ route('user-profile') }}" class="btn btn-sm btn-info pink-bg font-weight-bolder d-flex align-items-center"><i class="icon-xl la la-user-alt"></i> Manage Profile</a>
					</div>
				</div>
				<!--end::Title-->

				<!--begin::Content-->
				<div class="d-flex flex-wrap justify-content-between mt-1">
					<div class="d-flex flex-column flex-grow-1">
						<div class="d-flex flex-wrap mb-4">
							<a href="#" class=" text-hover-primary font-weight-bold mr-lg-8 mr-5 mb-lg-0 mb-2">
								<i class="flaticon2-new-email mr-2 font-size-lg"></i>{{ Auth::user()->email }}
							</a>
							<a href="#" class=" text-hover-primary font-weight-bold mr-lg-8 mr-5 mb-lg-0 mb-2">
								<i class="flaticon2-calendar-3 mr-2 font-size-lg"></i>{{ Auth::user()->user_fname.' '.Auth::user()->user_lname }}
							</a>
							<a href="#" class=" text-hover-primary font-weight-bold">
								<i class="flaticon2-placeholder mr-2 font-size-lg"></i>{{ Auth::user()->user_address }}
							</a>
						</div>
					</div>
				</div>
				<!--end::Content-->

				<!--end::Details-->
				<div class="separator separator-solid"></div>

				<!--begin::Items-->
				<div class="d-flex align-items-center flex-wrap mt-4">
					<!--begin::Item-->
					<div class="d-flex align-items-center flex-lg-fill mr-5 mb-2">
						<span class="mr-4">
							<i class="flaticon-file-2 display-4 font-weight-bold"></i>
						</span>
						<div class="d-flex flex-column">
							<a href="{{route('scout-experience-list')}}" class="font-weight-bolder font-size-lg">Total Experiences</a>
							<span class="font-weight-bolder font-size-h5">
								<span class=" font-weight-bold"></span>{{$totalExperience}}
							</span>
						</div>
					</div>
					<!--end::Item-->

					

					<!--begin::Item-->
					<div class="d-flex align-items-center flex-lg-fill mr-5 mb-2">
						<span class="mr-4">
							<i class="flaticon-list display-4 font-weight-bold"></i>
						</span>
						<div class="d-flex flex-column">
							<a href="{{route('scout-booked-experience-list')}}" class="font-weight-bolder font-size-lg">Booked Experiences</a>
							<span class="font-weight-bolder font-size-h5">
								<span class=" font-weight-bold"></span>{{$totalBooking}}
							</span>
						</div>
					</div>
					<!--end::Item-->
					<!--begin::Item-->
					<div class="d-flex align-items-center flex-lg-fill mr-5 mb-2">
						<span class="mr-4">
							<i class="flaticon-piggy-bank display-4 font-weight-bold"></i>
						</span>
						<div class="d-flex flex-column">
							<span class="font-weight-bolder font-size-lg">Total Earnings</span>
							<span class="font-weight-bolder font-size-h5">
								<span class=" font-weight-bold"></span><i class="fas fa-euro-sign" aria-hidden="true"></i> {{$totalEarning}}
							</span>
						</div>
					</div>
					<!--end::Item-->
				</div>
				<!--begin::Items-->
			</div>
			<!--end::Info-->
		</div>
	</div>
</div>

<!--  Chat Inbox html Start here  -->
<div class="content d-flex flex-column flex-column-fluid main-area-message" id="kt_content">
	<!--begin::Entry-->
	<div class="d-flex flex-column-fluid">
		<!--begin::Container-->
		<!--begin::Chat-->
		<div class="d-flex flex-row w-100 mb-10">
			<!--begin::Aside-->
			<div class="flex-row-auto offcanvas-mobile w-350px w-xl-400px" id="kt_chat_aside">
				<!--begin::Card-->
				<div class="card card-custom">
					<!--begin::Body-->
					<div class="card-body">
						<!--begin:Search-->
						<div class="input-group input-group-solid">
							<div class="input-group-prepend">
								<span class="input-group-text">
									<span class="svg-icon svg-icon-lg">
										<!--begin::Svg Icon | path:assets/media/svg/icons/General/Search.svg-->
										<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
											<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
												<rect x="0" y="0" width="24" height="24" />
												<path d="M14.2928932,16.7071068 C13.9023689,16.3165825 13.9023689,15.6834175 14.2928932,15.2928932 C14.6834175,14.9023689 15.3165825,14.9023689 15.7071068,15.2928932 L19.7071068,19.2928932 C20.0976311,19.6834175 20.0976311,20.3165825 19.7071068,20.7071068 C19.3165825,21.0976311 18.6834175,21.0976311 18.2928932,20.7071068 L14.2928932,16.7071068 Z" fill="#000000" fill-rule="nonzero" opacity="0.3" />
												<path d="M11,16 C13.7614237,16 16,13.7614237 16,11 C16,8.23857625 13.7614237,6 11,6 C8.23857625,6 6,8.23857625 6,11 C6,13.7614237 8.23857625,16 11,16 Z M11,18 C7.13400675,18 4,14.8659932 4,11 C4,7.13400675 7.13400675,4 11,4 C14.8659932,4 18,7.13400675 18,11 C18,14.8659932 14.8659932,18 11,18 Z" fill="#000000" fill-rule="nonzero" />
											</g>
										</svg>
										<!--end::Svg Icon-->
									</span>
								</span>
							</div>
							<input id="usersearch" type="text" class="form-control py-4 h-auto" placeholder="Search user" />
						</div>
						<!--end:Search-->
						<!--begin:Users-->
						<div class="mt-7 scroll scroll-pull userListing">


							<!--begin:User-->
							@foreach($userList as $k => $userLists)

							@foreach($userLists as $userData)
							@php
							  $is_online = "";
							  $labelStatus ="label-offline";
							  $labelStatusText ="offline";
							  @endphp
							@if($userData->is_online == 1)
							 @php
							  $is_online = "online_active_user";
							  $labelStatus ="label-success";
							  $labelStatusText ="Active";
							  @endphp
							@endif 
							<div  class="d-flex align-items-center justify-content-between mb-5 user-listing-main">
									<a href="javascript:void(0);" id="user_list_{{ $userData->id }}" data-scoutid="{{ $userData->id }}"  data-trevid="{{ $user->id }}" data-io="{{ $k }}"  data-scoutname="{{ $userData->user_fname }} {{ $userData->user_lname }}"   class="d-flex align-items-center userListing-inner selectToChat"
										data-alabel="{{ $labelStatus }}"
										data-alabeltxt="{{ $labelStatusText }}"
										data-isonline="{{$userData->is_online}}"
										data-userEmail="{{$userData->email}}"
										>
									<div class="symbol symbol-circle symbol-50 mr-3">
										@if(!empty($userData->user_image))
										<img alt="Pic" src="{{ asset($userData->user_image_path.'/'.$userData->user_image) }}" />
										@else
										<img alt="Pic" src="https://i.stack.imgur.com/l60Hf.png" />
										@endif
									</div>
									<div class="d-flex flex-column userListing-inn">
										<span class="text-hover-primary font-weight-bold font-size-lg">{{ $userData->user_fname .' ' .$userData->user_lname }} </span>
										<div class="onlineStatus"><span class="label label-sm label-dot {{ $labelStatus }}"></span>
											<span class="font-weight-bold text-muted font-size-sm">{{ $labelStatusText }}</span>
										</div>


										<span class=" font-weight-bold font-size-sm "> </span>
								</div>
								</a>
								@if(isset($userData->messages_count) && $userData->messages_count > 0)
								<span class="message-count mes-count-{{ $k }}">{{ $userData->messages_count }}
								</span>
								@endif
							</div>

							@endforeach
							@endforeach



						</div>
						<!--end:Users-->
					</div>
					<!--end::Body-->
				</div>
				<!--end::Card-->
			</div>
			<!--end::Aside-->
			<!--begin::Content-->
			<div class="flex-row-fluid ml-lg-8" id="kt_chat_content">
				<!--begin::Card-->
				<div class="card card-custom">
					<!--begin::Header-->
					<div class="card-header align-items-center px-4 py-3">
						<div class="text-left flex-grow-1 d-lg-none">
							<!--begin::Aside Mobile Toggle-->
							<button type="button" class="btn btn-clean btn-sm btn-icon btn-icon-md d-lg-none" id="kt_app_chat_toggle">
								<span class="svg-icon svg-icon-lg">
									<!--begin::Svg Icon | path:assets/media/svg/icons/Communication/Adress-book2.svg-->
									<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
										<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
											<rect x="0" y="0" width="24" height="24" />
											<path d="M18,2 L20,2 C21.6568542,2 23,3.34314575 23,5 L23,19 C23,20.6568542 21.6568542,22 20,22 L18,22 L18,2 Z" fill="#000000" opacity="0.3" />
											<path d="M5,2 L17,2 C18.6568542,2 20,3.34314575 20,5 L20,19 C20,20.6568542 18.6568542,22 17,22 L5,22 C4.44771525,22 4,21.5522847 4,21 L4,3 C4,2.44771525 4.44771525,2 5,2 Z M12,11 C13.1045695,11 14,10.1045695 14,9 C14,7.8954305 13.1045695,7 12,7 C10.8954305,7 10,7.8954305 10,9 C10,10.1045695 10.8954305,11 12,11 Z M7.00036205,16.4995035 C6.98863236,16.6619875 7.26484009,17 7.4041679,17 C11.463736,17 14.5228466,17 16.5815,17 C16.9988413,17 17.0053266,16.6221713 16.9988413,16.5 C16.8360465,13.4332455 14.6506758,12 11.9907452,12 C9.36772908,12 7.21569918,13.5165724 7.00036205,16.4995035 Z" fill="#000000" />
										</g>
									</svg>
									<!--end::Svg Icon-->
								</span>
							</button>
							<!--end::Aside Mobile Toggle-->
						</div> 
						<div class="text-center flex-grow-1">
							<div class="font-weight-bold font-size-h5" id="scoutName">{{Auth::user()->user_fname.' '.Auth::user()->user_lname}}</div>
							<div class="onlineStatus onlineStatus_dash">
								<span class="label label-sm label-dot"></span>
								<span class="font-weight-bold label-text text-muted font-size-sm"></span>
							</div>
						</div>

					</div>
					<!--end::Header-->
					<!--begin::Body-->
					<div class="card-body">
						<!--begin::Scroll-->
						<div class="scroll scroll-pull main-message-area" data-mobile-height="350">
							<div class="loader-lfz" id="loader-lfz" style="display: none;"><img src="<?php echo URL::to('/');?>/public/images/loader.gif"></div>
							<!--begin::Messages-->
							<div class="messages-custom"> 
								<div class="start-chat-wlcmes">
									<h4> Start your chat</h4>
									<p> Click on left side list to get started </p>
									<div class="image-logo">
										<img src="{{asset('images/chat-with-us.jpg')}}">
									</div>	
								</div>
							</div>
							<!--end::Scroll-->
						</div>
						<!--end::Body-->
						<!--begin::Footer-->
						<div class="card-footer send-textarea align-items-center force-hide">
							<!--begin::Compose-->
							<textarea class="form-control border-0 p-0 write_msg" rows="2" placeholder="Type a message" data-date_time = "{{ date('Y-m-d h:i:s A') }}" data-sender_id = "{{ $user->id }}"></textarea>
							<div class="d-flex align-items-center justify-content-between main-sendBtn">

								<div class="SendBtn ">
									<input type="hidden" id="reciver_id" value="">
									<button id="msg_send_btn" type="button" class="btn btn-primary btn-md text-uppercase font-weight-bold py-2 px-6"><i class="fas fa-paper-plane"></i></button>
								</div>
							</div>
							<!--begin::Compose-->
						</div>
						<!--end::Footer-->
					</div>
					<!--end::Card-->
				</div>
				<!--end::Content-->
			</div>
			<!--end::Chat-->
		</div>
		<!--end::Container-->
		<!--end::Entry-->
	</div>
</div>

@endsection

{{-- Scripts Section --}}
@section('scripts')
<script src="{{ asset('js/pages/widgets.js') }}" type="text/javascript"></script>
<script src="{{asset('js/pages/custom/chat/chat.js')}}"></script>
<script type="text/javascript">
$(document).ready(function(){

	$("#user_list_{{ $scoutId }}").click();  
});
</script>
@endsection
