{{-- Extends layout --}}
@extends('layout.default')
{{-- Content --}}
@section('content')

@php 
    $current_sta = '';

    if(isset($scoutData) && optional($scoutData->user_meta)){
        $current_sta = optional($scoutData->user_meta)->current_status;
    }
@endphp
<style>
	.toggle.ios, .toggle-on.ios, .toggle-off.ios { border-radius: 20rem; }
	.toggle.ios .toggle-handle { border-radius: 20rem; }
	div#action-scout {
	    align-items: center;
	    justify-content: center;
	}
</style>
 @if(session()->has('success'))
            <div class="alert alert-success alluser-page-suc" style="text-align: left;float: left;width: 100%;">
                {{ session()->get('success') }}
            </div>
            <script type="text/javascript">setTimeout(function(){ $('.alluser-page-suc').fadeOut(); }, 3000);</script>
        @elseif(session()->has('failure'))
            <div class="alert alert-danger alluser-page-suc" style="text-align: left; left;float: left;width: 100%;">
                {{ session()->get('failure') }}
            </div>
            <script type="text/javascript">setTimeout(function(){ $('.alluser-page-suc').fadeOut(); }, 3000);</script>
        @endif
    <div class="main-body">
          <!--  <div class="user-profile-buttons">
            <a href="{{ route('user-profile-edit') }}" class="navi-item">Update Profile</a>
            <a href="{{ route('user-change-password-form') }}" class="navi-item">Change Password</a>
          </div> -->
		  
		  
		  <div class="d-flex flex-row w-100">
								@include('profile-sidebar')
									<!--begin::Content-->
									<div class="flex-row-fluid ml-lg-8 profile-right_form">
										<!--begin::Card-->
										<div class="card card-custom card-stretch card-sticky" id="kt_page_sticky_card">
											<!--begin::Header-->
											<div class="card-header py-3 ">
												<div class="card-title align-items-start flex-column">
													<h3 class="card-label font-weight-bolder text-dark">Personal Information</h3>
													<span class="text-muted font-weight-bold font-size-sm mt-1">Update your personal informaiton</span>
												</div>
												<div class="card-toolbar">
													<button type="button" class="btn btn-success mr-2 green-bg-btn" id="save-profile-button">Save Changes</button>
													<!-- <button type="reset" class="btn btn-secondary">Cancel</button> -->
												</div>
											</div>
											<!--end::Header-->
											<!--begin::Form-->
											<form id="edit-scout-form" class="form" method="post" action="{{ route('profile-update') }}" enctype="multipart/form-data">
												<!--begin::Body-->
												@csrf
												<div class="card-body">
													<div class="form-group row manage-personal-info">
														<div class="col-lg-4">
														<label class="col-xl-12 col-lg-12 col-form-label">Avatar</label>
														<div class="col-lg-12 col-xl-12">
															@if (file_exists(public_path($scoutData->user_image_path)) && $scoutData->user_image)
															<div class="image-input image-input-outline" id="kt_profile_avatar" style="background-image: url({{ asset($scoutData->user_image_path.'/'.$scoutData->user_image) }})">
															@else
															<div class="image-input image-input-outline" id="kt_profile_avatar" style="background-image: url({{ asset('users/user.png') }})">
															@endif
																<div class="image-input-wrapper" style="background-image: url(/metronic/theme/html/demo1/dist/assets/media/users/300_21.jpg)"></div>
																<label class="btn btn-xs btn-icon btn-circle btn-white btn-hover-text-primary btn-shadow" data-action="change" data-toggle="tooltip" title="" data-original-title="Change avatar">
																	<i class="fa fa-pen icon-sm text-muted"></i>
																	<input type="file" name="user_image">
																	<input type="hidden" name="profile_avatar_remove">
																</label>
																<span class="btn btn-xs btn-icon btn-circle btn-white btn-hover-text-primary btn-shadow" data-action="cancel" data-toggle="tooltip" title="" data-original-title="Cancel avatar">
																	<i class="ki ki-bold-close icon-xs text-muted"></i>
																</span>
																<span class="btn btn-xs btn-icon btn-circle btn-white btn-hover-text-primary btn-shadow" data-action="remove" data-toggle="tooltip" title="" data-original-title="Remove avatar">
																	<i class="ki ki-bold-close icon-xs text-muted"></i>
																</span>
															</div>
															<span class="form-text text-muted">Allowed file types: png, jpg, jpeg.</span>
															<span class="form-text text-muted">Note: Please upload image of 160 X 160 dimension.</span>
															<label id="user_image-error" class="error" style="display:none;" for="user_image">Please select the image</label>
														</div>
													</div>

													<div class="col-lg-8 pl-0 pr-0">
														<div class="form-group row">
														<label class="col-xl-12 col-lg-12 col-form-label">First Name</label>
														<div class="col-lg-12 col-xl-12">
															<input class="form-control form-control-lg form-control-solid" name="user_fname" type="text" value="{{$scoutData->user_fname}}">
														</div>
													</div>
													<div class="form-group row">
														<label class="col-xl-12 col-lg-12 col-form-label">Last Name</label>
														<div class="col-lg-12 col-xl-12">
															<input class="form-control form-control-lg form-control-solid" name="user_lname" type="text" value="{{$scoutData->user_lname}}">
														</div>
													</div>
													@if(Auth::user()->user_meta->account_type==1)
													<div class="form-group row">
														
														<div class="col-lg-12 col-xl-12 d-flex align-items-center now-scout">
															<input class="form-control form-control-lg form-control-solid" name="account_type" type="checkbox" value="0">
															<label>Now I am scout</label>
														</div>
													</div>
													@endif
													</div>

													</div>





													
													<!-- <div class="form-group row">
														<label class="col-xl-3 col-lg-3 col-form-label">Company Name</label>
														<div class="col-lg-9 col-xl-6">
															<input class="form-control form-control-lg form-control-solid" type="text" value="Loop Inc.">
															<span class="form-text text-muted">If you want your invoices addressed to a company. Leave blank to use your full name.</span>
														</div>
													</div> -->
													<div class="row">
														<div class="col-lg-12 col-xl-12">
															<h5 class="font-weight-bold mt-10 heading-scout-form">Contact Info</h5>
														</div>
													</div>
													<div class="form-group row contact-scout-form">
														<label class="col-xl-3 col-lg-3 col-form-label">Contact Phone</label>
														<div class="col-lg-9 col-xl-6">
															<div class="input-group input-group-lg">
																<!-- <div class="input-group-prepend">
																	<span class="input-group-text">
																		<i class="la la-phone"></i>
																	</span>
																</div> -->
																<select name="user_mobile_code" class="country-code-select selectpicker profileCountryCode" data-size="5" data-live-search="true">
																	@forelse($getCountryCode as $CountryCode)
																	@php 
																		$selected_countryCode = '';
																		if(!empty($getCountryCode) && $CountryCode->phonecode==$scoutData->user_mobile_code){
																		$selected_countryCode = 'selected';
																	}
																	@endphp
							                                        <option value="{{ $CountryCode->phonecode }}" {{$selected_countryCode}}>+{{ $CountryCode->phonecode.' '.$CountryCode->iso }}</option>
							                                        @empty
							                                        @endforelse
																</select>
																<input type="text" class="form-control form-control-lg form-control-solid user_mobileMasking" value="{{$scoutData->user_mobile}}" id="user_mobile" name="user_mobile" placeholder="Phone">
															</div>
															<span class="form-text text-muted">We'll never share your email with anyone else.</span>
														</div>
													</div>
													<div class="form-group row contact-scout-form">
														<label class="col-xl-3 col-lg-3 col-form-label">Email Address</label>
														<div class="col-lg-9 col-xl-6">
															<div class="input-group input-group-lg input-group-solid">
																<div class="input-group-prepend">
																	<span class="input-group-text">
																		<i class="la la-at"></i>
																	</span>
																</div>
																<input type="email" name="email" {{ isset($scoutData) ? "readonly" : "" }} value="{{ isset($scoutData) ? $scoutData->email : '' }}" class="form-control form-control-lg form-control-solid {{ isset($scoutData) ? 'read-only' : '' }}"  placeholder="Email">
															</div>
														</div>
													</div>
													 <div class="form-group row contact-scout-form">
														<label class="col-xl-3 col-lg-3 col-form-label">Address</label>
														<div class="col-lg-9 col-xl-6">
															<div class="input-group input-group-lg input-group-solid">
																<input type="text" name="user_address" class="form-control form-control-lg form-control-solid" value="{{$scoutData->user_address}}" placeholder="Username" value="loop">
																
															</div>
														</div>
													</div> 


													<!-- Addtional Information section start from here  -->
													<div class="row">
														<div class="col-lg-12 col-xl-12">
															<h5 class="font-weight-bold mt-10 heading-scout-form">Addtional Information</h5>
														</div>
													</div>
													<div class="form-group my-error new_status value_inner english_level">
														<label>What is your English level? *</label>
														<div class="d-flex flex-wrap">
															@php 
															$english_levels = [
															'A1 Beginner','A2 Elementary English','B1 Intermediate English','B2 Upper-Intermediate English','C1 Advanced English','C2 Proficiency English','Other'
															];

															$get_english_level = '';

															if(isset($scoutData) && !empty($scoutData->user_english_level)){
															$get_english_level = $scoutData->user_english_level;
														}
														@endphp
														@foreach($english_levels as $english_level)
														@php 
														$selected_english_level = '';
														if(!empty($get_english_level) && $english_level==$get_english_level){
														$selected_english_level = 'checked';
													}
													@endphp
													<div class="inner_form col-xl-6 col-lg-12 col-sm-12 p-0">
														<input type="radio" value="{{ $english_level }} " name="user_english_level" {{ $selected_english_level }}/>
														<span></span>
														<label for="radio01-01">{{ $english_level }}</label>
													</div>
													@endforeach
													<label for="user_english_level" class="error" style="display:none"></label>
												</div>
											</div>
											<div class="form-group row scout-profile-lang">
												<div class="col-lg-12 col-xl-6 mb-lg-5 pb-lg-2 mb-md-5 pb-md-2">
													<label class="min-height-add">What is your primary language? <span class="cursor-pointer i-color-chnge" data-action="change" data-toggle="tooltip" title="" data-original-title="You should be able to read, write, and speak in this language."><i class="fas fa-question-circle"></i></span></label>
													<select name="user_primary_language" id="primary_language" class="form-control form-control-solid h-auto py-6 px-6 rounded-sm font-size-h6 " placeholder="Select Type" >
														@php 
														$get_primary_language = '';
														if(isset($scoutData) && !empty($scoutData->user_primary_language)){
														$get_primary_language = $scoutData->user_primary_language;
													}
													@endphp
													@if(isset($languages) && !empty($languages))
													@foreach($languages as $language)
													@php
													$selected_language = '';
													if(!empty($get_primary_language) && $language->id==$get_primary_language){
													$selected_language = 'selected';
												}
												@endphp
												<option value="{{ $language->id }}" {{ $selected_language }}>{{ $language->name }}</option> 
												@endforeach
												@endif
											</select>
										</div>
										<div class="col-lg-12 col-xl-6 my-error">
											<label class="min-height-add">What other languages do you speak fluently? <span class="cursor-pointer i-color-chnge" data-action="change" data-toggle="tooltip" title="" data-original-title="Only add languages you˙d be comfortable scouting in."><i class="fas fa-question-circle"></i></span></label>
											<select  name="user_other_languages[]" class="form-control form-control-solid h-auto py-6 px-6 rounded-sm font-size-h6"  multiple="multiple">
												@php 
												$get_other_language = '';
												if(isset($scoutData) && !empty($scoutData->user_primary_language)){
												$get_other_language = explode(",",$scoutData->user_other_languages);
											}
											@endphp
											@if(isset($languages) && !empty($languages))                                @foreach($languages as $language)
											@php
											$select_language = '';
											if(in_array($language->id,$get_other_language)){
											$select_language = 'selected';
										}
										@endphp
										<option value="{{ $language->id }}" {{ $select_language }}>{{ $language->name }}</option> 
										@endforeach
										@endif
									</select>
								</div>
							</div>
							<div class="form-group new_status value_inner current_status my-error">
								<label>Your current status: *</label>
								<label for="current_status" class="error" style="display: none;">This field is required.</label>
								<div class="d-flex flex-wrap">
									<div class="inner_form col-xl-4 col-lg-12 col-sm-12 p-0">
										<input type="radio" id="radio70" value="Student"  name="current_status" {{ $current_sta=='Student' ? 'checked':''}} /><span></span><label for="radio01-01">Student</label>
									</div>
									<div class="inner_form col-xl-4 col-lg-12 col-sm-12 p-0">
										<input type="radio" id="radio71" value="Self employed" name="current_status" {{ $current_sta=='Self employed' ? 'checked':''}}/><span></span><label for="radio01-02">Self employed</label>
									</div>
									<div class="inner_form col-xl-4 col-lg-12 col-sm-12 p-0">
										<input type="radio"   id="radio72" value="Regularly employed" name="current_status" {{ $current_sta=='Regularly employed' ? 'checked':''}}/><span></span><label for="radio01-03">Regularly employed</label>
									</div>
									<div class="inner_form col-xl-4 col-lg-12 col-sm-12 p-0">
										<input  type="radio" id="radio73" value="Unemployed" name="current_status" {{ $current_sta=='Unemployed' ? 'checked':''}}/><span></span><label for="radio01-03">Unemployed</label>
									</div>
									<div class="inner_form col-xl-4 col-lg-12 col-sm-12 p-0">
										<input   type="radio" id="radio74" value="Retired" name="current_status" {{ $current_sta=='Retired' ? 'checked':''}}/><span></span><label for="radio01-03">Retired</label>
									</div>
									<div class="inner_form col-xl-4 col-lg-12 col-sm-12 p-0">
										<input type="radio" id="radio75" value="Other" name="current_status" {{ $current_sta=='other' ? 'checked':''}}/><span></span><label for="radio01-03">Other:</label>
									</div>
								</div>
							</div>
							<div class="form-group my-error">
								<label>Describe yourself: * <span class="cursor-pointer i-color-chnge" data-action="change" data-toggle="tooltip" title="" data-original-title="*What makes you uniquely qualified to become a scout? Tell guests why you’re passionate and knowledgeable about."><i class="fas fa-question-circle"></i></span><br><b>Note: This description goes public.</b></label>
								@php 
								$about_self = '';
								if(isset($scoutData) && !empty($scoutData->about_me)){
								$about_self = $scoutData->about_me;
							}
							@endphp
							<textarea placeholder="Write Message" id="describe_youself" name="about_me" class=" form-control" rows="3">{{ $about_self ? $about_self : ''}}</textarea>
						</div>
													 
													<!-- Addtional Information section end here  -->

												</div>
												<!--end::Body-->
											</form>
											<!--end::Form-->
										</div>
									</div>
									<!--end::Content-->
								</div>
		  
        </div>
        <!-- /.modal -->
	    <div class="modal fade" id="action-scout">
	        <div class="modal-dialog">
	            <form id="action-scout-form" action="" method="post">
	                <div class="modal-content">
	                    <div class="modal-header">
	                        <h4 class="modal-title scout-title"></h4>
	                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	                        <span aria-hidden="true">&times;</span></button>
	                    </div>
	                    <div class="modal-body">
	                        <p id="cnt-modal"></p>
	                        <span id="dynInput"></span>
	                        @csrf
	                        <input type="hidden" name="status" id="status" value="{{ $scoutData->status }}">              
	                        <input type="hidden" name="scout_id" id="scout_id" value="<?php echo base64_encode($scoutData->id)?>">              
	                    </div>
	                    <div class="modal-footer justify-content-between">
	                        <button type="button" class="btn btn-secondary no-action" data-dismiss="modal">No</button>
	                        <button type="button" class="btn btn-secondary"data-dismiss="modal" onclick="formSubmit()">Yes</button>
	                    </div>
	                </div>
	            </form>
	        </div>
	    </div>
	    <!-- /.modal -->

@endsection
{{-- Scripts Section --}}
@section('scripts')
<script type="text/javascript">
	 $("select[multiple='multiple']").bsMultiSelect({
      placeholder    : 'Add other languages',
    });

    $.ajaxSetup({headers: {'X-XSRF-TOKEN': decodeURIComponent(/XSRF-Token=([^;]*)/ig.exec(document.cookie)[1])}});
    $(document).ready(function () {
    	var user = '<?php echo (!empty($scoutData->user_image))? $scoutData->user_image :''; ?>';
        $.validator.addMethod('filesize', function (value, element, param) {
            return this.optional(element) || (element.files[0].size <= param)
        }, 'File size must be less than {0}');
        $('#edit-scout-form').validate({
            
            rules: {
                user_fname: {
                    required: true
                },
                user_lname: {
                    required: true,
                },
                email:{
                    required: true,
                },
                user_address:{
                    required: true
                },
                user_mobile:{
                    required: true, 
                    remote: {
                        url: HOST_URL+"/check-user-mobile-existence",
                        type: "post"
                    }
                },
                user_image:{
                    required: function(){
                        if(user!=''){
                            return false;
                        }else{
                            return true;
                        }
                    },
                    extension: "jpg,jpeg,png",
                    filesize: 1*1024*1024,
                },
            },
            messages: {
                user_fname: {
                    required: "Please enter your first name",
                },  
                user_lname: {                 
                    required : "Please enter your last name",
                },
                user_address: {
                    required : "Please enter your address",
                },
                email: {
                    required : "Please enter your email address",
                },
                user_mobile: {
                    required : "Please enter your mobile number",
                    remote: "This mobile number already exist"
                },
                user_image:{
                    required: "Please select the image",
                    extension: "Please select jpg,jpeg and png image",
                    filesize: "File size must be less than 2MB",
                },
            }
        });
         $("#save-profile-button").click(function(){
          if($("#edit-scout-form").valid())
          {
          	let myForm = document.getElementById('edit-scout-form');
          	 myForm.submit();
          }
        });
    });
</script> 
@endsection