{{-- Extends layout --}}
@extends('layout.default')
{{-- Content --}}
@section('content')
<style type="text/css">
    .read-only{
        background-color: '#dedada' !important;
    }
</style>
@php 
    $company_na = '';
    $company_reg_no = '';
    $company_link = '';
    $legal_req = '';
    $guid_txt = '';
    $other_txt = '';
    $acc_hold_name = '';
    $bank_name = '';
    $swift_code = '';
    $iban_num = '';
    $form_action = '';

    if(isset($userData) && optional($userData->user_meta)){
        $company_na = optional($userData->user_meta)->company_name;
        $company_reg_no = optional($userData->user_meta)->company_registration_no;
        $company_link = optional($userData->user_meta)->company_web_link;
        $legal_req = optional($userData->user_meta)->legal_requirements;
        $guid_txt = optional($userData->user_meta)->guiding_text;
        $other_txt = optional($userData->user_meta)->other_text;
    }

    if(isset($userData) && optional($userData->user_bank_detail)){

        $acc_hold_name = optional($userData->user_bank_detail)->account_holder_name;

        $bank_name = optional($userData->user_bank_detail)->bank_name;

        $swift_code = optional($userData->user_bank_detail)->swift_code;

        $iban_num = optional($userData->user_bank_detail)->iban_number;
    }

    if(Auth::user()->user_role==3){
        $form_action = route('scout-create-host');
    }
    elseif(Auth::user()->user_role==4){
        $form_action = route('host-profile-update');
    }
@endphp
<div class="row">
    <div class="col-lg-12">
        <div class="card card-custom gutter-b example example-compact">
            <div class="card-header">
				<div class="card-title w-100">
					<span class="card-icon"><i class="fas fa-plus-circle"></i></span> 
					<div class="list_sec">
					<h3 class="card-label">{{ isset($userData) ? 'Edit Host' : 'Add Host' }}</h3>
					</div>
				</div>
                <div class="card-toolbar" style="display: none;">
                    <div class="example-tools justify-content-center">
                        <span class="example-toggle" data-toggle="tooltip" title="View code"></span>
                        <span class="example-copy" data-toggle="tooltip" title="Copy code"></span>
                    </div>
                </div>
            </div>
            <!--begin::Form-->
            <form id="edit-scout-form" class="form" method="post" action="{{ $form_action }}" enctype="multipart/form-data">
                @csrf
                <div class="accordion accordion-solid accordion-toggle-plus" id="accordionExample6">
                    <div class="card">
                        <div class="card-header" id="headingOne6">
                            <div class="card-title" id="basic_detail" data-toggle="collapse" data-target="#basic-detail">
                            <i class="far fa-list-alt"></i>Basic Details</div>
                        </div>
                        <div id="basic-detail" class="collapse show" data-parent="#accordionExample6">
                            <div class="card-body">
                                <div class="form-group row">
                                    <input type="hidden" name="user_id" value="{{@$userData->id}}" id="existing_id">
                                    <div class="col-lg-4 my-error">
                                        <label>Host's first name</label>
                                        <input type="text" name="user_fname" value="{{ isset($userData) ? $userData->user_fname : '' }}" class="form-control" placeholder="Enter Host's first name" />
                                    </div>
                                    <div class="col-lg-4 my-error">
                                        <label>Host's last name</label>
                                        <input type="text" name="user_lname" value="{{ isset($userData) ? $userData->user_lname : '' }}" class="form-control" placeholder="Enter Host's last name" />
                                    </div>
                                    <div class="col-lg-4 my-error">
                                        <label>Host's most commonly used e-mail</label>
                                        <input type="email" name="email" {{ isset($userData) ? "readonly" : "" }} value="{{ isset($userData) ? $userData->email : '' }}" class="form-control {{ isset($userData) ? 'read-only' : '' }}" placeholder="Enter Host's email address" />
                                    </div>
                                </div>
                                <div class="form-group row">                        
                                    <div class="col-lg-4 my-error">
                                        <label>Host's home adress</label>
                                        <input type="text" name="user_address" value="{{ isset($userData) ? $userData->user_address : '' }}" class="form-control" placeholder="Enter Host's address" />
                                    </div>
                                    <div class="col-lg-4 my-error">
                                        <label>Host's mobile phone</label>
                                        <input type="tel" id="user_mobile" name="user_mobile" value="{{ isset($userData) ? $userData->user_mobile : '' }}" class="form-control" placeholder="Enter Host's mobile number" />
                                    </div>
                                    <div class="col-lg-4 my-error">
                                        <label>Host's Image:</label>
                                        <input type="file" name="user_image" value="" class="form-control" placeholder="Browse Image" title="Upload Image" />
                                        <input type="hidden" name="old_user_img" id="old_user_img" value="{{ isset($userData) ? $userData->user_image : '' }}">
                                        
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-lg-6 my-error">
                                        <label>Full and official name of the host's Company/Organisation*</label>

                                        <input type="text" name="company_name" value="{{ $company_na }}" class="form-control" placeholder="Enter Host's Company Name" />
                                    </div>
                                    <div class="col-lg-6 my-error">
                                        <label>Company/Organisation registration number*</label>
                                        <input type="text" name="company_registration_no" value="{{ $company_reg_no }}" class="form-control" placeholder="Enter Host's Company Registration Number" />
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-lg-6 my-error">
                                        <label>Company/Organisation website (insert a link if any)</label>
                                        <input type="text" name="company_web_link"  value="{{ $company_link }}" class="form-control" placeholder="Enter Host's Company Website Link" />
                                    </div>
                                    <div class="col-lg-6 my-error">
                                        <label>Languages Spoken</label>
                                        <select  name="user_other_languages[]" id="primary_language" class="form-control form-control-solid h-auto py-6 px-6 rounded-sm font-size-h6 exp_multi"  multiple="multiple">
                                            @php 
                                                $get_other_language = [];
                                                if(isset($userData) && !empty($userData->user_other_languages)){
                                                    $get_other_language = explode(",",$userData->user_other_languages);
                                                }
                                            @endphp 
                                            @if(isset($languages) && !empty($languages))                         @foreach($languages as $language)
                                            @php
                                                $select_language = '';
                                                if(in_array($language->short,$get_other_language)){
                                                    $select_language = 'selected';
                                                }
                                            @endphp
                                            <option value="{{ $language->short }}" {{ $select_language }}>{{ $language->name }}</option> 
                                            @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group new_status value_inner current_status my-error">
                                    <label>Tick the options below, if this host meets the listed legal requirements for
                                    conducting an experience. Note that in some cases the host must meet these requirements (food preparation and serving, guiding, etc.) - *</label>
                                    <label for="legal_requirements" class="error" style="display: none;">This field is required.</label>
                                    <div class="d-flex flex-wrap">
                                        @php 
                                            $legal_haccp = '';
                                            $legal_guid = '';
                                            $legal_oth = '';
                                            $legal_arr = [];

                                            if(!empty($legal_req)){
                                                $legal_arr = explode(",",$legal_req);
                                                if(in_array('1',$legal_arr)){
                                                    $legal_haccp = 'checked';
                                                }
                                                if (in_array('2',$legal_arr)) {
                                                    $legal_guid = 'checked';
                                                }
                                                if (in_array('3', $legal_arr)) {
                                                    $legal_oth = 'checked';
                                                }
                                            }
                                        @endphp 
                                        <div class="inner_form col-md-12 col-sm-12 p-0 haccp-input">
                                            <input type="checkbox" value=1  name="legal_requirements[]" {{ $legal_haccp }}/><label>HACCP - Hazard analysis and critical control points</label>
                                        </div>
                                        <div class="inner_form col-md-12 col-sm-12 p-0 haccp-input">
                                            <input type="checkbox" value=2 name="legal_requirements[]" id="legal_guiding" {{ $legal_guid}}/><label>Guding licence</label>
                                        </div>
                                        <div class="inner_form col-md-12 col-sm-12 p-0 haccp-input">
                                            <input type="checkbox" value=3 name="legal_requirements[]" id="legal_other" {{ $legal_oth}}/><label>Other</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row legal_req">
                                    <div class="col-lg-6 my-error" style="{{ $legal_guid ? '' : 'display: none;' }}" id="guiding_txt">
                                        <label>Guiding License</label>
                                        <input type="text" name="guiding_text" value="{{ $guid_txt }}" class="form-control" placeholder="Enter Guiding License" id="guid_input"/>
                                    </div>
                                    <div class="col-lg-6 my-error" style="{{ $legal_oth ? '' : 'display: none;'}}" id="other_txt">
                                        <label>Other</label>
                                        <input type="text" name="other_text" value="{{ $other_txt }}" class="form-control" placeholder="Enter Other" id="othr_input"/>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-lg-6 ">
                                        <p>
                                            <a class="btn btn-primary next" data-next="second_step">Next</a>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingTwo6">
                            <div class="card-title collapsed" id="bank_detail" data-toggle="" data-target="#bank-information">
                            <i class="far fa-plus-square"></i>Bank Details</div>
                        </div>
                        <div id="bank-information" class="collapse" data-parent="#accordionExample6">
                            <div class="card-body">
                                <div class="form-group row">
                                    <div class="col-lg-4 my-error">
                                        <label>Account Holder</label>
                                        <input type="text" name="account_holder" value="{{ $acc_hold_name }}" class="form-control" placeholder="Enter Account Holder name" />
                                    </div>
                                    <div class="col-lg-4 my-error">
                                        <label>Name of the bank</label>
                                        <input type="text" name="bank_name" value="{{ $bank_name }}" class="form-control" placeholder="Enter Bank Name" />
                                    </div>
                                    <div class="col-lg-4 my-error">
                                        <label>SWIFT/BIC ID</label>
                                        <input type="text" name="swift_code" value="{{ $swift_code }}" class="form-control" placeholder="Enter SWIFT/BIC ID" />
                                    </div>
                                </div>
                                <div class="form-group row">                        
                                    <div class="col-lg-4 my-error">
                                        <label>IBAN</label>
                                        <input type="text" name="iban" value="{{ $iban_num }}" class="form-control" placeholder="Enter IBAN" />
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer">
                                <div class="row">
                                    <div class="col-lg-6">
                                        <button type="submit" class="btn btn-primary mr-2" id="sub_btn">Save</button>
                                        <a class="btn btn-secondary previous" data-prev="first_step" >Previous</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
            <!--end::Form-->
        </div>
    </div>
</div>
@endsection
{{-- Scripts Section --}}
@section('scripts')
<script type="text/javascript">

    $("select[multiple='multiple']").bsMultiSelect({
        placeholder    : 'Add a language',
    });
    
    $.ajaxSetup({headers: {'X-XSRF-TOKEN': decodeURIComponent(/XSRF-Token=([^;]*)/ig.exec(document.cookie)[1])}});

    $(document).ready(function () {

        $(".next").click(function(){

            $.validator.addMethod('filesize', function (value, element, param) {
                return this.optional(element) || (element.files[0].size <= param)
            }, 'File size must be less than {0}');

            $.validator.addMethod("alphanumeric", function(value, element) {
                return this.optional(element) || /^[A-Za-z0-9]+$/.test(value);
            }, "Please enter Letters and numbers only");

            var form = $('#edit-scout-form');
            form.validate({
                submitHandler: function(form) {
                  // do other things for a valid form
                  form.submit();
                },
                rules: {
                    user_fname: {
                        required: true
                    },
                    user_lname: {
                        required: true,
                    },
                    email:{
                        required: true,
                        remote: {
                            url: HOST_URL+"/check-unique-Email",
                            type: "post",
                            data: {
                              id: function() {
                                return $( "#existing_id" ).val();
                              }
                            }
                        }
                    },
                    user_address:{
                        required: true
                    },
                    company_name:{
                        required:true
                    },
                    company_registration_no:{
                        required:true
                    },
                    'user_other_languages[]':{
                        required:true
                    },
                    'legal_requirements[]':{
                        required:true
                    },
                    guiding_text:{
                        required: function(){
                            if($("#legal_guiding").is(':checked')){
                                return true;
                            }else{
                                return false;
                            }
                        },
                    },
                    other_text:{
                        required: function(){
                            if($("#legal_other").is(':checked')){
                                return true;
                            }else{
                                return false;
                            }
                        },
                    },
                    swift_code:{
                        minlength:8,
                        maxlength:11,
                        alphanumeric:true
                    },
                    iban:{
                        maxlength:50,
                        alphanumeric:true
                    },
                    user_mobile:{
                        required: true,
                        digits: true,
                        remote: {
                            url: HOST_URL+"/check-unique-Mobile",
                            type: "post",
                            data: {
                              id: function() {
                                return $( "#existing_id" ).val();
                              }
                            }
                        }
                    },
                    user_image:{
                        required: function(){
                            if($("#old_user_img").val()){
                                return false;
                            }else{
                                return true;
                            }
                        },
                        extension: "jpg,jpeg,png",
                        filesize: 2*1024*1024,
                    },
                },
                messages: {
                    user_fname: {
                        required: "Please enter your first name",
                    },  
                    user_lname: {                 
                        required : "Please enter your last name",
                    },
                    user_address: {
                        required : "Please enter your address",
                    },
                    email: {
                        required : "Please enter your email address",
                        remote: "this email id already exists"
                    },
                    company_name: {
                        required : "Please enter your company name",
                    },
                    company_registration_no: {
                        required : "Please enter your comany registration number",
                    },
                    swift_code:{
                        minlength: "Please enter minimum 8 characters",
                        maxlength: "Please enter maximum 11 characters"
                    },
                    iban:{
                        maxlength: "You can not add more than 50 characters"
                    },
                    'user_other_languages[]': {
                        required : "Please select a language",
                    },
                    'legal_requirements[]': {
                        required : "Please select a checkbox",
                    },
                    user_mobile: {
                        required : "Please enter your mobile number",
                        remote: "This mobile number already exist"
                    },
                    user_image:{
                        required: "Please select the image",
                        extension: "Please select jpg,jpeg and png image",
                        filesize: "File size must be less than 2MB",
                    },
                },
                errorElement: 'span', 
                ignore: ':hidden:not(".exp_multi")',          
                errorPlacement: function (error, element) {
                  error.addClass('invalid-feedback');
                  element.closest('.my-error').append(error);
                },
                highlight: function (element, errorClass, validClass) {
                  $(element).addClass('is-invalid');
                },
                unhighlight: function (element, errorClass, validClass) {
                  $(element).removeClass('is-invalid');
                }
            });

            if (form.valid() === true){
                if($('#basic-detail').hasClass('show')) {
                    current_fs = $('#basic_detail');
                    next_fs = $('#bank_detail');
                }
                next_fs.attr("data-toggle", "collapse");
                next_fs.trigger('click');
                current_fs.attr("data-toggle", "collapse");
                current_fs.trigger('click');
            }
        });
        
/*        $('.next').click(function(){
            if($('#basic-detail').hasClass('show')) {
                current_fs = $('#basic_detail');
                next_fs = $('#bank_detail');
            }
            next_fs.attr("data-toggle", "collapse");
            next_fs.trigger('click');
            current_fs.attr("data-toggle", "collapse");
            current_fs.trigger('click');
        });*/
        
        $('.previous').click(function(){
            if($('#bank-detail').hasClass('show')) {
                current_fs = $('#basic_detail');
            }
            current_fs.attr("data-toggle", "collapse");
            current_fs.trigger('click');
        });

        $('#legal_guiding').on('click',function(){

            var checked = $(this).is(':checked');
            var txt_selector = $('#guiding_txt');

            $('#guid_input').val('');

            if(checked){
                $(txt_selector).show();
            }
            else{
                $(txt_selector).hide();
            }
        });

        $('#legal_other').on('click',function(){

            var checked = $(this).is(':checked');
            var txt_selector = $('#other_txt');

            $('#othr_input').val('');

            if(checked){
                $(txt_selector).show();
            }
            else{
                $(txt_selector).hide();
            }
        });

    });
</script>
@endsection