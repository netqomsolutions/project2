{{-- Extends layout --}}
@extends('layout.default')
{{-- Content --}}
@section('content')
<style>
	.toggle.ios, .toggle-on.ios, .toggle-off.ios { border-radius: 20rem; }
	.toggle.ios .toggle-handle { border-radius: 20rem; }
	div#action-scout {
	    align-items: center;
	    justify-content: center;
	}
	#social-activity-form .form-group.row .icon-inner {
    display: block;
    margin-top: 13px; 
    cursor: pointer; 
}
.fa-facebook-f:hover{
color: #3b5998; 
}.fa-instagram:hover{
color: #c434a9; 
}.fa-twitter:hover{
color: #50abf1; 
}
.fa-linkedin-in:hover{
color: #0077b5; 
}.fa-pinterest:hover{
color: #c01b26; 
}
.icon-inner i {
    font-size: 22px;}
    span.step-done, span.step-done * {
    color: #93b146;
}
span.step-done {
    position: relative;
    z-index: 99;
    background-color: #f3f6f9;
    left: 2px;
}
</style>
@php
$org=(!empty($response_array['org']) ? $response_array['org']:false);   
//$acc=(!empty($response_array['acc']) ? $response_array['acc']:false);   
$businessPremises=$response_array['premises'];


@endphp
 
 @if(session()->has('success'))
            <div class="alert alert-success alluser-page-suc" style="text-align: left;float: left;width: 100%;">
                {{ session()->get('success') }}
            </div>
            <script type="text/javascript">setTimeout(function(){ $('.alluser-page-suc').fadeOut(); }, 3000);</script>
        @elseif(session()->has('failure'))
            <div class="alert alert-danger alluser-page-suc" style="text-align: left; left;float: left;width: 100%;">
                {{ session()->get('failure') }}
            </div>
            <script type="text/javascript">setTimeout(function(){ $('.alluser-page-suc').fadeOut(); }, 3000);</script>
        @endif
        
    <div class="main-body">

          <!--  <div class="user-profile-buttons">
            <a href="{{ route('user-profile-edit') }}" class="navi-item">Update Profile</a>
            <a href="{{ route('user-change-password-form') }}" class="navi-item">Change Password</a>
          </div> -->
		 	  
	<div class="d-flex flex-row w-100">
		@include('./profile-sidebar')	
        <!--begin::Content-->
		<div class="flex-row-fluid ml-lg-8">
			<!--begin::Card-->
			<!-- card-sticky class  -->
			<div class="card card-custom  card-sticky card-stretch" id="">
				<!--begin::Header-->
				<div class="card-header py-3">
					<div class="card-title align-items-start flex-column">
						<h3 class="card-label font-weight-bolder text-dark">Fiscalization</h3>
						<span class="text-muted font-weight-bold font-size-sm mt-1">Add/Update your Fiscalization Details </span>
					</div>
					<div class="card-toolbar">
						<!--button type="button" id="save-changes-button" class="btn btn-success mr-2 green-bg-btn">Save Changes</button-->
						<!--button type="reset" class="btn btn-secondary">Cancel</button-->
					</div>
				</div>
				<!--end::Header-->
				<!--begin::Form-->
				<div class="d-flex flex-column-fluid mt-10" id="guru">
        <div class=" container ">
            <link href="https://cdnjs.cloudflare.com/ajax/libs/cropperjs/1.5.11/cropper.css" rel="stylesheet">
<div class="row">
    <div class="col-lg-12">
       
       <p> The following setup guide was created to help you set up fiscal verification of invoices for Slovenia based on Slovenian Tax office (FURS) requirements.</p>

<p><b>IMPORTANT:</b> We strongly recommend that users review the requirements and discuss fiscalization use with accountant prior to setup as there are some rules that have to be followed to ensure proper operation, we can only provide a certain level of operation technically, the responsibility of correct data input and final liability connected to issuing invoices is still on the end users.</p>
        <div class="card card-custom gutter-b example example-compact fiscalization-main">
            <div class="card-header">
                <div class="card-title w-100">
                    <span class="card-icon"><i class="fas fa-plus-circle"></i></span> 
                    <div class="list_sec">
                    <h3 class="card-label">Fiscalization</h3>
                    </div>
                     
                </div>
                <div class="alert alert-danger alluser-page-suc ajax-error" style="text-align: left; left;float: left;width: 100%; display: none;">
            </div>
                <div class="card-toolbar" style="display: none;">
                    <div class="example-tools justify-content-center">
                        <span class="example-toggle" data-toggle="tooltip" title="" data-original-title="View code"></span>
                        <span class="example-copy" data-toggle="tooltip" title="" data-original-title="Copy code"></span>
                    </div>
                </div>
            </div>
           
           <div class="card-body">

            
                <!--begin::Accordion-->
                <div class="accordion accordion-solid accordion-toggle-plus" id="accordionExample6">
                    <div class="card">
                        <div class="card-header" id="headingOne6">
                            <div class="card-title collapsed d-flex align-items-center justify-content-between flex-wrap" id="basic_detail" data-toggle="collapse" data-target="#basic-deatil" aria-expanded="false">
                            <span><i class="fas fa-database"></i>Basic data</span>
                            <!--span class="step-done">Done <i class="fas fa-check-circle fa-lg"></i></span-->
                        </div>
                        </div>
                        <div id="basic-deatil" class="collapse" data-parent="#accordionExample6" style="">
                             <!--begin::Form--> 
                <form class="form" id="fsclzn-basic-form" method="post" action="{{ route('fiscalization-request') }}" enctype="multipart/form-data">
                     @csrf 
                       <input type="hidden" name="id" id="id" value="{{(!empty($org)) ? $org->id : ''}}">
                       <!--input type="hidden" name="acc_id" id="acc_id" value="{{(!empty($acc)) ? $acc->id : ''}}"-->
                            <div class="card-body">
                                <div class="form-group row">                                    
                                    <div class="col-lg-12 mt-5">
                                      <p class="basic fiscalization-heading">Organization data </p>
                                        <div class="row">
                                            <div class="form-group col-xl-6 mb-0 pl-0 pr-0">
                                                <label class="col-xl-12 col-lg-12 col-form-label">Name</label>
                                                <div class="col-lg-12 col-xl-12">
                                                    <input class="form-control form-control-lg form-control-solid" name="name" type="text" value="{{(!empty($org)) ? $org->name : ''}}" placeholder="Enter Org name">
                                                </div>
                                            </div>
                                            <div class="form-group col-xl-6 mb-0 pl-0 pr-0">
                                                <label class="col-xl-12 col-lg-12 col-form-label">Address</label>
                                                <div class="col-lg-12 col-xl-12">
                                                    <input class="form-control form-control-lg form-control-solid" name="address" type="text" value="{{(!empty($org)) ? $org->address : ''}}" placeholder="Enter Org Address">
                                                </div>
                                            </div>

                                            <div class="form-group col-xl-6 mb-0 pl-0 pr-0">
                                                <label class="col-xl-12 col-lg-12 col-form-label">Tax number</label>
                                                <div class="col-lg-12 col-xl-12">
                                                    <input class="form-control form-control-lg form-control-solid" name="taxNumber" type="text"    pattern="[0-9]{8}"  maxlength="8"  title="Eight numbers tax number" value="{{(!empty($org) && isset($org->taxNumber)) ? $org->taxNumber : ''}}" placeholder="Enter Tax number">
                                                </div>
                                            </div>
                                            <div class="form-group col-xl-4 mb-0 pl-0 pr-0">                                                
                                                <div class="col-lg-12 col-xl-12">
                                                    <div class="custom-control custom-switch">
                                                      <input type="checkbox" class="custom-control-input" {{(!empty($org) && isset($org->taxSubject) && $org->taxSubject ==1) ? 'checked': ''}} name='taxSubject' id="customSwitch1">
                                                      <label class="custom-control-label" for="customSwitch1">Tax subject</label>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group col-xl-6 mb-0 pl-0 pr-0">
                                                <label class="col-xl-12 col-lg-12 col-form-label">Post code</label>
                                                <div class="col-lg-12 col-xl-12">
                                                    <input class="form-control form-control-lg form-control-solid"    maxlength="4"  name="zip" type="text" value="{{(!empty($org) && isset($org->zip)) ? $org->zip : ''}}" placeholder="Enter Post code">
                                                </div>
                                            </div>
                                            <div class="form-group col-xl-6 mb-0 pl-0 pr-0">
                                                <label class="col-xl-12 col-lg-12 col-form-label">City</label>
                                                <div class="col-lg-12 col-xl-12">
                                                    <input  id='basicCity' class="form-control form-control-lg form-control-solid" name="city" type="text" value="{{(!empty($org)  && isset($org->city)) ? $org->city : ''}}" placeholder="Enter city">
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <!--div class="col-lg-12 mt-5">
                                        <p class="basic mb-5 mt-5">Numbering type</p>
                                        <div class="row">
                                            <div class="form-group col-lg-12 mb-0">
                                            <select class="form-control changeExpType" name="value" id="exp_price_type">
                                    <option value="null">Choose</option>
                                    <option value="C">By business premise </option> 
                                    <option value="B">By electronic device </option>
                                </select>
                                            </div>
                                        </div>
                                    </div-->

                                    <!--div class="col-lg-12 mt-10">
                                        <p class="basic">Representative data </p>
                                        <div class="row">
                                            <div class="form-group col-xl-6 mb-0 pl-0 pr-0">
                                                <label class="col-xl-12 col-lg-12 col-form-label">First name</label>
                                                <div class="col-lg-12 col-xl-12">
                                                    <input class="form-control form-control-lg form-control-solid" name="Account[firstname]" type="text" value="" id='' placeholder="Enter Host's first name">
                                                </div>
                                            </div>
                                            <div class="form-group col-xl-6 mb-0 pl-0 pr-0">
                                                <label class="col-xl-12 col-lg-12 col-form-label">Last name</label>
                                                <div class="col-lg-12 col-xl-12">
                                                    <input class="form-control form-control-lg form-control-solid" name="Account[lastname]" type="text" value="" placeholder="Enter Host's last name">
                                                </div>
                                            </div>
                                            <div class="form-group col-xl-6 mb-0 pl-0 pr-0">
                                                <label class="col-xl-12 col-lg-12 col-form-label">Label</label>
                                                <div class="col-lg-12 col-xl-12">
                                                    <input class="form-control form-control-lg form-control-solid" name="Account[label]" type="text" value="" placeholder="Enter Label">
                                                </div>
                                            </div>
                                            <div class="form-group col-xl-6 mb-0 pl-0 pr-0">
                                                <label class="col-xl-12 col-lg-12 col-form-label">Tax number</label>
                                                <div class="col-lg-12 col-xl-12">
                                                    <input class="form-control form-control-lg form-control-solid"    pattern="[0-9]{8}"  maxlength="8"  title="Eight numbers tax number"  name="Account[taxNumber]" type="text" value="" placeholder="Enter tax number">
                                                </div>
                                            </div>
                                        </div>
                                    </div-->


                       
                                </div>
                            <div class="form-group row">
                            <div class="col-lg-12 ">
                                <p class="text-right">
                                    <a class="btn btn-primary   basic_detail_fslz" data-next="second_step">Next</a>
                                </p>
                            </div>
                        </div>
                    </form>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                    <div class="card-header" id="headingTwo6">
                           <div class="card-title collapsed d-flex align-items-center justify-content-between flex-wrap" id="additional_detail"   data-toggle="{{(!empty($response_array['org']) && isset($response_array['org']->taxNumber) && !empty($response_array['org']->taxNumber))? 'collapse' : ''}}"   aria-expanded="false"  data-target="#additional-information">
                            <span><i class="fas fa-database"></i>Certificate</span>
                            @if($response_array['hasCertificate']==1)
                            <span class="step-done  certificate-done">Done <i class="fas fa-check-circle fa-lg"></i></span>
                            @else
                            <span class="step-done certificate-done" style="display:none">Done <i class="fas fa-check-circle fa-lg"></i></span>
                            @endif
                        </div>
 
                    </div>
                    <div id="additional-information" class="collapse" data-parent="#accordionExample6">
                        <div class="card-body">
            <form class="form" id="fsclzn-certificate-form" method="post" action="{{ route('fiscalization-request') }}" enctype="multipart/form-data">
                     @csrf 
                                    <input type="hidden" name="id" id="id" value="{{(!empty($org)) ? $org->id : ''}}">
                            <div class="form-group">
                            <div class="col-lg-12 mt-5">
	<div class="row certificate-sec">
		<div class="form-group col-xl-12- pl-0 pr-0 mb-0">
		    <p>One of the steps required to enable fiscal verification is to obtain a certificate issued by FURS. You can obtain a certificate on eDavki platform.</p>
		</div>
		<div class="form-group col-xl-12 pl-0 pr-0">
			<label class="col-xl-3 col-lg-3 col-form-label pl-0">Certified </label>
			<div class="col-lg-9 col-xl-9">
            <div class="input-group mb-3">
  <div class="input-group-prepend">
  </div>
  <div class="custom-file">
    <input type="file" class="" name='certificate' id="certificate-file">
    <span class="custom-file-label"><i class="fa fa-upload" aria-hidden="true"></i> <b id="certificate-file-name" class="font-weight-normal">Choose file</b></span>
  </div>
  <label id="certificate-file-error" class="error w-100" for="certificate-file" style="display: none"></label>
</div>
			</div>
		</div>
		<div class="form-group col-xl-12 pl-0 pr-0 mb-0 pt-5">
		    <p>If you are not a user of eDavki and you dont plan on signing up there, it is also possible to authorize another person (for example, your accountant) to do it for you. </p>
		</div>
		<div class="form-group col-xl-12 pl-0 pr-0">
			<label class="col-xl-3 col-lg-3 col-form-label pl-0">Passphrase </label>
			<div class="col-lg-9 col-xl-9">
			    <input class="form-control form-control-lg form-control-solid" name="passphrase" type="text" value="" placeholder="">
			</div>
		</div>
	</div>
</div>                          
                              
                        
                            </div>
                                <div class="form-group row">
                              <div class="col-lg-12 ">
                                <p class="text-right">
                                    <a class="btn btn-primary certificate_detail_fslz" data-next="second_step">Next</a>
                                </p>
                            </div>
                        </div>
                            </div>
                            </form>
                        </div>
                    </div>
                    
                    <div class="card">
                    <div class="card-header" id="headingTwo6">
                          <div class="card-title collapsed d-flex align-items-center justify-content-between flex-wrap" id="business-premises-toggle" data-toggle="{{(!empty($response_array['org']) && !empty($response_array['hasCertificate']))? 'collapse' : ''}}"   aria-expanded="true" data-target="#business-premises">
                            <span><i class="fas fa-database"></i>Business premises</span>
                            @if(!empty($businessPremises))
                            <span class="step-done business-premises-done">Done <i class="fas fa-check-circle fa-lg"></i></span>
                            @else
                            <span class="step-done business-premises-done" style="display:none">Done <i class="fas fa-check-circle fa-lg"></i></span>
                            @endif 
                        </div> 
                    </div>
                    <div id="business-premises" class="collapse" data-parent="#accordionExample6">
                        <div class="card-body">
                            <div class="form-group">
                            <div class="col-lg-12 mt-5">
	<div class="row certificate-sec">
		<div class="form-group col-xl-12 pl-0 pr-0 mb-0">
			<div class="col-xl-6 col-lg-6">
                <p>Business premise is any moveable or non-movable space where you continuously, occasionally or temporarily issue receipts for the supply of goods or services and receive payment in cash. The business premise should have unique id and should be defined in detail at least to the address level. </p>
          </div>
          <div class="col-xl-6 col-lg-6">
                <p>Business premise is any moveable or non-movable space where you continuously, occasionally or temporarily issue receipts for the supply of goods or services and receive payment in cash. The business premise should have unique id and should be defined in detail at least to the address level. </p>
          </div>
			</div>
			<div class="form-group col-xl-12 pl-0 pr-0 mb-0">
        <button type="button" class="btn btn-sm float-right green-bg action-add-edit-btn" list-id=" " act-name="add-business" data-toggle="modal" data-target="#add-business-modal" data-id="0"><span class="plus-icon"><i class="fa fa-plus-circle" aria-hidden="true"></i>
</span> Add Business premise </button>
		</div>
		</div>
		
	</div>
                         
                              
                        
                            </div>
                                <!-- <div class="form-group row">
                              <div class="col-lg-12 ">
                                <p class="text-right">
                                    <a class="btn btn-primary next" data-next="second_step">Next</a>
                                </p>
                            </div>
                        </div> -->
                            </div>
                            
                        </div>
                    </div>
                    <div class="card">
                    <div class="card-header" id="headingTwo6">
                            <div class="card-title collapsed d-flex align-items-center justify-content-between flex-wrap"  id="electronic-devicess"    data-toggle="{{(!empty($response_array['org']) && !empty($response_array['hasCertificate']) && !empty($response_array['premises']))? 'collapse' : ''}}"   aria-expanded="false"  data-target="#electronic-devices">
                            <span><i class="fas fa-database"></i>Electronic devices</span>
                             @if(!empty($response_array['devices']))
                            <span class="step-done electronic-done">Done <i class="fas fa-check-circle fa-lg"></i></span>
                            @else
                            <span class="step-done electronic-done" style="display:none">Done <i class="fas fa-check-circle fa-lg"></i></span>
                            @endif   
                        </div> 
                         
                    </div>
                    <div id="electronic-devices" class="collapse" data-parent="#accordionExample6">
                        <div class="card-body electronic_col">
                            <div class="form-group">
                            <div class="col-lg-12 mt-5">
	<div class="row certificate-sec">
		<div class="form-group col-xl-12- pl-0 pr-0 mb-0">
		    <p>One of the steps required to enable fiscal verification is to obtain a certificate issued by FURS. You can obtain a certificate on eDavki platform.</p>
		</div>
		<div class="form-group col-xl-12- pl-0 pr-0 mb-0">
        <div class="form-group col-xl-12 pl-0 pr-0 mb-0">
        <button type="button" class="btn btn-sm float-right green-bg action-add-edit-btn" list-id=" " act-name="add-testimonials" data-toggle="modal" data-target="#add-device-modal" data-id="0"><span class="plus-icon"><i class="fa fa-plus-circle" aria-hidden="true"></i>
</span> Add New</button> 

		</div>
		</div>
	</div>
</div>                          
                              
                        
                            </div>
                               <!--  <div class="form-group row">
                              <div class="col-lg-12 ">
                                <p class="text-right">
                                    <a class="btn btn-primary next" data-next="second_step">Next</a>
                                </p>
                            </div>
                        </div> -->
                            </div>
                            
                        </div>
                    </div>
                 
                
                    </div>
                <!--end::Accordion-->
            </form>
             <!--end::Form-->                                 
            </div>
             
        </div>
    </div>
</div>
 
 
        </div>
    </div>
				<!--end::Form-->
			</div>
		</div>
									<!--end::Content-->
	  </div>

<div class="modal fade   bd-example-modal-sm" id="add-device-modal" style="display: none;" aria-hidden="true">
			        <div class="modal-dialog">
			  <form class="form" id="fsclzn-devices-form" method="post"   enctype="multipart/form-data">
                     @csrf 
                  <input type="hidden" name="id" id="id" value="{{(!empty($org)) ? $org->id : ''}}">
			                <div class="modal-content">
			                    <div class="modal-header">
			                        <h4 class="modal-title scout-title">Add device</h4>
			                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
			                        <span aria-hidden="true">×</span></button>
			                    </div>
			                    <div class="modal-body">
			                        			                        
			                        <div class="form-group">
			                        <label>Belongs to premise:</label>
			                        <select class="form-control select-host-option" id='SelectBusinessPremiseId' name="businessPremiseId">
                                            <option value="">Choose</option>
                                            @foreach($businessPremises as $key=>$premises )
                                                <option value="{{$key}}">{{$premises}}</option>
                                                @endforeach
                                            </select>
			                    	</div>
			                    	<div class="form-group">
			                    	 <label>Device ID</label>
			                    	<input class="form-control" type="text" name="electronicDeviceId" id="electronicDeviceId" value="" placeholder="D1">
                                    </div> 
			                    </div>
			                    <div class="modal-footer justify-content-between">
			                        <button type="button" class="btn btn-outline-light" data-dismiss="modal">Cancel</button>
			                        <a class="btn btn-primary   certificate_devices_fslz pink-bg-btn" id="certificate_devices_fslz" data-next="second_step">Submit</a> 
			                    </div>
			                </div>
			            </form>
			        </div>
                </div>





                <div class="modal fade add-scout-cls" id="add-business-modal" style="display: none;" aria-hidden="true">
			     <div class="modal-dialog">
			      <form class="form" id="fsclzn-business-form" method="post" a enctype="multipart/form-data">
                     @csrf 
                  <input type="hidden" name="id" id="id" value="{{(!empty($org)) ? $org->id : ''}}">
			                <div class="modal-content">
			                    <div class="modal-header">
			                        <h4 class="modal-title scout-title">Add Business premise</h4>
			                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
			                        <span aria-hidden="true">×</span></button>
			                    </div>
			                    <div class="modal-body">
			                        	
                                    <div class="row">
                                    <div class="col-xl-6">		                        
			                        <div class="form-group">
			                        <label>Premise type</label>
			                        <select class="form-control select-host-option" name="type">
                                                <option value="realestate">Realestate</option>
                                                <option value="movable">Movable</option>
                                    </select>
                                    </div>
                                    </div>
                                    <div class="col-xl-6">
			                    	<div class="form-group">
			                    	 <label>Premise ID</label>
			                    	<input class="form-control" type="text" name="businessPremiseId" id="businessPremiseId" value="P1" placeholder="P1">
                                    </div>
                                     </div>
                        </div>
                        <div class="row">
                                    <div class="col-xl-6">
                                    <div class="form-group">
                                     <label>Street</label>
                                     <input class="form-control" type="text" name="street" id="street" value="{{(!empty($org)) ? $org->address : ''}}" placeholder="Street">
                                    </div>
                                     </div>
                                     <div class="col-xl-3">
                                    <div class="form-group">
                                     <label>Num</label>
                                     <input type="number" name="houseNumber" class="form-control" id="houseNumber" value="" min="1" placeholder="Write in numbers eg. 3">
                                    </div>
                        </div>
                                    <div class="col-xl-3">
                                    <div class="form-group">
                                     <label>Additional</label>
                                     <input class="form-control" type="text" name="houseNumberAdditional" id="houseNumberAdditional" value="" placeholder="B">
                                    </div>
                                    </div>
                        </div>
                        <div class="row">
                                    
                                    <div class="col-xl-6">
                                    <div class="form-group">
                                     <label>City</label>
                                     <input class="form-control" type="text" name="city" id="premiseCity" value="{{(!empty($org)  && isset($org->city)) ? $org->city : ''}}" placeholder="Ljubljana">
                                    </div>
                                    </div>
                                    <div class="col-xl-3">
                                    <div class="form-group">
                                     <label>Post</label>
                                     <input type="number" name="postalCode" class="form-control" id="postalCode" value="{{(!empty($org)  && isset($org->zip)) ? $org->zip : ''}}" min="1" placeholder="1000">
                                    </div>
                                     </div>
                                     <div class="col-xl-3">
                                    <div class="form-group">
                                     <label>Community</label>
                                     <input class="form-control" type="text" name="community" id="community" value="" placeholder="Ljubljana">
                                    </div>
                                    </div>
                        </div>
                        <div class="row">
                                    
                                    <div class="col-xl-6">
                                    <div class="form-group">
                                     <label>Cadastral number</label>
                                     <input class="form-control" type="text" name="cadastralNumber" maxlength="4" id="cadastralNumber" value="" placeholder="Codastral no.">
                                    </div>
                                    </div>
                                    <div class="col-xl-6">
                                    <div class="form-group">
                                     <label>Building number</label>
                                     <input class="form-control" type="text" name="buildingNumber" id="buildingNumber" value="" placeholder="Building no.">
                                    </div>
                                    </div>
                        </div>
                        <div class="row">
                                    <div class="col-xl-6">
                                    <div class="form-group">
                                     <label>Building section number</label>
                                     <input type="number" name="buildingSectionNumber" class="form-control" id="buildingSectionNumber" value="" min="0" max="9999" placeholder="Building section number">
                                    </div>
                                </div>
                                    <div class="col-xl-6">
                                    <div class="form-group">
                                     <label>Invoice first number</label>
                                     <input type="number" name="lastNumber" class="form-control" id="lastNumber" value=""  placeholder="1">
                                    </div>
                                </div>
								 </div>
			                    <div class="modal-footer justify-content-between">
			                        <button type="button" class="btn btn-outline-light" data-dismiss="modal">Cancel</button>
                                    <a class="btn btn-primary certificate_business_fslz pink-bg-btn" id="certificate_business_fslz" data-next="second_step">Submit</a> 
			                    </div>
			               
			            
			        </div>
                </div>
				</form>
				</div>
				</div>
                      @endsection
{{-- Scripts Section --}}
@section('scripts')
<script type="text/javascript">
 $.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});
    $(document).ready(function () {
    	//var user = '<?php echo (!empty($userData->user_image))? $userData->user_image :''; ?>';
        /* $.validator.addMethod('filesize', function (value, element, param) {
                return this.optional(element) || (element.files[0].size <= param)
            }, 'File size must be less than {0}');

            $.validator.addMethod("alphanumeric", function(value, element) {
                return this.optional(element) || /^[A-Za-z0-9]+$/.test(value);
            }, "Please enter Letters and numbers only");*/

            var form = $('#fsclzn-basic-form');
            form.validate({
                rules: {
                    name: {
                        required: true
                    },
                    address: {
                       required: true,
                    },
                    taxNumber: {
                       required: true,
                       number:true,
                       max:99999999,
                       min:10000000,
                    },
                    zip: {
                       required: true,
                       minlength:4,
                       maxlength:4,
                    },
                },        
              
            });
             var form2 = $('#fsclzn-certificate-form');
            form2.validate({
                rules: {
                    certificate: {
                        required: true
                    },
                    passphrase: {
                       required: true,
                    },
                },        
              
            }); 
            var form3 = $('#fsclzn-business-form');
            form3.validate({
                rules: {
                    businessPremiseId: {
                        required: true
                    },
                    street: {
                       required: true,
                    }, 
                    city: {
                       required: true,
                    },
                    postalCode: {
                       required: true,
                       minlength:4,
                       maxlength:4,

                    }, 
                    community: {
                       required: true,
                    }, 

                    houseNumber: {
                       required: true,
                    }, 
                    cadastralNumber: {
                       required: true,
                        number:true,
                       max:9999,
                    },
                    buildingNumber: {
                       required: true,
                    }, 
                    buildingSectionNumber: {
                       required: true,
                    },
                },        
              
            });
            var form4 = $('#fsclzn-devices-form');
            form4.validate({
                rules: {
                    businessPremiseId: {
                        required: true
                    },
                    electronicDeviceId: {
                       required: true,
                    },
                },        
              
            }); 
      
    $(".basic_detail_fslz").click(function () {
    if ($("#fsclzn-basic-form").valid()) {
        var myForm = document.getElementById('fsclzn-basic-form');
        var formData1 = new FormData(myForm);
        $.ajax({
            url: `{{ route('fiscalization-request') }}`,
            type: "POST",
            data: formData1,
            dataType: "Json",
            contentType: false,
            cache: false,
            processData: false,
            success: function (data) {
                if (data.isSucceeded) {
                    toastr.success("Basic details updated successfully");
                      $("#additional_detail").attr("data-toggle", "collapse");
                    $("#additional_detail").trigger('click');
                } else {
                     $(".ajax-error").show();
                       $(".ajax-error").html(data.msg);
                     $('html, body').animate({
                        scrollTop: $("#guru").offset().top
                    }, 2000);
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                toastr.error("Please try again");
            }
        });
    }
 });

    $(".certificate_detail_fslz").click(function () {
    if ($("#fsclzn-certificate-form").valid()) {
        var myForm = document.getElementById('fsclzn-certificate-form');
        var formData1 = new FormData(myForm);
        $.ajax({
            url: `{{ route('upload-certificate') }}`,
            type: "POST",
            data: formData1,
            dataType: "Json",
            contentType: false,
            cache: false,
            processData: false,
            success: function (data) {
                if (data.isSucceeded) {
                    toastr.success("Certificate upload successfully");
                     $("#business-premises-toggle").attr("data-toggle", "collapse");
                    console.log($("#business-premises-toggle").attr("data-target"));

                    $("#business-premises-toggle").trigger('click');
                    $('.certificate-done').show();
                } else {
                   $(".ajax-error").html(data.msg);
                     $('html, body').animate({
                        scrollTop: $("#guru").offset().top
                    }, 2000);
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                toastr.error("Please try again");
            }
        });
    }
 });

    $("#certificate_business_fslz").click(function () {
    if ($("#fsclzn-business-form").valid()) {
        var myForm = document.getElementById('fsclzn-business-form');
        var formData1 = new FormData(myForm);
        $.ajax({
            url: `{{ route('add-business-premises') }}`,
            type: "POST",
            data: formData1,
            dataType: "Json",
            contentType: false,
            cache: false,
            processData: false,
            success: function (data) {
                if (data.isSucceeded) {
                    $('#add-business-modal').modal('hide');
                    toastr.success("Business premises added successfully");
                     $("#electronic-devicess").attr("data-toggle", "collapse");
                    $("#electronic-devicess").trigger('click');
                    $(".business-premises-done").show();
                    $('#SelectBusinessPremiseId').html(data.premises);
                } else {
                   toastr.error(data.msg);
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                toastr.error("Please try again");
            }
        });
    }
 });


    $("#certificate_devices_fslz").click(function () {
    if ($("#fsclzn-devices-form").valid()) {
        var myForm = document.getElementById('fsclzn-devices-form');
        var formData1 = new FormData(myForm);
        $.ajax({
            url: `{{ route('add-devices') }}`,
            type: "POST",
            data: formData1,
            dataType: "Json",
            contentType: false,
            cache: false,
            processData: false,
            success: function (data) {
                if (data.isSucceeded) {
                    $('#add-device-modal').modal('hide');
                    toastr.success("Device successfully updated");
                      $(".electronic-done").show();
                } else {
                   toastr.error(data.msg);
                    // $(".ajax-error").html(data.msg);
                    //  $('html, body').animate({
                    //     scrollTop: $("#guru").offset().top
                    // }, 2000);
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                toastr.error("Please try again");
            }
        });
    }
 });
     $('#certificate-file').change(function() {
        var filename = $('#certificate-file').val().replace(/C:\\fakepath\\/i, '');
        if(filename!=='')
        {
            $('#certificate-file-name').html(filename);
        }else{
       
        $('#certificate-file-name').html("Choose file");
    }
    });



});

</script>
<script src="{{asset('js/autocomplete.js')}}" ></script>
<script src="https://maps.googleapis.com/maps/api/js?key={{config('services.google_map_key.key')}}&libraries=places&callback=initMap" type="text/javascript"></script>
@endsection