@extends('layouts.app')
@section('content')

	<!-- featured exp start here -->
	<section class="blog-sec aos-item" data-aos="fade-down">
	<div class="container">
	<div class="row">
	<div class="col-lg-12 col-md-12 col-sm-12">
		<a  href="{{ route('blogs') }}"'" class="btn serach-blog-btn btn-danger pink-bg mb-5"><i class="fa fa-arrow-left mr-2" aria-hidden="true"></i> BACK TO ALL BLOGS</a>
		<!-- single blog start here -->
		<div class="single-blog">
			
			<img src="{{ asset('pages/blogs/'.$blog->feature_image) }}" alt="splash-img">
			
			<div class="blog-content">
				<div class="date-admin">
					
					<ul>
						<li class="pink-color"><i class="fas fa-calendar-week"></i> {{ date('d M, Y', strtotime($blog->created_at))}}</li>
						<li class="pink-color"><i class="fas fa-user"></i> By Admin</li>
						
					</ul>

				</div>

				<div class="blog-text">
					<h4>{{ $blog->title }}</h4>
					{!! $blog->blog_description !!}								
				</div>

			</div>

		</div>
				<!-- single blog ends here -->
	</div>

	
	</div>
	</div>
	</section>


	@endsection
@section('script')
@endsection