@if(count($expSchedule))
<input type="hidden" id="booking_amount" value="{{$slotsData['totalAmount']}}">
 <input type="hidden" id="booking_total_amount" value="{{$slotsData['totalwithoutDiscount']}}">
 <input type="hidden" id="booking_discount" value="{{$slotsData['childDiscount']}}">
 @php
  $member= 'member';
  $totalwithoutDiscount='';
   $alreadyBookedSlot=[];
   $key=0;
    if($totalPerson>1)
    {
      $member= 'members';
    }
     $priceValid="Price valid for ".$totalPerson." ".$member;
    if($slotsData['childDiscount']!=0)
    {
        $totalwithoutDiscount='€'.number_format($slotsData['totalwithoutDiscount']);
    }
     $allAlreayBookedSlots= $alreayBookedSlots->toArray();
        if(!empty($allAlreayBookedSlots))
        {
        $alreadyBookedSlot=array_column($allAlreayBookedSlots,'slot_id');
      }
      $i=0;
@endphp
<div class="availability-right-inner"><h5 class="mb-3">{{date('D, d M',strtotime($reqBookingDate))}}</h5>
@foreach($expSchedule as $key=>$slot)
 @php
$advanceTime=$slot->experience->experience_booking_in_advance_time;
$bookingDatetime=$slot->start_date.' '.$slot->start_time;
$advanceBookingTime = date('Y-m-d H:i:s', strtotime($bookingDatetime) - 60 * 60 * $advanceTime);
$userCurrentDateTime= date('Y-m-d H:i:s');
@endphp
  <div class="price-choose-sec">
        <div class="left-price-chse-inr">
        <p>{{date('h:i a',strtotime($slot->start_time))}}-{{date('h:i a',strtotime($slot->end_time))}} (CET)</p>
           <input type="hidden" id="slot-id-{{$i}}" value="{{base64_encode($slot->id)}}">
        <p class="mb-4">
          @if($slotsData['totalAmount']<$slotsData['totalwithoutDiscount'])
          <b class="total-price-overline">{{$totalwithoutDiscount}}</b>
          @endif
          <b class="mr-1">€{{number_format($slotsData['totalAmount'])}}</b>
          @if($slot->experience->minimum_participant == 1 )
              {{'/person'}}
              @else
              @if($totalPerson > $slot->experience->minimum_participant)
              {{'(Price for 1 - '.$totalPerson.' persons)'}}
              @else
             {{'(Price for 1 - '.$slot->experience->minimum_participant.' persons)'}}
             @endif
             @endif
           </p> </div><div class="right-price-chse-inr">
        @if (Auth::check())
        @if(in_array($slot->id,$alreadyBookedSlot))
        <a class="exp-sold-out" data-row="{{$i}}" href="javascript:void(0)" disabled>Sold Out</a>
        @elseif(strtotime($userCurrentDateTime)> strtotime($advanceBookingTime))
        <a class="exp-sold-out" data-row="{{$i}}" href="javascript:void(0)" disabled>Timeout</a>
        @else
        <a class="book-me-now" data-row="{{$i}}" href="javascript:void(0)">Choose</a>
        @endif
        @else
         @if(in_array($slot->id,$alreadyBookedSlot))
          <a class="exp-sold-out" data-row="{{$i}}" href="javascript:void(0)" disabled>Sold Out</a>
           @elseif(strtotime($userCurrentDateTime)> strtotime($advanceBookingTime))
        <a class="exp-sold-out" data-row="{{$i}}" href="javascript:void(0)" disabled>Timeout</a>
        @else
        <a class="" href="{{route('login')}}">Choose</a>
        @endif
        @endif
      </div>
    </div>
  </div>
@php
$i++;
@endphp
@endforeach
@else
<div class="price-choose-sec">
    <p>The experience is not available on this date.</p>
</div>
@endif