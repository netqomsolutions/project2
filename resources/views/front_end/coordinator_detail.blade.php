 @extends('layouts.app')
@section('content')
<!-- featured exp start here -->
<section class="featured-exp featured-exp-listing-grid aos-item product-detail-section" data-aos="fade-down">
    <div class="container">
        <div class="row">
            <!-- exp-box start -->
                <div class="slider-detail-page col-md-4 col-sm-12">
						@if (file_exists(public_path($userData->user_image_path)) && $userData->user_image)
						<img src="{{ asset($userData->user_image_path.'/'.$userData->user_image) }}" />
					@else
						<img src="{{ asset('users/user.png') }}" />
					@endif 
                </div>
                <div class="right-scout-detail col-md-8 col-sm-12">
                <div class="main-heading text-left d-flex align-items-center justify-content-between">
                    <h3>{{  $userData->user_fname .' '. $userData->user_lname }}</h3>
                </div>
                <div class="filters-left back-transparent">
				<div class="language-spoken">
                    <!--p><strong>Spoken Languages :</strong> English, Slovene, German</p-->
                    <p><strong>Phone No :</strong> {{  $userData->user_mobile }}</p>
                </div>
				<div class="chat-box">
				<div class="scout-rating">
		<p>4.36 <i class="fas fa-star" aria-hidden="true"></i> (25)</p>
		</div>
		</div>
                    <p>{{  $userData->about_me }}</p>
                </div>
            </div>
			</div>
			</div>
					
					
</section>
@endsection
@section('script')
@endsection
<!-- featured exp ends here -->