<div class="container">
    <div class="row">
        <div class="col-lg-6 col-md-6 col-sm-12 col-12">
            <a href="#" class="footer logo"><img src="{{ asset('front_end/images/logo.png') }}" alt="locals from zero" /></a>
        </div>
        <div class="col-lg-6 col-md-6 col-sm-12 col-12">
            <ul class="footer-social">
                <li><a href="{{(isset($socialLinks) && $socialLinks->facebook!='')? $socialLinks->facebook : '#'}}"><i class="fab fa-facebook-f"></i></a></li>
                <li><a href="{{(isset($socialLinks) && $socialLinks->instagram!='')? $socialLinks->instagram : '#'}}"><i class="fab fa-instagram"></i></a></li>
                <li><a href="{{(isset($socialLinks) && $socialLinks->twitter!='')? $socialLinks->twitter : '#'}}"><i class="fab fa-twitter"></i></a></li>
                <li><a href="{{(isset($socialLinks) && $socialLinks->linkedin!='')? $socialLinks->linkedin : '#'}}"><i class="fab fa-linkedin-in"></i></a></li>
            </ul>
        </div>
        <hr>
        <div class="col-lg-4 col-md-4 col-sm-12 col-12">
            <h3>About Us</h3>
            <p>We are a group of young, dedicated people with one goal: to bring tourism back to its roots. Inspired by the global <a href='https://tourismfromzero.org/'>TourismFromZero</a> initiative, we strive to highlight our local heroes. Now is the time for you to discover hidden experiences that have not been part of the global tourism market.</p>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-12 col-12">
            <h3>Helpful Links</h3>
            <ul class="quick-links">
                <li><a href="{{route('about-us')}}">About</a></li>
                <li><a href="{{route('privacy-policy')}}">Privacy policy</a></li>
                <li><a href="https://www.tourismfromzero.org/en/about/" target="_blank">Tourism From Zero</a></li>
                <li><a href="{{route('cancellation-policy')}}">Cancellation policy</a></li>
                <!--<li><a href="#">Gift Card</a></li>-->
                <li><a href="https://www.tourismfromzero.org/en/podcast/" target="_blank">Podcast</a></li>
                <li><a href="javascript:void;">Our Partners</a></li>
                <li><a href="{{route('contact-us')}}">Contact Us</a></li>
                <li><a href="{{route('terms-of-use')}}">Terms of Use</a></li>
                <li><a target='_blank' href="https://docs.google.com/forms/d/e/1FAIpQLSdviDZuWe831VbzsVAXsyaRaPgOl_NNkzK4JH0sbHGRIuIZIw/viewform">Stay tuned</a></li>
            </ul>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-12 col-12">
            <h3>Contact Us</h3>
            <p class="d-flex align-items-start p-contant-footer"><span class="contact-span"><i class="fas fa-map-marker-alt"></i></span> <a class="footer-address" href="{{$contact->address_link}}" target="_blank">{{ $contact->address }}<br><b>{{ $contact->address_line_2 }}</b></a></p>
            <p><span class="contact-span"><i class="fas fa-phone-alt"></i></span> Phone: <a href="tel:{{ RemoveFormatPhoneNo($contact->phone) }}">{{ $contact->phone }}</a></p>
            <p class="flex-class-footer d-flex align-items-start"><span class="contact-span"><i class="fas fa-envelope"></i></span> Email:&nbsp;<b class="footer-email-address"><a href="mailto:{{ $contact->email }}">{{ $contact->email }}</a><a href="mailto:sales@localsfromzero.org">sales@localsfromzero.org</a></b></p>
        </div>
    </div>
</div>