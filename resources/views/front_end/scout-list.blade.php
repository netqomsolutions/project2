 @if (count($listOfScouts))
 @foreach($listOfScouts as $scout)
                <div class="col-lg-4 col-md-4 col-sm-12">
                    <div class="scouts-box">
                    @if ($scout->user_image_path && file_exists(public_path($scout->user_image_path)) && $scout->user_image)
                    <img src="{{ asset($scout->user_image_path.'/'.$scout->user_image) }}" alt="{{ $scout->user_fname .' '. $scout->user_lname  }}" class="" />
                    @else
                    <img src="{{ asset('users/user.png') }}" alt="{{ $scout->user_fname .' '. $scout->user_lname  }}" class=""  />
                    @endif 
                        <div class="scout-text">
                            <a href="#">{{ $scout->user_fname .' '. $scout->user_lname  }}</a>

                            @if($mainArrRa = array_column($scout->review_ratings->toArray(), 'rating'))
                                @php
                                    $eachUpeer = array();  $eachLower = array(); $loop = 0;
                                    $mainArrRa = array_count_values($mainArrRa);
                                    foreach ($mainArrRa as $key => $val) {
                                      $eachUpeer[] = $key * $val;
                                      $eachLower[] = $val;
                                    }
                                    $ratingValSum = array_sum($eachUpeer);
                                    $ratingNumSum = array_sum($eachLower);
                                    $rating = $ratingValSum / $ratingNumSum;    
                                @endphp
                                
                                <div class="rating">
                                    <ul>
                                        {{--Start Rating--}}
                                        @for ($i = 0; $i < 5; $i++)
                                            @if (floor($rating) - $i >= 1)
                                                {{--Full Start--}}
                                                <i class="fas fa-star text-warning"> </i>
                                            @elseif ($rating - $i > 0)
                                                {{--Half Start--}}
                                                <i class="fas fa-star-half-alt text-warning"> </i>
                                            @else
                                                {{--Empty Start--}}
                                                <i class="far fa-star text-warning"> </i>
                                            @endif
                                        @endfor
                                        {{--End Rating--}}
                                        <li>{{ $scout->review_ratings_count }} Reviews</li>
                                    </ul>
                                    <p>{{ $scout->about_me }}</p>
                                    <a href="{{ route('scout-detail',['id' => base64_encode($scout->id)]) }}" class="pink-color">View Profile</a>
                                </div>
                            @else
                                <div class="rating">
                                    <ul>
                                        <i class="far fa-star text-warning"> </i>
                                        <i class="far fa-star text-warning"> </i>
                                        <i class="far fa-star text-warning"> </i>
                                        <i class="far fa-star text-warning"> </i>
                                        <i class="far fa-star text-warning"> </i>
                                        <li>0 Reviews</li>
                                    </ul>
                                    <p>{{ $scout->about_me }}</p>
                                    <a href="{{ route('scout-detail',['id' => base64_encode($scout->id)]) }}" class="pink-color">View Profile</a>
                                </div>                                
                            @endif
                        </div>
                    </div>
                </div>
            @endforeach
    <input type="hidden" name="hidden_page" id="hidden_page" value="1" />
    <div class="col-sm-12 p-0">
    <div class="scout-page-pagination pagination_sec" style="margin-top: 20px;">
        <div>
            {!! $listOfScouts->links() !!}
        </div>
    </div>
</div>
@else
 
@endif