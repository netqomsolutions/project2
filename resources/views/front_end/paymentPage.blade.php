@extends('layouts.app')
{{-- Content --}}
@section('content')
<script src="https://js.stripe.com/v3/"></script>
<section class="payment-page">
    <div class="container">
        <div class="booking-error" style="display:none;">
            <p><i class="fas fa-exclamation-triangle"></i><span>This experience is already booked for this time peridod.</span></p>
        </div>
        <div class="row">
            <div class="payment-inner-left col-lg-6 col-md-12 col-sm-12">
            <input type="hidden" value="{{ Session('cart.0.freshExpId') }}" id="freshExpId">
            <div class="back-btn"><a href="{{ route('get-experience-slots',['id'=> base64_encode($experience->id)])}}"><i class="fas fa-angle-left" aria-hidden="true"></i></a></div>
            <h2 class="mb-5">Confirm and pay</h2>
            <div class="payment-experience">
                <h4>Your experience</h4>
                 <!--p class="mb-3 mt-4 font-weight-normal">Minimum no. participant : <b style="color:#ff4486;">{{ $experience->minimum_participant }}</b></p-->
                   @if($experience->experience_meta_detail->is_children_discount_active ==1 )
                            <div class="d-flex align-items-center justify-content-between">
                            <p class="font-weight-normal m-0"> Infants up to  <b style="color:#ff4486;">{{ $experience->experience_meta_detail->children_age_for_free }}</b> years: Free </p> 
                            </div>
                             <div class="d-flex align-items-center justify-content-between"> 
                            <p class="font-weight-normal m-0">Discount for children between <b style="color:#ff4486;">{{ $experience->experience_meta_detail->children_age_for_free }}</b> and <b style="color:#ff4486;">{{ $experience->experience_meta_detail->children_age_for_discount }}</b> years: <b style="color:#ff4486;">{{ $experience->experience_meta_detail->children_discount_1 }}%</b></p> 
                            </div>
                            @endif
                <h5 class="mt-4">Date</h5>
                <p>{{date('D, d M',strtotime($session['booking_date'])).' '.date('h:i a',strtotime($session['booking_start_time'])).' - '.date('h:i a',strtotime($session['booking_end_time']))}} (CET)</p>
              
                <h5 class="mt-3">Guests</h5>
                <div class="guest-count-main">
                <div class="guests-count">
                    <a id="guest-count" href="javascript:;"><span><b class="font-weight-normal" id="dynamic_guest_count">{{ Session::get('cart.0.adults') + Session::get('cart.0.children') }}{{(Session::get('cart.0.adults') + Session::get('cart.0.children')> 1)? ' guests' : ' guest'}}</b></span> <i class="fas fa-angle-down"></i></a>
                </div>
                <div class="guest_inner_sec" style="display: none;">
                            <ul>
                                <li>
                                    <span class="left_bar">Adults </span>
                                    <span class="right_bar">
                                        <button class="minus-adult-button"><i class="fa fa-minus" aria-hidden="true"></i></button> 
                                        <span>
                                            <input type="number" min="1" max="{{ $experience->maximum_participant }}" value="{{ $session['adults']}}" id="quantity" name="adults" class="adult-cls" readonly>
                                            <button class="plus-adult-button"><i class="fa fa-plus" aria-hidden="true"></i></button>
                                        </span>
                                    </span>
                                </li>
                                <li>
                                    <span class="left_bar">Children <br>
                                          @if($experience->experience_meta_detail->is_children_discount_active ==1 )
                                        <span class="text_sec_inner">{{ $experience->experience_meta_detail->children_age_for_free }}-{{ $experience->experience_meta_detail->children_age_for_discount }}</span>
                                        @else
                                         <span class="text_sec_inner">2-12</span>
                                        @endif
                                    </span>
                                    <span class="right_bar">
                                        <button class="minus-children-button"><i class="fa fa-minus" aria-hidden="true"></i></button>
                                        <span>
                                            <input type="number" min="0" name="children" class="child-cls" max="{{ $experience->maximum_participant }}" value="{{$session['children'] }}" id="quantity" readonly>
                                            <button class="plus-children-button"><i class="fa fa-plus plus-button" aria-hidden="true"></i></button>
                                        </span>
                                    </span>
                                </li>
                                <li>
                                    <span class="left_bar">Infants <br>
                                         @if($experience->experience_meta_detail->is_children_discount_active ==1 )
                                        <span class="text_sec_inner">Under {{ $experience->experience_meta_detail->children_age_for_free }}</span>  
                                        @else
                                        <span class="text_sec_inner">Under 12</span>
                                       @endif
                                    </span>
                                    <span class="right_bar">
                                        <button class="minus-infant-button"><i class="fa fa-minus minus-button" aria-hidden="true"></i> </button>
                                        <span>
                                            <input type="number" name="infant" min="0" class="infant-cls" value="{{$session['infants']}}" id="quantity" readonly>
                                            <button class="plus-infant-button"><i class="fa fa-plus plus-button" aria-hidden="true"></i></button>
                                        </span>
                                    </span>
                                </li>
                            </ul>
                            <!--button class="btn btn-success book-me-now" style="float: right;color: #fff;background-color: #1e7e34;border-color: #1c7430;">Save</button-->
                            <a href="javascript:void(0);" class="get-payment-total">Save</a>
                        </div>
                        </div>
            </div>

            <div class="payment-experience">
                <h4>Pay with</h4>
                <div class="card-details">
                     <label>
                        <span>Card</span>
                        <div id="card-element" class="field"></div>
                        <button id="card-button" data-min='{{ $experience->minimum_participant }}' data-current="{{ Session::get('cart.0.adults') + Session::get('cart.0.children') }}" class="btn btn-danger pink-bg mt-3"><i class="fas fa-lock"></i> Confirm and pay</button>
                    </label>
                    <span style="color: red; text-align: center;" id="st-err"></span>
                </div>
            </div>
            </div>

            <div class="payment-inner-right col-lg-6 col-md-12 col-sm-12 pl-lg-5">
                <div class="payment-inner-right-box">
                    <div class="img-with-info">
                        <div class="img-left-payment">
                            <img src="{{ asset('pages/experiences/'.$experience->experience_feature_image)}}" />
                        </div>
                        <div class="right-payment-inner">
                            <h5>{{ $experience->experience_name }}</h5>
                           
                            <!--p>Hosted in English</p-->
                            <div class="d-flex align-items-center tab-ratings">
                               {!! getExperienceReview($experience->id,'ratings') !!}
                              {{ getExperienceReview($experience->id,'reviews') }} Reviews
                            </div>
                        </div>
                        </div>
                        @php 
                          if($experience->experience_type==1){
                            $experience_price= number_format($experience->experience_low_price);
                          }else{
                             $experience_price= $experience->minimum_participant * $experience->experience_additional_person;
                         }
                    @endphp
                        <div class="payment-price-details" id="dynamic-price-details">
                                <h4>Price details</h4>
                                <p>Number of guests <span>{{$session['adults']+$session['children'] }}</span></p>
                                <p>Discount <span>€ {{number_format($session['discount']) }}</span></p>
                                <p><b>Total</b> <span>
                                    @if($session['amount']<$session['total_price'])
                                    <b class="total-price-overline">€ {{number_format($session['total_price'])}}</b>
                                    @endif
                                     <b>€ {{ number_format($session['amount']) }}</b></span></p>
                                     @if($experience->experience_meta_detail->is_children_discount_active==1)
                                 <p class="align-items-start"><b style="color:#ff4486;">Note:</b>Discount for children only applies above the minimum price (€{{number_format($experience_price)}}) per experience. </p>
                                 @endif
                            </div>
                            <div class="cancellation-policy">
                                <h4>Cancellation policy</h4>
                                <p>Full refund available for cancellation made up to 48 h prior to booking start time</p>
                            </div>
                    
                </div>
            </div>


        </div>
    </div>
</section>
@endsection
{{-- Scripts Section --}}
@section('javascript')
<script type="text/javascript">
    /**********************Start Stripe Working**********/
    var stripe_key= "{{config('services.stripe.key')}}";
    var maxParticipants="{{ Session('cart.0.maxParticipants') }}";
        var schedule='';
    var stripe = Stripe(stripe_key);
    const elements = stripe.elements();
    const cardElement = elements.create('card');
    cardElement.mount('#card-element');
    const cardButton = document.getElementById('card-button');
    cardButton.addEventListener('click', async (e) => {
        stripe.createToken(cardElement).then(function(result) {
            if (result.error) {
                $('#st-err').html(result.error.message);
            } else {
                
                var minParticipants=$('#card-button').attr("data-min");
                var currentParticipants=$('#card-button').attr("data-current");

                console.log(minParticipants);
                console.log('>>>'+currentParticipants);
                if(currentParticipants >=minParticipants){ 
                $('#st-err').html('');
                $('.spinner').show();
                $('#card-button').prop('disabled', true);
                $.ajax({
                    url: '{{ route("booking-confirm-and-pay") }}',
                    type: 'POST',
                    data: { token : result.token },
                    success: function(response){
                        setTimeout(function(){
                            if(response.isSucceeded){
                                 window.location.href= "{{route('thank-you-for-booking')}}";
                            }else{
                                if(response.session=='FALSE')
                                {    
                                    $(".booking-error").html('<p><i class="fas fa-exclamation-triangle"></i><span>This experience is already booked for this time peridod.</span></p>');
                                    $(".booking-error").show();
                                    $("html, body").animate({ scrollTop: $('.booking-error').offset().top }, 1000);
                                }
                            }
                        },2000);
                    }
                });
            }else{
                 $(".booking-error").html('<p><i class="fas fa-exclamation-triangle"></i><span>Minimum '+minParticipants+' participants required for this experience.</span></p>');
                 $(".booking-error").show();
                 $("html, body").animate({ scrollTop: $('.booking-error').offset().top }, 1000);
            }
            }
        });
    });
</script>
<script type="text/javascript">
       $(document).click(function(e){
         var container = $(".guest-count-main"); 
        // If the target of the click isn't the container
        if(!container.is(e.target) && container.has(e.target).length === 0 ){ 
                 $(".guest_inner_sec").slideUp();
        } 
    });
</script>
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="{{ asset('js/experience-payment-page.js') }}"></script>
@endsection