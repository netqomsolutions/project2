
                <div class="slider">
                   
                    @forelse ($listOfCoordinators as $coordinator)
                        <div class="slide team-box">
                            @if(!empty($coordinator->user_image_path) && !empty($coordinator->user_image))
                            @if (file_exists(public_path($coordinator->user_image_path)) && $coordinator->user_image)
                               <img src="{{ asset($coordinator->user_image_path.'/'.$coordinator->user_image) }}" />
                            @else
                                <img src="{{ asset('users/user.png') }}" alt="team" data="{{ $coordinator->id }}" />
                            @endif
                            @endif
                            @if($coordinator->is_scout==1)
                            <h4>{{ 'Scout '.$coordinator->user_fname .' '. $coordinator->user_lname  }}
                                <!--span class="team-rating">
                                    @php $mainArrRa = array_column($coordinator->review_ratings->toArray(), 'rating') @endphp                                    
                                    <?php 
                                    if(!empty($mainArrRa)){
                                        $eachUpeer = array();  $eachLower = array(); $loop = 0;
                                        $mainArrRa = array_count_values($mainArrRa);
                                        foreach ($mainArrRa as $key => $val) {
                                          $eachUpeer[] = $key * $val;
                                          $eachLower[] = $val;
                                        }
                                        $ratingValSum = array_sum($eachUpeer);
                                        $ratingNumSum = array_sum($eachLower);
                                        $rating = $ratingValSum / $ratingNumSum;    
                                        ?><i class="fas fa-star text-warning" data="asxsax"> </i>
                                        <?php echo $rating;
                                    }else{ ?>
                                    <i class="far fa-star text-warning" data="=huiii"> </i>( 0 )<?php 
                                    } ?>
                                </span-->
                            </h4>
                             <h5>{{!empty($coordinator->user_meta ) && !empty($coordinator->user_meta->current_status) ? $coordinator->user_meta->current_status:""}}</h5> 

                            @else
                             <h4>{{$coordinator->user_fname .' '. $coordinator->user_lname  }}</h4>
                             <h5>{{!empty($coordinator->user_meta ) && !empty($coordinator->user_meta->current_status) ? $coordinator->user_meta->current_status:""}}</h5> 
                             @endif
                               @php
                                $cod_str_length=\Illuminate\Support\Str::length($coordinator->about_me);
                               @endphp
                            <!--p class="minimize" data-lenght="{{$cod_str_length}}">{{$coordinator->about_me}}</p-->
                             @if($coordinator->is_scout==1)
                            <!--a href="{{route('scout-detail',['id' => base64_encode($coordinator->id)])}}" class="btn btn-success green-bg">Read More</a-->
                            @else
                             <!--a href="{{route('coordinator-detail',['id' => base64_encode($coordinator->id)])}}" class="btn btn-success green-bg">Read More</a-->
                             @endif
                             @php 
                                $social_links = json_decode($coordinator->user_meta->social_links);  
                            @endphp
                          <ul class="contact-social">
                            @if(!empty($social_links->facebook_link))  
                            <li><a target="_blank" href="{{ !empty($social_links->facebook_link) ? $social_links->facebook_link : 'javascript:;' }}"><i class="fab fa-facebook-f" aria-hidden="true"></i></a></li>
                            @endif
                            @if(!empty($social_links->instagram_link)) 
                            <li><a target="_blank" href="{{ !empty($social_links->instagram_link) ? $social_links->instagram_link : 'javascript:;' }}"><i class="fab fa-instagram" aria-hidden="true"></i></a></li>
                            @endif
                            @if(!empty($social_links->pinterest_link)) 
                            <li><a target="_blank" href="{{ !empty($social_links->pinterest_link) ? $social_links->pinterest_link : 'javascript:;' }}"><i class="fab fa-pinterest-p" aria-hidden="true"></i></a></li>
                            @endif
                            @if(!empty($social_links->linkedin_link)) 
                            <li><a target="_blank" href="{{ !empty($social_links->linkedin_link) ? $social_links->linkedin_link : 'javascript:;' }}"><i class="fab fa-linkedin-in" aria-hidden="true"></i></a></li>
                            @endif
                            @if(!empty($social_links->twitter_link)) 
                            <li><a target="_blank" href="{{ !empty($social_links->twitter_link) ? $social_links->twitter_link : 'javascript:;' }}"><i class="fab fa-twitter" aria-hidden="true"></i></a></li>
                            @endif 
                        </ul>
                        </div>

                    @empty

                    @endforelse
                </div>