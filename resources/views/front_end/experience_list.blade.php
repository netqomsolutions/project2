@extends('layouts.app')
@section('content')
<section class="inner-banner listing-banner" style="background: url( {{ asset('front_end/images/listing-banner.jpg') }} ) no-repeat;">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-12">
                <h3>Locals <span class="green-color">From</span> Zero</h3>
               <!--  <p>Marketplace For Local Experiences</p> -->
            </div>
        </div>
    </div>
</section>
<!-- search form start here -->
<div class="container">
    <div class="row">
        <div class="col-md-12">
             <form class="search-form" method="get" action="{{route('experiences-list')}}">
                <div class="form-group">
                    <input type="text" name="name" value="{{request()->query('name')}}" class="form-control" id="lets-find" placeholder="Type activity-related keyword">
                </div>
                <div class="form-group mx-sm-3 position-relative" id="search-current-location">
                    <input type="text" name="address" value="{{request()->query('address')}}" class="form-control" id="exp-location" placeholder="Location">
                     <p class="explore_inner">Location <span><i class="fas fa-times close-explore-nearby"></i></span></p>
                     @if(request()->query('address')!=='')
                    <input type="hidden" name="lng" class="form-control" id="exp-longitude" placeholder="Location" value="{{ request()->query('lng') }}">
                    @else
                     <input type="hidden" name="lng" class="form-control" id="exp-longitude" placeholder="Location" value="">
                     @endif
                    @if(request()->query('address')!=='' && request()->query('lng')!=='' && request()->query('lat')!=='')
                    <input type="hidden" name="lat" class="form-control" id="exp-latitude" placeholder="Location" value="{{request()->query('lat')}}">
                    @else
                     <input type="hidden" name="lat" class="form-control" id="exp-latitude" placeholder="Location" value="">
                     @endif
                      <div class="location-search-box">
                        <a><img src="{{asset('front_end/images/search-location-img-icon.jpg')}}" /> <span>Explore nearby destinations</span> </a>
                    </div>
                </div>
                <div class="form-group mx-sm-3 home-search-category">
                    <select class="form-control" name="cat">
                        <option value="">All Categories</option>
                        @forelse ($experienceCategories as $expCat)
                            <option value="{{ $expCat->id }}" {{(request()->query('cat')== $expCat->id)? 'selected' : ''}}>{{ $expCat->name }}</option>
                        @empty
                        @endforelse
                    </select>
                </div>
                <button type="submit" class="btn btn-danger pink-bg home-ser">Search</button>
            </form>
        </div>
    </div>
</div>
<!-- featured exp start here -->
<section class="featured-exp featured-exp-listing-grid">
    <div class="container-fluid">
        <div class="row">
            <!-- exp-box start -->
            <div class="col-lg-3 col-md-4 col-12">
                <button type="button" class="btn btn-success  filter-collapse-btn" data-toggle="collapse" data-target="#filter-toggle"><i class="fas fa-filter"></i> More Filter</button>
                <div class="filter-box collapse" id="filter-toggle">
                    <div class="filter-heading">
                        <h4>Filter By</h4>
                    </div>
                    <div class="filters-left">
                        <div class="single-filter">
                            <h5>Choose Category</h5>
                            @forelse ($experienceCategories as $experienceCategory)                    
                                <p>
                                    <input type="checkbox" class="form-check-input experience_categories_check" name="experience_categories[]" value="{{ $experienceCategory->id }}" id="{{ 'expcat_'.$experienceCategory->id }}"  />
                                    <label class="form-check-label" for="{{ 'expcat_'.$experienceCategory->id }}">{{ ucwords($experienceCategory->name) }}</label>
                                </p>
                            @empty

                            @endforelse
                        </div>
                        <hr>
                        <h5>Price Range (€)</h5>
                        <div class="container">
                            <div class="row slider-labels">
                                <div class="col-xs-6 caption">
                                   <strong></strong> <span id="slider-range-value1"></span>
                                </div>
                                <span> - </span>
                                <div class="col-xs-6 text-right caption">
                                    <strong></strong> <span id="slider-range-value2"></span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12 pl-0">
                                    <div id="slider-range"></div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <form>
                                        <input type="hidden" name="min-value" value="">
                                        <input type="hidden" name="max-value" value="">
                                    </form>
                                </div>
                            </div>
                        </div>
                        <div class="time-range">
                            <h5>Duration (hours)</h5>
                            <div class="container">
                                <div class="row slider-labels">
                                    <div class="col-xs-6 caption">
                                        <strong></strong> <span id="slider-range-value1-1"></span>
                                    </div>
                                    <span> - </span>
                                    <div class="col-xs-6 text-right caption">
                                        <strong></strong> <span id="slider-range-value2-1"></span>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-12 pl-0">
                                        <div id="slider-range1"></div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-12">
                                        <form>
                                            <input type="hidden" name="min-value" value="">
                                            <input type="hidden" name="max-value" value="">
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <hr>
                        <div class="single-filter">
                            <h5>Start Time</h5>
                            <p><input type="checkbox" class="form-check-input experience_start_time" name="experience_start_time[]" id="Morning" value="1" >
                                <label class="form-check-label" for="Morning">Morning <span class="time">01:00 - 11:00</span></label>
                            </p>
                            <p><input type="checkbox" class="form-check-input experience_start_time" id="Afternoon" name="experience_start_time[]" value="2">
                                <label class="form-check-label" for="Afternoon">Afternoon <span class="time">12:00 - 18:00</span></label>
                            </p>
                            <p><input type="checkbox" class="form-check-input experience_start_time" id="Evening" name="experience_start_time[]" value="3">
                                <label class="form-check-label" for="Evening">Evening <span class="time">19:00 - 00:00</span></label>
                            </p>
                        </div>
                        <hr>
                        <div class="single-filter">
                            <h5>Spoken Languages (By Scout)</h5>
                            <!-- <p><input class="form-check-input" type="radio" name="language" id="English" value="1" >
                                <label class="form-check-label radio-label" for="English">English</label>
                            </p>
                            <p><input class="form-check-input" type="radio" name="language" id="Slovene" value="2" checked>
                                <label class="form-check-label radio-label" for="Slovene">Slovene</label>
                            </p>
                            <p><input class="form-check-input" type="radio" name="language" id="German" value="3" >
                                <label class="form-check-label radio-label" for="German">German</label>
                            </p> -->
                            <!-- <button type="button" class="btn btn-success green-bg rounded-btn" tabindex="0">Chose other languages</button> -->
                            <select  name="user_other_languages[]"  class="form-control other_lang_check form-control-solid h-auto py-6 px-6 rounded-sm font-size-h6 "  multiple="multiple" style="display: block;" >
                                    @foreach($languages as $language)
                                    <option value="{{ $language->id }}" >{{ $language->name }}</option> 
                                    @endforeach
                            </select>


                            <h5>Spoken Languages (By Host)</h5>
                            <!-- <p><input class="form-check-input" type="radio" name="language" id="English" value="1" >
                                <label class="form-check-label radio-label" for="English">English</label>
                            </p>
                            <p><input class="form-check-input" type="radio" name="language" id="Slovene" value="2" checked>
                                <label class="form-check-label radio-label" for="Slovene">Slovene</label>
                            </p>
                            <p><input class="form-check-input" type="radio" name="language" id="German" value="3" >
                                <label class="form-check-label radio-label" for="German">German</label>
                            </p> -->
                            <!-- <button type="button" class="btn btn-success green-bg rounded-btn" tabindex="0">Chose other languages</button> -->
                            <select  class="form-control other_lang_check_host form-control-solid h-auto py-6 px-6 rounded-sm font-size-h6 "  multiple="multiple" style="display: block;" >
                                    @foreach($languages as $language)
                                    <option value="{{ $language->id }}" >{{ $language->name }}</option> 
                                    @endforeach
                            </select>
                            <hr>
                            <div class="single-filter">
                                <h5>Filter by group type</h5>
                                @forelse ($experienceFeatures as $experienceFeature)
                                    <p>
                                        <input type="checkbox" class="form-check-input experience_features_check" name="experience_features[]" id="{{ 'expfea_'.$experienceFeature->id }}"  value="{{ $experienceFeature->id }}">
                                        <label class="form-check-label" for="{{ 'expfea_'.$experienceFeature->id }}">{{ ucwords($experienceFeature->name) }}</label>
                                    </p>
                                @empty

                                @endforelse                                
                            </div>
                        </div>
                    </div>
                    <!-- testimonial left start here -->
                    <div class="filter-heading">
                        <h4>Recent Reviews from Travelers</h4>
                    </div>
                    <div class="left-testimonial-box overflow-box-list">
                       
                        @forelse ($reviewRatingsForExperience as $reviewRatForExp)
                            @php
                                $exp_path = '';
                                $exp_image = '';
                                 $str_length=\Illuminate\Support\Str::length($reviewRatForExp->review);
                                if(isset($reviewRatForExp) && optional($reviewRatForExp->user)){
                                    $exp_image = optional($reviewRatForExp->user)->user_image;
                                    $exp_path = optional($reviewRatForExp->user)->user_image_path;
                                }
                            @endphp
                            <div class="single-testimonial">
                                <div class="customer-testimonial-details">
                                    @if (file_exists(public_path($exp_path)) && $exp_image)
                                    @if(filter_var($exp_image, FILTER_VALIDATE_URL) === FALSE)
                                        <img class="img-responsive" src="{{ asset($exp_path.'/'.$exp_image) }}" alt="">
                                    @else
                                    <img class="img-responsive" src="{{ $exp_image }}" alt="">
                                    @endif
                                    @else
                                        <img class="img-responsive" src="{{ asset('users/user.png') }}" alt="">
                                    @endif 
                                    <div class="name-country">
                                        <a href="#">{{ $reviewRatForExp->user->user_fname .' '. $reviewRatForExp->user->user_lname }}</a>
                                        <p><span class="coutry-flag"><img src="images/uk-flag.jpg" /> {{ $reviewRatForExp->user->user_address}}</span> <span class="date green-color"> @php echo date('j M, Y', strtotime($reviewRatForExp->created_at));   @endphp </span></p>
                                        <div class="rating">
                                            @php 
                                                $rating = $reviewRatForExp->rating; 
                                            @endphp
                                            <ul>
                                                {{--Start Rating--}}
                                                @for ($i = 0; $i < 5; $i++)
                                                    @if (floor($rating) - $i >= 1)
                                                        {{--Full Start--}}
                                                        <li><i class="fas fa-star text-warning"> </i></li>
                                                    @elseif ($rating - $i > 0)
                                                        {{--Half Start--}}
                                                        <li><i class="fas fa-star-half-alt text-warning"> </i></li>
                                                    @else
                                                        {{--Empty Start--}}
                                                        <li><i class="far fa-star text-warning"> </i></li>
                                                    @endif
                                                @endfor
                                                {{--End Rating--}}
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <p class="minimize" data-lenght="{{$str_length}}">
                                    {{ $reviewRatForExp->review }}
                                </p>
                                <a href="{{route('experience-detail',['id'=>base64_encode($reviewRatForExp->experience_id)])}}" class="pink-color">See experience</a>
                            </div>
                            <hr>
                        @empty

                        @endforelse
                        
                    </div>
                    <!-- testimonial left ends here -->
                </div>
            </div>
            <div class="col-lg-6 col-md-8 col-12">
                <div class="sort-head">
                    <div class="sort-by">
                        <label for="exampleFormControlSelect1">Sort by:</label>
                        <select class="form-control" id="exampleFormControlSelect1">
                            <option value="1">Best Rated Experiences</option>
                            <option value="2">Best Rated Scouts</option>
                            <option value="3">Most Popular Experiences</option>
                        </select>
                    </div>
                    <div class="grid-list-btns">
                        <button class="btn btn-primary btn-grid"><i class="fas fa-th"></i></button>
                        <button class="btn btn-danger btn-list"><i class="fas fa-list"></i></button>
                    </div>
                </div>
                <div class="row grid-container" id="mainCatDiv">
                     @include('front_end.experience_mat')

                </div>
                  <div class="loader exp-list-loader" style="display: none;"><div class="inner_loader"></div></div>
                <input type="hidden" name="hidden_page" id="hidden_page" value="1" />
                <input type="hidden" name="sort_by" id="sort_by" value="1" />
            </div>
            <div id="testing"></div>
            <div id="dynamic" class="col-lg-3 col-md-12 col-12 map-main-right" >
                <div class="right-map clearHeader" >
                    <div id="map" style="height:100%"></div>
                </div>
            </div>
        </div>
    </div>
    
</section>
<!-- featured exp ends here -->
@endsection
@section('javascript')
<script src="https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/markerclusterer.js"></script>
<script src="{{ asset('front_end/js/custom.js') }}" type="text/javascript"></script>
<script>
var rangeSlider = document.getElementById('slider-range');
var rangeSlider1 = document.getElementById('slider-range1');

$(document).ready(function(){
    var lat1="{{(request()->query('lat')!='')? request()->query('lat') : ''}}";
    var lng1="{{(request()->query('lng')!='')? request()->query('lng') : ''}}";
    var QueryAdress="{{(request()->query('address')!='')? request()->query('address') : ''}}";
    if(lat1!=='' && lng1!=='' && QueryAdress!=='')
    {
        var lat=lat1;
        var lng=lng1;
    }
    else{
         var lat='46.1437662';
        var lng='13.8718184';
        $("#exp-longitude").val('');
        $("#exp-latitude").val('');
    }
    if(lat=='46.1437662' && lng=='13.8718184')
    {
        var zoomLevel=4;
    }else{
        var zoomLevel=10;
    }
    $("#exp-location").click(function(){
        $(".location-search-box").show();
    });
    $('.location-search-box').click(function(){
        $("#search-current-location").addClass('explore-near-by');
        $("#exp-location").val('Explore nearby destinations');
          getCurrentAddress();
    });
    $(".close-explore-nearby").click(function(){
        $("#search-current-location").removeClass('explore-near-by');
        $(".location-search-box").hide();
        $("#exp-location").val('');
        $("#exp-latitude").val('');
        $("#exp-longitude").val('');
    });
    var myLatlng = new google.maps.LatLng(parseFloat(lat),parseFloat(lng));
                    var mapOptions = {
                    zoom:zoomLevel,
                    center: myLatlng,
                    mapTypeControlOptions: {
                    style: google.maps.MapTypeControlStyle.HORIZONTAL_BAR,
                    position: google.maps.ControlPosition.TOP_CENTER,
                      },
                    zoomControl: true,
                    zoomControlOptions: {
                    position: google.maps.ControlPosition.RIGHT_TOP,
                      },
                    scaleControl: true,
                    streetViewControl: true,
                    streetViewControlOptions: {
                    position: google.maps.ControlPosition.LEFT_TOP,
                    },
                    fullscreenControl: true,
                    }

                    console.log("myLatlng",myLatlng); 
    var map = new google.maps.Map(document.getElementById("map"), mapOptions);
    var icon = {
            url: "{{asset('front_end/images/marker.png')}}", // url
            scaledSize: new google.maps.Size(30, 40), // scaled size
            origin: new google.maps.Point(0,0), // origin
            anchor: new google.maps.Point(0, 0) // anchor
        };

        filter_data(1);
    function filter_data(page){
        var minimumPrice= 0 , maximumPrice = 0 , minimumDuration = 0,  maximumDuration = 0;
         var search_name="{{request()->query('name')}}";
        var search_address="{{request()->query('address')}}";
        var search_cat="{{request()->query('cat')}}";
        var search_lat="{{request()->query('lat')}}";
        var search_lng="{{request()->query('lng')}}";
        // Set visual min and max values and also update value hidden form inputs for price 
        minimumPrice = $('#slider-range-value1').html();
        maximumPrice = $('#slider-range-value2').html();
        // Set visual min and max values and also update value hidden form inputs for duration
        minimumDuration = $('#slider-range-value1-1').html();
        maximumDuration = $('#slider-range-value2-1').html();
        $('.filter_data').html('<div id="loading" style="" ></div>');
        var exp_cat = get_range_slider('experience_categories_check');
        var other_lang_check = get_range_slider('other_lang_check');
        var other_lang_check_host = get_range_slider('other_lang_check_host');
        var exp_feat = get_range_slider('experience_features_check');
        var lang = $('input[name=language]:checked').val() == undefined ? 0 : $('input[name=language]:checked').val();
        var start_time =get_range_slider('experience_start_time');
        var sort_by = $('#exampleFormControlSelect1').find(":selected").val();
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: HOST_URL+"/mat?page="+page,
            method:"GET",
            data:{minimumPrice:minimumPrice, maximumPrice:maximumPrice, minimumDuration:minimumDuration, maximumDuration:maximumDuration, exp_cat:exp_cat, exp_feat:exp_feat,sort_by:sort_by,start_time:start_time,search_name:search_name,search_address:search_address,search_cat:search_cat,other_lang_check:other_lang_check,other_lang_check_host:other_lang_check_host,search_lat:search_lat,search_lng:search_lng},
            success:function(data){
                var locations=[];
                  if(data.locations!='')
                  {
                    var locations=JSON.parse(data.locations);
                  }
                   
                    
                    $('.exp-list-loader').css('display','none');
                    if(data.isSucceeded){
                         var infowindow = new google.maps.InfoWindow();
                        var marker, i;

                     var markers = locations.map(function(location, i) {
                    var marker = new google.maps.Marker({
                      position: location,
                      title:location.exp_name,
                      icon:icon,
                    });
                    google.maps.event.addListener(marker, 'click', function(evt){
                       var locationDetail = getCurrentMarker(location.exp_id);
                        var html=buildIWContent(locationDetail);
                        console.log(html);
                        infowindow.setContent(html);
                        infowindow.open(map, marker, html);
                    });
                    return marker;
                  });
                     var markerCluster = new MarkerClusterer(map, markers, {
                        imagePath:
                          "https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/m",
                      });
                        $('#mainCatDiv').html('');
                        $('#mainCatDiv').html(data.data);
                    }else{
                        $('#mainCatDiv').html(data.data);
                    }
            }
        });
    }

    //Set visual min and max values and also update value hidden form inputs for price
    rangeSlider.noUiSlider.on('change', function(values, handle) {
        $('.exp-list-loader').css('display','flex');
        var page = 1;
        if(values[handle]){
            filter_data(page);
        }
    });

    //Set visual min and max values and also update value hidden form inputs for duration
    rangeSlider1.noUiSlider.on('change', function(values, handle) {
        var page = 1;
        $('.exp-list-loader').css('display','flex');
        if(values[handle]){
            filter_data(page);
        }
    });

    $('.experience_categories_check').click(function(){
        var page = 1;
        $('.exp-list-loader').css('display','flex');
        filter_data(page);
    });

    $('.other_lang_check').on('change',function(){
        var page = 1;
        $('.exp-list-loader').css('display','flex');
        filter_data(page);
    });

    $('.other_lang_check_host').on('change',function(){
        var page = 1;
        $('.exp-list-loader').css('display','flex');
        filter_data(page);
    });
    

    $('.experience_features_check').click(function(){
        var page = 1;
        $('.exp-list-loader').css('display','flex');
        filter_data(page);
    });

    $('.experience_start_time').click(function(){
        var page = 1;
        $('.exp-list-loader').css('display','flex');
        filter_data(page);
    });

    function get_range_slider(class_name){
        var filter = [];
        if(class_name == 'other_lang_check'){
            var arNewOtherLang = $(".other_lang_check").val();
            $.each(arNewOtherLang, function( index, value ) {
              filter.push(value);
            });
        }else if(class_name == 'other_lang_check_host'){
            var arNewOtherLang = $(".other_lang_check_host").val();
            $.each(arNewOtherLang, function( index, value ) {
              filter.push(value);
            });
        }else{
            $('.'+class_name+':checked').each(function(){
                filter.push($(this).val());
            });
        }   
        return filter;
    }

    $('#exampleFormControlSelect1').on('change',function(){
        $('.exp-list-loader').css('display','flex');
        var page = 1;
        filter_data(page);
    })

    $(document).on('click', '.pagination a', function(event){
        $('.exp-list-loader').css('display','flex');
        event.preventDefault();
        var page = $(this).attr('href').split('page=')[1];
        $('#hidden_page').val(page);
        $('li').removeClass('active');
        $(this).parent().addClass('active');
        filter_data(page);
    });
    $(document).click(function(e){
         var container = $("#exp-location"); 
        // If the target of the click isn't the container
        if(!container.is(e.target) && container.has(e.target).length === 0 ){ 
                 $(".location-search-box").hide();
        } 
    });
});

 $("select[multiple='multiple']").bsMultiSelect({
      placeholder    : 'Chose other languages',
      maxHeight: 450
 });
     



  
    
</script>



@endsection