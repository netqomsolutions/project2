@extends('layouts.app')
@section('content')
<!-- featured exp start here -->
<section class="feedback-form">
    <div class="container">
        <div class="feedback_sec">
             <div class="innerText">
                    <h1>{{ $message }}</h1> 
                    <p>{{ $expName }}</p>
                    
                </div>
            <div class="row">

                    <div class="col-md-4 col-sm-12">
                        <div class="form-group word_sec">
                        <form data-iscompleted="{{ $forMCheck['is-completed-exp'] }}" id="experince-review" class="review-form {{ $forMCheck['experience-form'] }}" method="post">
                            <label class="title-head">Please Enter Your Review For Experience</label>
                            <div class="rating_top rating-stars">
                                <ul class="stars">
                                    <li class='star' title='Poor' data-value='1'><i class='fa fa-star fa-fw'></i></li>
                                    <li class='star' title='Fair' data-value='2'><i class='fa fa-star fa-fw'></i></li>
                                    <li class='star' title='Good' data-value='3'><i class='fa fa-star fa-fw'></i></li>
                                    <li class='star' title='Excellent' data-value='4'><i class='fa fa-star fa-fw'></i></li>
                                    <li class='star' title='WOW!!!' data-value='5'><i class='fa fa-star fa-fw'></i></li>
                                </ul>
                            </div>
                            <input type="hidden" name="star_value" class="star-value">
                            <input type="hidden" name="experience_id" value="{{ $expid }}">
                            <input type="hidden" name="review_for" value="0">
                            <input type="hidden" name="user_type"  value="0">
                            <textarea  placeholder="Write Review for Experince" id="review-experince-text" name="review_text" class="inner_textarea"></textarea>
                            <div class="error-text"></div>
                              <div class="submit-area">
                            <button data-form="#experince-review" class="btn btn-sucess submitthis" id="review-experince-btn" type="button">Save</button>
                            <div class="review-loader" style="display: none;">
                             <img src="https://media.tenor.com/images/47f855960d5dc83774d7b3b428964c93/tenor.gif">
                            </div>
                            </div>
                        </form>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-12">
                        <div class="form-group word_sec">
                              <form data-iscompleted="{{ $forMCheck['is-completed-host'] }}" id="review-host" class="review-form {{ $forMCheck['host-form'] }}" method="post">
                           <label class="title-head">Please Enter Your Review For Host</label>
                            <div class="rating_top rating-stars">
                                <ul class="stars">
                                    <li class='star' title='Poor' data-value='1'><i class='fa fa-star fa-fw'></i></li>
                                    <li class='star' title='Fair' data-value='2'><i class='fa fa-star fa-fw'></i></li>
                                    <li class='star' title='Good' data-value='3'><i class='fa fa-star fa-fw'></i></li>
                                    <li class='star' title='Excellent' data-value='4'><i class='fa fa-star fa-fw'></i></li>
                                    <li class='star' title='WOW!!!' data-value='5'><i class='fa fa-star fa-fw'></i></li>
                                </ul>
                            </div>
                            <input type="hidden" name="star_value" class="star-value">
                            <input type="hidden" name="experience_id" value="{{ $expid }}">
                            <input type="hidden" name="review_for" value="{{ $experience->assigned_host }}">
                            <input type="hidden" name="user_type"  value="4">
                            <textarea  placeholder="Write Review for Host" id="review-review-text" name="review_text" class="inner_textarea"></textarea>
                            <div class="error-text"></div>
                              <div class="submit-area">
                            <button data-form="#review-host" class="btn btn-sucess submitthis"  id="review-review-btn" type="button">Save</button>
                            <div class="review-loader" style="display: none;">
                             <img src="https://media.tenor.com/images/47f855960d5dc83774d7b3b428964c93/tenor.gif">
                            </div>
                            </div>
                        </form>
                        </div>
                    </div> 
                    <div class="col-md-4 col-sm-12">
                        <div class="form-group word_sec">
                              <form data-iscompleted="{{ $forMCheck['is-completed-scout'] }}" id="review-scout" class="review-form {{ $forMCheck['scout-form'] }}"  method="post">
                                <label class="title-head">Please Enter Your Review For Scout</label>
                            <div class="rating_top rating-stars">
                                <ul class="stars">
                                    <li class='star' title='Poor' data-value='1'><i class='fa fa-star fa-fw'></i></li>
                                    <li class='star' title='Fair' data-value='2'><i class='fa fa-star fa-fw'></i></li>
                                    <li class='star' title='Good' data-value='3'><i class='fa fa-star fa-fw'></i></li>
                                    <li class='star' title='Excellent' data-value='4'><i class='fa fa-star fa-fw'></i></li>
                                    <li class='star' title='WOW!!!' data-value='5'><i class='fa fa-star fa-fw'></i></li>
                                </ul>
                            </div>
                            <input type="hidden" name="star_value" class="star-value">
                            <input type="hidden" name="experience_id" value="{{ $expid }}">
                            <input type="hidden" name="review_for" value="{{ $experience->user_id }}">
                            <input type="hidden" name="user_type"  value="3">
                            <textarea placeholder="Write Review for Scout" id="scout-review-text" name="review_text" class="inner_textarea"></textarea>
                            <div class="error-text"></div>
                            <div class="submit-area">
                               <button data-form="#review-scout" class="btn btn-sucess submitthis"  class="btn btn-sucess" id="scout-review-btn" type="button">Save</button>
                            <div class="review-loader" style="display: none;">
                             <img src="https://media.tenor.com/images/47f855960d5dc83774d7b3b428964c93/tenor.gif">
                            </div> 
                            </div> 
                        </form>
                        </div>
                    </div>     
                    
            </div>    
        </div>
    </div>
    
</section>

@endsection
@section('script')
@endsection 
@section('javascript')
<script>
   

$(document).ready(function(){
  
  /* 1. Visualizing things on Hover - See next part for action on click */
  $('.stars li').on('mouseover', function(){
    var onStar = parseInt($(this).data('value'), 10); // The star currently mouse on
   
    // Now highlight all the stars that's not after the current hovered star
    $(this).parent().children('li.star').each(function(e){
      if (e < onStar) {
        $(this).addClass('hover');
      }
      else {
        $(this).removeClass('hover');
      }
    });
    
  }).on('mouseout', function(){
    $(this).parent().children('li.star').each(function(e){
      $(this).removeClass('hover');
    });
  });
  
  
  /* 2. Action to perform on click */
  $('.stars li').on('click', function(){
    $this = $(this); 
    var onStar = parseInt($(this).data('value'), 10); // The star currently selected
    var stars = $(this).parent().children('li.star');
    
    for (i = 0; i < stars.length; i++) {
      $(stars[i]).removeClass('selected');
    }
    
    for (i = 0; i < onStar; i++) {
      $(stars[i]).addClass('selected');
    }
    
    // JUST RESPONSE (Not needed) 

    // var ratingValue = parseInt($('.stars li.selected').last().data('value'), 10);
    var ratingValue = parseInt($this.data('value'), 10);
    var msg = "";
    if (ratingValue > 1) {
        msg = "Thanks! You rated this " + ratingValue + " stars.";   
    }
    else { 
        msg = "We will improve ourselves. You rated this " + ratingValue + " stars.";
    }
    responseMessage(msg,$(this),ratingValue);
    
  });
  
  
});


function responseMessage(msg,evt,ratingValue) {
    
    evt.parents(".review-form").find(".star-value").val(ratingValue);
    evt.parents(".review-form").find("textarea").attr("placeholder",msg); 
}

$(".submitthis").click(function(){

    var formId = $(this).attr("data-form"); 
    var starVal = $(formId).find(".star-value").val();
    var textareaVal = $(formId).find("textarea").val();
    var isvaild,isvaild2=false; 
   
    if(starVal=='') {
        $(formId).find(".rating_top").append("<label class='error'>Please choose rating ..</label>");
    }else{
        isvaild = true;
        $(formId).find(".error").hide();

    }
    if(textareaVal==''){ 
        $(formId).find(".error-text").append("<label class='error'>Please type rating text ..</label>");
    }else{
        isvaild2 = true;
        $(formId).find(".error-text").hide();

    } 
if(isvaild == true && isvaild2==true){
    $(formId).find(".review-loader").show();
    $.ajax({
            headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            type: 'post',
            url: "{{route('review-submission')}}",
            data: $(formId).serialize(),
            dataType: 'json',
            success: function (res) {
            $(formId).find(".review-loader").hide();
                if(res.isSucceeded == 1){
                 $(formId).append("<div class='success'> Thanks for submitting this </div>");
                 $(formId).attr("data-iscompleted",1); 
                 $(formId).fadeOut(1500); 

                 if($("#review-host").attr("data-iscompleted") == 1 && $("#experince-review").attr("data-iscompleted") == 1 && $("#review-scout").attr("data-iscompleted") == 1){
                     $(".innerText").find("h1").text("Thank you for sharing your valuable feedback").fadeIn(1600); 
                     $(".innerText").find("p").text(''); 
                 } 
                   
  
                } 

              
            }
          }); 
    
} 
     
});
</script> 
@endsection 