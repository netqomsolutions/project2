@extends('layouts.app')
@section('content')
 <?php 
    $url = asset('pages/privacy/'.$pageData->page_featured_image); ?>
<section class="inner-banner about-banner" style="background-image: url('<?php echo $url; ?>');">
	<div class="container">
	<div class="row">
		<div class="col-lg-12 col-md-12 col-12">
		<h3>Terms <span class="green-color">of use</span></h3>
		<!--p>{{ $pageData->page_description }}</p-->
		</div>
	</div>
	</div>
	</section>
	
	<section class="privacy-policy">
		<div class="container">
			<div class="row">
				{!! $pageData->page_content !!}
			</div>
		</div>
	</section>
	<!-- partner ends here -->
	@endsection
@section('script')

@endsection