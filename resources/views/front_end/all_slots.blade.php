@extends('layouts.app')
@section('content')
@php 
 $social_link = $experience->user->user_meta->social_links;
 $social_links = json_decode($social_link); 
    $schedule = '';
    $scheduleArray = [];
    if(!empty($experience->experience_schedule->toArray())){
        $schedule = array_column($experience->experience_schedule->toArray(),'start_date');
        $for_view= date('M d',strtotime($experience->experience_schedule[0]->start_date));
        $for_value= date('Y-m-d', strtotime($experience->experience_schedule[0]->start_date));
    }
    else{
       $for_view= date('M d');
       $for_value= date('Y-m-d');
}
    if(!empty($schedule))
    {
     foreach($schedule as $sch)
     {
        $scheduleArray[]=date('j-n-Y',strtotime($sch));
     }
    }


    $schedule=json_encode($scheduleArray,JSON_UNESCAPED_SLASHES);
    $freshExp = json_encode($experience,JSON_UNESCAPED_SLASHES);

@endphp
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<section class="availability">
    <div class="container">
        <div class="row">
            <div class="main-heading w-100">
                <h3>{{$experience->experience_name}}</h3>
            </div>
            <div class="col-md-6 col-sm-12">
            	<div class="guru">
                <div class="select-dates ">
                    <h3>Select date</h3>
                    <input type="hidden" id="freshExpId" value="{{$experience->id}}">
                    <div class="dates-guests d-flex align-items-center">
                        <p class="flex-column align-items-start showDatepicker">DATE<span><input id="datepickerdesktop" value="Add date" class="" autocomplete="off" readonly><input type="hidden" name="booking_date" id="booking_date" value="" autocomplete="off"></span></p>
                        <p id="guest-count" class="flex-column align-items-start guest-field">GUESTS<span class="d-flex align-items-center"><b class="font-weight-normal mr-1" id="dynamic_guest_count">1 guest</b></span></p>
                        <div class="guest_inner_sec" style="display: none;">
                            <ul>
                                <li>
                                    <span class="left_bar">Adults </span>
                                    <span class="right_bar">
                                    <button class="minus-adult-button"><i class="fa fa-minus" aria-hidden="true"></i></button> 
                                    <span>
                                    <input type="number" min="1" max="{{$experience->maximum_participant}}" value="1" id="quantity" name="adults" class="adult-cls" readonly>
                                    <button class="plus-adult-button"><i class="fa fa-plus" aria-hidden="true"></i></button>
                                    </span>
                                    </span>
                                </li>
                                <li>
                                    <span class="left_bar">Children <br>
                                    <span class="text_sec_inner">2-12</span>
                                    </span>
                                    <span class="right_bar">
                                    <button class="minus-children-button"><i class="fa fa-minus" aria-hidden="true"></i></button>
                                    <span>
                                    <input type="number" min="0" name="children" class="child-cls" max="{{$experience->maximum_participant}}" value="0" id="quantity" readonly>
                                    <button class="plus-children-button"><i class="fa fa-plus plus-button" aria-hidden="true"></i></button>
                                    </span>
                                    </span>
                                </li>
                                <li>
                                    <span class="left_bar">Infants <br>
                                    <span class="text_sec_inner">Under 2</span>
                                    </span>
                                    <span class="right_bar">
                                    <button class="minus-infant-button"><i class="fa fa-minus minus-button" aria-hidden="true"></i> </button>
                                    <span>
                                    <input type="number" name="infant" min="0" class="infant-cls" value="0" id="quantity" readonly>
                                    <button class="plus-infant-button"><i class="fa fa-plus plus-button" aria-hidden="true"></i></button>
                                    </span>
                                    </span>
                                </li>
                            </ul><a href="javascript:void(0)" class="save-guest-data">Save</a>
                        </div>
                    </div>
                </div>

                <div class="chat-box fresh-chat">
                    <div class="chat-stick d-flex align-items-center">
                        @if ($experience->user->user_image_path && file_exists(public_path($experience->user->user_image_path)) && $experience->user->user_image)
                        <img src="{{ asset($experience->user->user_image_path.'/'.$experience->user->user_image) }}" alt="Admin" class="" />
                        @else
                        <img src="{{ asset('users/user.png') }}" alt="Admin" class=""  />
                        @endif 
                        <div class="chat-stick-inner pl-4">
                            <h3><span>Scout:</span> <i class="far fa-comment-dots"></i> {{$experience->user->user_fname.' '.$experience->user->user_lname}}</h3>
                            <div class="scout-rating d-flex align-items-center">
                                @if($mainArrRa = array_column($experience->user->review_ratings->toArray(), 'rating'))
                                    @php
                                        $mainArrRa = array_count_values($mainArrRa);                                    
                                        $eachUpeer = array();  $eachLower = array(); $loop = 0;
                                        foreach ($mainArrRa as $key => $val) {
                                            $eachUpeer[] = $key * $val;
                                            $eachLower[] = $val;
                                        }
                                        $ratingValSum = array_sum($eachUpeer);
                                        $ratingNumSum = array_sum($eachLower);
                                        $rating = $ratingValSum / $ratingNumSum;    
                                    @endphp
                                <p class="m-0">{{ round($rating, 2) }}  <i class="fas fa-star" aria-hidden="true"></i> ({{ $ratingNumSum }})</p>
                                  @else
                                  <p class="m-0">0 <i class="fas fa-star" aria-hidden="true"></i> (0)</p>
                                  @endif
                                <ul class="contact-social pl-3 mt-0">
                                   @if(!empty($social_links->facebook_link))  
                                      <li><a target="_blank" href="{{ !empty($social_links->facebook_link) ? $social_links->facebook_link : 'javascript:;' }}"><i class="fab fa-facebook-f" aria-hidden="true"></i></a></li>
                                      @endif
                                      @if(!empty($social_links->instagram_link)) 
                                      <li><a target="_blank" href="{{ !empty($social_links->instagram_link) ? $social_links->instagram_link : 'javascript:;' }}"><i class="fab fa-instagram" aria-hidden="true"></i></a></li>
                                      @endif
                                      @if(!empty($social_links->pinterest_link)) 
                                      <li><a target="_blank" href="{{ !empty($social_links->pinterest_link) ? $social_links->pinterest_link : 'javascript:;' }}"><i class="fab fa-pinterest-p" aria-hidden="true"></i></a></li>
                                      @endif
                                      @if(!empty($social_links->linkedin_link)) 
                                      <li><a target="_blank" href="{{ !empty($social_links->linkedin_link) ? $social_links->linkedin_link : 'javascript:;' }}"><i class="fab fa-linkedin-in" aria-hidden="true"></i></a></li>
                                      @endif
                                      @if(!empty($social_links->twitter_link)) 
                                      <li><a target="_blank" href="{{ !empty($social_links->twitter_link) ? $social_links->twitter_link : 'javascript:;' }}"><i class="fab fa-twitter" aria-hidden="true"></i></a></li>
                                    @endif 
                                </ul>
                            </div>
                            @if(!Auth::user())
                             <a class="btn btn-success green-bg rounded-btn mt-3 contact_all_slot" href="{{ route('login') }}">Contact</a>
                             @else
                            @if(Auth::user()->user_role==2)
                            <a href="{{route('traveler-dashboard',$experience->user->id) }}" class="btn btn-success green-bg rounded-btn mt-3 contact_all_slot">Contact</a>
                            @endif
                            @endif

                        </div>
                    </div>
                </div>
                </div>
            </div>
            <div class="col-md-6 col-sm-12">
                <div class="availability-right pl-lg-5 mt-lg-2 mt-sm-5" id="dynamic-all-slots">

                	<?php $interval = new DateInterval('PT'.$experience->experience_duration.'H'); 
                    $i=0;
                    ?>
                	@forelse ($experience->experience_schedule as $k => $expSchedule)
                    @php
                    $advanceTime=$experience->experience_booking_in_advance_time;
                    $bookingDatetime=$expSchedule->start_date.' '.$expSchedule->start_time;
                    $advanceBookingTime = date('Y-m-d H:i:s', strtotime($bookingDatetime) - 60 * 60 * $advanceTime);
                    $userCurrentDateTime= date('Y-m-d H:i:s');
                     if($experience->experience_type==1){
                        $experience_price= number_format($experience->experience_low_price);
                      }else{
                                        $experience_price= $experience->minimum_participant * $experience->experience_additional_person;
                     }
                    @endphp
                		<div class="availability-right-inner">

                                @if( array_key_exists($k,$experince_new) )
	                            <h5 class="mb-3"> 
                                  {{ date('D, d M',strtotime($experince_new[$k]))}}
                                </h5>
                                @endif 
                               
                        <input type="hidden" id="booking_amount" value="{{$experience_price}}">
                        <input type="hidden" id="booking_total_amount" value="{{$experience_price}}">
                        <input type="hidden" id="booking_discount" value="0">
                       
	                        <?php 
                            $alreadyBookedSlot=[];
                            if(!empty($alreayBookedSlots))
                            {
                           $alreadyBookedSlot=array_column($alreayBookedSlots->toArray(),'slot_id');
                            }?>
	                       
                        <div class="price-choose-sec">
                            <div class="left-price-chse-inr">
                                <p>{{date('h:i a',strtotime($expSchedule->start_time))}}-{{date('h:i a',strtotime($expSchedule->end_time))}} (CET)</p>
                                 <input type="hidden" id="slot-id-{{$i}}" value="{{base64_encode($expSchedule->id)}}">
                                
                                <p class="mb-4">
                                    @if($experience->experience_type==1)
                                       <b class="mr-1"><i class="fas fa-euro-sign"></i>{{ number_format($experience->experience_low_price) }} </b>
                                    @else
                                        <b class="mr-1"><i class="fas fa-euro-sign"></i>{{ $experience->minimum_participant * $experience->experience_additional_person}}</b>
                                   @endif 
                                   {{ ($experience->minimum_participant == 1 ) ? "/person" : '(Price for 1 - '.$experience->minimum_participant.' persons)'  }} 
                                    <!-- {{($experience->experience_price_vailid_for=='null')? '1': '1'}} {{($experience->experience_price_vailid_for=='null' || $experience->experience_price_vailid_for== 1)? 'member': 'members'}} -->
                                </p>
                            </div>
                            <div class="right-price-chse-inr">
                                 @if (Auth::check())

                                @if(in_array($expSchedule->id,$alreadyBookedSlot))
                                <a class="exp-sold-out" data-row="{{$i}}" href="javascript:void(0)" disabled>Sold Out</a>
                                @elseif(strtotime($userCurrentDateTime)> strtotime($advanceBookingTime))
                                <a class="exp-sold-out" data-row="{{$i}}" href="javascript:void(0)" disabled>Timeout</a>
                                @else
                                <a class="book-me-now" data-row="{{$i}}" href="javascript:void(0)">Choose</a>
                                @endif
                              @else
                                 @if(in_array($expSchedule->id,$alreadyBookedSlot))
                                  <a class="exp-sold-out" data-row="{{$i}}" href="javascript:void(0)" disabled>Sold Out</a>
                                   @elseif(strtotime($userCurrentDateTime)> strtotime($advanceBookingTime))
                                <a class="exp-sold-out" data-row="{{$i}}" href="javascript:void(0)" disabled>Timeout</a>
                                @else
                                <a class="" href="{{route('login')}}">Choose</a>
                                @endif
                                @endif

				                            </div>
				                        </div> 
	                    </div>
                        <?php $i++;?>
					@empty
					    <p>There is no more slot for this experience </p>
					@endforelse
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
@section('javascript')
 <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js" ></script>
<script type="text/javascript">
    var exp_id="{{$experience->id}}",
    maxParticipants="{{ $experience->maximum_participant }}";
    var schedule=<?= $schedule;?>;
    var freshExp = <?= $freshExp;?>;
</script>

<script type="text/javascript">
       $(document).click(function(e){
         var container = $(".dates-guests"); 
        // If the target of the click isn't the container
        if(!container.is(e.target) && container.has(e.target).length === 0 ){ 
                $(".guest_inner_sec").slideUp();
        } 
    });
</script>
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="{{ asset('js/experience-payment-page.js') }}"></script>
@endsection