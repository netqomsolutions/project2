<nav class="navbar navbar-expand-md navbar-dark bg-dark custom-nav aos-item fixed-top"  data-aos="fade-down">
    <a class="navbar-brand" href="{{route('/')}}"><img src="{{ asset('front_end/images/logo.png') }}" alt="logo" /></a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#main-menu" aria-controls="main-menu" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="main-menu">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item {{ Request::is('about-us') ? 'active' : ''}}">
                <a class="nav-link" href="{{route('about-us')}}">About Us</a>
            </li>
            <li class="nav-item {{ Request::is('experience-list') ? 'active' : ''}}">
                <a class="nav-link" href="{{route('experiences-list')}}">Experiences</a>
            </li>
            <li class="nav-item {{ Request::is('how-it-works') ? 'active' : ''}}">
                <a class="nav-link" href="{{route('how-it-works')}}">How It Works</a>
            </li>
            <li class="nav-item ">
                <a class="nav-link" href="https://www.tourismfromzero.org/en/about/" target="_blank">Tourism From Zero</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="https://www.tourismfromzero.org/en/podcast/" target="_blank">Podcast</a>
            </li>
            <li class="nav-item {{ Request::is('blogs') ? 'active' : ''}}">
                <a class="nav-link" href="{{route('blogs')}}">Blog </a>
            </li>
            <li class="nav-item {{ Request::is('contact-us') ? 'active' : ''}}">
                <a class="nav-link" href="{{route('contact-us')}}">Contact Us</a>
            </li>
            @if(!Auth::user())
                <li class="nav-item login-btn">
                    <a class="nav-link btn btn-danger pink-bg" href="{{ route('login') }}"><i class="fas fa-unlock-alt"></i> Login</a>
                </li>
                <li class="nav-item register-btn">
                    <a class="nav-link btn btn-success green-bg" href="{{ route('register') }}"><i class="fas fa-user-plus"></i> Register</a>
                </li>
               
            @endif
        </ul>
    </div>
	
    <form class="form-inline my-2 my-md-0">
	 @if(!Auth::user())
        <a href="{{route('scout-register')}}" class="btn btn-success green-bg become-scout"><i class="fas fa-suitcase"></i> Become a Scout</a>
		 @else
             
				 <div class="scout-login position-relative"><a id="scout-after-login" href="javascript:void(0);" class="btn btn-success green-bg become-scout d-inline-flex align-items-center"><span class="scout-img">
                      @if(Auth::user()->user_image!='')
                     @if(filter_var(Auth::user()->user_image, FILTER_VALIDATE_URL) === FALSE)
                     <img src="{{ asset(Auth::user()->user_image_path.'/profile-thumb-'.Auth::user()->user_image) }}" />
                     @else
                     <img src="{{ Auth::user()->user_image}}" />
                     @endif
                     @else
                      <img src="{{asset('users/user.png')}}" />
                     @endif
                 </span> {{Auth::user()->user_fname .' '.Auth::user()->user_lname}} <i class="fas fa-angle-down"></i></a>
		<div class="dropdown-scout">
			<ul>
                @if(Auth::user()->user_role==1)

				<li><a href="{{route('admin-dashboard')}}"><i class="far fa-user-circle"></i>Dashboard</a></li>
                @elseif(Auth::user()->user_role==2)
                <li><a href="{{route('traveler-dashboard')}}"><i class="far fa-user-circle"></i>Dashboard</a></li>
                @elseif(Auth::user()->user_role==3)
                <li><a href="{{route('scout-dashboard')}}"><i class="far fa-user-circle"></i>Dashboard</a></li>
                @else
                <li><a href="{{route('traveler-dashboard')}}"><i class="far fa-user-circle"></i>Dashboard</a></li>
                @endif
				<li><a href="{{ route('logout') }}"><i class="fas fa-sign-out-alt"></i> Logout</a></li>
			</ul>
		</div>
		</div>
		 @endif
    </form>
	
	
</nav>