 @php 
$get_other_language = [];
if(isset($userData) && !empty($userData->user_other_languages)){
    $get_other_language = explode(",",$userData->user_other_languages);
}
if($userData->user_primary_language!=''){  
     if(!in_array($userData->user_primary_language, $get_other_language))
     { 
        array_push($get_other_language, $userData->user_primary_language); 
     }
 }
$select_language = [];

@endphp 

@if(isset($languages) && !empty($languages))                         
@foreach($languages as $language)
@php

if(in_array($language->id,$get_other_language)){
    $select_language[] =$language->name ;
}
$social_links = json_decode($social_link);
@endphp
@endforeach

@endif

 




<div class="d-flex align-items-start justify-content-between">
    <div class="left-scout-img">
        @if ($userData->user_image_path && file_exists(public_path($userData->user_image_path)) && $userData->user_image)
<img src="{{ asset($userData->user_image_path.'/'.$userData->user_image) }}" alt="Admin" class="" />
@else
<img src="{{ asset('users/user.png') }}" alt="Admin" class=""  />
@endif 
    </div>
    <div class="right-scout-info">
        <h3>{{ $userData->user_fname .' '. $userData->user_lname  }}</h3>
        @if($mainArrRa = array_column($userData->review_ratings->toArray(), 'rating'))
            @php
                $eachUpeer = array();  $eachLower = array(); $loop = 0;
                $mainArrRa = array_count_values($mainArrRa);
                foreach ($mainArrRa as $key => $val) {
                  $eachUpeer[] = $key * $val;
                  $eachLower[] = $val;
                }
                $ratingValSum = array_sum($eachUpeer);
                $ratingNumSum = array_sum($eachLower);
                $rating = $ratingValSum / $ratingNumSum;    
            @endphp
        <div class="rating">
            <ul mk="3">
                 {{--Start Rating--}}
            @for ($i = 0; $i < 5; $i++)
                @if (floor($rating) - $i >= 1)
                    {{--Full Start--}}
                    <i class="fas fa-star text-warning"> </i>
                @elseif ($rating - $i > 0)
                    {{--Half Start--}}
                    <i class="fas fa-star-half-alt text-warning"> </i>
                @else
                    {{--Empty Start--}}
                    <i class="far fa-star text-warning"> </i>
                @endif
            @endfor
            {{--End Rating--}}
                <li>{{ $userData->review_ratings_count }} Reviews</li>
            </ul>
            
        </div>
         @else
            <div class="rating">
                <ul>
                    <i class="far fa-star text-warning"> </i>
                    <i class="far fa-star text-warning"> </i>
                    <i class="far fa-star text-warning"> </i>
                    <i class="far fa-star text-warning"> </i>
                    <i class="far fa-star text-warning"> </i>
                    <li>0 Reviews</li>
                </ul>
            </div>                                
            @endif
        <div class="language-spoken">
            <p><strong>Spoken Language :</strong>{{!empty($select_language)? implode(', ',$select_language) : ''}} </p> 
            <!--p><strong>Other Spoken Languages :</strong>{{ implode(',',$select_language) }} </p-->
        </div>
       
    </div>
    </div>
	<p class="limit-word">{{  $userData->about_me }}</p>
    <div class="chat-box">
    <ul class="contact-social">
    @if(!empty($social_links->facebook_link))   
    <li><a target="_blank" href="{{$social_links->facebook_link}}"><i class="fab fa-facebook-f" aria-hidden="true"></i></a></li>
    @endif 
      @if(!empty($social_links->instagram_link))   
    <li><a target="_blank" href="{{ !empty($social_links->instagram_link) ? $social_links->instagram_link : 'javascript:;' }}"><i class="fab fa-instagram" aria-hidden="true"></i></a></li>
    @endif

    @if(!empty($social_links->pinterest_link)) 
    <li><a target="_blank" href="{{ !empty($social_links->pinterest_link) ? $social_links->pinterest_link : 'javascript:;' }}"><i class="fab fa-pinterest-p" aria-hidden="true"></i></a></li>
    @endif
     @if(!empty($social_links->linkedin_link)) 
    <li><a target="_blank" href="{{ !empty($social_links->linkedin_link) ? $social_links->linkedin_link : 'javascript:;' }}"><i class="fab fa-linkedin-in" aria-hidden="true"></i></a></li>
    @endif
      @if(!empty($social_links->twitter_link))   
    <li><a target="_blank" href="{{ !empty($social_links->twitter_link) ? $social_links->twitter_link : 'javascript:;' }}"><i class="fab fa-twitter" aria-hidden="true"></i></a></li>
    @endif
    </ul>
</div>