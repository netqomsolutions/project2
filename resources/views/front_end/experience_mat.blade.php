
@if (isset($experiences))
@if(count($experiences))
    @foreach ($experiences as $key => $experience)
    @php
        $exp_path = '';
        $exp_image = '';
        $exp_title=\Illuminate\Support\Str::limit($experience->experience_name,60);
        
        if(isset($experience) && optional($experience->user)){
            $exp_image = optional($experience->user)->user_image;
            $exp_path = optional($experience->user)->user_image_path;
        }
    @endphp
        <div class="col-lg-6 col-md-6 col-sm-12">
            <div class="exp-box">
                <div class="exp-img"><a href="{{route('experience-detail',['id'=>base64_encode($experience->id)])}}"><img src="{{ asset('pages/experiences/thumbs/list-thumb-'.$experience->experience_feature_image) }}"/></a></div>
                <div class="exp-content">
                    <div class="title"><a href="{{route('experience-detail',['id'=>base64_encode($experience->id)])}}">{{ $exp_title }}</a></div>
                    <div class="rating-price">
                      @php 
                                $rating=0;$ratingValSum=0;$ratingNumSum=0;
                                @endphp
                                @if(!empty($experience->review_ratings))
                                    @php
                                        $eachUpeer = array();  $eachLower = array(); $loop = 0; $mainArrRa = array();
                                        foreach ($experience->review_ratings->toArray() as $key => $val) {
                                            if($val['review_for'] == 0){
                                                $mainArrRa[] = $val['rating'];
                                            }
                                        }
                                        $mainArrRa = array_count_values($mainArrRa);
                                        if(!empty($mainArrRa))
                                        {
                                        foreach ($mainArrRa as $key => $val) {
                                          $eachUpeer[] = $key * $val;
                                          $eachLower[] = $val;
                                        }
                                        $ratingValSum = array_sum($eachUpeer);
                                        $ratingNumSum = array_sum($eachLower);
                                        $rating = $ratingValSum / $ratingNumSum; 
                                        }   
                                    @endphp
                            <div class="rating">
                                <ul>
                                    {{--Start Rating--}}
                                    @for ($i = 0; $i < 5; $i++)
                                    @if (floor($rating) - $i >= 1)
                                    {{--Full Start--}}
                                    <i class="fas fa-star text-warning"> </i>
                                    @elseif ($rating - $i > 0)
                                    {{--Half Start--}}
                                    <i class="fas fa-star-half-alt text-warning"> </i>
                                    @else
                                    {{--Empty Start--}}
                                    <i class="far fa-star text-warning"> </i>
                                    @endif
                                    @endfor
                                    {{--End Rating--}}
                                    <li>{{ $ratingNumSum}} Reviews</li>
                                </ul>
                            </div>
                        @else
                            <div class="rating">
                                <ul>
                                    <i class="far fa-star text-warning"> </i>
                                    <i class="far fa-star text-warning"> </i>
                                    <i class="far fa-star text-warning"> </i>
                                    <i class="far fa-star text-warning"> </i>
                                    <i class="far fa-star text-warning"> </i>
                                    <li>0 Reviews</li>
                                </ul>
                            </div>
                        @endif 


                           @php
                                      $addToFav = "wishlist-span";
                                      $addToFavText = "Login as Traveller";
                                      $userId =""; 
                                      $is_traveler =""; 
                                    @endphp

                                @if(Auth::check()) 

                                 @if(!empty($user) && $user->user_role == 2)
                                    @php
                                     $userId = $user->id; 
                                     $is_traveler = "addToWishList";
                                     $addToFavText = "Add to wishlist";
                                    @endphp
                                  @else
                                   @php
                                    $is_traveler = "notToaddInWishList-notuse";
                                    $userId = 0; 
                                    $addToFavText = "Only for Traveller";
                                    @endphp
                                  @endif 
                                 @endif     
                                    
                               @if(!empty($UserwishList))
                                     @foreach ($UserwishList as $key => $val) 
                                        @if($experience->id == $val->experience_id && $val->is_favourite == 1)
                                         @php
                                                $addToFav = "add-to-fav";
                                                $addToFavText = "In WishList";
                                                @endphp
                                         @endif 
                                    @endforeach
                                @endif
                         
                        <div class="price experince-list-meta">
                            <a data-toggle="tooltip" data-placement="top" title="{{ $addToFavText }}" id="wishlist-show" href="javascript:void(0)" data-default="wishlist-span" data-expid="{{ $experience->id }}" data-id="{{ $userId }}" class="{{ $addToFav }} {{ $is_traveler }}"><i class="far fa-heart"></i></a>
              <span id="wishlist-tooltip"> {{ $addToFavText }} </span>
                        </div>
                    </div>
                    <div class="price-div">
                        <span class="price-info">
                        <!--span class="price-span">€{{ number_format($experience->experience_low_price) }} {{ ($experience->experience_high_price != 0.00 && $experience->experience_type == 3 ) ? "- ".number_format($experience->experience_high_price) : ''  }}</span>
                            @if($experience->experience_type==2 || $experience->experience_price_vailid_for == 1)
                               @if($experience->experience_type==3)
                                   (Where €{{ number_format($experience->experience_low_price) }} are valid for 1 guest)
                               @else
                                   Price For 1 guest
                              @endif
                              @elseif($experience->experience_type==1)
                                  Price For 1 - {{$experience->experience_price_vailid_for}} guests
                              @else
                               @if($experience->experience_type==3)
                                   (Where €{{ number_format($experience->experience_low_price) }} are valid for 1 - {{$experience->experience_price_vailid_for}} guests)
                               @else
                                  Group Price For 1 - {{$experience->experience_price_vailid_for}} guests
                              @endif
                          @endif
                        </span-->
                        <span class="price-info">
                        <span class="price-span"> From 
                        @if($experience->experience_type==1)
                       €{{ number_format($experience->experience_low_price) }} 
                        @else
                       €{{$experience->minimum_participant * $experience->experience_additional_person}}
                       @endif 
                        </span>
                        <span class="{{ ($experience->minimum_participant == 1 ) ? 'person-span' : 'multiperson-span'  }}">
                       {{ ($experience->minimum_participant == 1 ) ? "/person" : '(Price for 1 - '.$experience->minimum_participant.' persons)'  }}
                       </span>
                        </span>
                    </div>
                    <div class="booking-conditions exp-list-front-main">
                        <ul>
                            <li><i class="far fa-user"></i> Max: {{ $experience->maximum_participant }}</li>
                            <li><i class="far fa-clock"></i> {{ $experience->experience_duration}} h</li>
                            @if($experience->extra_person > 0 && $experience->experience_type==2)
                              <li><i class="fa fa-percent exp-list-percent"></i> <b>{{ $experience->extra_person}}+</b> {{($experience->extra_person == 1 ) ? "person" : 'persons'}}</li>
                             @endif 
                            <li class="exp-list-address_limit"><i class="fas fa-map-marker-alt"></i> {{ $experience->experience_lname}}</li>
                        </ul>
                    </div>
          <div class="popup-new-scout" data-user-id="{{ base64_encode($experience->user->id) }}">
                    <a href="{{route('scout-detail',['id'=>base64_encode($experience->user->id)])}}">
                        <div class="scout-box green-bg">
                            <div class="scout-img-name">
                                @if (file_exists(public_path($exp_path)) && $exp_image)
                                <img src="{{ asset($exp_path.'/profile-thumb-'.$exp_image) }}" />
                                @else
                                <img src="{{ asset('users/user.png') }}" />
                                @endif 
                                {{ $experience->user->user_fname .' '. $experience->user->user_lname  }}
                            </div>
                            <div class="scout-rating">
                                @if($mainArrRa = array_column($experience->user->review_ratings->toArray(), 'rating'))
                                    @php
                                        $mainArrRa = array_count_values($mainArrRa);                                    
                                        $eachUpeer = array();  $eachLower = array(); $loop = 0;
                                        foreach ($mainArrRa as $key => $val) {
                                            $eachUpeer[] = $key * $val;
                                            $eachLower[] = $val;
                                        }
                                        $ratingValSum = array_sum($eachUpeer);
                                        $ratingNumSum = array_sum($eachLower);
                                        $rating = $ratingValSum / $ratingNumSum;    
                                    @endphp
                                <p>{{ round($rating, 2) }} <i class="fas fa-star"></i> ({{ $ratingNumSum }})</p>
                                @else
                                <p>0<i class="fas fa-star"></i> (0)</p>
                                @endif
                            </div>
                        </div>
                    </a>
          <div class="scout-main-info dynamic-scout-content">
                       
                                </div>
                </div>
                </div>
            </div>
        </div>

    <!-- single exp-box ends here -->
    @endforeach
    <div class="pagination_sec" style="margin-top: 40px;">
        <div>
            {!! $experiences->links() !!}
        </div>
    </div>
@else
    <div class="col-lg-12">
        <div class="exp-box no-data-found" style="text-align: center;font-weight: 500;">
      <img src="{{asset('images/no-data-found.png')}}" />
            <h3>There is no data related to this filter.Please try more filters.</h3>
        </div>
    </div>
@endif
@endif
<script type="text/javascript">
    $(".popup-new-scout").mouseenter(function(){
   var scout_id= $(this).data('user-id');
   $.ajax({
    headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      },
      url: HOST_URL+"/popup-scout-detail",
      method:"POST",
      data:{scout_id:scout_id},
      success:function(data){
        if(data.isSucceeded){
          
          $('.dynamic-scout-content').html('');
          $('.dynamic-scout-content').html(data.data);
        }else{
          $('.dynamic-scout-content').html(data.data);
        }
      }
  });
   $(".popup-new-scout").mouseleave(function(){
    $('.dynamic-scout-content').html('');
   });
  });
</script>


