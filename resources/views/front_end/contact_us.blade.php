@extends('layouts.app')
@section('content')
<style type="text/css">
	.contact_submit{
	    margin-top: 8px;
	    background-color: #afc578;
	    color: #fff;
	    text-transform: uppercase;
	    font-size: 19px;
	    max-width: 30%;
	}
	.suc{
		color: green;
	}
	.fail{
		color: red;
	}    
</style>
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/css/bootstrap-select.min.css"/>
<?php $url = asset('pages/privacy/'.$contactUs->page_featured_image); ?>
<section class="inner-banner about-banner contact-banner" style="background-image: url('<?php echo $url; ?>')">
	<div class="container">
	<div class="row">
		<div class="col-lg-12 col-md-12 col-12">
		<h3>Contact <span class="green-color">Us</span></h3> 
		</div>
	</div>
	</div>
	</section>
	
	<section class="contact-sec-info">
		<div class="container">
			<div class="row">
				<div class="company-info d-flex justify-content-between">
					<div class="common-info-box company-address">
						<a class="contact-page-address d-flex align-items-center justify-content-center" target="_blank" href="{{$contact->address_link}}"><span><i class="fas fa-map-marker-alt" aria-hidden="true"></i></span>
						<b class="font-weight-normal">{{ $contactUs->address }}<br><b class="font-weight-normal">{{ $contactUs->address_line_2 }}</b></b></a>
					</div>
					<div class="common-info-box phone-no">
						<span><i class="fas fa-phone-alt" aria-hidden="true"></i></span>
						<p><b>Phone:</b> <a href="tel:{{ RemoveFormatPhoneNo($contactUs->phone) }}">{{ $contactUs->phone }}</a></p>
					</div>
					<div class="common-info-box email-address">
						<span><i class="fas fa-envelope" aria-hidden="true"></i></span>
						<p><b>Email:</b> <a href="mailto:{{ $contactUs->email }}">{{ $contactUs->email }}</a></p>
					</div>
				</div>
				<div class="contact-form d-flex justify-content-between">
					<div class="main-heading">
						<h5 class="curly-font green-color">Contact  Us</h5>
						<h3>Have Any Questions? <br>Feel Free To Contact <br>Us.</h3>
						<ul class="contact-social">
						<li><a href="{{(isset($socialLinks) && $socialLinks->facebook!='')? $socialLinks->facebook : '#'}}"><i class="fab fa-facebook-f"></i></a></li>
		                <li><a href="{{(isset($socialLinks) && $socialLinks->instagram!='')? $socialLinks->instagram : '#'}}"><i class="fab fa-instagram"></i></a></li>
		                <li><a href="{{(isset($socialLinks) && $socialLinks->twitter!='')? $socialLinks->twitter : '#'}}"><i class="fab fa-twitter"></i></a></li>
		                <li><a href="{{(isset($socialLinks) && $socialLinks->linkedin!='')? $socialLinks->linkedin : '#'}}"><i class="fab fa-linkedin-in"></i></a></li>
						</ul>
					</div>
					<form method="post" id="contact-us-form" action="">
						<div class="my-error">
							<input type="text" placeholder="Your Name" name="name" id="name" />
						</div>
						<div class="my-error">
							<input type="email" placeholder="Your Email" name="email" id="email" />
						</div>
						<div class="my-error d-flex align-items-start justify-content-between contact-country-code">
							 <select id="CointryCode" name="user_mobile_code" data-size="5" data-live-search="true" class="selectpicker">
							 	<option value=" ">None</option>
                                         @foreach($getCountryCode as $CountryCode)
                                        <option value="{{ $CountryCode->phonecode }}">+{{ $CountryCode->phonecode.' '.$CountryCode->iso }}</option>
                                        @endforeach
                                    </select>
							<input type="text" class="user_mobileMasking" placeholder="Phone Number" name="phone" id="phone" />
						</div>
						<div class="my-error">
							<input type="text" placeholder="Subject" name="subject" id="subject" />
						</div>
						<div class="my-error textarea-form">
							<textarea placeholder="Your Message" name="message" id="message"></textarea>
						</div>
						<input type="submit" class="submit-btn" value="Send Message" />
						<span id="msg" style="display: block;"></span>
					</form>
				</div>
			</div>
		</div>
	</section>
	@endsection
@section('javascript')

<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/js/bootstrap-select.min.js"></script> 
<script src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/3/jquery.inputmask.bundle.js"></script> 
<script type="text/javascript">

	$(document).ready(function () {
		$('select').selectpicker();
		 $(".user_mobileMasking").inputmask("mask", {
         "mask": "(0) 99 999 999"
         });
		$('#contact-us-form').validate({
			submitHandler: function(form) {
			  ajaxSubmit();
			},
		    rules: {
			    name: {
			    	required: true,
			    },
			    email: {
			    	required: true,
			    },
			    subject:{
		          	required: true,
		        },
		        message:{
	                required: true,
	            },
	            
		    },
		    messages: {
		    	name: {
		    		required: "Please enter your name",
		      	},	
	        	email: {	        		
	          		required: "Please enter your email",           
	        	},
	        	subject: {
	          		required : "Please enter your subject",
	       		},
		        message:{
	                required:"Please enter message here",
	            },
	            
		    },
		    errorElement: 'span',		    
		    errorPlacement: function (error, element) {
		      	error.addClass('invalid-feedback');
		      	element.closest('.my-error').append(error);
		    },
		    highlight: function (element, errorClass, validClass) {
		      	$(element).addClass('is-invalid');
		    },
		    unhighlight: function (element, errorClass, validClass) {
		      	$(element).removeClass('is-invalid');
		    }
		});
	});


	function ajaxSubmit(){
		$('.loader').css('display','flex');
		var name = $('#name').val();
		var email = $('#email').val();
		var message = $('#message').val();
		var subject = $('#subject').val();
		var phone = $('#phone').val();
		$.ajax({
	        url: '{{ route("contact-us-post") }}',
	        type: 'POST',
	        data: { name: name ,email: email ,message: message ,phone: phone , subject: subject , "_token": "{{ csrf_token() }}", },
	        success: function(response)
	        {
	        	$('.loader').css('display','none');
	        	$('#contact-us-form')[0].reset();
	        	$('#msg').html(response.message);
	        	if(response.isSucceeded){
	        		$('#msg').removeClass('suc');
	        		$('#msg').removeClass('fail');
	        		$('#msg').addClass('suc');
	        	}else{
	        		$('#msg').removeClass('suc');
	        		$('#msg').removeClass('fail');
	        		$('#msg').addClass('fail');
	        	}	    
	        	$('#msg').fadeIn();
	        	setTimeout(function(){
	        		$('#msg').fadeOut();
	        	},5000)
	        }
	    });
	}
</script>

@endsection