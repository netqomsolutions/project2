<!DOCTYPE html>
<html>
    <head>
        <title>BECOME A SCOUT</title>
    </head>
<body style="margin:0;padding:0;">
    <table cellspacing="0" cellpadding="0" width="600" align="center" style="font-family: Arial, Helvetica, sans-serif;background: url({{ asset('images/email-template-bg.jpg') }});">
        <tr><td style="padding:50px 0 0;" align="center"><img src="logo.png"/></td></tr>
        <tr><td align="center" style="font-size:25px;color:#0d0d0d;letter-spacing: 1px;padding:40px 0 0;">Hi</td></tr>
        <tr><td align="center" style="color:#ff4883;font-weight: bold;font-size: 45px;letter-spacing: 1px;">Lia Grazia</td></tr>
        <tr><td align="center" style="font-size:16px;line-height: 27px;padding: 35px 50px 38px;color:#0d0d0d;">Thank you for signing up for the <span style="color:#91ba2b;">#LocalsFromZero</span> marketplace. Welcome aboard! Enjoy exploring and visiting lesser known places, locals and their authentic experiences. Start making pleasant memories and meaningful connections.</td></tr>
		<tr><td align="center" style="padding:0 0 50px;"><a style="background-color: #ff4883;color: #fff;text-decoration: none;font-size: 15px;padding: 14px;border-radius: 7px;display:inline-block;" href="{{$verify_url}}">Verify Email Address</a></td></tr>
        <tr><td align="center" style="font-weight: bold;font-size:17px;color:#0d0d0d;padding:0 0 20px;">Any additional question?</td></tr>
        <tr>
            <td>
                <table width="100%" style="background: #dfdfdf;padding: 20px 25px 16px;">
                    <tr>
                        <td colspan="2" style="font-weight: bold;font-size:15px;padding:0;"><a style="color:#0d0d0d;text-decoration: none;" href="mailto:locals@tourismfromzero.org"><img src="{{ asset('images/mail-icon-email.png') }}" style="margin: 0 4px 0 0px;position: relative;top: 1px;"> locals@tourismfromzero.org</a></td>
                        <td align="right" colspan="2" style="font-weight: bold;font-size:15px;padding:0;"><a style="color:#0d0d0d;text-decoration: none;" href="tel:+386041825569"><img src="{{ asset('images/phone-icon-email.png') }}" style="margin: 0 4px 0 0px;position: relative;top: 3px;"> +386 (0) 41 825 569</a></td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</body>
</html>