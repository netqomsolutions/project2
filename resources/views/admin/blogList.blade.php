{{-- Extends layout --}}
@extends('layout.default')
{{-- Content --}}
@section('content')
<style type="text/css">
	.dataTables_wrapper .dataTable th:before {content:none !important;}
	.dataTables_wrapper .dataTable th:after {content:none !important;}
</style>
<div class="row">
	 @if(session()->has('success'))
            <div class="alert alert-success alluser-page-suc" style="text-align: left;float: left;width: 100%;">
                {{ session()->get('success') }}
            </div>
            <script type="text/javascript">setTimeout(function(){ $('.alluser-page-suc').fadeOut(); }, 3000);</script>
        @elseif(session()->has('failure'))
            <div class="alert alert-danger alluser-page-suc" style="text-align: left; left;float: left;width: 100%;">
                {{ session()->get('failure') }}
            </div>
            <script type="text/javascript">setTimeout(function(){ $('.alluser-page-suc').fadeOut(); }, 3000);</script>
        @endif
    <div class="col-lg-12">
    	<meta name="csrf-token" content="{{ csrf_token() }}" />
		<script type="text/javascript">setTimeout(function(){ jQuery('.scout-list-success').fadeOut(); }, 3000);</script>
		<div class="card card-custom">
			<div class="card-header">
				<div class="card-title w-100 justify-content-between">
					<div class="d-flex align-items-center">
					<span class="card-icon">
						<i class="flaticon2-supermarket text-primary"></i>
					</span>
					<div class="list_sec">
					<h3 class="card-label">List of Blogs</h3>
				  </div>
					</div>
				  <div class="add-btn">
					<a id="" href="{{route('admin-add-blog')}}" class="btn btn-sm float-right green-bg">Add New <i class="fa fa-plus" aria-hidden="true"></i></a>
				</div>
				</div>
			</div>
			<div class="card-body">
				<!--begin: Datatable-->
				<table class="table table-bordered yajra-datatable table-responsive-sm blog-list-mb" style="margin-top: 13px !important;">
					<thead>
						<tr>
							<th>ID</th>
							<th>Title</th>
							<th>Category</th>
							<th>Actions</th>
						</tr>
					</thead>
				</table>
				<!--end: Datatable-->
			
			</div>
		</div>
		<!--end::Card-->
    </div>
</div>
@endsection
{{-- Scripts Section --}}
@section('scripts')
<script type="text/javascript">
  	$.ajaxSetup({ headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') } });
  	$(document).ready(function(){
  		var table = $('.yajra-datatable').DataTable({
	        processing: true,
	        serverSide: true,
	        ajax: "{{ route('admin-blog-list') }}",
	        columns: [
	            {data: 'DT_RowIndex', id: 'DT_RowIndex',orderable: false,render:function(data,type,row){	       
	            	var imagUrl = '';
	                   
	            		imagUrl = HOST_URL + '/public/pages/blogs/'+row.feature_image;
	            		return "<img src=\"" + imagUrl + "\" height=\"50\"/>";
	            	
	            		
	            	        
	            }},
	            {data: 'blog_title', name: 'blog_title'},
	            {data: 'blog_title', name: 'blog_title',render:function(data,type,row){
	            		return row.blog_cat.name;
	            }},	          
	            {
	                data: 'action', 
	                name: 'action', 
	                orderable: false, 
	                searchable: true
	            },
	        ],
	        order: [[1, 'asc']]
	    });
  	});
 
/* 01-02-2021 @RT Change the delete popup design , using by bootstrap-confirmation.min.js */
  $(document).ready(function(){
    setTimeout(function(){  
      $('.action-blog-delete-btn').confirmation({
            onConfirm: function() {
                var blog_id=$(this).attr('blog-id'); 
	                $.ajax({
			            url: `{{ route("admin-delete-blog") }}`,
			            type: "POST",
			            data: { blog_id : blog_id},
			            dataType: "Json",
			            success: function (data) {
			            toastr.options.timeOut = 1500;
			            if(data.isSucceeded){
			            toastr.success(data.message);
			            $('.yajra-datatable').DataTable().ajax.reload();
			            }else{
			            toastr.error("Please try again");
			            }
			            },
			            error: function (xhr, ajaxOptions, thrownError) {
			            toastr.error("Please try again");
			            }
			         });
                }

     });
     },1000);
});

</script>
@endsection