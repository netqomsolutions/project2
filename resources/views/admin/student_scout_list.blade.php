{{-- Extends layout --}}
@extends('layout.default')
{{-- Content --}}
@section('content')
<style type="text/css">
	.dataTables_wrapper .dataTable th:before {content:none !important;}
	.dataTables_wrapper .dataTable th:after {content:none !important;}
</style>
<div class="row">
    <div class="col-lg-12">
    	<meta name="csrf-token" content="{{ csrf_token() }}" />
       	<!--begin::Card-->
       	@if(session()->has('success'))
            <div class="alert alert-success alluser-page-suc" style="text-align: left;width: 100%;">
                {{ session()->get('success') }}
            </div>
            <script type="text/javascript">setTimeout(function(){ $('.alluser-page-suc').fadeOut(); }, 3000);</script>
        @elseif(session()->has('failure'))
            <div class="alert alert-danger alluser-page-suc" style="text-align: left; left;width: 100%;">
                {{ session()->get('failure') }}
            </div>
            <script type="text/javascript">setTimeout(function(){ $('.alluser-page-suc').fadeOut(); }, 3000);</script>
        @endif
		<script type="text/javascript">setTimeout(function(){ $('.scout-list-success').fadeOut(); }, 3000);</script>
		<div class="card card-custom">
			<div class="card-header">
				<div class="card-title">
					<span class="card-icon">
						<i class="flaticon2-supermarket text-primary"></i>
					</span>
					<h3 class="card-label">List Of Scouts</h3>
				</div>
			</div>
			<div class="card-body">
				<!--begin: Datatable-->
				<table class="table table-bordered yajra-datatable scoutlist-table table-responsive-md" style="margin-top: 13px !important">
					<thead>
						<tr>
							<th>AVATAR</th>
							<th>NAME</th>
							<th>EMAIL</th>
							<th>MOBILE NO.</th>
							<th>Payment</th>
							<th>ACTIONS</th>
						</tr>
					</thead>
				</table>
				<!--end: Datatable-->
			
			</div>
		</div>
		<!--end::Card-->

    </div>
</div>
<!-- /.modal -->
			    <div class="modal fade" id="studentPaymentModal">
			        <div class="modal-dialog">
			            <form id="send-scout-payment-form" class="send-scout-payment" action="{{route('admin-send-payment-scout')}}" method="post" enctype="multipart/form-data">
			                <div class="modal-content">
			                    <div class="modal-header">
			                        <h4 class="modal-title scout-title">Send Payment To Scout</h4>
			                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
			                        <span aria-hidden="true">&times;</span></button>
			                    </div>
			                    <div class="modal-body">
			                       
			                        @csrf
			                        <input type="hidden" name="scout_id" id="scout_id" value="">
			                        <div class="d-flex">
			                        <div class="form-group d-flex position-relative align-items-center payment-full-modal mr-7">
			                        <input type="radio" name="payment_type" value="1" id="full_payment" checked>
			                        <label for="full_payment" class="m-0">Send Full payment</label>
			                        </div>
			                        <div class="form-group d-flex position-relative align-items-center payment-full-modal">
			                        <input type="radio" name="payment_type" value="2" id="partilly_payment">
			                        <label for="partilly_payment" class="m-0">Send Partial Payment</label>
			                    	</div>
			                    	</div>
			                    	<div class="form-group payment-col partial_payment_input_div">
			                        <label>Payment </label>
			                        <input type="text" name="partial_amount" value="" id="partial_payment_input" >              
			                    	</div>
			                    	<div class="form-group total-pay">
			                    		<p>Total <spna class="total-pay-text"></spna></p>
			                    	</div>
			                    	
			                    </div>
			                    <div class="modal-footer justify-content-between">
			                        <button type="button" class="btn btn-outline-light" data-dismiss="modal">Cancel</button>
			                        <button type="button" class="btn btn-outline-light pink-bg-btn" id="pay-payment-button">Pay</button>
			                    </div>
			                </div>
			            </form>
			        </div>
			    </div>
			    <!-- /.modal -->
@endsection
{{-- Scripts Section --}}
@section('scripts')
<script type="text/javascript">
  	
  	$(document).ready(function(){
  		$.ajaxSetup({ headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') } });
  		var table = $('.yajra-datatable').DataTable({
	        processing: true,
	        serverSide: true,
	        ajax: "{{ route('admin-student-scouts') }}",
	        columns: [
	            {data: 'DT_RowIndex', id: 'DT_RowIndex',orderable: false,render:function(data,type,row){	       
	            	var imagUrl = '';
	            	var Status='';
	            	if(row.status == '1'){ 
	                   Status = '<span style=" margin-left: 4px;" class="label label-lg label-light-success active-status label-inline" data-toggle="tooltip" data-placement="right" title="Active"></span>';
	                }else if (row.status == '2') { 
	                    Status = '<span style=" margin-left: 4px;" class="label label-lg label-light-danger inactive-status label-inline"  data-toggle="tooltip" data-placement="right" title="Inactive"></span>';
	                }
	            	if(row.user_image == null){	       
	            		imagUrl = HOST_URL + '/public/users/user.png';
	            		return "<img src=\"" + imagUrl + "\" height=\"50\"/>"+Status;
	            	}else{
	            		imagUrl = HOST_URL + '/public/'+row.user_image_path+'/'+row.user_image;
	            		return "<img src=\"" + imagUrl + "\" height=\"50\"/>" +Status;
	            	}	            
	            }},
	            {data: 'user_fname', name: 'user_fname',render:function(data, type, row){ 
	             return  row.user_fname +' '+ row.user_lname;
	            }},
	            {data: 'email', name: 'email'},
	            {data: 'user_mobile', name: 'user_mobile',render:function(data, type, row){ 
	             return  '+'+row.user_mobile_code +' '+ row.user_mobile;
	            }},
	            {data: 'student_balance', name: 'student_balance',render:function(data, type, row){ 
	            	if(row.user_meta.student_balance==null)
	            	{
	            		return '0.00';
	            	}else{
	            		 return  row.user_meta.student_balance;
	            	}
	            
	            }},
	            {
	                data: 'action', 
	                name: 'action', 
	                orderable: false, 
	                searchable: true
	            },
	        ],
	        order: [[1, 'asc']],
            fnDrawCallback: function( oSettings ) {
              /* 01-02-2021 @RT */
              $('[data-toggle="tooltip"]').tooltip();
              $('.action-scout-btn').confirmation({
              	   content: "You want to delete this.",
		            onConfirm: function() {
		                  var scout_id=$(this).attr('scout_id');
		                  return false;
					          $.ajax({
					            url: `{{ route("admin-delete-scout") }}`,
					            type: "POST",
					            data: { scout_id : scout_id},
					            dataType: "Json",
					            success: function (data) {
					            toastr.options.timeOut = 1500;
					            if(data.isSucceeded){
					            swal.close();
					            toastr.success(data.message);
					            $('.yajra-datatable').DataTable().ajax.reload();
					            }else{
					            swal("Error updating!", "Please try again", "error");
					            }
					            },
					            error: function (xhr, ajaxOptions, thrownError) {
					            swal("Error updating!", "Please try again", "error");
					            }
					           });
		                }

			     });
            }
	    });
	    $(document).on("click","input[type=radio]",function(){
            var val = $(this).val();
           if(val==2)
           {
             $(".partial_payment_input_div").css("display","flex");
           }else{
           	$(".partial_payment_input_div").css("display","none");
           }
	    });
	    $(document).on("keyup change","#partial_payment_input",function(){
          var val= $(this).val();
          $(".total-pay-text").text(val);

	    });
	    $("#send-scout-payment-form").validate({
            rules: {
                partial_amount:{
                  required: function(){
                        if($("input[name=payment_type]:checked").val()==2)
                        {
                           return true; 
                       }else{
                           return false;
                       }
                    },
                    number: true,
                }, 
            }
           
        });
         $("body").on('click', '#pay-payment-button', function() {
                if($("#send-scout-payment-form").valid())
        {
            let myForm = document.getElementById('send-scout-payment-form');
            myForm.submit();
        }
    });
     $('#studentPaymentModal').on('hidden.bs.modal', function () {
        $(this).find('form').trigger('reset');
         $("#scout_id").val(' ');
        });
       $(document).on("click",".scout-pay-button",function(){

            var scout_id= $(this).attr('scout_id');
        $.ajax({
          type: 'POST',
           url: "{{ route('admin-scout-by-id') }}",
          data: {_token: "{{ csrf_token() }}", 'id':scout_id},
          dataType: "Json",
            success: function(resultData) { 
            	//console.log(resultData);
                $("#scout_id").val(scout_id);
                //console.log(resultData.userData.user_meta.student_balance);
                if(resultData.userData.user_meta.student_balance==null)
                {
                	var student_balance=0.00;
                }else{
                	var student_balance=resultData.userData.user_meta.student_balance;
                }
                $(".total-pay-text").text(student_balance);
            }
        });
           
    });

	
  	});
</script>
@endsection