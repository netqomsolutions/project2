{{-- Extends layout --}}
@extends('layout.default')
{{-- Content --}}
@section('content')
 
 @php
  $languagearrya=[];
 
  foreach($languages as $language)  {
     $languagearrya[$language->id]=$language->name; 
  } 
  
  $ability_skills = unserialize($scoutData->user_meta->ability_skills);
  $ability_skills_lable= [
                         'personal_travel ' => 'Using online resources for personal travel needs',
                         'social_media ' => 'Managing social media accounts',
                         'reservation_systems ' => 'Using reservation systems' ,
                         'managing_reservation ' => 'Managing reservation systems' ,
                         'mobile_travel_app ' => 'Using mobile travel applications' ,
                         'developing_mobile_travel_app ' => 'Developing mobile travel applications' ,
                         'photography ' => 'Photography' ,
                         'video_production ' => 'Video production',
                         '360_pictures_video_production ' => '360 pictures and video production' ,
                         'personal_deadline_management ' => 'Personal time/deadliness management' ,
                         'team_management ' =>'Team management'];
  $ability_skills_val= [
                         '1' => 'No skills',
                         '2' => 'Some skills',
                         '3' => 'Average skills' ,
                         '4' => 'Above average skills' ,
                         '5' => ' Professional level skills' ];
 
@endphp  
<style>
  .toggle.ios, .toggle-on.ios, .toggle-off.ios { border-radius: 20rem; }
  .toggle.ios .toggle-handle { border-radius: 20rem; }
  div#action-scout {
      align-items: center;
      justify-content: center;
  }
  .form-group.row.contact-scout-form {
    align-items: center;
    margin-bottom: 5px;
}
.contact-scout-form .col-form-label {
    font-weight: 500;
}
</style>
 
<!--begin::Container-->
<div class="container">
  <!--begin::Profile 4-->
  <div class="d-flex flex-row">
    <!--begin::Aside-->
    <div class="flex-row-auto offcanvas-mobile w-300px w-xl-350px" id="kt_profile_aside">
      <!--begin::Card-->
      <div class="card card-custom gutter-b">
        <!--begin::Body-->
        <div class="card-body pt-4">
          <!--begin::Toolbar--> 
          <!--end::Toolbar-->
          <!--begin::User-->
          <div class="d-flex align-items-center">
            <div class="symbol symbol-60 symbol-xxl-100 mr-5 align-self-start align-self-xxl-center">
                @if ($scoutData->user_image_path && file_exists(public_path($scoutData->user_image_path)) && $scoutData->user_image) 
                 <div class="symbol-label" style="background-image:url('{{ asset($scoutData->user_image_path.'/'.$scoutData->user_image) }}')"></div>
              @else
                  <div class="symbol-label" style="background-image:url('{{ asset('users/user.png') }}')"></div> 
              @endif 
            
              <i class="symbol-badge bg-success"></i>
            </div>
            <div>
              <a href="#" class="font-weight-bold font-size-h5 text-dark-75 text-hover-primary"> 
               {{ $scoutData->user_fname .' '. $scoutData->user_lname}}</a> 
              <div class="mt-2">
                <button class="btn btn-outline-primary pink-bg-btn act-sc scoutApproveAction"   data-action="1">Confirmation Status</button> 
              </div>
            </div>
          </div>
          <!--end::User-->
          <!--begin::Contact-->
          <div class="pt-8 pb-6">
            <div class="d-flex align-items-center justify-content-between mb-2">
              <span class="font-weight-bold mr-2">Email:</span>
              <a href="mailto:{{ $scoutData->email}}" class="text-muted text-hover-primary">{{ $scoutData->email}}</a>
            </div>
            <div class="d-flex align-items-center justify-content-between mb-2">
              <span class="font-weight-bold mr-2">Phone:</span>
              <span class="text-muted">{{ formatPhoneNo($scoutData->user_mobile,$scoutData->user_mobile_code)}}</span>
            </div> 
          </div>
          <!--end::Contact--> 
        </div>
        <!--end::Body-->
      </div>
      <!--end::Card--> 
    </div>
    <!--end::Aside-->
    <!--begin::Content-->
    <div class="flex-row-fluid ml-lg-8">
   <!--begin::Advance Table Widget 8-->
      <div class="card card-custom gutter-b">
        <!--begin::Header-->
        <div class="card-header  py-5">
          <h3 class="card-title align-items-start flex-column">
            <span class="card-label font-weight-bolder text-dark">Information</span> 
          </h3>
          <div class="card-toolbar">
             <div class="mr-2">
                 <span data-href="/admin/export-csv/{{ base64_encode($scoutData->id)}}" id="export" class="btn btn-sm btn-text btn-light-danger text-uppercase font-weight-bold pink-bg-btn" onclick="exportTasks(event.target);">Export</span>
            </div>
          </div>
        </div>
        <!--end::Header-->
        <!--begin::Body--> 
        <div class="card pt-0 pb-3">
            <div class="card-body"> 
 
               <div class="form-group row contact-scout-form">
                 <label class="col-xl-3 col-lg-3 col-form-label">Full Name</label>
                 <div class="col-lg-9 col-xl-6">
                  <div class="input-group input-group-lg"> {{ $scoutData->user_fname .' '. $scoutData->user_lname}}</div> 
                 </div>
              </div>
               <div class="form-group row contact-scout-form">
                 <label class="col-xl-3 col-lg-3 col-form-label">Email</label>
                 <div class="col-lg-9 col-xl-6">
                  <div class="input-group input-group-lg">{{ $scoutData->email}}</div> 
                 </div>
              </div>
               <div class="form-group row contact-scout-form">
                 <label class="col-xl-3 col-lg-3 col-form-label">Date of birth</label>
                 <div class="col-lg-9 col-xl-6">
                  <div class="input-group input-group-lg"> {{ $scoutData->user_meta->dob}}</div> 
                 </div>
              </div>
              <div class="form-group row contact-scout-form">
                 <label class="col-xl-3 col-lg-3 col-form-label">Type of Account</label>
                 <div class="col-lg-9 col-xl-6">
                  <div class="input-group input-group-lg"> {{ ($scoutData->user_meta->account_type==0)? 'Scout' : 'Student'}}</div> 
                 </div>
              </div>
               <div class="form-group row contact-scout-form">
                 <label class="col-xl-3 col-lg-3 col-form-label">Status</label>
                 <div class="col-lg-9 col-xl-6">
                  <div class="input-group input-group-lg">
                            <?php if($scoutData->status == 0 || $scoutData->status ==2 ){
                                  echo "Inactive";
                                }else if($scoutData->status == 1){
                                  echo "Active";
                                } ?>
                  </div> 
                 </div>
              </div> 
               <div class="form-group row contact-scout-form">
                 <label class="col-xl-3 col-lg-3 col-form-label">Gender</label>
                 <div class="col-lg-9 col-xl-6">
                  <div class="input-group input-group-lg">
                            <?php if($scoutData->user_meta->gender ==2 ){
                                  echo "Female";
                                }else if($scoutData->user_meta->gender == 1){
                                  echo "Male";
                                } ?>
                  </div> 
                 </div>
              </div> 
               <div class="form-group row contact-scout-form">
                 <label class="col-xl-3 col-lg-3 col-form-label">Address</label>
                 <div class="col-lg-9 col-xl-6">
                  <div class="input-group input-group-lg"> {{ $scoutData->user_address ? $scoutData->user_address  : 'Not available'}}</div> 
                 </div>
              </div>
               <div class="form-group row contact-scout-form">
                 <label class="col-xl-3 col-lg-3 col-form-label">Place of residence</label>
                 <div class="col-lg-9 col-xl-6">
                  <div class="input-group input-group-lg"> {{ $scoutData->user_meta->user_residence}}</div> 
                 </div>
              </div>
              @if($scoutData->user_role==3) 

                <div class="row">
                 <div class="col-lg-12 col-xl-12">
                  <h5 class="font-weight-bold mt-10 heading-scout-form">Addtional Information</h5>
                 </div>
                </div> 
               <div class="form-group row contact-scout-form">
                 <label class="col-xl-3 col-lg-3 col-form-label">English level</label>
                 <div class="col-lg-9 col-xl-6">
                  <div class="input-group input-group-lg">  {{ $scoutData->user_english_level}}</div> 
                 </div>
              </div>
               <div class="form-group row contact-scout-form">
                 <label class="col-xl-3 col-lg-3 col-form-label">Primary language</label>
                 <div class="col-lg-9 col-xl-6">
                  <div class="input-group input-group-lg">
                  @php 
                    $get_primary_language = '';
                    if(isset($scoutData) && !empty($scoutData->user_primary_language)){
                    echo $languagearrya[$scoutData->user_primary_language];  
                     }
                  @endphp
                  </div> 
                 </div>
              </div>
               <div class="form-group row contact-scout-form">
                 <label class="col-xl-3 col-lg-3 col-form-label">Other languages</label>
                 <div class="col-lg-9 col-xl-6">
                  <div class="input-group input-group-lg"> 
                    @php 
                      $get_other_language = [];
                      if(isset($scoutData) && !empty($scoutData->user_other_languages)){
                        $get_other_language = explode(",",$scoutData->user_other_languages);
                      }                        
                       foreach($get_other_language as $language){
                       echo $languagearrya[$language] .', ';
                      } 
                     @endphp    
                    </div> 
                 </div>
              </div>
               <div class="form-group row contact-scout-form">
                 <label class="col-xl-3 col-lg-3 col-form-label">Your current status</label>
                 <div class="col-lg-9 col-xl-6">
                  <div class="input-group input-group-lg">  {{ $scoutData->user_meta->current_status}}</div> 
                 </div>
              </div>
               <div class="form-group row contact-scout-form">
                 <label class="col-xl-12 col-lg-12 col-form-label">Why would you join us?</label>
                 <div class="col-lg-12 col-xl-12">
                  <div class="input-group input-group-lg">  {{ $scoutData->user_meta->why_join_us}}</div> 
                 </div>
              </div>
               <div class="form-group row contact-scout-form">
                 <label class="col-xl-12 col-lg-12 col-form-label">Briefly describe two unique experiences in your region that first came to your mind as the right "#LFZ scouting material". </label>
                 <div class="col-lg-12 col-xl-12">
                  <div class="input-group input-group-lg">  {{ $scoutData->user_meta->unique_experiences}}</div> 
                 </div>
              </div>
               <div class="form-group row contact-scout-form">
                 <label class="col-xl-12 col-lg-12 col-form-label">What are your personal and/or professional areas of interest?</label>
                 <div class="col-lg-12 col-xl-12">
                  <div class="input-group input-group-lg">  {{ $scoutData->user_meta->user_interest}}</div> 
                 </div>
              </div>
                <div class="row">
                 <div class="col-lg-12 col-xl-12">
                  <h5 class="font-weight-bold mt-10 heading-scout-form">How would you rate your ability in the fields of *</h5>
                 </div>
                </div> 
                @foreach($ability_skills as $key=>$val)
               <div class="form-group row contact-scout-form">
                 <label class="col-xl-6 col-lg-6 col-form-label">{{ $ability_skills_lable[$key]}}</label>
                 <div class="col-lg-6 col-xl-6">
                  <div class="input-group input-group-lg"> {{$ability_skills_val[$val]}} </div> 
                 </div>
              </div>
              @endforeach 

               <div class="form-group row contact-scout-form">
                 <label class="col-xl-6 col-lg-6 col-form-label">What describes Scout best (Group 1)</label>
                 <div class="col-lg-6 col-xl-6">
                  <div class="input-group input-group-lg">  {{ $scoutData->user_meta->describes_you_best}} </div> 
                 </div>
              </div>
               <div class="form-group row contact-scout-form">
                 <label class="col-xl-6 col-lg-6 col-form-label">What describes Scout best (Group 2)</label>
                 <div class="col-lg-6 col-xl-6">
                  <div class="input-group input-group-lg">  {{ $scoutData->user_meta->describes_you_best_2}} </div> 
                 </div>
              </div>
               <div class="form-group row contact-scout-form">
                 <label class="col-xl-12 col-lg-12 col-form-label">Describe yourself</label>
                 <div class="col-lg-12 col-xl-12">
                  <div class="input-group input-group-lg">  {{ $scoutData->about_me}} </div> 
                 </div>
              </div>
              @endif
               
              
          </div>
            </div> 
          </div> 
        <!--end::Body-->
      </div>
      <!--end::Advance Table Widget 8-->
    </div>
    <!--end::Content-->
  </div> 
<!--end::Container-->
<!-- /.modal -->
  <div class="modal fade" id="action-scout">
      <div class="modal-dialog">
          <form id="action-scout-form" action="" method="post">
              <div class="modal-content">
                  <div class="modal-header">
                      <h4 class="modal-title scout-title" style="text-align: center;"></h4>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span></button>
                  </div>
                  <div class="modal-body">
                      <p id="cnt-modal"></p>
                      <span id="dynInput"></span>
                      @csrf
                      <input type="hidden" name="status" id="status" value="{{ $scoutData->status }}">              
                      <input type="hidden" name="scout_id" id="scout_id" value="<?php echo base64_encode($scoutData->id)?>">              
                  </div>
                  <div class="modal-footer justify-content-between">
                      <button type="button" class="btn btn-secondary no-action" data-dismiss="modal">No</button>
                      <button type="button" class="btn btn-secondary"data-dismiss="modal" onclick="formSubmit()">Yes</button>
                  </div>
              </div>
          </form>
      </div>
  </div>
  <!-- /.modal --> 
@endsection
{{-- Scripts Section --}}
@section('scripts')
<script type="text/javascript">
 
    function formSubmit(){
    $("#action-scout-form").submit();
  }

  /* 01-02-2021 @RT */ 
 $(document).ready(function(){
  $.ajaxSetup({ headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') } });
    $('.scoutApproveAction').confirmation({
      title           : 'Are you sure?',
      btnOkLabel      : 'Approve',
      btnCancelLabel  : 'Reject',
    onConfirm: function() {
      var scout_id='<?php echo base64_encode($scoutData->id)?>';
      var status=1;
      scoutConfirmationAction(scout_id,status);
    },
    onCancel: function(){
      var scout_id='<?php echo base64_encode($scoutData->id)?>';
      var status=3; 
      scoutConfirmationAction(scout_id,status);
    }
  }); 
});
function scoutConfirmationAction(scout_id,status) {
     var url = '',title = '', cnt_modal = '', pub_val = ''; 
     url = '{{ route("admin-update-scout-request-status") }}';   
      if(status == 1){         
           $('#status').val(1);
      }else if(status == 3){ 
           $('#status').val(3);
      }
      $("#action-scout-form").attr('action', url);
      formSubmit(); 
  }
</script>
<script>
   function exportTasks(_this) {
      let _url = $(_this).data('href');
      window.location.href = _url;
   }
</script>
@endsection