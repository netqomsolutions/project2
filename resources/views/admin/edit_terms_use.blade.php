{{-- Extends layout --}}
@extends('layout.default')
<script src="https://cdn.ckeditor.com/4.12.1/standard/ckeditor.js"></script>
{{-- Content --}}
@section('content')
<div class="row">
	 @if(session()->has('success'))
            <div class="alert alert-success alluser-page-suc" style="text-align: left;float: left;width: 100%;">
                {{ session()->get('success') }}
            </div>
            <script type="text/javascript">setTimeout(function(){ $('.alluser-page-suc').fadeOut(); }, 3000);</script>
        @elseif(session()->has('failure'))
            <div class="alert alert-danger alluser-page-suc" style="text-align: left; left;float: left;width: 100%;">
                {{ session()->get('failure') }}
            </div>
            <script type="text/javascript">setTimeout(function(){ $('.alluser-page-suc').fadeOut(); }, 3000);</script>
        @endif
    <div class="col-lg-12">
        <div class="card card-custom">
			<!--begin::Form-->
			<form class="form" id="page-privacy-policy" method="post" enctype="multipart/form-data" action="{{ route('admin-update-cms-pages') }}">
				<div class="card-body">@csrf
					<div class="form-group row">
						<input type="hidden" name="page_id" value="2"/>
						<div class="col-lg-6 my-error">
							<label>Page Title:</label>
							<input type="text" class="form-control" value="{{ $PageData->page_title }}" name="page_title" id="page_title" placeholder="Enter page title">
						</div>
						<div class="col-lg-6 my-error">
							<label>Page Slug:</label>
							<input type="text" class="form-control" value="{{ $PageData->page_slug }}"  name="page_slug" id="page_slug" placeholder="Enter slug">
						</div>						
					</div>
					<div class="form-group row">
						<div class="col-lg-6 my-error">
							<label>Page Meta:</label>
							<input type="text" class="form-control" value="{{ $PageData->page_meta }}" name="page_meta" id="page_meta" placeholder="Enter page meta">
						</div>						
						<div class="col-lg-6 my-error"> <label class="cms_banner_dimension w-100 d-flex justify-content-between flex-wrap">Banner Image: <span>Please upload image of 1600 X 430 dimension.</span></label> 
							<input type="file" name="page_featured_image"  value="{{  $PageData->page_featured_image }}" id="page_featured_image" class="form-control" placeholder="Browse Image">
						</div>
					</div>
				
					<div class="form-group row">
						<div class="col-lg-12 my-error">
							<label>Page Description:</label>
							<input type="text" class="form-control" value="{{ $PageData->page_description }}" name="page_description" id="page_description" placeholder="Enter page description">
						</div>
					</div>
					<div class="form-group row">
						<div class="col-lg-12 my-error">
							<label>Content:</label>
							<div class="input-group">
								<textarea name="page_content"  id="page_content">{{ $PageData->page_content }}</textarea>
							</div>
						</div>
					</div>
					<div class="form-group row">
						<div class="col-lg-6">
							<button type="submit" class="btn green-bg mr-2">Save</button>
							<!-- <button type="back" class="btn btn-secondary">Back</button> -->
						</div>						
					</div>
					
				</div>
			</form>
			<!--end::Form-->
		</div>
	</div>
</div>
@endsection
{{-- Scripts Section --}}
@section('scripts')
<script type="text/javascript">
$(document).ready(function () {
	$('#page-privacy-policy').validate({
		ignore: [],
		submitHandler: function(form) {
		  // do other things for a valid form
		  form.submit();
		},
	    rules: {
		    page_slug: {
		    	required: true
		    },
		    page_meta: {
		    	required: true,
		    },
		    page_title:{
	          	required: true,
	        },
	        page_description:{
	          	required: true
	        },
	        page_content:{
                required: function() {
                 	CKEDITOR.instances.page_content.updateElement();
                },
                minlength:10
            }
	    },
	    messages: {
	    	page_slug: {
	    		required: "Please enter a page slug",
	      	},	
        	page_description: {	        		
          		required : "Please enter a page description",
        	},
        	page_title: {
          		required : "Please enter a page title",
       		},
        	page_meta: {
          		required : "Please enter page meta",
        	},
	        page_content:{
                required:"Please enter content here",
                minlength:"Please enter 10 characters"
            }
	    },
	    errorElement: 'span',		    
	    errorPlacement: function (error, element) {
	      error.addClass('invalid-feedback');
	      element.closest('.my-error').append(error);
	    },
	    highlight: function (element, errorClass, validClass) {
	      $(element).addClass('is-invalid');
	    },
	    unhighlight: function (element, errorClass, validClass) {
	      $(element).removeClass('is-invalid');
	    }
	});
});

// For ckeditor call 
CKEDITOR.replace('page_content', {
    filebrowserUploadUrl: "{{route('ckeditor.upload', ['_token' => csrf_token() ])}}",
    filebrowserUploadMethod: 'form'
});
</script>
@endsection