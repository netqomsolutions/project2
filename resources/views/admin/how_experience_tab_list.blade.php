<div class="row">
    <div class="col-lg-12">
    	<meta name="csrf-token" content="{{ csrf_token() }}" />
      
		 <div class="section-heading-home text-center">
                    <h3> Experience Providers Tab Management</h3>
                   </div>
		<div class="card card-custom">
			<div class="card-header">
				<div class="card-title justify-content-between w-100 flex-column-sm">
					<div class="left-cart-label d-flex align-items-center">
					<span class="card-icon">
						<i class="flaticon2-supermarket text-primary"></i>
					</span>
					<h3 class="card-label">Experience Providers Tab Listing </h3>
				</div>
					<button type="button" class="btn btn-sm float-right green-bg action-add-edit-btn"  list-id=" " page-name="experience_tab" act-name="add-blog-list" data-toggle="modal" data-target="#add-scout-list" data-id="0">Add New</button>
				</div>
			</div>
			<div class="card-body">
				
				<!--begin: Datatable-->
				<table class="table table-bordered yajra-datatable-2" style="margin-top: 13px !important">
					<thead>
						<tr>
							<th>List Title</th>
							<th>Actions</th>
						</tr>
					</thead>
				</table>
				<!--end: Datatable-->
				<!-- /.modal -->
			</div>
		</div>
	</div>
</div>
			  