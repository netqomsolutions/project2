	<div class="row">
    <div class="col-lg-12">
    	<meta name="csrf-token" content="{{ csrf_token() }}" />
      
		 <div class="section-heading-home text-center">
                    <h3> Coordinators Management</h3>
                   </div>
		<div class="card card-custom">
			<div class="card-header">
				<div class="card-title justify-content-between w-100 flex-column-sm">
					<div class="left-cart-label d-flex align-items-center">
					<span class="card-icon">
						<i class="flaticon2-supermarket text-primary"></i>
					</span>
					<h3 class="card-label">Coordinators Listing </h3>
				</div>
					<a href="{{route('admin-add-coordinators')}}" class="btn btn-sm float-right green-bg action-add-edit-btn" data-id="0">Add New</a>
				</div>
			</div>
			<div class="card-body table-responsive table-coordinators">
				
				<!--begin: Datatable-->
				<table class="table table-bordered yajra-datatable2 table-responsive-sm" style="margin-top: 13px !important">
					<thead>
						<tr>
							<th>AVATAR</th>
							<th>NAME</th>
							<th>POSITION</th>
							<th>IS SCOUT</th>
							<th>ACTION</th> 
						</tr>
					</thead>
				</table>
			
			</div>
		</div>
		<!--end::Card-->
    </div>
</div>