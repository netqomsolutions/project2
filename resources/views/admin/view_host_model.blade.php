     @php
        $languagearrya=[];
       
        foreach($languages as $language)  {
           $languagearrya[$language->id]=$language->name; 
        } 

         $legal_requirements=[ '1'=>'HACCP - Hazard analysis and critical control points',
                               '2'=>'Guding licence',
                               '3'=>'Other'
                               ] ;
     @endphp                                
 <div class="row gutters-sm"><div class="col-md-4 mb-3">
  <div class="card"><div class="card-body">
    <div class="d-flex flex-column align-items-center text-center view-host-img-sec">
          @if ($data->user_image_path && file_exists(public_path($data->user_image_path)) && $data->user_image)
            <img src="{{ asset($data->user_image_path.'/profile-thumb-'.$data->user_image) }}" alt="Admin" class="rounded-circle" width="150" />
          @else
            <img src="{{ asset('users/user.png') }}" alt="Admin" class="rounded-circle" width="150" />
          @endif  

                   <div class="mt-3"> <p class="text-secondary mb-1 host-email " ><a href="mailto:{{$data->email}}"><i class="la la-envelope"></i> {{$data->email}}</a></p>
                                      <p class="text-secondary mb-1 host-mobile " ><a href="tel:{{RemoveFormatPhoneNo($data->user_mobile,$data->user_mobile_code)}}"><i class="la la-phone"></i> {{formatPhoneNo($data->user_mobile,$data->user_mobile_code)}}</a></p>
                                      <p class="text-secondary mb-1 host-address"><a><i class="la la-map-marker"></i> {{$data->user_address}}</a></p>
                                              <p class="text-muted font-size-sm"></p>  
                                            </div> 
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="col-md-8">
                                      <div class="card mb-3">
                                        <div class="card-body admin-host-list">  
                                          <div class="row m-0">
                                            <div class="col-sm-5">
                                              <h6 class="mb-0">Added By</h6>
                                            </div>
                                            <div class="col-sm-7 text-secondary host-scout">{{$data->scout->user_fname}}  {{$data->scout->user_lname}}</div>
                                          </div>
                                          <hr>
                                          <div class="row m-0">
                                            <div class="col-sm-5">
                                              <h6 class="mb-0">Company/Organisation</h6>
                                            </div>
                                            <div class="col-sm-7 text-secondary host-mobile">{{$data->user_meta->company_name}}</div>
                                          </div>
                                          <hr>
                                          <div class="row m-0">
                                            <div class="col-sm-5">
                                              <h6 class="mb-0">Website </h6>
                                            </div>
                                            <div class="col-sm-7 text-secondary host-mobile">{{$data->user_meta->company_web_link}}</div>
                                          </div>
                                          <hr>
                                          <div class="row m-0">
                                            <div class="col-sm-5">
                                              <h6 class="mb-0">Registration number</h6>
                                            </div>
                                            <div class="col-sm-7 text-secondary host-mobile">{{$data->user_meta->company_registration_no}}</div>
                                          </div>
                                          <hr>
                                          <div class="row m-0">
                                            <div class="col-sm-5">
                                              <h6 class="mb-0">Primary language</h6>
                                            </div>
                                            <div class="col-sm-7 text-secondary host-mobile"> 
                                             @php 
                                                $get_primary_language = '';
                                                if(isset($data) && !empty($data->user_primary_language)){
                                                echo $languagearrya[$data->user_primary_language];  
                                                 }
                                              @endphp
                                              
                                           </div>
                                          </div>
                                          <hr>
                                          <div class="row m-0">
                                              <div class="col-sm-5">
                                                <h6 class="mb-0">Other languages</h6>
                                              </div>
                                              <div class="col-sm-7 text-secondary host-mobile">
                                                @php 
                                                  $get_other_language = [];
                                                  if(isset($data) && !empty($data->user_other_languages)){
                                                      $get_other_language = explode(",",$data->user_other_languages);
                                                  }                        
                                               foreach($get_other_language as $language){
                                                 echo $languagearrya[$language] .', ';
                                                } 
                                                 @endphp 
                                               </div>
                                          </div>
                                           @if(!empty($data->user_meta->legal_requirements) )
                                          <hr>
                                          <div class="row m-0">
                                            <div class="col-sm-5">
                                              <h6 class="mb-0">Legal Requirements</h6>
                                            </div>
                                            <div class="col-sm-7 text-secondary host-mobile">
                                               @php 
                                                  $get_other_language = [];
                                                  if(isset($data) && !empty($data->user_meta->legal_requirements)){
                                                      $get_legal_requirements = explode(",",$data->user_meta->legal_requirements);
                                                  }                        
                                                 foreach($get_legal_requirements as $requirement){
                                                   echo $legal_requirements[$requirement] .'<br>';
                                                  } 
                                                 @endphp  </div>
                                          </div>
                                          @endif
                                          @if(!empty($data->user_meta->guiding_text) )
                                          <hr>
                                          <div class="row m-0">
                                            <div class="col-sm-5">
                                              <h6 class="mb-0">Guding licence</h6>
                                            </div>
                                            <div class="col-sm-7 text-secondary host-mobile">{{$data->user_meta->guiding_text}} </div>
                                          </div>
                                          <hr>
                                          @endif
                                          @if(!empty($data->user_meta->other_text) )
                                          <div class="row m-0">
                                            <div class="col-sm-5">
                                              <h6 class="mb-0">Other</h6>
                                            </div>
                                            <div class="col-sm-7 text-secondary host-mobile"> {{$data->user_meta->other_text}} </div>
                                          </div>
                                            @endif
                                        </div>
                                      </div>
                                    </div>
                                  </div> 