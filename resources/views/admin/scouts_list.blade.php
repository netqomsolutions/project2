{{-- Extends layout --}}
@extends('layout.default')
{{-- Content --}}
@section('content')
<style type="text/css">
	.dataTables_wrapper .dataTable th:before {content:none !important;}
	.dataTables_wrapper .dataTable th:after {content:none !important;}
</style>
<div class="row">
    <div class="col-lg-12">
    	<meta name="csrf-token" content="{{ csrf_token() }}" />
       	<!--begin::Card-->
       	@if(session()->has('success'))
            <div class="alert alert-success alluser-page-suc" style="text-align: left;width: 100%;">
                {{ session()->get('success') }}
            </div>
            <script type="text/javascript">setTimeout(function(){ $('.alluser-page-suc').fadeOut(); }, 3000);</script>
        @elseif(session()->has('failure'))
            <div class="alert alert-danger alluser-page-suc" style="text-align: left; left;width: 100%;">
                {{ session()->get('failure') }}
            </div>
            <script type="text/javascript">setTimeout(function(){ $('.alluser-page-suc').fadeOut(); }, 3000);</script>
        @endif
		<script type="text/javascript">setTimeout(function(){ $('.scout-list-success').fadeOut(); }, 3000);</script>
		<div class="card card-custom">
			<div class="card-header">
				<div class="card-title">
					<span class="card-icon">
						<i class="flaticon2-supermarket text-primary"></i>
					</span>
					<h3 class="card-label">List Of Scouts</h3>
				</div>
			</div>
			<div class="card-body">
				<!--begin: Datatable-->
				<table class="table table-bordered yajra-datatable scoutlist-table table-responsive-md" style="margin-top: 13px !important">
					<thead>
						<tr>
							<th>AVATAR</th>
							<th>NAME</th>
							<th>EMAIL</th>
							<th>MOBILE NO.</th>
							<th>ACTIONS</th>
						</tr>
					</thead>
				</table>
				<!--end: Datatable-->
			
			</div>
		</div>
		<!--end::Card-->
    </div>
</div>
@endsection
{{-- Scripts Section --}}
@section('scripts')
<script type="text/javascript">
  	
  	$(document).ready(function(){
  		$.ajaxSetup({ headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') } });
  		var table = $('.yajra-datatable').DataTable({
	        processing: true,
	        serverSide: true,
	        ajax: "{{ route('admin-scouts') }}",
	        columns: [
	            {data: 'DT_RowIndex', id: 'DT_RowIndex',orderable: false,render:function(data,type,row){	       
	            	var imagUrl = '';
	            	var Status='';
	            	if(row.status == '1'){ 
	                   Status = '<span style=" margin-left: 4px;" class="label label-lg label-light-success active-status label-inline" data-toggle="tooltip" data-placement="right" title="Active"></span>';
	                }else if (row.status == '2') { 
	                    Status = '<span style=" margin-left: 4px;" class="label label-lg label-light-danger inactive-status label-inline"  data-toggle="tooltip" data-placement="right" title="Inactive"></span>';
	                }
	            	if(row.user_image == null){	       
	            		imagUrl = HOST_URL + '/public/users/user.png';
	            		return "<img src=\"" + imagUrl + "\" height=\"50\"/>"+Status;
	            	}else{
	            		imagUrl = HOST_URL + '/public/'+row.user_image_path+'/profile-thumb-'+row.user_image;
	            		return "<img src=\"" + imagUrl + "\" height=\"50\"/>" +Status;
	            	}	            
	            }},
	            {data: 'user_fname', name: 'user_fname',render:function(data, type, row){ 
	             return  row.user_fname +' '+ row.user_lname;
	            }},
	            {data: 'email', name: 'email'},
	            {data: 'user_mobile', name: 'user_mobile',render:function(data, type, row){ 
	             return  '+'+row.user_mobile_code +' '+ row.user_mobile;
	            }},
	            {
	                data: 'action', 
	                name: 'action', 
	                orderable: false, 
	                searchable: true
	            },
	        ],
	        order: [[1, 'asc']],
            fnDrawCallback: function( oSettings ) {
              /* 01-02-2021 @RT */
              $('[data-toggle="tooltip"]').tooltip();
              $('.action-scout-btn').confirmation({
              	   content: "You want to delete this.",
		            onConfirm: function() {
		                  var scout_id=$(this).attr('scout_id');
		                  return false;
					          $.ajax({
					            url: `{{ route("admin-delete-scout") }}`,
					            type: "POST",
					            data: { scout_id : scout_id},
					            dataType: "Json",
					            success: function (data) {
					            toastr.options.timeOut = 1500;
					            if(data.isSucceeded){
					            swal.close();
					            toastr.success(data.message);
					            $('.yajra-datatable').DataTable().ajax.reload();
					            }else{
					            swal("Error updating!", "Please try again", "error");
					            }
					            },
					            error: function (xhr, ajaxOptions, thrownError) {
					            swal("Error updating!", "Please try again", "error");
					            }
					           });
		                }

			     });
            }
	    });
	
  	});
</script>
@endsection