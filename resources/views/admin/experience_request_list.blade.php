{{-- Extends layout --}}
@extends('layout.default')
{{-- Content --}}
@section('content')
<style type="text/css">
	.dataTables_wrapper .dataTable th:before {content:none !important;}
	.dataTables_wrapper .dataTable th:after {content:none !important;}
</style>
<div class="row">
    <div class="col-lg-12">
    	<meta name="csrf-token" content="{{ csrf_token() }}" />
       	<!--begin::Card-->
       	@if(session()->has('success'))
            <div class="alert alert-success alluser-page-suc" style="text-align: left;float: left;width: 100%;">
                {{ session()->get('success') }}
            </div>
            <script type="text/javascript">setTimeout(function(){ $('.alluser-page-suc').fadeOut(); }, 3000);</script>
        @elseif(session()->has('failure'))
            <div class="alert alert-danger alluser-page-suc" style="text-align: left; left;float: left;width: 100%;">
                {{ session()->get('failure') }}
            </div>
            <script type="text/javascript">setTimeout(function(){ $('.alluser-page-suc').fadeOut(); }, 3000);</script>
        @endif
		<script type="text/javascript">setTimeout(function(){ $('.scout-list-success').fadeOut(); }, 3000);</script>
		<div class="card card-custom">
			<div class="card-header">
				<div class="card-title">
					<span class="card-icon">
						<i class="flaticon2-supermarket text-primary"></i>
					</span>
					<h3 class="card-label">List Of Experiences Approvals</h3>
				</div>
			</div>
			<div class="card-body">
				<!--begin: Datatable-->
				<table class="table table-bordered yajra-datatable table-responsive-md" style="margin-top: 13px !important">
					<thead>
						<tr> 
							<th style="width:185px;">EXPERIENCE NAME</th>
							<th>SCOUT</th>
							<th>HOST</th>
							<th>STATUS</th>
							<th>ACTIONS</th>
						</tr>
					</thead>
				</table>
				<!--end: Datatable-->
				<!-- /.modal -->
			
			    <!-- /.modal -->
			</div>
		</div>
		<!--end::Card-->
    </div>
</div>
        <!-- /. Give the reason to  Scout for reject the exeperience @RT 05.02.2021 --> 

<div class="modal fade add-scout-cls schedule-modal update-schedule" id="scout-commision">
  <div class="modal-dialog"> 
    <div class="modal-content">
      <div class="modal-header">
        <div class="update-main-schedule">
        <h4 class="modal-title">Give Reason</h4>
         
     </div>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>

      <!-- Modal body -->
        <div class="modal-body">
        @csrf

            <div class="d-flex flex-wrap">
              
                <div class="form-group col-md-12 col-sm-12 pl-0" style="">
                    <label class="">Reason </label>
                    <div class="input-group input-group-solid date"> 
                         <textarea name="reason" id="reason" class="form-control form-control-solid"  placeholder="Enter reason"  ></textarea>  
                        <input type="hidden" name="exp_id" id="exp_id" value="">       
                       
                    </div>
                </div>  
                </div> 
            
        </div>

      <!-- Modal footer -->
      <div class="modal-footer">
         <button class="btn btn-outline-light" id="add-schedule-button2"  onclick="changeExpStatusReason()" >Submit</button>
      </div>

    </div> 
  </div>
</div>
@endsection
{{-- Scripts Section --}}
@section('scripts')
<script type="text/javascript">
	$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});
  	
  	$(document).ready(function(){
  		var table = $('.yajra-datatable').DataTable({
	        processing: true,
	        serverSide: true,
	        ajax: "{{ route('admin-experience-approval-list') }}",
	        columns: [ 
	            {data: 'experience_name', name: 'experience_name'},
	            {data: 'scout_user_fname', name: 'scout_user_fname',render:function(data, type, row){ 
	             return row.user ? row.user.user_fname +' '+ row.user.user_lname : 'Not assign';
	            }}, 
	            {data: 'host_user_fname', name: 'host_user_fname',render:function(data, type, row){ 
	             return row.host ? row.host.user_fname +' '+ row.host.user_lname : 'Not assign';
	            }},
	            {data: 'status', name: 'status',render:function(data, type, row){ 
	            	var check='btn-danger';
	            	var status='Ready For Approval';
	            	var exp_id=btoa(row.id);
	                if(row.status==1)
	                {
                      var check='btn-success';
                      var status='Active';
	                }
	                  return `<span data-id=`+exp_id+` class="status_checks btn `+check+`">`+status+`</span>`;
	                
	            }},
	            {
	                data: 'action', 
	                name: 'action', 
	                orderable: false, 
	                searchable: true
	            },
	        ],
	        order: [[1, 'asc']],
	        fnDrawCallback: function( oSettings ) {
		               $('.status_checks').confirmation({
				      	  	title           : 'Are you sure?',
				      	  	content: "You want to change status.",
					    	 btnOkLabel      : 'Approve',
       						 btnCancelLabel  : 'Reject',
				            onConfirm: function() {
				               	 var id = $(this).data('id');
								 var status=1;  
				                 changeExpStatus(id,status);
							
				                },
				                onCancel: function(){
								var id = $(this).data('id');
								$('#exp_id').val('');
								$('#reason').val('');
								$('#exp_id').val(id);  
							    $('#scout-commision').modal('show');
								var status=2;
								///changeExpStatus(id,status);
						}

				     });
		       }
	    }); 
  	});
/* 01-02-2021 @RT Change the delete popup design , using by bootstrap-confirmation.min.js */
 
function changeExpStatus(id,status) {
  	$.ajax({          
			 url: `{{ route('admin-change-experience-app-status') }}`,  
			 type: "POST",  
			 _token: "{{ csrf_token() }}",   
			 data: {          
			 id: id,status:status   
			 },         
			 dataType: "Json", 
		      success: function (data) {
		 	if(data.isSucceeded){
		 		 
              		 
              $('.yajra-datatable').DataTable().ajax.reload();
		 	}else{
              toastr.error("Please try again"); 
		 	}           
		 },          
		 error: function (xhr, ajaxOptions, thrownError) {    
		toastr.error("Please try again");
		 }     
	});
}
function changeExpStatusReason() {
	var reason = $('#reason').val(); 
	var id = $('#exp_id').val(); 
	var status =2; 
  	$.ajax({          
			 url: `{{ route('admin-change-experience-app-status') }}`,  
			 type: "POST",  
			 _token: "{{ csrf_token() }}",   
			 data: {          
			 id: id,status:status,reason:reason   
			 },         
			 dataType: "Json", 
		      success: function (data) {
		 	if(data.isSucceeded){
		 		 $('#scout-commision').modal('hide'); 
              		 
              $('.yajra-datatable').DataTable().ajax.reload();
		 	}else{
              toastr.error("Please try again"); 
		 	}           
		 },          
		 error: function (xhr, ajaxOptions, thrownError) {    
		toastr.error("Please try again");
		 }     
	});
}
</script>
@endsection