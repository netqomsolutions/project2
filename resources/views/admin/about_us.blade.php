{{-- Extends layout --}}
@extends('layout.default')
<script src="https://cdn.ckeditor.com/4.12.1/standard/ckeditor.js"></script>
{{-- Content --}}
@section('content')
<div class="row">
	 @if(session()->has('success'))
            <div class="alert alert-success alluser-page-suc" style="text-align: left;float: left;width: 100%;">
                {{ session()->get('success') }}
            </div>
            <script type="text/javascript">setTimeout(function(){ $('.alluser-page-suc').fadeOut(); }, 3000);</script>
        @elseif(session()->has('failure'))
            <div class="alert alert-danger alluser-page-suc" style="text-align: left; left;float: left;width: 100%;">
                {{ session()->get('failure') }}
            </div>
            <script type="text/javascript">setTimeout(function(){ $('.alluser-page-suc').fadeOut(); }, 3000);</script>
        @endif
    <div class="col-lg-12">
        <div class="card card-custom">
			<!--begin::Form-->
			<form class="form" id="page-about-us" method="post" enctype="multipart/form-data" action="{{ route('admin-update-about-us') }}">
				<div class="card-body">@csrf
					<div class="form-group row">
						<div class="col-lg-6 my-error">
							<label>Page Title:</label>
							<input type="text" class="form-control" value="{{ $aboutPageData->page_title }}" name="page_title" id="page_title" placeholder="Enter page title">
						</div>
						<div class="col-lg-6 my-error">
							<label>Page Slug:</label>
							<input type="text" class="form-control" value="{{ $aboutPageData->page_slug }}"  name="page_slug" id="page_slug" placeholder="Enter slug">
						</div>						
					</div>
					<div class="form-group row">
						<div class="col-lg-6 my-error">
							<label>Page Meta:</label>
							<input type="text" class="form-control" value="{{ $aboutPageData->page_meta }}" name="page_meta" id="page_meta" placeholder="Enter page meta">
						</div>						
						<div class="col-lg-6 my-error">
							<label class="cms_banner_dimension w-100 d-flex justify-content-between flex-wrap">Banner Image: <span>Please upload image of 1600 X 450 dimension.</span></label>
							<input type="file" name="page_featured_image"  value="{{  $aboutPageData->page_featured_image }}" id="page_featured_image" class="form-control" placeholder="Browse Image">
						</div>
					</div>
					<div class="form-group row">
						<div class="col-lg-6 my-error">
							<label class="cms_banner_dimension w-100 d-flex justify-content-between flex-wrap">Section One Image: <span>Please upload image of 800 X 600 dimension.</span></label>
							<input type="file" class="form-control" value="{{  $aboutPageData->about_first_image }}" id="about_first_image" name="about_first_image" class="form-control" placeholder="Browse Image">
						</div>						
						<div class="col-lg-6 my-error">
							<label class="cms_banner_dimension w-100 d-flex justify-content-between flex-wrap">Section Two Image: <span>Please upload image of 800 X 600 dimension.</span></label>
							<input type="file" name="about_second_image"   value="{{  $aboutPageData->about_second_image }}" id="about_second_image" class="form-control" placeholder="Browse Image">
						</div>
					</div>
					<div class="form-group row">
						<div class="col-lg-12 my-error">
							<label>Page Description:</label>
							<input type="text" class="form-control" value="{{ $aboutPageData->page_description }}" name="page_description" id="page_description" placeholder="Enter page description">
						</div>
					</div>
					<div class="form-group row">
						<div class="col-lg-12 my-error">
							<label>How it works Content:</label>
								<textarea name="page_content"  id="page_content">{{ $aboutPageData->page_content }}</textarea>
						</div>
					</div>
					<div class="form-group row">
						<div class="col-lg-12 my-error">
							<label>How it all started Content:</label>
							
								<textarea name="how_it_started"  id="how_it_started">{{ $aboutPageData->how_it_started }}</textarea>
						</div>
					</div>	
					<div class="form-group row">
						<div class="col-lg-12 my-error">
							<label>Our Story Content:</label>
								<textarea name="our_story"  id="our_story">{{ $aboutPageData->our_story }}</textarea>
						</div>
					</div>
					<div class="form-group row">
						<div class="col-lg-6">
							<button type="submit" class="btn green-bg mr-2">Save</button>
							<!--button type="back" class="btn btn-secondary">Back</button-->
						</div>						
					</div>
					
				</div>
			</form>
			<!--end::Form-->
		</div>
	</div>
</div>
@include('admin.admin_testimonial_list')
@include('admin.coordinators_list')
@endsection
{{-- Scripts Section --}}
@section('scripts')
<script type="text/javascript">
CKEDITOR.replace('page_content');
CKEDITOR.replace('how_it_started');
CKEDITOR.replace('our_story');
$(document).ready(function () {
	$.ajaxSetup({ headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') } });
	$('#page-about-us').validate({
		ignore: [],
		submitHandler: function(form) {
		  // do other things for a valid form
		  form.submit();
		},
	    rules: {
		    page_slug: {
		    	required: true
		    },
		    page_meta: {
		    	required: true,
		    },
		    page_title:{
	          	required: true,
	        },
	        page_description:{
	          	required: true
	        },
	        page_content:{
                required: function() {
                 	CKEDITOR.instances.page_content.updateElement();
                },
                minlength:300
            },
            how_it_started:{
                required: function() {
                 	CKEDITOR.instances.how_it_started.updateElement();
                },
                maxlength:1000
            },
             our_story:{
                required: function() {
                 	CKEDITOR.instances.our_story.updateElement();
                },
                maxlength:1000
            },
	    },
	    messages: {
	    	page_slug: {
	    		required: "Please enter a page slug",
	      	},	
        	page_description: {	        		
          		required : "Please enter a page description",
        	},
        	page_title: {
          		required : "Please enter a page title",
       		},
        	page_meta: {
          		required : "Please enter page meta",
        	},
	        page_content:{
                required:"Please enter content here",
                minlength:"Please enter 300 characters"
            }
	    },
	    errorElement: 'span',		    
	    errorPlacement: function (error, element) {
	      error.addClass('invalid-feedback');
	      element.closest('.my-error').append(error);
	    },
	    highlight: function (element, errorClass, validClass) {
	      $(element).addClass('is-invalid');
	    },
	    unhighlight: function (element, errorClass, validClass) {
	      $(element).removeClass('is-invalid');
	    }
	});
	$.validator.addMethod('filesize', function (value, element, param) {
		return this.optional(element) || (element.files[0].size <= param)
	}, 'File size must be less than {0}');
	$('#add-update-testimonial').validate({
		ignore: [],
		submitHandler: function(form) {
		  // do other things for a valid form
		  form.submit();
		},
	    rules: {
		    title: {
		    	required: true
		    },
		    person_name: {
		    	required: true
		    },
		    profile_image: {
		    	// required: function(){
       //              if($("#old_img").val()!=''){
       //                  return false;
       //              }else{
       //                  return true;
       //              }
       //          },
                extension: "jpg,jpeg,png",
                filesize: 1*1024*1024,
		    },
		    description:{
	          	required: true,
	          	maxlength:300
	        },
	    },
	    
	});
	  var table = $('.yajra-datatable').DataTable({
            processing: true,
            serverSide: true,
            ajax: "{{ route('admin-testimonials') }}",
            columns: [
                {data: 'DT_RowIndex', id: 'DT_RowIndex',orderable: false,render:function(data,type,row){	       
	            	var imagUrl = '';
	            	if(row.profile_image == null){	       
	            		imagUrl = HOST_URL + '/public/users/user.png';
	            		return "<img src=\"" + imagUrl + "\" height=\"50\"/>";
	            	}else{
	            		imagUrl = HOST_URL + '/public/pages/testimonialUser/'+row.profile_image;
	            		return "<img src=\"" + imagUrl + "\" height=\"50\"/>";
	            	}	 
	            		
	            	        
	            }},
                {data: 'person_name', name: 'person_name'},
                {data: 'title', name: 'title'},
                {
                    data: 'action', 
                    name: 'action', 
                    orderable: false, 
                    searchable: true
                },
            ],
            order: [[1, 'asc']],
             fnDrawCallback: function( oSettings ) {
             	/* 01-02-2021 @RT Change the delete popup design , using by bootstrap-confirmation.min.js */

			      $('.action-testimonial-delete-btn').confirmation({
			      	    content: "You want to delete this.",
			            onConfirm: function() {
				             var testimonial_id=$(this).attr('testimonial-id');
				              $.ajax({
						            url: `{{ route("admin-delete-testimonial") }}`,
						            type: "POST",
						            data: { testimonial_id : testimonial_id},
						            dataType: "Json",
						            success: function (data) {
						            toastr.options.timeOut = 1500;
						            if(data.isSucceeded){
						            toastr.success(data.message);
						            $('.yajra-datatable, .yajra-datatable-2').DataTable().ajax.reload();
						            }else{
						            toastr.error("Please try again");
						            }
						            },
						            error: function (xhr, ajaxOptions, thrownError) {
						           toastr.error("Please try again");
						            }
						        });
				            }

				     });
             }
    });
	  $(document).on("click",".action-add-edit-btn",function(){
            var url = '',title = '', cnt_modal = '', pub_val = '';
            if($(this).attr('act-name') == 'edit-testimonial' && $(this).attr('testimonial-id')!=''){

            var testimonial_id= $(this).attr('testimonial-id');
        $.ajax({
          type: 'POST',
          url: "{{ route('admin-get-testimonial') }}",
          data: {_token: "{{ csrf_token() }}", 'id':testimonial_id},
          dataType: "Json",
            success: function(resultData) { 
                $("#title").val(resultData.data.title);
                $("#description").val(resultData.data.description);
                $("#person_name").val(resultData.data.person_name);
                $("#old_img").val(resultData.data.profile_image);
                $("#testimonial_id").val(testimonial_id);
            }
        });
                title = 'Update New Testimonial';
            }
            else{
                    title = 'Add New Testimonial';
            }
            $('.modal-title').html(title);   
           
    });
	  $(document).ready(function(){
  		var table = $('.yajra-datatable2').DataTable({
	        processing: true,
	        serverSide: true,
	        ajax: "{{ route('admin-coordinator-list') }}",
	        columns: [
	            {data: 'DT_RowIndex', id: 'DT_RowIndex',orderable: false,render:function(data,type,row){	       
	            	var imagUrl = '';
	            	if(row.user_image == null){	       
	            		imagUrl = HOST_URL + '/public/users/user.png';
	            		return "<img src=\"" + imagUrl + "\" height=\"50\"/ width=\"50\"/>";
	            	}else{
	            		imagUrl = HOST_URL + '/public/users/coordinator/'+row.user_image;
	            		return "<img src=\"" + imagUrl + "\" height=\"50\"/ width=\"50\"/>";
	            	}	            
	            }}, 
	            {data: 'coordinator_user_fname', name: 'coordinator_user_lname',render:function(data, type, row){ 
	             return  row.user_fname +' '+ row.user_lname;
	            }},
	            {data: 'postion', name: 'postion',render:function(data, type, row){ 
	             return  row.user_meta.current_status;
	            }},
	            {data: 'is_scout', name: 'is_scout',render:function(data, type, row){ 
	            	console.log(row.is_scout);
	                var statusLeadColor = '';
	                var leadStatusName = '';
	                if(row.is_scout == '1'){
	                    statusLeadColor = 'label-light-success';
	                    leadStatusName = 'Yes';
	                }else if (row.is_scout == '0') {
	                    statusLeadColor = 'label-light-danger';
	                    leadStatusName = 'No';
	                }
	                return '<span class="label label-lg '+statusLeadColor+' label-inline">'+leadStatusName +'</span>' 
	            }},
                {
                    data: 'action', 
                    name: 'action', 
                    orderable: false, 
                    searchable: true
                }, 
	            
	             
	           
	        ],
	        order: [[1, 'asc']],
             fnDrawCallback: function( oSettings ) {
             	/* 22-02-2021 @RT  */
             	  $('.action-coordinator-delete-btn').confirmation({
			      	    content: "You want to delete this.",
			            onConfirm: function() {
				             var coordinator_id=$(this).attr('coordinator-id');
				              $.ajax({
						            url: `{{ route("admin-delete-coordinator") }}`,
						            type: "POST",
						            data: { coordinator_id : coordinator_id},
						            dataType: "Json",
						            success: function (data) {
						            toastr.options.timeOut = 1500;
						            if(data.isSucceeded){
						            toastr.success(data.message);
						            $('.yajra-datatable, .yajra-datatable2').DataTable().ajax.reload();
						            }else{
						            toastr.error("Please try again");
						            }
						            },
						            error: function (xhr, ajaxOptions, thrownError) {
						           toastr.error("Please try again");
						            }
						        });
				            }

				     });
             }
	    });

 
});
	});
     function formSubmit(){
         $("#delete-testimonial-form").submit();
        }
     $('#add-testimonial-modal').on('hidden.bs.modal', function () {
    $(this).find('form').trigger('reset');
     $("#testimonial_id").val(' ');
     $("#old_img").val(' ');
     })

 
</script>
@endsection