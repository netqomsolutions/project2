{{-- Extends layout --}}
@extends('layout.default')
{{-- Content --}}
@section('content')
<style>
  .toggle.ios, .toggle-on.ios, .toggle-off.ios { border-radius: 20rem; }
  .toggle.ios .toggle-handle { border-radius: 20rem; }
  div#action-scout {
      align-items: center;
      justify-content: center;
  }
  .form-group.row.contact-scout-form {
    align-items: center;
    margin-bottom: 5px;
}
.contact-scout-form .col-form-label {
    font-weight: 500;
}
</style>
 
<!--begin::Container-->
<div class="container">
  <!--begin::Profile 4-->
  <div class="d-flex flex-row"> 
    <!--begin::Content-->
    <div class="flex-row-fluid ml-lg-12">
   <!--begin::Advance Table Widget 8-->
      <div class="card card-custom gutter-b">
        <!--begin::Header-->
        <div class="card-header py-5 align-items-center">
          <h3 class="card-title align-items-start flex-column col-xl-2 pl-0 d-none-responsive">
            <span class="card-label font-weight-bolder text-dark">Experience</span> 
          </h3>
          <h3 class="card-title align-items-start flex-column col-xl-3 pl-1 d-none-responsive">
            <span class="card-label font-weight-bolder text-dark">Old Data</span> 
          </h3>
          <h3 class="card-title align-items-start flex-column col-xl-3 pl-2 d-none-responsive">
            <span class="card-label font-weight-bolder text-dark">New Data</span> 
          </h3>
          <div class="card-toolbar">
               <div class="mr-2"> 
               <span data-id='{{ base64_encode($experience->id)}}' class="status_checks btn pink-bg-btn" >Action</span>
                               </div>
          </div>
        </div>
        <!--end::Header-->
        <!--begin::Body--> 
        <div class="card pt-0 pb-3">
            <div class="card-body">
            
              
               <div class="form-group row contact-scout-form">
                 <label class="col-xl-3 col-lg-12 col-form-label">What is the name of the Experience?</label>
                   <div class="col-lg-12 col-xl-4 old-data-responsive">
                        <div class="input-group input-group-lg"> {{$experience->experience_name}}</div> 
                   </div>
                   <div class="col-lg-12 col-xl-4">
                     <div class="input-group input-group-lg"> {{$experienceAprroval->experience_name}}</div> 
                  </div>
              </div>
               <div class="form-group row contact-scout-form gray-bg">
                 <label class="col-xl-3 col-lg-12 col-form-label">Describe the experience</label>
                   <div class="col-lg-12 col-xl-4 old-data-responsive">
                        <div class="input-group input-group-lg"> {{$experience->experience_description}}</div> 
                   </div>
                   <div class="col-lg-12 col-xl-4">
                     <div class="input-group input-group-lg"> {{$experienceAprroval->experience_description}}</div> 
                  </div>
              </div>
               <!--div class="form-group row contact-scout-form">
                 <label class="col-xl-3 col-lg-3 col-form-label">Pricing strategies</label>
                   <div class="col-lg-12 col-xl-4">
                        <div class="input-group input-group-lg"> {{$experience->experience_type}}</div> 
                   </div>
                   <div class="col-lg-12 col-xl-4">
                     <div class="input-group input-group-lg"> {{$experienceAprroval->experience_type}}</div> 
                  </div>
              </div--> 
               <div class="form-group row contact-scout-form min-num-part">
                 <label class="col-xl-3 col-lg-12 col-form-label">Min number of participants </label>
                   <div class="col-lg-12 col-xl-4 old-data-responsive">
                        <div class="input-group input-group-lg"> 
                         <i class="text-euro">€</i> <span><span class="price-inner">{{ number_format($experience->experience_low_price) }} {{ ($experience->experience_high_price != 0.00 && $experience->experience_type == 3 )? "- ".number_format($experience->experience_high_price) : ''  }}</span> 

                        @if($experience->experience_type==2 || $experience->experience_price_vailid_for == 1)
                        @if($experience->experience_type == 3)
                        <b>(Where €{{ number_format($experience->experience_low_price) }} are valid for 1 guest)  </b>
                        @else
                        valid for:<b> 1 guest</b>
                        @endif
                        @else
                        @if($experience->experience_type == 3)
                        <b>(Where €{{ number_format($experience->experience_low_price) }} are valid for 1 - {{$experience->experience_price_vailid_for}} guests)  </b>
                        @else
                        valid for:<b> 1 - {{$experience->experience_price_vailid_for}} guests</b>
                        @endif
                        @endif
                      </span>  

                        </div> 
                   </div>
                   <div class="col-lg-12 col-xl-4">
                     <div class="input-group input-group-lg">  <i class="text-euro">€</i> <span><span class="price-inner">{{ number_format($experienceAprroval->experience_low_price) }} {{ ($experienceAprroval->experience_high_price != 0.00 && $experienceAprroval->experience_type == 3 )? "- ".number_format($experienceAprroval->experience_high_price) : ''  }}</span> 

                                @if($experienceAprroval->experience_type==2 || $experienceAprroval->experience_price_vailid_for == 1)
                                @if($experienceAprroval->experience_type == 3)
                                <b>(Where €{{ number_format($experienceAprroval->experience_low_price) }} are valid for 1 guest)  </b>
                                @else
                                valid for:<b> 1 guest</b>
                                @endif
                                @else
                                @if($experienceAprroval->experience_type == 3)
                                <b>(Where €{{ number_format($experienceAprroval->experience_low_price) }} are valid for 1 - {{$experienceAprroval->experience_price_vailid_for}} guests)  </b>
                                @else
                                valid for:<b> 1 - {{$experienceAprroval->experience_price_vailid_for}} guests</b>
                                @endif
                                @endif
                              </span> 
                      
                    </div> 
                  </div>
              </div> 
                <div class="form-group row contact-scout-form gray-bg">
                 <label class="col-xl-3 col-lg-12 col-form-label">Extra guest:</label>
                   <div class="col-lg-12 col-xl-4 old-data-responsive">
                        <div class="input-group input-group-lg"> {{$experience->experience_additional_person}} EUR</div> 
                   </div>
                   <div class="col-lg-12 col-xl-4">
                     <div class="input-group input-group-lg"> {{$experienceAprroval->experience_additional_person }} EUR</div> 
                  </div>
              </div> 
                <div class="form-group row contact-scout-form">
                 <label class="col-xl-3 col-lg-12 col-form-label">Max guests: </label>
                   <div class="col-lg-12 col-xl-4 old-data-responsive">
                        <div class="input-group input-group-lg"> {{$experience->maximum_participant}}</div> 
                   </div>
                   <div class="col-lg-12 col-xl-4">
                     <div class="input-group input-group-lg"> {{$experienceAprroval->maximum_participant }}</div> 
                  </div>
              </div> 
                <div class="form-group row contact-scout-form gray-bg">
                 <label class="col-xl-3 col-lg-12 col-form-label">Duration:</label>
                   <div class="col-lg-12 col-xl-4 old-data-responsive">
                        <div class="input-group input-group-lg"> {{$experience->experience_duration}}</div> 
                   </div>
                   <div class="col-lg-12 col-xl-4">
                     <div class="input-group input-group-lg"> {{$experienceAprroval->experience_duration }}</div> 
                  </div>
              </div> 
                <div class="form-group row contact-scout-form">
                 <label class="col-xl-3 col-lg-12 col-form-label">Book in advance:</label>
                   <div class="col-lg-12 col-xl-4 old-data-responsive">
                        <div class="input-group input-group-lg"> {{$experience->experience_booking_in_advance_time}}</div> 
                   </div>
                   <div class="col-lg-12 col-xl-4">
                     <div class="input-group input-group-lg"> {{$experienceAprroval->experience_booking_in_advance_time }}</div> 
                  </div>
              </div> 
              <div class="form-group row contact-scout-form  gray-bg">
                 <label class="col-xl-3 col-lg-12 col-form-label">Experience Location:</label>
                   <div class="col-lg-12 col-xl-4 old-data-responsive">
                        <div class="input-group input-group-lg"> {{$experience->experience_lname}}</div> 
                   </div>
                   <div class="col-lg-12 col-xl-4">
                     <div class="input-group input-group-lg"> {{$experienceAprroval->experience_lname}}</div> 
                  </div>
              </div> 
              @if(!empty($experience->experience_meta_detail))

               <div class="form-group row contact-scout-form gray-bg">
                 <label class="col-xl-3 col-lg-12 col-form-label">Experience Meet Location:</label>
                   <div class="col-lg-12 col-xl-4 old-data-responsive">
                        <div class="input-group input-group-lg"> {{$experience->experience_meta_detail->meet_location}}</div> 
                   </div>
                   <div class="col-lg-12 col-xl-4">
                     <div class="input-group input-group-lg"> {{$experienceAprroval->experience_meta_detail->meet_location}}</div> 
                  </div>
              </div> 
               <div class="form-group row contact-scout-form gray-bg">
                 <label class="col-xl-3 col-lg-12 col-form-label">Price includes:</label>
                   <div class="col-lg-12 col-xl-4 old-data-responsive">
                        <div class="input-group input-group-lg"> {{$experience->experience_meta_detail->exp_price_included}}</div> 
                   </div>
                   <div class="col-lg-12 col-xl-4">
                     <div class="input-group input-group-lg"> {{$experienceAprroval->experience_meta_detail->exp_price_included}}</div> 
                  </div>
              </div> 
               <div class="form-group row contact-scout-form">
                 <label class="col-xl-3 col-lg-12 col-form-label">Price excludes:</label>
                   <div class="col-lg-12 col-xl-4 old-data-responsive">
                        <div class="input-group input-group-lg"> {{$experience->experience_meta_detail->exp_price_excluded}}</div> 
                   </div>
                   <div class="col-lg-12 col-xl-4">
                     <div class="input-group input-group-lg"> {{$experienceAprroval->experience_meta_detail->exp_price_excluded}}</div> 
                  </div>
              </div> 
               <div class="form-group row contact-scout-form  gray-bg">
                 <label class="col-xl-3 col-lg-12 col-form-label">More information about the experience:</label>
                   <div class="col-lg-12 col-xl-4 old-data-responsive">
                        <div class="input-group input-group-lg"> {{$experience->experience_meta_detail->exp_additional_information}}</div> 
                   </div>
                   <div class="col-lg-12 col-xl-4">
                     <div class="input-group input-group-lg"> {{$experienceAprroval->experience_meta_detail->exp_additional_information}}</div> 
                  </div>
              </div> 
               <div class="form-group row contact-scout-form">
                 <label class="col-xl-3 col-lg-12 col-form-label">More information about the provider:</label>
                   <div class="col-lg-12 col-xl-4 old-data-responsive">
                        <div class="input-group input-group-lg"> {{$experience->experience_meta_detail->provider_description}}</div> 
                   </div>
                   <div class="col-lg-12 col-xl-4">
                     <div class="input-group input-group-lg"> {{$experienceAprroval->experience_meta_detail->provider_description}}</div> 
                  </div>
              </div> 
               <div class="form-group row contact-scout-form  gray-bg">
                 <label class="col-xl-3 col-lg-12 col-form-label">Other info:</label>
                   <div class="col-lg-12 col-xl-4 old-data-responsive">
                        <div class="input-group input-group-lg"> {{$experience->experience_meta_detail->exp_other_info}}</div> 
                   </div>
                   <div class="col-lg-12 col-xl-4">
                     <div class="input-group input-group-lg"> {{$experienceAprroval->experience_meta_detail->exp_other_info}}</div> 
                  </div>
              </div> 
               @endif
            </div>
            </div> 
          </div> 
        <!--end::Body-->
      </div>
 
      <!--end::Advance Table Widget 8-->
    </div>
    <!--end::Content-->
  </div>  
<!--end::Container-->
         <!-- /. Give the reason to  Scout for reject the exeperience @RT 05.02.2021 --> 

<div class="modal fade add-scout-cls schedule-modal update-schedule" id="scout-commision">
  <div class="modal-dialog"> 
    <div class="modal-content">
      <div class="modal-header">
        <div class="update-main-schedule">
        <h4 class="modal-title">Give Reason</h4>
         
     </div>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>

      <!-- Modal body -->
        <div class="modal-body">
        @csrf

            <div class="d-flex flex-wrap">
              
                <div class="form-group col-md-12 col-sm-12 pl-0" style="">
                    <label class="">Reason </label>
                    <div class="input-group input-group-solid date"> 
                         <textarea name="reason" id="reason" class="form-control form-control-solid"  placeholder="Enter reason"  ></textarea>  
                        <input type="hidden" name="exp_id" id="exp_id" value="">       
                       
                    </div>
                </div>  
                </div> 
            
        </div>

      <!-- Modal footer -->
      <div class="modal-footer">
         <button class="btn btn-outline-light" id="add-schedule-button2"  onclick="changeExpStatusReason()" >Submit</button>
      </div>

    </div> 
  </div>
</div>
@endsection
{{-- Scripts Section --}}
@section('scripts')
 <script type="text/javascript">
    $.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});
    
   var path = HOST_URL+'/';
    $('.status_checks').confirmation({
       title           : 'Are you sure?',
       content: "You want to change status.",
       btnOkLabel      : 'Approve',
       btnCancelLabel  : 'Reject',
      onConfirm: function() {
           var id = $(this).data('id'); 
            var status=1;  
           changeExpStatus(id,status);

          },
         onCancel: function(){
                var id = $(this).data('id');
                $('#exp_id').val('');
                $('#reason').val('');
                $('#exp_id').val(id);  
                  $('#scout-commision').modal('show');
                var status=2;
                ///changeExpStatus(id,status);
            }

});
  function changeExpStatus(id,status) {
     toastr.options.timeOut = 1500;
    $.ajax({          
       url: `{{ route('admin-change-experience-app-status') }}`,  
       type: "POST", 
         headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },  
       data: {          
       id: id,status:status   
       },         
       dataType: "Json", 
          success: function (data) {
      if(data.isSucceeded){
          toastr.success(data.message); 
         setTimeout(function(){ 
           window.location.href = path+"admin/experience-approval-list";  
        }, 1000); 
      }else{
              toastr.error("Please try again"); 
      }           
     },          
     error: function (xhr, ajaxOptions, thrownError) {    
    toastr.error("Please try again");
     }     
  });
}
function changeExpStatusReason() {
  var reason = $('#reason').val(); 
  var id = $('#exp_id').val(); 
  var status =2; 
    $.ajax({          
       url: `{{ route('admin-change-experience-app-status') }}`,  
       type: "POST",  
       _token: "{{ csrf_token() }}",   
       data: {          
       id: id,status:status,reason:reason   
       },         
       dataType: "Json", 
          success: function (data) {
      if(data.isSucceeded){
         $('#scout-commision').modal('hide'); 
                   
              $('.yajra-datatable').DataTable().ajax.reload();
      }else{
              toastr.error("Please try again"); 
      }           
     },          
     error: function (xhr, ajaxOptions, thrownError) {    
    toastr.error("Please try again");
     }     
  });
}
  </script>
@endsection