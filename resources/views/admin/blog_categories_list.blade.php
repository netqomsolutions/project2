<div class="row">
    <div class="col-lg-12">
    	<meta name="csrf-token" content="{{ csrf_token() }}" />
      
		 <div class="section-heading-home text-center">
                    <h3> Blog Category Management</h3>
                   </div>
		<div class="card card-custom">
			<div class="card-header">
				<div class="card-title justify-content-between w-100 flex-column-sm">
					<div class="left-cart-label d-flex align-items-center">
					<span class="card-icon">
						<i class="flaticon2-supermarket text-primary"></i>
					</span>
					<h3 class="card-label">List of Blog Categories </h3>
				</div>
					<button type="button" class="btn btn-sm float-right green-bg action-blog-edit-btn"  blog-cat-id="" page-name="" act-name="edit-blog-cat" data-toggle="modal" data-target="#add-bog-cat-modal" data-id="0">Add New Category</button>
				</div>
			</div>
			<div class="card-body">
				
				<!--begin: Datatable-->
				<table class="table table-bordered yajra-datatable blog-cat-adm" style="margin-top: 13px !important">
					<thead>
						<tr>
							<th>Name</th>
							<th>Actions</th>
						</tr>
					</thead>
				</table>
				<!--end: Datatable-->
				<!-- /.modal -->
			    <div class="modal fade" id="add-bog-cat-modal">
			        <div class="modal-dialog">
			            <form id="add-blog-category-form" action="{{route('admin-add-blog-category')}}" method="post" enctype="multipart/form-data">
			                <div class="modal-content">
			                    <div class="modal-header">
			                        <h4 class="modal-title scout-title">Add New Category</h4>
			                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
			                        <span aria-hidden="true">&times;</span></button>
			                    </div>
			                    <div class="modal-body">
			                       
			                        @csrf
			                        <div class="form-group">
			                        <label>Name</label>
			                        <input type="text" name="name" value="" id="name">
			                        <input type="hidden" name="blog_cat_id" id="blog_cat_id" value="">
			                    	</div>
			                    	
			                    </div>
			                    <div class="modal-footer justify-content-between">
			                        <button type="button" class="btn btn-outline-light" data-dismiss="modal">Cancel</button>
			                        <button type="button" class="btn btn-outline-light" id="add-blog-cat-button">Submit</button>
			                    </div>
			                </div>
			            </form>
			        </div>
			    </div>
			    <!-- /.modal -->
			</div>
		</div>
		<!--end::Card-->
    </div>
</div>
