<div class="row">
    <div class="col-lg-12">
    	<meta name="csrf-token" content="{{ csrf_token() }}" />
      
		<div class="section-heading-home text-center">
                    <h3> Partner Management</h3>
                   </div>
		<div class="card card-custom">
			<div class="card-header">
				<div class="card-title justify-content-between w-100 flex-column-sm">
					<div class="left-cart-label d-flex align-items-center">
					<span class="card-icon">
						<i class="flaticon2-supermarket text-primary"></i>
					</span>
					<h3 class="card-label">List of Partners</h3>
				</div>
					<button type="button" id="add-partner-modal" class="btn btn-sm float-right green-bg action-partner-add-edit-btn" act-name="add-partners" partner-id=" " data-toggle="modal" data-target="#add-partners-modal">Add New Partner</button>
				</div>
			</div>
			<div class="card-body">
				 <div class="table-responsive home-partner-list">
					<!--begin: Datatable-->
					<table class="table table-bordered yajra-datatable scoutlist-table table-responsive-md" style="margin-top: 13px !important">
						<thead>
							<tr>
								<th>Logo</th>
								<th>Name</th>
								<th>Link</th>
								<th>Actions</th>
							</tr>
						</thead>
					</table>
					<!--end: Datatable-->
				</div>
				<!-- /.modal -->
			    <div class="modal fade" id="add-partners-modal">
			        <div class="modal-dialog">
			            <form id="add-partners-form" action="{{route('admin-add-partner')}}" method="post" enctype="multipart/form-data">
			                <div class="modal-content">
			                    <div class="modal-header">
			                        <h4 class="modal-title scout-title">Add New Partner</h4>
			                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
			                        <span aria-hidden="true">&times;</span></button>
			                    </div>
			                    <div class="modal-body">
			                       
			                        @csrf
			                        <input type="hidden" name="partner_id" id="partner_id" value=""> 
			                        <input type="hidden" id="partner_image" value="">
			                        <div class="form-group">
			                        <label>Name</label>
			                        <input type="text" name="name" value="" id="partner_name">
			                    	</div>
			                    	<div class="form-group">
			                        <label>Link</label>
			                        <input type="url" name="link" value="" id="partner_link">              
			                    	</div>
			                    	<div class="form-group mb-1">
			                        <label>Logo</label>
			                        <input type="file" name="image" value="" id="image">              
			                    	</div>
			                    	<div class="form-group mb-0">  
			                        <label></label><span class="blog-img-cls">Max ratio 165px x 50px </span>             
			                    	</div>
			                    </div>
			                    <div class="modal-footer justify-content-between">
			                        <button type="button" class="btn btn-outline-light" data-dismiss="modal">Cancel</button>
			                        <button type="button" class="btn btn-outline-light pink-bg-btn" id="add-partner-button">Submit</button>
			                    </div>
			                </div>
			            </form>
			        </div>
			    </div>
			    <!-- /.modal -->
			</div>
		</div>
		<!--end::Card-->
    </div>
</div>
