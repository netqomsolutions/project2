{{-- Extends layout --}}
@extends('layout.default')
{{-- Content --}}
@section('content')

@php
if(isset($userData))
{
 $social_links = json_decode($userData->social_link); 
}
@endphp 
<style>
	.toggle.ios, .toggle-on.ios, .toggle-off.ios { border-radius: 20rem; }
	.toggle.ios .toggle-handle { border-radius: 20rem; }
	div#action-scout {
	    align-items: center;
	    justify-content: center;
	}
</style>

 @if(session()->has('success'))
            <div class="alert alert-success alluser-page-suc" style="text-align: left;float: left;width: 100%;">
                {{ session()->get('success') }}
            </div>
            <script type="text/javascript">setTimeout(function(){ $('.alluser-page-suc').fadeOut(); }, 3000);</script>
        @elseif(session()->has('failure'))
            <div class="alert alert-danger alluser-page-suc" style="text-align: left; left;float: left;width: 100%;">
                {{ session()->get('failure') }}
            </div>
            <script type="text/javascript">setTimeout(function(){ $('.alluser-page-suc').fadeOut(); }, 3000);</script>
        @endif
    <div class="main-body">
          <!--  <div class="user-profile-buttons">
            <a href="{{ route('user-profile-edit') }}" class="navi-item">Update Profile</a>
            <a href="{{ route('user-change-password-form') }}" class="navi-item">Change Password</a>
          </div> -->
		  
		  
		  <div class="d-flex flex-row w-100">
		  	                   @if(isset($userData))
								@include('admin.coordinator-sidebar')
								@endif
									<!--begin::Content-->
									<div class="flex-row-fluid profile-right_form">
										<!--begin::Card-->
										<div class="card card-custom card-stretch card-sticky" id="kt_page_sticky_card">
											<!--begin::Header-->
											<div class="card-header py-3 ">
												<div class="card-title align-items-start flex-column">
													<h3 class="card-label font-weight-bolder text-dark">Personal Information</h3>
													<span class="text-muted font-weight-bold font-size-sm mt-1">{{ isset($userData) ? 'Update' : 'Add' }} host personal informaiton</span>
												</div>
												<div class="card-toolbar">
													<button type="button" class="btn btn-success mr-2 green-bg-btn" id="save-profile-button">Save Changes</button>
													<!-- <button type="reset" class="btn btn-secondary">Cancel</button> -->
												</div>
											</div>
											<!--end::Header-->
											<!--begin::Form-->
											<form id="add-coordinator-form" class="form" method="post" action="{{route('create-update-coordinator')}}" enctype="multipart/form-data">
												<!--begin::Body-->
												@csrf
                                    			<input type="hidden" name="user_id" value="{{@$userData->id}}" id="existing_id">
												<div class="card-body">
													<div class="form-group row manage-personal-info">
														<div class="col-lg-4">
														<label class="col-xl-12 col-lg-12 col-form-label">Avatar</label>
														<div class="col-lg-12 col-xl-12">
															@if (isset($userData) && file_exists(public_path($userData->user_image_path)) && $userData->user_image)
															<div class="image-input image-input-outline" id="kt_profile_avatar" style='background-image: url("{{ asset($userData->user_image_path.'/'.$userData->user_image) }}")'>
															@else
															<div class="image-input image-input-outline" id="kt_profile_avatar" style="background-image: url({{ asset('users/user.png') }})">
															@endif

																<div class="image-input-wrapper" ></div>
																<label class="btn btn-xs btn-icon btn-circle btn-white btn-hover-text-primary btn-shadow" data-action="change" data-toggle="tooltip" title="" data-original-title="Change avatar">
																	<i class="fa fa-pen icon-sm text-muted"></i>
																	<input type="file" name="user_image">
																	<input type="hidden" name="profile_avatar_remove">
																</label>
																<span class="btn btn-xs btn-icon btn-circle btn-white btn-hover-text-primary btn-shadow" data-action="cancel" data-toggle="tooltip" title="" data-original-title="Cancel avatar">
																	<i class="ki ki-bold-close icon-xs text-muted"></i>
																</span>
																<span class="btn btn-xs btn-icon btn-circle btn-white btn-hover-text-primary btn-shadow" data-action="remove" data-toggle="tooltip" title="" data-original-title="Remove avatar">
																	<i class="ki ki-bold-close icon-xs text-muted"></i>
																</span>
															</div>
															<span class="form-text text-muted">Allowed file types: png, jpg, jpeg.</span>
															<span class="form-text text-muted">Note: Please upload image of 160 X 160 dimension.</span>
															<label id="user_image-error" class="error" style="display:none;" for="user_image">Please select the image</label>
														</div>
													</div>

													<div class="col-lg-8 pl-0 pr-0">
														<div class="form-group row">
														<label class="col-xl-12 col-lg-12 col-form-label">Coordinator's first name</label>
														<div class="col-lg-12 col-xl-12">
															<input class="form-control form-control-lg form-control-solid" name="user_fname" type="text" value="{{ isset($userData) ? $userData->user_fname : '' }}" placeholder="Enter Coordinator's first name">
														</div>
													</div>
													<div class="form-group row">
														<label class="col-xl-12 col-lg-12 col-form-label">Coordinator's last name</label>
														<div class="col-lg-12 col-xl-12">
															<input class="form-control form-control-lg form-control-solid" name="user_lname" type="text" value="{{ isset($userData) ? $userData->user_lname : '' }}" placeholder="Enter Coordinator's last name">
														</div>
													</div>
													</div>

													</div>





													
													<!-- <div class="form-group row">
														<label class="col-xl-3 col-lg-3 col-form-label">Company Name</label>
														<div class="col-lg-9 col-xl-6">
															<input class="form-control form-control-lg form-control-solid" type="text" value="Loop Inc.">
															<span class="form-text text-muted">If you want your invoices addressed to a company. Leave blank to use your full name.</span>
														</div>
													</div> -->
													<div class="row">
														<div class="col-lg-12 col-xl-12">
															<h5 class="font-weight-bold mt-10 heading-scout-form">Additional Information</h5>
														</div>
													</div>
														 <div class="form-group row contact-scout-form">
														<label class="col-xl-3 col-lg-3 col-form-label">About Coordinator</label>
														<div class="col-lg-9 col-xl-6">
															<div class="input-group input-group-lg input-group-solid">
																<textarea type="text" name="about_me" class="form-control form-control-lg form-control-solid" placeholder="" value="loop">{{ isset($userData) ? $userData->about_me : '' }}</textarea>
																
															</div>
														</div>
													</div> 
													 <div class="form-group row contact-scout-form">
														<label class="col-xl-3 col-lg-3 col-form-label">Coordinator's Position</label>
														<div class="col-lg-9 col-xl-6">
															<div class="input-group input-group-lg input-group-solid">
																<input type="text" name="current_status" class="form-control form-control-lg form-control-solid" value="{{ isset($userData) ? $userData->current_status : '' }}" placeholder="" >
																
															</div>
														</div>
													</div> 
													<div class="form-group row contact-scout-form">
														<label class="col-xl-3 col-lg-3 col-form-label">Is the coordinator is scout?</label>
														<div class="col-lg-9 col-xl-6">
															<div class="input-group input-group-lg input-group-solid add-new-cordinate p-4">
																<div class="form-group d-inline-flex align-items-center m-0"><input type="radio" name="is_scout" class="form-control form-control-lg form-control-solid" value="1" placeholder="" {{isset($userData)? $userData->is_scout == 1 ? 'checked' : '' : ''}} ><span>Yes</span>
																</div>

																<div class="form-group d-inline-flex align-items-center m-0"><input type="radio" name="is_scout" class="form-control form-control-lg form-control-solid" value="0" placeholder="" {{isset($userData)? $userData->is_scout == 0 ? 'checked' : '' : 'checked'}} ><span>No</span>
																</div>
																
															</div>
														</div>
													</div> 
													<div class="row">
														<div class="col-lg-12 col-xl-12">
															<h5 class="font-weight-bold mt-10 heading-scout-form">Social Information</h5>
														</div>
													</div>
												
													
													 <div class="form-group d-flex align-items-center">
							<div class="icon-inner">
								<a  target="_blank" href="{{ !empty($social_links->facebook_link) ? $social_links->facebook_link : 'javascript:;' }}">
								   <i class="fab fa-facebook-f" aria-hidden="true"></i>
							    </a>
							</div>
							<!-- <label class="col-xl-3 col-lg-3 col-form-label text-alert"></label> -->
							<div class="col-lg-9 col-xl-6">
								<input type="text" class="form-control form-control-lg form-control-solid mb-2" id="facebook_link" name="social[facebook_link]" value="{{ !empty($social_links->facebook_link) ? $social_links->facebook_link : '' }}" placeholder="Enter facebook profile link" />
								<!--a href="#" class="text-sm font-weight-bold">Forgot password ?</a-->

								
								

							</div>
						</div>
						<div class="form-group d-flex align-items-center">
							<div class="icon-inner">
								<a target="_blank" href="{{ !empty($social_links->instagram_link) ? $social_links->instagram_link : 'javascript:;' }}">
								<i class="fab fa-instagram" aria-hidden="true"></i>
							  </a>
							</div>
							<!-- <label class="col-xl-3 col-lg-3 col-form-label text-alert"></label> -->
							<div class="col-lg-9 col-xl-6">
								<input type="text" class="form-control form-control-lg form-control-solid" id="instagram_link"  name="social[instagram_link]" value="{{ !empty($social_links->instagram_link) ? $social_links->instagram_link : '' }}" placeholder="Enter instagram profile link" />
							</div>
						</div>
						<div class="form-group d-flex align-items-center">
							<div class="icon-inner">
								<a target="_blank" href="{{ !empty($social_links->twitter_link) ? $social_links->twitter_link : 'javascript:;' }}">
								<i class="fab fa-twitter" aria-hidden="true"></i>
							</a>
							</div>
							<!-- <label class="col-xl-3 col-lg-3 col-form-label text-alert"></label> -->
							<div class="col-lg-9 col-xl-6">
								<input type="text" class="form-control form-control-lg form-control-solid" id="twitter_link" value="{{ !empty($social_links->twitter_link) ? $social_links->twitter_link : '' }}" placeholder="Enter twitter profile link" name="social[twitter_link]" />
							</div>
						</div>
						<div class="form-group d-flex align-items-center">
							<div class="icon-inner">
								<a target="_blank" href="{{ !empty($social_links->linkedin_link) ? $social_links->linkedin_link : 'javascript:;' }}">
								<i class="fab fa-linkedin-in" aria-hidden="true"></i>
							</a>
							</div>
							<!-- <label class="col-xl-3 col-lg-3 col-form-label text-alert"></label> -->
							<div class="col-lg-9 col-xl-6">
								<input type="text" class="form-control form-control-lg form-control-solid" id="linkedin_link" value="{{ !empty($social_links->linkedin_link) ? $social_links->linkedin_link : '' }}" placeholder="Enter linkedin profile link" name="social[linkedin_link]" />
							</div>
						</div>
						<div class="form-group d-flex align-items-center">
							<div class="icon-inner">
								<a target="_blank" href="{{ !empty($social_links->pinterest_link) ? $social_links->pinterest_link : 'javascript:;' }}">
								<i class="fab fa-pinterest" aria-hidden="true"></i>
							</a>
							</div>
							<!-- <label class="col-xl-3 col-lg-3 col-form-label text-alert"></label> -->
							<div class="col-lg-9 col-xl-6">
								<input type="text" class="form-control form-control-lg form-control-solid" id="pinterest_link" value="{{ !empty($social_links->pinterest_link) ? $social_links->pinterest_link : '' }}" placeholder="Enter Pinterest Profile Link" name="social[pinterest_link]" />
							</div>
						</div>


						
							
							
													 
													<!-- Addtional Information section end here  -->

												</div>
												<!--end::Body-->
											</form>
											<!--end::Form-->
										</div>
									</div>
									<!--end::Content-->
								</div>
		  
        </div>
        <!-- /.modal -->
	 
	    <!-- /.modal -->

@endsection
{{-- Scripts Section --}}
@section('scripts')
<script type="text/javascript">

    $.ajaxSetup({headers: {'X-XSRF-TOKEN': decodeURIComponent(/XSRF-Token=([^;]*)/ig.exec(document.cookie)[1])}});
    $(document).ready(function () {
    	var user = '<?php echo (!empty($userData->user_image))? $userData->user_image :''; ?>';
         $.validator.addMethod('filesize', function (value, element, param) {
                return this.optional(element) || (element.files[0].size <= param)
            }, 'File size must be less than {0}');

            $.validator.addMethod("alphanumeric", function(value, element) {
                return this.optional(element) || /^[A-Za-z0-9]+$/.test(value);
            }, "Please enter Letters and numbers only");

            var form = $('#add-coordinator-form');
            form.validate({
                submitHandler: function(form) {
                  // do other things for a valid form
                  form.submit();
                },
                rules: {
                    user_fname: {
                        required: true
                    },
                    user_lname: {
                        required: true,
                    },
                     
                    user_image:{
                        required: function(){
                            if(user!=''){
                                return false;
                            }else{
                                return true;
                            }
                        },
                        extension: "jpg,jpeg,png",
                        filesize: 2*1024*1024,
                    },
                },
                messages: {
                    user_fname: {
                        required: "Please enter your first name",
                    },  
                    user_lname: {                 
                        required : "Please enter your last name",
                    },
                    user_image:{
                        required: "Please select the image",
                        extension: "Please select jpg,jpeg and png image",
                        filesize: "File size must be less than 2MB",
                    },
                },
                ignore: ':hidden:not(".exp_multi")',          
              
            });
         $("#save-profile-button").click(function(){
          if($("#add-coordinator-form").valid())
          {
          	let myForm = document.getElementById('add-coordinator-form');
          	 myForm.submit();
          }
        });
          

        
    });
</script>
@endsection