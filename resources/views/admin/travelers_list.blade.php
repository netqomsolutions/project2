{{-- Extends layout --}}
@extends('layout.default')
{{-- Content --}}
@section('content')
<style type="text/css">
	.dataTables_wrapper .dataTable th:before {content:none !important;}
	.dataTables_wrapper .dataTable th:after {content:none !important;}
</style>
<div class="row">
    <div class="col-lg-12">
    	<meta name="csrf-token" content="{{ csrf_token() }}" />
       	<!--begin::Card-->
       	@if (session('success'))
		    <div class="alert alert-success scout-list-success">
		        {{ session('success') }}
		    </div>
		@elseif(session('failure'))
			<div class="alert alert-danger scout-list-success">
		        {{ session('failure') }}
		    </div>			    
		@endif
		<script type="text/javascript">setTimeout(function(){ $('.scout-list-success').fadeOut(); }, 3000);</script>
		<div class="card card-custom">
			<div class="card-header">
				<div class="card-title">
					<span class="card-icon">
						<i class="flaticon2-supermarket text-primary"></i>
					</span>
					<h3 class="card-label">List Of Travellers</h3>
				</div>
			</div>
			<div class="card-body">
				<!--begin: Datatable-->
				<table class="table table-bordered yajra-datatable table-responsive-sm" style="margin-top: 13px !important">
					<thead>
						<tr>
							<th>AVATAR</th>
							<th>NAME</th>
							<th>EMAIL</th>
							<th>ACTIONS</th>
						</tr>
					</thead>
				</table>
				<!--end: Datatable-->
			
			</div>
		</div>
		<!--end::Card-->
    </div>
</div>
@endsection
{{-- Scripts Section --}}
@section('scripts')
<script type="text/javascript">
  	
  	$(document).ready(function(){
  		$.ajaxSetup({ headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') } });
  		var table = $('.yajra-datatable').DataTable({
	        processing: true,
	        serverSide: true,
	        ajax: "{{ route('admin-travelers') }}",
	        columns: [
	            {data: 'DT_RowIndex', id: 'DT_RowIndex',orderable: false,render:function(data,type,row){	       
	            	var imagUrl = '';
	            	if(row.user_image == null){	       
	            		imagUrl = HOST_URL + '/public/users/user.png';
	            		return "<img src=\"" + imagUrl + "\" height=\"50\"/>";
	            	}else{
                         if(isUrl(row.user_image))
                         {
                            imagUrl = row.user_image;
                         }else{
	            		imagUrl = HOST_URL + '/public/users/travellers/'+row.user_image;
	            		
	            	}
	            	return "<img src=\"" + imagUrl + "\" height=\"50\"/>";
	            	}	            
	            }},
	            {data: 'trveler_user_fname', name: 'trveler_user_fname',render:function(data, type, row){ 
	             return  row.user_fname +' '+ row.user_lname;
	            }},
	            {data: 'email', name: 'email'},
	            {
	                data: 'action', 
	                name: 'action', 
	                orderable: false, 
	                searchable: true
	            },
	        ],
	        order: [[1, 'asc']]
	    });
	
  	});
  	function isUrl(s) {
   var regexp = /(ftp|http|https):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?/
   return regexp.test(s);
}
</script>
@endsection