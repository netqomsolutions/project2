<div class="row">
    <div class="col-lg-12">
    	 <div class="section-heading-home text-center">
                    <h3> Discover & Connect Management</h3>
                   </div>
        <div class="card card-custom">
			<!--begin::Form-->
			<form class="form" id="page-add-blog-form" method="post" enctype="multipart/form-data" action="{{ route('admin-update-cms-pages') }}">
				<div class="card-body">@csrf
					<input type="hidden" name="page_id" value="6"/>
					<div class="form-group row">
						<div class="col-lg-6 my-error">
							<label>Section One Title:</label>
							<input type="text" class="form-control" value="{{ isset($PageData) ? $PageData->section_one_title : '' }}" name="section_one_title" id="section_one_title" placeholder="Enter Section one title">
						</div>
					<div class="col-lg-6 my-error">
							<label>Section One Description</label>
							<textarea class="form-control" name="section_one_description"  id="section_one_description">{{ isset($PageData) ? $PageData->section_one_description : '' }}</textarea>
					</div>
							
					</div>
					<div class="form-group row">
						<div class="col-lg-6 my-error">
							<label>Section Two Title:</label>
							<input type="text" class="form-control" value="{{ isset($PageData) ? $PageData->section_two_title : '' }}" name="section_two_title" id="section_two_title" placeholder="Enter Section Two title">
						</div>
					<div class="col-lg-6 my-error">
							<label>Section Two Description</label>
							<textarea class="form-control" name="section_two_description"  id="section_two_description">{{ isset($PageData) ? $PageData->section_two_description : '' }}</textarea>
					</div>
							
					</div>
					<div class="form-group row">
						<div class="col-lg-6 my-error">
							<label>Section Two Title:</label>
							<input type="text" class="form-control" value="{{ isset($PageData) ? $PageData->section_three_title : '' }}" name="section_three_title" id="section_three_title" placeholder="Enter Section Three title">
						</div>
					<div class="col-lg-6 my-error">
							<label>Section Two Description</label>
							<textarea class="form-control" name="section_three_description"  id="section_three_description">{{ isset($PageData) ? $PageData->section_three_description : '' }}</textarea>
					</div>
							
					</div>
					
                <div class="form-group row">
                	<div class="col-lg-6">
							<button type="submit" class="btn green-bg mr-2">Save</button>
							
						</div>
                </div>
					
				</div>
			</form>
			<!--end::Form-->
		</div>
	</div>
</div>