{{-- Extends layout --}}
@extends('layout.default')
<script src="https://cdn.ckeditor.com/4.12.1/standard/ckeditor.js"></script>
{{-- Content --}}
@section('content')
<div class="row">
     @if(session()->has('success'))
            <div class="alert alert-success alluser-page-suc" style="text-align: left;float: left;width: 100%;">
                {{ session()->get('success') }}
            </div>
            <script type="text/javascript">setTimeout(function(){ $('.alluser-page-suc').fadeOut(); }, 3000);</script>
        @elseif(session()->has('failure'))
            <div class="alert alert-danger alluser-page-suc" style="text-align: left; left;float: left;width: 100%;">
                {{ session()->get('failure') }}
            </div>
            <script type="text/javascript">setTimeout(function(){ $('.alluser-page-suc').fadeOut(); }, 3000);</script>
        @endif
    <div class="col-lg-12">
        <div class="card card-custom">
			<!--begin::Form-->
			<form class="form" id="page-contact-us" method="post" enctype="multipart/form-data" action="{{ route('admin-update-cms-pages') }}">
				<div class="card-body">@csrf
					<div class="form-group row">
						<input type="hidden" name="page_id" value="4"/>
						<div class="col-lg-6 my-error">
							<label>Page Title:</label>
							<input type="text" class="form-control" value="{{ $contactUs->page_title }}" name="page_title" id="page_title" placeholder="Enter page title">
						</div>
						<div class="col-lg-6 my-error">
							<label>Page Slug:</label>
							<input type="text" class="form-control" value="{{ $contactUs->page_slug }}"  name="page_slug" id="page_slug" placeholder="Enter slug">
						</div>						
					</div>
					<div class="form-group row">
						<div class="col-lg-6 my-error">
							<label>Page Meta:</label>
							<input type="text" class="form-control" value="{{ $contactUs->page_meta }}" name="page_meta" id="page_meta" placeholder="Enter page meta">
						</div>						
						<div class="col-lg-6 my-error"><label class="cms_banner_dimension w-100 d-flex justify-content-between flex-wrap">Banner Image: <span>Please upload image of 1600 X 430 dimension.</span></label>  
							<input type="file" name="page_featured_image"  id="page_featured_image" class="form-control" placeholder="Browse Image">
							<input type="hidden" id="old_img" value="{{ $contactUs->page_featured_image }}">

						</div>
					</div>
					<div class="form-group row">
						<div class="col-lg-6 my-error">
							<label>Email:</label>
							<input type="email" class="form-control" value="{{ $contactUs->email }}" name="email" id="page_meta" placeholder="Enter your email">
						</div>						
						<div class="col-lg-6 my-error">
							<label>Address Line 1:</label>
							<input type="text" name="address" value="{{ $contactUs->address }}" class="form-control" placeholder="Enter your address">
						</div>
					</div>
                    <div class="form-group row">
                        <div class="col-lg-6 my-error">
                            <label>Address Line 2:</label>
                            <input type="text" class="form-control" value="{{ $contactUs->address_line_2 }}" name="address_line_2" id="address_line_2" placeholder="Enter Address Line 2">
                        </div>                      
                        <div class="col-lg-6 my-error">
                            <label>Link:</label>
                            <input type="text" name="address_link" value="{{ $contactUs->address_link }}" class="form-control" placeholder="Enter address Link">
                        </div>
                    </div>
				
					<div class="form-group row">
						<div class="col-lg-6 my-error">
                            <label>Phone:</label>
                            <input type="tel" id="phone" name="phone" value="{{ $contactUs->phone }}" class="form-control" placeholder="Enter your phone number" />
                        </div>
						<div class="col-lg-6 my-error">
							<label>Page Description:</label>
							<input type="text" class="form-control" value="{{ $contactUs->page_description }}" name="page_description" id="page_description" placeholder="Enter page description">
						</div>
					</div>

					<div class="form-group row">
						<div class="col-lg-6">
							<button type="submit" class="btn green-bg mr-2">Save</button>
							<!-- <button type="back" class="btn btn-secondary">Back</button> -->
						</div>	
					</div>
				</div>
			</form>
			<!--end::Form-->
		</div>
	</div>
</div>
@include('admin.edit_social_links')
@endsection
{{-- Scripts Section --}}
@section('scripts')
<script type="text/javascript">
$(document).ready(function () {
	$.validator.addMethod('filesize', function (value, element, param) {
            return this.optional(element) || (element.files[0].size <= param)
        }, 'File size must be less than {0}');
    $('#page-contact-us').validate({
        submitHandler: function(form) {
          // do other things for a valid form
          form.submit();
        },
        rules: {
            page_title: {
                required: true
            },
            page_slug: {
                required: true,
            },
            page_meta: {
                required: true,
            },
            email:{
                required: true,
            },
            address:{
                required: true
            },
             link:{
                required: true,
                url:true
            },
            phone:{
                required: true, 
                maxlength: 20,
                minlength: 11,
            },
            page_description:{
            	required: true
            },
            page_featured_image:{
                required: function(){
                    if($("#old_img").val()!=''){
                        return false;
                    }else{
                        return true;
                    }
                },
                extension: "jpg,jpeg,png",
                filesize: 2*1024*1024,
            },
        },
        messages: {
            page_title: {
                required: "Please enter page title",
            },  
            page_slug: {                 
                required : "Please enter page slug",
            },
            page_meta:{
            	required: "Please enter page meta"
            },
            address: {
                required : "Please enter address",
            },
            email: {
                required : "Please enter email address",
            },
            phone: {
                required : "Please enter phone number",
            },
            page_description:{
            	required: "Please enter page de"
            },
            page_featured_image:{
                required: "Please select the image",
                extension: "Please select jpg,jpeg and png image",
                filesize: "File size must be less than 2MB",
            },
        },
        errorElement: 'span',           
        errorPlacement: function (error, element) {
          error.addClass('invalid-feedback');
          element.closest('.my-error').append(error);
        },
        highlight: function (element, errorClass, validClass) {
          $(element).addClass('is-invalid');
        },
        unhighlight: function (element, errorClass, validClass) {
          $(element).removeClass('is-invalid');
        }
    })
});
</script>
@endsection