{{-- Extends layout --}}
@extends('layout.default')
{{-- Content --}}
@section('content')
<style type="text/css">
	.dataTables_wrapper .dataTable th:before {content:none !important;}
	.dataTables_wrapper .dataTable th:after {content:none !important;}
</style>
<div class="row">
    <div class="col-lg-12">
    	<meta name="csrf-token" content="{{ csrf_token() }}" />
       	<!--begin::Card-->
       	@if (session('success'))
		    <div class="alert alert-success scout-list-success">
		        {{ session('success') }}
		    </div>
		@elseif(session('failure'))
			<div class="alert alert-danger scout-list-success">
		        {{ session('failure') }}
		    </div>			    
		@endif
		
		<div class="card card-custom">
			<div class="card-header">
				<div class="card-title">
					<span class="card-icon">
						<i class="flaticon2-supermarket text-primary"></i>
					</span>
					<div class="list_sec">
				   <h3 class="card-label">List of Host</h3>
				</div>
				</div>
			</div>
			<div class="card-body">
				<!--begin: Datatable-->
				<table class="table table-bordered yajra-datatable table-responsive-sm" style="margin-top: 13px !important">
					<thead>
						<tr>
							<th>AVATAR</th>
							<th>NAME</th>
							<th>EMAIL</th>
							<th>SCOUT BY</th>
							<th>STATUS</th> 
						</tr>
					</thead>
				</table>
				<!--end: Datatable-->
				<!-- /.modal -->
			    <div class="modal fade" id="delete-host">
			        <div class="modal-dialog">
			            <form id="delete-host-form" action="" method="post">
			                <div class="modal-content">
			                    <div class="modal-header">
			                        <h4 class="modal-title scout-title">Delete Lead</h4>
			                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
			                        <span aria-hidden="true">&times;</span></button>
			                    </div>
			                    <div class="modal-body">
			                        <p id="cnt-modal"></p>
			                        <span id="dynInput"></span>
			                        @csrf
			                        <input type="hidden" name="host_id" value="" id="host_id">              
			                    </div>
			                    <div class="modal-footer justify-content-between">
			                        <button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
			                        <button type="button" class="btn btn-secondary" data-dismiss="modal" onclick="formSubmit()">Yes</button>
			                    </div>
			                </div>
			            </form>
			        </div>
			    </div>
			    <!-- /.modal -->
			</div>
		</div>
		<!--end::Card-->
    </div> 
<!-- Modal-->
<div class="modal fade" id="exampleModalCustomScrollable" tabindex="-1" role="dialog" aria-labelledby="staticBackdrop" aria-hidden="true">
    <div class="modal-dialog   modal-lg" role="document">
	        <div class="modal-content">
	            <div class="modal-header">
	                <h5 class="modal-title" id="exampleModalLabel"><span class="host-name">View Host</span> &nbsp;&nbsp;&nbsp;<span class="label label-lg label-light-danger label-inline host-status-top">Rejected</span></h5>
	                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	                    <i aria-hidden="true" class="ki ki-close"></i>
	                </button>
	            </div>
	            <div class="modal-body">
	                <div  >
		                 <div class="main-body view-host-details">
								 
		                  </div>
	                <div>
	            </div> 
	        </div>
	    </div>
	</div>
	</div>
</div>
</div>
@endsection
{{-- Scripts Section --}}
@section('scripts')
<script type="text/javascript">
  	
  	$(document).ready(function(){
  		var table = $('.yajra-datatable').DataTable({
	        processing: true,
	        serverSide: true,
	        ajax: "{{ route('admin-host-list') }}",
	        columns: [
	            {data: 'DT_RowIndex', id: 'DT_RowIndex',orderable: false,render:function(data,type,row){	       
	            	var imagUrl = '';
	            	if(row.user_image == null){	       
	            		imagUrl = HOST_URL + '/public/users/user.png';
	            		return "<img src=\"" + imagUrl + "\" height=\"50\"/ width=\"50\"/>";
	            	}else{
	            		imagUrl = HOST_URL + '/public/users/host/'+row.user_image;
	            		return "<img src=\"" + imagUrl + "\" height=\"50\"/ width=\"50\"/>";
	            	}	            
	            }}, 
	            {data: 'host_user_fname', name: 'host_user_fname',render:function(data, type, row){ 
	             return  row.user_fname +' '+ row.user_lname;
	            }},
	            {data: 'email', name: 'email'},
	             {data: 'scout_user_fname', name: 'scout_user_fname',render:function(data, type, row){ 
	             return row.scout ? row.scout.user_fname +' '+ row.scout.user_lname : 'Not assign';
	            }},
	            {data: 'status', name: 'status',render:function(data, type, row){ 
	                var statusLeadColor = '';
	                var leadStatusName = '';
	                if(row.status == '1'){
	                    statusLeadColor = 'label-light-success';
	                    leadStatusName = 'Active';
	                }else if (row.status == '2') {
	                    statusLeadColor = 'label-light-danger';
	                    leadStatusName = 'Inactive';
	                }
	                return '<span class="label label-lg '+statusLeadColor+' label-inline">'+leadStatusName +'</span><button  data-name= "'+ row.user_fname +' '+ row.user_lname +'"  data-status="'+leadStatusName+'" data-id="'+ btoa(row.id)+'"  class="btn btn-light btn-sm view-host "><i class="la la-eye"></i></button>' 
	            }}, 
	        ],
	        order: [[1, 'asc']]
	    }); 
		 $(document).on("click",".view-host",function(){ 
		 	var hostId = $(this).data('id');
            AddHostDetails(this,'host-name','name');
            var hostStatus = $(this).data('status');
            if(hostStatus == 'Active'){  
                     $(".host-status-top").removeClass("label-light-danger");      
                     $(".host-status-top").addClass("label-light-success");      
            		 
            	}else{  
            		 $(".host-status-top").removeClass("label-light-success");  
                     $(".host-status-top").addClass("label-light-danger");     
            	}

            AddHostDetails(this,'host-status-top','status');
               GetHostDetails(hostId); 
		 });
	    $(document).on("click",".action-scout-btn",function(){
		    var url = '',title = '', cnt_modal = '', pub_val = '';
		    if($(this).attr('act-name') == 'del-host'){
		      	url = '{{ route("scout-delete-host") }}';
		      	title = 'Delete Scout';
		      	cnt_modal = 'Are you sure you want to delete this host permanently?';  
		    }
		    $('.modal-title').html(title);   
		    $("#delete-host-form").attr('action', url);
		    $('#cnt-modal').html(cnt_modal);
		    $('#host_id').val($(this).attr('host_id'));
		});
		$('.no-action').click(function(){
			$('#action-scout').css('opacity',0);
		  $('#action-scout').css('display','none');
		})
		
  	});

	function formSubmit(){
		$("#delete-host-form").submit();
	}
	function AddHostDetails(clickedone,calssName,dataName){
	        var myClasses = document.getElementsByClassName(calssName); 
            for (var i = 0; i < myClasses.length; i++) {
			    myClasses[i].innerHTML = $(clickedone).data(dataName);
			  }
		    
	}
	/* 04-02-2021 @RT  */
 
	function GetHostDetails(id) {
	  	$.ajax({          
				 url: `{{ route('admin-host-details') }}`,  
				 type: "POST",    
				 data: { "_token": "{{ csrf_token() }}", 'id': id },         
				 dataType: "Json", 
			      success: function (data) {
			 	if(data.isSucceeded){
			 		    $('.view-host-details').html(data.html); 
			 		    $('#exampleModalCustomScrollable').modal('show');
			 	}else{
	               
			 	}           
			 },          
			 error: function (xhr, ajaxOptions, thrownError) {     
			 }     
		});
	}

</script>
<script type="text/javascript">setTimeout(function(){ $('.scout-list-success').fadeOut(); }, 3000);</script>
@endsection