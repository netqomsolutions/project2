{{-- Extends layout --}}
@extends('layout.default')
<script src="https://cdn.ckeditor.com/4.12.1/standard/ckeditor.js"></script>
{{-- Content --}}
@section('content')
<link href="https://cdnjs.cloudflare.com/ajax/libs/cropperjs/1.5.11/cropper.css" rel="stylesheet"/>
<div class="row">
    <div class="col-lg-12">
        <div class="card card-custom">
      <!--begin::Form-->
      <form class="form" id="page-add-blog-form" method="post" enctype="multipart/form-data" action="">
        <div class="card-body">@csrf
          <input type="hidden" name="blog_id" value="{{ isset($resData) ? $resData->id : 0 }}">
          <div class="form-group row">
            <div class="col-lg-6 my-error">
              <label>Blog Title:</label>
              <input type="text" class="form-control" value="{{ isset($resData) ? $resData->blog_title : '' }}" name="blog_title" id="blog_title" placeholder="Enter Blog title">
            </div>
            <div class="col-lg-6 my-error">
              <label>Blog Category:</label>
             <select class="form-control" name="blog_category_id" id="blog_category_id">
                          <option value="">Select category</option>
                          @forelse ($blogCategories as $cat)
                              <option value="{{ $cat->id }}" {{ (isset($resData) && $resData->blog_category_id==$cat->id) ? 'selected' : '' }}>{{ $cat->name }}</option>
                          @empty

                          @endforelse
                      </select>
            </div>    
          </div>
          <div class="form-group row blog-featured_img ">
            
            <div class="col-lg-12 my-error">
            <label class="w-100">Blog Feature Image:</label>
             @if (isset($resData) && file_exists(public_path('pages/blogs/'.$resData->feature_image)))
                                            @php 
                                            $image=asset('pages/blogs/'.$resData->feature_image);
                                            @endphp
                                            @else
                                             @php 
                                            $image="";
                                            @endphp
                                            @endif 
             <div class="image-input image-input-empty image-input-outline w-50 mb-3 {{(!empty($image))? 'icon-before-hide' : ''}}" id="kt_image_5" style="background-image: url('{{(!empty($image))? $image : ""}}')">
                                            <div class="image-input-wrapper w-100"></div>
                                            <label class="btn btn-xs btn-icon btn-circle btn-white btn-hover-text-primary btn-shadow feature-image-label" data-action="change" data-toggle="tooltip" title="" data-original-title="Change avatar">
                                                <i class="fa fa-pen icon-sm text-muted"></i>

                                  <input type="file" name="feature_image"  value="" id="feature_image" class="form-control feature_image" placeholder="Browse Image"> 
                                            </label>
                                          
                                            <span class="btn btn-xs btn-icon btn-circle btn-white btn-hover-text-primary btn-shadow image-cancel-button" data-action="cancel" data-toggle="tooltip" title="Cancel avatar">
                                                <i class="ki ki-bold-close icon-xs text-muted"></i>
                                            </span>
                                            <span class="btn btn-xs btn-icon btn-circle btn-white btn-hover-text-primary btn-shadow" data-action="remove" data-toggle="tooltip" title="Remove avatar">
                                                <i class="ki ki-bold-close icon-xs text-muted"></i>
                                            </span>
                                             Please upload image of 1100 X 600 dimension.
                                        </div>
            <input type="hidden" id="old_img" value="{{ (isset($resData) && $resData->feature_image!=null) ? $resData->feature_image : '' }}">
            </div>  
          </div>
          <div class="form-group row">
            <div class="col-lg-12 my-error">
              <label>Blog Description :</label>
              
                <textarea class="form-control" name="blog_description"  id="blog_description">{{ isset($resData) ? $resData->blog_description : '' }}</textarea>
              
            </div>
          </div>
                <div class="form-group row">
                  <div class="col-lg-6">
              <button type="button" class="btn green-bg mr-2 save-blog-button">Save</button>
              
            </div>
                </div>
          
        </div>
      </form>
      <!--end::Form-->
    </div>
  </div>
</div>
@include('admin.image_cropper_modal')
@endsection
{{-- Scripts Section --}}
@section('scripts')

<script src="{{asset('js/pages/crud/file-upload/image-input.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/cropperjs/1.5.11/cropper.js" ></script>
<script type="text/javascript">

$(document).ready(function () {

     var new_feature_image = document.getElementById('kt_image_5');
            var input = $(".feature_image");
            var cropper;
            var initialAvatarURL;
            var canvas;
   $('[data-toggle="tooltip"]').tooltip();

            input.on('change', function (e) {
              var files = e.target.files;
              var done = function (url) {
               console.log(url);
                input.value = '';
                    var editor = document.createElement('div');
                editor.style.position = 'fixed';
                editor.style.left = 0;
                editor.style.right = 0;
                editor.style.top = 0;
                editor.style.bottom = 0;
                editor.style.zIndex = 9999;
                editor.style.backgroundColor = '#000';

                // Create the confirm button
                var confirm = document.createElement('button');
                confirm.style.position = 'absolute';
                confirm.style.left = '10px';
                confirm.style.top = '10px';
                confirm.style.backgroundColor = '#ff4883';
                confirm.style.paddingTop = '0.65rem';
                confirm.style.paddingBottom = '0.65rem';
                confirm.style.paddingRight = '1rem';
                confirm.style.paddingLeft = '1rem';
                confirm.style.fontSize = '1rem';
                confirm.style.color = '#fff';
                confirm.style.borderRadius = '5px';
                confirm.style.borderTop = 'none';
                confirm.style.borderBottom = 'none';
                confirm.style.borderRight = 'none';
                confirm.style.borderLeft = 'none';
                confirm.style.zIndex = 9999;
                confirm.textContent = 'Confirm';
                editor.appendChild(confirm);

                // Create the cancel button
                var cancel = document.createElement('button');
                cancel.style.position = 'absolute';
                cancel.style.left = '100px';
                cancel.style.top = '10px';
                cancel.style.backgroundColor = '#333';
                cancel.style.paddingTop = '0.65rem';
                cancel.style.paddingBottom = '0.65rem';
                cancel.style.paddingRight = '1rem';
                cancel.style.paddingLeft = '1rem';
                cancel.style.fontSize = '1rem';
                cancel.style.color = '#fff';
                cancel.style.borderRadius = '5px';
                cancel.style.borderTop = 'none';
                cancel.style.borderBottom = 'none';
                cancel.style.borderRight = 'none';
                cancel.style.borderLeft = 'none';
                cancel.style.zIndex = 9999;
                cancel.textContent = 'Cancel';
                editor.appendChild(cancel);
                confirm.addEventListener('click', function() {

                    // Get the canvas with image data from Cropper.js
                    if (cropper) {
                        canvas = cropper.getCroppedCanvas({
                         width: 1070,
                         height: 600,
                        });
                       initialAvatarURL = new_feature_image.src;
                       new_feature_image.src = canvas.toDataURL();
                       $(".image-input-wrapper").css('background-image', 'url(' + new_feature_image.src + ')');
                    }
                 // Remove the editor from view
                    editor.parentNode.removeChild(editor);
                    });
                    cancel.addEventListener('click', function() {

                    $(".image-input-wrapper").css('background-image', 'url(" ")');
                    reset_form_element($('.experience_feature_image'));
                     $(".image-cancel-button").hide();
                      // Remove the editor from view
                    editor.parentNode.removeChild(editor);
                    });
                 // Load the image
                var image = new Image();
                image.src = URL.createObjectURL(file);
                editor.appendChild(image);

                // Append the editor to the page
                document.body.appendChild(editor);

                // Create Cropper.js and pass image
                var cropper = new Cropper(image, {
                    aspectRatio: 16/9,
                    viewMode: 2,
                });
              };
              var reader;
              var file;
              var url;

              if (files && files.length > 0) {
                file = files[0];

                if (URL) {
                  done(URL.createObjectURL(file));
                } else if (FileReader) {
                  reader = new FileReader();
                  reader.onload = function (e) {
                    done(reader.result);
                  };
                  reader.readAsDataURL(file);
                }
              }
            });
  CKEDITOR.replace("blog_description", {
        filebrowserUploadUrl: "{{route('ckeditor-upload', ['_token' => csrf_token() ])}}",
        filebrowserUploadMethod: 'form'
    });
  $.validator.addMethod('filesize', function (value, element, param) {
    return this.optional(element) || (element.files[0].size <= param)
  }, 'File size must be less than {0}');
  $('#page-add-blog-form').validate({
    ignore: [],
      rules: {
        blog_title: {
          required: true
        },
        feature_image: {
          required: function(){
                    if($("#old_img").val()!=''){
                        return false;
                    }else{
                        return true;
                    }
                },
                extension: "jpg,jpeg,png,JPG,JPEG,PNG",
                filesize: 2*1024*1024,
        },
        blog_category_id:{
              required: true,
          },
          blog_description:{
                required: function() {
                  CKEDITOR.instances.blog_description.updateElement();
                },
                minlength:10
            }
      },
      messages: {
        blog_title: {
          required: "Please enter a blog slug",
          },  
          feature_image: {              
              required: "Please select the image",
                extension: "Please select jpg,jpeg and png image",
                filesize: "File size must be less than 2MB",
          },
          blog_category_id: {
              required : "Please Select Blog Category",
          },
          blog_description:{
                required:"Please enter content here",
                minlength:"Please enter 10 characters"
            }
      },
      errorElement: 'span',       
      errorPlacement: function (error, element) {
        error.addClass('invalid-feedback');
        element.closest('.my-error').append(error);
      },
      highlight: function (element, errorClass, validClass) {
        $(element).addClass('is-invalid');
      },
      unhighlight: function (element, errorClass, validClass) {
        $(element).removeClass('is-invalid');
      }
  });
  $(".save-blog-button").click(function(){
        if($("#page-add-blog-form").valid()){
          var myForm = document.getElementById('page-add-blog-form');
          var formData = new FormData(myForm); 
          console.log(canvas);
           if(canvas!==undefined)
          {
          canvas.toBlob(function (blob) {      
          formData.append("newFile", blob,"newExperience.jpg");
           blogFormSubmit(formData);    
             for (var key of formData.entries()) {
                      console.log(key[0] + ', ' + key[1]);
                   }
           });
          }else{
            blogFormSubmit(formData);
          }
          
          
        }
    });
});
function blogFormSubmit(formData)
{
   $.ajax({
       url: "{{ route('admin-add-update-blog') }}", 
       type: "POST",             
       data: formData,
       dataType:'json',
       contentType: false,
       cache: false,             
       processData: false, 
           
       success: function(data) {
       if(data.isSucceeded)
       {
       toastr.success(data.message, 'Success!', {timeOut: 5000});
       window.location.href="{{route('admin-blog-list')}}";
       }
       else{
       toastr.error(data.message, 'Error!', {timeOut: 5000});
       }
       
       }

    });
}
function reset_form_element(e) {
            e.wrap('<form>').parent('form').trigger('reset');
            e.unwrap();
        }
</script>
@endsection