{{-- Extends layout --}}
@extends('layout.default')
<script src="https://cdn.ckeditor.com/4.12.1/standard/ckeditor.js"></script>
{{-- Content --}}
@section('content')
<div class="row">
        @if(session()->has('success'))
            <div class="alert alert-success alluser-page-suc" style="text-align: left;float: left;width: 100%;">
                {{ session()->get('success') }}
            </div>
            <script type="text/javascript">setTimeout(function(){ $('.alluser-page-suc').fadeOut(); }, 3000);</script>
        @elseif(session()->has('failure'))
            <div class="alert alert-danger alluser-page-suc" style="text-align: left; left;float: left;width: 100%;">
                {{ session()->get('failure') }}
            </div>
            <script type="text/javascript">setTimeout(function(){ $('.alluser-page-suc').fadeOut(); }, 3000);</script>
        @endif
    <div class="col-lg-12">
        <div class="card card-custom">
			<!--begin::Form-->
			<form class="form" id="page-contact-us" method="post" enctype="multipart/form-data" action="{{ route('admin-update-cms-pages') }}">
				<div class="card-body">@csrf
					<div class="form-group row">
						<input type="hidden" name="page_id" value="5"/>
						<div class="col-lg-6 my-error">
							<label>Page Title:</label>
							<input type="text" class="form-control" value="{{ $PageData->page_title }}" name="page_title" id="page_title" placeholder="Enter page title">
						</div>
						<div class="col-lg-6 my-error">
							<label>Page Slug:</label>
							<input type="text" class="form-control" value="{{ $PageData->page_slug }}"  name="page_slug" id="page_slug" placeholder="Enter slug">
						</div>						
					</div>
					<div class="form-group row">
						<div class="col-lg-6 my-error">
							<label>Page Meta:</label>
							<input type="text" class="form-control" value="{{ $PageData->page_meta }}" name="page_meta" id="page_meta" placeholder="Enter page meta">
						</div>						
						<div class="col-lg-6 my-error"><label class="cms_banner_dimension w-100 d-flex justify-content-between flex-wrap">Banner Image: <span>Please upload image of 1600 X 430 dimension.</span></label> 
							<input type="file" name="page_featured_image"  id="page_featured_image" class="form-control" placeholder="Browse Image">
							<input type="hidden" id="old_img" value="{{ $PageData->page_featured_image }}">

						</div>
					</div>
					<div class="form-group row">
                        <div class="col-lg-6 my-error">
                            <label>Page Description:</label>
                            <input type="text" class="form-control" value="{{ $PageData->page_description }}" name="page_description" id="page_description" placeholder="Enter page description">
                        </div>
						<div class="col-lg-6 my-error"><label class="cms_banner_dimension w-100 d-flex justify-content-between flex-wrap">The need for change Image: <span>Please upload image of 602 X 400 dimension.</span></label>  
							<input type="file" class="form-control" value="" name="tab_one_image" id="tab_one_image" placeholder="Enter your email">
                            <input type="hidden" id="old_tab_one_img" name="old_tab_one_img" value="{{ $PageData->tab_one_image }}">
						</div>						
					</div>
                    <div class="form-group row">
                        <div class="col-lg-12 my-error">
                            <label>The need for change Content:</label>
                            <textarea class="form-control" name="tab_one_content"  id="tab_one_content">{{ $PageData->tab_one_content }}</textarea>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-lg-12 my-error">
                            <label> The new model Content:</label>
                            <textarea class="form-control" name="tab_two_content"  id="tab_two_content">{{ $PageData->tab_two_content }}</textarea>
                        </div>
                    </div>
                      <div class="form-group row">
                        <div class="col-lg-12 my-error">
                            <label> The Community Content:</label>
                            <textarea class="form-control" name="tab_three_content"  id="tab_three_content">{{ $PageData->tab_three_content }}</textarea>
                        </div>
                    </div>

					<div class="form-group row">
						<div class="col-lg-6">
							<button type="submit" class="btn green-bg mr-2">Save</button>
							<!-- <button type="back" class="btn btn-secondary">Back</button> -->
						</div>	
					</div>
				</div>
			</form>
			<!--end::Form-->
		</div>
	</div>
</div>
@include('admin.how_scout_tab')
@include('admin.how_experience_tab_list')
@endsection
{{-- Scripts Section --}}
@section('scripts')
<script type="text/javascript">
$(document).ready(function () {
    CKEDITOR.replace("tab_one_content");
    CKEDITOR.replace("tab_two_content");
    CKEDITOR.replace("tab_three_content");
$.ajaxSetup({ headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') } });
	$.validator.addMethod('filesize', function (value, element, param) {
            return this.optional(element) || (element.files[0].size <= param)
        }, 'File size must be less than {0}');
    $('#page-contact-us').validate({
        ignore: [],
        submitHandler: function(form) {
          // do other things for a valid form
          form.submit();
        },
        rules: {
            page_title: {
                required: true
            },
            page_slug: {
                required: true,
            },
            page_meta: {
                required: true,
            },
            tab_one_content:{
                required: function() {
                    CKEDITOR.instances.tab_one_content.updateElement();
                },
                minlength:10
            },
            tab_two_content:{
                required: function() {
                    CKEDITOR.instances.tab_two_content.updateElement();
                },
                minlength:10
            },
            tab_three_content:{
                required: function() {
                    CKEDITOR.instances.tab_three_content.updateElement();
                },
                minlength:10
            },
            tab_one_image:{
                required: function(){
                    if($("#old_tab_one_img").val()!=''){
                        return false;
                    }else{
                        return true;
                    }
                },
                extension: "jpg,jpeg,png",
                filesize: 1*1024*1024,
            },
            page_description:{
            	required: true
            },
            page_featured_image:{
                required: function(){
                    if($("#old_img").val()!=''){
                        return false;
                    }else{
                        return true;
                    }
                },
                extension: "jpg,jpeg,png",
                filesize: 2*1024*1024,
            },
        },
        messages: {
            page_title: {
                required: "Please enter page title",
            },  
            page_slug: {                 
                required : "Please enter page slug",
            },
            page_meta:{
            	required: "Please enter page meta"
            },
            address: {
                required : "Please enter address",
            },
            email: {
                required : "Please enter email address",
            },
            phone: {
                required : "Please enter phone number",
            },
            page_description:{
            	required: "Please enter page description"
            },
            page_featured_image:{
                required: "Please select the image",
                extension: "Please select jpg,jpeg and png image",
                filesize: "File size must be less than 2MB",
            }, 
            tab_one_image:{
                required: "Please select the image",
                extension: "Please select jpg,jpeg and png image",
                filesize: "File size must be less than 1MB",
            },
            tab_one_content:{
                required:"Please enter content here",
                minlength:"Please enter 10 characters"
            },
            tab_two_content:{
                required:"Please enter content here",
                minlength:"Please enter 10 characters"
            },
            tab_three_content:{
                required:"Please enter content here",
                minlength:"Please enter 10 characters"
            },
        },
        errorElement: 'span',           
        errorPlacement: function (error, element) {
          error.addClass('invalid-feedback');
          element.closest('.my-error').append(error);
        },
        highlight: function (element, errorClass, validClass) {
          $(element).addClass('is-invalid');
        },
        unhighlight: function (element, errorClass, validClass) {
          $(element).removeClass('is-invalid');
        }
    });
     CKEDITOR.replace("list_description");
    $("#add-update-scout-tab-list").validate({
        ignore: [],
        submitHandler: function(form) {
          // do other things for a valid form
          form.submit();
        },
        rules: {
            title:{
              required: true
            },
             description:{
              required: function() {
                    CKEDITOR.instances.list_description.updateElement();
                },
                minlength:10
            },
        }
           
    });
    var table = $('.yajra-datatable').DataTable({
            processing: true,
            serverSide: true,
            ajax: "{{ route('admin-scout-tab-list') }}",
            columns: [
                {data: 'title', name: 'title'},
                {
                    data: 'action', 
                    name: 'action', 
                    orderable: false, 
                    searchable: true
                },
            ],
            order: [[1, 'asc']] ,
              fnDrawCallback: function( oSettings ) {
            scoutListDelete();
        }
    });
     var table2 = $('.yajra-datatable-2').DataTable({
            processing: true,
            serverSide: true,
            ajax: "{{ route('admin-experience-tab-list') }}",
            columns: [
                
                {data: 'title', name: 'title'},
                {
                    data: 'action', 
                    name: 'action', 
                    orderable: false, 
                    searchable: true
                },
            ],
            order: [[1, 'asc']],
            fnDrawCallback: function( oSettings ) {
            /* 01-02-2021 @RT */
            scoutListDelete();
        }
    });
        $(document).on("click",".action-add-edit-btn",function(){
            var url = '',title = '', cnt_modal = '', pub_val = '';
            $("#list_type").val($(this).attr('page-name'))
            if($(this).attr('act-name') == 'edit-scout-tab-list' && $(this).attr('list-id')!=''){
            var list_id= $(this).attr('list-id');
        $.ajax({
          type: 'POST',
          url: "{{ route('admin-get-scout-tab-list') }}",
          data: {_token: "{{ csrf_token() }}", 'id':list_id},
          dataType: "Json",
            success: function(resultData) { 
                $("#list_title").val(resultData.data.title);
                CKEDITOR.instances['list_description'].setData(resultData.data.description);
                $("#list_id").val(list_id);
            }
        });
            title = 'Update New List';
            }
            else{
            title = 'Add New List';
            }
            $('.modal-title').html(title);   
           
    });
   

});
  $('#add-scout-list').on('hidden.bs.modal', function () {
    $(this).find('form').trigger('reset');
     CKEDITOR.instances['list_description'].setData('');
     $("#list_id").val(' ');
     })

/* 01-02-2021 @RT */
  function scoutListDelete() {
            $('.action-scout-list-delete-btn').confirmation({
            content: "You want to delete this.",
            onConfirm: function() {
                var list_id=$(this).attr('list-id');
                $.ajax({
                        url: `{{ route("admin-delete-how-work-list") }}`,
                        type: "POST",
                        data: { list_id : list_id},
                        dataType: "Json",
                        success: function (data) {
                        toastr.options.timeOut = 1500;
                        if(data.isSucceeded){
                        toastr.success(data.message);
                        $('.yajra-datatable, .yajra-datatable-2').DataTable().ajax.reload();
                        }else{
                        toastr.error("Please try again");  
                        }
                        },
                        error: function (xhr, ajaxOptions, thrownError) {
                         toastr.error("Please try again");  
                        }
                    });
                }

     });
  }


</script>
@endsection