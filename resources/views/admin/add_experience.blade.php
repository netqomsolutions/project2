{{-- Extends layout --}}
@extends('layout.default')
{{-- Content --}}
<style type="text/css">
#second_step{
    display:none;
}
.my-error{
    color: red;
}
</style>
@section('content')
<div class="row">
    <div class="col-lg-12">
        <div class="card card-custom gutter-b example example-compact">
            <div class="card-header">
                <h3 class="card-title">Add experience</h3>
                <div class="card-toolbar" style="display: none;">
                    <div class="example-tools justify-content-center">
                        <span class="example-toggle" data-toggle="tooltip" title="View code"></span>
                        <span class="example-copy" data-toggle="tooltip" title="Copy code"></span>
                    </div>
                </div>
            </div>
            <!--begin::Form-->
            <form class="form" enctype="multipart/form-data" id="add-exp" method="post" action="{{ route('admin-create-experience') }}">
                <div class="card-body">@csrf
                    <fieldset id="first_step" class="">
                        <div class="form-group row">
                            <div class="col-lg-4 my-error">
                                <label>Experience name:</label>
                                <input type="text" name="data[experiences][experience_name]" class="form-control" placeholder="Enter experience name" />
                            </div>
                            <div class="col-lg-4 my-error">
                                <label>Experience type:</label>
                                <select class="form-control changeExpType" name="data[experiences][experience_type]">
                                    <option value="">Select experience type</option>
                                    <option value="1">PRICING SCENARIO 1</option>
                                    <option value="2">PRICING SCENARIO 2</option>
                                    <option value="3">PRICING SCENARIO 3</option>
                                </select>
                            </div>
                            <div class="col-lg-4 my-error">
                                <label>Experience category:</label>
                                <select class="form-control" name="data[experiences][experience_category_id]">
                                    <option value="">Select category</option>
                                    @forelse ($experienceCategories as $experienceCategory)
                                        <option value="{{ $experienceCategory->id }}">{{ $experienceCategory->name }}</option>
                                    @empty

                                    @endforelse
                                </select>
                            </div>
                            
                        </div>
                        <div class="form-group row">                        
                            <div class="col-lg-4 my-error">
                                <label>Max guest:</label>
                                <select class="form-control" name="data[experiences][experience_group_size]">
                                    <option value="">Select max guest</option>
                                    @for($i = 1; $i <= 12; $i++)
                                        <option value="{{ $i }}">{{ $i }}</option>
                                    @endfor
                                </select>
                            </div>
                            <div class="col-lg-4 my-error">
                                <label>Duration (in hours):</label>
                                <input type="number" onkeydown="if(event.key==='.'){event.preventDefault();}"  oninput="event.target.value = event.target.value.replace(/[^0-9]*/g,'');" name="data[experiences][experience_duration]" class="form-control" placeholder="Enter experience duration" />
                            </div>
                            <div class="col-lg-4 my-error">
                                <label>Experience description:</label>
                                <input type="text" name="data[experiences][experience_description]" class="form-control" placeholder="Enter experience description" />
                            </div>

                        </div>
                        <div class="form-group row">                        
                            <div class="col-lg-4 my-error">
                                <label>Experience low price:</label>
                                <input type="number" name="data[experiences][experience_low_price]" class="form-control" placeholder="Enter experience low price" />
                            </div>
                            <div class="col-lg-4 my-error" id="high-price" style="display: none;">
                                <label>Experience high price:</label>
                                <input type="number" name="data[experiences][experience_high_price]" class="form-control" placeholder="Enter experience high price" />
                            </div>
                            <div class="col-lg-4 my-error">
                                <label>Experience price vailid for:</label>
                                <select class="form-control" name="data[experiences][experience_price_vailid_for]">
                                    <option value="">Number of person</option>
                                    @for($i = 1; $i <= 12; $i++)
                                        @if ($i == 1)
                                            <option value="{{ $i }}">{{ $i }}</option>
                                        @else
                                            <option value="{{ $i }}">{{ 1 .'-'. $i }}</option>
                                        @endif                                        
                                    @endfor
                                </select>
                            </div>                            
                        </div>                                       
                        <div class="form-group row">
                            <div class="col-lg-6 my-error">
                                <p>
                                    <a class="btn btn-primary next">Next</a>
                                </p>
                            </div>
                        </div>
                    </fieldset>
                    <fieldset id="second_step" class="">
                        <div class="form-group row">
                            <div class="col-lg-4 my-error">
                                <label>Experience country:</label>
                                <select class="form-control onChangeCountry" name="data[locations][country_id]">
                                    <option value="">Select country </option>
                                    @forelse ($countries as $country)
                                        <option value="{{ $country->id }}">{{ $country->name }}</option>
                                    @empty

                                    @endforelse
                                </select>
                            </div>
                            <div class="col-lg-4 my-error">
                                <label>Experience state:</label>
                                <select class="form-control onChangeState" name="data[locations][state_id]" id="state_id">
                                    <option value="">Select State</option>
                                </select>
                            </div>
                            <div class="col-lg-4 my-error">
                                <label>Experience city:</label>
                                <select class="form-control" name="data[locations][city_id]" id="city_id">
                                    <option value="">Select City</option>
                                </select>
                            </div>
                        </div>   
                        <div class="form-group row">
                            <div class="col-lg-4 my-error">
                                <label>Experience hint:</label>
                                <input type="text" name="data[experiences][experience_hint]" class="form-control" placeholder="Enter your experience hint" />
                            </div>                             
                            <div class="col-lg-4 my-error">
                                <label>Location Name:</label>
                                <input type="text"  name="data[locations][lname]" class="form-control" placeholder="Enter experience location name" />
                            </div>
                            <div class="col-lg-4 my-error">
                                <label>Feature image:</label>
                                <input type="file"  name="data[experiences][experience_feature_image]" class="form-control" placeholder="Browse image" />
                            </div>   
                        </div>
                        <div class="form-group row">                            
                            <div class="col-lg-4 my-error" >
                                <label>Addon image:</label>
                                <input type="file"  name="data[experience_images][image_name][]" class="form-control" multiple placeholder="Browse image" />
                            </div>
                            <div class="col-lg-4 my-error">
                                <label>Experience feature:</label>
                                <select class="form-control" name="data[experience_feature_relations][experience_feature_id]">
                                    <option value="">Select </option>
                                    @forelse ($experienceFeatures as $experienceFeature)
                                        <option value="{{ $experienceFeature->id }}">{{ $experienceFeature->name }}</option>
                                    @empty

                                    @endforelse
                                </select>
                            </div>  
                            <div class="col-lg-4 my-error">
                                <label>Experience start time:</label>
                                <select class="form-control" name="data[experiences][experience_start_time]">
                                    <option value="">Select start time</option>
                                    <option value="1">Morning ( 01:00 - 11:00 )</option>
                                    <option value="2">Afternoon ( 12:00 - 18:00 )</option>
                                    <option value="3">Evening ( 19:00 - 00:00 )</option>
                                </select>
                            </div>                             
                        </div>
                        <div class="form-group row">
                            <div class="col-lg-6">
                                <p>                        
                                    <a class="btn btn-secondary" id="previous" >Previous</a>
                                    <button type="submit" class="btn btn-primary">Save</button>
                                </p>
                            </div>
                        </div>
                    </fieldset>
                </div>
                <div class="card-footer">
                </div>
            </form>
            <!--end::Form-->
        </div>
    </div>
</div>
@endsection
{{-- Scripts Section --}}
@section('scripts')
<script type="text/javascript">
$(document).ready(function () {
    //On click next button check validation
    $(".next").click(function(){
        var form = $('#add-exp');
        form.validate({
            rules: {
                "data[experiences][experience_name]": {
                    required: true
                },
                "data[experiences][experience_description]": {
                    required: true,
                },
                "data[experiences][experience_type]":{
                    required: true,
                },
                "data[experiences][experience_category_id]":{
                    required: true
                },
                "data[experiences][experience_group_size]":{
                    required: true,
                },
                "data[experiences][experience_duration]":{
                    required: true,
                },
                "data[experiences][experience_hint]":{
                    required: true,
                },
                "data[experiences][experience_low_price]":{
                    required: true,
                },
                "data[experiences][experience_high_price]":{
                    required: true,
                },
                "data[experiences][experience_feature_image]":{
                    required: true,
                },
                "data[experience_feature_relations][experience_feature_id]":{
                    required: true,
                },
                "data[locations][lname]":{
                    required: true,
                },
                "data[experiences][experience_price_vailid_for]":{
                    required: true,
                },
                "data[locations][country_id]":{
                    required: true,
                },
                "data[locations][state_id]":{
                    required: true,
                },
                "data[locations][city_id]":{
                    required: true,
                },
                "data[experience_images][image_name][]":{
                    required :true
                },
                "data[experiences][experience_start_time]":{
                    required :true
                }
            },
            messages: {
                
                "data[experiences][experience_name]": {
                    required: "Please enter experience name",
                },  
                "data[experiences][experience_description]": {              
                    required : "Please enter your experience description",
                },
                "data[experiences][experience_type]":{
                    required : "Please select your experience type",
                },
                "data[experiences][experience_category_id]":{
                    required : "Please select your experience category",
                },
                "data[experiences][experience_group_size]":{
                    required : "Please enter max group size", 
                },
                "data[experiences][experience_duration]":{
                    required: "Please enter duration in hours",
                },
                "data[experiences][experience_hint]":{
                    required: "Please enter experience hint"
                },
                "data[experiences][experience_low_price]":{
                    required: "Please enter low price"
                },
                "data[experiences][experience_high_price]":{
                    required: "Please enter high price"
                },
                "data[experiences][experience_feature_image]":{
                    required: "Please select feature image first"
                },
                "data[experience_feature_relations][experience_feature_id]":{
                    required: "Please select feature first",
                },
                "data[locations][lname]":{
                    required: "Please enter location name"
                },
                "data[experiences][experience_price_vailid_for]":{
                    required: "Please select price valid for"
                },
                "data[locations][country_id]":{
                    required: "Please select country first"
                },
                "data[locations][state_id]":{
                    required: "Please select state first"
                },
                "data[locations][city_id]":{
                    required: "Please select city first"
                },
                "data[experience_images][image_name][]":{
                    required : "Please select add-ons image"
                },
                "data[experiences][experience_start_time]":{
                    required :"Please select start time"
                }
            },
            errorElement: 'span',
            errorClass: 'form-group',
            highlight: function(element, errorClass, validClass) {
                $(element).closest('.my-error').addClass("has-error");
            },
            unhighlight: function(element, errorClass, validClass) {
                $(element).closest('.my-error').removeClass("has-error");
            },
        })
        
        if (form.valid() === true){
            //if form true then procced form submit else repeat actions
             if($('#second_step').is(":visible")){
                current_fs = $('#second_step');
                next_fs = $('#first_step');
                form.submit();
            }else if ($('#first_step').is(":visible")){
                current_fs = $('#first_step');
                next_fs = $('#second_step');
            }
            next_fs.show();
            current_fs.hide();
        }
    });
    
    //On click prev show previous filled form
    $('#previous').click(function(){
        if($('#second_step').is(":visible")){
            current_fs = $('#second_step');
            next_fs = $('#first_step');
        }else if ($('#first_step').is(":visible")){
            current_fs = $('#first_step');
            next_fs = $('#second_step');
        }
        next_fs.show();
        current_fs.hide();
    });
});

//On change exp type hide show input
$('.changeExpType').on('change',function(){
    if($(this).val() == 3){
        $('#high-price').show();
    }else{
        $('#high-price').hide();
    }
})
//Set header for ajax
$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

//On change get state according country
$('.onChangeCountry').on('change',function(){
    $('#state_id').html('<option value="">Select State</option>');
    $('#city_id').html('<option value="">Select City</option>');
    var countryId = $(this).val();
    $.ajax({
        url: '{{ route("admin-get-state") }}',
        type: 'POST',
        data: { countryId: countryId , "_token": "{{ csrf_token() }}", },
        success: function(response)
        {
            if(response.isSucceeded){
                $('#state_id').html(response.data)
            }else{
                $('#state_id').html(response.data)
            }
        }
    });
})

//On change get state according country
$('.onChangeState').on('change',function(){
    $('#city_id').html('<option value="">Select City</option>');
    var stateId = $(this).val();
    $.ajax({
        url: '{{ route("admin-get-city") }}',
        type: 'POST',
        data: { stateId: stateId , "_token": "{{ csrf_token() }}", },
        success: function(response)
        {
            if(response.isSucceeded){
                $('#city_id').html(response.data)
            }else{
                $('#city_id').html(response.data)
            }
        }
    });
})

</script>
@endsection