{{-- Extends layout --}}
@extends('layout.default')
{{-- Content --}}
@section('content')
<style type="text/css">
	.dataTables_wrapper .dataTable th:before {content:none !important;}
	.dataTables_wrapper .dataTable th:after {content:none !important;}
</style>
<div class="row">
	<div class="col-lg-12">
		<meta name="csrf-token" content="{{ csrf_token() }}" />
		<!--begin::Card-->
		@if (session('success'))
		<div class="alert alert-success scout-list-success">
			{{ session('success') }}
		</div>
		@elseif(session('failure'))
		<div class="alert alert-danger scout-list-success">
			{{ session('failure') }}
		</div>			    
		@endif
		<script type="text/javascript">setTimeout(function(){ $('.scout-list-success').fadeOut(); }, 3000);</script>
		<div class="card card-custom">
			<div class="card-header">
				<div class="card-title w-100">
					<span class="card-icon">
						<i class="flaticon2-supermarket text-primary"></i>
					</span>
					<div class="list_sec">
						<h3 class="card-label">List of Wishlist Experince</h3>
					</div>
<!-- 					<div class="add-btn">
						<a id="" href="{{route('scout-add-host')}}" class="btn btn-sm float-right green-bg">Add New <i class="fa fa-plus" aria-hidden="true"></i></a>
					</div> -->
				</div>
			</div>
			<div class="card-body">
				<div class="row">
					
						@php
						$addToFav = "add-to-fav";
						$is_traveler = "addToWishList";
						@endphp
						@forelse ($Wishlistdata as $key => $Wishltdata)
						<div class="col-lg-4 col-md-4 col-sm-12 wishList-main">
							<div class="exp-box">
								<div class="exp-img">
									<a href="{{ route('experience-detail',['id'=>base64_encode($Wishltdata->expid)]) }}"><img src="{{asset('pages/experiences/'.$Wishltdata->experience_feature_image)}}"></a>
								</div>
								<div class="exp-content">
									<div class="title">
										<a href="{{ route('experience-detail',['id'=>base64_encode($Wishltdata->expid)]) }}">{{ $Wishltdata->experience_name }}</a>
									</div>
									<div class="rating-price">
										<div class="rating">
											<ul mk="4">
												{!! getExperienceReview($Wishltdata->experience_id,'ratings') !!}
<!-- 												<i class="fas fa-star text-warning" aria-hidden="true"> </i>
												<i class="fas fa-star text-warning" aria-hidden="true"> </i>
												<i class="fas fa-star text-warning" aria-hidden="true"> </i>
												<i class="fas fa-star text-warning" aria-hidden="true"> </i>
												<i class="far fa-star text-warning" aria-hidden="true"> </i> -->
												<li>{{ getExperienceReview($Wishltdata->expid,'reviews') }} Reviews</li>
											</ul>
										</div>
									</div>
									<div class="price-div">
									    <span class="price-info">
									    <span class="price-span">€{{ number_format($Wishltdata->experience_low_price) }} {{ $Wishltdata->experience_high_price != 0.00 ? "- ".number_format($Wishltdata->experience_high_price) : ''  }}</span>
									    Group Price For 1-{{ $Wishltdata->experience_price_vailid_for }} Persons
									    </span>
									</div>
									<div class="booking-conditions">
										<ul>
											<li><i class="far fa-user" aria-hidden="true"></i> Max: {{ $Wishltdata->experience_group_size }}</li>
											<li><i class="far fa-clock" aria-hidden="true"></i> {{ $Wishltdata->experience_duration }} Hours</li>
											<li><i class="fas fa-map-marker-alt" aria-hidden="true"></i> {{ $Wishltdata->experience_lname }}</li>
										</ul>
									</div>

									<div class="d-flex mt-3 justify-content-center flex-wrap">
										<a href="{{ route('traveler-dashboard',['scout_id'=>$Wishltdata->scout_id]) }}" class="btn btn-light-primary font-weight-bold btn-sm px-4 font-size-base yellow-bg-btn">Chat with Scout</a>

										<a target="_blank" href="{{route('experience-detail',['id'=>base64_encode($Wishltdata->expid)])}}" class="btn btn-light-primary  ml-2 font-weight-bold btn-sm px-4 font-size-base green-bg-btn">View</a>

										<div class="price manage-list-traveler">
											<a id="wishlist-show" href="javascript:void(0)" data-default="wishlist-span" data-expid="{{ $Wishltdata->expid }}" data-id="{{ $user->id }}" class="{{ $addToFav }} {{ $is_traveler }} delete  btn btn-light-primary font-weight-bold btn-sm px-4 font-size-base ml-2 pink-bg-btn remove-wishlist-confirm">
												Remove
											</a> 
										</div>
									</div>
								</div>
							</div>
						</div>
						 
						@empty
						<div class="exp-box no-data-found no-data-backend" style="text-align: center;font-weight: 500;">
							<img src="{{asset('images/no-data-found.png')}}">
            				<h3>No Record Found.</h3>
       					 </div>
						@endforelse
				</div>
			</div>
		</div>
		<!--end::Card-->
	</div>
</div>
@endsection
{{-- Scripts Section --}}
@section('scripts')
<script type="text/javascript">
$(function() {
	$('.confirmation-callback').confirmation({
			onConfirm: function() { 

			 },
			onCancel: function() {  }
		});
	});
	</script>
@endsection
