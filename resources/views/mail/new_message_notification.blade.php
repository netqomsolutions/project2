<!DOCTYPE html>
<html>
    <head>
        <title>New Message</title>
    </head>
<body style="margin:0;padding:0;">
    <table cellspacing="0" cellpadding="0" width="600" align="center" style="font-family: Arial, Helvetica, sans-serif;background: url({{ asset('images/email-template-bg.jpg') }});">
        <tr><td style="padding:50px 0 0;" align="center"><img src="{{ asset('front_end/images/logo.png') }}"/></td></tr>
        <tr><td align="center" style="font-size:25px;color:#0d0d0d;letter-spacing: 1px;padding:40px 0 0;">DEAR SCOUT</td></tr>
        <tr><td align="center" style="color:#ff4883;font-weight: bold;font-size: 45px;letter-spacing: 1px;">{{ isset($data) ? ucfirst($data['scout']['user_fname']).' '.ucfirst($data['scout']['user_lname']): '' }}</td></tr>
        <tr><td align="center" style="font-size:16px;line-height: 27px;padding: 35px 30px 15px;color:#0d0d0d;">Someone sent you a message. Please check your dashboard ASAP.<br>Be as responsive as possible and make a name for yourself! Every minute counts!</td></tr>
        <tr><td align="center" style="color:#ff4883;font-weight: bold;font-size: 35px;letter-spacing: 1px;">Thank You!</td></tr>
        <tr><td align="center" style="font-weight: bold;font-size:17px;color:#0d0d0d;padding:0 0 20px;">Any additional questions?</td></tr>
        <tr>
            <td>
                <table width="100%" style="background: #dfdfdf;padding: 20px 25px 16px;">
                    <tr>
                        <td colspan="2" style="font-weight: bold;font-size:15px;padding:0;"><a style="color:#0d0d0d;text-decoration: none;" href="mailto:{{config('services.mail_footer_detail.email')}}"><img src="{{ asset('images/mail-icon-email.png') }}" style="margin: 0 4px 0 0px;position: relative;top: 1px;"> {{config('services.mail_footer_detail.email')}}</a></td>
                        <td align="right" colspan="2" style="font-weight: bold;font-size:15px;padding:0;"><a style="color:#0d0d0d;text-decoration: none;" href="tel:{{ RemoveFormatPhoneNo(config('services.mail_footer_detail.phone'),'+386') }}"><img src="{{ asset('images/phone-icon-email.png') }}" style="margin: 0 4px 0 0px;position: relative;top: 3px;">{{ formatPhoneNoWithFormat(config('services.mail_footer_detail.phone'),'+386')  }}</a></td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</body>
</html>