<!doctype html>
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
  <head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <meta name="format-detection" content="telephone=no">
    <meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0; user-scalable=no;">
    <meta http-equiv="x-ua-compatible" content="IE=9; IE=8; IE=7; IE=EDGE">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="x-ua-compatible" content="ie-edge">
    <meta name="x-apple-disable-message-reformatting">
    <title>Email</title> 
  </head>
<body style="margin:0;padding:0;">
    <table cellspacing="0" cellpadding="0" width="600" align="center" style="font-family: Arial, Helvetica, sans-serif;background-image: url({{ asset('images/email-template-bg.jpg') }});background-size:cover;border: 1px solid #f9f9f9;">
        <tr><td style="padding:50px 0 0;" align="center"><img src="{{ asset('front_end/images/logo.png') }}"/></td></tr>
        <tr><td align="center" style="color:#ff4883;font-weight: bold;font-size: 45px;letter-spacing: 1px;">Hi #LocalsFromZero team</td></tr>
        <tr><td align="center" style="font-size:16px;line-height: 27px;padding: 35px 50px 38px;color:#0d0d0d;">{{ isset($data) ? ucfirst($data['name']): '' }} sent you an inquiry via the »Contact us« form! Please, respond quickly!</td></tr>
        <tr>
            <td style="padding:0 0px 40px 40px;">
                <table width="100%">
                    <tr>
                        <td align="left" style="font-weight: normal;font-size:15px;padding:0;"><span style="font-weight: bold;display: block;">Name</span><br/>{{ isset($data) ? ucfirst($data['name']): '' }}</td>
                        <td align="left" style="font-weight: normal;font-size:15px;padding:0;"><span style="font-weight: bold;display: block;">E-mail</span><br/><a style="color:#0d0d0d;text-decoration: none;" href="mailto:{{ isset($data) ? $data['email'] : '' }}">{{ isset($data) ? $data['email'] : '' }}</a></td>
                         <td align="left" style="font-weight: normal;font-size:15px;padding:0;"><span style="font-weight: bold;display: block;">Phone</span><br/><a style="color:#0d0d0d;text-decoration: none;" href="tel:{{ (isset($data) && $data['phone']!='') ? $data['phone'] : '-' }}">{{ (isset($data) && $data['phone']!='') ? $data['phone'] : '-' }}</a></td>
                        <td align="left" style="font-weight: normal;font-size:15px;padding:0;"><span style="font-weight: bold;display: block;">Subject</span><br>{{ isset($data) ? $data['subject'] : '' }} </td>
                    </tr>
                    <tr>
                        <td colspan="4" style="padding:30px 0 0;">
                      <table width="100%">
                            <tbody>
                                <tr><td colspan="2"><span style="font-weight:bold;padding:0 0 10px;display: block;">Message</span></td></tr>
                                <tr><td colspan="2" style="font-size:15px;line-height: 21px;">{{ isset($data) ? $data['message'] : '' }}</td></tr>
                            </tbody>
                      </table>
                  </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr><td align="center" style="font-weight: bold;font-size:17px;color:#0d0d0d;padding:0 0 20px;">Any additional questions?</td></tr>
        <tr>
            <td>
                <table width="100%" style="background: #dfdfdf;padding: 20px 25px 16px;">
                    <tr>
                        <td colspan="2" style="font-weight: bold;font-size:15px;padding:0;"><a style="color:#0d0d0d;text-decoration: none;" href="mailto:{{config('services.mail_footer_detail.email')}}"><img src="{{ asset('images/mail-icon-email.png') }}" style="margin: 0 4px 0 0px;position: relative;top: 1px;"> {{config('services.mail_footer_detail.email')}}</a></td>
                        <td align="right" colspan="2" style="font-weight: bold;font-size:15px;padding:0;"><a style="color:#0d0d0d;text-decoration: none;" href="tel:{{ RemoveFormatPhoneNo(config('services.mail_footer_detail.phone'),'+386') }}"><img src="{{ asset('images/phone-icon-email.png') }}" style="margin: 0 4px 0 0px;position: relative;top: 3px;">{{ formatPhoneNoWithFormat(config('services.mail_footer_detail.phone'),'+386')  }}</a></td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</body>
</html>