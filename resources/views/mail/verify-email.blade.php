<!DOCTYPE html>
<html>
    
<body style="margin:0;padding:0;">
    <table cellspacing="0" cellpadding="0" width="600" align="center" style="font-family: Arial, Helvetica, sans-serif;background: url({{ asset('images/email-template-bg.jpg') }});">
        <tr><td style="padding:50px 0 0;" align="center"><img src="{{ asset('front_end/images/logo.png') }}"/></td></tr>
        <tr><td align="center" style="font-size:25px;color:#0d0d0d;letter-spacing: 1px;padding:40px 0 0;">Hi</td></tr>
        <tr><td align="center" style="color:#ff4883;font-weight: bold;font-size: 45px;letter-spacing: 1px;">{{ isset($user) ? ucfirst($user->user_fname).' '.ucfirst($user->user_lname): '' }}</td></tr>
        <tr><td align="center" style="font-size:16px;line-height: 27px;padding: 35px 50px 38px;color:#0d0d0d;">Thank you for signing up for the <span style="color:#91ba2b;">#LocalsFromZero</span> marketplace. Welcome aboard! Enjoy exploring and visiting lesser known places, locals and their authentic experiences. Start making pleasant memories and meaningful connections.</td></tr>
        <tr><td align="center" style="padding:0 0 50px;"><a style="background-color: #ff4883;color: #fff;text-decoration: none;font-size: 15px;padding: 14px;border-radius: 7px;display:inline-block;" href="{{$verify_url}}">Verify Email Address</a></td></tr>
        <tr><td align="center" style="font-weight: bold;font-size:17px;color:#0d0d0d;padding:0 0 20px;">Any additional questions?</td></tr>
        <tr>
            <td>
                <table width="100%" style="background: #dfdfdf;padding: 20px 25px 16px;">
                     <tr>
                        <td colspan="2" style="font-weight: bold;font-size:15px;padding:0;"><a style="color:#0d0d0d;text-decoration: none;" href="mailto:{{config('services.mail_footer_detail.email')}}"><img src="{{ asset('images/mail-icon-email.png') }}" style="margin: 0 4px 0 0px;position: relative;top: 1px;"> {{config('services.mail_footer_detail.email')}}</a></td>
                        <td align="right" colspan="2" style="font-weight: bold;font-size:15px;padding:0;"><a style="color:#0d0d0d;text-decoration: none;" href="tel:{{ RemoveFormatPhoneNo(config('services.mail_footer_detail.phone'),'+386') }}"><img src="{{ asset('images/phone-icon-email.png') }}" style="margin: 0 4px 0 0px;position: relative;top: 3px;">{{ formatPhoneNoWithFormat(config('services.mail_footer_detail.phone'),'+386')  }}</a></td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</body>
</html>