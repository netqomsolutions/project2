{{-- Extends layout --}}
@extends('layout.default')
{{-- Content --}}
@section('content')
<style type="text/css">
	.dataTables_wrapper .dataTable th:before {content:none !important;}
	.dataTables_wrapper .dataTable th:after {content:none !important;}
</style>
<div class="row">
    <div class="col-lg-12">
    	<meta name="csrf-token" content="{{ csrf_token() }}" />
       	<!--begin::Card-->
       	@if (session('success'))
		    <div class="alert alert-success scout-list-success">
		        {{ session('success') }}
		    </div>
		@elseif(session('failure'))
			<div class="alert alert-danger scout-list-success">
		        {{ session('failure') }}
		    </div>			    
		@endif
		<script type="text/javascript">setTimeout(function(){ $('.scout-list-success').fadeOut(); }, 3000);</script>
		<div class="card card-custom">
			<div class="card-header">
				<div class="card-title">
					<span class="card-icon">
						<i class="flaticon2-supermarket text-primary"></i>
					</span>
					<h3 class="card-label">List Of Experiences</h3>
				</div>
			</div>
			<div class="card-body">
				<div class="card-body p-0">
					<div class="form-group d-flex align-items-center flex-wrap">
						<select id="status" class="form-control" style="width: 200px">
							<option value="">--Select Status--</option>
							<option value="1">Completed</option>
							<option value="2">Upcoming</option>
						</select>
						<button class="btn btn-success btn-md ml-2" name="refresh" id="refresh">Refresh</button>
					</div>
				</div>
				<!--begin: Datatable-->
				<table class="table table-bordered yajra-datatable table-responsive-md list-exp-tble" style="margin-top: 13px !important">
					<thead>
						<tr>
							<th>Image</th>
							<th style="width:185px;">Experience Name</th>
							<th>Scout</th>
							<th>Status</th>
							<th>Actions</th>
						</tr>
					</thead>
				</table>
				<!--end: Datatable-->
			
			</div>
		</div>
		<!--end::Card-->
    </div>
</div>
@endsection
{{-- Scripts Section --}}
@section('scripts')
<script type="text/javascript">
  	
  	$(document).ready(function(){
  		$.ajaxSetup({ headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') } });
  		var table = $('.yajra-datatable').DataTable({
	        processing: true,
	        serverSide: true,
	        ajax: "{{ route('host-experience-list') }}",
	        columns: [
	            {data: 'DT_RowIndex', id: 'DT_RowIndex',orderable: false,render:function(data,type,row){	       
	            	var imagUrl = '';
	            	
	            		imagUrl = HOST_URL + '/public/pages/experiences/'+row.experience_feature_image;
	            		return "<img src=\"" + imagUrl + "\" height=\"50\"  width=\"75\"/>";
	            		            
	            }},
	            {data: 'experience_name', name: 'experience_name'},
	            {data: 'user_fname', name: 'user_fname',render:function(data, type, row){ 
	                return row.user ? row.user.user_fname +' '+ row.user.user_lname : 'Not assign';
	            }},
	            {data: 'status', name: 'status',render:function(data, type, row){ 
	                var statusLeadColor = '';
	                var leadStatusName = '';
	                if(row.status == '1'){
	                    statusLeadColor = 'badge badge-success';
	                    leadStatusName = 'Active';
	                }else if (row.status == '0') {
	                    statusLeadColor = 'badge badge-danger';
	                    leadStatusName = 'Waiting for admin approval.';
	                }
	                return leadStatusName 
	            }},
	            {
	                data: 'action', 
	                name: 'action', 
	                orderable: false, 
	                searchable: true
	            },
	        ],
	        order: [[1, 'asc']]
	    });
    });

</script>
@endsection