{{-- Extends layout --}}
@extends('layout.default')
{{-- Content --}}
@section('content')
<style>
	.toggle.ios, .toggle-on.ios, .toggle-off.ios { border-radius: 20rem; }
	.toggle.ios .toggle-handle { border-radius: 20rem; }
	div#action-scout {
   align-items: center;
   justify-content: center;
 }
</style>
<div class="container">
  <div class="main-body">
    <div class="row gutters-sm">
      <div class="col-md-4 mb-3">
        <div class="card">
          <div class="card-body">
            <div class="d-flex flex-column align-items-center text-center">
              <img src="{{ asset('pages/experiences/'.$experience['experience_feature_image']) }}" alt="{{$experience->experience_name}}" class="rounded-circle" width="150" />
              <div class="mt-3">
                <h4>{{$experience->experience_name}}</h4>
              </div> 
            </div>
          </div>
        </div>
      </div>
      <div class="col-md-8">
        <div class="card mb-3">
          <div class="card-body">
            <div class="row">
              <div class="col-sm-3">
                <h6 class="mb-0">Location</h6>
              </div>
              <div class="col-sm-9 text-secondary">
               {{ $experience->experience_lname  }}
              </div>
            </div>
            <hr>
            <div class="row">
              <div class="col-sm-3">
                <h6 class="mb-0">Price</h6>
              </div>
              <div class="col-sm-9 text-secondary">
               {{ number_format($experience->experience_low_price) }} {{ $experience->experience_high_price != 0.00 ? "- ".number_format($experience->experience_high_price) : ''  }}
              </div>
            </div>
            <hr>
            <div class="row">
              <div class="col-sm-3">
                <h6 class="mb-0">valid for:</h6>
              </div>
              <div class="col-sm-9 text-secondary">
               1 - {{$experience->experience_price_vailid_for}} guests
             </div>
            </div>
            <hr>  
            <div class="row">
              <div class="col-sm-3">
                <h6 class="mb-0">Extra guest:</h6>
              </div>
              <div class="col-sm-9 text-secondary">

              </div>
            </div>
            <hr> 
            <div class="row">
              <div class="col-sm-3">
                <h6 class="mb-0">Max guests:</h6>
              </div>
              <div class="col-sm-9 text-secondary">
               {{$experience->experience_group_size}}
              </div>
            </div>
            <hr>
            <div class="row">
              <div class="col-sm-3">
                <h6 class="mb-0">Duration:</h6>
              </div>
              <div class="col-sm-9 text-secondary">
                {{$experience->experience_duration}} h
              </div>
            </div>
            <hr> 
            <div class="row">
              <div class="col-sm-3">
                <h6 class="mb-0">Book in advance:</h6>
              </div>
              <div class="col-sm-9 text-secondary">
               {{$experience->experience_booking_in_advance_time}} h
              </div>
            </div>
            <hr>
            <div class="row">
              <div class="col-sm-3">
                <h6 class="mb-0">Status</h6>
              </div>
              <div class="col-sm-9 text-secondary">
                 <?php if($experience['status'] == 0){
                  echo "Inactive";
                }else if($experience['status'] == 1){
                  echo "Active";
                } ?>
              </div>
            </div>
            <hr>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- /.modal -->
</div>

@endsection
{{-- Scripts Section --}}
@section('scripts')
<script type="text/javascript">

</script>
@endsection