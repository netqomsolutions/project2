{{-- Extends layout --}}
@extends('layout.default')
{{-- Content --}}
@section('content')
@php
$hosLanguage= getUserLangById($experience->experience->host->id);
$condata = json_decode($contact->page_content,true);
$contact->email=$condata['email'];
$contact->address=$condata['address'];
$contact->phone=$condata['phone'];

 $experience_price_vailid_for = $experience->experience->experience_price_vailid_for;
            $children_discount_1 = $experience->experience_meta_detail->children_discount_1;
            $totalAmt = 0;
            $totalChildAmt = 0;
            if($experience->experience->experience_type == 1){
                for ($i=0; $i < $experience->adults ; $i++) { 
                    if($i<$experience->experience->experience_price_vailid_for){
                        $totalAmt = $experience->experience->experience_low_price;
                    }else{
                        $totalAmt = $totalAmt + $experience->experience->experience_additional_person;
                    }
                }
            }else if($experience->experience->experience_type == 2){
                for ($i=0; $i < $experience->adults ; $i++) { 
                    if($i < $experience->experience->minimum_participant){
                        $totalAmt = $experience->experience->experience_low_price;
                    }else{
                        $totalAmt = $totalAmt + $experience->experience->experience_additional_person;
                    }
                }
            }else if($experience->experience->experience_type == 3){
                if(!empty($experience->experience->experience_additional_prices)){
                    $totalAmt = $experience->experience->experience_additional_prices[0]->additional_price;
                }
            }
            $totalChildAmt = $experience->children * $experience->experience->experience_low_price;
            $disPrice = ($children_discount_1 / 100 ) * $totalChildAmt; 
            $childNetPrice = $totalChildAmt - $disPrice;
            $totalaAmt =  $childNetPrice + $totalAmt;
@endphp
<style type="text/css">
	.dataTables_wrapper .dataTable th:before {content:none !important;}
	.dataTables_wrapper .dataTable th:after {content:none !important;}
</style>
<div class="row icons-same-class">
	<div class="col-lg-12">
		<meta name="csrf-token" content="{{ csrf_token() }}" />
		<!--begin::Card-->
		@if (session('success'))
		<div class="alert alert-success scout-list-success">
			{{ session('success') }}
		</div>
		@elseif(session('failure'))
		<div class="alert alert-danger scout-list-success">
			{{ session('failure') }}
		</div>			    
		@endif
		<script type="text/javascript">setTimeout(function(){ $('.scout-list-success').fadeOut(); }, 3000);</script>
								
								<div class="card card-custom gutter-b">
									<div class="card-body pb-custom">
										<div class="d-flex">
											<!--begin: Pic-->
											<div class="flex-shrink-0 mr-7 mt-lg-0 mt-3">
												<div class="symbol symbol-120 symbol-lg-140">
													<img alt="Pic" src="{{ asset('pages/experiences/'.$experience->experience->experience_feature_image)}}">
												</div>
												<div class="d-flex flex-column my-2 exp-box-btm-list">
															<a class="text-muted text-hover-primary font-weight-bold mb-1 cursor-pointer">
															<i class="far fa-clock"></i> <span>Duration: <b>{{$experience->experience->experience_duration}} h</b></span></a>
															<a class="text-muted text-hover-primary font-weight-bold mb-1 cursor-pointer"><i class="fas fa-user-plus"></i> <span>Extra guest: <b>€{{number_format($experience->experience->experience_additional_person)}}</b></span></a>
															<a class="text-muted text-hover-primary font-weight-bold cursor-pointer"><i class="far fa-user"></i> <i class="far fa-user"></i> <span>Max guests: <b>{{$experience->experience->maximum_participant}}</b></span></a>
														</div>
												
											</div>
											<!--end: Pic-->
											<!--begin: Info-->
											<div class="flex-grow-1">
												<!--begin: Title-->
												<div class="d-flex align-items-center justify-content-between flex-wrap">
														<!--begin::Name-->
														<a href="#" class="d-flex align-items-center text-dark text-hover-primary font-size-h5 font-weight-bold mr-3">{{$experience->experience->experience_name}}</a>
														<!--end::Name-->
														<div class="d-flex align-items-center flex-wrap bg-transparent mt-res-class">
															<div class="mr-2">
																@if($experience->status==0)
																<a data-toggle="tooltip" data-theme="dark" title="In Processing" type="button" class="btn btn-sm btn-text btn-light-danger text-uppercase font-weight-bold pink-bg-btn" href="javascript:void(0)"><i class="la la-spinner p-0 text-white"></i> Processing</a>
																@elseif($experience->status==1)
																<a data-toggle="tooltip" data-theme="dark" title="Approved" href="javascript:void(0);" type="button" class="btn btn-sm btn-text btn-light-danger text-uppercase font-weight-bold green-bg-btn"><i class="la la-check-circle p-0 text-white"></i> Approved</a>
																@elseif($experience->status==2 || $experience->status==6 || $experience->status==7 || $experience->status==8 || $experience->status==9)
																<a data-toggle="tooltip" data-theme="dark" title="Rejected" href="javascript:void(0);" type="button" class="btn btn-sm btn-text btn-light-danger text-uppercase font-weight-bold pink-bg-btn"><i class="la la-times-circle p-0 text-white"></i> Rejected</a>
																@endif
															</div>
															<div class="mr-2">
																<a  href="{{route('host-pdf-create',['id'=> base64_encode($experience->id)])}}" data-toggle="tooltip" data-theme="dark" title="Download invoice" type="button" class="btn btn-sm btn-text btn-light-danger text-uppercase font-weight-bold pink-bg-btn"><i class="la la-download p-0 text-white"></i></a>
															</div>
															<div class="">
																<button data-toggle="modal"  data-target="#kt_chat_modal" data-toggle="tooltip" data-theme="dark" title="View invoice" type="button" class="btn btn-sm btn-text btn-light-danger text-uppercase font-weight-bold pink-bg-btn"  ><i class="la la-eye p-0 text-white"></i></button>
															</div>
														</div>
													<div class="flex-grow-1 font-weight-bold text-dark-50 py-5 py-lg-2 mr-5">{{$experience->experience->experience_description}}</div>
													<div class="d-flex align-items-center justify-content-between flex-wrap w-100">
										<!--begin: Content-->
												<div class="d-flex align-items-center flex-wrap justify-content-between">
													<div class="d-flex flex-wrap align-items-center py-2">
															<div class="mr-6">
																<div class="font-weight-bold mb-1 font-size-dec">Booked Date</div>
																<span class="btn btn-sm btn-text btn-light-primary text-uppercase font-weight-bold green-bg-btn">{{date('d-M-Y',strtotime($experience->booking_date))}}</span>
															</div>
															<div class="mr-6">
																<div class="font-weight-bold mb-1 font-size-dec">Start Time</div>
																<span class="btn btn-sm btn-text btn-light-danger text-uppercase font-weight-bold green-bg-btn">{{date('H:i A',strtotime($experience->booking_start_time))}}</span>
															</div>
															<div class="mr-6">
																<div class="font-weight-bold mb-1 font-size-dec">End Time</div>
																<span class="btn btn-sm btn-text btn-light-danger text-uppercase font-weight-bold green-bg-btn">{{date('H:i A',strtotime($experience->booking_end_time))}}</span>
															</div>
															<div class="mr-6">
																<div class="font-weight-bold mb-1 font-size-dec">Amount</div>
																<span class="btn btn-sm btn-text btn-light-danger text-uppercase font-weight-bold green-bg-btn">€{{number_format($experience->amount)}}</span>
															</div>
															<div class="">
																<div class="font-weight-bold mb-1 font-size-dec">Discount</div>
																<span class="btn btn-sm btn-text btn-light-danger text-uppercase font-weight-bold green-bg-btn">€{{number_format($experience->discount)}}</span>
															</div>
													</div>
												</div>
												<!--end: Content-->
										
										<!--begin: Items-->
										<div class="d-flex align-items-center flex-wrap text-font-change">
											<!--begin: Item-->
											<div class="d-flex align-items-center flex-lg-fill mr-7 my-1">
												<span class="mr-2">
													<i class="icon-2x text-muted fas fa-male"></i>
												</span>
												<div class="d-flex flex-column text-dark-75">
													<span class="font-weight-bold font-size-sm">Adults</span>
													<span class="font-weight-bold font-size-h5">
													<span class="text-dark-50 font-weight-bold"></span>{{$experience->adults}}</span>
												</div>
											</div>
											<!--end: Item-->
											<!--begin: Item-->
											<div class="d-flex align-items-center flex-lg-fill mr-7 my-1">
												<span class="mr-2">
													<i class="icon-2x text-muted fas fa-child"></i>
												</span>
												<div class="d-flex flex-column text-dark-75">
													<span class="font-weight-bold font-size-sm">Children</span>
													<span class="font-weight-bold font-size-h5">
													<span class="text-dark-50 font-weight-bold"></span>{{$experience->children}}</span>
												</div>
											</div>
											<!--end: Item-->
											<!--begin: Item-->
											<div class="d-flex align-items-center flex-lg-fill mr-0 my-1">
												<span class="mr-2">
													<i class="icon-2x text-muted fas fa-baby"></i>
												</span>
												<div class="d-flex flex-column text-dark-75">
													<span class="font-weight-bold font-size-sm">Infants</span>
													<span class="font-weight-bold font-size-h5">
													<span class="text-dark-50 font-weight-bold"></span>{{$experience->infants}}</span>
												</div>
											</div>
											<!--end: Item-->
										</div>
										<!--begin: Items-->
										</div>
												</div>
												<!--end: Title-->
											</div>
											<!--end: Info-->
										</div>
										<!--div class="separator separator-solid my-7"></div-->
																			
									</div>
								</div>
								<div class="d-flex justify-content-between scout-booking-details">
									<div class="card card-custom gutter-b col-md-6 p-0">
									<div  class="card-heading-custom d-flex align-items-center justify-content-between mb-8">
									<h3>Booked By</h3>
									
									</div>
									<div class="card-body pt-0">
										<div class="d-flex">
											<!--begin: Pic-->
											<div class="flex-shrink-0 mr-7 mt-lg-0 mt-3">
												<div class="symbol symbol-50 symbol-lg-120">
													@if ( file_exists(public_path($experience->traveler->user_image_path)) && $experience->traveler->user_image)
													@if(filter_var($experience->traveler->user_image, FILTER_VALIDATE_URL) === FALSE)
                                                <img src="{{ asset($experience->traveler->user_image_path.'/'.$experience->traveler->user_image) }}" alt="Admin" class="" />
                                                @else
                                                 <img src="{{ $experience->traveler->user_image}}" alt="Admin" class="" />
                                                @endif
                                                @else
                                                <img src="{{ asset('users/user.png') }}" alt="Admin" class=""  />
                                                @endif
												</div>
											</div>
											<!--end: Pic-->
											<!--begin: Info-->
											<div class="flex-grow-1">
												<!--begin: Title-->
												<div class="d-flex align-items-center justify-content-between flex-wrap">
													<div class="mr-3">
														<!--begin::Name-->
														<a href="#" class="d-flex align-items-center text-dark text-hover-primary font-size-h5 font-weight-bold mr-3">{{$experience->traveler->user_fname .' '.$experience->traveler->user_lname}}</a>
														<!--end::Name-->
														<!--begin::Contacts-->
														<div class="d-flex flex-wrap my-2 info-book-detail">
															<div class="left-icon-host col-md-6 p-0">
															<a href="mailto:{{$experience->traveler->email}}" class="text-muted text-hover-primary font-weight-bold mr-lg-8 mr-5 mb-lg-0 mb-2">
															<i class="icon-xl la la-envelope-o"></i><span>{{$experience->traveler->email}}</span></a>
															<a href="tel:{{RemoveFormatPhoneNo($experience->traveler->user_mobile,$experience->traveler->user_mobile_code)}}" class="text-muted text-hover-primary font-weight-bold mr-lg-8 mr-5 mb-lg-0 mb-2"><i class="icon-xl la la-phone"></i><span>{{formatPhoneNo($experience->traveler->user_mobile,$experience->traveler->user_mobile_code)}}</span></a>
															</div>
															<div class="left-icon-host col-md-6 p-0">
															<a class="text-muted text-hover-primary font-weight-bold cursor-pointer">
															<i class="icon-xl la la-location-arrow"></i><span>{{$experience->traveler->user_address}}</span></a>
															</div>
														</div>
														<!--end::Contacts-->
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="card card-custom gutter-b col-md-6 p-0">
								<div  class="card-heading-custom d-flex align-items-center justify-content-between mb-8">
									<h3>Assigned By</h3>
									<a data-toggle="tooltip" data-theme="dark" title="Call now" href="tel:{{RemoveFormatPhoneNo($experience->scout->user_mobile,$experience->scout->user_mobile_code)}}" class="btn btn-sm btn-light-success pink-bg-btn font-weight-bolder"><i class="fas fa-phone-volume p-0"></i></a>
									</div>
									<div class="card-body pt-0">
										<div class="d-flex">
											<!--begin: Pic-->
											<div class="flex-shrink-0 mr-7 mt-lg-0 mt-3">
												<div class="symbol symbol-50 symbol-lg-120">
												@if ( file_exists(public_path($experience->scout->user_image_path)) && $experience->scout->user_image)
												@if(filter_var($experience->scout->user_image, FILTER_VALIDATE_URL) === FALSE)
                                                <img src="{{ asset($experience->scout->user_image_path.'/'.$experience->scout->user_image) }}" alt="Admin" class="" />
                                                @else
                                                <img src="{{ $experience->scout->user_image }}" alt="Admin" class="" />
                                                @endif
                                                @else
                                                <img src="{{ asset('users/user.png') }}" alt="Admin" class=""  />
                                                @endif
												</div>
											</div>
											<!--end: Pic-->
											<!--begin: Info-->
											<div class="flex-grow-1">
												<!--begin: Title-->
												<div class="d-flex align-items-center justify-content-between flex-wrap">
													<div class="mr-3">
														<!--begin::Name-->
														<a href="#" class="d-flex align-items-center text-dark text-hover-primary font-size-h5 font-weight-bold mr-3">{{$experience->scout->user_fname.' '. $experience->scout->user_lname}}</a>
														<!--end::Name-->
														<!--begin::Contacts-->
														<div class="d-flex flex-wrap my-2 info-book-detail">
															<div class="left-icon-host col-md-6 p-0">
															<a href="mailto:{{$experience->scout->email}}" class="text-muted text-hover-primary font-weight-bold mr-lg-8 mr-5 mb-lg-0 mb-2">
															<i class="icon-xl la la-envelope-o"></i><span>{{$experience->scout->email}}</span></a>
															<a href="tel:{{RemoveFormatPhoneNo($experience->experience->host->user_mobile,$experience->experience->host->user_mobile_code)}}" class="text-muted text-hover-primary font-weight-bold mr-lg-8 mr-5 mb-lg-0 mb-2"><i class="icon-xl la la-phone"></i><span>{{formatPhoneNo($experience->scout->user_mobile,$experience->scout->user_mobile_code)}}</span></a>
															<a class="text-muted text-hover-primary font-weight-bold cursor-pointer">
															<i class="icon-xl la la-location-arrow"></i><span>{{$experience->scout->user_address}}</span></a>
															</div>
															
														</div>
														<!--end::Contacts-->
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
								</div>

								<div class="modal modal-sticky  " id="kt_chat_modal" data-backdrop="false" style="display: none;" aria-hidden="true">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<!--begin::Card-->
					<div class="card card-custom position-relative">
					<button type="button" class="btn btn-clean btn-sm btn-icon btn-icon-md close-invoice-btn" data-dismiss="modal">
									<i class="ki ki-close icon-1x"></i>
								</button>
						<!--begin::Header-->
						<!--div class="card-header align-items-center px-4 py-3">
							<div class="text-left flex-grow-1">
								 
							</div>
							<div class="text-center flex-grow-1">
								<div class="text-dark-75 font-weight-bold font-size-h5">INVOICE</div>
								 
							</div>
							<div class="text-right flex-grow-1">
								<button type="button" class="btn btn-clean btn-sm btn-icon btn-icon-md" data-dismiss="modal">
									<i class="ki ki-close icon-1x"></i>
								</button>
							</div>
						</div-->
						<!--end::Header-->
						<!--begin::Body-->
						<div class="card-body p-0">
						
							<!--begin::Scroll-->
							<div class="scroll scroll-pull ps ps--active-y p-0 m-0" data-height="375" data-mobile-height="300" style="height: 375px; overflow: hidden;">
								<!--begin::Messages--> 
								        <div class="d-flex flex-row w-100">
											<div class="card card-custom overflow-hidden w-100 section-to-print">
												<div class="card-body p-0 pl-5 pr-5 " id="printdivcontent">
													<!-- begin: Invoice-->
													<!-- begin: Invoice header-->
													<div class="row justify-content-center py-8 px-8 px-md-0">
														<div class="col-md-12">
															<div class="d-flex pb-5 flex-column">
																<h1 class="display-4 font-weight-boldest mb-3">INVOICE</h1>
																<div class="d-flex flex-column px-0">
																	<!--begin::Logo-->
																	<!--end::Logo-->
																	<span class="d-flex flex-column opacity-70">
																	    <span>{{$contact->address}}</span>
																		<!--span>Mississippi 96522</span-->
																	</span>
																</div>
															</div>
															<div class="border-bottom w-100"></div>
															<div class="d-flex justify-content-between pt-6">
																<div class="d-flex flex-column flex-root">
																	<span class="font-weight-bolder mb-2">Date</span>
																	<span class="opacity-70">{{date('M d, Y ', strtotime($experience->booking_date))}}</span>
																</div>
																<div class="d-flex flex-column flex-root">
																	<span class="font-weight-bolder mb-2">INVOICE NO.</span>
																	<span class="opacity-70">{{$experience->charge_id}}</span>
																</div>
																<div class="d-flex flex-column flex-root">
																	<span class="font-weight-bolder mb-2">INVOICE TO.</span>
																	<span class="opacity-70">{{$experience->traveler->user_address}}</span>
																</div>
															</div>
														</div>
													</div>
													<div class="border-bottom w-100"></div>
													<!-- end: Invoice header-->
													<!-- begin: Invoice body-->
														<div class="row justify-content-center py-8 px-8 py-md-10 px-md-0">
															<div class="col-md-9">
																<div class="table-responsive">
																	<table class="table">
																		<thead>
																			<tr>
																				<th class="pl-0 font-weight-bold text-muted text-uppercase">Members</th>
																				<th class="text-right font-weight-bold text-muted text-uppercase">Count</th>
																				<th class="text-right font-weight-bold text-muted text-uppercase">Discount</th>
																				<th class="text-right pr-0 font-weight-bold text-muted text-uppercase">Amount</th>
																			</tr>
																		</thead>
																		<tbody>
																			<tr class="font-weight-boldest">
																				<td class="pl-0 pt-7">Adults </td>
																				<td class="text-right pt-7">{{$experience->adults}}</td>
																				<td class="text-right pt-7">0</td>
																				<td class="text-pink pr-0 pt-7 text-right">{{number_format($totalAmt)}}</td>
																			</tr>
																			<tr class="font-weight-boldest border-bottom-0">
																				<td class="border-top-0 pl-0 py-4">Children </td>
																				<td class="border-top-0 text-right py-4">{{$experience->children}}</td>
																				<td class="border-top-0 text-right py-4">{{number_format($disPrice)}}</td>
																				<td class="text-pink border-top-0 pr-0 py-4 text-right">{{number_format($childNetPrice)}}</td>
																			</tr>
																			<tr class="font-weight-boldest border-bottom-0">
																				<td class="border-top-0 pl-0 py-4">Infants</td>
																				<td class="border-top-0 text-right py-4">{{$experience->infants}}</td>
																				<td class="border-top-0 text-right py-4">0</td>
																				<td class="text-pink border-top-0 pr-0 py-4 text-right">Free</td>
																			</tr>
																		</tbody>
																	</table>
																</div>
															</div>
														</div>
													<!-- end: Invoice body-->
											 
														<div class="row justify-content-center bg-gray-100 py-8 px-8 py-md-10 px-md-0">
															<div class="col-md-9">
																<div class="table-responsive">
																	<table class="table">
																		<thead>
																			<tr>
																				<th class="font-weight-bold text-muted text-uppercase">Card</th>
																				<th class="font-weight-bold text-muted text-uppercase">Number</th>
																				<th class="font-weight-bold text-muted text-uppercase">Expiry Date</th>
																				<th class="font-weight-bold text-muted text-uppercase">TOTAL AMOUNT</th>
																			</tr>
																		</thead>
																		<tbody>
																			<tr class="font-weight-bolder">
																				<td>{{$experience->network}}</td>
																				<td>**** **** **** {{$experience->last4}}</td>
																				<td>{{$experience->exp_month.'/'.$experience->exp_year}}</td>
																				<td class="text-pink font-size-h3 font-weight-boldest">{{number_format($totalaAmt)}}</td>
																			</tr>
																		</tbody>
																	</table>
																</div>
															</div>
														</div>
													<!-- end: Invoice footer-->
													<!-- begin: Invoice action-->
													<!--div class="row justify-content-center py-8 px-8 py-md-10 px-md-0">
														<div class="col-md-12">
															<div class="d-flex justify-content-between">
															 
															</div>
														</div>
													</div-->
													<!-- end: Invoice action-->
													<!-- end: Invoice-->
												</div>
											</div>
								        </div> 
								<!--end::Messages-->
							<div class="ps__rail-x" style="left: 0px; bottom: 0px;"><div class="ps__thumb-x" tabindex="0" style="left: 0px; width: 0px;"></div></div><div class="ps__rail-y" style="top: 0px; right: -2px; height: 375px;"><div class="ps__thumb-y" tabindex="0" style="top: 0px; height: 133px;"></div></div></div>
							<!--end::Scroll-->
						</div>
						<!--end::Body-->
						<!--begin::Footer-->
						<div class="card-footer align-items-center p-5">
						    <a  href="{{route('host-pdf-create',['id'=> base64_encode($experience->id)])}}" data-toggle="tooltip" data-theme="dark" title="Download invoice" type="button" class="btn btn-sm btn-text btn-light-danger text-uppercase font-weight-bold pink-bg-btn" onclick=""><i class="la la-download p-0 text-white"></i></a>
							<button  data-toggle="tooltip" data-theme="dark" title="Print invoice" type="button" class="btn btn-sm btn-text btn-light-danger text-uppercase font-weight-bold pink-bg-btn" onclick="PrintDiv();"  ><i class="la la-print p-0 text-white"></i></button>
							<!--begin::Compose-->
						</div>
						<!--end::Footer-->
					</div>
					<!--end::Card-->
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">  
    function PrintDiv() 
   {  
       var divContents = document.getElementById("printdivcontent").innerHTML;  
       var printWindow = window.open('', '', '');  
       printWindow.document.write('<html><head><title>LFZ Invoice</title>');  
       printWindow.document.write('<link href="{{ asset('css/style.bundle.css') }}" rel="stylesheet" type="text/css"></head><body >');  
       printWindow.document.write(divContents);  
       printWindow.document.write('</body></html>');  
     
       setTimeout(function(){  printWindow.print();printWindow.close();   }, 500);  
       
    }  
</script>  
@endsection