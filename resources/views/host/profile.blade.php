{{-- Extends layout --}}
@extends('layout.default')
{{-- Content --}}
@section('content')

@php 
    $company_na = '';
    $company_reg_no = '';
    $company_link = '';
    $legal_req = '';
    $guid_txt = '';
    $other_txt = '';
    $acc_hold_name = '';
    $bank_name = '';
    $swift_code = '';
    $iban_num = '';
    $form_action = '';

    if(isset($scoutData) && optional($scoutData->user_meta)){
        $company_na = optional($scoutData->user_meta)->company_name;
        $company_reg_no = optional($scoutData->user_meta)->company_registration_no;
        $company_link = optional($scoutData->user_meta)->company_web_link;
        $legal_req = optional($scoutData->user_meta)->legal_requirements;
        $guid_txt = optional($scoutData->user_meta)->guiding_text;
        $other_txt = optional($scoutData->user_meta)->other_text;
    }

    if(isset($scoutData) && optional($scoutData->user_bank_detail)){

        $acc_hold_name = optional($scoutData->user_bank_detail)->account_holder_name;

        $bank_name = optional($scoutData->user_bank_detail)->bank_name;

        $swift_code = optional($scoutData->user_bank_detail)->swift_code;

        $iban_num = optional($scoutData->user_bank_detail)->iban_number;
    }

    if(Auth::user()->user_role==3){
        $form_action = route('scout-create-host');
    }
    elseif(Auth::user()->user_role==4){
        $form_action = route('host-profile-update');
    }
@endphp
<style>
	.toggle.ios, .toggle-on.ios, .toggle-off.ios { border-radius: 20rem; }
	.toggle.ios .toggle-handle { border-radius: 20rem; }
	div#action-scout {
	    align-items: center;
	    justify-content: center;
	}
</style>

 @if(session()->has('success'))
            <div class="alert alert-success alluser-page-suc" style="text-align: left;float: left;width: 100%;">
                {{ session()->get('success') }}
            </div>
            <script type="text/javascript">setTimeout(function(){ $('.alluser-page-suc').fadeOut(); }, 3000);</script>
        @elseif(session()->has('failure'))
            <div class="alert alert-danger alluser-page-suc" style="text-align: left; left;float: left;width: 100%;">
                {{ session()->get('failure') }}
            </div>
            <script type="text/javascript">setTimeout(function(){ $('.alluser-page-suc').fadeOut(); }, 3000);</script>
        @endif
    <div class="main-body">
          <!--  <div class="user-profile-buttons">
            <a href="{{ route('user-profile-edit') }}" class="navi-item">Update Profile</a>
            <a href="{{ route('user-change-password-form') }}" class="navi-item">Change Password</a>
          </div> -->
		  
		  
		  <div class="d-flex flex-row w-100">
								@include('profile-sidebar')
									<!--begin::Content-->
									<div class="flex-row-fluid ml-lg-8 profile-right_form">
										<!--begin::Card-->
										<div class="card card-custom card-stretch card-sticky" id="kt_page_sticky_card">
											<!--begin::Header-->
											<div class="card-header py-3 ">
												<div class="card-title align-items-start flex-column">
													<h3 class="card-label font-weight-bolder text-dark">Personal Information</h3>
													<span class="text-muted font-weight-bold font-size-sm mt-1">{{ isset($scoutData) ? 'Update' : 'Add' }} host personal informaiton</span>
												</div>
												<div class="card-toolbar">
													<button type="button" class="btn btn-success mr-2 green-bg-btn" id="save-profile-button">Save Changes</button>
													<!-- <button type="reset" class="btn btn-secondary">Cancel</button> -->
												</div>
											</div>
											<!--end::Header-->
											<!--begin::Form-->
											<form id="add-host-form" class="form" method="post" action="{{ route('profile-update') }}" enctype="multipart/form-data">
												<!--begin::Body-->
												@csrf
												<div class="card-body">
													<div class="form-group row manage-personal-info">
														<div class="col-lg-4">
														<label class="col-xl-12 col-lg-12 col-form-label">Avatar</label>
														<div class="col-lg-12 col-xl-12">
															@if (isset($scoutData) && file_exists(public_path($scoutData->user_image_path)) && $scoutData->user_image)
															<div class="image-input image-input-outline" id="kt_profile_avatar" style="background-image: url({{ asset($scoutData->user_image_path.'/'.$scoutData->user_image) }})">
															@else
															<div class="image-input image-input-outline" id="kt_profile_avatar" style="background-image: url({{ asset('users/user.png') }})">
															@endif
																<div class="image-input-wrapper" style="background-image: url(/metronic/theme/html/demo1/dist/assets/media/users/300_21.jpg)"></div>
																<label class="btn btn-xs btn-icon btn-circle btn-white btn-hover-text-primary btn-shadow" data-action="change" data-toggle="tooltip" title="" data-original-title="Change avatar">
																	<i class="fa fa-pen icon-sm text-muted"></i>
																	<input type="file" name="user_image">
																	<input type="hidden" name="profile_avatar_remove">
																</label>
																<span class="btn btn-xs btn-icon btn-circle btn-white btn-hover-text-primary btn-shadow" data-action="cancel" data-toggle="tooltip" title="" data-original-title="Cancel avatar">
																	<i class="ki ki-bold-close icon-xs text-muted"></i>
																</span>
																<span class="btn btn-xs btn-icon btn-circle btn-white btn-hover-text-primary btn-shadow" data-action="remove" data-toggle="tooltip" title="" data-original-title="Remove avatar">
																	<i class="ki ki-bold-close icon-xs text-muted"></i>
																</span>
															</div>
															<span class="form-text text-muted">Allowed file types: png, jpg, jpeg.</span>
															<span class="form-text text-muted">Note: Please upload image of 160 X 160 dimension.</span>
															<label id="user_image-error" class="error" style="display:none;" for="user_image">Please select the image</label>
														</div>
													</div>

													<div class="col-lg-8 pl-0 pr-0">
														<div class="form-group row">
														<label class="col-xl-12 col-lg-12 col-form-label">First name</label>
														<div class="col-lg-12 col-xl-12">
															<input class="form-control form-control-lg form-control-solid" name="user_fname" type="text" value="{{ isset($scoutData) ? $scoutData->user_fname : '' }}" placeholder="Enter Host's first name">
														</div>
													</div>
													<div class="form-group row">
														<label class="col-xl-12 col-lg-12 col-form-label">Last name</label>
														<div class="col-lg-12 col-xl-12">
															<input class="form-control form-control-lg form-control-solid" name="user_lname" type="text" value="{{ isset($scoutData) ? $scoutData->user_lname : '' }}" placeholder="Enter Host's last name">
														</div>
													</div>
													</div>

													</div>





													
													<!-- <div class="form-group row">
														<label class="col-xl-3 col-lg-3 col-form-label">Company Name</label>
														<div class="col-lg-9 col-xl-6">
															<input class="form-control form-control-lg form-control-solid" type="text" value="Loop Inc.">
															<span class="form-text text-muted">If you want your invoices addressed to a company. Leave blank to use your full name.</span>
														</div>
													</div> -->
													<div class="row">
														<div class="col-lg-12 col-xl-12">
															<h5 class="font-weight-bold mt-10 heading-scout-form">Contact Info</h5>
														</div>
													</div>
													<div class="form-group row contact-scout-form">
														<label class="col-xl-3 col-lg-3 col-form-label">Contact Phone</label>
														<div class="col-lg-9 col-xl-6">
															<div class="input-group input-group-lg input-group-solid">
																<!--div class="input-group-prepend">
																	<span class="input-group-text">
																		<i class="la la-phone"></i>
																	</span>
																</div-->

																<select name="user_mobile_code" class="country-code-select selectpicker profileCountryCode" data-size="5" data-live-search="true">
																	@forelse($getCountryCode as $CountryCode)
																	@php 
																		$selected_countryCode = '';
																		if(!empty($getCountryCode) && $CountryCode->phonecode==$scoutData->user_mobile_code){
																		$selected_countryCode = 'selected';
																	}
																	@endphp
							                                        <option value="{{ $CountryCode->phonecode }}" {{$selected_countryCode}}>+{{ $CountryCode->phonecode.' '.$CountryCode->iso }}</option>
							                                        @empty
							                                        @endforelse
																</select>
																<input type="text" class="form-control form-control-lg form-control-solid user_mobileMasking" value="{{ isset($scoutData) ? $scoutData->user_mobile : '' }}" id="user_mobile" name="user_mobile" placeholder="Enter Host's mobile number">
															</div>
															<span class="form-text text-muted">We'll never share your email with anyone else.</span>
														</div>
													</div>
													<div class="form-group row contact-scout-form">
														<label class="col-xl-3 col-lg-3 col-form-label">Email Address</label>
														<div class="col-lg-9 col-xl-6">
															<div class="input-group input-group-lg input-group-solid">
																<div class="input-group-prepend">
																	<span class="input-group-text">
																		<i class="la la-at"></i>
																	</span>
																</div>
																<input type="email" name="email" {{ isset($scoutData) ? "readonly" : "" }} value="{{ isset($scoutData) ? $scoutData->email : '' }}" class="form-control form-control-lg form-control-solid {{ isset($scoutData) ? 'read-only' : '' }}"  placeholder="Email">
															</div>
														</div>
													</div>
													 <div class="form-group row contact-scout-form">
														<label class="col-xl-3 col-lg-3 col-form-label">Address</label>
														<div class="col-lg-9 col-xl-6">
															<div class="input-group input-group-lg input-group-solid">
																<input type="text" name="user_address" class="form-control form-control-lg form-control-solid" value="{{ isset($scoutData) ? $scoutData->user_address : '' }}" placeholder="Enter Host's address" value="loop">
																
															</div>
														</div>
													</div> 


													<!-- Addtional Information section start from here  -->
													<div class="row">
														<div class="col-lg-12 col-xl-12">
															<h5 class="font-weight-bold mt-10 heading-scout-form">Addtional Information</h5>
														</div>
													</div>
													<div class="form-group my-error new_status value_inner english_level">
														<label>Full and official name of the Company/Organisation*</label>
														<div class="d-flex flex-wrap">
														
													<div class="inner_form col-xl-12 col-lg-12 col-sm-12 p-0">
														<input type="text" name="company_name" value="{{ $company_na }}" class="form-control form-control-lg form-control-solid" placeholder="Enter Host's Company Name"/>
													</div>
													
												</div>
											</div>
											<div class="form-group my-error new_status value_inner english_level">
														<label>Company/Organisation registration number*</label>
														<div class="d-flex flex-wrap">
														
													<div class="inner_form col-xl-12 col-lg-12 col-sm-12 p-0">
														<input type="text" name="company_registration_no" value="{{ $company_reg_no }}"  placeholder="Enter Host's Company Registration Number" class="form-control form-control-lg form-control-solid" />
													</div>
													
												</div>
											</div>
											<div class="form-group my-error new_status value_inner english_level">
														<label>Company/Organisation website (insert a link if any)</label>
														<div class="d-flex flex-wrap">
														
													<div class="inner_form col-xl-12 col-lg-12 col-sm-12 p-0">
														<input type="text" name="company_web_link"  value="{{ $company_link }}"  placeholder="Enter Host's Company Website Link" class="form-control form-control-lg form-control-solid" />
													</div>
													
												</div>
											</div>
											<div class="form-group row scout-profile-lang">
												<div class="col-lg-12 col-xl-6 mb-lg-5 pb-lg-2 mb-md-5 pb-md-2">
													<label class="min-height-add">What is your primary language? <span class="cursor-pointer i-color-chnge" data-action="change" data-toggle="tooltip" title="" data-original-title="You should be able to read, write, and speak in this language."><i class="fas fa-question-circle"></i></span></label>
													<select name="user_primary_language" id="primary_language" class="form-control form-control-lg form-control-solid valid" placeholder="Select Type" >
														@php 
														$get_primary_language = '';
														if(isset($scoutData) && !empty($scoutData->user_primary_language)){
														$get_primary_language = $scoutData->user_primary_language;
													}
													@endphp
													@if(isset($languages) && !empty($languages))
													@foreach($languages as $language)
													@php
													$selected_language = '';
													if(!empty($get_primary_language) && $language->id==$get_primary_language){
													$selected_language = 'selected';
												}
												@endphp
												<option value="{{ $language->id }}" {{ $selected_language }}>{{ $language->name }}</option> 
												@endforeach
												@endif
											</select>
										</div>
												<div class="col-lg-12 col-xl-6 mb-lg-5 pb-lg-2 mb-md-5 pb-md-2">
													<label class="min-height-add">What other languages do you speak fluently? <span class="cursor-pointer i-color-chnge" data-action="change" data-toggle="tooltip" title="" data-original-title="You should be able to read, write, and speak in this language."><i class="fas fa-question-circle"></i></span></label>
													<select name="user_other_languages[]" id="primary_language" class="form-control form-control-solid h-auto py-6 px-6 rounded-sm font-size-h6 exp_multi" placeholder="Select Type"  multiple="multiple">
													@php 
                                                $get_other_language = [];
                                                if(isset($scoutData) && !empty($scoutData->user_other_languages)){
                                                    $get_other_language = explode(",",$scoutData->user_other_languages);
                                                }
                                            @endphp 
                                            @if(isset($languages) && !empty($languages))                         
                                            @foreach($languages as $language)
                                            @php
                                                $select_language = '';
                                                if(in_array($language->id,$get_other_language)){
                                                    $select_language = 'selected';
                                                }
                                            @endphp
												<option value="{{ $language->id }}" {{ $select_language }}>{{ $language->name }}</option> 
                                            @endforeach
                                            @endif
											</select>
										</div>
									
							</div>
							<div class="form-group row scout-profile-lang">
							<div class="col-lg-12 col-xl-12 mb-lg-5 pb-lg-2 mb-md-5 pb-md-2">
								 <label>Tick the options below, if this host meets the listed legal requirements for
                                    conducting an experience. Note that in some cases the host must meet these requirements (food preparation and serving, tourist guide license, etc.) - *</label>
                                     @php 
                                            $legal_haccp = '';
                                            $legal_guid = '';
                                            $legal_oth = '';
                                            $legal_arr = [];

                                            if(!empty($legal_req)){
                                                $legal_arr = explode(",",$legal_req);
                                                if(in_array('1',$legal_arr)){
                                                    $legal_haccp = 'checked';
                                                }
                                                if (in_array('2',$legal_arr)) {
                                                    $legal_guid = 'checked';
                                                }
                                                if (in_array('3', $legal_arr)) {
                                                    $legal_oth = 'checked';
                                                }
                                            }
                                        @endphp 
                                         <div class="inner_form col-md-12 col-sm-12 p-0 haccp-input">
                                            <input type="checkbox" value=1  name="legal_requirements[]" {{ $legal_haccp }}/><label>HACCP - Hazard analysis and critical control points</label>
                                        </div>
                                        <div class="inner_form col-md-12 col-sm-12 p-0 haccp-input">
                                            <input type="checkbox" value=2 name="legal_requirements[]" id="legal_guiding" {{ $legal_guid}}/><label>Tourist Guide License</label>
                                        </div>
                                        <div class="inner_form col-md-12 col-sm-12 p-0 haccp-input">
                                            <input type="checkbox" value=3 name="legal_requirements[]" id="legal_other" {{ $legal_oth}}/><label>Other</label>
                                        </div>
                                        <label id="legal_requirements[]-error" class="error" for="legal_requirements[]" style="display: none">Please select a checkbox</label>
							</div>
						</div>
						<div class="form-group my-error new_status value_inner " id="guiding_txt" style="{{ $legal_guid ? '' : 'display: none;' }}">
														<label>Tourist Guide License</label>
														<div class="d-flex flex-wrap">
														
													<div class="inner_form col-xl-6 col-lg-6 col-sm-6 p-0">
														<input type="text" name="guiding_text" value="{{ $guid_txt }}" class="form-control form-control-lg form-control-solid" placeholder="Enter Guiding License" id="guid_input"/>
													</div>
													
												</div>
											</div>
											<div class="form-group my-error new_status value_inner" id="other_txt" style="{{ $legal_oth ? '' : 'display: none;' }}">
														 <label>Other</label>
														<div class="d-flex flex-wrap">
														
													<div class="inner_form col-xl-6 col-lg-6 col-sm-6 p-0">
														<input type="text" name="other_text" value="{{ $other_txt }}" class="form-control form-control-lg form-control-solid" placeholder="Enter Other" id="othr_input"/>
													</div>
													
												</div>
											</div>
							
							
													 
													<!-- Addtional Information section end here  -->

												</div>
												<!--end::Body-->
											</form>
											<!--end::Form-->
										</div>
									</div>
									<!--end::Content-->
								</div>
		  
        </div>
        <!-- /.modal -->
	 
	    <!-- /.modal -->

@endsection
{{-- Scripts Section --}}
@section('scripts')
<script type="text/javascript">
	 $("select[multiple='multiple']").bsMultiSelect({
      placeholder    : 'Add other languages',
    });

    $.ajaxSetup({headers: {'X-XSRF-TOKEN': decodeURIComponent(/XSRF-Token=([^;]*)/ig.exec(document.cookie)[1])}});
    $(document).ready(function () {
    	var user = '<?php echo (!empty($scoutData->user_image))? $scoutData->user_image :''; ?>';
         $.validator.addMethod('filesize', function (value, element, param) {
                return this.optional(element) || (element.files[0].size <= param)
            }, 'File size must be less than {0}');

            $.validator.addMethod("alphanumeric", function(value, element) {
                return this.optional(element) || /^[A-Za-z0-9]+$/.test(value);
            }, "Please enter Letters and numbers only");

            var form = $('#add-host-form');
            form.validate({
                submitHandler: function(form) {
                  // do other things for a valid form
                  form.submit();
                },
                rules: {
                    user_fname: {
                        required: true
                    },
                    user_lname: {
                        required: true,
                    },
                   email:{
                    required: true,
                },
                    user_address:{
                        required: true
                    },
                    company_name:{
                        required:true
                    },
                    company_registration_no:{
                        required:true
                    },
                    'user_other_languages[]':{
                        required:true
                    },
                    'legal_requirements[]':{
                        required:true
                    },
                    guiding_text:{
                        required: function(){
                            if($("#legal_guiding").is(':checked')){
                                return true;
                            }else{
                                return false;
                            }
                        },
                    },
                    other_text:{
                        required: function(){
                            if($("#legal_other").is(':checked')){
                                return true;
                            }else{
                                return false;
                            }
                        },
                    },
                    user_mobile:{
                    required: true,
                    //digits: true,
                    // maxlength: 10,
                    //minlength: 10,
                    remote: {
                        url: HOST_URL+"/check-user-mobile-existence",
                        type: "post"
                    }
                },
                    user_image:{
                    required: function(){
                        if(user!=''){
                            return false;
                        }else{
                            return true;
                        }
                    },
                    extension: "jpg,jpeg,png",
                    filesize: 1*1024*1024,
                },
                },
                messages: {
                    user_fname: {
                        required: "Please enter your first name",
                    },  
                    user_lname: {                 
                        required : "Please enter your last name",
                    },
                    user_address: {
                        required : "Please enter your address",
                    },
                    email: {
                        required : "Please enter your email address",
                    },
                    company_name: {
                        required : "Please enter your company name",
                    },
                    company_registration_no: {
                        required : "Please enter your comany registration number",
                    },
                    'user_other_languages[]': {
                        required : "Please select a language",
                    },
                    'legal_requirements[]': {
                        required : "Please select a checkbox",
                    },
                    user_mobile: {
                        required : "Please enter your mobile number",
                        remote: "This mobile number already exist"
                    },
                    user_image:{
                        required: "Please select the image",
                        extension: "Please select jpg,jpeg and png image",
                        filesize: "File size must be less than 2MB",
                    },
                },
                ignore: ':hidden:not(".exp_multi")',          
              
            });
         $("#save-profile-button").click(function(){
          if($("#add-host-form").valid())
          {
          	let myForm = document.getElementById('add-host-form');
          	 myForm.submit();
          }
        });
           $('#legal_guiding').on('click',function(){

            var checked = $(this).is(':checked');
            var txt_selector = $('#guiding_txt');

            $('#guid_input').val('');

            if(checked){
                $(txt_selector).show();
            }
            else{
                $(txt_selector).hide();
            }
        });

        $('#legal_other').on('click',function(){

            var checked = $(this).is(':checked');
            var txt_selector = $('#other_txt');

            $('#othr_input').val('');

            if(checked){
                $(txt_selector).show();
            }
            else{
                $(txt_selector).hide();
            }
        });
    });
</script>
@endsection