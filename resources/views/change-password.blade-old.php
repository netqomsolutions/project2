{{-- Extends layout --}}
@extends('layout.default')
{{-- Content --}}
@section('content')
<style type="text/css">
    .read-only{
        background-color: '#dedada' !important;
    }
</style>
<div class="row">
     @if(session()->has('success'))
            <div class="alert alert-success alluser-page-suc" style="text-align: left;float: left;width: 100%;">
                {{ session()->get('success') }}
            </div>
            <script type="text/javascript">setTimeout(function(){ $('.alluser-page-suc').fadeOut(); }, 3000);</script>
        @elseif(session()->has('failure'))
            <div class="alert alert-danger alluser-page-suc" style="text-align: left; left;float: left;width: 100%;">
                {{ session()->get('failure') }}
            </div>
            <script type="text/javascript">setTimeout(function(){ $('.alluser-page-suc').fadeOut(); }, 3000);</script>
        @endif
    <div class="col-lg-12">
        <div class="card card-custom gutter-b example example-compact">
            <div class="card-header">
                <h3 class="card-title">Change Password</h3>
                <div class="card-toolbar" style="display: none;">
                    <div class="example-tools justify-content-center">
                        <span class="example-toggle" data-toggle="tooltip" title="View code"></span>
                        <span class="example-copy" data-toggle="tooltip" title="Copy code"></span>
                    </div>
                </div>
            </div>
            <!--begin::Form-->
            <form id="change-password-form" class="form" method="post" action="{{ route('user-change-password') }}" enctype="multipart/form-data">
                @csrf
                <div class="card-body">
                    <div class="form-group row">
                        <input type="hidden" name="user_id" value="{{@$userData->id}}">
                        <div class="col-lg-6 my-error">
                            <label for="exampleInputName1">Current Password</label>
                        <input id="current-password" type="password" class="form-control" name="current_password">
                        </div>
                        <div class="col-lg-6 my-error new-pass-space">
                             <label for="exampleInputName1">New Password</label>
                        <input id="new-password" type="password" class="form-control" name="password">
                        </div>
                        <div class="col-lg-6 my-error mt-5">
                           <div class="form-group">
                        <label for="exampleInputEmail1">Confirm New Password</label>
                        <input id="new-password-confirm" type="password" class="form-control" name="confirm_password">
                    </div>
                        </div>
                    </div>
                </div>
                <div class="card-footer">
                    <input type="hidden" name="created_by" value="{{ Auth::user()->id }}">
                    <div class="row">
                        <div class="col-lg-6">
                            <button type="submit" class="btn btn-primary mr-2">Save</button>
                            <a href="{{ route('scout-host-list') }}" class="btn btn-secondary">Back</a>
                        </div>
                    </div>
                </div>
            </form>
            <!--end::Form-->
        </div>
    </div>
</div>
@endsection
{{-- Scripts Section --}}
@section('scripts')
<script type="text/javascript">
    $.ajaxSetup({headers: {'X-XSRF-TOKEN': decodeURIComponent(/XSRF-Token=([^;]*)/ig.exec(document.cookie)[1])}});
    $(document).ready(function () {

        $.validator.addMethod('filesize', function (value, element, param) {
            return this.optional(element) || (element.files[0].size <= param)
        }, 'File size must be less than {0}');
        $('#change-password-form').validate({
            submitHandler: function(form) {
              form.submit();
            },
            rules: {
                current_password: {
                    required: true,
                    remote: {
                        url: HOST_URL+"/check-old-password",
                        type: "post"
                    }
                },
                password: {
                    required: true,
                    minlength:true,
                },
                confirm_password:{
                    required: true,
                    equalTo : "#new-password"
                },
            },
            messages: {
                current_password: {
                    required: "Please enter your current password",
                    remote: "Your current password does not matches with the password you provided."
                },  
                password: {                 
                    required : "Please enter your new password",
                },
                confirm_password: {
                    required : "The password confirmation is required",
                    equalTo:"The password and its confirm are not the same",
                },
                
            },
            errorElement: 'span',           
            errorPlacement: function (error, element) {
              error.addClass('invalid-feedback');
              element.closest('.my-error').append(error);
            },
            highlight: function (element, errorClass, validClass) {
              $(element).addClass('is-invalid');
            },
            unhighlight: function (element, errorClass, validClass) {
              $(element).removeClass('is-invalid');
            }
        });
    });
</script>
@endsection