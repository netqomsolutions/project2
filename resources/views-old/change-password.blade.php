{{-- Extends layout --}}
@extends('layout.default')
{{-- Content --}}
@section('content')
<style>
	.toggle.ios, .toggle-on.ios, .toggle-off.ios { border-radius: 20rem; }
	.toggle.ios .toggle-handle { border-radius: 20rem; }
	div#action-scout {
	    align-items: center;
	    justify-content: center;
	}
</style>
 @if(session()->has('success'))
            <div class="alert alert-success alluser-page-suc" style="text-align: left;float: left;width: 100%;">
                {{ session()->get('success') }}
            </div>
            <script type="text/javascript">setTimeout(function(){ $('.alluser-page-suc').fadeOut(); }, 3000);</script>
        @elseif(session()->has('failure'))
            <div class="alert alert-danger alluser-page-suc" style="text-align: left; left;float: left;width: 100%;">
                {{ session()->get('failure') }}
            </div>
            <script type="text/javascript">setTimeout(function(){ $('.alluser-page-suc').fadeOut(); }, 3000);</script>
        @endif
    <div class="main-body">
          <!--  <div class="user-profile-buttons">
            <a href="{{ route('user-profile-edit') }}" class="navi-item">Update Profile</a>
            <a href="{{ route('user-change-password-form') }}" class="navi-item">Change Password</a>
          </div> -->
		  
		  
	<div class="d-flex flex-row w-100">
		@include('profile-sidebar')	
        <!--begin::Content-->
		<div class="flex-row-fluid ml-lg-8">
			<!--begin::Card-->
			<div class="card card-custom card-sticky card-stretch" id="kt_page_sticky_card">
				<!--begin::Header-->
				<div class="card-header py-3">
					<div class="card-title align-items-start flex-column">
						<h3 class="card-label font-weight-bolder text-dark">Change Password</h3>
						<span class="text-muted font-weight-bold font-size-sm mt-1">Change your account password</span>
					</div>
					<div class="card-toolbar">
						<button type="button" id="save-changes-button" class="btn btn-success mr-2 green-bg-btn">Save Changes</button>
						<!--button type="reset" class="btn btn-secondary">Cancel</button-->
					</div>
				</div>
				<!--end::Header-->
				<!--begin::Form-->
				<form class="form" id="change-password-form" method="post" action="{{ route('user-change-password') }}" enctype="multipart/form-data">
					 @csrf
					<div class="card-body">
						<!--begin::Alert-->
						<!-- <div class="alert alert-custom alert-light-danger fade show mb-10" role="alert">
							<div class="alert-icon">
								<span class="svg-icon svg-icon-3x svg-icon-danger">
									<!--begin::Svg Icon | path:assets/media/svg/icons/Code/Info-circle.svg->
									<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
										<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
											<rect x="0" y="0" width="24" height="24" />
											<circle fill="#000000" opacity="0.3" cx="12" cy="12" r="10" />
											<rect fill="#000000" x="11" y="10" width="2" height="7" rx="1" />
											<rect fill="#000000" x="11" y="7" width="2" height="2" rx="1" />
										</g>
									</svg>
									<!--end::Svg Icon->
								</span>
							</div>
							<div class="alert-text font-weight-bold">Configure user passwords to expire periodically. Users will need warning that their passwords are going to expire,
							<br />or they might inadvertently get locked out of the system!</div>
							<div class="alert-close">
								<button type="button" class="close" data-dismiss="alert" aria-label="Close">
									<span aria-hidden="true">
										<i class="ki ki-close"></i>
									</span>
								</button>
							</div>
						</div> -->
						<!--end::Alert-->
						<div class="form-group row">
							<label class="col-xl-3 col-lg-3 col-form-label text-alert">Current Password</label>
							<div class="col-lg-9 col-xl-6">
								<input type="password" class="form-control form-control-lg form-control-solid mb-2" id="current-password" name="current_password" value="" placeholder="Current password" />
								<!--a href="#" class="text-sm font-weight-bold">Forgot password ?</a-->
							</div>
						</div>
						<div class="form-group row">
							<label class="col-xl-3 col-lg-3 col-form-label text-alert">New Password</label>
							<div class="col-lg-9 col-xl-6">
								<input type="password" class="form-control form-control-lg form-control-solid" id="new-password"  name="password" value="" placeholder="New password" />
							</div>
						</div>
						<div class="form-group row">
							<label class="col-xl-3 col-lg-3 col-form-label text-alert">Verify Password</label>
							<div class="col-lg-9 col-xl-6">
								<input type="password" class="form-control form-control-lg form-control-solid" id="new-password-confirm" value="" placeholder="Verify password" name="confirm_password" />
							</div>
						</div>
					</div>
				</form>
				<!--end::Form-->
			</div>
		</div>
									<!--end::Content-->
	  </div>
	  @endsection
{{-- Scripts Section --}}
@section('scripts')
<script type="text/javascript">
    $.ajaxSetup({headers: {'X-XSRF-TOKEN': decodeURIComponent(/XSRF-Token=([^;]*)/ig.exec(document.cookie)[1])}});
    $(document).ready(function () {

        $.validator.addMethod('filesize', function (value, element, param) {
            return this.optional(element) || (element.files[0].size <= param)
        }, 'File size must be less than {0}');
        $('#change-password-form').validate({
            rules: {
                current_password: {
                    required: true,
                    remote: {
                        url: HOST_URL+"/check-old-password",
                        type: "post"
                    }
                },
                password: {
                    required: true,
                    minlength:8,
                },
                confirm_password:{
                    required: true,
                    equalTo : "#new-password"
                },
            },
            messages: {
                current_password: {
                    required: "Please enter your current password",
                    remote: "Your current password does not matches with the password you provided."
                },  
                password: {                 
                    required : "Please enter your new password",
                },
                confirm_password: {
                    required : "The password confirmation is required",
                    equalTo:"The password and its confirm are not the same",
                },
                
            }
        });
        $("#save-changes-button").click(function(){
          if($("#change-password-form").valid())
          {
          	let myForm = document.getElementById('change-password-form');
          	 myForm.submit();
          }
        });
    });
</script>
@endsection