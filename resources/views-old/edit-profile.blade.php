{{-- Extends layout --}}
@extends('layout.default')
{{-- Content --}}
@section('content')
<style type="text/css">
    .read-only{
        background-color: '#dedada' !important;
    }
</style>
<div class="row">
     @if(session()->has('success'))
            <div class="alert alert-success alluser-page-suc" style="text-align: left;float: left;width: 100%;">
                {{ session()->get('success') }}
            </div>
            <script type="text/javascript">setTimeout(function(){ $('.alluser-page-suc').fadeOut(); }, 3000);</script>
        @elseif(session()->has('failure'))
            <div class="alert alert-danger alluser-page-suc" style="text-align: left; left;float: left;width: 100%;">
                {{ session()->get('failure') }}
            </div>
            <script type="text/javascript">setTimeout(function(){ $('.alluser-page-suc').fadeOut(); }, 3000);</script>
        @endif
    <div class="col-lg-12">
        <div class="card card-custom gutter-b example example-compact">
            <div class="card-header">
                <h3 class="card-title">Update Profile</h3>
                <div class="card-toolbar" style="display: none;">
                    <div class="example-tools justify-content-center">
                        <span class="example-toggle" data-toggle="tooltip" title="View code"></span>
                        <span class="example-copy" data-toggle="tooltip" title="Copy code"></span>
                    </div>
                </div>
            </div>
            <!--begin::Form-->
            <form id="edit-scout-form" class="form" method="post" action="{{ route('profile-update') }}" enctype="multipart/form-data">
                @csrf
                <div class="card-body">
                    <div class="form-group row">
                        <input type="hidden" name="user_id" value="{{@$userData->id}}">
                        <div class="col-lg-4 my-error">
                            <label>First Name:</label>
                            <input type="text" name="user_fname" value="{{ isset($userData) ? $userData->user_fname : '' }}" class="form-control" placeholder="Enter first name" />
                        </div>
                        <div class="col-lg-4 my-error">
                            <label>Last Name:</label>
                            <input type="text" name="user_lname" value="{{ isset($userData) ? $userData->user_lname : '' }}" class="form-control" placeholder="Enter last name" />
                        </div>
                        <div class="col-lg-4 my-error">
                            <label>Email:</label>
                            <input type="email" name="email" {{ isset($userData) ? "readonly" : "" }} value="{{ isset($userData) ? $userData->email : '' }}" class="form-control {{ isset($userData) ? 'read-only' : '' }}" placeholder="Enter your email address" />
                        </div>
                    </div>
                    <div class="form-group row">                        
                        <div class="col-lg-4 my-error">
                            <label>Address:</label>
                            <input type="text" name="user_address" value="{{ isset($userData) ? $userData->user_address : '' }}" class="form-control" placeholder="Enter your address" />
                        </div>
                        <div class="col-lg-4 my-error">
                            <label>Mobile:</label>
                            <input type="tel" id="user_mobile" name="user_mobile" value="{{ isset($userData) ? $userData->user_mobile : '' }}" class="form-control" placeholder="Enter your mobile number" />
                        </div>
                        <div class="col-lg-4 my-error">
                            <label>Image:</label>
                            <input type="file" name="user_image" value="" class="form-control" placeholder="Browse Image" />
                            <input type="hidden" name="old_user_img" id="old_user_img" value="{{ isset($userData) ? $userData->user_image : '' }}">
                            
                        </div>


                    </div>

                   <!--  <div class="form-group row">                        
                        <div class="col-lg-4 my-error">
                            <label>Why would you join us? :</label>
                            <textarea name="inner_textarea error" class="inner_textarea error">{{ isset($userData) ? $userData->user_address : '' }}</textarea>
                             
                        </div>
                        <div class="col-lg-4 my-error">
                            <label>Briefly describe two unique experiences in your region that first came to your mind as the right "#LFZ scouting material". *:</label>

                            <textarea name="unique_experiences" id="unique_experiences" class="inner_textarea error"></textarea>
                            
                        </div>
                        <div class="col-lg-4 my-error">
                            <label>What are your personal and/or professional areas of interest? *:</label>
                            <textarea name="user_interest" id="user_interest" class="inner_textarea error"></textarea>
                            
                            
                        </div>

                         
                    </div> -->
 <!--  <div class="form-group row">
                        <p class="form_group_text">What is your English level? *</p>
                    <div class="d-flex flex-wrap">
                                <div class="inner_form col-md-6 col-sm-12 p-0">
                                    <input type="radio" id="radio56" value="A1 Beginner" name="user_english_level">
                                    <span></span>
                                    <label for="radio01-01">A1 Beginner</label>
                                </div>
                                <div class="inner_form col-md-6 col-sm-12 p-0">
                                    <input type="radio" id="radio57" value="A2 Elementary English" name="user_english_level">
                                    <span></span>
                                    <label for="radio01-02">A2 Elementary English</label>
                                </div>
                                <div class="inner_form col-md-6 col-sm-12 p-0">
                                    <input type="radio" id="radio58" value="B1 Intermediate English" name="user_english_level">
                                    <span></span>
                                    <label for="radio01-03">B1 Intermediate English</label>
                                </div>
                                <div class="inner_form col-md-6 col-sm-12 p-0">
                                    <input type="radio" id="radio59" value="B2 Upper-Intermediate English" name="user_english_level">
                                    <span></span>
                                    <label for="radio01-03">B2 Upper-Intermediate English</label>
                                </div>
                                <div class="inner_form col-md-6 col-sm-12 p-0">
                                    <input type="radio" id="radio60" value="C1 Advanced English" name="user_english_level">
                                    <span></span>
                                    <label for="radio01-03">C1 Advanced English</label>
                                </div>
                                <div class="inner_form col-md-6 col-sm-12 p-0">
                                    <input type="radio" id="radio61" value="C2 Proficiency English" name="user_english_level">
                                    <span></span>
                                    <label for="radio01-03">C2 Proficiency English</label>
                                </div>
                                <div class="inner_form col-md-6 col-sm-12 p-0">
                                    <input type="radio" id="radio62" value="Other" name="user_english_level">
                                    <span></span>
                                    <label for="radio01-03">Other:</label>
                                </div>
                                <label for="user_english_level" class="error" style="display:none"></label>
                            </div>
                            </div> -->

  
               <!--  <div class="form-group word_sec">
                                <label>What is your primary language? <span>You should be able to read, write, and speak in this language.</span></label>
                                <select name="user_primary_language" id="primary_language" class="form-control form-control-solid h-auto py-6 px-6 rounded-sm font-size-h6 " placeholder="Select Type">
                                    <option value=" " selected="">--Select--</option>

                                                                        <option value="en">English</option> 
                                                                        <option value="de">German</option> 
                                                                        <option value="fr">French</option> 
                                                                        <option value="nl">Dutch</option> 
                                                                        <option value="it">Italian</option> 
                                                                        <option value="es">Spanish</option> 
                                                                        <option value="pl">Polish</option> 
                                                                        <option value="ru">Russian</option> 
                                                                        <option value="ja">Japanese</option> 
                                                                        <option value="pt">Portuguese</option> 
                                                                        <option value="sv">Swedish</option> 
                                                                        <option value="zh">Chinese</option> 
                                                                        <option value="ca">Catalan</option> 
                                                                        <option value="uk">Ukrainian</option> 
                                                                        <option value="no">Norwegian (BokmÃ¥l)</option> 
                                                                        <option value="fi">Finnish</option> 
                                                                        <option value="vi">Vietnamese</option> 
                                                                        <option value="cs">Czech</option> 
                                                                        <option value="hu">Hungarian</option> 
                                                                        <option value="ko">Korean</option> 
                                                                        <option value="id">Indonesian</option> 
                                                                        <option value="tr">Turkish</option> 
                                                                        <option value="ro">Romanian</option> 
                                                                        <option value="fa">Persian</option> 
                                                                        <option value="ar">Arabic</option> 
                                                                        <option value="da">Danish</option> 
                                                                        <option value="eo">Esperanto</option> 
                                                                        <option value="sr">Serbian</option> 
                                                                        <option value="lt">Lithuanian</option> 
                                                                        <option value="sk">Slovak</option> 
                                                                        <option value="ms">Malay</option> 
                                                                        <option value="he">Hebrew</option> 
                                                                        <option value="bg">Bulgarian</option> 
                                                                        <option value="sl">Slovenian</option> 
                                                                        <option value="vo">VolapÃ¼k</option> 
                                                                        <option value="kk">Kazakh</option> 
                                                                        <option value="war">Waray-Waray</option> 
                                                                        <option value="eu">Basque</option> 
                                                                        <option value="hr">Croatian</option> 
                                                                        <option value="hi">Hindi</option> 
                                                                        <option value="et">Estonian</option> 
                                                                        <option value="az">Azerbaijani</option> 
                                                                        <option value="gl">Galician</option> 
                                                                        <option value="simple">Simple English</option> 
                                                                        <option value="nn">Norwegian (Nynorsk)</option> 
                                                                        <option value="th">Thai</option> 
                                                                        <option value="new">Newar / Nepal Bhasa</option> 
                                                                        <option value="el">Greek</option> 
                                                                        <option value="roa-rup">Aromanian</option> 
                                                                        <option value="la">Latin</option> 
                                                                        <option value="oc">Occitan</option> 
                                                                        <option value="tl">Tagalog</option> 
                                                                        <option value="ht">Haitian</option> 
                                                                        <option value="mk">Macedonian</option> 
                                                                        <option value="ka">Georgian</option> 
                                                                        <option value="sh">Serbo-Croatian</option> 
                                                                        <option value="te">Telugu</option> 
                                                                        <option value="pms">Piedmontese</option> 
                                                                        <option value="ceb">Cebuano</option> 
                                                                        <option value="ta">Tamil</option> 
                                                                        <option value="be-x-old">Belarusian (TaraÅ¡kievica)</option> 
                                                                        <option value="br">Breton</option> 
                                                                        <option value="lv">Latvian</option> 
                                                                        <option value="jv">Javanese</option> 
                                                                        <option value="sq">Albanian</option> 
                                                                        <option value="be">Belarusian</option> 
                                                                        <option value="mr">Marathi</option> 
                                                                        <option value="cy">Welsh</option> 
                                                                        <option value="lb">Luxembourgish</option> 
                                                                        <option value="is">Icelandic</option> 
                                                                        <option value="bs">Bosnian</option> 
                                                                        <option value="yo">Yoruba</option> 
                                                                        <option value="mg">Malagasy</option> 
                                                                        <option value="an">Aragonese</option> 
                                                                        <option value="bpy">Bishnupriya Manipuri</option> 
                                                                        <option value="lmo">Lombard</option> 
                                                                        <option value="fy">West Frisian</option> 
                                                                        <option value="bn">Bengali</option> 
                                                                        <option value="io">Ido</option> 
                                                                        <option value="sw">Swahili</option> 
                                                                        <option value="gu">Gujarati</option> 
                                                                        <option value="ml">Malayalam</option> 
                                                                        <option value="pnb">Western Panjabi</option> 
                                                                        <option value="af">Afrikaans</option> 
                                                                        <option value="nds">Low Saxon</option> 
                                                                        <option value="scn">Sicilian</option> 
                                                                        <option value="ur">Urdu</option> 
                                                                        <option value="ku">Kurdish</option> 
                                                                        <option value="zh-yue">Cantonese</option> 
                                                                        <option value="hy">Armenian</option> 
                                                                        <option value="qu">Quechua</option> 
                                                                        <option value="su">Sundanese</option> 
                                                                        <option value="ne">Nepali</option> 
                                                                        <option value="diq">Zazaki</option> 
                                                                        <option value="ast">Asturian</option> 
                                                                        <option value="tt">Tatar</option> 
                                                                        <option value="nap">Neapolitan</option> 
                                                                        <option value="ga">Irish</option> 
                                                                        <option value="cv">Chuvash</option> 
                                                                        <option value="bat-smg">Samogitian</option> 
                                                                        <option value="wa">Walloon</option> 
                                                                        <option value="am">Amharic</option> 
                                                                        <option value="kn">Kannada</option> 
                                                                        <option value="als">Alemannic</option> 
                                                                        <option value="bug">Buginese</option> 
                                                                        <option value="my">Burmese</option> 
                                                                        <option value="ia">Interlingua</option> 
                                                                    </select>
                            </ div>-->
                <div class="card-footer">
                    <input type="hidden" name="created_by" value="{{ Auth::user()->id }}">
                    <div class="row">
                        <div class="col-lg-6">
                            <button type="submit" class="btn btn-primary mr-2">Save</button>
                            <a href="{{ route('scout-host-list') }}" class="btn btn-secondary">Back</a>
                        </div>
                    </div>
                </div>
            </form>
            <!--end::Form-->
        </div>
    </div>
</div>
@endsection
{{-- Scripts Section --}}
@section('scripts')
<script type="text/javascript">
    $.ajaxSetup({headers: {'X-XSRF-TOKEN': decodeURIComponent(/XSRF-Token=([^;]*)/ig.exec(document.cookie)[1])}});
    $(document).ready(function () {

        $.validator.addMethod('filesize', function (value, element, param) {
            return this.optional(element) || (element.files[0].size <= param)
        }, 'File size must be less than {0}');
        $('#edit-scout-form').validate({
            submitHandler: function(form) {
              // do other things for a valid form
              form.submit();
            },
            rules: {
                user_fname: {
                    required: true
                },
                user_lname: {
                    required: true,
                },
                email:{
                    required: true,
                },
                user_address:{
                    required: true
                },
                user_mobile:{
                    required: true,
                    digits: true,
                    // maxlength: 10,
                    minlength: 10,
                    remote: {
                        url: HOST_URL+"/check-user-mobile-existence",
                        type: "post"
                    }
                },
                user_image:{
                    required: function(){
                        if($("#old_user_img").val()){
                            return false;
                        }else{
                            return true;
                        }
                    },
                    extension: "jpg,jpeg,png",
                    filesize: 2*1024*1024,
                },
            },
            messages: {
                user_fname: {
                    required: "Please enter your first name",
                },  
                user_lname: {                 
                    required : "Please enter your last name",
                },
                user_address: {
                    required : "Please enter your address",
                },
                email: {
                    required : "Please enter your email address",
                },
                user_mobile: {
                    required : "Please enter your mobile number",
                    remote: "This mobile number already exist"
                },
                user_image:{
                    required: "Please select the image",
                    extension: "Please select jpg,jpeg and png image",
                    filesize: "File size must be less than 2MB",
                },
            },
            errorElement: 'span',           
            errorPlacement: function (error, element) {
              error.addClass('invalid-feedback');
              element.closest('.my-error').append(error);
            },
            highlight: function (element, errorClass, validClass) {
              $(element).addClass('is-invalid');
            },
            unhighlight: function (element, errorClass, validClass) {
              $(element).removeClass('is-invalid');
            }
        });
    });
</script>
@endsection