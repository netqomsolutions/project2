<!doctype html>
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
  <head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <meta name="format-detection" content="telephone=no">
    <meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0; user-scalable=no;">
    <meta http-equiv="x-ua-compatible" content="IE=9; IE=8; IE=7; IE=EDGE">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="x-ua-compatible" content="ie-edge">
    <meta name="x-apple-disable-message-reformatting">
    <title>Email</title>
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,100;0,500;0,700;1,400;1,500;1,700&display=swap" rel="stylesheet">
    <style type="text/css">
      #outlook a {
      padding: 0;
      }
      body {
      width: 100% !important;
      size-adjust: 100%;
      -ms-text-size-adjust: 100%;
      margin: 0;
      padding: 0;
      font-family: 'Montserrat', sans-serif;
      }
      .ii a[href] {
    color: #fff;
    text-decoration: none;
}
      .fl {
      width: 100%;
      height: 40px;
      border: 1px solid #fff;
      margin-bottom: 20px;
      background-color: transparent;
      color: #fff;
      }
      ::placeholder {
      color: #fff !important;
      padding-left: 10px;
      font-size: 16px;
      font-weight: 300;
      }
      table td {
      border-collapse: collapse;
      }
      #outlook a {
      padding: 0;
      }
      body {
      width: 100% !important;
      size-adjust: 100%;
      -ms-text-size-adjust: 100%;
      margin: 0;
      padding: 0;
      -webkit-font-smoothing: antialiased;
      }
      table td {
      border-collapse: collapse;
      }
      .ExternalClass * {
      line-height: 115%;
      }
      @media all and (min-width: 481px) and (max-width: 616px) {
      body {
      max-width: 96% !important;
      width: 96% !important;
      margin: 0 auto !important;
      padding: 0 !important;
      }
      .wrapper {
      width: 100% !important;
      max-width: 100% !important;
      text-align: center !important;
      }
      .full-width {
      width: 100% !important;
      max-width: 100% !important;
      text-align: center !important;
      display: inline-block !important;
      margin-left: auto !important;
      margin-right: auto !important;
      }
      .font_h1 {
      font-size: 30px !important;
      }
      .m-120 {
      height: 125px !important;
      height: 125 !important;
      }
      }
      @media all and (max-width: 480px) {
      body {
      max-width: 96% !important;
      width: 96% !important;
      margin: 0 auto !important;
      padding: 0 !important;
      }
      .wrapper {
      width: 100% !important;
      max-width: 100% !important;
      text-align: center !important;
      }
      .Heading1 {
      font-size: 28px !important;
      }
      .full-width {
      width: 100% !important;
      max-width: 100% !important;
      text-align: center !important;
      display: inline-block !important;
      margin-left: auto !important;
      margin-right: auto !important;
      }
      .m-center-image {
      text-align: center !important;
      }
      .m-center-text {
      text-align: center !important;
      }
      .PaddLR {
      padding-left: 10px !important;
      padding-right: 10px !important;
      }
      .m-none {
      display: none !important;
      height: 0px !important;
      height: 0 !important;
      }
      .desktop {
      display: none !important;
      }
      .mobile {
      display: block !important;
      }
      .font_h1 {
      font-size: 19px !important;
      }
      .m-120 {
      height: 20px !important;
      height: 20 !important;
      }
      .ii a[href] {
    color: #fff;
    text-decoration: none;
}
      }
    </style>
  </head>
  <body bgcolor="#E9E9E9" style="padding:0; margin:0 auto; background:#E9E9E9;">
    <table align="center" cellpadding="0" cellspacing="0" width="100%" bgcolor="#E9E9E9">
    <tr>
      <td align="center" width="100%" height="30" class=""></td>
    </tr>
    <tr>
      <td align="center" valign="top" style="">
              <table bgcolor="#fff  " align="center" border="0" cellpadding="0" cellspacing="0" width="600" class="wrapper" style="border-radius:10px;">
                <tbody>
                  <tr>
                    <td align="center" valign="top" style="padding:0px 0px 0px 0px;">
                      <table bgcolor="#fff  " align="center" border="0" cellpadding="0" cellspacing="0" width="600" class="wrapper" style="border-radius: 10px 10px;">
                        <tbody>
                          <tr>
                            <td align="center" valign="top">
                              <table bgcolor="#fff  " align="center" border="0" cellpadding="0" cellspacing="0" width="600" class="wrapper" style="border-radius: 10px;">
                                <tbody>
                                  <tr>
                                    <td align="center" width="100%" height="20" class=""></td>
                                  </tr>
                                  <tr>
                                    <td bgcolor=""  align="center" valign="middle" style="text-align:center; padding:0px 20px;" class=""><a href="#"><img src="{{ asset('front_end/images/logo.png') }}" width="109"></a></td>
                                  </tr>
                                  <tr>
                                    <td align="center" width="100%" height="10" class=""></td>
                                  </tr>
                                </tbody>
                              </table>
                            </td>
                          </tr>
                          <tr>
                            <td align="center" valign="top">
                              <table bgcolor="" align="center" border="0" cellpadding="0" cellspacing="0" width="600" class="wrapper" style="">
                                <tbody>
                                  <tr>
                                    <td bgcolor="" width="100%" align="center" valign="top" background="images/banner3.png" style="background-image:url(images/banner3.png) !important; background-size:cover; background-position:top !important; background-repeat:no-repeat;" class="paddLR20">
                                      <table bgcolor="" align="center" border="0" cellpadding="0" cellspacing="0" width="100%" class="wrapper" style="">
                                        <tbody>
                                          <tr>
                                            <td bgcolor=""  align="left" valign="top" style="text-align:left; padding:20px 20px 0px 20px; background-color: #ff4882;" class="">
                                              <div style="color:#fff; padding: 0px; text-align:center;font-size:24px;font-family: 'Montserrat', sans-serif !important; font-weight:bold; vertical-align:top;" class="">Welcome to localsfromzero</div>
                                            </td>
                                          </tr>
                                          <tr>
                                            <td bgcolor=""  align="left" valign="top" style="text-align:center; padding:0px 20px 20px 20px; background-color: #ff4882;" class="">
                                              <div style="color:#fff; padding: 15px 0px 0px 0px; text-align:center;font-size:18px;font-family: 'Montserrat', sans-serif !important; font-weight:medium; vertical-align:top; width:100%;" class="">you have some message notification.</div>
                                            </td>
                                          </tr>
                                          <tr>                  
                                            <table bgcolor="#afc578" align="center" border="0" cellpadding="0" cellspacing="0" width="100%" class="wrapper" style="">
                                              <tr>


                                            
                                           <td width="100%" bgcolor=""  align="left" valign="top" style="text-align:center; padding:0px 20px 0px 20px;" class="">
                                              <div style="color:#fff; padding: 15px 0px 0px 0px; text-align:left;font-size:14px;font-family: 'Montserrat', sans-serif !important; font-weight:medium; vertical-align:top;" class=""><span style="text-decoration: none;color: #fff;">
                                                    {{ isset($data) ? $data['message'] : '' }}
                                              </span>
                                          </div>
                                            </td> 
                                             


                                          </tr>
                                           
                                          </table>
                                          </tr>                                          
                                          
                                          
                                        </tbody>
                                      </table>
                                    </td>
                                  </tr>
                                </tbody>
                              </table>
                            </td>
                          </tr>
                          <tr>
                            <td bgcolor="#afc578"  align="center" valign="middle" style="" class="">
                              <table bgcolor="" align="center" width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                  <td align="center" width="100%" style="" class="full-width">
                                    <table bgcolor="" align="center" width="100%" border="0" cellspacing="0" cellpadding="0">
                                      <tr>
                                        <td bgcolor=""  align="center" valign="middle" style="color:#000000;padding:0px 20px 0px 20px; text-align:left;font-size: 15.2px; line-height:120%;font-family:Arial, Helvetica, sans-serif !important; font-weight:400;" class="">
                                        </td>
                                      </tr>
                                      <tr>
                                        <td align="center" width="100%" height="10" class="" bgcolor="#afc578"></td>
                                      </tr>
                                    </table>
                                  </td>
                                </tr>
                              </table>
                            </td>
                          </tr>
                          <tr>
                            <td align="center" width="100%" height="20" class="" style="border-radius: 0px 0px 10px 10px; background-color: #afc578"></td>
                          </tr>
                        </tbody>
                      </table>
                    </td>
                  </tr>
              </table>
            </td>
          </tr>
          <tr>
            <td align="center" width="100%" height="30" class=""></td>
          </tr>
        </table>
  </body>
</html>