@extends('layouts.app')
@section('content')
@php 
    $schedule = '';
    $scheduleArray = [];
    if(!empty($experience->experience_schedule->toArray())){
        $schedule = array_column($experience->experience_schedule->toArray(),'start_date');
    }
    if(!empty($schedule))
    {
     foreach($schedule as $sch)
     {
        $scheduleArray[]=date('j-n-Y',strtotime($sch));
     }
    }


    $schedule=json_encode($scheduleArray,JSON_UNESCAPED_SLASHES);
    $freshExp = json_encode($experience,JSON_UNESCAPED_SLASHES);

@endphp
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<section class="availability">
    <div class="container">
        <div class="row">
            <div class="main-heading w-100">
                <h3>DISCOVER COSMETICS FROM SNAIL SALIVA</h3>
            </div>
            <div class="col-md-6 col-sm-12">
            	<div class="guru">
                <div class="select-dates ">
                    <h3>Select dates</h3>
                    <input type="hidden" id="freshExpId" value="{{$experience->id}}">
                    <div class="dates-guests d-flex align-items-center">
                        <p class="flex-column align-items-start showDatepicker">DATES<span><input id="datepickerdesktop" value="{{ date('M d',strtotime($experience->experience_schedule[0]->start_date)) }}" class="" autocomplete="off"><input type="hidden" name="booking_date" id="booking_date" value="{{ date('Y-m-d', strtotime($experience->experience_schedule[0]->start_date)) }}" autocomplete="off"></span></p>
                        <p id="guest-count" class="flex-column align-items-start guest-field">GUESTS<span class="d-flex align-items-center"><b class="font-weight-normal mr-1" id="dynamic_guest_count">1</b> guest</span></p>
                        <div class="guest_inner_sec" style="display: none;">
                            <ul>
                                <li>
                                    <span class="left_bar">Adults </span>
                                    <span class="right_bar">
                                    <button class="minus-adult-button"><i class="fa fa-minus" aria-hidden="true"></i></button> 
                                    <span>
                                    <input type="number" min="1" max="{{$experience->maximum_participant}}" value="1" id="quantity" name="adults" class="adult-cls">
                                    <button class="plus-adult-button"><i class="fa fa-plus" aria-hidden="true"></i></button>
                                    </span>
                                    </span>
                                </li>
                                <li>
                                    <span class="left_bar">Children <br>
                                    <span class="text_sec_inner">2-12</span>
                                    </span>
                                    <span class="right_bar">
                                    <button class="minus-children-button"><i class="fa fa-minus" aria-hidden="true"></i></button>
                                    <span>
                                    <input type="number" min="0" name="children" class="child-cls" max="{{$experience->minimum_participant}}" value="0" id="quantity">
                                    <button class="plus-children-button"><i class="fa fa-plus plus-button" aria-hidden="true"></i></button>
                                    </span>
                                    </span>
                                </li>
                                <li>
                                    <span class="left_bar">Infants <br>
                                    <span class="text_sec_inner">Under 2</span>
                                    </span>
                                    <span class="right_bar">
                                    <button class="minus-infant-button"><i class="fa fa-minus minus-button" aria-hidden="true"></i> </button>
                                    <span>
                                    <input type="number" name="infant" min="0" class="infant-cls" value="0" id="quantity">
                                    <button class="plus-infant-button"><i class="fa fa-plus plus-button" aria-hidden="true"></i></button>
                                    </span>
                                    </span>
                                </li>
                            </ul><a href="javascript:void(0)" class="save-guest-data">Save</a>
                        </div>
                    </div>
                </div>

                <div class="chat-box fresh-chat">
                    <div class="chat-stick d-flex align-items-center">
                        @if ($experience->user->user_image_path && file_exists(public_path($experience->user->user_image_path)) && $experience->user->user_image)
                        <img src="{{ asset($experience->user->user_image_path.'/'.$experience->user->user_image) }}" alt="Admin" class="" />
                        @else
                        <img src="{{ asset('users/user.png') }}" alt="Admin" class=""  />
                        @endif 
                        <div class="chat-stick-inner pl-4">
                            <h3><span>Scout:</span> <i class="far fa-comment-dots"></i> {{$experience->user->user_fname.' '.$experience->user->user_lname}}</h3>
                            <div class="scout-rating d-flex align-items-center">
                                <p class="m-0">4.36 <i class="fas fa-star" aria-hidden="true"></i> (25)</p>
                                <ul class="contact-social pl-3 mt-0">
                                    <li><a href="#"><i class="fab fa-facebook-f" aria-hidden="true"></i></a></li>
                                    <li><a href="#"><i class="fab fa-instagram" aria-hidden="true"></i></a></li>
                                    <li><a href="#"><i class="fab fa-pinterest-p" aria-hidden="true"></i></a></li>
                                    <li><a href="#"><i class="fab fa-linkedin-in" aria-hidden="true"></i></a></li>
                                </ul>
                            </div>
                            <button class="btn btn-success green-bg rounded-btn mt-3">Contact</button>
                        </div>
                    </div>
                </div>
                </div>
            </div>
            <div class="col-md-6 col-sm-12">
                <div class="availability-right pl-lg-5 mt-lg-2 mt-sm-5" id="dynamic-all-slots">

                	<?php $interval = new DateInterval('PT'.$experience->experience_duration.'H'); ?>
                	@forelse ($experience->experience_schedule as $expSchedule)
                		<div class="availability-right-inner">
	                        <h5 class="mb-3"> {{ date('D, d M',strtotime($expSchedule->start_date))}}</h5>
                        <input type="hidden" id="booking_amount" value="{{$experience->experience_low_price}}">
                        <input type="hidden" id="booking_total_amount" value="{{$experience->experience_low_price}}">
                        <input type="hidden" id="booking_discount" value="0">
                       
	                        <?php 
                            $alreadyBookedStartTime=[];
                            $alreadyBookedEndTime=[];
                            if(!empty($alreayBookedSlots[$expSchedule->start_date]))
                            {
                            $alreadyBookedStartTime=array_column($alreayBookedSlots[$expSchedule->start_date],'booking_start_time');
                            $alreadyBookedEndTime=array_column($alreayBookedSlots[$expSchedule->start_date],'booking_end_time');
                            }
	                        if($expSchedule->schedule_type!=1){
				                $startDateTime = new DateTime($expSchedule->start_time);
				                $endDateTime   = new DateTime($expSchedule->end_time);
				                $to_time = strtotime($expSchedule->start_date.' '.$expSchedule->end_time);
				                $from_time = strtotime($expSchedule->start_date.' '.$expSchedule->start_time);
				            }else{
				                $startDateTime = new DateTime("00:00:00");
				                $endDateTime   = new DateTime("23:59:59"); 
				                $to_time = strtotime($expSchedule->start_date.' 23:59:00');
				                $from_time = strtotime($expSchedule->start_date.' 00:00:00');
				            }
				            $times    = new DatePeriod($startDateTime, $interval, $endDateTime);
				            $freshExperienceDuration = $expSchedule->experience->experience_duration * 60;
				            $totalDuration = round(abs($to_time - $from_time) / 60,2);
				            $diffr = floor($totalDuration / $freshExperienceDuration);
				            $diffr = abs($diffr);
				            if($diffr>0){            
				                $i=0;
				                foreach($times as $key=>$time) {
				                    if($i<$diffr){ 
                                        $start_time=$time->format('h:i A');
                                        $end_time=$time->add($interval)->format('h:i A');
                                        if (Auth::check()) {
                                            if(in_array($start_time,$alreadyBookedStartTime) && in_array($end_time,$alreadyBookedEndTime))
                                        {
                                            $bookAnchor='<a class="exp-sold-out" href="javascript:void(0)" disabled>Sold Out</a>'; 
                                        }else{
                                            $bookAnchor='<a class="book-me-now" data-row="'.date('d',strtotime($expSchedule->start_date)).'-'.$key.'" href="javascript:void(0)">Choose</a>';
                                        }
                                        }else{
                                             if(in_array($start_time,$alreadyBookedStartTime) && in_array($end_time,$alreadyBookedEndTime))
                                        {
                                            $bookAnchor='<a class="exp-sold-out" data-row="'.$key.'" href="javascript:void(0)" disabled>Sold Out</a>'; 
                                        }else{
                                            $bookAnchor='<a class=" " href="'.route('login').'">Choose</a>';
                                        }
                                        }
                                        ?>
				                        <div class="price-choose-sec">
				                            <div class="left-price-chse-inr">
				                                <p><?php 
				                                	echo $start_time.'-'.$end_time
				                                	?>  
				                            	</p>
                                                 <input type="hidden" id="booking_date-{{date('d',strtotime($expSchedule->start_date)).'-'.$key}}" value="{{ date('Y-m-d',strtotime($expSchedule->start_date))}}">
                                            <input type="hidden" id="booking_start_time-{{date('d',strtotime($expSchedule->start_date)).'-'.$key}}" value="<?= date('H:i:s',strtotime($start_time));?>">
                                            <input type="hidden" id="booking_end_time-{{date('d',strtotime($expSchedule->start_date)).'-'.$key}}" value="<?= date('H:i:s',strtotime($end_time));?>">
				                                <p class="mb-4"><i class="fas fa-euro-sign"></i>{{number_format($experience->experience_low_price)}}
                                                    valid for 1 member
                                                    <!-- {{($experience->experience_price_vailid_for=='null')? '1': '1'}} {{($experience->experience_price_vailid_for=='null' || $experience->experience_price_vailid_for== 1)? 'member': 'members'}} -->
                                                </p>
				                            </div>
				                            <div class="right-price-chse-inr">
                                                  <?= $bookAnchor;?>

				                            </div>
				                        </div> <?php
				                	}  
				                    $i++;      
				                }
				            }else{

				            } ?>
	                    </div>
					@empty
					    <p>There is no more slot for this experience </p>
					@endforelse
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
@section('javascript')
 <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js" ></script>
<script type="text/javascript">
    var exp_id="{{$experience->id}}",
    maxParticipants="{{ $experience->maximum_participant }}";
    var schedule=<?= $schedule;?>;
    var freshExp = <?= $freshExp;?>;
</script>

<script type="text/javascript">
       $(document).click(function(e){
         var container = $(".dates-guests"); 
        // If the target of the click isn't the container
        if(!container.is(e.target) && container.has(e.target).length === 0 ){ 
                $(".guest_inner_sec").slideUp();
        } 
    });
</script>
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="{{ asset('js/experience-payment-page.js') }}"></script>
@endsection