 @extends('layouts.app')
@section('content')
@php 
    $social_links = json_decode($social_link); 
@endphp
<!-- featured exp start here -->
<section class="featured-exp featured-exp-listing-grid aos-item product-detail-section scout-detail-main-sec" data-aos="fade-down">
    <div class="container">
        <div class="row">
            <!-- exp-box start -->
                <div class="front-scout-detail-page col-md-3 col-sm-12">
					@if ($userData->user_image_path && file_exists(public_path($userData->user_image_path)) && $userData->user_image)
						<img src="{{ asset($userData->user_image_path.'/'.$userData->user_image) }}" alt="Admin" class="" />
					@else
						<img src="{{ asset('users/user.png') }}" alt="Admin" class=""  />
					@endif 
					 @unless(Auth::check())
						<a class="btn btn-success pink-bg rounded-btn" href="{{ route('login') }}"><i class="far fa-comments"></i> Contact Scout</a>
					@endunless
					@if(Auth::check() && Auth::user()->user_role==2)
						<a class="btn btn-success pink-bg rounded-btn" href="{{route('traveler-dashboard',$userData->id) }}"><i class="far fa-comments"></i> Contact Scout</a>
					@endif
                </div>
                <div class="right-scout-detail col-md-9 col-sm-12">
                <div class="main-heading text-left d-flex align-items-center justify-content-between">
                    <h3>Scout {{  $userData->user_fname .' '. $userData->user_lname }}</h3>
                    @unless(Auth::check())
                    	<div class="right-scout-inner d-flex align-items-center">
                    		<div class="chat-box">
						<ul class="contact-social">
                            @if(!empty($social_links->facebook_link))  
                            <li><a target="_blank" href="{{ !empty($social_links->facebook_link) ? $social_links->facebook_link : 'javascript:;' }}"><i class="fab fa-facebook-f" aria-hidden="true"></i></a></li>
                            @endif
                            @if(!empty($social_links->instagram_link)) 
                            <li><a target="_blank" href="{{ !empty($social_links->instagram_link) ? $social_links->instagram_link : 'javascript:;' }}"><i class="fab fa-instagram" aria-hidden="true"></i></a></li>
                            @endif
                            @if(!empty($social_links->pinterest_link)) 
                            <li><a target="_blank" href="{{ !empty($social_links->pinterest_link) ? $social_links->pinterest_link : 'javascript:;' }}"><i class="fab fa-pinterest-p" aria-hidden="true"></i></a></li>
                            @endif
                            @if(!empty($social_links->linkedin_link)) 
                            <li><a target="_blank" href="{{ !empty($social_links->linkedin_link) ? $social_links->linkedin_link : 'javascript:;' }}"><i class="fab fa-linkedin-in" aria-hidden="true"></i></a></li>
                            @endif
                            @if(!empty($social_links->twitter_link)) 
                            <li><a target="_blank" href="{{ !empty($social_links->twitter_link) ? $social_links->twitter_link : 'javascript:;' }}"><i class="fab fa-twitter" aria-hidden="true"></i></a></li>
                            @endif 
						</ul>
						</div>
					</div>
					@endunless
					@if(Auth::check() && Auth::user()->user_role==2)
						<div class="right-scout-inner d-flex align-items-center">
                    		<div class="chat-box">
						<ul class="contact-social">
                              @if(!empty($social_links->facebook_link))  
                            <li><a target="_blank" href="{{ !empty($social_links->facebook_link) ? $social_links->facebook_link : 'javascript:;' }}"><i class="fab fa-facebook-f" aria-hidden="true"></i></a></li>
                            @endif
                            @if(!empty($social_links->instagram_link)) 
                            <li><a target="_blank" href="{{ !empty($social_links->instagram_link) ? $social_links->instagram_link : 'javascript:;' }}"><i class="fab fa-instagram" aria-hidden="true"></i></a></li>
                            @endif
                            @if(!empty($social_links->pinterest_link)) 
                            <li><a target="_blank" href="{{ !empty($social_links->pinterest_link) ? $social_links->pinterest_link : 'javascript:;' }}"><i class="fab fa-pinterest-p" aria-hidden="true"></i></a></li>
                            @endif
                            @if(!empty($social_links->linkedin_link)) 
                            <li><a target="_blank" href="{{ !empty($social_links->linkedin_link) ? $social_links->linkedin_link : 'javascript:;' }}"><i class="fab fa-linkedin-in" aria-hidden="true"></i></a></li>
                            @endif
                            @if(!empty($social_links->twitter_link)) 
                            <li><a target="_blank" href="{{ !empty($social_links->twitter_link) ? $social_links->twitter_link : 'javascript:;' }}"><i class="fab fa-twitter" aria-hidden="true"></i></a></li>
                            @endif 
						</ul>
						</div>
					</div>
					@endif
                </div>
                <div class="filters-left back-transparent">
				<div class="language-spoken">
                    <p><strong>Primary Spoken Language :</strong> {{ optional($userData->user_languages)->name }}</p>
                    @php 
                        $get_other_language = [];
                        if(isset($userData) && !empty($userData->user_other_languages)){
                            $get_other_language = explode(",",$userData->user_other_languages);
                        }
                        if($userData->user_primary_language!=''){  
                             if (($key = array_search($userData->user_primary_language, $get_other_language)) !== false) {
                                 unset($get_other_language[$key]);
                             }
                         }
                      $select_language = [];
                    @endphp 

                    @if(isset($languages) && !empty($languages))                         @foreach($languages as $language)
                    @php
                       
                        if(in_array($language->id,$get_other_language)){
                            $select_language[] =$language->name ;
                        }

                        
                    @endphp
                     @endforeach
                     @endif
                    <p><strong>Other Spoken Language :</strong> {{ implode(', ',$select_language) }}</p>
                    <p><strong>Phone No :</strong> {{ formatPhoneNo($userData->user_mobile,$userData->user_mobile_code)  }}</p>
                 
                </div>
				<div class="chat-box">
				<div class="scout-rating">
		 @if($mainArrRa = array_column($userData->review_ratings->toArray(), 'rating'))
            @php
                $mainArrRa = array_count_values($mainArrRa);                                    
                $eachUpeer = array();  $eachLower = array(); $loop = 0;
                foreach ($mainArrRa as $key => $val) {
                    $eachUpeer[] = $key * $val;
                    $eachLower[] = $val;
                }
                $ratingValSum = array_sum($eachUpeer);
                $ratingNumSum = array_sum($eachLower);
                $rating = $ratingValSum / $ratingNumSum;    
            @endphp
            <p>{{ round($rating, 2) }} <i class="fas fa-star"></i> ({{ $ratingNumSum }})</p>
            @else
            <p>0<i class="fas fa-star"></i> (0)</p>
            @endif
		</div>
		</div>
                   <p>{{  $userData->about_me }}</p>
                </div>
            </div>
			</div>
			</div>
					
	<div class="scout-detail-list w-100">
	<div class="container">
		<div class="main-heading">
		<h3>Experience List</h3>
		</div>
    <div class="row m-0 scout-experience-list">
    @include('front_end.scout-experience-list')


</div>
</div>
</div>
@php
		$reviews = $userData->scout_review_ratings;
		$experience_reviews = $reviews->where('review_type',0);
		$scout_reviews = $reviews->where('review_type',3);
		$host_reviews = $reviews->where('review_type',4);
	@endphp				
                <div class="tab-reviews w-100 mt-5 pl-3 pr-3 exp-detail-new">
				<div class="container">
				<div class="row m-0">
        
        <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
  <li class="nav-item" role="presentation">
    <a class="nav-link active" id="pills-home-tab" data-toggle="pill" href="#pills-profile" role="tab" aria-controls="pills-profile" aria-selected="true">Scout Reviews <div class="rating">
			
			<div class="d-flex align-items-center tab-ratings">{{getScoutReviewbyType($userData->id,3)}}
			<ul>
			<li class="filled-star"><i class="fas fa-star" aria-hidden="true"></i></li>
			</ul>({{$reviews->where('review_type',3)->count()}})</div>
			
			</div></a>
  </li>
  <li class="nav-item" role="presentation">
    <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#pills-home" role="tab" aria-controls="pills-home" aria-selected="false">Experience Reviews <div class="rating">
			
			<div class="d-flex align-items-center tab-ratings">{{getScoutReviewbyType($userData->id,0)}}
			<ul>
			<li class="filled-star"><i class="fas fa-star" aria-hidden="true"></i></li>
			</ul>({{$reviews->where('review_type',0)->count()}})</div>
			
			</div></a>
  </li>
  <li class="nav-item" role="presentation">
    <a class="nav-link" id="pills-contact-tab" data-toggle="pill" href="#pills-contact" role="tab" aria-controls="pills-contact" aria-selected="false">Host Reviews <div class="rating">
			
			<div class="d-flex align-items-center tab-ratings">{{getScoutReviewbyType($userData->id,4)}}
			<ul>
			<li class="filled-star"><i class="fas fa-star" aria-hidden="true"></i></li>
			</ul>({{$reviews->where('review_type',4)->count()}})</div>
			
			</div></a>
  </li>
</ul>

<div class="tab-content w-100" id="pills-tabContent">
  
  <div class="tab-pane fade" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">
  <!-- testimonial left start here -->

    <div class="left-testimonial-box">
    	@if(!empty($experience_reviews))
         @if($experience_reviews->count() > 0)
    	@foreach($experience_reviews as $exp_review)
    	@php
    	     $str_length=\Illuminate\Support\Str::length($exp_review->review);
            $exp_image = optional($exp_review->user)->user_image;
            $exp_path = optional($exp_review->user)->user_image_path;

            if(empty($exp_image)){
    	    	$exp_image = 'testimonial-left-img.png';
        	}

        	if(empty($exp_path)){
    		    $exp_path = 'front_end/images';
        	}
    	    
    	@endphp
    <div class="single-testimonial">
    
	<div class="customer-testimonial-details">
		<img src="{{ asset($exp_path.'/'.$exp_image) }}" />
		<div class="name-country">
		<div class="rate-name">
			<a href="javascript:void(0);">{{ $exp_review->user->user_fname.' '.$exp_review->user->user_lname }}</a><div class="rating">
		<div class="d-flex align-items-center tab-ratings">{{ $exp_review->rating }}
		<ul>
		<li class="filled-star"><i class="fas fa-star" aria-hidden="true"></i></li>
		</ul></div>
		
		</div></div>
			<p><span class="coutry-flag"><img src="images/newzeland-flag.jpg" />{{ $exp_review->user->user_address}}</span> <span class="date green-color">{{ date('d M, Y',strtotime($exp_review->created_at))}}</span></p>
				
		</div>
	</div>
        
        <p class="minimize" data-lenght="{{$str_length}}">{{ $exp_review->review }}</p>
    </div>
    
    
    <hr>
    @endforeach
      @else
            <p class="no-reviews d-flex align-items-center m-0 p-4">No Reviews Found <a href="{{route('login') }}" class="btn btn-danger pink-bg ml-4">Give Review</a></p>
         @endif
	@endif
    </div>
    
    <!-- testimonial left ends here --> 

  </div>
  
  <div class="tab-pane fade show active" id="pills-profile" role="tabpanel" aria-labelledby="pills-profile-tab">
    
 <!-- testimonial left start here -->

        <div class="left-testimonial-box ">

        	@if(!empty($scout_reviews))
            @if($scout_reviews->count() > 0)
        	   @foreach($scout_reviews as $exp_review)
                	@php
                	    $str_length=\Illuminate\Support\Str::length($exp_review->review);
                        $exp_image = optional($exp_review->user)->user_image;
                        $exp_path = optional($exp_review->user)->user_image_path;

                        if(empty($exp_image)){
                	    	$exp_image = 'testimonial-left-img.png';
                    	}

                    	if(empty($exp_path)){
                		    $exp_path = 'front_end/images';
                    	}
                	    
                	@endphp
                <div class="single-testimonial">
                
            	<div class="customer-testimonial-details">
            		<img src="{{ asset($exp_path.'/'.$exp_image) }}" />
            		<div class="name-country">
            		<div class="rate-name">
            			<a href="javascript:void(0);">{{ $exp_review->user->user_fname.' '.$exp_review->user->user_lname }}</a><div class="rating">
            		<div class="d-flex align-items-center tab-ratings">{{ $exp_review->rating }}
            		<ul>
            		<li class="filled-star"><i class="fas fa-star" aria-hidden="true"></i></li>
            		</ul></div>
            		
            		</div></div>
            			<p><span class="coutry-flag"><img src="images/newzeland-flag.jpg" />{{ $exp_review->user->user_address}}</span> <span class="date green-color">{{ date('d M, Y',strtotime($exp_review->created_at))}}</span></p>
            				
            		</div>
            	</div>
                    
                    <p class="minimize" data-lenght="{{$str_length}}">{{ $exp_review->review }}</p>
                </div>
                
                
                <hr>
                 @endforeach  
         @else
            <p class="no-reviews d-flex align-items-center m-0 p-4">No Reviews Found <a href="{{route('login') }}" class="btn btn-danger pink-bg ml-4">Give Review</a></p>
    	 @endif
     @endif
        </div>
    
    <!-- testimonial left ends here -->

  </div>
  <div class="tab-pane fade" id="pills-contact" role="tabpanel" aria-labelledby="pills-contact-tab">
    
     <!-- testimonial left start here -->

        <div class="left-testimonial-box">
        	@if(!empty($host_reviews))
            @if($host_reviews->count() > 0)
        	@foreach($host_reviews as $exp_review)
        	@php
        	    $str_length=\Illuminate\Support\Str::length($exp_review->review);
                $exp_image = optional($exp_review->user)->user_image;
                $exp_path = optional($exp_review->user)->user_image_path;

                if(empty($exp_image)){
        	    	$exp_image = 'testimonial-left-img.png';
            	}

            	if(empty($exp_path)){
        		    $exp_path = 'front_end/images';
            	}
        	    
        	@endphp
        <div class="single-testimonial">
        
    	<div class="customer-testimonial-details">
    		<img src="{{ asset($exp_path.'/'.$exp_image) }}" />
    		<div class="name-country">
    		<div class="rate-name">
    			<a href="javascript:void(0);">{{ $exp_review->user->user_fname.' '.$exp_review->user->user_lname }}</a><div class="rating">
    		<div class="d-flex align-items-center tab-ratings">{{ $exp_review->rating }}
    		<ul>
    		<li class="filled-star"><i class="fas fa-star" aria-hidden="true"></i></li>
    		</ul></div>
    		
    		</div></div>
    			<p><span class="coutry-flag"><img src="images/newzeland-flag.jpg" />{{ $exp_review->user->user_address}}</span> <span class="date green-color">{{ date('d M, Y',strtotime($exp_review->created_at))}}</span></p>
    				
    		</div>
    	</div>
            
            <p class="minimize" data-lenght="{{$str_length}}">{{ $exp_review->review }}</p>
        </div>
        
        
        <hr>
        @endforeach
         @else
            <p class="no-reviews d-flex align-items-center m-0 p-4">No Reviews Found <a href="{{route('login') }}" class="btn btn-danger pink-bg ml-4">Give Review</a></p>
         @endif
     @endif
        </div>
    
    <!-- testimonial left ends here -->
  </div>
</div>

    </div>
        </div>
    </div>
</section>
@endsection
@section('javascript')
<script>
$(document).ready(function(){
$(document).on('click', '.pagination a', function(event){
		$('.loader').css('display','flex');
		event.preventDefault();
	  	var page = $(this).attr('href').split('page=')[1];
	  	$('#hidden_page').val(page);
	  	$('li').removeClass('active');
	    $(this).parent().addClass('active');
	  	scoutExperienceDdata(page);
	});
});
function scoutExperienceDdata(page){
    var user_id="{{$id}}";
	$('.filter_data').html('<div id="loading" style="" ></div>');
	$.ajax({
		headers: {
	        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	    },
	    url: HOST_URL+"/scout-mat?page="+page,
	    method:"GET",
	    data:{user_id:user_id},
	    success:function(data){
	        $('.loader').css('display','none');
	    	if(data.isSucceeded){
	        
	    		$('.scout-experience-list').html('');
	    		$('.scout-experience-list').html(data.data);
	    	}else{
	    		$('.scout-experience-list').html(data.data);
	    	}
	    }
	});
		}
	</script>
@endsection
<!-- featured exp ends here -->