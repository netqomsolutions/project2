@extends('layouts.app')
@section('content')
@php  
    $allBanner = json_decode($homePageData->banner_content);
    $explore = json_decode($homePageData->explore_content);
    $bgIm = asset('pages/homepage/'.$explore->explore_background_image);
@endphp
<!-- home-slider start here -->
<div class="carousel slide aos-item home-banner" data-aos="fade-up" id="main-carousel" data-ride="carousel">
    <ol class="carousel-indicators">
        @forelse ($allBanner as $key => $bannerIm)
            <li data-target="#main-carousel" data-slide-to="{{ $key }}" class="{{ $key == 0 ? 'active' : '' }}"></li>
        @empty

        @endforelse
    </ol>
    <!-- /.carousel-indicators -->
    <div class="carousel-inner">
        @forelse ($allBanner as $key => $banner)
            <div class="carousel-item {{ $key == 0 ? 'active' : ''}}">
                <div class="slide-content">
                    <img class="d-block img-fluid" src="{{ asset('pages/homepage/'.$banner->banner_image) }}" alt="">
                    <div class="carousel-caption d-none d-md-block">
                        <h5 class="curly-font">
                        {{ $banner->banner_small_heading }}</h3>
                        <h3>{{ $banner->banner_title }}</h3>
                        <p>{{ $banner->banner_description }}</p>
                        <a href="{{isset($banner->banner_button_link) || !empty($banner->banner_button_link)? $banner->banner_button_link : '#'}}" class="btn btn-danger pink-bg banner-btn">{{ $banner->banner_button_name }}</a>
                    </div>
                </div>
            </div>
        @empty

        @endforelse
    </div>
</div>
<!-- /.carousel -->
<!-- home-slider ends here -->
<!-- search form start here -->
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <form class="search-form" method="get" action="{{route('experiences-list')}}">
                <div class="form-group">
                    <input type="text" name="name" class="form-control" id="lets-find" placeholder="Type activity-related keyword">
                </div>
                <div class="form-group mx-sm-3">
                    <input type="text" name="address" class="form-control" id="exp-location" placeholder="Location">
                    <input type="hidden" name="lng" class="form-control" id="exp-longitude" placeholder="Location">
                    <input type="hidden" name="lat" class="form-control" id="exp-latitude" placeholder="Location">
                </div>
                <div class="form-group mx-sm-3">
                    <select class="form-control" name="cat">
                        <option value="">All Categories</option>
                        @forelse ($experienceCategories as $expCat)
                            <option value="{{ $expCat->id }}">{{ $expCat->name }}</option>
                        @empty
                        @endforelse
                    </select>
                </div>
                <button type="submit" class="btn btn-danger pink-bg home-ser">Search</button>
            </form>
        </div>
    </div>
</div>
<!-- search form ends here -->
<!-- how it work start here -->
<section class="how-it-work aos-item" data-aos="fade-down">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="main-heading">
                    <h5 class="curly-font green-color">Discover & Connect</h5>
                    <h3>How it works</h3>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-12">
                <div class="step aos-item" data-aos="fade-in">
                    <a href="{{route('how-it-works')}}">
                        <span class="cloud-span"><img src="{{ asset('front_end/images/how-work-icon1.png') }}" /></span>
                        <h4>{{$connect->section_one_title}}</h4>
                        <p>{{$connect->section_one_description}}</p>
                        <span class="count-num">01</span>    
                    </a>                    
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-12">
                <div class="step aos-item" data-aos="fade-in">
                    <a href="{{route('how-it-works')}}">
                        <span class="cloud-span"><img src="{{ asset('front_end/images/how-work-icon2.png') }}" /></span>
                        <h4>{{$connect->section_two_title}}</h4>
                        <p>{{$connect->section_two_description}}</p>
                        <span class="count-num">02</span>    
                    </a>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-12">
                <div class="step aos-item" data-aos="fade-in">
                    <a href="{{route('how-it-works')}}">
                        <span class="cloud-span"><img src="{{ asset('front_end/images/how-work-icon3.png') }}" /></span>
                        <h4>{{$connect->section_three_title}}</h4>
                        <p>{{$connect->section_three_description}}</p>
                        <span class="count-num">03</span>    
                    </a>                    
                </div>
            </div>
        </div>
    </div>
</section>
<!-- how it work start here -->
<!-- featured exp start here -->
<section class="featured-exp">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="main-heading">
                    <h5 class="curly-font green-color">Choose your perfect Holiday</h5>
                    <h3>Featured Experiences</h3>
                </div>
            </div>
            @forelse ($experiences as $key => $experience)
            @php
                $exp_path = '';
                $exp_image = '';
                
                if(isset($experience) && optional($experience->user)){
                    $exp_image = optional($experience->user)->user_image;
                    $exp_path = optional($experience->user)->user_image_path;
                }
            @endphp
                <!-- exp-box start -->
                <div class="col-lg-4 col-md-4 col-sm-12">
                    <div class="exp-box">
                        <div class="exp-img"><a href="{{route('experience-detail',['id'=>base64_encode($experience->id)])}}"><img src="{{ asset('pages/experiences/'.$experience->experience_feature_image) }}"/></a></div>
                        <div class="exp-content">
                            <div class="title"><a href="{{route('experience-detail',['id'=>base64_encode($experience->id)])}}">{{ $experience->experience_name }}</a></div>
                            <div class="rating-price">
                                @php

                                $rating=0;$ratingValSum=0;$ratingNumSum=0;
                                @endphp
                                @if(!empty($experience->review_ratings))
                                    @php
                                        $eachUpeer = array();  $eachLower = array(); $loop = 0; $mainArrRa = array();
                                        foreach ($experience->review_ratings->toArray() as $key => $val) {
                                            if($val['review_for'] == 0){
                                                $mainArrRa[] = $val['rating'];
                                            }
                                        }
                                        $mainArrRa = array_count_values($mainArrRa);
                                        if(!empty($mainArrRa))
                                        {
                                        foreach ($mainArrRa as $key => $val) {
                                          $eachUpeer[] = $key * $val;
                                          $eachLower[] = $val;
                                        }
                                        $ratingValSum = array_sum($eachUpeer);
                                        $ratingNumSum = array_sum($eachLower);
                                        $rating = $ratingValSum / $ratingNumSum; 
                                        }   
                                    @endphp
                                    
                                    <div class="rating">
                                        <ul mk="{{ $rating }}">
                                            {{--Start Rating--}}
                                            @for ($i = 0; $i < 5; $i++)
                                                @if (floor($rating) - $i >= 1)
                                                    {{--Full Start--}}
                                                    <i class="fas fa-star text-warning"> </i>
                                                @elseif ($rating - $i > 0)
                                                    {{--Half Start--}}
                                                    <i class="fas fa-star-half-alt text-warning"> </i>
                                                @else
                                                    {{--Empty Start--}}
                                                    <i class="far fa-star text-warning"> </i>
                                                @endif
                                            @endfor
                                            {{--End Rating--}}
                                            <li>{{ $ratingNumSum}} Reviews</li>
                                        </ul>
                                        
                                    </div>
                                @else
                                    <div class="rating">
                                        <ul>
                                            <i class="far fa-star text-warning"> </i>
                                            <i class="far fa-star text-warning"> </i>
                                            <i class="far fa-star text-warning"> </i>
                                            <i class="far fa-star text-warning"> </i>
                                            <i class="far fa-star text-warning"> </i>
                                            <li>0 Reviews</li>
                                        </ul>
                                    </div>                                
                                @endif


                                   @php
                                      $addToFav = "wishlist-span";
                                      $addToFavText = "Login as Traveller";
                                      $userId =""; 
                                      $is_traveler =""; 
                                    @endphp

                                @if(Auth::check()) 

                                 @if(!empty($user) && $user->user_role == 2)
                                    @php
                                     $userId = $user->id; 
                                     $is_traveler = "addToWishList";
                                     $addToFavText = "Add to wishlist";
                                    @endphp
                                  @else
                                   @php
                                    $is_traveler = "notToaddInWishList-notuse";
                                    $userId = 0; 
                                    $addToFavText = "Only for Traveller";
                                    @endphp
                                  @endif 
                                 @endif     
                                    
                               @if(!empty($UserwishList))
                                     @foreach ($UserwishList as $key => $val) 
                                        @if($experience->id == $val->experience_id && $val->is_favourite == 1)
                                         @php
                                                $addToFav = "add-to-fav";
                                                $addToFavText = "In WishList";
                                                @endphp
                                         @endif 
                                    @endforeach
                                @endif

                                 <div class="price home-page-list">
                                    <a id="wishlist-show" href="javascript:void(0)" data-default="wishlist-span" data-expid="{{ $experience->id }}" data-id="{{ $userId }}" class="{{ $addToFav }} {{ $is_traveler }}"><i class="far fa-heart"></i></a>
                                    <span id="wishlist-tooltip"> {{ $addToFavText }} </span>
                                </div>
                               <!--  <div class="price homepage">
                                    <a id="wishlist-show" href="#" class="wishlist-span"><i class="far fa-heart"></i></a>
									<span id="wishlist-tooltip" class="for-home-page">Add to wishlist</span>
                                </div> -->
                            </div>
                            <div class="price-div">
                                <span class="price-info">
                                <span class="price-span">€{{ number_format($experience->experience_low_price) }} {{ $experience->experience_high_price != 0.00 ? "- ".number_format($experience->experience_high_price) : ''  }}</span>
                                Group Price For 1-{{ $experience->experience_price_vailid_for }} Persons
                                </span>
                            </div>
                            <div class="booking-conditions">
                                <ul>
                                    <li><i class="far fa-user"></i> Max:  {{ $experience->experience_group_size }}</li>
                                    <li><i class="far fa-clock"></i> {{ $experience->experience_duration }} Hours</li>
                                    <li><i class="fas fa-map-marker-alt"></i>{{ $experience->experience_lname}}</li>
                                </ul>
                            </div>
                            <div class="popup-new-scout" data-user-id="{{base64_encode($experience->user->id)}}">
							<a href="{{route('scout-detail',['id'=>base64_encode($experience->user->id)])}}" class="experience-popup">
								<div class="scout-box green-bg">
									<div class="scout-img-name">
										 @if (file_exists(public_path($exp_path)) && $exp_image)
                                <img src="{{ asset($exp_path.'/'.$exp_image) }}" />
                                @else
                                <img src="{{ asset('users/user.png') }}" />
                                @endif 
										{{ $experience->user->user_fname .' '. $experience->user->user_lname  }}
									</div>
									<div class="scout-rating">
										 @if($mainArrRa = array_column($experience->user->review_ratings->toArray(), 'rating'))
											@php
												$mainArrRa = array_count_values($mainArrRa);                                    
												$eachUpeer = array();  $eachLower = array(); $loop = 0;
												foreach ($mainArrRa as $key => $val) {
												  $eachUpeer[] = $key * $val;
												  $eachLower[] = $val;
												}
												$ratingValSum = array_sum($eachUpeer);
												$ratingNumSum = array_sum($eachLower);
												$rating = $ratingValSum / $ratingNumSum;    
											@endphp
											<p>{{ round($rating, 2) }} <i class="fas fa-star"></i> ({{ $ratingNumSum }})</p>
										@else
											<p>0<i class="fas fa-star"></i> (0)</p>
										@endif
									</div>
								</div>
							</a>
                            <div class="scout-main-info dynamic-scout-content">
                                </div>
                        </div>
							
                        </div>
                    </div>
                </div>
                <!-- exp-box ends -->
            @empty

            @endforelse 
			<div class="col-sm-12 p-0 text-center">
             <a href="{{route('experiences-list')}}" class="btn btn-danger pink-bg experience-round-btn">View All Experiences</a>           
			 </div>
        </div>
    </div>
</section>
<!-- featured exp ends here -->
<!-- video start here -->   
<section class="video-sec aos-item" data-aos="fade-in" style="background: url('<?php echo $bgIm; ?>');">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="main-heading">
                    <h5 class="curly-font green-color">{{ $explore->explore_small_heading }}</h5>
                    <h3>{{ $explore->explore_title }}</h3>
                </div>
            </div>
            <div class="col-lg-10 col-md-10 col-sm-12 offset-lg-1 offset-md-1">
                {!! $explore->explore_description !!}
                <div class="video-wrapper video-thumb">
                    <p>
                        @php
                            $ext = pathinfo($explore->explore_video, PATHINFO_EXTENSION);
                        @endphp
                        <video class="video" id="exp_video" poster="{{ asset('pages/homepage/'.$explore->explore_video_image ) }}" width="100%" height="">
                            <source src="{{ asset('pages/homepage/'.$explore->explore_video ) }}" type="video/{{$ext}}" />
                        </video>
                    </p>
                    <div class="playpause"></div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- video ends here -->
<!-- our tesm start here -->
<section class="team-section aos-item" data-aos="fade-in">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="main-heading">
                    <h5 class="curly-font green-color">Our Team</h5>
                    <h3>Coordinators</h3>
                </div>
            </div>
            <div class="col-lg-12 col-md-12 col-12">
                <div class="slider">
                    @forelse ($listOfCoordinators as $coordinator)
                        <div class="slide team-box">
                            @if(!empty($userData->user_image_path) && !empty($userData->user_image))
                            @if (file_exists(public_path($userData->user_image_path)) && $userData->user_image)
                               <img src="{{ asset($userData->user_image_path.'/'.$userData->user_image) }}" />
                            @else
                                <img src="{{ asset('users/user.png') }}" alt="team" data="{{ $coordinator->id }}" />
                            @endif
                            @endif
                            @if($coordinator->is_scout==1)
                            <h4>{{ 'Scout '.$coordinator->user_fname .' '. $coordinator->user_lname  }}
                                <span class="team-rating">
                                    @php $mainArrRa = array_column($coordinator->review_ratings->toArray(), 'rating') @endphp                                    
                                    <?php 
                                    if(!empty($mainArrRa)){
                                        $eachUpeer = array();  $eachLower = array(); $loop = 0;
                                        $mainArrRa = array_count_values($mainArrRa);
                                        foreach ($mainArrRa as $key => $val) {
                                          $eachUpeer[] = $key * $val;
                                          $eachLower[] = $val;
                                        }
                                        $ratingValSum = array_sum($eachUpeer);
                                        $ratingNumSum = array_sum($eachLower);
                                        $rating = $ratingValSum / $ratingNumSum;    
                                        ?><i class="fas fa-star text-warning" data="asxsax"> </i>
                                        <?php echo $rating;
                                    }else{ ?>
                                    <i class="far fa-star text-warning" data="=huiii"> </i>( 0 )<?php 
                                    } ?>
                                </span>
                            </h4>
                            @else
                             <h4>{{$coordinator->user_fname .' '. $coordinator->user_lname  }}</h4>
                             @endif
                            <p>{{\Str::limit($coordinator->about_me,150,'....') }}</p>
                             @if($coordinator->is_scout==1)
                            <a href="{{route('scout-detail',['id' => base64_encode($coordinator->id)])}}" class="btn btn-success green-bg">Read More</a>
                            @else
                             <a href="{{route('coordinator-detail',['id' => base64_encode($coordinator->id)])}}" class="btn btn-success green-bg">Read More</a>
                             @endif
                        </div>

                    @empty

                    @endforelse
                </div>
            </div>
        </div>
    </div>
</section>
<!-- our tesm ends here -->
<!-- scouts start here -->
<section class="scouts aos-item" data-aos="fade-up">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="main-heading">
                    <h5 class="curly-font green-color">Locals From Zero</h5>
                    <h3>Meet Our Scouts</h3>
                </div>
            </div>
            <div class="dynamic-scout-list d-flex flex-wrap">
             @include('front_end.scout-list')

         </div>
          <div class="loader scout-loader" style="display: none;"><div class="inner_loader"></div></div>
        </div>
    </div>
</section>
<!-- scouts ends here -->
<!-- TESTIMONIALS -->
<section class="testimonials aos-item" data-aos="fade-in">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="main-heading">
                    <h5 class="curly-font green-color">What Travellers Say</h5>
                    <h3>Testimonials</h3>
                </div>
            </div>
            <div class="col-sm-12">
                <div id="customers-testimonials" class="owl-carousel">
                    <!--TESTIMONIAL 1 -->
                    @forelse ($testimonials as $test)
                    @php
                    $str_length=\Illuminate\Support\Str::length($test->description);
                    @endphp
                        <div class="item">
                            <div class="shadow-effect">
                                <div class="item-details">
                                    <h5>{{ $test->title }}</h5>
                                    <p class="minimize" data-lenght="{{$str_length}}">{{ $test->description }}
                                    </p>

                                </div>
                                @if($test->profile_image!=null)
                                    <img class="img-responsive" src="{{ asset('pages/testimonialUser/'.$test->profile_image) }}" alt="">  
                                @else
                                <img class="img-responsive" src="{{ asset('users/user.png') }}" alt="team" />
                                @endif
                                <h4 class="testimonial-name">{{ $test->person_name}}<!--span class="designation">Traveler</span--></h4>
                            </div>
                        </div>
                    @empty

                    @endforelse
                </div>
            </div>
        </div>
    </div>
</section>
<!-- END OF TESTIMONIALS -->
@endsection
 
@section('javascript')
<script src="{{ asset('front_end/js/owl.carousel.min.js') }}" type="text/javascript" charset="utf-8"></script>

    <script>
        $(document).ready(function($) {
        "use strict";
        $('#customers-testimonials').owlCarousel( {
                loop: true,
                center: true,
                items: 3,
                margin: 30,
                autoplay: true,
                dots:true,
            nav:true,
                autoplayTimeout: 8500,
                smartSpeed: 450,
            navText: ['<i class="fa fa-angle-left"></i>','<i class="fa fa-angle-right"></i>'],
                responsive: {
                    0: {
                        items: 1
                    },
                    768: {
                        items: 2
                    },
                    1170: {
                        items: 3
                    }
                }
            });
        });
    </script>
<script type="text/javascript">
    $('document').ready(function(){
        var vid = document.getElementById('exp_video');
        vid.addEventListener('ended', function(e) {
         $(".playpause").show();
          this.load();
        }, false);
        $('.home-ser').click(function(){
            window.location.href = 'http://myfileshosting.com/localsfromzero/experience-list'; 
        });
    });
</script>
<script type="text/javascript">
$(document).ready(function(){
$(document).on('click', '.pagination a', function(event){
        $('.scout-loader').css('display','flex');
        event.preventDefault();
        var page = $(this).attr('href').split('page=')[1];
        $('#hidden_page').val(page);
        $('li').removeClass('active');
        $(this).parent().addClass('active');
        getAllScoutDdata(page);
    });
});
function getAllScoutDdata(page){
    $('.filter_data').html('<div id="loading" style="" ></div>');
    $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        url: HOST_URL+"/get-scout?page="+page,
        method:"GET",
        data:{},
        success:function(data){
            $('.scout-loader').css('display','none');
            if(data.isSucceeded){
            
                $('.dynamic-scout-list').html('');
                $('.dynamic-scout-list').html(data.data);
            }else{
                $('.dynamic-scout-list').html(data.data);
            }
        }
    });
        }
        </script>

@endsection
