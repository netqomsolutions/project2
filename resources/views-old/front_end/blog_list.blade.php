@extends('layouts.app')
@section('content')
<?php $url = asset('pages/privacy/'.$pageData->page_featured_image); ?>
<section class="inner-banner blog-banner" style="background-image: url('<?php echo $url; ?>');">
	<div class="container">
	<div class="row">
		<div class="col-lg-12 col-md-12 col-12">
		<h3>Our <span class="green-color">Blog</span></h3>
						<p>{{ $pageData->page_description }}</p>
		</div>
	</div>
	</div>
	</section>

	
	<!-- featured exp start here -->
	<section class="blog-sec aos-item" data-aos="fade-down">
	<div class="container">
	<div class="row">
	<!-- exp-box start -->

	<div class="col-lg-8 col-md-8 col-sm-12">

		@forelse ($blogsData as $blog)
			<div class="single-blog">			
				<img src="{{ asset('pages/blogs/'.$blog->feature_image) }}" alt="splash-img">			
				<div class="blog-content">
					<div class="date-admin">					
						<ul>
							<li class="pink-color"><i class="fas fa-calendar-week"></i> {{ date('d M, Y', strtotime($blog->created_at))}}</li>
							<li class="pink-color"><a class="pink-color" href="#"><i class="fas fa-user"></i> By Admin</a></li>
						</ul>
					</div>
					<div class="blog-text">
						<h4>{{ $blog->blog_title }}</h4>
						<!-- {!! $blog->blog_description !!} -->
						{{ \Illuminate\Support\Str::limit(strip_tags($blog->blog_description),300,'...') }}
						<a href="{{ route('blog-detail',['id' => $blog->id]) }}" class="btn btn-success green-bg rounded-btn">Read More</a>
					</div>
				</div>
			</div>
			<!-- single blog ends here -->
		@empty
		   <div class="not-found"><p>
                Data not found.
                </p></div>
		@endforelse
	</div>

	<div class="col-lg-4 col-md-4 col-sm-12">
      
		<div class="right-search-box">
			 
			<input type="texe" name="search" id="search-text" required="required" placeholder="Search...">
			<button type="button" data-url="/{{ Request::path() }}" class="btn serach-blog-btn btn-danger pink-bg"><i class="fas fa-search"></i></button>
			  
		</div>

		<div class="blog-categories">
			<h3>Categories</h3>
			<ul>
				@forelse ($blogCategory as $blogCat)
				    <li><a href="?cat={{ $blogCat->slug }}" class="green-color">{{ $blogCat->name }}</a></li>
				@empty
				@endforelse
			</ul>
		</div>
	</div>
	</div>
	</div>
	</section>
	@endsection
@section('javascript')

<script type="text/javascript">
	
	$(".serach-blog-btn").click(function(){
	  var name = $("#search-text").attr("name");
	  var searchtext = $("#search-text").val();
	  var cUrl = $(this).attr("data-url");
	  var url=window.location.href;
	  var res=updateQueryStringParameter(url,name,searchtext);
	  var newurl = res;
        window.history.pushState({
            path: newurl
        }, '', newurl);
        window.location.href=newurl;
 
	});
	function updateQueryStringParameter(uri, key, value) {
    var re = new RegExp("([?&])" + key + "=.*?(&|$)", "i");
    var separator = uri.indexOf('?') !== -1 ? "&" : "?";
    if (uri.match(re)) {
        return uri.replace(re, '$1' + key + "=" + value + '$2');
    } else {
        return uri + separator + key + "=" + value;
    }
}

</script>
@endsection
	<!-- featured exp ends here -->