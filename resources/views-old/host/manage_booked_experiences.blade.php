{{-- Extends layout --}}
@extends('layout.default')
{{-- Content --}}
@section('content')
<style type="text/css">
	.dataTables_wrapper .dataTable th:before {content:none !important;}
	.dataTables_wrapper .dataTable th:after {content:none !important;}
</style>
<div class="row">
	<div class="col-lg-12">
		<meta name="csrf-token" content="{{ csrf_token() }}" />
		<!--begin::Card-->
		@if (session('success'))
		<div class="alert alert-success scout-list-success">
			{{ session('success') }}
		</div>
		@elseif(session('failure'))
		<div class="alert alert-danger scout-list-success">
			{{ session('failure') }}
		</div>			    
		@endif
		<script type="text/javascript">setTimeout(function(){ $('.scout-list-success').fadeOut(); }, 3000);</script>
		<div class="card card-custom">
			<div class="card-header">
				<div class="card-title w-100">
					<span class="card-icon">
						<i class="flaticon2-supermarket text-primary"></i>
					</span>
					<div class="list_sec">
						<h3 class="card-label">Booked Experiences</h3>
					</div>
<!-- 					<div class="add-btn">
						<a id="" href="{{route('scout-add-host')}}" class="btn btn-sm float-right green-bg">Add New <i class="fa fa-plus" aria-hidden="true"></i></a>
					</div> -->
				</div>
			</div>
			<div class="card-body">
				<div class="row">
						@forelse($experiences as $experience)
						@php
							$exp_name = '';
							$exp_high_price = '';
							$exp_low_price = '';
							$exp_price_valid = '';
							$exp_duration = '';
							$exp_group_size = '';
							$exp_lname = '';
							$exp_fea_img = '';

							if(optional($experience->experience)){

								$exp_name = optional($experience->experience)->experience_name;

								$exp_high_price = optional($experience->experience)->experience_high_price;

								$exp_low_price = optional($experience->experience)->experience_low_price;

								$exp_price_valid = optional($experience->experience)->experience_price_vailid_for;

								$exp_duration = optional($experience->experience)->experience_duration;

								$exp_group_size = optional($experience->experience)->experience_group_size;
								
								$exp_lname = optional($experience->experience)->experience_lname;

								$exp_fea_img = optional($experience->experience)->experience_feature_image;
							}
						@endphp
						<div class="col-lg-4 col-md-4 col-sm-12 wishList-main blur-img-cls">
							<div class="exp-box">
								<div class="exp-img">
									<a href="{{ route('experience-detail',base64_encode($experience->experience_id)) }}"><img src="{{asset('pages/experiences/'.$exp_fea_img)}}">
										<div class="manage-book-info">
											<div class="title">
										<span>{{ $exp_name }}</span>
									</div>
									<div class="rating-price">
										<div class="rating">
											<ul mk="4">
												{!! getExperienceReview($experience->id,'ratings') !!}
<!-- 												<i class="fas fa-star text-warning" aria-hidden="true"> </i>
												<i class="fas fa-star text-warning" aria-hidden="true"> </i>
												<i class="fas fa-star text-warning" aria-hidden="true"> </i>
												<i class="fas fa-star text-warning" aria-hidden="true"> </i>
												<i class="far fa-star text-warning" aria-hidden="true"> </i> -->
												<li>{{ getExperienceReview($experience->id,'reviews') }} Reviews</li>
											</ul>
										</div>
									</div>
										</div>
									</a>
								</div>
								<div class="exp-content">
									<!--div class="host-info-exp">
										<h4>Booking Information</h4>
										<!-div class="host-data">
											<p><span><i class="fas fa-male"></i> Adult: <b>{{$experience->adults}}</b></span>
											<span><i class="fas fa-child"></i> Children: <b>{{$experience->children}}</b></span>
											<span><i class="fas fa-baby"></i> Infants: <b>{{$experience->infants}}</b></span><br>
											<span><i class="fas fa-euro-sign"></i> Booked Price: <b>€{{$experience->amount}}</b></span><br>
											<span><i class="far fa-calendar-alt"></i> Booked Date:<b>  {{date('M-d-Y',strtotime($experience->booking_date))}}</b></span><br>
											<span><i class="far fa-clock"></i> Booked Time:<b> {{date('H:i A',strtotime($experience->booking_start_time))}}-{{date('H:i A',strtotime($experience->booking_end_time))}}</b></span>
											</p>
										</div>
									</div-->
									<div class="host-info-exp">
										<div class="heading-host-data d-flex align-items-center justify-content-between">
										<h4>Booking Information</h4>
											<a href="{{route('host-booked-experience-detail',['id'=>base64_encode($experience->id)])}}" class="view-host-more" title="View More Details"><i class="la la-eye"></i></a>
										</div>
										<div class="host-data">
											<div class="travler-info d-flex align-items-start position-relative">
                                                	@if ($experience->traveler->user_image_path && file_exists(public_path($experience->traveler->user_image_path)) && $experience->traveler->user_image)
                                                <img src="{{ asset($experience->traveler->user_image_path.'/'.$experience->traveler->user_image) }}" alt="Admin" class="" />
                                                @else
                                                <img src="{{ asset('users/user.png') }}" alt="Admin" class=""  />
                                                @endif
                                                <p class="m-0 text-dark-75 font-weight-bolder mb-1 font-size-lg">
												 
												{{$experience->traveler->user_fname.' '.$experience->traveler->user_lname}}
												<span class="font-weight-normal d-block p-0 mt-2"><i class="la la-map-marker"></i> {{$experience->traveler->user_address}}</span>
												
												<span class="font-weight-normal d-block p-0"><i class="la la-phone"></i> {{$experience->traveler->user_mobile}}</span>
												<span class="font-weight-normal d-block p-0"><i class="la la-envelope"></i> {{$experience->traveler->email}}</span>
                                         <!--a id="wishlist-show" href="{{route('scout-dashboard',['scout_id'=>$experience->user_id]) }}" data-default="wishlist-span" class="chat-host" title="Chat Now">
												<i class="flaticon-chat-1 text-muted font-weight-bold"></i>
											</a---> 
										
												</p>
											</div>
										</div>
									</div>
									

									
								</div>
							</div>
						</div>
						@empty
						<div class="exp-box no-data-found no-data-backend" style="text-align: center;font-weight: 500;">
							<img src="{{asset('images/no-data-found.png')}}">
            				<h3>No Record Found.</h3>
       					 </div>
						@endforelse
				</div>
			</div>
		</div>
		<!--end::Card-->
	</div>
</div>
@endsection
