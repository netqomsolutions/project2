{{-- Extends layout --}}
@extends('layout.default')
{{-- Content --}}
<style type="text/css">
#second_step{
    display:none;
}
.my-error{
    color: red;
}
#calendar {
  max-width: 900px;
  margin: 40px auto;
}
/*div#calendar table.fc-col-header {
    width: 100% !important;
    height: auto !important;
}
#calendar .fc-view-harness.fc-view-harness-active {
    height: auto !important;
}
#calendar.fc .fc-view-harness-active > .fc-view, #calendar.fc .fc-scroller-liquid-absolute {
    position: relative;
}
#calendar .fc-view-harness.fc-view-harness-active .fc-daygrid-body.fc-daygrid-body-unbalanced {
    width: 100% !important;
}
#calendar .fc-view-harness.fc-view-harness-active .fc-daygrid-body.fc-daygrid-body-unbalanced table.fc-scrollgrid-sync-table {
    width: 100% !important;
    height: auto !important;
}*/
</style>
@section('content')
@php 

    $selectedCategory=[];
    $selectedFeature=[];
    $selectedAddonImages=[];
    $selectedSchedule=[];
    $tooltipArray=[];
    if(isset($expData))
    {
     
        $tooltipArray = array(
            "1"=>array(
                "price_type_tooltip"  => 'Select this option only if a minimum number of people per experience is required and is bigger than 1. (e. g. 2, 3, 4 ,5…). This option will allow you to set only a fixed price for any additional person that exceeds a minimum number of people.',
                "price_type_tooltip_max"  => 'Set a maximum number of participants (usually no more than 10).',
                "price_type_tooltip_min"  => 'Set a minimum number of participants greater than 1.',
                "minimum_price"  => 'Set a minimum price that will apply to a minimum number of people you specified for this experience (e.g. if a minimum number of people for this experience is 3, set a minimum price for 3 persons).',
                "maximum_price"=>'Just confirm here that your listed price applies for the minimum number of people you specified',
                "price_valid"=>'Just confirm here that your listed price applies for a minimum number of people you specified (select the appropriate range)',
                "additional_person"=>'Set a fixed price for any additional person that exceeds your minimum number of people',
            ),
            "2"=>array(
                "price_type_tooltip"  => 'Select this option only if a minimum number of people per experience is equal to 1. This option will allow you to set only a fixed price for any additional person that exceeds a minimum number of people.',
                "price_type_tooltip_max"  => 'Set a maximum number of participants (usually no more than 10).',
                "price_type_tooltip_min"  => 'The number can only be 1. It was pre-filled automatically',
                  "maximum_price"=>'Just confirm here that your listed price applies for the minimum number of people you specified',
                "minimum_price" =>'Set a price for 1 person',
                "price_valid"=>'',
                "additional_person"=>'Set a fixed price for any additional person',
            ),
            "3"=>array(
                "price_type_tooltip"=>'Select this option only if your pricing is dynamic (e.g. the price decreases with any additional person). This scenario applies to both options: whether you require a minimum number of people or not.',
                "price_type_tooltip_max"  => 'Set a maximum number of participants (usually no more than 10).',
                "price_type_tooltip_min"  => 'It can be any number.',
                "maximum_price"=>'Just confirm here that your listed price applies for the minimum number of people you specified',
                "minimum_price"=>'Set a minimum price that will apply to the minimum number of people you specified for this experience (e.g. if a minimum number of people for this experience is 1, set a minimum price for 3 person, if a minimum number of people for this experience is 1, set a minimum price for 3 person).',
                "price_valid"=>'Just confirm here that your listed price applies for the minimum number of people you specified',
                "additional_person"=>'Set a variable price for any additional person',
            ), 

        );
    if(!empty($expData['experience_category_relation']))
    {
        foreach($expData['experience_category_relation'] as $cat)
        {
            $selectedCategory[]=$cat['experience_category_id'];
        }
    }
    if(!empty($expData['experience_feature_relations']))
    {  
        foreach($expData['experience_feature_relations'] as $key=>$feat)
        {
            $selectedFeature[]=$feat['experience_feature_id'];
        }
    }
    if(!empty($expData['experience_images']))
    {
        foreach($expData['experience_images'] as $image)
        {
            $selectedAddonImages[]=array(
            "id"=>$image['id'],
            "src"=>asset('pages/experiences/'.$image['image_name']),
            );
        }
    }
    if(!empty($expData['experience_schedule']))
    {
        foreach($expData['experience_schedule'] as $schedule)
        {
            if($schedule->schedule_type==1)
            {
                $endtime= $schedule['end_date'];
                $starttime=$schedule['start_date'];
            }
            else{
                $endtime   =$schedule['end_date'].'T'.$schedule['end_time'];
                $starttime =$schedule['start_date'].'T'.$schedule['start_time'];
            }
                $selectedSchedule[]=array(
                    "id"           =>$schedule['id'],
                    "exp_id"       =>$expData['id'],
                    "sche_type"    =>$schedule['schedule_type'],
                    "title"        => $expData->experience_name,
                    "start"        =>$starttime,
                    "end"          =>$endtime,
                    "className"    =>['selected-eventCls selected-event-'.$schedule['id']]
                );
        }
      
    }
    }
    $selectedAddonImages=json_encode($selectedAddonImages,JSON_UNESCAPED_SLASHES);
    $selectedSchedule=json_encode($selectedSchedule,JSON_UNESCAPED_SLASHES);
@endphp
<div class="row">
    <div class="col-lg-12">
        <div class="card card-custom gutter-b example example-compact">
            <div class="card-header">
                <div class="card-title w-100">
					<span class="card-icon"><i class="fas fa-plus-circle"></i></span> 
					<div class="list_sec">
					<h3 class="card-label">Add Experience</h3>
					</div>
				</div>
                <div class="card-toolbar" style="display: none;">
                    <div class="example-tools justify-content-center">
                        <span class="example-toggle" data-toggle="tooltip" title="View code"></span>
                        <span class="example-copy" data-toggle="tooltip" title="Copy code"></span>
                    </div>
                </div>
            </div>
           
           <div class="card-body">
             <!--begin::Form-->
            <form class="form" name="add-exp-form" enctype="multipart/form-data" id="add-exp" method="post" action="">
                @csrf
                 <input type="hidden" name="exp_id" id="experience_id" value="{{(isset($expData))? $expData->id :0}}"/>
                <!--begin::Accordion-->
                <div class="accordion accordion-solid accordion-toggle-plus" id="accordionExample6">
                    <div class="card">
                        <div class="card-header" id="headingOne6">
                            <div class="card-title" id="basic_detail" data-toggle="collapse" data-target="#basic-deatil">
                            <i class="far fa-list-alt"></i>Basic Details</div>
                        </div>
                        <div id="basic-deatil" class="collapse show" data-parent="#accordionExample6">
                            <div class="card-body">
                                <div class="form-group row">
                                     <div class="col-lg-12 mt-5">
                                        <label class="font-weight-bold">Assign Host</label>
                                        <p>If you haven't already, you must first add a provider (host) for this experience before filling in this field (go to
                                        the <b>Manage host</b> section in the left menu and click --> <b>Add host</b> ). Once you have done this, select the appropriate provider from the
                                        drop-down menu below</p>
                                        <select class="form-control" name="data[experiences][assigned_host]">
                                            <option value="">Select Host</option>
                                            @forelse ($hosts as $host)
                                                <option value="{{ $host->id }}" {{(isset($expData) && $expData['assigned_host'] == $host->id) ? 'selected' : ''}}>{{ $host->user_fname.' '.$host->user_lname }}</option>
                                            @empty

                                            @endforelse
                                        </select>
                                    </div>
                                    <div class="col-lg-12  mt-5">
                                    <label class="font-weight-bold">Category</label>
                                    <p>Choose up to two categories that best match this experience</p>
                                    <select class="form-control exp_multi ignore" id="exp_category" multiple='multiple' size="2" name="data[experience_category_relation][experience_category_id][]">
                                    @forelse ($experienceCategories as $experienceCategory)
                                        <option value="{{ $experienceCategory->id }}" {{(isset($expData) && in_array($experienceCategory->id,$selectedCategory)? 'selected':'')}}>{{ $experienceCategory->name }}</option>
                                    @empty

                                    @endforelse
                                </select>
                                <span id="exp_category-error" style="display:none" class="form-group">Please select your experience category</span>
                            </div>

                            <div class="col-lg-12 mt-5">
                                <label class="font-weight-bold">What is the name of the Experience?</label>
                                <p>It should be short, descriptive, and appealing</p>
                                <input type="text" name="data[experiences][experience_name]" id="experience_name" class="form-control" value="{{(isset($expData)) ? $expData->experience_name : ''}}" placeholder="Enter experience name" />
                            </div>
                             <div class="col-lg-12 mt-5 word_sec">
                                <label class="font-weight-bold">Describe the experience</label>
                                <p>In the experience description, you have a chance to excite guests about the experience.<br>
                                    Tell them what they will do during the experience. Don’t forget to emphasize the uniqueness that sets your
                                provider apart from others.<br>
                                Be detailed in your description, so guests will know what to expect.<br>
                                At the end tell guests what you want them to gain by attending your experience. Knowledge? Friendship? An
                                authentic look at traditions and culture? End with a strong selling point.</p>
                                <textarea name="data[experiences][experience_description]" onkeyup="showWordCount(this)" rows=5 class="form-control" placeholder="Enter experience description" >{{(isset($expData)) ? $expData->experience_description : ''}}</textarea>
                                 <span class="word-limit-text ">
                                    <span data-minneed="200" data-maxneed="500" data-getCount="" class="counter">0</span>/500 <b>(200 needed)</b>
                                </span>
                            </div>
                           
                            <div class="col-lg-6 mt-5">
                                <label class="font-weight-bold">Pricing strategies</label>
                                <i class="fa fa-info-circle price_type_tooltip" data-toggle="tooltip" data-placement="top" title="{{(isset($expData))? $tooltipArray[$expData->experience_type]['price_type_tooltip'] : 'Please Select Price Scenario'}}" aria-hidden="true"></i>
                                <select class="form-control changeExpType" name="data[experiences][experience_type]" id="exp_price_type">
                                    <option value="">Select experience type</option>
                                    <option value="1" {{(isset($expData)&& $expData['experience_type'] == 1 )? 'selected' : ''}}>PRICING SCENARIO 1</option>
                                    <option value="2" {{(isset($expData)&& $expData['experience_type'] == 2 )? 'selected' : ''}}>PRICING SCENARIO 2</option>
                                    <option value="3" {{(isset($expData)&& $expData['experience_type'] == 3 )? 'selected' : ''}}>PRICING SCENARIO 3</option>
                                </select>
                            </div>
                             <div class="col-lg-6 mt-5" id="" style="">
                                <label class="font-weight-bold">Min number of participants</label>
                                <i class="fa fa-info-circle price_type_tooltip" data-toggle="tooltip" data-placement="top" title="What is the minimum number of participants that should book the activity so that it takes place? We strive that this number is as low as possible." aria-hidden="true"></i>
                                <input type="number" name="data[experiences][minimum_participant]" class="form-control" id="minimum_participant"  value="{{(isset($expData)) ? $expData->minimum_participant : ''}}" min="" placeholder="Write in numbers eg. 3" />
                            </div>
                            <div class="col-lg-6  mt-5" id="" style="">
                                <label class="font-weight-bold">Max number of participants</label>
                                 <i class="fa fa-info-circle price_type_tooltip_max" data-toggle="tooltip" data-placement="top" title="Tell us what is the maximum number of participants who can participate in the experience.We recommend this number is not higher than 10." aria-hidden="true"></i>
                                <input type="number" min=1 name="data[experiences][maximum_participant]" class="form-control" value="{{(isset($expData)) ? $expData->maximum_participant : ''}}" id="maximum_participant" placeholder="Write in numbers eg. 3" />
                            </div>
                              <div class="col-lg-6 mt-5">
                                <label class="font-weight-bold">Minimum price (In Euro)</label>
                                <i class="fa fa-info-circle minimum_price" data-toggle="tooltip" data-placement="top" title="{{(isset($expData))? $tooltipArray[$expData->experience_type]['minimum_price'] : ''}}" aria-hidden="false"></i>
                                <input type="number" min=1 name="data[experiences][experience_low_price]" class="form-control" value="{{(isset($expData)) ? $expData->experience_low_price : ''}}" placeholder="Enter experience low price" id="minimum_price_input"/>
                            </div>

                                           
                           
                            <div class="col-lg-6 mt-5" id="high-price" style="display:{{(isset($expData) && $expData->experience_type==3)? 'block' :'none'}}">
                                <label class="font-weight-bold">Maximum price (In Euro)</label>
                                  <i class="fa fa-info-circle maximum_price" data-toggle="tooltip" data-placement="top" title="{{(isset($expData))? $tooltipArray[$expData->experience_type]['maximum_price'] : 'Set the price for the maximum number of participants you specified above.'}}" aria-hidden="false"></i>
                                <input type="number"  name="data[experiences][experience_high_price]" class="form-control" value="{{(isset($expData)) ? $expData->experience_high_price : ''}}" placeholder="Enter experience high price" id="maximum_price_input"/>
                            </div>
                            <div class="col-lg-6 mt-5" id="price-valid-div" style="display:{{(isset($expData) && $expData->experience_type==2)? 'none' :'block'}}">
                                <label class="font-weight-bold">Price valid for (No. of participants)</label>
                                <i class="fa fa-info-circle price_valid" data-toggle="tooltip" data-placement="top" title="{{(isset($expData))? $tooltipArray[$expData->experience_type]['price_valid'] : ''}}" aria-hidden="false"></i>
                                 <input type="number" min=1 name="data[experiences][experience_price_vailid_for]" id="experience_price_vailid_for" class="form-control" value="{{(isset($expData)) ? $expData->experience_price_vailid_for : ''}}" placeholder="Enter Price Valid For" />
                            </div>  
                            <div class="col-lg-6 mt-5" id="exp_additional_person_div" style="display:{{(isset($expData) && $expData->experience_type==3)? 'none' :'block'}}">
                                <label class="font-weight-bold">Any additional participant price (In Euro)</label>
                                <i class="fa fa-info-circle additional_person" data-toggle="tooltip" data-placement="top" title="{{(isset($expData))? $tooltipArray[$expData->experience_type]['additional_person'] : ''}}" aria-hidden="true"></i>
                                <input type="number" name="data[experiences][experience_additional_person]" value="{{(isset($expData)) ? $expData->experience_additional_person : ''}}" class="form-control" id="experience_additional_person" placeholder="" min=1 />
                            </div>
                            <div class="additional-cost-price col-lg-6 mt-5" id="additional-cost-price" style="display:{{(isset($expData) && $expData->experience_type==3)? 'block' :'none'}}">
                                <label class="font-weight-bold">Additional participant cost / price</label>
                                <div class="add-cost-price-inner">
                                    <table>
                                        <thead>
                                            <tr>
                                                <th></th>
                                                <th>Minimum Participant</th>
                                                <th>Price</th>
                                            </tr>
                                        </thead>
                                        <tbody class="additional-price-detail">
                                             @php
                                             $i=0;
                                            if(isset($expData)){
                                            $i=$expData->experience_price_vailid_for;
                                            }
                                            @endphp
                                            @if(isset($expData))
                                            @forelse ( $expData->experience_additional_price as $key=>$addPrice) 

                                            <tr>
                                                <td>upto</td>
                                                <td><input type="number" max="{{$i}}" min="{{$i}}" name="data[experience_additional_price][additional_person][{{$key}}]" id="additional-person-1" value="{{$addPrice['additional_person']}}" readonly /></td>
                                                <td><input type="number" name="data[experience_additional_price][additional_price][{{$key}}]" id="additional-price-1" value="{{$addPrice['additional_price']}}" readonly /></td>
                                            </tr>
                                             @php
                                            $i=++$i;
                                            @endphp
                                            @empty
                                            @endforelse
                                            @endif
                                            
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="child-discount col-lg-6 mt-5">
								<div class="child-dis-check">
                                <input type="checkbox" name="data[additional_detail][is_children_discount_active]" id="dicount-checkbox" value="1" {{(isset($expData) && $expData->experience_meta_detail->is_children_discount_active==1) ? 'checked': ''}}  />
                                <label>Child Discount</label>
								</div>
                                <div class="children-discount-div" style="display:{{(isset($expData) && $expData->experience_meta_detail->is_children_discount_active==1) ? 'block': 'none'}};">
                                <div class="children-upto">
                                    <label>Children up to</label>
                                    <div class="years-free">
                                       <input type="number" name="data[additional_detail][children_age_for_free]" value="{{(isset($expData)) ? $expData->experience_meta_detail->children_age_for_free : 0}}">
                                        years free
                                    </div>
                                </div>
                                <div class="children-upto">
                                    <div class="years-free">
                                      <input type="number" name="data[additional_detail][children_age_for_discount]" value="{{(isset($expData)) ? $expData->experience_meta_detail->children_age_for_discount : 0}}">
                                        years
                                    </div>
                                    <div class="years-free">
                                         <input type="number" name="data[additional_detail][children_discount_1]" value="{{(isset($expData)) ? $expData->experience_meta_detail->children_discount_1 : 0}}">
                                        % discount
                                    </div>
                                </div>
                            </div>
                            </div>
                             <div class="col-lg-12  mt-5">
                                <label class="font-weight-bold">Traveler/Group Type</label>
                                <select class="form-control exp_multi" id="exp_feature" name="data[experience_feature_relations][experience_feature_id][]" multiple=multiple>
                                    @forelse ($experienceFeatures as $experienceFeature)
                                        <option value="{{ $experienceFeature->id }}" {{(isset($expData) && in_array($experienceFeature->id,$selectedFeature)? 'selected':'')}}>{{ $experienceFeature->name }}</option>
                                    @empty

                                    @endforelse
                                </select>
                                <span id="exp_feature-error" style="display:none" class="form-group">Please select Traveler/Group Type</span>
                            </div>  
                             <div class="col-lg-12  mt-5">
                                <label class="font-weight-bold">Book in advance (in hours)</label>
                                <p>The guests need to book this experience at least (hours) in advance</p>
                                <input type="number" min=1 name="data[experiences][experience_booking_in_advance_time]" value="{{(isset($expData)) ? $expData->experience_booking_in_advance_time : ''}}" class="form-control" placeholder="Enter experience booking advance time" />
                            </div> 
                                </div>
                            <div class="form-group row">
                            <div class="col-lg-12 ">
                                <p class="text-right">
                                    <a class="btn btn-primary next" data-next="second_step">Next</a>
                                </p>
                            </div>
                        </div>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                    <div class="card-header" id="headingTwo6">
                        <div class="card-title collapsed" id="additional_detail" data-toggle="{{(isset($expData))? "collapse" :" "}}" data-target="#additional-information">
                        <i class="far fa-plus-square"></i>Additional Details</div>
                    </div>
                    <div id="additional-information" class="collapse"  data-parent="#accordionExample6">
                        <div class="card-body">
                            <div class="form-group row">
                                <div class="col-lg-12 mt-5">
                                <label class="font-weight-bold">More information about a provider</label>
                                <p>Present your provider as humanly as possible, like you would present your friend. Tell guests why they are suitable to host this activity. Are they an expert in this field, or are they passionate about their experience.You can also tell the story of how you met or why you are as a Scout helping them.</p>
                                <textarea name="data[additional_detail][provider_description]" class="form-control" placeholder="Who is the experience provider? How are you helping them?">{{(isset($expData)) ? $expData->experience_meta_detail->provider_description : ''}}</textarea>
                                </div>
                                <div class="col-lg-12 mt-5">
                                <label class="font-weight-bold">More information about the experience</label>
                                <p>Is there anything guests should know beforehand? How should they be dressed? Should they bring any special equipment? Their lunch? Think about every aspect of the experience and how you can provide additional information.</p>
                                <textarea name="data[additional_detail][exp_additional_information]" class="form-control" placeholder="Type in your response">{{(isset($expData)) ? $expData->experience_meta_detail->exp_additional_information : ''}}</textarea>
                                </div> 
                                <div class="col-lg-12 mt-5">
                                <label class="font-weight-bold">What is included in the price</label>
                                <p>Name the activities and things that are included in the price. Eg. horse riding, 3 samples of wine, tour guide,etc.</p>
                                <textarea name="data[additional_detail][exp_price_included]" class="form-control" placeholder="Type in your response">{{(isset($expData)) ? $expData->experience_meta_detail->exp_price_included : ''}}</textarea>
                                </div>
                                <div class="col-lg-12  mt-5">
                                <label class="font-weight-bold">What is excluded from the price</label>
                                <p>Make sure your guests do not end up disappointed. Point out the activities that are not included in the price,if that is not made clear enough in the description. Eg. Price does not include bottled wine.</p>
                                <textarea name="data[additional_detail][exp_price_excluded]" class="form-control" placeholder="Type in your response">{{(isset($expData)) ? $expData->experience_meta_detail->exp_price_excluded : ''}}</textarea>
                                </div> 
                                 <div class="col-lg-12 mt-5">
                                <label class="font-weight-bold">Other</label>
                                <p>Anything else you wish to add? Does your experience include transportation or a third party provider?</p>
                                <textarea name="data[additional_detail][exp_other_info]" class="form-control" placeholder="Anything else we should know about?">{{(isset($expData)) ? $expData->experience_meta_detail->exp_other_info : ''}}</textarea>
                                </div>  
                                <div class="col-lg-12 mt-5"> <!-- Date:30-01-2021 @RT add new Field  meet_location -->
                                  <label class="font-weight-bold">Exact location to meet</label> 
                                  <input type="text"   name="data[additional_detail][meet_location]" value="{{(isset($expData)) ?  $expData->experience_meta_detail->meet_location : ''}}" class="form-control" placeholder="Enter exact location to meet" />
                                </div> 
                            <div class="col-lg-6 mt-5">
                                <label class="font-weight-bold">Duration (in hours)</label>
                                <input type="text"  name="data[experiences][experience_duration]" value="{{(isset($expData)) ? $expData->experience_duration : ''}}" class="form-control" placeholder="Enter experience duration" />
                            </div>
                            <div class="col-lg-6 mt-5">
                                <label class="font-weight-bold">Experience start time</label>
                                <select class="form-control" name="data[experiences][experience_start_time]">
                                    <option value="">Select start time</option>
                                    <option value="1" {{(isset($expData)&& $expData['experience_start_time'] == 1 )? 'selected' : ''}}>Morning ( 01:00 - 11:00 )</option>
                                    <option value="2" {{(isset($expData)&& $expData['experience_start_time'] == 2 )? 'selected' : ''}}>Afternoon ( 12:00 - 18:00 )</option>
                                    <option value="3" {{(isset($expData)&& $expData['experience_start_time'] == 3 )? 'selected' : ''}}>Evening ( 19:00 - 00:00 )</option>
                                </select>
                            </div>                             
                              
                        
                            </div>
                                <div class="form-group row">
                              <div class="col-lg-12 ">
                                <p class="text-right">
                                     <a class="btn btn-secondary previous" data-prev="first_step" >Previous</a>
                                    <a class="btn btn-primary next" data-next="second_step">Next</a>
                                </p>
                            </div>
                        </div>
                            </div>
                            
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingTwo6">
                            <div class="card-title collapsed" id="location_detail" data-toggle="{{(isset($expData))? "collapse" :" "}}" data-target="#location-details">
                            <i class="fas fa-map-marker-alt"></i>Location/Details</div>
                        </div>
                        <div id="location-details" class="collapse" data-parent="#accordionExample6">
                            <div class="card-body">
                        <div class="form-group row">
                            <div class="col-lg-6 mt-5">
                                <label class="font-weight-bold">Location</label>
                                <input type="text"  name="data[experiences][experience_lname]" class="form-control" id="exp-location" value="{{(isset($expData)) ? $expData->experience_lname : ''}}" placeholder="Enter experience location name" />
                                <input type="hidden" value="{{(isset($expData)) ? $expData->experience_longitude : ''}}" name="data[experiences][experience_longitude]" class="form-control" id="exp-longitude" />
                                <input type="hidden" value="{{(isset($expData)) ? $expData->experience_latitude : ''}}"  name="data[experiences][experience_latitude]" class="form-control" id="exp-latitude" />
                            </div>
                            
                                </div>
                                    <div class="form-group row">
                              <div class="col-lg-12">
                                <p class="text-right">
                                     <a class="btn btn-secondary previous" data-prev="first_step" >Previous</a>
                                     
                                    <a class="btn btn-primary next" data-next="second_step">Next</a>
                                   
                                </p>
                            </div>
                        </div>
                        </div>
                        </div>
                    </div>
                   
                    <div class="card">
                         <div class="loader experience-loader" style="display: none;"><div class="inner_loader"></div></div>
                        <div class="card-header" id="headingThree6">
                            <div class="card-title collapsed" id="images_detail" data-toggle="{{(isset($expData))? "collapse" :" "}}" data-target="#experience-images">
                            <i class="far fa-image"></i>Images</div>
                        </div>
                        <div id="experience-images" class="collapse" data-parent="#accordionExample6">
                            <div class="card-body">
                                <div class="form-group row">
                                    <div class="col-lg-4 col-md-4 col-sm-12">
                                     <label class="col-lg-12 font-weight-bold p-0">Feature Image</label>

                                    <div class="col-lg-12 mt-8 p-0 pr-5">
                                            @if (isset($expData) && file_exists(public_path('pages/experiences/'.$expData->experience_feature_image)))
                                            @php 
                                            $image=asset('pages/experiences/'.$expData->experience_feature_image);
                                            @endphp
                                            @else
                                             @php 
                                            $image="";
                                            @endphp
                                            @endif 
                                        <div class="image-input image-input-empty image-input-outline w-100 mb-3 {{(!empty($image))? 'icon-before-hide' : ''}}" id="kt_image_5" style="background-image: url({{(!empty($image))? $image : ""}})">
                                            <div class="image-input-wrapper w-100"></div>
                                            <label class="btn btn-xs btn-icon btn-circle btn-white btn-hover-text-primary btn-shadow feature-image-label" data-action="change" data-toggle="tooltip" title="" data-original-title="Change avatar">
                                                <i class="fa fa-pen icon-sm text-muted"></i>
                                                <input type="file" id="experience_feature_image" name="data[experiences][experience_feature_image]"/>
                                            </label>
                                          
                                            <span class="btn btn-xs btn-icon btn-circle btn-white btn-hover-text-primary btn-shadow" data-action="cancel" data-toggle="tooltip" title="Cancel avatar">
                                                <i class="ki ki-bold-close icon-xs text-muted"></i>
                                            </span>
                                            <span class="btn btn-xs btn-icon btn-circle btn-white btn-hover-text-primary btn-shadow" data-action="remove" data-toggle="tooltip" title="Remove avatar">
                                                <i class="ki ki-bold-close icon-xs text-muted"></i>
                                            </span>
                                        </div>
                                         <span id="experience_feature_image-error" class="form-group" style="display:none;">Please select feature image first</span>
                                        <p>Note: Please upload 365 x 140 Image</p>
                                    </div>
                                    </div>
                                    <div class="col-lg-8 col-md-8 col-sm-12">
                                         <label class="col-lg-12 font-weight-bold p-0">Addon Images</label>
                                <p class="col-lg-12 mt-8 p-0">Add at least 4 good quality photos. This means no filters, no dark photos, and no blurry photos. Pictures should present the provider, the experience, the location the experience is taking place in, and guests engaging in the experience itself.</p>
                                <div class="col-lg-12">
                                  <div class="input-images-1" style="padding-top: .5rem;">
                                  </div>
                                  <p>Note: Please upload 985 x 530 Image</p>
                                </div>
                                    </div>
                            </div>
                            <div class="form-group row">
                               
                            </div>
                                <div class="form-group row">
                              <div class="col-lg-12">
                                <p class="text-right">
                                     <a class="btn btn-secondary previous" data-prev="first_step" >Previous</a>
                                     <button type="button" id="add-exp-button" class="btn btn-primary">Next</button>
                                </p>
                            </div>

                        </div>
                        </div>
                    </div>
                </div>
                 <div class="card">
                        <div class="card-header" id="headingTwo6">
                            <div class="card-title collapsed" id="schedule_detail" data-toggle="{{(isset($expData))? "collapse" :" "}}" data-target="#Schedule">
                            <i class="far fa-calendar-alt"></i>Schedule</div>
                        </div>
                        <div id="Schedule" class="collapse" data-parent="#accordionExample6">
                            <div class="card-body">
                                <link href="{{ asset('calendar/main.css') }}" rel="stylesheet"/>
                                <div class="form-group row">
                                    <div class="col-lg-12">
                                        <div id='calendar'></div>
                                    </div>
                                </div>
                                 <div class="form-group row">
                              <div class="col-lg-6 ">
                                <p>
                                     <a class="btn btn-secondary previous" data-prev="first_step" >Previous</a>
                                </p>
                            </div>
                        </div>
                            </div>
                        </div>
                    </div>
                
                    </div>
                <!--end::Accordion-->
            </form>
             <!--end::Form-->                                 
            </div>
             
        </div>
    </div>
</div>
<div class="modal fade add-scout-cls schedule-modal" id="calendarModal">
  <div class="modal-dialog">
    <form id="create-update-schedule-form" name="create-update-schedule-form" action="" method="post" enctype="multipart/form-data">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Set Experience Sechdule</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>

      <!-- Modal body -->
        <div class="modal-body">
        @csrf
        <input type="hidden" name="experience_id" id="sechdule_exp_id" value="{{(isset($expData))? $expData->id :0}}">
            <div class="form-group">
                <label class="">Schedule Type</label>
                <select name="schedule_type" id="schedule_type">
                    <option value="1">Upon Agreement</option>
                    <option value="2">Recurring slots</option>
                    <option value="3">Only specific dates and hours</option>
                </select>
            </div>

            <div class="d-flex flex-wrap">
                <div class="form-group col-md-6 col-sm-12 pl-0" id="start_date_div" style="">
                    <label class="">Start Date </label>
                    <div class="input-group input-group-solid date">
                        <input type="text" name="start_date" class="form-control form-control-solid datetimepicker-input" placeholder="Select Start date" id="datepicker_start_date" autocomplete="off" />
                        <label id="datepicker_start_date-error" class="error" for="datepicker_start_date"></label>
                       
                    </div>
                </div> 
                <div class="form-group col-md-6 col-sm-12 pr-0 hidden-fields" id="end_date_div">
                    <label class="">End Date</label>
                    <div class="input-group input-group-solid date">
                        <input type="text" name="end_date" class="form-control form-control-solid datetimepicker-input" placeholder="Select End date" id="datepicker_end_date" autocomplete="off" />
                    </div>
                </div> 
                <div class="form-group hidden-fields col-md-6 col-sm-12 pl-0" style="display:none">
                    <label class="">Start Time </label>
                    <div class="input-group input-group-solid date">
                        <input type="text" name="start_time" class="form-control form-control-solid datetimepicker-input" placeholder="Select Start Time" id="timepicker_start_time" data-toggle="datetimepicker" data-target="#timepicker_start_time" autocomplete="off" />
                       
                    </div>
                </div>  
                <div class="form-group hidden-fields col-md-6 col-sm-12 pr-0" style="display:none">
                    <label class="">End Time </label>
                    <div class="input-group input-group-solid date">
                        <input type="text" name="end_time" class="form-control form-control-solid datetimepicker-input" placeholder="Select End Time" id="timepicker_end_time" data-toggle="datetimepicker" data-target="#timepicker_end_time" autocomplete="off" />
                        <label id="timepicker_end_time-error" class="error" for="timepicker_end_time" style="display:none;">This field is required.</label>
                       
                    </div>
                </div> 
                </div>
                <div class="form-group hidden-fields recurrence-fields" id="recurrence-div" style="display:none" >
                <label class="">Recurrence</label>
                <div class="rec-radio"><input type="radio" name="recurrence" value="1">
                    <span>Daily</span></div>
                <div class="rec-radio"><input type="radio" name="recurrence" value="2">
                    <span>Weekly</span></div>
                <div class="rec-radio rec-radio-main">
                    <div class="left-rec-radio"><input type="radio" name="recurrence" value="3">
                    <span>Monthly</span></div>
                    <select name="monthly_type" id="monthly_type" class="monthly-div recurrence-fields" style="display:none">
                    </select>

                </div>
                <label id="recurrence-error" class="error" for="recurrence" style="display:none;">Please first select start date & end date.</label>
            </div> 
            <div class="form-group weekly-div-style recurrence-fields" id="weekly-div" style="display:none" >
                <label class="">Weekly Repeat Days</label>
                <div class="rec-radio rec-checkbox"><input type="checkbox" name="week_repeat_days[]" value="Monday">
                    <span>Monday</span>
                    <!-- <div class="time-inputs">
                        <input type="text" name="week_repeat_time[mon_start_time]" placeholder="Start Time">
                        <input type="text" name="week_repeat_time[mon_end_time]" placeholder="End Time">
                    </div> -->
                </div>
                <div class="rec-radio rec-checkbox"><input type="checkbox" name="week_repeat_days[]" value="Tuesday">
                    <span>Tuesday</span>
                    <!--  <div class="time-inputs">
                        <input type="text" name="week_repeat_time[tue_start_time]" placeholder="Start Time">
                        <input type="text" name="week_repeat_time[tue_end_time]" placeholder="End Time">
                    </div> -->
                </div>
                <div class="rec-radio rec-checkbox"><input type="checkbox" name="week_repeat_days[]" value="Wednesday">
                    <span>Wednesday</span>
                    <!--  <div class="time-inputs">
                        <input type="text" name="week_repeat_time[wed_start_time]" placeholder="Start Time">
                        <input type="text" name="week_repeat_time[wed_end_time]" placeholder="End Time">
                    </div> -->
                </div> 
                <div class="rec-radio rec-checkbox"><input type="checkbox" name="week_repeat_days[]" value="Thursday">
                    <span>Thursday</span>
                    <!--  <div class="time-inputs">
                        <input type="text" name="week_repeat_time[thus_start_time]" placeholder="Start Time">
                        <input type="text" name="week_repeat_time[thus_end_time]" placeholder="End Time">
                    </div> -->
                </div> 
                <div class="rec-radio rec-checkbox"><input type="checkbox" name="week_repeat_days[]" value="Friday">
                    <span>Friday</span>
                    <!--  <div class="time-inputs">
                        <input type="text" name="week_repeat_time[fri_start_time]" placeholder="Start Time">
                        <input type="text" name="week_repeat_time[fri_end_time]" placeholder="End Time">
                    </div> -->
                </div> 
                <div class="rec-radio rec-checkbox"><input type="checkbox" name="week_repeat_days[]" value="Saturday">
                    <span>Saturday</span>
                     <!-- <div class="time-inputs">
                        <input type="text" name="week_repeat_time[sat_start_time]" placeholder="Start Time">
                        <input type="text" name="week_repeat_time[sat_end_time]" placeholder="End Time">
                    </div> -->
                </div>
                <div class="rec-radio rec-checkbox"><input type="checkbox" name="week_repeat_days[]" value="Sunday">
                    <span>Sunday</span>
                    <!--  <div class="time-inputs">
                        <input type="text" name="week_repeat_time[sun_start_time]" placeholder="Start Time">
                        <input type="text" name="week_repeat_time[sun_end_time]" placeholder="End Time">
                    </div> -->
                </div>
            </div>    
            
      </div>

      <!-- Modal footer -->
      <div class="modal-footer">
        <button type="button" class="btn btn-outline-light" data-dismiss="modal">Close</button>
         <button type="submit" class="btn btn-danger" id="add-schedule-button">Submit</button>
      </div>

    </div>
  </div>
</form>
</div>
<div class="modal fade add-scout-cls schedule-modal update-schedule" id="updateCalendarModal">
  <div class="modal-dialog">
    <form id="update-schedule-form" name="update-schedule-form" action="" method="post" enctype="multipart/form-data">
    <div class="modal-content">
      <div class="modal-header">
        <div class="update-main-schedule">
        <h4 class="modal-title">Update Sechdule</h4>
         
     </div>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>

      <!-- Modal body -->
        <div class="modal-body">
        @csrf
        <input type="hidden" name="schedule_id" id="schedule_id" value="">
        <input type="hidden" name="schedule_exp_id" id="schedule_exp_id" value="{{(isset($expData))? $expData->id :0}}">

            <div class="d-flex flex-wrap">
              
                <div class="form-group col-md-6 col-sm-12 pl-0" style="">
                    <label class="">Start Time </label>
                    <div class="input-group input-group-solid date">
                        <input type="text" name="start_time" class="form-control form-control-solid datetimepicker-input" placeholder="Select Start Time" id="timepicker_start_time2" data-toggle="datetimepicker" data-target="#timepicker_start_time2" />
                       
                    </div>
                </div>  
                <div class="form-group col-md-6 col-sm-12 pr-0" style="">
                    <label class="">End Time </label>
                    <div class="input-group input-group-solid date">
                        <input type="text" name="end_time" class="form-control form-control-solid datetimepicker-input" placeholder="Select End Time" id="timepicker_end_time2" data-toggle="datetimepicker" data-target="#timepicker_end_time2" />
                       
                    </div>
                </div> 
                </div> 
            
        </div>

      <!-- Modal footer -->
      <div class="modal-footer">
        <a type="button" class=" delete  btn btn-light-primary action-experience-del-btn font-weight-bold btn-sm px-4 font-size-base ml-2 pink-bg-btn schedule-delete-confirmation" id="btn-delete-event" data-id="">Delete Schedule</a>
         <button type="submit" class="btn btn-outline-light" id="add-schedule-button2">Update</button>
      </div>

    </div>
</form>
  </div>
</div>
@endsection
{{-- Scripts Section --}}
@section('scripts')
<script src="{{asset('js/pages/crud/file-upload/image-input.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js" ></script>
<script src="{{ asset('calendar/main.js') }}"></script>
<script type="text/javascript">
        var formAction="{{(isset($expData))? route('scout-update-experience') : route('scout-create-experience')}}";
        var feature_image = "{{(isset($expData) && file_exists(public_path('pages/experiences/'.$expData->experience_feature_image)))? $expData->experience_feature_image : ''}}";
        var availability='';
        var preloaded=<?= $selectedAddonImages;?>;
        var events=<?= $selectedSchedule;?>;
</script>
<script src="{{asset('js/pages/crud/forms/widgets/bootstrap-datepicker.js')}}"></script>
<script src="{{asset('js/pages/crud/forms/widgets/bootstrap-datetimepicker.js')}}"></script>
<script src="{{asset('js/pages/crud/forms/widgets/select2.js')}}"></script>
<script src="{{asset('js/backend_experience.js')}}" ></script>
<script src="{{asset('js/autocomplete.js')}}" ></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDny-gFxyDw-Xur7z92RCuhfZuO3wHzAKw&libraries=places&callback=initMap" type="text/javascript"></script>
@endsection