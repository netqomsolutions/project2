<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Invoice - LFZ</title>

    <style type="text/css">
        @page {
            margin: 0px;
        }
        body {
            margin: 0px;
            font-size: 20px;
        }
        * {
            font-family: Verdana, Arial, sans-serif;
        }
        a {
            color: #fff;
            text-decoration: none;
        }
        table {
            font-size: x-small;
        }
        tfoot tr td {
            font-weight: bold;
            font-size: x-small; 
        }
        td { 
            font-size: 16px;
             color: #2c2e35;
        }
        .invoice table {
            margin: 15px;
        }
        .invoice h3 {
            margin-left: 15px;
        }
        .information {
            background-color: #fff;
            color: #2c2e35;
        }
        .information .logo {
            margin: 5px;
        }
        .information table {
            padding: 10px;
        }
        .bgGray {
               background-color: #f4f4f4;
			    font-size: 16px;
			    padding: 26px 20px; 
			    border-radius: 8px;
			    margin: 40px 0 0 0;
        }

       .invoice td { 
			    font-size: 16px;
			    padding: 22px 16px; 
			    border-radius: 8px;
			    margin: 40px 0 0 0;
        }
        .invoice-foot {
        	    font-size: 26.67px;
                font-weight: 500;
                display: block;
            }
        .logo{
            width:100%;
        }
    </style>

</head>
<body>

<div class="information">
    <table width="100%">
        <tr>
            <td align="left" style="width: 40%;">
             <img src="http://104.131.176.128/public/images/logo.jpg" alt="Logo"   class="logo"/>

            </td>
            
            <td align="right" style="width: 40%;">
  
                   <span style="font-size: 20px;font-weight: 500;text-align: right;margin:0;">Ideas From Zero, Zavod za razvoj in<br/>
					promocijo turizma</span><br/><br/>

					Cankarjeva cesta 1<br/>
					1270 Litija<br/>
					Tax ID: 14056887<br/>
					IBAN: SI56 6100 0002 4450 361<br/>
					BIC code: HDELSI22<br/>
					Registration No.: 8714207000<br/>
                 
            </td>
        </tr>

    </table>
</div>
<div class="information">
    <table width="100%">
        <tr>
            <td align="left" style="width: 40%;">
                 <b>Customer invoice No.</b> 21-0001<br/><b>Reservation code:</b> H205XML<br/>
    				Litija, Jan, 03,2021<br/>
    				<p style="font-size: 20px;font-weight: 400;margin: 30px 0 0 0;">Tadej Rogelja<br>Vojščica 32<br>5296 Kostanjevica na Krasu</p>
           </td>
            
            <td align="right" style="width: 40%;">
             
                 <div style="width: 145px;text-align: center;">
                        <img src="http://104.131.176.128/public/images/qr-code-img.png">
                        <p style="font-size: 16.67px;font-weight: 400;margin:0;">Experience info.</p>
                    </div>

					<b style="font-size: 20px;font-weight: 400;margin:32px 0 0;text-align: right;" >Scout phone:</b> +386 (0) 41 825 569<br/>
                   <b>Local provider phone:</b> +386 (0) 31 228 579<br/>
                 
            </td>
        </tr>

    </table>
</div>


<br/>

<div class="invoice"> 
    <table width="100%">
      
        <tbody>
        <tr class='bgGray'>
            <th align="left" >Experience title:</th>
            <td align="right" >Tea for two</td> 
        </tr>
        <tr>
            <td align="left" >No. of guests:</td>
            <td align="right" >5</td> 
        </tr>
        <tr class='bgGray'>
            <td align="left" >Travel to destination:</td>
            <td align="right">Slovenia, Vojščica 32, 5296 Kostanjevica na Krasu</td> 
        </tr>
        <tr>
            <td align="left" >Exact location to meet:</td>
            <td align="right">Infront of the church</td> 
        </tr>
        <tr class='bgGray'>
            <td align="left" >Experience date and time:</td>
            <td align="right">Feb, 25, 2021 at 17:00</td> 
        </tr>
        </tbody>

        <tfoot>
        <tr> 
            <td align="center" colspan="2">RESERVATION CHARGES</td> 
        </tr> 
          <tr  class='bgGray'> 
            <td align="left" style="margin: 0;font-weight: 400;font-size: 16.67px;" ><b style="font-size: 26.67px;font-weight: 500;display: block;" >Total amount:</b> <br/> Tax:</td>
            <td align="right" class="gray" style="margin: 0;font-weight: 400;font-size: 16.67px;"><b style="font-size: 26.67px;font-weight: 500;display: block;">EUR 200,</b></br>EUR 0,00</td>
        </tr>
        </tfoot>
    </table>
</div>
 
</body>
</html>