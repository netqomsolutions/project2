{{-- Extends layout --}}
@extends('layout.default')
{{-- Content --}}
@section('content')
<style type="text/css">
	.dataTables_wrapper .dataTable th:before {content:none !important;}
	.dataTables_wrapper .dataTable th:after {content:none !important;}
</style>
<div class="row">
	<div class="col-lg-12">
		<meta name="csrf-token" content="{{ csrf_token() }}" />
		<!--begin::Card-->
		@if (session('success'))
		<div class="alert alert-success scout-list-success">
			{{ session('success') }}
		</div>
		@elseif(session('failure'))
		<div class="alert alert-danger scout-list-success">
			{{ session('failure') }}
		</div>			    
		@endif
		<script type="text/javascript">setTimeout(function(){ $('.scout-list-success').fadeOut(); }, 3000);</script>
		<div class="card card-custom">
			<div class="card-header">
				<div class="card-title w-100">
					<span class="card-icon">
						<i class="flaticon2-supermarket text-primary"></i>
					</span>
					<div class="list_sec">
						<h3 class="card-label">List of Experiences</h3>
					</div>
<!-- 					<div class="add-btn">
						<a id="" href="{{route('scout-add-host')}}" class="btn btn-sm float-right green-bg">Add New <i class="fa fa-plus" aria-hidden="true"></i></a>
					</div> -->
				</div>
			</div>
			<div class="card-body">
				<div class="row">
						@forelse ($experiences as $key => $experience)
						<div class="col-lg-4 col-md-4 col-sm-12 wishList-main exp-list-main" id="item-{{$experience->id}}">
							<div class="exp-box">
								<div class="exp-img">
									<a href="{{ route('experience-detail',['id'=>base64_encode($experience->id)]) }}"><img src="{{asset('pages/experiences/'.$experience->experience_feature_image)}}"></a>
								</div>
								<div class="exp-content">
									<div class="title">
										<a href="{{ route('experience-detail',['id'=>base64_encode($experience->id)]) }}">{{ $experience->experience_name }}</a>
									</div>
									<div class="rating-price">
										<div class="rating">
											<ul mk="4">
												{!! getExperienceReview($experience->experience_id,'ratings') !!}
<!-- 												<i class="fas fa-star text-warning" aria-hidden="true"> </i>
												<i class="fas fa-star text-warning" aria-hidden="true"> </i>
												<i class="fas fa-star text-warning" aria-hidden="true"> </i>
												<i class="fas fa-star text-warning" aria-hidden="true"> </i>
												<i class="far fa-star text-warning" aria-hidden="true"> </i> -->
												<li>{{ getExperienceReview($experience->expid,'reviews') }} Reviews</li>
											</ul>
										</div>
									</div>
									<div class="price-div">
									    <span class="price-info">
									    <span class="price-span">€{{ number_format($experience->experience_low_price) }} {{ $experience->experience_high_price != 0.00 ? "- ".number_format($experience->experience_high_price) : ''  }}</span>
									    Group Price For 1-{{ $experience->experience_price_vailid_for }} Persons
									    </span>
									</div>
									<div class="booking-conditions">
										<ul>
											<li><i class="far fa-user" aria-hidden="true"></i> Max: {{ $experience->maximum_participant }}</li>
											<li><i class="far fa-clock" aria-hidden="true"></i> {{ $experience->experience_duration }} Hours</li>
											<li><i class="fas fa-map-marker-alt" aria-hidden="true"></i> {{ $experience->experience_lname }}</li>
										</ul>
									</div>

									<div target="_blank" class="d-flex mt-3 justify-content-center flex-wrap btns-action-exp">
										<a href="{{route('experience-detail',['id'=>base64_encode($experience->id)]) }}" class="btn btn-light-primary font-weight-bold btn-sm px-4 font-size-base yellow-bg-btn">View</a>

										<a href="{{route('scout-edit-experience',['id' => base64_encode($experience->id)])}}" class="btn btn-light-primary  ml-2 font-weight-bold btn-sm px-4 font-size-base green-bg-btn">Edit</a>

										<div class="price manage-list-traveler">
											<a id="wishlist-show" href="javascript:void(0)" data-expid="{{ base64_encode($experience->id) }}" data-id="{{$experience->id}}"class=" delete  btn btn-light-primary action-experience-del-btn font-weight-bold btn-sm px-4 font-size-base ml-2 pink-bg-btn exp-delete-confirmation">
												Remove
											</a> 
										</div>
									</div>
								</div>
							</div>
						</div>
						
						@empty
						<div class="exp-box no-data-found no-data-backend" style="text-align: center;font-weight: 500;">
							<img src="{{asset('images/no-data-found.png')}}">
            				<h3>No Record Found.</h3>
       					 </div>
						@endforelse
				</div>
			</div>
		</div>
		<!--end::Card-->
	</div>
</div>
@endsection
{{-- Scripts Section --}}
@section('scripts')
<script type="text/javascript">
$(document).ready(function(){
	$.ajaxSetup({ headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') } });
$('.exp-delete-confirmation').confirmation({
    onConfirm: function() { 
    	 toastr.options.timeOut = 1500;
    	var exp_id=$(this).data('expid');
    	var id=$(this).data('id');
    	var i=100;
            $.ajax({
            url: `{{ route("scout-delete-experience") }}`,
            type: "POST",
            data: { exp_id : exp_id},
            dataType: "Json",
            success: function (data) {
            if(data.isSucceeded){
            toastr.success(data.message);
             $('#item-'+id).fadeOut(1500, function() { 
                $('#item-'+id).remove(); 
            }); 
            }else{
            toastr.error("Please try again");
            }
            },
            error: function (xhr, ajaxOptions, thrownError) {
            toastr.error("Please try again");
            }
            });
        }
        });
		});
</script>
@endsection
