<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Invoice - LFZ</title>

    <style type="text/css">
        @page {
            margin: 0px;
        }
        body {
            margin: 0px;
            font-size: 20px;
        }
        * {
            font-family: Verdana, Arial, sans-serif;
        }
        a {
            color: #fff;
            text-decoration: none;
        }
        table {
            font-size: x-small;
        }
        tfoot tr td {
            font-weight: bold;
            font-size: x-small; 
        }
        td { 
            font-size: 16px;
             color: #2c2e35;
        }
        .invoice table {
            margin: 15px;
        }
        .invoice h3 {
            margin-left: 15px;
        }
        .information {
            background-color: #fff;
            color: #2c2e35;
        }
        .information .logo {
            margin: 5px;
        }
        .information table {
            padding: 10px;
        }
        .bgGray {
               background-color: #f4f4f4;
			    font-size: 16px;
			    padding: 26px 20px; 
			    border-radius: 8px;
			    margin: 40px 0 0 0;
        }

       .invoice td { 
			    font-size: 16px;
			    padding: 22px 16px; 
			    border-radius: 8px;
			    margin: 40px 0 0 0;
        }
        .invoice-foot {
        	    font-size: 26.67px;
                font-weight: 500;
                display: block;
            }
        .logo{
            width:100%;
        }
    </style>

</head>
<body>
<?php 

$condata = json_decode($contact->page_content,true);
$contact->email=$condata['email'];
$contact->address=$condata['address'];
$contact->phone=$condata['phone'];

 $experience_price_vailid_for = $bookingDetail->experience->experience_price_vailid_for;
            $children_discount_1 = $bookingDetail->experience_meta_detail->children_discount_1;
            $totalAmt = 0;
            $totalChildAmt = 0;
            if($bookingDetail->experience->experience_type == 1){
                for ($i=0; $i < $bookingDetail->adults ; $i++) { 
                    if($i<$bookingDetail->experience->experience_price_vailid_for){
                        $totalAmt = $bookingDetail->experience->experience_low_price;
                    }else{
                        $totalAmt = $totalAmt + $bookingDetail->experience->experience_additional_person;
                    }
                }
            }else if($bookingDetail->experience->experience_type == 2){
                for ($i=0; $i < $bookingDetail->adults ; $i++) { 
                    if($i < $bookingDetail->experience->minimum_participant){
                        $totalAmt = $bookingDetail->experience->experience_low_price;
                    }else{
                        $totalAmt = $totalAmt + $bookingDetail->experience->experience_additional_person;
                    }
                }
            }else if($bookingDetail->experience->experience_type == 3){
                if(!empty($bookingDetail->experience->experience_additional_prices)){
                    $totalAmt = $bookingDetail->experience->experience_additional_prices[0]->additional_price;
                }
            }
            $totalChildAmt = $bookingDetail->children * $bookingDetail->experience->experience_low_price;
            $disPrice = ($children_discount_1 / 100 ) * $totalChildAmt; 
            $childNetPrice = $totalChildAmt - $disPrice;
            $totalaAmt =  $childNetPrice + $totalAmt;
             
?>
<div class="information">
    <table width="100%">
        <tr>
            <td align="left" style="width: 40%;">
             <img src="http://104.131.176.128/public/images/logo.jpg" alt="Logo"   class="logo"/>

            </td>
            
            <td align="right" style="width: 40%;">
  
                   <span style="font-size: 20px;font-weight: 500;text-align: right;margin:0;">{{$contact->address}}</span><br/><br/>

					Cankarjeva cesta 1<br/>
					1270 Litija<br/>
					Tax ID: 14056887<br/>
					IBAN: SI56 6100 0002 4450 361<br/>
					BIC code: HDELSI22<br/>
					Registration No.: 8714207000<br/>
                 
            </td>
        </tr>

    </table>
</div>
<div class="information">
    <table width="100%">
        <tr>
            <td align="left" style="width: 40%;">
                 <b>Customer invoice No.</b>{{$bookingDetail->charge_id}}<br/><b>Reservation code:</b> H205XML<br/>
    				Litija,{{date('M d, Y ', strtotime($bookingDetail->booking_date))}}<br/>
    				<p style="font-size: 20px;font-weight: 400;margin: 30px 0 0 0;">{{$bookingDetail->traveler->user_address}}</p>
           </td>
            
            <td align="right" style="width: 40%;">
             
                 <div style="width: 145px;text-align: center;">
                        <img src="http://104.131.176.128/public/images/qr-code-img.png">
                        <p style="font-size: 16.67px;font-weight: 400;margin:0;">Experience info.</p>
                    </div>

					<b style="font-size: 20px;font-weight: 400;margin:32px 0 0;text-align: right;" >Scout phone:</b> +386 (0) 41 825 569<br/>
                   <b>Local provider phone:</b> +386 (0) 31 228 579<br/>
                 
            </td>
        </tr>

    </table>
</div>


<br/>

<div class="invoice"> 
    <table width="100%">
      
        <tbody>
        <tr class='bgGray'>
            <th align="left" >Experience title:</th>
            <td align="right" >{{$bookingDetail->experience->experience_name}}</td> 
        </tr>
        <tr>
            <td align="left" >No. of guests:</td>
            <td align="right" >{{$bookingDetail->experience->experience_name}}</td> 
        </tr>
        <tr class='bgGray'>
            <td align="left" >Travel to destination:</td>
            <td align="right">{{$bookingDetail->experience->experience_name}}</td> 
        </tr>
        <tr>
            <td align="left" >Exact location to meet:</td>
            <td align="right">{{$bookingDetail->experience->experience_name}}</td> 
        </tr>
        <tr class='bgGray'>
            <td align="left" >Experience date and time:</td>
            <td align="right">{{$bookingDetail->experience->experience_name}}</td> 
        </tr>
        </tbody>

        <tfoot>
        <tr> 
            <td align="center" colspan="2">RESERVATION CHARGES</td> 
        </tr> 
          <tr  class='bgGray'> 
            <td align="left" style="margin: 0;font-weight: 400;font-size: 16.67px;" ><b style="font-size: 26.67px;font-weight: 500;display: block;" >Total amount:</b> <br/> Tax:</td>
            <td align="right" class="gray" style="margin: 0;font-weight: 400;font-size: 16.67px;"><b style="font-size: 26.67px;font-weight: 500;display: block;">EUR 200,</b></br>EUR 0,00</td>
        </tr>
        </tfoot>
    </table>
</div>
 
</body>
</html>