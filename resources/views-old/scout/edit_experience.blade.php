{{-- Extends layout --}}
@extends('layout.default')
{{-- Content --}}
<style type="text/css">
#second_step{
    display:none;
}
.my-error{
    color: red;
}
</style>
@section('content')
<div class="row">
    <div class="col-lg-12">
        <div class="card card-custom gutter-b example example-compact">
            <div class="card-header">
                <h3 class="card-title">Edit experience</h3>
                <div class="card-toolbar" style="display: none;">
                    <div class="example-tools justify-content-center">
                        <span class="example-toggle" data-toggle="tooltip" title="View code"></span>
                        <span class="example-copy" data-toggle="tooltip" title="Copy code"></span>
                    </div>
                </div>
            </div>
            <!--begin::Form-->
            <form class="form" enctype="multipart/form-data" id="add-exp" method="post" action="{{ route('scout-update-experience') }}">
                <div class="card-body">@csrf
                    <fieldset id="first_step" class="">
                        <input type="hidden" name="exp_id" value="{{$expData['id']}}"/>
                        <input type="hidden" name="exp_fea_rel_id" value="{{$expData['experience_feature_relations']['id'] }}"/>
                        <div class="form-group row">
                            <div class="col-lg-4 my-error mt-5">
                                <label>Experience name:</label>
                                <input type="text" name="data[experiences][experience_name]" class="form-control" value="{{$expData['experience_name']}}" placeholder="Enter experience name" />
                            </div>
                            <div class="col-lg-4 my-error mt-5">
                                <label>Experience type:</label>
                                <select class="form-control changeExpType" name="data[experiences][experience_type]">
                                    <option value="">Select experience type</option>
                                    <option value="1" {{$expData['experience_type'] == 1 ? 'selected' : ''}}>PRICING SCENARIO 1</option>
                                    <option value="2" {{$expData['experience_type'] == 2 ? 'selected' : ''}}>PRICING SCENARIO 2</option>
                                    <option value="3" {{$expData['experience_type'] == 3 ? 'selected' : ''}}>PRICING SCENARIO 3</option>
                                </select>
                            </div>
                            <div class="col-lg-4 my-error mt-5">
                                <label>Assign Host</label>
                                <select class="form-control" name="data[experiences][assigned_host]">
                                    <option value="">Select Host</option>
                                    @forelse ($hosts as $host)
                                        <option value="{{ $host->id }}" {{$expData['assigned_host'] == $host->id ? 'selected' : ''}} >{{ $host->user_fname.' '.$host->user_lname }}</option>
                                    @empty

                                    @endforelse
                                </select>
                            </div>
                            <div class="col-lg-4 my-error mt-5">
                                <label>Experience category:</label>
                                <select class="form-control" name="data[experiences][experience_category_id]">
                                    <option value="">Select category</option>
                                    @forelse ($experienceCategories as $experienceCategory)
                                        <option value="{{ $experienceCategory->id }}" {{$expData['experience_category_id'] == $experienceCategory->id ? 'selected' : ''}} >{{ $experienceCategory->name }}</option>
                                    @empty

                                    @endforelse
                                </select>
                            </div>
                                                
                            <div class="col-lg-4 my-error mt-5">
                                <label>Max guest:</label>
                                <select class="form-control" name="data[experiences][experience_group_size]">
                                    <option value="">Select max guest</option>
                                    @for($i = 1; $i <= 12; $i++)
                                        <option value="{{ $i }}" {{$expData['experience_group_size'] == $i ? 'selected' : ''}} >{{ $i }}</option>
                                    @endfor
                                </select>
                            </div>
                            <div class="col-lg-4 my-error mt-5">
                                <label>Duration (in hours):</label>
                                <input type="number" onkeydown="if(event.key==='.'){event.preventDefault();}"  oninput="event.target.value = event.target.value.replace(/[^0-9]*/g,'');" name="data[experiences][experience_duration]" class="form-control" placeholder="Enter experience duration" value="{{$expData['experience_duration']}}" />
                            </div>
                           
                            <div class="col-lg-4 my-error mt-5">
                                <label>Experience low price:</label>
                                <input type="number" name="data[experiences][experience_low_price]" class="form-control" placeholder="Enter experience low price" value="{{$expData['experience_low_price']}}" />
                            </div>

                                              
                            
                            <div class="col-lg-4 my-error mt-5" id="high-price" style="display: none;">
                                <label>Experience high price:</label>
                                <input type="number" name="data[experiences][experience_high_price]" class="form-control" placeholder="Enter experience high price" value="{{$expData['experience_high_price']}}" />
                            </div>
                            <div class="col-lg-4 my-error mt-5">
                                <label>Experience price vailid for:</label>
                                <select class="form-control" name="data[experiences][experience_price_vailid_for]">
                                    <option value="">Number of person</option>
                                    @for($i = 1; $i <= 12; $i++)
                                        @if ($i == 1)
                                            <option value="{{ $i }}" {{$expData['experience_price_vailid_for'] == $i ? 'selected' : ''}}>{{ $i }}</option>
                                        @else
                                            <option value="{{ $i }}" {{$expData['experience_price_vailid_for'] == $i ? 'selected' : ''}}>{{ 1 .'-'. $i }}</option>
                                        @endif                                        
                                    @endfor
                                </select>
                            </div>                            
                        
                             <div class="col-lg-8 my-error mt-5">
                                <label>Experience description:</label>
                                <textarea  name="data[experiences][experience_description]" class="form-control" placeholder="Enter experience description" >{{$expData['experience_description']}}</textarea>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-lg-6 my-error">
                                <p>
                                    <a class="btn btn-primary next">Next</a>
                                </p>
                            </div>
                        </div>
                    </fieldset>
                    <fieldset id="second_step" class="">
                        <div class="form-group row">                            
                            <div class="col-lg-4 my-error">
                                <label>Location Name:</label>
                                <input type="text"  name="data[experiences][experience_lname]" class="form-control" id="exp-location" placeholder="Enter experience location name" value="{{$expData['experience_lname']}}" />
                                <input type="hidden"  name="data[experiences][experience_longitude]" class="form-control" id="exp-longitude" value="{{$expData['experience_longitude']}}" />
                                <input type="hidden"  name="data[experiences][experience_latitude]" class="form-control" id="exp-latitude" value="{{$expData['experience_latitude']}}" />
                            </div>
                             <div class="col-lg-4 my-error">
                                <label>Experience hint:</label>
                                <input type="text" name="data[experiences][experience_hint]" class="form-control" placeholder="Enter your experience hint" value="{{$expData['experience_hint']}}" />
                            </div> 
                            <div class="col-lg-4 my-error">
                                <label>Feature image:</label>
                                <input type="file"  name="data[experiences][experience_feature_image]" class="form-control" placeholder="Browse image" /> 
                            </div>   
                        </div>
                        <div class="form-group row">                            
                            <div class="col-lg-4 my-error" >
                                <label>Addon image:</label>
                                <input type="file"  name="data[experience_images][image_name][]" class="form-control" multiple placeholder="Browse image" />
                            </div>
                            <div class="col-lg-4 my-error">
                                <label>Experience feature:</label>
                                <select class="form-control" name="data[experience_feature_relations][experience_feature_id]">
                                    <option value="">Select </option>
                                    @forelse ($experienceFeatures as $experienceFeature)
                                        <option value="{{ $experienceFeature->id }}" {{$expData['experience_feature_relations']['experience_feature_id'] == $experienceFeature->id ? 'selected' : ''}}>{{ $experienceFeature->name }}</option>
                                    @empty

                                    @endforelse
                                </select>
                            </div>  
                            <div class="col-lg-4 my-error">
                                <label>Experience start time:</label>
                                <select class="form-control" name="data[experiences][experience_start_time]">
                                    <option value="">Select start time</option>
                                    <option value="1" {{$expData['experience_start_time'] ==1 ? 'selected' : ''}}>Morning ( 01:00 - 11:00 )</option>
                                    <option value="2" {{$expData['experience_start_time'] ==2 ? 'selected' : ''}}>Afternoon ( 12:00 - 18:00 )</option>
                                    <option value="3" {{$expData['experience_start_time'] ==3 ? 'selected' : ''}}>Evening ( 19:00 - 00:00 )</option>
                                </select>
                            </div>                             
                        </div>
                        <div class="form-group row">
                             <div class="col-lg-4 my-error">
                                <label>Experience booking advance time (in hours):</label>
                                <input type="number" name="data[experiences][experience_booking_in_advance_time]" class="form-control" placeholder="Enter experience booking advance time" value="{{$expData['experience_booking_in_advance_time']}}" max=24 min=0 />
                            </div>   
                        </div>
                        <div class="form-group row">
                            <div class="col-lg-6">
                                <p>                        
                                    <a class="btn btn-secondary previous" id="previous" >Previous</a>
                                    <a class="btn btn-primary next" data-next="third_step">Next</a>
                                </p>
                            </div>
                        </div>
                    </fieldset>
                     <fieldset id="third_step" class="step-form-hide" style="display:none;">
                        <div class="form-group row">
                             <div class="col-lg-12">
                            <div id='calendar'></div>
                        </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-lg-6">
                                <p>                        
                                    <a class="btn btn-secondary previous" data-prev="second_step" >Previous</a>
                                    <button type="submit" class="btn btn-primary">Save</button>
                                </p>
                            </div>
                        </div>
                    </fieldset>
                </div>
                <div class="card-footer">
                </div>
            </form>
            <!--end::Form-->
        </div>
    </div>
</div>
@endsection
{{-- Scripts Section --}}
@section('scripts')
<script type="text/javascript">
        var feature_image = `{{$expData['experience_feature_image']}}`;
        var addon_image = `{{ count($expData['experience_images']) }}`;
        var availability=[
        {
          title: 'Business Lunch',
          start: '2020-09-03T13:00:00',
          constraint: 'businessHours'
        },
        {
          title: 'Meeting',
          start: '2020-09-13T11:00:00',
          constraint: 'availableForMeeting', // defined below
          color: '#257e4a'
        },
        {
          title: 'Conference',
          start: '2020-09-18',
          end: '2020-09-20'
        },
        {
          title: 'Party',
          start: '2020-09-29T20:00:00'
        },

        // areas where "Meeting" must be dropped
        {
          groupId: 'availableForMeeting',
          start: '2020-09-11T10:00:00',
          end: '2020-09-11T16:00:00',
          display: 'background'
        },
        {
          groupId: 'availableForMeeting',
          start: '2020-09-13T10:00:00',
          end: '2020-09-13T16:00:00',
          display: 'background'
        },

        // red areas where no events can be dropped
        {
          start: '2020-09-24',
          end: '2020-09-28',
          overlap: false,
          display: 'background',
          color: '#ff9f89'
        },
        {
          start: '2020-09-06',
          end: '2020-09-08',
          overlap: false,
          display: 'background',
          color: '#ff9f89'
        }
      ];
</script>
<script src="{{asset('js/backend_experience.js')}}" type="text/javascript"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDny-gFxyDw-Xur7z92RCuhfZuO3wHzAKw&libraries=places&callback=initMap" type="text/javascript"></script>
@endsection