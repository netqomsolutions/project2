{{-- Extends layout --}}
@extends('layout.default')
{{-- Content --}}
@section('content')
<style>
    .toggle.ios, .toggle-on.ios, .toggle-off.ios { border-radius: 20rem; }
    .toggle.ios .toggle-handle { border-radius: 20rem; }
    div#action-scout {
        align-items: center;
        justify-content: center;
    }
</style>
 @if(session()->has('success'))
            <div class="alert alert-success alluser-page-suc" style="text-align: left;float: left;width: 100%;">
                {{ session()->get('success') }}
            </div>
            <script type="text/javascript">setTimeout(function(){ $('.alluser-page-suc').fadeOut(); }, 3000);</script>
        @elseif(session()->has('failure'))
            <div class="alert alert-danger alluser-page-suc" style="text-align: left; left;float: left;width: 100%;">
                {{ session()->get('failure') }}
            </div>
            <script type="text/javascript">setTimeout(function(){ $('.alluser-page-suc').fadeOut(); }, 3000);</script>
        @endif
    <div class="main-body">
          <!--  <div class="user-profile-buttons">
            <a href="{{ route('user-profile-edit') }}" class="navi-item">Update Profile</a>
            <a href="{{ route('user-change-password-form') }}" class="navi-item">Change Password</a>
          </div> -->
          
          
          <div class="d-flex flex-row w-100">
                                @include('scout.host-sidebar')
                                    <!--begin::Content-->
                                    <div class="flex-row-fluid ml-lg-8">
                                        <!--begin::Card-->
                                        <div class="card card-custom card-stretch card-sticky" id="kt_page_sticky_card">
                                            <!--begin::Header-->
                                            <div class="card-header py-3">
                                                <div class="card-title align-items-start flex-column">
                                                    <h3 class="card-label font-weight-bolder text-dark">Account Information</h3>
                                                    <span class="text-muted font-weight-bold font-size-sm mt-1">Add Account Information</span>
                                                </div>
                                                <div class="card-toolbar">
                                                    <button type="button" class="btn btn-success mr-2 green-bg-btn" id="save-account-button">Save Changes</button>
                                                    <!-- <button type="reset" class="btn btn-secondary">Cancel</button> -->
                                                </div>
                                            </div>
                                            <!--end::Header-->
                                            <!--begin::Form-->
                                            <form id="add-account-information" class="form" method="post" action="{{route('create-update-account')}}" enctype="multipart/form-data">
                                                <!--begin::Body-->
                                                @csrf
                                                <div class="card-body">
                                                    <div class="form-group row manage-personal-info">
                                                    <input type="hidden" name="u_id" value="{{$userData->id}}">
                                                    <input type="hidden" name="is_host" value="1">

                                                    <div class="col-lg-12 pl-0 pr-0">
                                                        <div class="form-group row">
                                                        <label class="col-xl-3 col-lg-12 col-md-12 col-form-label">Account Holder First Name</label>
                                                        <div class="col-lg-12 col-xl-9 col-md-12">
                                                            <input class="form-control form-control-lg form-control-solid" name="user_fname" type="text" value="{{$userData->user_fname}}">
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label class="col-xl-3 col-lg-12 col-md-12 col-form-label">Account Holder Last Name</label>
                                                        <div class="col-lg-12 col-xl-9 col-md-12">
                                                            <input class="form-control form-control-lg form-control-solid" name="user_lname" type="text" value="{{$userData->user_lname}}">
                                                        </div>
                                                    </div> 
                                                    <div class="form-group row">
                                                        <label class="col-xl-3 col-lg-12 col-md-12 col-form-label">Email</label>
                                                        <div class="col-lg-12 col-xl-9 col-md-12">
                                                            <input class="form-control form-control-lg form-control-solid" name="email" type="text" value="{{$userData->email}}">
                                                        </div>
                                                    </div> 
                                                    <div class="form-group row">
                                                        <label class="col-xl-3 col-lg-12 col-md-12 col-form-label">Phone</label>
                                                        <div class="col-lg-12 col-xl-9 col-md-12">
                                                            <input class="form-control form-control-lg form-control-solid" name="phone" type="text" value="" placeholder="Enter Number Without Country Code">
                                                        </div>
                                                    </div> 
                                                    <div class="form-group row">
                                                        <label class="col-xl-3 col-lg-12 col-md-12 col-form-label"> DOB</label>
                                                        <div class="col-lg-12 col-xl-9 col-md-12">
                                                            <input class="form-control form-control-lg form-control-solid" name="dob" id="user_dob" type="text" value="{{$userData->user_meta->dob}}" autocomplete="off">
                                                        </div>
                                                    </div> 
                                                    <div class="form-group row">
                                                        <label class="col-xl-3 col-lg-12 col-md-12 col-form-label">Upload Identity Document (Front)</label>
                                                        <div class="col-lg-12 col-xl-9 col-md-12">
                                                            <input class="form-control form-control-lg form-control-solid" name="identity_document_front" type="file" value="">
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label class="col-xl-3 col-lg-12 col-md-12 col-form-label">Upload Identity Document (back)</label>
                                                        <div class="col-lg-12 col-xl-9 col-md-12">
                                                            <input class="form-control form-control-lg form-control-solid" name="identity_document_back" type="file" value="">
                                                        </div>
                                                    </div> 
                                                    <div class="form-group row">
                                                        <label class="col-xl-3 col-lg-12 col-md-12 col-form-label">Upload Additional Document (Front)</label>
                                                        <div class="col-lg-12 col-xl-9 col-md-12">
                                                            <input class="form-control form-control-lg form-control-solid" name="additional_document_front" type="file" value="">
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label class="col-xl-3 col-lg-12 col-md-12 col-form-label">Upload Additional Document (Back)</label>
                                                        <div class="col-lg-12 col-xl-9 col-md-12">
                                                            <input class="form-control form-control-lg form-control-solid" name="additional_document_back" type="file" value="">
                                                        </div>
                                                    </div>
                                                     <div class="form-group row">
                                                        <label class="col-xl-3 col-lg-12 col-md-12 col-form-label">Account Number</label>
                                                        <div class="col-lg-12 col-xl-9 col-md-12">
                                                            <input class="form-control form-control-lg form-control-solid" name="account_number" type="text" value="">
                                                        </div>
                                                    </div>
                                                     <div class="form-group row">
                                                        <label class="col-xl-3 col-lg-12 col-md-12 col-form-label">Country</label>
                                                        <div class="col-lg-12 col-xl-9 col-md-12">
                                                            <select class="form-control form-control-lg form-control-solid selectpicker" name="country" data-live-search="true" data-size="5">
                                                                <option value="">Select Country</option>
                                                                <option value="AT">Austria</option>
                                                                <option value="BE">Belgium</option>
                                                                <option value="BG">Bulgaria</option>
                                                                <option value="CZ">Czech Republic</option>
                                                                <option value="DK">Denmark</option>
                                                                <option value="EE">Estonia</option>
                                                                <option value="FI">Finland</option> 
                                                                <option value="FR">France</option> 
                                                                <option value="DE">Germany</option> 
                                                                <!--option value="GI">Gibraltar</option--> 
                                                                <option value="GR">Greece</option> 
                                                                <option value="HU">Hungary</option>
                                                                <option value="IE">Ireland</option>
                                                                <option value="IT">Italy</option> 
                                                                <option value="LI">Liechtenstein</option>
                                                                <option value="LT">Lithuania</option>
                                                                <option value="LU">Luxembourg</option> 
                                                                <option value="LV">Latvia</option>
                                                                <option value="MT">Malta</option> 
                                                                <option value="NL">Netherlands</option>
                                                                <option value="NO">Norway</option>
                                                                <option value="PL">Poland</option>
                                                                <option value="PT">Portugal</option> 
                                                                <option value="RO">Romania</option>
                                                                <option value="SK">Slovakia</option>
                                                                <option value="SI" selected>Slovenia</option> 
                                                                <option value="ES">Spain</option> 
                                                                <option value="SE">Sweden</option>
                                                                <option value="CH">Switzerland</option> 
                                                                <!--option value="GB">United Kingdom</option-->
                                                            </select>
                                                        </div>
                                                        <label id="country-error" class="error" for="country" style="display:none"></label>
                                                    </div> 
                                                    <div class="form-group row">
                                                        <label class="col-xl-3 col-lg-12 col-md-12 col-form-label">City</label>
                                                        <div class="col-lg-12 col-xl-9 col-md-12">
                                                            <input class="form-control form-control-lg form-control-solid" name="city" type="text" value="">
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label class="col-xl-3 col-lg-12 col-md-12 col-form-label">Address (Line 1)</label>
                                                        <div class="col-lg-12 col-xl-9 col-md-12">
                                                            <input class="form-control form-control-lg form-control-solid" name="address_line1" type="text" value="">
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label class="col-xl-3 col-lg-12 col-md-12 col-form-label">Address (Line 2)</label>
                                                        <div class="col-lg-12 col-xl-9 col-md-12">
                                                            <input class="form-control form-control-lg form-control-solid" name="address_line2" type="text" value="">
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label class="col-xl-3 col-lg-12 col-md-12 col-form-label">Postal Code </label>
                                                        <div class="col-lg-12 col-xl-9 col-md-12">
                                                            <input class="form-control form-control-lg form-control-solid" name="postal_code" type="text" value="">
                                                        </div>
                                                    </div>
                                                    </div>
                                                </div>
                                                <!--end::Body-->
                                            </form>
                                            <!--end::Form-->
                                        </div>
                                    </div>
                                    <!--end::Content-->
                                </div>
          
        </div>
        

@endsection
{{-- Scripts Section --}}
@section('scripts')
<script type="text/javascript">
    $.ajaxSetup({headers: {'X-XSRF-TOKEN': decodeURIComponent(/XSRF-Token=([^;]*)/ig.exec(document.cookie)[1])}});
    $(document).ready(function () {
        var enddate = new Date();
        enddate.setFullYear( enddate.getFullYear() - 14);
        var endyear=enddate.getFullYear();
        var startdate = new Date();
        startdate.setFullYear( startdate.getFullYear() - 54);
        var startyear=startdate.getFullYear();
        $('#user_dob').datepicker({
            maxViewMode:0,
            format:'yyyy-mm-dd',
            startDate: new Date(startyear, 11, 31),
            endDate: new Date(endyear, 11, 31),
        })
        var user = '<?php echo (!empty($scoutData->user_image))? $scoutData->user_image :''; ?>';
        $.validator.addMethod('filesize', function (value, element, param) {
            return this.optional(element) || (element.files[0].size <= param)
        }, 'File size must be less than {0}');
        $('#add-account-information').validate({
            
            rules: {
                user_fname: {
                    required: true
                },
                user_lname: {
                    required: true,
                },
                dob:{
                    required: true,
                },
                email:{
                    required: true,
                    email: true,
                },
                phone:{
                    required: true,
                    digits: true,
                },
                identity_document_front:{
                    required: true,
                    extension: "jpg,jpeg,png",
                },
                identity_document_back:{
                    required: true,
                    extension: "jpg,jpeg,png",
                },
                additional_document_front:{
                    required: true,
                    extension: "jpg,jpeg,png",
                },
                additional_document_back:{
                    required: true,
                    extension: "jpg,jpeg,png",
                },
                account_number:{
                    required: true,
                },
                country:{
                    required: true,
                },
                city:{
                    required: true,
                },
                address_line1:{
                    required: true,
                },
                postal_code:{
                    required: true,
                    digits: true,
                },
               
            },
        });
         $("#save-account-button").click(function(){
          if($("#add-account-information").valid())
          {
            let myForm = document.getElementById('add-account-information');
             myForm.submit();
          }
        });
    });
</script>
@endsection