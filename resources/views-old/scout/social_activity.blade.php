{{-- Extends layout --}}
@extends('layout.default')
{{-- Content --}}
@section('content')
<style>
	.toggle.ios, .toggle-on.ios, .toggle-off.ios { border-radius: 20rem; }
	.toggle.ios .toggle-handle { border-radius: 20rem; }
	div#action-scout {
	    align-items: center;
	    justify-content: center;
	}
	#social-activity-form .form-group.row .icon-inner {
    display: block;
    margin-top: 13px; 
    cursor: pointer; 
}
.fa-facebook-f:hover{
color: #3b5998; 
}.fa-instagram:hover{
color: #c434a9; 
}.fa-twitter:hover{
color: #50abf1; 
}
.fa-linkedin-in:hover{
color: #0077b5; 
}.fa-pinterest:hover{
color: #c01b26; 
}
.icon-inner i {
    font-size: 22px;}
</style>

@php $social_links = json_decode($social_link); @endphp 
 @if(session()->has('success'))
            <div class="alert alert-success alluser-page-suc" style="text-align: left;float: left;width: 100%;">
                {{ session()->get('success') }}
            </div>
            <script type="text/javascript">setTimeout(function(){ $('.alluser-page-suc').fadeOut(); }, 3000);</script>
        @elseif(session()->has('failure'))
            <div class="alert alert-danger alluser-page-suc" style="text-align: left; left;float: left;width: 100%;">
                {{ session()->get('failure') }}
            </div>
            <script type="text/javascript">setTimeout(function(){ $('.alluser-page-suc').fadeOut(); }, 3000);</script>
        @endif
    <div class="main-body">
          <!--  <div class="user-profile-buttons">
            <a href="{{ route('user-profile-edit') }}" class="navi-item">Update Profile</a>
            <a href="{{ route('user-change-password-form') }}" class="navi-item">Change Password</a>
          </div> -->
		  
		  
	<div class="d-flex flex-row w-100">
		@include('./profile-sidebar')	
        <!--begin::Content-->
		<div class="flex-row-fluid ml-lg-8">
			<!--begin::Card-->
			<!-- card-sticky class  -->
			<div class="card card-custom  card-sticky card-stretch" id="kt_page_sticky_card">
				<!--begin::Header-->
				<div class="card-header py-3">
					<div class="card-title align-items-start flex-column">
						<h3 class="card-label font-weight-bolder text-dark">Social Activity</h3>
						<span class="text-muted font-weight-bold font-size-sm mt-1">Add/Update your Social Profile </span>
					</div>
					<div class="card-toolbar">
						<button type="button" id="save-changes-button" class="btn btn-success mr-2 green-bg-btn">Save Changes</button>
						<!--button type="reset" class="btn btn-secondary">Cancel</button-->
					</div>
				</div>
				<!--end::Header-->
				<!--begin::Form-->
				<form class="form" id="social-activity-form" method="post" action="{{ route('social-activity') }}" enctype="multipart/form-data">
					 @csrf
					<div class="card-body">
					 
						<div class="form-group d-flex align-items-center">
							<div class="icon-inner">
								<a  target="_blank" href="{{ !empty($social_links->facebook_link) ? $social_links->facebook_link : 'javascript:;' }}">
								   <i class="fab fa-facebook-f" aria-hidden="true"></i>
							    </a>
							</div>
							<!-- <label class="col-xl-3 col-lg-3 col-form-label text-alert"></label> -->
							<div class="col-lg-9 col-xl-6">
								<input type="text" class="form-control form-control-lg form-control-solid mb-2" id="facebook_link" name="facebook_link" value="{{ !empty($social_links->facebook_link) ? $social_links->facebook_link : '' }}" placeholder="Enter facebook profile link" />
								<!--a href="#" class="text-sm font-weight-bold">Forgot password ?</a-->

								
								

							</div>
						</div>
						<div class="form-group d-flex align-items-center">
							<div class="icon-inner">
								<a target="_blank" href="{{ !empty($social_links->instagram_link) ? $social_links->instagram_link : 'javascript:;' }}">
								<i class="fab fa-instagram" aria-hidden="true"></i>
							  </a>
							</div>
							<!-- <label class="col-xl-3 col-lg-3 col-form-label text-alert"></label> -->
							<div class="col-lg-9 col-xl-6">
								<input type="text" class="form-control form-control-lg form-control-solid" id="instagram_link"  name="instagram_link" value="{{ !empty($social_links->instagram_link) ? $social_links->instagram_link : '' }}" placeholder="Enter instagram profile link" />
							</div>
						</div>
						<div class="form-group d-flex align-items-center">
							<div class="icon-inner">
								<a target="_blank" href="{{ !empty($social_links->twitter_link) ? $social_links->twitter_link : 'javascript:;' }}">
								<i class="fab fa-twitter" aria-hidden="true"></i>
							</a>
							</div>
							<!-- <label class="col-xl-3 col-lg-3 col-form-label text-alert"></label> -->
							<div class="col-lg-9 col-xl-6">
								<input type="text" class="form-control form-control-lg form-control-solid" id="twitter_link" value="{{ !empty($social_links->twitter_link) ? $social_links->twitter_link : '' }}" placeholder="Enter twitter profile link" name="twitter_link" />
							</div>
						</div>
						<div class="form-group d-flex align-items-center">
							<div class="icon-inner">
								<a target="_blank" href="{{ !empty($social_links->linkedin_link) ? $social_links->linkedin_link : 'javascript:;' }}">
								<i class="fab fa-linkedin-in" aria-hidden="true"></i>
							</a>
							</div>
							<!-- <label class="col-xl-3 col-lg-3 col-form-label text-alert"></label> -->
							<div class="col-lg-9 col-xl-6">
								<input type="text" class="form-control form-control-lg form-control-solid" id="linkedin_link" value="{{ !empty($social_links->linkedin_link) ? $social_links->linkedin_link : '' }}" placeholder="Enter linkedin profile link" name="linkedin_link" />
							</div>
						</div>
						<div class="form-group d-flex align-items-center">
							<div class="icon-inner">
								<a target="_blank" href="{{ !empty($social_links->pinterest_link) ? $social_links->pinterest_link : 'javascript:;' }}">
								<i class="fab fa-pinterest" aria-hidden="true"></i>
							</a>
							</div>
							<!-- <label class="col-xl-3 col-lg-3 col-form-label text-alert"></label> -->
							<div class="col-lg-9 col-xl-6">
								<input type="text" class="form-control form-control-lg form-control-solid" id="pinterest_link" value="{{ !empty($social_links->pinterest_link) ? $social_links->pinterest_link : '' }}" placeholder="Enter Pinterest Profile Link" name="pinterest_link" />
							</div>
						</div>
						

						
						

					</div>
				</form>
				<!--end::Form-->
			</div>
		</div>
									<!--end::Content-->
	  </div>
	  @endsection
{{-- Scripts Section --}}
@section('scripts')
<script type="text/javascript">
     $.ajaxSetup({headers: {'X-XSRF-TOKEN': decodeURIComponent(/XSRF-Token=([^;]*)/ig.exec(document.cookie)[1])}});
    $(document).ready(function () {
    	var user = '<?php echo (!empty($userData->user_image))? $userData->user_image :''; ?>';
         $.validator.addMethod('filesize', function (value, element, param) {
                return this.optional(element) || (element.files[0].size <= param)
            }, 'File size must be less than {0}');

            $.validator.addMethod("alphanumeric", function(value, element) {
                return this.optional(element) || /^[A-Za-z0-9]+$/.test(value);
            }, "Please enter Letters and numbers only");

            var form = $('#social-activity-form');
            form.validate({
                rules: {
                    facebook_link: {
                        url: true
                    },
                    instagram_link: {
                        url: true,
                    },
                    twitter_link: {
                        url: true,
                    },
                    linkedin_link: {
                        url: true,
                    },
                     pinterest_link: {
                        url: true,
                    },
                },        
              
            });
            $("#save-changes-button").click(function(){
            	if($("#social-activity-form").valid())
		          {
		          	let myForm = document.getElementById('social-activity-form');
		          	 myForm.submit();
		          } 
         }); 
        });

</script>
@endsection