{{-- Extends layout --}}
@extends('layout.default')
{{-- Content --}}
@section('content')
<style>
	.toggle.ios, .toggle-on.ios, .toggle-off.ios { border-radius: 20rem; }
	.toggle.ios .toggle-handle { border-radius: 20rem; }
	div#action-scout {
	    align-items: center;
	    justify-content: center;
	}
</style>
<div class="container">
    <div class="main-body">
    
          <div class="row gutters-sm">
            <div class="col-md-4 mb-3">
              <div class="card">
                <div class="card-body">
                  <div class="d-flex flex-column align-items-center text-center">
                  	@if (file_exists(public_path('users/'.$scoutData->id)) && $scoutData->user_image)
						<img src="{{ asset('users/'.$scoutData->id.'/'.$scoutData->user_image) }}" alt="Admin" class="rounded-circle" width="150" />
					@else
						<img src="{{ asset('users/user.png') }}" alt="Admin" class="rounded-circle" width="150" />
					@endif 
                    <div class="mt-3">
                      <h4>{{ $scoutData->user_fname .' '. $scoutData->user_lname}}</h4>
                      <p class="text-secondary mb-1">{{ $scoutData->email }}</p>
                      <p class="text-muted font-size-sm">{{ $scoutData->user_adress }}</p>  
                       <button class="btn btn-outline-primary act-sc " data-toggle="modal" data-target="#action-scout" data-action="1">Active</button>
                      <button class="btn btn-outline-primary act-sc" data-toggle="modal" data-target="#action-scout" data-action="2">Inactive</button>                        
					  <!-- <input type="checkbox" {{$scoutData->status == 1 ? 'checked' : ''}} data-toggle="toggle" data-target="#action-scout" data-style="ios" id="toggle-two"> -->
                    </div> 
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-8">
              <div class="card mb-3">
                <div class="card-body">
                  <div class="row">
                    <div class="col-sm-3">
                      <h6 class="mb-0">Full Name</h6>
                    </div>
                    <div class="col-sm-9 text-secondary">
                      {{ $scoutData->user_fname .' '. $scoutData->user_lname}}
                    </div>
                  </div>
                  <hr>
                  <div class="row">
                    <div class="col-sm-3">
                      <h6 class="mb-0">Email</h6>
                    </div>
                    <div class="col-sm-9 text-secondary">
                      {{ $scoutData->email}}
                    </div>
                  </div>
                  <hr>
                  <div class="row">
                    <div class="col-sm-3">
                      <h6 class="mb-0">Status</h6>
                    </div>
                    <div class="col-sm-9 text-secondary">
                    	<?php if($scoutData->status == 0){
                    		echo "Inactive";
                    	}else if($scoutData->status == 1){
                    		echo "Active";
                    	} ?>
                    </div>
                  </div>
                  <hr>
                  <div class="row">
                    <div class="col-sm-3">
                      <h6 class="mb-0">Mobile</h6>
                    </div>
                    <div class="col-sm-9 text-secondary">
                      {{ $scoutData->user_mobile}}
                    </div>
                  </div>
                  <hr>
                  <div class="row">
                    <div class="col-sm-3">
                      <h6 class="mb-0">Address</h6>
                    </div>
                    <div class="col-sm-9 text-secondary">
                      {{ $scoutData->user_address ? : 'Not available'}}
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /.modal -->
	    <div class="modal fade" id="action-scout">
	        <div class="modal-dialog">
	            <form id="action-scout-form" action="" method="post">
	                <div class="modal-content">
	                    <div class="modal-header">
	                        <h4 class="modal-title scout-title"></h4>
	                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	                        <span aria-hidden="true">&times;</span></button>
	                    </div>
	                    <div class="modal-body">
	                        <p id="cnt-modal"></p>
	                        <span id="dynInput"></span>
	                        @csrf
	                        <input type="hidden" name="status" id="status" value="{{ $scoutData->status }}">              
	                        <input type="hidden" name="scout_id" id="scout_id" value="<?php echo base64_encode($scoutData->id)?>">              
	                    </div>
	                    <div class="modal-footer justify-content-between">
	                        <button type="button" class="btn btn-secondary no-action" data-dismiss="modal">No</button>
	                        <button type="button" class="btn btn-secondary"data-dismiss="modal" onclick="formSubmit()">Yes</button>
	                    </div>
	                </div>
	            </form>
	        </div>
	    </div>
	    <!-- /.modal -->
    </div>

@endsection
{{-- Scripts Section --}}
@section('scripts')
<script type="text/javascript">
	$(function() {
		var checkAct = 0;
	    $(".act-sc").click(function(){ 
	    	var url = '',title = '', cnt_modal = '', pub_val = ''; 
	    	url = '{{ route("admin-update-scout-status") }}';  	
		   if($(this).data("action") == 1){        
           title = 'Active Scout';
           cnt_modal = 'Are you sure you want to Active this scout?'; 
           $('#status').val(1);
        }else if($(this).data("action") == 2){
           title = 'Inactive Scout';
           cnt_modal = 'Are you sure you want to Inactive this scout?'; 
           $('#status').val(2);
        }
		    $('.modal-title').html(title);   
		    $("#action-scout-form").attr('action', url);
		    $('#cnt-modal').html(cnt_modal);
		})
		$('.no-action').click(function(){
			// if(checkAct){
			// 	$('#toggle-two').parent().removeClass('toggle btn ios btn-light off');
			// 	$('#toggle-two').parent().addClass('toggle btn btn-primary ios');
			// }else{
			// 	$('#toggle-two').parent().removeClass('toggle btn btn-primary ios');
			// 	$('#toggle-two').parent().addClass('toggle btn ios btn-light off');
			// }
			$('#action-scout').css('opacity',0);
		    $('#action-scout').css('display','none');
		})
	});
  	function formSubmit(){
		$("#action-scout-form").submit();
	}
</script>
@endsection