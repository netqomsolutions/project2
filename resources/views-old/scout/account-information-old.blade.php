{{-- Extends layout --}}
@extends('layout.default')
{{-- Content --}}
@section('content')
<style>
	.toggle.ios, .toggle-on.ios, .toggle-off.ios { border-radius: 20rem; }
	.toggle.ios .toggle-handle { border-radius: 20rem; }
	div#action-scout {
	    align-items: center;
	    justify-content: center;
	}
</style>
 @if(session()->has('success'))
            <div class="alert alert-success alluser-page-suc" style="text-align: left;float: left;width: 100%;">
                {{ session()->get('success') }}
            </div>
            <script type="text/javascript">setTimeout(function(){ $('.alluser-page-suc').fadeOut(); }, 3000);</script>
        @elseif(session()->has('failure'))
            <div class="alert alert-danger alluser-page-suc" style="text-align: left; left;float: left;width: 100%;">
                {{ session()->get('failure') }}
            </div>
            <script type="text/javascript">setTimeout(function(){ $('.alluser-page-suc').fadeOut(); }, 3000);</script>
        @endif
    <div class="main-body">
          <!--  <div class="user-profile-buttons">
            <a href="{{ route('user-profile-edit') }}" class="navi-item">Update Profile</a>
            <a href="{{ route('user-change-password-form') }}" class="navi-item">Change Password</a>
          </div> -->
		  
		  
		  <div class="d-flex flex-row w-100">
								@include('profile-sidebar')
            <div class="content d-flex flex-column flex-column-fluid" id="kt_content">
                                    <!--begin::Subheader-->
                                    
                                    <!--end::Subheader-->
                                    <!--begin::Entry-->
                                    <div class="d-flex flex-column-fluid">
                                        <!--begin::Container-->
                                        <div class="container">
                                            <!-- begin::Card-->
                                            <div class="card card-custom overflow-hidden">
                                                <div class="card-body p-0">
                                                    <!-- begin: Invoice-->
                                                    <!-- begin: Invoice header-->
                                                   <!--  <div class="row justify-content-center bgi-size-cover bgi-no-repeat py-8 px-8 py-md-27 px-md-0" style="background-image: url(assets/media/bg/bg-6.jpg);">
                                                        <div class="col-md-9">
                                                            <div class="d-flex justify-content-between pb-10 pb-md-20 flex-column flex-md-row">
                                                                <h1 class="display-4 text-white font-weight-boldest mb-10">INVOICE</h1>
                                                                <div class="d-flex flex-column align-items-md-end px-0">
                                                                    <!--begin::Logo->
                                                                    <a href="#" class="mb-5">
                                                                        <img src="assets/media/logos/logo-light.png" alt="" />
                                                                    </a>
                                                                    <!--end::Logo->
                                                                    <span class="text-white d-flex flex-column align-items-md-end opacity-70">
                                                                        <span>Cecilia Chapman, 711-2880 Nulla St, Mankato</span>
                                                                        <span>Mississippi 96522</span>
                                                                    </span>
                                                                </div>
                                                            </div>
                                                            <div class="border-bottom w-100 opacity-20"></div>
                                                            <div class="d-flex justify-content-between text-white pt-6">
                                                                <div class="d-flex flex-column flex-root">
                                                                    <span class="font-weight-bolde mb-2r">DATA</span>
                                                                    <span class="opacity-70">Dec 12, 2017</span>
                                                                </div>
                                                                <div class="d-flex flex-column flex-root">
                                                                    <span class="font-weight-bolder mb-2">INVOICE NO.</span>
                                                                    <span class="opacity-70">GS 000014</span>
                                                                </div>
                                                                <div class="d-flex flex-column flex-root">
                                                                    <span class="font-weight-bolder mb-2">INVOICE TO.</span>
                                                                    <span class="opacity-70">Iris Watson, P.O. Box 283 8562 Fusce RD.
                                                                    <br />Fredrick Nebraska 20620</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div> -->
                                                    <!-- end: Invoice header-->
                                                    <!-- begin: Invoice body-->
                                                    <div class="row justify-content-center py-8 px-8 py-md-10 px-md-0">
                                                        <div class="col-md-9">
                                                            <div class="d-flex flex-wrap justify-content-between flex-column flex-md-row font-size-lg">
															<div class="font-weight-bolder font-size-lg mb-3 flex-wrap w-100">Card Information</div>
                                                                <div class="d-flex flex-column mb-10 mb-md-0">
																	<div class="d-flex flex-wrap">
                                                                    <div class="d-flex flex-wrap justify-content-between mb-3 col-md-6 col-sm-12 p-0">
                                                                        <span class="font-weight-bold w-100 mb-2">Card Number</span>
                                                                        <span class="text-left w-100">**** **** **** 2548</span>
                                                                    </div>
                                                                    <div class="d-flex flex-wrap justify-content-between mb-3 col-md-6 col-sm-12 p-0">
                                                                        <span class="font-weight-bold w-100 mb-2">Expiry Date</span>
                                                                        <span class="text-left w-100">5/22</span>
                                                                    </div>
                                                                    </div>
                                                                    
                                                                </div>
                                                                <div class="d-flex flex-column text-md-right">
                                                                    <!--span class="font-size-lg font-weight-bolder mb-1">TOTAL AMOUNT</span>
                                                                    <span class="font-size-h2 font-weight-boldest text-danger mb-1">$20.600.00</span>
                                                                    <span>Taxes Included</span-->
                                                                        <!--a href="#" class="btn btn-light-primary font-weight-bolder btn-sm pink-bg-btn">Edit</a-->
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!-- end: Invoice body-->
                                                    <!-- begin: Invoice footer-->

                                                    <div class="row justify-content-center bg-gray-100 py-8 px-8 py-md-10 px-md-0">
                                                        <div class="col-md-9">
                                                            <div class="d-flex justify-content-between flex-column flex-md-row font-size-lg flex-wrap">
                                                                    <div class="font-weight-bolder font-size-lg mb-3 w-100 d-flex justify-content-between align-items-center">Banking Information 
                                                                    @if(empty($userData->user_bank_detail))
                                                                     <a href="{{route('add-account-information')}}" class="btn btn-light-primary font-weight-bolder btn-sm pink-bg-btn">Add Detail</a>
                                                                     @endif
                                                                    </div>
                                                                    @if(!empty($userData->user_bank_detail))
                                                                <div class="d-flex flex-column mb-10 mb-md-0">
                                                                    <div class="d-flex justify-content-between mb-3">
                                                                        <span class="mr-15 font-weight-bold">Account Name:</span>
                                                                        <span class="text-right">Barclays UK</span>
                                                                    </div>
                                                                    <div class="d-flex justify-content-between mb-3">
                                                                        <span class="mr-15 font-weight-bold">Account Number:</span>
                                                                        <span class="text-right">1234567890934</span>
                                                                    </div>
                                                                    <div class="d-flex justify-content-between">
                                                                        <span class="mr-15 font-weight-bold">Code:</span>
                                                                        <span class="text-right">BARC0032UK</span>
                                                                    </div>
                                                                </div>
                                                                <div class="d-flex flex-column text-md-right">
                                                                    <!--span class="font-size-lg font-weight-bolder mb-1">TOTAL AMOUNT</span>
                                                                    <span class="font-size-h2 font-weight-boldest text-danger mb-1">$20.600.00</span>
                                                                    <span>Taxes Included</span-->
                                                                        <a href="#" class="btn btn-light-primary font-weight-bolder btn-sm pink-bg-btn">Edit</a>
                                                                </div>
                                                                @endif
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!-- end: Invoice footer-->
                                                    <!-- begin: Invoice action-->
                                                    <div class="row justify-content-center py-8 px-8 py-md-10 px-md-0">
                                                       <!--  <div class="col-md-9">
                                                            <div class="d-flex justify-content-between">
                                                                <button type="button" class="btn btn-light-primary font-weight-bold" onclick="window.print();">Download Invoice</button>
                                                                <button type="button" class="btn btn-primary font-weight-bold" onclick="window.print();">Print Invoice</button>
                                                            </div>
                                                        </div> -->
                                                    </div>
                                                    <!-- end: Invoice action-->
                                                    <!-- end: Invoice-->
                                                </div>
                                            </div>
                                            <!-- end::Card-->
                                        </div>
                                        <!--end::Container-->
                                    </div>
                                    <!--end::Entry-->
                                </div>
                            </div>
@endsection
{{-- Scripts Section --}}
@section('scripts')

@endsection