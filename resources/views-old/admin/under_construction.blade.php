{{-- Extends layout --}}
@extends('layout.default')

{{-- Content --}}
@section('content')

    {{-- Dashboard 1 --}}
    <style>
        .under-construction img {
    width: 100%;
    max-width: 600px;
    margin: 0 auto;
    display: block;
}
.under-construction p {
    max-width: 600px;
    margin: 0 auto;
    text-align: center;
    background-color: #afc578;
    font-size: 37px;
    padding: 20px 0;
    color: #fff;
}
    </style>

    
<div class="under-construction">
     <img alt="{{ config('APP_NAME') }}" src="{{ asset('front_end/images/img-under-construction.jpg') }}"/>
   <p> Under Construction! </p>
</div>
@endsection

