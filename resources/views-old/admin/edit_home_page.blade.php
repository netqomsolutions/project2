{{-- Extends layout --}}
@extends('layout.default')
@php
$exploreContent= json_decode($oldHomeData->explore_content,true);
$bannerContentArray= json_decode($oldHomeData->banner_content,true);
$i=1;
@endphp
<style>
    .error{
    color:red !important;
    }
</style>
<script src="https://cdn.ckeditor.com/4.12.1/standard/ckeditor.js"></script>
{{-- Content --}}
@section('content')
 <div class="row">
       @if(session()->has('success'))
            <div class="alert alert-success alluser-page-suc" style="text-align: left;float: left;width: 100%;">
                {{ session()->get('success') }}
            </div>
            <script type="text/javascript">setTimeout(function(){ $('.alluser-page-suc').fadeOut(); }, 3000);</script>
        @elseif(session()->has('failure'))
            <div class="alert alert-danger alluser-page-suc" style="text-align: left; left;float: left;width: 100%;">
                {{ session()->get('failure') }}
            </div>
            <script type="text/javascript">setTimeout(function(){ $('.alluser-page-suc').fadeOut(); }, 3000);</script>
        @endif
        
    </div>
<div class="row">
    <div class="col-lg-12">
        <div class="card card-custom">
            <!--begin::Form-->
            <form class="form" name="update-home-page" id="update-home-page" method="post" enctype="multipart/form-data" action="{{ route('admin-update-home-page') }}">
                <div class="card-body">
                    @csrf
                   <div class="section-heading-home text-center">
                    <h3> Banner Management</h3>
                   </div>
                    <div class="banner_content">
                        @foreach($bannerContentArray as $key=>$banner)
                        <div class="add_banner_html">
                             <div class="section-heading-home-inner">
                    <h5>Slide {{$i}}</h5>
                   </div>
                            <div class="form-group row">
                                <div class="col-lg-6 my-error">
                                    <label>Banner Heading:</label>
                                    <input type="text" class="form-control" value="{{isset($banner['banner_small_heading'])? $banner['banner_small_heading'] : '' }}" name="banner_small_heading[{{$i}}]" id="banner_small_heading" placeholder="Enter banner heading">
                                </div>
                                <div class="col-lg-6 my-error">
                                    <label>Banner Title:</label>
                                    <input type="text" class="form-control" value="{{isset($banner['banner_title'])? $banner['banner_title'] : '' }}" name="banner_title[{{$i}}]" id="banner_title" placeholder="Enter banner title" required>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-lg-6 my-error">
                                    <label>Banner Description:</label>
                                    <input type="text" class="form-control" value="{{isset($banner['banner_description'])? $banner['banner_description'] : '' }}" name="banner_description[{{$i}}]" id="banner_description" placeholder="Enter banner description">
                                </div>
                                <div class="col-lg-6 my-error">
                                    <label>Banner Image:</label>
                                    <input type="file" name="banner_image[{{$i}}]"  value="" id="banner_image" class="form-control banner_image" placeholder="Browse Image" accept="image/*" {{isset($banner['banner_image'])? ' ' : 'required' }}  />
                                    <input type="hidden" name="old_banner_image[{{$i}}]" value="{{isset($banner['banner_image'])? $banner['banner_image'] : '' }}"/>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-lg-6 my-error">
                                    <label>Banner Button Name:</label>
                                    <input type="text" class="form-control" value="{{isset($banner['banner_button_name'])? $banner['banner_button_name'] : '' }}" id="banner_button_name" name="banner_button_name[{{$i}}]" class="form-control" placeholder="Banner Button Name">
                                </div>
                                <div class="col-lg-6 my-error">
                                    <label>Banner Button Link:</label>
                                    <input type="url" class="form-control" value="{{isset($banner['banner_button_link'])? $banner['banner_button_link'] : '' }}" id="banner_button_link" name="banner_button_link[{{$i}}]" class="form-control" placeholder="Banner Button Link">
                                </div>
                            </div>
                            @if($key >0)
                            <div class="form-group row">
                                <div class="col-lg-6 my-error">
                                    <button type="button" class="btn btn-danger removeBanner">Remove</button>
                                </div>
                            </div>
                            @endif
                        </div>
                        @php
                        $i++;
                        @endphp
                        @endforeach
                    </div>
                     <div class="form-group row">
                        <div class="col-lg-12 my-error">
                            <button id="addMoreBanner" type="button" class="btn btn-sm float-right green-bg"> Add More Banner</button>
                        </div>
                    </div>
                     <div class="section-heading-home text-center">
                    <h3> Explore Management</h3>
                   </div>
                   <div class="home-explore">
                    <div class="form-group row">
                        <div class="col-lg-6 my-error">
                            <label>Explore Heading:</label>
                            <input type="text" class="form-control" value="{{isset($exploreContent['explore_small_heading'])? $exploreContent['explore_small_heading'] : '' }}" name="explore_small_heading" id="explore_small_heading" placeholder="Enter banner heading">
                        </div>
                        <div class="col-lg-6 my-error">
                            <label>Explore Title:</label>
                            <input type="text" class="form-control" value="{{isset($exploreContent['explore_title'])? $exploreContent['explore_title'] : '' }}" name="explore_title" id="explore_title" placeholder="Enter explore title" required>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-lg-6 my-error">
                            <label>Explore video Image:</label>
                            <input type="file" class="form-control" value="" name="explore_video_image" id="explore_video_image" placeholder="Enter explore video image" {{isset($exploreContent['explore_video_image'])? ' ' : 'required' }}>
                            <input type="hidden" name="old_explore_video_image_old" value="{{isset($exploreContent['explore_video_image'])? $exploreContent['explore_video_image'] : '' }}">
                        </div>
                        <div class="col-lg-6 my-error">
                            <label>Explore Video:</label>
                            <input type="file" class="form-control" value="" name="explore_video" id="explore_video" placeholder="Enter explore video" {{isset($exploreContent['explore_video'])? ' ' : 'required' }} >
                            <input type="hidden" name="old_explore_video" value="{{isset($exploreContent['explore_video'])? $exploreContent['explore_video'] : '' }}">
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-lg-6 my-error">
                            <label>Explore Background Image:</label>
                            <input type="file" name="explore_background_image"   value="" id="explore_background_image" class="form-control" placeholder="Browse Image" {{isset($exploreContent['explore_background_image'])? ' ' : 'required' }} >
                            <input type="hidden" name="old_explore_background_image" value="{{isset($exploreContent['explore_background_image'])? $exploreContent['explore_background_image'] : '' }}">
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-lg-6 my-error">
                            <label>Page Slug:</label>
                            <input type="text" class="form-control" value="{{$oldHomeData->slug}}"  name="slug" id="slug" placeholder="Enter slug">
                        </div>
                        <div class="col-lg-6 my-error">
                            <label>Page Meta:</label>
                            <input type="text" class="form-control" value="{{$oldHomeData->meta}}" name="meta" id="meta" placeholder="Enter page meta">
                        </div>
                    </div>
                    <div class="form-group row">
                         <div class="col-lg-12 my-error">
                            <label>Explore Description:</label>
                            <textarea name="explore_description"  id="explore_description">{{isset($exploreContent['explore_description'])? $exploreContent['explore_description'] : '' }}</textarea>
                        </div>
                    </div>
                </div>
            </div>
                <div class="card-footer">
                    <div class="row">
                        <div class="col-lg-6">
                            <button type="button" id="update-home-button" class="btn mr-2 green-bg">Save</button>
                        </div>
                    </div>
                </div>
            </form>
            <!--end::Form-->
        </div>
    </div>
</div>
@include('admin.edit_discover_connect')
@include('admin.partners')
@endsection
{{-- Scripts Section --}}
@section('scripts')
<script type="text/javascript">
     $(document).ready(function(){ 
        $.ajaxSetup({ headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') } });
     	 CKEDITOR.replace('explore_description', {
    filebrowserUploadUrl: "{{route('ckeditor.upload', ['_token' => csrf_token() ])}}",
    filebrowserUploadMethod: 'form'
});
     
         
          var i='<?= $i; ?>'; 
          var html=''; 
    $(document).on('click','#addMoreBanner',function(){
    	html =`<div class="add_banner_html">
                            <div class="section-heading-home-inner">
                                        <h5>Slide {{$i}}</h5>
                                       </div>
                        	<div class="form-group row">
    						<div class="col-lg-6 my-error">
    							<label>Banner Heading:</label>
    							<input type="text" class="form-control" value="" name="banner_small_heading[{{$i}}]" id="banner_small_heading" placeholder="Enter banner heading">
    						</div>
    						<div class="col-lg-6 my-error">
    							<label>Banner Title:</label>
    							<input type="text" class="form-control" value="" name="banner_title[{{$i}}]" id="banner_title" placeholder="Enter banner title" required>
    						</div>												
    					</div>
    					<div class="form-group row">
    						<div class="col-lg-6 my-error">
    							<label>Banner Description:</label>
    							<input type="text" class="form-control" value="" name="banner_description[{{$i}}]" id="banner_description" placeholder="Enter banner description">
    						</div>					
    						<div class="col-lg-6 my-error">
    							<label>Banner Image:</label>
    							<input type="file" name="banner_image[{{$i}}]"  value="" id="banner_image" class="form-control banner_image" placeholder="Browse Image" required />
    						</div>
    					</div>
                             <div class="form-group row">						
    						<div class="col-lg-6 my-error">
    							<label>Banner Button Name:</label>
    							<input type="text" class="form-control" value="" id="banner_button_name" name="banner_button_name[{{$i}}]" class="form-control" placeholder="Banner Button Name">
    						</div>	
                          <div class="col-lg-6 my-error">
    							<label>Banner Button Link:</label>
    							<input type="text" class="form-control" value="" id="banner_button_link" name="banner_button_link[{{$i}}]" class="form-control" placeholder="Banner Button Link">
    						</div>	
    						
    					</div>
    					<div class="form-group row">
    						<div class="col-lg-6 my-error">
    							<button type="button" class="btn btn-danger removeBanner">Remove</button>
    						</div>	
    							
    					</div>
    					</div>`;
        $('.banner_content').append(html).slideDown("slow");
         i++;
     });
     $(document).on('click', '.removeBanner', function(){  
     	$this =  $(this);
             $this.closest(".add_banner_html").remove();
    
        });  
      $("#update-home-page").validate({
            rules: {
                explore_video_image:{
                	accept: "image/jpeg,image/png,image/jpg",
                }, 
                explore_background_image:{
                	accept: "image/jpeg,image/png,image/jpg",
                },
                 explore_video:{
                	extension: "mpeg|ogg|mp4|webm|3gp|mov|flv|avi|wmv|ts",
    
                },
            }
           
        });
        $("body").on('click', '#update-home-button', function() {
        	$("input.banner_image").each(function(){
           $(this).rules("add", {
               accept: "image/jpeg,image/png,image/jpg"
           });                    
     });
        	if($("#update-home-page").valid())
    	{
    		let myForm = document.getElementById('update-home-page');
    		myForm.submit();
    	}
        });
     $("#add-partners-form").validate({
            rules: {
                name:{
                  required: true
                }, 
                link:{
                	  required: true
                },
                 image:{
                	  required: true,
                	  accept: "image/jpeg,image/png,image/jpg",
    
                },
            }
           
        });

      $("body").on('click', '#add-partner-button', function() {
        		if($("#add-partners-form").valid())
    	{
    		let myForm = document.getElementById('add-partners-form');
    		myForm.submit();
    	}
    });

      var table = $('.yajra-datatable').DataTable({
          	columnDefs: [{
              targets: 0,
              className: 'logo-background'
            }],
	        processing: true,
	        serverSide: true,
	        ajax: "{{ route('admin-partners') }}",
	        columns: [
	            {data: 'DT_RowIndex', id: 'DT_RowIndex',orderable: false,render:function(data,type,row){	       
	            	var imagUrl = '';
	            	
	            		imagUrl = HOST_URL + '/public/pages/partners/'+row.image;
	            		return "<img src=\"" + imagUrl + "\" height=\"50\"/>";
	            	            
	            }},
	            {data: 'name', name: 'name'},
	            {data: 'link', name: 'link'},
	            {
	                data: 'action', 
	                name: 'action', 
	                orderable: false, 
	                searchable: true
	            },
	        ],
	        order: [[1, 'asc']],
            fnDrawCallback: function( oSettings ) {
                /* 01-02-2021 @RT Change the delete popup design , using by bootstrap-confirmation.min.js */
                    toastr.options.timeOut = 1500; 
                $('.action-partner-btn').confirmation({
                         content: "You want to delete this.",
                         onConfirm: function() {
                           var partner_id=$(this).attr('partner_id');
                              $.ajax({
                                url: `{{ route("admin-delete-partner") }}`,
                                type: "POST",
                                data: { partner_id : partner_id},
                                dataType: "Json",
                                success: function (data) {
                                toastr.options.timeOut = 1500;
                                if(data.isSucceeded){
                                toastr.success(data.message);
                                $('.yajra-datatable').DataTable().ajax.reload();
                                }else{
                                    toastr.error("Please try again");  
                                }
                                },
                                error: function (xhr, ajaxOptions, thrownError) {
                                  toastr.error("Please try again");
                                }
                              });
                            }

                     });
            }
	    });

             
    });
    $(document).ready(function () {
    $('#page-add-blog-form').validate({
        ignore: [],
        submitHandler: function(form) {
          // do other things for a valid form
          form.submit();
        },
        rules: {
            section_one_title: {
                required: true
            }, 
            section_two_title: {
                required: true
            }, 
            section_three_title: {
                required: true
            }, 
            section_one_description: {
                required: true,
                maxlength:200,
            }, 
            section_two_description: {
                required: true,
                maxlength:200,
            }, 
            section_three_description: {
                required: true,
                maxlength:200,
            },
        },
        errorElement: 'span',           
        errorPlacement: function (error, element) {
          error.addClass('invalid-feedback');
          element.closest('.my-error').append(error);
        },
        highlight: function (element, errorClass, validClass) {
          $(element).addClass('is-invalid');
        },
        unhighlight: function (element, errorClass, validClass) {
          $(element).removeClass('is-invalid');
        }
    });
});


</script>
@endsection