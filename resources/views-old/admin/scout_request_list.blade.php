{{-- Extends layout --}}
@extends('layout.default')
{{-- Content --}}
@section('content')
<style type="text/css">
	.dataTables_wrapper .dataTable th:before {content:none !important;}
	.dataTables_wrapper .dataTable th:after {content:none !important;}
</style>
<div class="row">
    <div class="col-lg-12">
    	<meta name="csrf-token" content="{{ csrf_token() }}" />
       	<!--begin::Card-->
       	@if (session('success'))
		    <div class="alert alert-success scout-list-success">
		        {{ session('success') }}
		    </div>
		@elseif(session('failure'))
			<div class="alert alert-danger scout-list-success">
		        {{ session('failure') }}
		    </div>			    
		@endif
		<script type="text/javascript">setTimeout(function(){ $('.scout-list-success').fadeOut(); }, 3000);</script>
		<div class="card card-custom">
			<div class="card-header">
				<div class="card-title">
					<span class="card-icon">
						<i class="flaticon2-supermarket text-primary"></i>
					</span>
					<h3 class="card-label">List Of Scouts</h3>
				</div>
			</div>
			<div class="card-body">
				<!--begin: Datatable-->
				<table class="table table-bordered yajra-datatable table-responsive-sm" style="margin-top: 13px !important">
					<thead>
						<tr>
							<th>AVATAR</th>
							<th>NAME</th>
							<th>EMAIL</th>
							<th>ACTIONS</th>
						</tr>
					</thead>
				</table>
				<!--end: Datatable-->
				<!-- /.modal -->
			    <div class="modal fade" id="delete-scout">
			        <div class="modal-dialog">
			            <form id="delete-scout-form" action="" method="post">
			                <div class="modal-content bg-danger">
			                    <div class="modal-header">
			                        <h4 class="modal-title scout-title">Delete Lead</h4>
			                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
			                        <span aria-hidden="true">&times;</span></button>
			                    </div>
			                    <div class="modal-body">
			                        <p id="cnt-modal"></p>
			                        <span id="dynInput"></span>
			                        @csrf
			                        <input type="hidden" name="scout_id" value="" id="scout_id">              
			                    </div>
			                    <div class="modal-footer justify-content-between">
			                        <button type="button" class="btn btn-outline-light" data-dismiss="modal">No</button>
			                        <button type="button" class="btn btn-outline-light"data-dismiss="modal" onclick="formSubmit()">Yes</button>
			                    </div>
			                </div>
			            </form>
			        </div>
			    </div>
			    <!-- /.modal -->
			</div>
		</div>
		<!--end::Card-->
    </div>
</div>
@endsection
{{-- Scripts Section --}}
@section('scripts')
<script type="text/javascript">
  	
  	$(document).ready(function(){
  		var table = $('.yajra-datatable').DataTable({
	        processing: true,
	        serverSide: true,
	        ajax: "{{ route('admin-scouts-request') }}",
	        columns: [
	            {data: 'DT_RowIndex', id: 'DT_RowIndex' , orderable: false,render:function(data,type,row){	       
	            	var imagUrl = '';
	            	if(row.user_image == null){	       
	            		imagUrl = HOST_URL + '/public/users/user.png';
	            		return "<img src=\"" + imagUrl + "\" height=\"50\"/>";
	            	}else{
	            		imagUrl = HOST_URL + '/public/'+row.user_image_path+'/'+row.user_image;
	            		return "<img src=\"" + imagUrl + "\" height=\"50\"/>";
	            	}	            
	            }},
	            {data: 'user_fname', name: 'user_fname',render:function(data, type, row){ 
	             return  row.user_fname +' '+ row.user_lname;
	            }},
	            {data: 'email', name: 'email'},
	            {
	                data: 'action', 
	                name: 'action', 
	                orderable: false, 
	                searchable: true
	            },
	        ],
	        order: [[1, 'asc']]
	    });
	    $(document).on("click",".action-scout-btn",function(){
		    var url = '',title = '', cnt_modal = '', pub_val = '';
		    if($(this).attr('act-name') == 'del-scout'){
		      	url = '{{ route("admin-delete-scout") }}';
		      	title = 'Delete Scout';
		      	cnt_modal = 'Are you sure you want to delete this scout permanently?';  
		    }
		    $('.modal-title').html(title);   
		    $("#delete-scout-form").attr('action', url);
		    $('#cnt-modal').html(cnt_modal);
		    $('#scout_id').val($(this).attr('scout_id'));
		});
  	});

	function formSubmit(){
		$("#delete-scout-form").submit();
	}

</script>
@endsection