{{-- Extends layout --}}
@extends('layout.default')

{{-- Content --}}
@section('content')

    {{-- Dashboard 1 --}}

    <div class="row">
        @if(session()->has('success'))
            <div class="alert alert-success alluser-page-suc" style="text-align: left;float: left;width: 100%;">
                {{ session()->get('success') }}
            </div>
            <script type="text/javascript">setTimeout(function(){ $('.alluser-page-suc').fadeOut(); }, 3000);</script>
        @elseif(session()->has('failure'))
            <div class="alert alert-danger alluser-page-suc" style="text-align: left; left;float: left;width: 100%;">
                {{ session()->get('failure') }}
            </div>
            <script type="text/javascript">setTimeout(function(){ $('.alluser-page-suc').fadeOut(); }, 3000);</script>
        @endif
        
    </div>
<div class="row justify-content-between">
   <div class="px-6 py-8 rounded-xl custom-width-boxes">
                                                            <span class="svg-icon svg-icon-3x svg-icon-warning d-block my-2">
                                                                <!--begin::Svg Icon | path:/metronic/theme/html/demo1/dist/assets/media/svg/icons/Media/Equalizer.svg-->
                                                              10
                                                                <!--end::Svg Icon-->
                                                            </span>
                                                            <a href="#" class=" font-weight-bold font-size-h6">Total Scouts</a>
                                                        </div>
                                                        <div class="px-6 py-8 rounded-xl custom-width-boxes">
                                                            <span class="svg-icon svg-icon-3x svg-icon-warning d-block my-2">
                                                                <!--begin::Svg Icon | path:/metronic/theme/html/demo1/dist/assets/media/svg/icons/Media/Equalizer.svg-->
                                                             10
                                                                <!--end::Svg Icon-->
                                                            </span>
                                                            <a href="#" class="font-weight-bold font-size-h6">Total Travelers</a>
                                                        </div>
                                                        <div class="px-6 py-8 rounded-xl custom-width-boxes">
                                                            <span class="svg-icon svg-icon-3x svg-icon-warning d-block my-2">
                                                                <!--begin::Svg Icon | path:/metronic/theme/html/demo1/dist/assets/media/svg/icons/Media/Equalizer.svg-->
                                                               10
                                                                <!--end::Svg Icon-->
                                                            </span>
                                                            <a href="#" class="font-weight-bold font-size-h6">Total Experiences</a>
                                                        </div>
</div>
@endsection

{{-- Scripts Section --}}
@section('scripts')
    <script src="{{ asset('js/pages/widgets.js') }}" type="text/javascript"></script>
@endsection
