{{-- Extends layout --}}
@extends('layout.default')
<script src="https://cdn.ckeditor.com/4.12.1/standard/ckeditor.js"></script>
{{-- Content --}}
@section('content')
<div class="row">
     @if(session()->has('success'))
            <div class="alert alert-success alluser-page-suc" style="text-align: left;float: left;width: 100%;">
                {{ session()->get('success') }}
            </div>
            <script type="text/javascript">setTimeout(function(){ $('.alluser-page-suc').fadeOut(); }, 3000);</script>
        @elseif(session()->has('failure'))
            <div class="alert alert-danger alluser-page-suc" style="text-align: left; left;float: left;width: 100%;">
                {{ session()->get('failure') }}
            </div>
            <script type="text/javascript">setTimeout(function(){ $('.alluser-page-suc').fadeOut(); }, 3000);</script>
        @endif
    <div class="col-lg-12">
        <div class="card card-custom">
			<!--begin::Form-->
			<form class="form" id="page-contact-us" method="post" enctype="multipart/form-data" action="{{ route('admin-update-setting') }}">
				<div class="card-body">@csrf
					<div class="form-group row">
						<div class="col-lg-12 my-error">
							<label>Scout Commission (%)</label>
							<input type="text" class="form-control" value="{{ getSettingMeta('scout_commission')}}" name="scout_commission" id="scout_commission" placeholder="Enter Scout Percentage (%)" >
						</div>
						<div class="col-lg-12 my-error">
							<label>Admin Commission (%)</label>
               <input type="text" class="form-control" value="{{ getSettingMeta('admin_commission')}}" name="admin_commission" id="admin_commission" placeholder="Enter Admin Percentage (%)" > 
						</div>	

            <div class="col-lg-6 my-error">
              <label>Google Analytics Script</label>
              <textarea type="text" class="form-control" value="" name="header_tags" id="header_tags" placeholder="Enter Google Analytics Script" rows="10">{{ getSettingMeta('header_tags')}}</textarea>
            </div>
            <div class="col-lg-6 my-error">
              <label>Google Tag Manager Script</label>
               <textarea type="text" class="form-control" value="" name="footer_tags" id="footer_tags" placeholder="Enter Tag Manager Script" rows="10">{{ getSettingMeta('footer_tags')}}</textarea>
            </div>  					
					</div>
					<div class="form-group row">
						<div class="col-lg-6">
							<button type="submit" class="btn green-bg mr-2">Save</button>
							<!-- <button type="back" class="btn btn-secondary">Back</button> -->
						</div>	
					</div>
				</div>
			</form>
			<!--end::Form-->
		</div>
	</div>
</div>

@endsection
{{-- Scripts Section --}}
@section('scripts')

@endsection