<div class="row">
    <div class="col-lg-12">
    	 <div class="section-heading-home text-center">
                    <h3>Social Media Management</h3>
                   </div>
        <div class="card card-custom">
			<!--begin::Form-->
			<form class="form" id="page-add-blog-form" method="post" enctype="multipart/form-data" action="{{ route('admin-update-cms-pages') }}">
				<div class="card-body">@csrf
					<input type="hidden" name="page_id" value="7"/>
					<input type="hidden" name="page_title" value="Site Social Links"/>
					<div class="form-group row">
						<div class="col-lg-6 my-error">
							<label>Facebook:</label>
							<input type="url" class="form-control" value="{{ isset($LinkData) ? $LinkData->facebook : '' }}" name="facebook" id="facebook" placeholder="Enter Facebook Url">
						</div>
					   	<div class="col-lg-6 my-error">
							<label>Instagram:</label>
							<input type="url" class="form-control" value="{{ isset($LinkData) ? $LinkData->instagram : '' }}" name="instagram" id="instagram" placeholder="Enter Instagram url">
						</div>
							
					</div>
					<div class="form-group row">
						<div class="col-lg-6 my-error">
							<label>Twitter:</label>
							<input type="url" class="form-control" value="{{ isset($LinkData) ? $LinkData->twitter : '' }}" name="twitter" id="twitter" placeholder="Enter Twitter Url">
						</div>
					   <div class="col-lg-6 my-error">
							<label>Linkedin:</label>
							<input type="url" class="form-control" value="{{ isset($LinkData) ? $LinkData->linkedin : '' }}" name="linkedin" id="linkedin" placeholder="Enter Linkedin Url">
						</div>
							
					</div>
					
                <div class="form-group row">
                	<div class="col-lg-6">
							<button type="submit" class="btn green-bg mr-2">Save</button>
							
						</div>
                </div>
					
				</div>
			</form>
			<!--end::Form-->
		</div>
	</div>
</div>