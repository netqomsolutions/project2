{{-- Extends layout --}}
@extends('layout.default')
{{-- Content --}}
@section('content')
@php
if(!empty($experience->review_ratings))
{
   $newArr = array_column($experience->review_ratings->toArray(),'review_type');
$forExp=getTravelerReview($experience->id,0,Auth::user()->id);
$forHost=getTravelerReview($experience->id,4,Auth::user()->id);
$forScout=getTravelerReview($experience->id,3,Auth::user()->id);

}

@endphp
@php
	$profile_pic = 'users/user.png';

	$profile_path = Auth::user()->user_image_path;

	$profile = Auth::user()->user_image;

	if($profile && $profile_path){
		$profile_pic = $profile_path.'/'.$profile;
	}
@endphp
<style>
	.toggle.ios, .toggle-on.ios, .toggle-off.ios { border-radius: 20rem; }
	.toggle.ios .toggle-handle { border-radius: 20rem; }
	div#action-scout {
	    align-items: center;
	    justify-content: center;
	}
</style>
@if(session()->has('success'))
            <div class="alert alert-success alluser-page-suc" style="text-align: left;float: left;width: 100%;">
                {{ session()->get('success') }}
            </div>
            <script type="text/javascript">setTimeout(function(){ $('.alluser-page-suc').fadeOut(); }, 3000);</script>
        @elseif(session()->has('failure'))
            <div class="alert alert-danger alluser-page-suc" style="text-align: left; left;float: left;width: 100%;">
                {{ session()->get('failure') }}
            </div>
            <script type="text/javascript">setTimeout(function(){ $('.alluser-page-suc').fadeOut(); }, 3000);</script>
        @endif
       <div class="d-flex flex-row w-100">
   <div class="d-flex flex-column">
      <div class="card card-custom gutter-b">
         <div class="card-body">
            <!--begin::Details-->
            <div class="d-flex mb-9">
               <!--begin: Pic-->
               <div class="flex-shrink-0 mr-7 mt-lg-0 mt-3">
                  <div class="symbol symbol-50 symbol-lg-120">
                     <img src="{{ asset('pages/experiences/'.$experience->experience->experience_feature_image)}}" alt="image">
                  </div>
                  <div class="symbol symbol-50 symbol-lg-120 symbol-primary d-none">
                     <span class="font-size-h3 symbol-label font-weight-boldest">JM</span>
                  </div>
               </div>
               <!--end::Pic-->
               <!--begin::Info-->
               <div class="flex-grow-1">
                  <!--begin::Title-->
                  <div class="d-flex justify-content-between flex-wrap mt-1">
                     <div class="d-flex mr-3">
                        <a href="{{ route('experience-detail',base64_encode($experience->experience->id)) }}" class="text-dark-75 text-hover-primary font-size-h5 font-weight-bold mr-3">{{$experience->experience->experience_name}}
                        </a>
                        <a href="{{ route('experience-detail',base64_encode($experience->experience->id)) }}">
                        <i class="flaticon2-correct text-success font-size-h5"></i>
                        </a>
                     </div>
                  </div>
                  <!--end::Title-->
                  <!--begin::Content-->
                  <div class="d-flex flex-wrap justify-content-between mt-1">
                     <div class="d-flex flex-column flex-grow-1 pr-8">
                        <div class="d-flex flex-wrap mb-4">
                           <a href="#" class="text-dark-50 text-hover-primary font-weight-bold mr-lg-8 mr-5 mb-lg-0 mb-2">
                           <i class="flaticon2-new-email mr-2 font-size-lg" style="
                              font-style: normal;
                              "></i>{{$experience->experience->user->email}}</a>
                           <a href="#" class="text-dark-50 text-hover-primary font-weight-bold mr-lg-8 mr-5 mb-lg-0 mb-2">
                           <i class="fas fa-euro-sign" style="
                              margin-right: 5px;
                              "></i>{{ number_format($experience->experience->experience_low_price) }} {{ $experience->experience_high_price != 0.00 ? "- ".number_format($experience->experience->experience_high_price) : ''  }} valid for: 1 - {{$experience->experience->experience_price_vailid_for}} guests</a>
                           <a href="#" class="text-dark-50 text-hover-primary font-weight-bold mr-lg-8 mr-5 mb-lg-0 mb-2">
                           <i class="fas fa-user-plus" aria-hidden="true" style="
                              margin-right: 5px;
                              "></i>Extra guest: {{$experience->experience->experience_additional_person}} EUR</a>
                           <a href="#" class="text-dark-50 text-hover-primary font-weight-bold">
                           <i class="far fa-clock" aria-hidden="true" style="
                              margin-right: 5px;
                              "></i>Duration:{{$experience->experience->experience_duration}} h
                           </a>
                        </div>
                        <span class="font-weight-bold text-dark-50">{{$experience->experience->experience_description}}</span>
                        <span class="font-weight-bold text-dark-50"></span>
                     </div>
                  </div>
                  <!--end::Content-->
               </div>
               <!--end::Info-->
            </div>
            <!--end::Details-->
            <div class="separator separator-solid"></div>
            <!--begin::Items-->
            <div class="d-flex align-items-center flex-wrap mt-4">
               <!--begin::Item-->
               <div class="d-flex align-items-center flex-lg-fill mr-5 mb-2">
                  <span class="mr-4">
                  <i class="far fa-user display-4 font-weight-bold"></i>
                  </span>
                  <div class="d-flex flex-column">
                     <span class="font-weight-bolder font-size-lg">Max guests</span>
                     <span class="font-weight-bolder font-size-h5">
                     <span class=" font-weight-bold"></span>{{$experience->experience->maximum_participant}}
                     </span>
                  </div>
               </div>
               <!--end::Item-->
               <!--begin::Item-->
               <div class="d-flex align-items-center flex-lg-fill mr-5 mb-2">
                  <span class="mr-4">
                  <i class="far fa-clock display-4 font-weight-bold"></i>
                  </span>
                  <div class="d-flex flex-column">
                     <span class="font-weight-bolder font-size-lg">Book in advance</span>
                     <span class="font-weight-bolder font-size-h5">
                     <span class=" font-weight-bold"></span>{{$experience->experience_booking_in_advance_time}} h
                     </span>
                  </div>
               </div>
               <!--end::Item-->
               <!--begin::Item-->
               <div class="d-flex align-items-center flex-lg-fill mr-5 mb-2">
                  <span class="mr-4">
                  <i class="far fa-comment-dots display-4 font-weight-bold"></i>
                  </span>
                  <div class="d-flex flex-column">
                     <span class="font-weight-bolder font-size-lg">Comments</span>
                     <a href="{{ route('experience-detail',base64_encode($experience->id)) }}" class="text-primary font-weight-bolder" style="color: #3F4254!important;">View</a>
                  </div>
               </div>
               <!--end::Item-->
            </div>
            <!--begin::Items-->
         </div>
      </div>
      @if(!in_array(0, $newArr) || !in_array(3, $newArr) || !in_array(4, $newArr))
      <div class="d-flex flex-wrap">
         <div class="w-100">
            <div class="custom-style-heading col-md-4">
               <h2>Write a review</h2>
            </div>
         </div>
         @if(!in_array(0, $newArr))
         <div class="col-md-4 pl-0" style="display:<?= (in_array(0, $newArr))? 'none' : 'block';?>">
            <!--begin::Card-->
            <div class="card card-custom gutter-b example example-compact experince-review-for-exp">
               <div class="card-header">
                  <h3 class="card-title">Please Enter Your Review For Experience</h3>
               </div>
               <!--begin::Form-->
               <form data-iscompleted="{{ $forMCheck['is-completed-exp'] }}" id="experince-review-for-exp" name="experince-review-for-exp" class="review-form {{ $forMCheck['experience-form'] }}" method="post">
                  <div class="card-body">
                     <div class="feedback-form for-experience">
                        <div class="form-group word_sec">
                           <div class="rating_top rating-stars">
                              <ul class="stars">
                                 <li class='star' title='Poor' data-value='1'><i class='fa fa-star fa-fw'></i></li>
                                 <li class='star' title='Fair' data-value='2'><i class='fa fa-star fa-fw'></i></li>
                                 <li class='star' title='Good' data-value='3'><i class='fa fa-star fa-fw'></i></li>
                                 <li class='star' title='Excellent' data-value='4'><i class='fa fa-star fa-fw'></i></li>
                                 <li class='star' title='WOW!!!' data-value='5'><i class='fa fa-star fa-fw'></i></li>
                              </ul>
                           </div>
                        </div>
                     </div>
                     <div class="form-group mb-1">
                        <label for="exampleTextarea">Write Review For Experience<span class="text-danger">*</span></label>
                         <input type="hidden" name="star_value" class="star-value">
                            <input type="hidden" name="experience_id" value="{{ $experience->experience->id }}">
                            <input type="hidden" name="booking_id" value="{{ $experience->id }}">
                            <input type="hidden" name="review_for" value="0">
                            <input type="hidden" name="user_type"  value="0">
                        <textarea class="inner_textarea form-control" placeholder="Write Review for Experince" id="review-experince-text" name="review_text" ></textarea>
                     </div>
                     <!--begin: Code-->
                     <!--end: Code-->
                  </div>
                  <div class="card-footer">
                     <button data-form="#experince-review-for-exp" data-form-name="experince-review-for-exp" class="btn btn-primary mr-2 submitthis" id="review-experince-btn" type="button">Save
                     </button>
                     <div class="review-loader" style="display: none;">
                        <img src="https://media.tenor.com/images/47f855960d5dc83774d7b3b428964c93/tenor.gif">
                     </div>
                  </div>
               </form>
               <!--end::Form-->
            </div>
           
         </div>
         @endif
         @if(!in_array(4, $newArr))
         <div class="col-md-4" style="display:<?= (in_array(4, $newArr))? 'none' : 'block';?>">
            <!--begin::Card-->
            <div class="card card-custom gutter-b example example-compact experince-review-for-host">
               <div class="card-header">
                  <h3 class="card-title">Please Enter Your Review For Host</h3>
               </div>
               <!--begin::Form-->
               <form data-iscompleted="{{ $forMCheck['is-completed-host'] }}" id="experince-review-for-host" name="experince-review-for-host" class="review-form {{ $forMCheck['host-form'] }}" method="post">
                  <div class="card-body">
                     <div class="feedback-form">
                        <div class="form-group word_sec">
                           <div class="rating_top rating-stars">
                              <ul class="stars">
                                 <li class='star' title='Poor' data-value='1'><i class='fa fa-star fa-fw'></i></li>
                                 <li class='star' title='Fair' data-value='2'><i class='fa fa-star fa-fw'></i></li>
                                 <li class='star' title='Good' data-value='3'><i class='fa fa-star fa-fw'></i></li>
                                 <li class='star' title='Excellent' data-value='4'><i class='fa fa-star fa-fw'></i></li>
                                 <li class='star' title='WOW!!!' data-value='5'><i class='fa fa-star fa-fw'></i></li>
                              </ul>
                           </div>
                        </div>
                     </div>
                     <div class="form-group mb-1">
                        <label for="exampleTextarea">Write Review For Host<span class="text-danger">*</span></label>
                         <input type="hidden" name="star_value" class="star-value">
                             <input type="hidden" name="experience_id" value="{{ $experience->experience->id }}">
                            <input type="hidden" name="booking_id" value="{{ $experience->id }}">
                            <input type="hidden" name="review_for" value="{{ $experience->experience->assigned_host }}">
                            <input type="hidden" name="user_type"  value="4">
                         <textarea  placeholder="Write Review for Host" id="review-review-text" name="review_text" class=" form-control inner_textarea"></textarea>
                         <div class="error-text"></div>

                     </div>
                     <!--begin: Code-->
                     <!--end: Code-->
                  </div>
                  <div class="card-footer">
                      <button data-form="#experince-review-for-host" data-form-name="experince-review-for-host" class="btn btn-primary mr-2 submitthis"  id="review-review-btn" type="button">Save</button>
                            <div class="review-loader" style="display: none;">
                             <img src="https://media.tenor.com/images/47f855960d5dc83774d7b3b428964c93/tenor.gif">
                            </div>
                  </div>
               </form>
               <!--end::Form-->
            </div>
          
         </div>
         @endif
         @if(!in_array(3, $newArr))
         <div class="col-md-4 pr-0"  style="display:<?= (in_array(3, $newArr))? 'none' : 'block';?>">
            <!--begin::Card-->
            <div class="card card-custom gutter-b example example-compact experince-review-for-scout">
               <div class="card-header">
                  <h3 class="card-title">Please Enter Your Review For Scout</h3>
               </div>
               <!--begin::Form-->
               <form data-iscompleted="{{ $forMCheck['is-completed-scout'] }}" id="experince-review-for-scout" name="experince-review-for-scout" class="review-form {{ $forMCheck['scout-form'] }}"  method="post">
                  <div class="card-body">
                     <div class="feedback-form">
                        <div class="form-group word_sec">
                           <div class="rating_top rating-stars">
                              <ul class="stars">
                                 <li class='star' title='Poor' data-value='1'><i class='fa fa-star fa-fw'></i></li>
                                 <li class='star' title='Fair' data-value='2'><i class='fa fa-star fa-fw'></i></li>
                                 <li class='star' title='Good' data-value='3'><i class='fa fa-star fa-fw'></i></li>
                                 <li class='star' title='Excellent' data-value='4'><i class='fa fa-star fa-fw'></i></li>
                                 <li class='star' title='WOW!!!' data-value='5'><i class='fa fa-star fa-fw'></i></li>
                              </ul>
                           </div>
                        </div>
                     </div>
                     <div class="form-group mb-1">
                        <label for="exampleTextarea">Write Review For Scout<span class="text-danger">*</span></label>
                        <input type="hidden" name="star_value" class="star-value">
                             <input type="hidden" name="experience_id" value="{{ $experience->experience->id }}">
                            <input type="hidden" name="booking_id" value="{{ $experience->id }}">
                            <input type="hidden" name="review_for" value="{{ $experience->experience->user_id }}">
                            <input type="hidden" name="user_type"  value="3">
                      
                         <textarea placeholder="Write Review for Scout" id="scout-review-text" name="review_text" class="form-control inner_textarea"></textarea>
                     </div>
                     <!--begin: Code-->
                     <!--end: Code-->
                  </div>
                  <div class="card-footer">
                     <button data-form="#experince-review-for-scout" data-form-name="experince-review-for-scout" class="btn btn-primary mr-2 submitthis" id="scout-review-btn" type="button">Save</button>
                            <div class="review-loader" style="display: none;">
                             <img src="https://media.tenor.com/images/47f855960d5dc83774d7b3b428964c93/tenor.gif">
                            </div> 
                  </div>
               </form>
               <!--end::Form-->
            </div>
            <!--end::Card-->
            <!--begin::Card-->
            <!--end::Card-->
            <!--begin::Card-->
            <!--end::Card-->
         </div>
         @endif
      </div>
      @else
      <div class="w-100">
         <div class="custom-style-heading col-md-4">
            <h2>Your Reviews</h2>
         </div>
      </div>
      <div class="card card-custom gutter-b">
         <div class="card-body">
            <ul class="nav nav-tabs" id="myTab" role="tablist">
               <li class="nav-item">
                  <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home">
                     <!--span class="nav-icon">
                        <i class="flaticon2-chat-1"></i>
                        </span-->
                     <span class="nav-text">Experience Reviews</span>
                  </a>
               </li>
               <li class="nav-item">
                  <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" aria-controls="profile">
                     <!--span class="nav-icon">
                        <i class="flaticon2-layers-1"></i>
                        </span-->
                     <span class="nav-text">Host Reviews</span>
                  </a>
               </li>
               <li class="nav-item">
                  <a class="nav-link" id="contact-tab" data-toggle="tab" href="#contact" aria-controls="contact">
                     <!--span class="nav-icon">
                        <i class="flaticon2-rocket-1"></i>
                        </span-->
                     <span class="nav-text">Scout Reviews</span>
                  </a>
               </li>
            </ul>
            <div class="tab-content mt-5" id="myTabContent">
               <div class="tab-pane fade active show" id="home" role="tabpanel" aria-labelledby="home-tab">
                  
                  <div class="d-flex mb-9 border-bottom pb-9">
                     <!--begin: Pic-->
                      @if(in_array(0, $newArr))
                     <div class="flex-shrink-0 mr-7 mt-lg-0 mt-3">
                        <div class="symbol symbol-50 symbol-lg-60">
                           <img src="{{ asset($profile_pic) }}" alt="image">
                        </div>
                        <div class="symbol symbol-50 symbol-lg-120 symbol-primary d-none">
                           <span class="font-size-h3 symbol-label font-weight-boldest">JM</span>
                        </div>
                     </div>
                     <!--end::Pic-->
                     <!--begin::Info-->
                     <div class="flex-grow-1">
                        <!--begin::Title-->
                        <div class="d-flex justify-content-between flex-wrap mt-1">
                           <div class="d-flex mr-3">
                              <a href="#" class="text-dark-75 text-hover-primary font-size-h5 font-weight-bold mr-3">{{$experience->experience->user->user_fname .' '.$experience->experience->user->user_lname}}</a>
                              <div class="d-flex align-items-center traveler-ratings-tabs">
                                 {{$forExp->rating}}
                                 <ul>
                                    <li class="filled-star"><i class="fas fa-star" aria-hidden="true"></i></li>
                                 </ul>
                              </div>
                           </div>
                        </div>
                        <!--end::Title-->
                        <!--begin::Content-->
                        <div class="d-flex flex-wrap justify-content-between mt-1">
                           <div class="d-flex flex-column flex-grow-1 pr-8">
                              <div class="d-flex flex-wrap mb-4 justify-content-between">
                                 <a href="#" class="text-dark-50 text-hover-primary font-weight-bold mr-lg-8 mr-5 mb-lg-0 mb-2">{{$experience->experience->user->user_address}}</a>
                                 <span class="text-green">{{date('d, M, Y',strtotime($forExp->created_at))}}</span>
                              </div>
                              <span class="font-weight-bold text-dark-50">{{$forExp->review}}</span>
                              <span class="font-weight-bold text-dark-50"></span>
                           </div>
                        </div>
                        <!--end::Content-->
                     </div>
                     @else
                      <p> You not giving review to experience</p>
                     @endif
                     <!--end::Info-->
                  </div>
               </div>
               <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                   <div class="d-flex mb-9 border-bottom pb-9">
                     <!--begin: Pic-->
                     @if(in_array(4, $newArr))
                     <div class="flex-shrink-0 mr-7 mt-lg-0 mt-3">
                        <div class="symbol symbol-50 symbol-lg-60">
                           <img src="{{ asset($profile_pic) }}" alt="image">
                        </div>
                        <div class="symbol symbol-50 symbol-lg-120 symbol-primary d-none">
                           <span class="font-size-h3 symbol-label font-weight-boldest">JM</span>
                        </div>
                     </div>
                     <!--end::Pic-->
                     <!--begin::Info-->
                     <div class="flex-grow-1">
                        <!--begin::Title-->
                        <div class="d-flex justify-content-between flex-wrap mt-1">
                           <div class="d-flex mr-3">
                              <a href="#" class="text-dark-75 text-hover-primary font-size-h5 font-weight-bold mr-3">{{$experience->experience->user->user_fname .' '.$experience->experience->user->user_lname}}</a>
                              <div class="d-flex align-items-center traveler-ratings-tabs">
                                {{$forHost->rating}}
                                 <ul>
                                    <li class="filled-star"><i class="fas fa-star" aria-hidden="true"></i></li>
                                 </ul>
                              </div>
                           </div>
                        </div>
                        <!--end::Title-->
                        <!--begin::Content-->
                        <div class="d-flex flex-wrap justify-content-between mt-1">
                           <div class="d-flex flex-column flex-grow-1 pr-8">
                              <div class="d-flex flex-wrap mb-4 justify-content-between">
                                 <a href="#" class="text-dark-50 text-hover-primary font-weight-bold mr-lg-8 mr-5 mb-lg-0 mb-2">{{$experience->experience->user->user_address }}</a>
                                 <span class="text-green">{{date('d, M, Y',strtotime($forHost->created_at))}}</span>
                              </div>
                              <span class="font-weight-bold text-dark-50">{{$forHost->review}}</span>
                              <span class="font-weight-bold text-dark-50"></span>
                           </div>
                        </div>
                        <!--end::Content-->
                     </div>
                     <!--end::Info-->
                     @else
                      <p> You not giving review to host</p>
                     @endif
                  </div>
                  
               </div>
               <div class="tab-pane fade" id="contact" role="tabpanel" aria-labelledby="contact-tab">
                   <div class="d-flex mb-9 border-bottom pb-9">
                     <!--begin: Pic-->
                      @if(in_array(3, $newArr))
                     <div class="flex-shrink-0 mr-7 mt-lg-0 mt-3">
                        <div class="symbol symbol-50 symbol-lg-60">
                           <img src="{{ asset($profile_pic) }}" alt="image">
                        </div>
                        <div class="symbol symbol-50 symbol-lg-120 symbol-primary d-none">
                           <span class="font-size-h3 symbol-label font-weight-boldest">JM</span>
                        </div>
                     </div>
                     <!--end::Pic-->
                     <!--begin::Info-->
                     <div class="flex-grow-1">
                        <!--begin::Title-->
                        <div class="d-flex justify-content-between flex-wrap mt-1">
                           <div class="d-flex mr-3">
                              <a href="#" class="text-dark-75 text-hover-primary font-size-h5 font-weight-bold mr-3">{{$experience->experience->user->user_fname .' '.$experience->experience->user->user_lname}}</a>
                              <div class="d-flex align-items-center traveler-ratings-tabs">
                                 {{$forScout->rating}}
                                 <ul>
                                    <li class="filled-star"><i class="fas fa-star" aria-hidden="true"></i></li>
                                 </ul>
                              </div>
                           </div>
                        </div>
                        <!--end::Title-->
                        <!--begin::Content-->
                        <div class="d-flex flex-wrap justify-content-between mt-1">
                           <div class="d-flex flex-column flex-grow-1 pr-8">
                              <div class="d-flex flex-wrap mb-4 justify-content-between">
                                 <a href="#" class="text-dark-50 text-hover-primary font-weight-bold mr-lg-8 mr-5 mb-lg-0 mb-2">{{$experience->experience->user->user_address}}</a>
                                 <span class="text-green">{{date('d, M, Y',strtotime($forScout->created_at))}}</span>
                              </div>
                              <span class="font-weight-bold text-dark-50">{{$forScout->review}}</span>
                              <span class="font-weight-bold text-dark-50"></span>
                           </div>
                        </div>
                        <!--end::Content-->
                     </div>
                     <!--end::Info-->
                      @else
                      <p> You not giving review to scout</p>
                     @endif
                  </div>
               </div>
            </div>
         </div>
      </div>
      @endif
   </div>
</div>
     @endsection
{{-- Scripts Section --}}
@section('scripts')
<script>
   

$(document).ready(function(){
  
  /* 1. Visualizing things on Hover - See next part for action on click */
  $('.stars li').on('mouseover', function(){
    var onStar = parseInt($(this).data('value'), 10); // The star currently mouse on
   
    // Now highlight all the stars that's not after the current hovered star
    $(this).parent().children('li.star').each(function(e){
      if (e < onStar) {
        $(this).addClass('hover');
      }
      else {
        $(this).removeClass('hover');
      }
    });
    
  }).on('mouseout', function(){
    $(this).parent().children('li.star').each(function(e){
      $(this).removeClass('hover');
    });
  });
  
  
  /* 2. Action to perform on click */
  $('.stars li').on('click', function(){
    $this = $(this); 
    var onStar = parseInt($(this).data('value'), 10); // The star currently selected
    var stars = $(this).parent().children('li.star');
    
    for (i = 0; i < stars.length; i++) {
      $(stars[i]).removeClass('selected');
    }
    
    for (i = 0; i < onStar; i++) {
      $(stars[i]).addClass('selected');
    }
    
    // JUST RESPONSE (Not needed) 

    // var ratingValue = parseInt($('.stars li.selected').last().data('value'), 10);
    var ratingValue = parseInt($this.data('value'), 10);
    var msg = "";
    if (ratingValue > 1) {
        msg = "Thanks! You rated this " + ratingValue + " stars.";   
    }
    else { 
        msg = "We will improve ourselves. You rated this " + ratingValue + " stars.";
    }
    responseMessage(msg,$(this),ratingValue);
    
  });
  
  
});


function responseMessage(msg,evt,ratingValue) {
    
    evt.parents(".review-form").find(".star-value").val(ratingValue);
    evt.parents(".review-form").find("textarea").attr("placeholder",msg); 
}
 $("#experince-review-for-exp").validate({
   	 ignore: [],
   	rules: {
   	  star_value:{
		required:true
	  },
	  review_text:{
	  	required:true
	  }
	},
	 messages: {
      star_value:{
		required:"Please choose rating."
	  },
	  review_text:{
         required:"Please give your review."
	  },
	}


   });
 $("#experince-review-for-host").validate({
   	 ignore: [],
   	rules: {
   	  star_value:{
		required:true
	  },
	  review_text:{
	  	required:true
	  }
	},
	 messages: {
      star_value:{
		required:"Please choose rating."
	  },
	  review_text:{
         required:"Please give your review."
	  },
	}

   });
 $("#experince-review-for-scout").validate({
   	 ignore: [],
   	rules: {
   	  star_value:{
		required:true
	  },
	  review_text:{
	  	required:true
	  }
	},
	 messages: {
      star_value:{
		required:"Please choose rating."
	  },
	  review_text:{
         required:"Please give your review."
	  },
	}

   });
$(".submitthis").click(function(){

    var formId = $(this).attr("data-form"); 
    var formIDD = $(this).data('form-name'); 
    var starVal = $(formId).find(".star-value").val();
    var textareaVal = $(formId).find("textarea").val();
    var isvaild,isvaild2=false; 
   
    if($("#"+formIDD).valid())
    {
    	let myForm = document.getElementById(formIDD);
    let formData = new FormData(myForm);
    $(formId).find(".review-loader").show();
    $.ajax({
            headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            type: 'post',
            url: "{{route('add-experience-feedback')}}",
            data: formData,
            dataType: 'json',
            contentType: false,
            cache: false,             
            processData: false,
            success: function (res) {
            $(formId).find(".review-loader").hide();
                if(res.isSucceeded == 1){
                 $(formId).append("<div class='success'> Thanks for submitting this </div>");
                 $(formId).attr("data-iscompleted",1); 
                  $("."+formIDD).fadeOut(1500, function() { 
                $("."+formIDD).remove(); 
            }); 
                 if($("#review-host").attr("data-iscompleted") == 1 && $("#experince-review").attr("data-iscompleted") == 1 && $("#review-scout").attr("data-iscompleted") == 1){
                     $(".innerText").find("h1").text("Thank you for sharing your valuable feedback").fadeIn(1600); 
                     $(".innerText").find("p").text(''); 
                 } 
                   
  
                } 

              
            }
          }); 
    
} 
     
});
</script> 
@endsection