@extends('layouts.app')
@section('content')

<main class="_16grqhk" id="site-content" tabindex="-1">
    <div class="_1cnse2m">
        <div class="_cne99n">
            <div>
                <div class="_gmaj6l">
                    <div>
                        <div class="_1gw6tte">
                            <div style="--gp-section-max-width:1128px;">
                                <div class="_zcn96s">
                                    <div class="_ugxysi">
                                        <div data-plugin-in-point-id="DESKTOP_TITLE" data-section-id="DESKTOP_TITLE" style="padding-bottom: 32px;">
                                            <section>
                                                <div class="_1m730l2">
                                                    <a href="/" class="_1mm2a5z">
                                                        <span class="_e296pg">
                                                            <svg viewBox="0 0 32 32" xmlns="http://www.w3.org/2000/svg" aria-label="Back" role="img" focusable="false" style="display: block; fill: none; height: 16px; width: 16px; stroke: currentcolor; stroke-width: 3; overflow: visible;">
                                                                <g fill="none">
                                                                    <path d="m20 28-11.29289322-11.2928932c-.39052429-.3905243-.39052429-1.0236893 0-1.4142136l11.29289322-11.2928932"></path>
                                                                </g>
                                                            </svg>
                                                        </span>
                                                    </a>
                                                </div>
                                                <div class="_onao3d">
                                                    <h1 tabindex="-1" class="_14i3z6h">Confirm and pay</h1>
                                                </div>
                                            </section>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="_g9yb1m">
                        <div class="_bdo76v7">
                            <div class="_1gw6tte">
                                <div style="--gp-section-max-width:1128px;">
                                    <div data-plugin-in-point-id="QUICKPAY_ERROR_MESSAGE" data-section-id="QUICKPAY_ERROR_MESSAGE">
                                        <div id="quick-pay-v2-error-message-container"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="_1gw6tte">
                                <div style="--gp-section-max-width:1128px;">
                                    <div data-plugin-in-point-id="TRIP_DETAILS_TITLE" data-section-id="TRIP_DETAILS_TITLE" style="padding-bottom: 32px;">
                                        <section>
                                            <div class="_zaj690">
                                                <h1 tabindex="-1" class="_14i3z6h">Your experience</h1>
                                            </div>
                                        </section>
                                    </div>
                                </div>
                            </div>
                            <div class="_1gw6tte">
                                <div style="--gp-section-max-width:1128px;">
                                    <div data-plugin-in-point-id="DATE_PICKER" data-section-id="DATE_PICKER">
                                        <section>
                                            <div class="_b7b6bk">
                                                <div class="_1qyi2pa">
                                                    <div class="_1khi3900">
                                                        <div class="_5kaapu">
                                                            <h2 tabindex="-1" class="_14i3z6h">Date</h2>
                                                        </div>
                                                    </div>
                                                    <div class="_h3yaq9">Fri, 15 Jan 7:30 pm - 8:45 pm (IST)</div>
                                                </div>
                                            </div>
                                        </section>
                                    </div>
                                </div>
                            </div>
                            <div class="_1gw6tte">
                                <div style="--gp-section-max-width:1128px;">
                                    <div data-plugin-in-point-id="GUEST_PICKER_DROPDOWN" data-section-id="GUEST_PICKER_DROPDOWN" style="padding-top: 16px;">
                                        <section>
                                            <div class="_1khi3900">
                                                <h2 tabindex="-1" class="_14i3z6h">Guests</h2>
                                            </div>
                                            <div style="display: inline-block;">
                                                <div class="_n8fy1d">
                                                    <div class="_4uwjd6">
                                                        <button data-testid="dropdown-guest-picker" type="button" class="_z75ovys">
                                                            <div class="_1athds8">
                                                                <div class="_1fm9zq">1 guest</div>
                                                                <div class="_j26h42">
                                                                    <svg viewBox="0 0 32 32" xmlns="http://www.w3.org/2000/svg" aria-hidden="true" role="presentation" focusable="false" style="display: block; fill: none; height: 16px; width: 16px; stroke: currentcolor; stroke-width: 4; overflow: visible;">
                                                                        <g fill="none">
                                                                            <path d="m12 4 11.2928932 11.2928932c.3905243.3905243.3905243 1.0236893 0 1.4142136l-11.2928932 11.2928932"></path>
                                                                        </g>
                                                                    </svg>
                                                                </div>
                                                            </div>
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                        </section>
                                    </div>
                                </div>
                            </div>
                            <div class="_1gw6tte">
                                <div style="--gp-section-max-width:1128px;">
                                    <div style="margin-top: 32px;">
                                        <div class="_npr0qi" style="border-top-color: rgb(221, 221, 221);"></div>
                                        <div data-plugin-in-point-id="ADDITIONAL_REQUIREMENTS_TITLE" data-section-id="ADDITIONAL_REQUIREMENTS_TITLE" style="padding-top: 32px; padding-bottom: 32px;">
                                            <section>
                                                <div class="_zaj690">
                                                    <h1 tabindex="-1" class="_14i3z6h">Required for your trip</h1>
                                                </div>
                                            </section>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="_1gw6tte">
                                <div style="--gp-section-max-width:1128px;">
                                    <div data-plugin-in-point-id="PHONE_VERIFICATION" data-section-id="PHONE_VERIFICATION">
                                        <div class="_b7b6bk">
                                            <div class="_1qyi2pa">
                                                <div class="_m4gvkxh">
                                                    <div class="_5kaapu">Phone Number</div>
                                                </div>
                                                <div class="_u5ls4vy">Add and verify your phone number so Airbnb can send you trip updates</div>
                                            </div>
                                            <button data-testid="checkout_platform.PHONE_VERIFICATION.add" type="button" class="_13xgr5hw">Add</button>
                                        </div>
                                        <div class="_v72lrv">
                                            <div>
                                                <div class="_g610es">
                                                    <div class="_8iif2u">
                                                        <svg viewBox="0 0 32 32" xmlns="http://www.w3.org/2000/svg" aria-hidden="true" role="presentation" focusable="false" style="display: block; height: 12px; width: 12px; fill: currentcolor;">
                                                            <path d="M16 1c8.284 0 15 6.716 15 15 0 8.284-6.716 15-15 15-8.284 0-15-6.716-15-15C1 7.716 7.716 1 16 1zm0 20.5a2 2 0 1 0 0 4 2 2 0 0 0 0-4zm1.5-16h-3V18h3V5.5z"></path>
                                                        </svg>
                                                    </div>
                                                    <div class="_1sqnphj">This field is required</div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="_cne99n"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="_1gw6tte">
                                <div style="--gp-section-max-width:1128px;">
                                    <div style="margin-top: 32px;">
                                        <div class="_npr0qi" style="border-top-color: rgb(221, 221, 221);"></div>
                                        <div data-plugin-in-point-id="OPTIONAL_INFO_TITLE" data-section-id="OPTIONAL_INFO_TITLE" style="padding-top: 32px; padding-bottom: 32px;">
                                            <section>
                                                <div class="_zaj690">
                                                    <h1 tabindex="-1" class="_14i3z6h">Optional info</h1>
                                                </div>
                                            </section>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="_1gw6tte">
                                <div style="--gp-section-max-width:1128px;" data-reactroot="">
                                    <div style="margin-top: 32px;">
                                        <div class="_npr0qi" style="border-top-color: rgb(221, 221, 221);"></div>
                                        <div data-plugin-in-point-id="THINGS_TO_KNOW_TITLE" data-section-id="THINGS_TO_KNOW_TITLE" style="padding-top: 32px; padding-bottom: 32px;">
                                            <section>
                                                <div class="_zaj690">
                                                    <h1 tabindex="-1" class="_14i3z6h">Things to know</h1>
                                                </div>
                                            </section>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="_1gw6tte">
                                <div style="--gp-section-max-width:1128px;" data-reactroot="">
                                    <div data-plugin-in-point-id="GUEST_REQUIREMENTS" data-section-id="GUEST_REQUIREMENTS">
                                        <section>
                                            <div class="_odim95z">
                                                <h2 tabindex="-1" class="_14i3z6h">Guest requirements</h2>
                                            </div>
                                            <div class="_u5ls4vy">Up to 10 guests ages 4 and up can attend.<span class="_pog3hg"><a loggingid="checkout_platform.GUEST_REQUIREMENTS.learn_more" href="/book/experiences/1550632/UGF5bWVudHNQcm9kdWN0UHJpY2VRdW90ZTowRzAwekFpRURhdFcxSUJpcllUWE9LVlJXbTM=?modal=GUEST_REQUIREMENTS" class="_gzog035">Learn more</a></span></div>
                                        </section>
                                    </div>
                                </div>
                            </div>
                            <div class="_1gw6tte">
                                <div style="--gp-section-max-width:1128px;" data-reactroot="">
                                    <div style="margin-top: 32px;">
                                        <div class="_npr0qi" style="border-top-color: rgb(221, 221, 221);"></div>
                                        <div data-plugin-in-point-id="FOR_WORK_TOGGLE" data-section-id="FOR_WORK_TOGGLE" style="padding-top: 32px;">
                                            <div class="_1eyqfglq">
                                                <div class="_1qyi2pa">
                                                    <div id="FOR_WORK_TOGGLE-title" class="_1khi3900">Booking for work?</div>
                                                </div>
                                                <button aria-checked="false" aria-labelledby="FOR_WORK_TOGGLE-title" id="FOR_WORK_TOGGLE-switch" role="switch" type="button" class="_aucgnq">
                                                    <div class="_nbd4yy8"></div>
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="_1gw6tte">
                                <div style="--gp-section-max-width:1128px;" data-reactroot="">
                                    <div style="margin-top: 32px;">
                                        <div class="_npr0qi" style="border-top-color: rgb(221, 221, 221);"></div>
                                        <div data-plugin-in-point-id="PAYMENT_OPTIONS" data-section-id="PAYMENT_OPTIONS" style="padding-top: 32px; padding-bottom: 16px;">
                                            <div>
                                                <div class="_gmaj6l">
                                                    <section>
                                                        <div data-testid="payment-option-dropdown-selector">
                                                            <div data-showat="largeAndAbove">
                                                                <div class="_15x2itj9">
                                                                    <label class="_xfxccjs" for="payment_option_selector">
                                                                        <div class="_qt50hbx">
                                                                            <h1 tabindex="-1" class="_14i3z6h">Pay with</h1>
                                                                        </div>
                                                                    </label>
                                                                    <div class="_1nc6ity"><span class="_huz4f0"><img src="https://a0.muscache.com/airbnb/static/packages/logo_visa.0adea522.svg" alt="Visa Card" height="9"></span><span class="_huz4f0"><img src="https://a0.muscache.com/airbnb/static/packages/logo_amex.84088b52.svg" alt="American Express Card" height="9"></span><span class="_huz4f0"><img src="https://a0.muscache.com/airbnb/static/packages/logo_mastercard.f18379cf.svg" alt="Mastercard" height="9"></span></div>
                                                                </div>
                                                            </div>
                                                            <span id="payment_option_selector_described_by" class="_krjbj">Select or add a new payment method</span>
                                                            <div>
                                                                <div class="_e296pg">
                                                                    <button type="button" aria-describedby="payment_option_selector_described_by" aria-expanded="false" aria-haspopup="listbox" aria-invalid="false" class="_wz0818y">
                                                                        <div aria-disabled="false" id="dropdown-selector-payment_option_selector-input" tabindex="-1" class="_1wcpnjd">
                                                                            <div class="_hgs47m">
                                                                                <div class="_ni9axhe">
                                                                                    <div>
                                                                                        <div style="margin-right: 16px;">
                                                                                            <span class="_edyuot">
                                                                                                <span class="_1vbf4vp">
                                                                                                    <svg viewBox="0 0 40 24" role="presentation" aria-hidden="true" focusable="false" style="display: block; fill: currentcolor;">
                                                                                                        <path d="m39 21.5c0 .8-.7 1.5-1.5 1.5h-35c-.8 0-1.5-.7-1.5-1.5v-19c0-.8.7-1.5 1.5-1.5h35c .8 0 1.5.7 1.5 1.5z" fill="#fff"></path>
                                                                                                        <path d="m25 12c0 .3-.2.5-.5.5h-4v4c0 .3-.2.5-.5.5s-.5-.2-.5-.5v-4h-4c-.3 0-.5-.2-.5-.5s.2-.5.5-.5h4v-4c0-.3.2-.5.5-.5s.5.2.5.5v4h4c .3 0 .5.2.5.5z" fill="#c4c4c4"></path>
                                                                                                        <path d="m2.5 0h35c1.4 0 2.5 1.1 2.5 2.5v19c0 1.4-1.1 2.5-2.5 2.5h-35c-1.4 0-2.5-1.1-2.5-2.5v-19c0-1.4 1.1-2.5 2.5-2.5zm0 1c-.8 0-1.5.7-1.5 1.5v19c0 .8.7 1.5 1.5 1.5h35c .8 0 1.5-.7 1.5-1.5v-19c0-.8-.7-1.5-1.5-1.5z" fill="#dbdbdb"></path>
                                                                                                    </svg>
                                                                                                </span>
                                                                                            </span>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="_10ejfg4u">
                                                                                    <div>Credit or Debit Card</div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </button>
                                                                    <input name="payment_option_selector" type="hidden" value="ADYEN_PAYU">
                                                                    <span class="_qcdbez">
                                                                        <div class="_nncr1bm">
                                                                            <div class="_ni9axhe">
                                                                                <div class="_wn4bip" style="transform: rotate(0deg);">
                                                                                    <svg viewBox="0 0 18 18" role="presentation" aria-hidden="true" focusable="false" style="height: 16px; width: 16px; display: block; fill: rgb(72, 72, 72);">
                                                                                        <path d="m16.29 4.3a1 1 0 1 1 1.41 1.42l-8 8a1 1 0 0 1 -1.41 0l-8-8a1 1 0 1 1 1.41-1.42l7.29 7.29z" fill-rule="evenodd"></path>
                                                                                    </svg>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </span>
                                                                </div>
                                                                <div class="_e296pg"></div>
                                                            </div>
                                                        </div>
                                                    </section>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="_1gw6tte">
                                <div style="--gp-section-max-width:1128px;" data-reactroot="">
                                    <div style="margin-bottom: 16px;">
                                        <div data-plugin-in-point-id="COUPONS" data-section-id="COUPONS" style="">
                                            <div class="_zjunba"><button type="button" class="_ejra3kg">Enter a coupon</button></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="_1gw6tte">
                                <div style="--gp-section-max-width:1128px;" data-reactroot="">
                                    <div class="_npr0qi" style="border-top-color: rgb(221, 221, 221);"></div>
                                    <div data-plugin-in-point-id="TERMS_AND_CONDITIONS" data-section-id="TERMS_AND_CONDITIONS" style="padding-top: 32px; padding-bottom: 32px;">
                                        <div class="_n6ouu8">By selecting the button below, you agree to the <a target="_blank" href="/terms/experiences_guest_waiver" class="_6rdr2z5">Guest Release and Waiver</a>, the <a target="_blank" href="/experiences/cancellation-policy" class="_6rdr2z5">Cancellation Policy</a>, and the <a target="_blank" href="/help/article/2278" class="_6rdr2z5">Guest Refund Policy</a>.</div>
                                    </div>
                                </div>
                            </div>
                            <div class="_1gw6tte">
                                <div style="--gp-section-max-width:1128px;" data-reactroot="">
                                    <div data-plugin-in-point-id="CONFIRM_AND_PAY" data-section-id="CONFIRM_AND_PAY" style="padding-top: 16px; padding-bottom: 48px;">
                                        <div class="_gmaj6l">
                                            <div data-testid="submit-button">
                                                <button type="button" class="_1501mvw9" data-veloute="checkout-flow-submit-button">
                                                    <span class="_163rr5i"><span class="_19di23v" style="background-position: calc((100 - var(--mouse-x, 0)) * 1%) calc((100 - var(--mouse-y, 0)) * 1%);"></span></span>
                                                    <span class="_tcp689">
                                                        <span class="_14d5b3i">
                                                            <span class="_1r769ej">
                                                                <svg viewBox="0 0 24 24" role="presentation" aria-hidden="true" focusable="false" style="height: 1em; width: 1em; display: block; fill: currentcolor;">
                                                                    <path d="m17.1403105 10.3322131v-1.83599021c-.0044371-2.48293439-2.0786109-4.49407316-4.6371478-4.49622289h-1.0063254c-2.56010616.00215385-4.6349284 2.01566919-4.63714782 4.50012925v1.83208385c-1.0387857.0478557-1.85657082.8775506-1.85968948 1.886773v6.8825219c0 1.0485082.87586388 1.898492 1.95629672 1.898492h10.08740658c1.0804328 0 1.9562967-.8499838 1.9562967-1.898492v-6.8825219c-.0031187-1.0092224-.8209038-1.8389173-1.8596895-1.886773zm-7.90518974-1.83599021c.00267117-2.2124746 1.58801224-2.19412124 2.26171654-2.19488726h1.0063254c.7450523.00084715 2.2590036-.05221743 2.2617165 2.19488726v1.83599021h-5.52975844z"></path>
                                                                </svg>
                                                            </span>
                                                            <span class="_3hmsj">Confirm and pay</span>
                                                        </span>
                                                    </span>
                                                </button>
                                            </div>
                                        </div>
                                        <div class="_gmaj6l"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="_ai1he2">
                            <div class="_1uqg5g6r">
                                <div class="_7efq7j">
                                    <div class="_1gw6tte">
                                        <div style="--gp-section-max-width:1128px;">
                                            <div style="margin-top: 24px;">
                                                <div data-plugin-in-point-id="LISTING_CARD_EXPERIENCES" data-section-id="LISTING_CARD_EXPERIENCES">
                                                    <div class="_i77e3d">
                                                        <div class="_8kqo1s">
                                                            <div class="_1h6n1zu" style="display: inline-block; vertical-align: bottom; height: 100%; width: 100%; min-height: 1px;">
                                                                <img class="_9ofhsl" aria-hidden="true" alt="" id="FMP-target" src="https://a0.muscache.com/im/pictures/lombard/MtTemplate-1550632-media_library/original/8d83a883-2fd6-4eab-9c8f-188f8d6d2b10.jpeg?aki_policy=large" data-original-uri="https://a0.muscache.com/im/pictures/lombard/MtTemplate-1550632-media_library/original/8d83a883-2fd6-4eab-9c8f-188f8d6d2b10.jpeg?aki_policy=large" style="object-fit: cover; vertical-align: bottom;">
                                                                <div class="_15p4g025" style="background-image: url(&quot;https://a0.muscache.com/im/pictures/lombard/MtTemplate-1550632-media_library/original/8d83a883-2fd6-4eab-9c8f-188f8d6d2b10.jpeg?aki_policy=large&quot;); background-size: cover;"></div>
                                                            </div>
                                                        </div>
                                                        <div class="_ooekcv">
                                                            <div class="_1ssrgzh">Online · 75 mins</div>
                                                            <div class="_3hmsj">
                                                                <div id="LISTING_CARD_EXPERIENCES-title" class="_1tn13uh">Sunset Hike with a Geologist</div>
                                                                <div class="_1hunydg">Hosted in English</div>
                                                            </div>
                                                            <div class="_1joc06t">
                                                                <div class="_uwozh7">
                                                                    <span class="_60hvkx2" role="img" aria-label="Rating 4.93 out of 5; 40 reviews">
                                                                        <span class="_1lzlk52">
                                                                            <svg viewBox="0 0 32 32" xmlns="http://www.w3.org/2000/svg" aria-hidden="true" role="presentation" focusable="false" style="display: block; height: 12px; width: 12px; fill: currentcolor;">
                                                                                <path d="M15.094 1.579l-4.124 8.885-9.86 1.27a1 1 0 0 0-.542 1.736l7.293 6.565-1.965 9.852a1 1 0 0 0 1.483 1.061L16 25.951l8.625 4.997a1 1 0 0 0 1.482-1.06l-1.965-9.853 7.293-6.565a1 1 0 0 0-.541-1.735l-9.86-1.271-4.127-8.885a1 1 0 0 0-1.814 0z" fill-rule="evenodd"></path>
                                                                            </svg>
                                                                        </span>
                                                                        <span class="_2a6au1i" aria-hidden="true">4.93</span><span class="_a7a5sx" aria-hidden="true">&nbsp;(40)</span>
                                                                    </span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="_1gw6tte">
                                        <div style="--gp-section-max-width:1128px;">
                                            <div style="margin-top: 24px;">
                                                <div class="_npr0qi" style="border-top-color: rgb(221, 221, 221);"></div>
                                                <div data-plugin-in-point-id="PRICE_DETAIL_TITLE" data-section-id="PRICE_DETAIL_TITLE" style="padding-top: 24px;">
                                                    <section>
                                                        <div class="_zaj690">
                                                            <h1 tabindex="-1" class="_14i3z6h">Price details</h1>
                                                        </div>
                                                    </section>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="_1gw6tte">
                                        <div style="--gp-section-max-width:1128px;">
                                            <div style="margin-top: 24px;">
                                                <div data-plugin-in-point-id="PRICE_DETAIL" data-section-id="PRICE_DETAIL">
                                                    <div class="_gmaj6l">
                                                        <div>
                                                            <div style="margin-top: 0px;">
                                                                <div class="_hgs47m">
                                                                    <div class="_10ejfg4u">
                                                                        <div class="_x3c6nv">₹731.24 x 1 guest</div>
                                                                    </div>
                                                                    <div class="_ni9axhe">
                                                                        <div class="_4afltz"><span class="_17j792vp"><span>₹731.24</span></span></div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div style="margin-top: 16px;">
                                                                <div class="_hgs47m">
                                                                    <div class="_10ejfg4u">
                                                                        <div class="_1k04cz3c">Total <button id="MowebCurrencyPicker_trigger" type="button" class="_ejra3kg">(INR)</button></div>
                                                                    </div>
                                                                    <div class="_ni9axhe">
                                                                        <div class="_4afltz"><span class="_ba3mo2p"><span>₹731.24</span></span></div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="_1gw6tte">
                                        <div style="--gp-section-max-width:1128px;">
                                            <div style="margin-top: 24px;">
                                                <div class="_npr0qi" style="border-top-color: rgb(221, 221, 221);"></div>
                                                <div data-plugin-in-point-id="CANCELLATION_POLICY" data-section-id="CANCELLATION_POLICY" style="padding-top: 24px;">
                                                    <section>
                                                        <div class="_11jkqip">
                                                            <div class="_z2qhxz">
                                                                <span id="CANCELLATION_POLICY-title" class="_1e3bi8a">
                                                                    <h1 tabindex="-1" class="_14i3z6h">Cancellation policy</h1>
                                                                </span>
                                                                <span class="_t8y1n6">Any experience can be cancelled and fully refunded within 24 hours of purchase, or at least 7 days before the experience starts. <a rel="noopener noreferrer" target="_blank" href="https://www.airbnb.co.in/help/article/1593/what-is-the-experiences-cancellation-policy" class="_gzog035">Learn More</a></span>
                                                            </div>
                                                        </div>
                                                    </section>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
@endsection
{{-- Scripts Section --}}
@section('javascript')
<script type="text/javascript">
</script>
@endsection