{{-- Extends layout --}}
@extends('layout.default')
{{-- Content --}}
@section('content')
<style>
	.toggle.ios, .toggle-on.ios, .toggle-off.ios { border-radius: 20rem; }
	.toggle.ios .toggle-handle { border-radius: 20rem; }
	div#action-scout {
	    align-items: center;
	    justify-content: center;
	}
</style>
 @if(session()->has('success'))
            <div class="alert alert-success alluser-page-suc" style="text-align: left;float: left;width: 100%;">
                {{ session()->get('success') }}
            </div>
            <script type="text/javascript">setTimeout(function(){ $('.alluser-page-suc').fadeOut(); }, 3000);</script>
        @elseif(session()->has('failure'))
            <div class="alert alert-danger alluser-page-suc" style="text-align: left; left;float: left;width: 100%;">
                {{ session()->get('failure') }}
            </div>
            <script type="text/javascript">setTimeout(function(){ $('.alluser-page-suc').fadeOut(); }, 3000);</script>
        @endif
    <div class="main-body">
          <!--  <div class="user-profile-buttons">
            <a href="{{ route('user-profile-edit') }}" class="navi-item">Update Profile</a>
            <a href="{{ route('user-change-password-form') }}" class="navi-item">Change Password</a>
          </div> -->
		  
		  
		  <div class="d-flex flex-row w-100">
								@include('profile-sidebar')
									<!--begin::Content-->
									<div class="flex-row-fluid ml-lg-8">
										<!--begin::Card-->
										<div class="card card-custom card-stretch card-sticky" id="kt_page_sticky_card">
											<!--begin::Header-->
											<div class="card-header py-3">
												<div class="card-title align-items-start flex-column">
													<h3 class="card-label font-weight-bolder text-dark">Personal Information</h3>
													<span class="text-muted font-weight-bold font-size-sm mt-1">Update your personal informaiton</span>
												</div>
												<div class="card-toolbar">
													<button type="button" class="btn btn-success mr-2 green-bg-btn" id="save-profile-button">Save Changes</button>
													<!-- <button type="reset" class="btn btn-secondary">Cancel</button> -->
												</div>
											</div>
											<!--end::Header-->
											<!--begin::Form-->
											<form id="edit-scout-form" class="form" method="post" action="{{ route('profile-update') }}" enctype="multipart/form-data">
												<!--begin::Body-->
												@csrf
												<div class="card-body">
													<div class="form-group row manage-personal-info">
														<div class="col-lg-4">
														<label class="col-xl-12 col-lg-12 col-form-label">Avatar</label>
														<div class="col-lg-12 col-xl-12">
															@if (file_exists(public_path($scoutData->user_image_path)) && $scoutData->user_image)
															<div class="image-input image-input-outline" id="kt_profile_avatar" style="background-image: url({{ asset($scoutData->user_image_path.'/'.$scoutData->user_image) }})">
															@else
															<div class="image-input image-input-outline" id="kt_profile_avatar" style="background-image: url({{ asset('users/user.png') }})">
															@endif
																<div class="image-input-wrapper" style="background-image: url(/metronic/theme/html/demo1/dist/assets/media/users/300_21.jpg)"></div>
																<label class="btn btn-xs btn-icon btn-circle btn-white btn-hover-text-primary btn-shadow" data-action="change" data-toggle="tooltip" title="" data-original-title="Change avatar">
																	<i class="fa fa-pen icon-sm text-muted"></i>
																	<input type="file" name="user_image">
																	<input type="hidden" name="profile_avatar_remove">
																</label>
																<span class="btn btn-xs btn-icon btn-circle btn-white btn-hover-text-primary btn-shadow" data-action="cancel" data-toggle="tooltip" title="" data-original-title="Cancel avatar">
																	<i class="ki ki-bold-close icon-xs text-muted"></i>
																</span>
																<span class="btn btn-xs btn-icon btn-circle btn-white btn-hover-text-primary btn-shadow" data-action="remove" data-toggle="tooltip" title="" data-original-title="Remove avatar">
																	<i class="ki ki-bold-close icon-xs text-muted"></i>
																</span>
															</div>
															<span class="form-text text-muted">Allowed file types: png, jpg, jpeg.</span>
															<label id="user_image-error" class="error" style="display:none;" for="user_image">Please select the image</label>
														</div>
													</div>

													<div class="col-lg-8 pl-0 pr-0">
														<div class="form-group row">
														<label class="col-xl-12 col-lg-12 col-form-label">First Name</label>
														<div class="col-lg-12 col-xl-12">
															<input class="form-control form-control-lg form-control-solid" name="user_fname" type="text" value="{{$scoutData->user_fname}}">
														</div>
													</div>
													<div class="form-group row">
														<label class="col-xl-12 col-lg-12 col-form-label">Last Name</label>
														<div class="col-lg-12 col-xl-12">
															<input class="form-control form-control-lg form-control-solid" name="user_lname" type="text" value="{{$scoutData->user_lname}}">
														</div>
													</div>
													</div>

													</div>





													
													<!-- <div class="form-group row">
														<label class="col-xl-3 col-lg-3 col-form-label">Company Name</label>
														<div class="col-lg-9 col-xl-6">
															<input class="form-control form-control-lg form-control-solid" type="text" value="Loop Inc.">
															<span class="form-text text-muted">If you want your invoices addressed to a company. Leave blank to use your full name.</span>
														</div>
													</div> -->
													<div class="row">
														<label class="col-xl-3"></label>
														<div class="col-lg-9 col-xl-6">
															<h5 class="font-weight-bold mt-10 mb-6">Contact Info</h5>
														</div>
													</div>
													<div class="form-group row">
														<label class="col-xl-3 col-lg-3 col-form-label">Contact Phone</label>
														<div class="col-lg-9 col-xl-6">
															<div class="input-group input-group-lg input-group-solid">
																<!--div class="input-group-prepend">
																	<span class="input-group-text">
																		<i class="la la-phone"></i>
																	</span>
																</div-->

																<select name="user_mobile_code" class="country-code-select selectpicker profileCountryCode" data-size="5" data-live-search="true">
																	@forelse($getCountryCode as $CountryCode)
																	@php 
																		$selected_countryCode = '';
																		if(!empty($getCountryCode) && $CountryCode->phonecode==$scoutData->user_mobile_code){
																		$selected_countryCode = 'selected';
																	}
																	@endphp
							                                        <option value="{{ $CountryCode->phonecode }}" {{$selected_countryCode}}>+{{ $CountryCode->phonecode.' '.$CountryCode->iso }}</option>
							                                        @empty
							                                        @endforelse
																</select>
																<input type="text" class="form-control form-control-lg form-control-solid user_mobileMasking" value="{{$scoutData->user_mobile}}" id="user_mobile" name="user_mobile" placeholder="Phone">
															</div>
															<span class="form-text text-muted">We'll never share your email with anyone else.</span>
														</div>
													</div>
													<div class="form-group row">
														<label class="col-xl-3 col-lg-3 col-form-label">Email Address</label>
														<div class="col-lg-9 col-xl-6">
															<div class="input-group input-group-lg input-group-solid">
																<div class="input-group-prepend">
																	<span class="input-group-text">
																		<i class="la la-at"></i>
																	</span>
																</div>
																<input type="email" name="email" {{ isset($scoutData) ? "readonly" : "" }} value="{{ isset($scoutData) ? $scoutData->email : '' }}" class="form-control form-control-lg form-control-solid {{ isset($scoutData) ? 'read-only' : '' }}"  placeholder="Email">
															</div>
														</div>
													</div>
													 <div class="form-group row">
														<label class="col-xl-3 col-lg-3 col-form-label">Address</label>
														<div class="col-lg-9 col-xl-6">
															<div class="input-group input-group-lg input-group-solid">
																<input type="text" name="user_address" class="form-control form-control-lg form-control-solid" value="{{$scoutData->user_address}}" placeholder="Username" value="loop">
																
															</div>
														</div>
													</div> 
												</div>
												<!--end::Body-->
											</form>
											<!--end::Form-->
										</div>
									</div>
									<!--end::Content-->
								</div>
		  
        </div>
        <!-- /.modal -->
	    <div class="modal fade" id="action-scout">
	        <div class="modal-dialog">
	            <form id="action-scout-form" action="" method="post">
	                <div class="modal-content">
	                    <div class="modal-header">
	                        <h4 class="modal-title scout-title"></h4>
	                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	                        <span aria-hidden="true">&times;</span></button>
	                    </div>
	                    <div class="modal-body">
	                        <p id="cnt-modal"></p>
	                        <span id="dynInput"></span>
	                        @csrf
	                        <input type="hidden" name="status" id="status" value="{{ $scoutData->status }}">              
	                        <input type="hidden" name="scout_id" id="scout_id" value="<?php echo base64_encode($scoutData->id)?>">              
	                    </div>
	                    <div class="modal-footer justify-content-between">
	                        <button type="button" class="btn btn-secondary no-action" data-dismiss="modal">No</button>
	                        <button type="button" class="btn btn-secondary"data-dismiss="modal" onclick="formSubmit()">Yes</button>
	                    </div>
	                </div>
	            </form>
	        </div>
	    </div>
	    <!-- /.modal -->

@endsection
{{-- Scripts Section --}}
@section('scripts')
<script type="text/javascript">
    $.ajaxSetup({headers: {'X-XSRF-TOKEN': decodeURIComponent(/XSRF-Token=([^;]*)/ig.exec(document.cookie)[1])}});
    $(document).ready(function () {
    	var user = '<?php echo (!empty($scoutData->user_image))? $scoutData->user_image :''; ?>';
        $.validator.addMethod('filesize', function (value, element, param) {
            return this.optional(element) || (element.files[0].size <= param)
        }, 'File size must be less than {0}');
        $('#edit-scout-form').validate({
            
            rules: {
                user_fname: {
                    required: true
                },
                user_lname: {
                    required: true,
                },
                email:{
                    required: true,
                },
                user_address:{
                    required: true
                },
                user_mobile:{
                    required: true,
                   // digits: true,
                    // maxlength: 10,
                    minlength: 10,
                    remote: {
                        url: HOST_URL+"/check-user-mobile-existence",
                        type: "post"
                    }
                },
                user_image:{
                    required: function(){
                        if(user!=''){
                            return false;
                        }else{
                            return true;
                        }
                    },
                    extension: "jpg,jpeg,png",
                    filesize: 1*1024*1024,
                },
            },
            messages: {
                user_fname: {
                    required: "Please enter your first name",
                },  
                user_lname: {                 
                    required : "Please enter your last name",
                },
                user_address: {
                    required : "Please enter your address",
                },
                email: {
                    required : "Please enter your email address",
                },
                user_mobile: {
                    required : "Please enter your mobile number",
                    remote: "This mobile number already exist"
                },
                user_image:{
                    required: "Please select the image",
                    extension: "Please select jpg,jpeg and png image",
                    filesize: "File size must be less than 2MB",
                },
            }
        });
         $("#save-profile-button").click(function(){
          if($("#edit-scout-form").valid())
          {
          	let myForm = document.getElementById('edit-scout-form');
          	 myForm.submit();
          }
        });
    });
</script>
@endsection